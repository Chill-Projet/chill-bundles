<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

class ernel extends BaseKernel
{
    use MicroKernelTrait;

    private function getBundlesPath(): string
    {
        return $this->getConfigDir().'/bundles.php';
    }

    private function getConfigDir(): string
    {
        return $this->getProjectDir().'/tests/app/config';
    }
}
