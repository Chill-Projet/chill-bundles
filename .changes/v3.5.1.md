## v3.5.1 - 2024-12-16
### Fixed
* Filiation: fix the display of the gender label in the graph 
* Wrap handling of PdfSignedMessage into transactions 
