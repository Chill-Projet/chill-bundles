## v2.6.0 - 2023-09-14
### Feature
* ([#133](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/133)) Add locations in Aside Activity. By default, suggest user location, otherwise a select with all locations. 
* ([#133](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/133)) Adapt Aside Activity exports: display location, filter by location, group by location 
* Use the CRUD controller for center entity + add the isActive property to be able to mask instances of Center that are no longer in use. 
### Fixed
* ([#107](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/107)) reinstate the fusion of duplicate persons 
* Missing translation in Work Actions exports 
* Reimplement the mission type filter on tasks, only for instances that have a config parameter indicating true for this. 
* ([#135](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/135)) Corrects a typing error in 2 filters, which caused an 
error when trying to reedit a saved export

 
* ([#136](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/136)) [household] when moving a person to a sharing position to a not-sharing position on the same household on the same date, remove the previous household membership on the same household. This fix duplicate member. 
* Add missing translation for comment field placeholder in repositionning household editor.
 
* Do not send an email to creator twice when adding a comment to a notification 
* ([#107](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/107)) Fix gestion doublon functionality to work with chill bundles v2 
### UX
* Uniformize badge-person in household banner (background, size)
 
