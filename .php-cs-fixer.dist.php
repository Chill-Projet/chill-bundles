<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

$finder = PhpCsFixer\Finder::create();

$finder
    ->in(__DIR__.'/src')
    ->in(__DIR__.'/utils')
    ->append([__FILE__])
    ->exclude(['docs/', 'tests/app'])
    ->notPath('tests/app')
    ->ignoreDotFiles(true)
    ->name('**.php')
;

$config = new PhpCsFixer\Config();
$config
    ->setFinder($finder)
    ->setRiskyAllowed(true)
    ->setCacheFile('var/php-cs-fixer.cache')
    ->setUsingCache(true)
    ->setParallelConfig(PhpCsFixer\Runner\Parallel\ParallelConfigFactory::detect())
;

$rules = $config->getRules();

$riskyRules = [
    'ternary_to_elvis_operator' => false,
    'php_unit_mock_short_will_return' => false,
    'php_unit_set_up_tear_down_visibility' => false,
    'php_unit_construct' => false,
    'php_unit_dedicate_assert' => false,
    'php_unit_expectation' => false,
    'php_unit_mock' => false,
    'php_unit_namespaced' => false,
    'php_unit_no_expectation_annotation' => false,
    'php_unit_test_case_static_method_calls' => false,
    'php_unit_test_annotation' => false,
    //    'final_internal_class' => false,
    //    'strict_param' => false,
    //    'declare_strict_types' => false,
    //    'strict_comparison' => false,
    //    'no_unreachable_default_argument_value' => false,
    //    'ereg_to_preg' => false,
    //    'ordered_interfaces' => false,
    //    'error_suppression' => false,
    //    'non_printable_character' => false,
    //    'ordered_traits' => false,
    //    'no_useless_sprintf' => false,
    //    'dir_constant' => false,
    //    'no_alias_functions' => false,
    //    'implode_call' => false,
    //    'combine_nested_dirname' => false,
    //    'pow_to_exponentiation' => false,
    //    'comment_to_phpdoc' => false,
    //    'no_unset_on_property' => false,
    //    'native_constant_invocation' => false,
    //    'function_to_constant' => false,
    //    'is_null' => false,
    //    'native_function_invocation' => false,
    //    'no_trailing_whitespace_in_string' => false,
    //    'array_push' => false,
    //    'fopen_flag_order' => false,
    //    'fopen_flags' => false,
    //    'logical_operators' => false,
    //    'modernize_types_casting' => false,
    //    'no_homoglyph_names' => false,
    //    'no_unneeded_final_method' => false,
    //    'random_api_migration' => false,
    //    'static_lambda' => false,
    //    'set_type_to_cast' => false,
    //    'string_line_ending' => false,
    //    'psr_autoloading' => false,
];

$untilFullSwitchToPhp8 = [
    'blank_line_between_import_groups' => false,
    'declare_strict_types' => true,
    'multiline_whitespace_before_semicolons' => false,
    'phpdoc_no_empty_return' => false,
];

$rules = array_merge(
    [
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => false,
        '@Symfony' => true,
        '@Symfony:risky' => false,
        'ordered_class_elements' => [
            'order' => [
                'use_trait',
                'constant_public',
                'constant_protected',
                'constant_private',
                'property_public',
                'property_protected',
                'property_private',
                'construct',
                'destruct',
                'magic',
                'phpunit',
                'method_public',
                'method_protected',
                'method_private',
            ],
            'sort_algorithm' => 'alpha',
        ],
        'single_line_empty_body' => true,
    ],
    $rules,
    $riskyRules,
    $untilFullSwitchToPhp8,
);

$rules['header_comment']['header'] = trim(file_get_contents(__DIR__.'/resources/header.txt'));

return $config->setRules($rules);
