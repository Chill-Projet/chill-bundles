// @ts-ignore Cannot find module (when used within an app)
import { trans, getLocale, setLocale, setLocaleFallbacks } from "@symfony/ux-translator";

setLocaleFallbacks({"en": "fr", "nl": "fr", "fr": "en"});
setLocale('fr');

export { trans };
// @ts-ignore Cannot find module (when used within an app)
export * from '../var/translations';
