
# Switch to symfony 5.0

- the tag `chill.role` is now renamed to `chill_main.provide_role`.

  **Note**: It is not necessary to apply this tag on service definition: the tag is automatically applyied if the
  service implements `\Chill\MainBundle\Security\ProvideRoleInterface`.

- those annotation can be converted to attribute:

  - `Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\AccompanyingPeriodValidity`
  - `Chill\PersonBundle\Validator\Constraints\Household\HouseholdMembershipSequential`
  - `Chill\PersonBundle\Validator\Constraints\Household\MaxHolder`
  - `Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ConfidentialCourseMustHaveReferrer`
  - `Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\LocationValidity`
  - `Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ParticipationOverlap`
  - `Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ResourceDuplicateCheck`
  - `Chill\PersonBundle\Validator\Constraints\Person\Birthdate`
  - `Chill\PersonBundle\Validator\Constraints\Person\PersonHasCenter`
  - `Chill\PersonBundle\Validator\Constraints\Relationship\RelationshipNoDuplicate`
  - `Chill\ActivityBundle\Validator\Constraints\ActivityValidity`
  - `Chill\DocStoreBundle\Validator\Constraints\AsyncFileExists`
  - `Chill\MainBundle\Validation\Constraint\PhonenumberConstraint`
  - `Chill\MainBundle\Validator\Constraints\Entity\UserCircleConsistency`
  - `Chill\MainBundle\Workflow\Validator\EntityWorkflowCreation`

  Here is the rector rule that can be used to switch attributes to annotations:

  ```php
    $rectorConfig->ruleWithConfiguration(\Rector\Php80\Rector\Class_\AnnotationToAttributeRector::class, [
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\AccompanyingPeriodValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Household\HouseholdMembershipSequential'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Household\MaxHolder'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ConfidentialCourseMustHaveReferrer'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\LocationValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ParticipationOverlap'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ResourceDuplicateCheck'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Person\Birthdate'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Person\PersonHasCenter'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Relationship\RelationshipNoDuplicate'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\ActivityBundle\Validator\Constraints\ActivityValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\DocStoreBundle\Validator\Constraints\AsyncFileExists'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Validation\Constraint\PhonenumberConstraint'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Validator\Constraints\Entity\UserCircleConsistency'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Workflow\Validator\EntityWorkflowCreation'),
    ]);
    ```

- to keep cleaner definition in container's dependency injection and framework bundle, the definition of crud or api
  requires to define explicitly a controller.

  Before:

  ```php
  $container->prependExtensionConfig('chill_main', [
      'apis' => [
          [
              'class' => ThirdParty::class,
              'name' => 'thirdparty',
              'base_path' => '/api/1.0/thirdparty/thirdparty',
              'actions' => [
                  '_entity' => [
                      'methods' => [
                          Request::METHOD_GET => true,
                          Request::METHOD_HEAD => true,
                          Request::METHOD_POST => true,
                          Request::METHOD_PUT => true,
                          Request::METHOD_PATCH => true,
                      ],
                      'roles' => [
                          Request::METHOD_GET => ThirdPartyVoter::SHOW,
                          Request::METHOD_HEAD => ThirdPartyVoter::SHOW,
                          Request::METHOD_POST => ThirdPartyVoter::CREATE,
                          Request::METHOD_PUT => ThirdPartyVoter::CREATE,
                          Request::METHOD_PATCH => ThirdPartyVoter::CREATE,
                      ],
                  ],
              ],
          ],
      ],
  ]);

  After:

  ```php
  $container->prependExtensionConfig('chill_main', [
      'apis' => [
          [
              'class' => ThirdParty::class,
              'controller' => ThirdPartyApiController::class,
              'name' => 'thirdparty',
              'base_path' => '/api/1.0/thirdparty/thirdparty',
              'actions' => [
                  '_entity' => [
                      'methods' => [
                          Request::METHOD_GET => true,
                          Request::METHOD_HEAD => true,
                          Request::METHOD_POST => true,
                          Request::METHOD_PUT => true,
                          Request::METHOD_PATCH => true,
                      ],
                      'roles' => [
                          Request::METHOD_GET => ThirdPartyVoter::SHOW,
                          Request::METHOD_HEAD => ThirdPartyVoter::SHOW,
                          Request::METHOD_POST => ThirdPartyVoter::CREATE,
                          Request::METHOD_PUT => ThirdPartyVoter::CREATE,
                          Request::METHOD_PATCH => ThirdPartyVoter::CREATE,
                      ],
                  ],
              ],
          ],
      ],
  ]);

  ```


