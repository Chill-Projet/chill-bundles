# Chill Conventions

In Progress

## Translations

Per bundle, all translations for Twig pages are located in a single file: translations/messages.fr.yaml.

## File Locations

    Controllers, form types, and Twig templates are placed in the root folders Controller, Form, and Resources/views, respectively.
    Admin pages are no longer placed in subfolders under Admin.

## Assets: Entrypoint Naming

There are three types of entry points:

* Vue application-specific (often for a single page): Prefixed with vue_.
  * Reusable JavaScript/CSS code:
      Examples include:
          * ckeditor
          * async_upload (used for forms)
          * bootstrap
          * chill.js
          * ...
        => We prefix with `mod_`

  * Page-specific CSS/JS:
      * Often reuses functionalities like ShowHide.
        => We prefix with `page_`.

## Folder Structure

````
# Under Resources/public:

    - chill/ => Contains the theme (Chill).
        - chillmain.scss: Compiled into the chill entrypoint.
    - lib/ => Libraries never used as entrypoints but reused elsewhere.
        - Examples: ShowHide, Collection, Select2.
    - module/: Ends up in reusable entrypoints (mod_).
        - bootstrap
            - custom.scss
            - custom/
                - variable.scss
                - ...
        - AsyncUpload.
    - vue/ => Vue applications only (vue_).
        - Examples: _components, app.
    - page/ => Page-specific assets (page_).
        - login
        - person
        - personvendee
        - household_edit_metadata
            - index.js
````

# Stylesheet Organization

1. The mod_bootstrap (module bootstrap) entry point is the first level. All parts (modules) of bootstrap are included in the bootstrap.js file located in ChillMainBundle/Resources/public/module/bootstrap.

    * At the beginning, this file imports the variables.scss file, which determines most of the bootstrap settings as customized. This file overrides the original, and many variables are adjusted for Chill.
        * Care must be taken to ensure this file can always be compared to the original bootstrap file. In the event of a bootstrap update, a diff must be generated and this diff applied to the variable file of the new version.
    * At the end, it imports the custom.scss file, which includes bootstrap adaptations to prepare it for our Chill theme.
        * This `custom.scss` file can be split into smaller files using `@import 'custom/...'`.
    * The idea is that this first bootstrap layer sets an important part of the application’s styles, particularly those related to layout positioning, responsive breakpoints, and the default margins and spacing applied to elements being manipulated.

2. The chill entry point is the second level. It contains the Chill theme, which is recognizable in the application.

    * Each bundle has a `Resources/public/chill` folder containing a main sass file, which can optionally be split using `@imports`. All these files are compiled into a single Chill entry point, which serves as the application’s theme and overrides bootstrap.
    * The chillmain.scss file should contain the most general style cascades, those applied to many areas of the application.
    * The chillperson.scss file also includes styles specific to different contexts related to people: person, household, and accompanying course.
    * Some smaller bundles contain only styles specific to their functionality.

3. The vue_ entry points are used for Vue components. Vue files can contain an scss style block. These styles are specific to the component and its inheritance, with the scoped tag precisely defining their scope (see the documentation).

4. The page_ entry points are used to add assets specific to certain pages, most often scripts and styles.

## HTML Tagging and Style Cascades

The following example shows how to tag a code element without overdoing it. Note that:

* It is not necessary to tag all inner classes.
* The parent class should not be repeated in all child classes. Sass cascading allows for flexible HTML structuring without overloading the tag hierarchy.
* Often, the first class will have variations created with additional classes that start in the same way: bloc-dark simply adds the dark version of bloc. We do not use bloc dark because we don’t want the dark class of bloc to interact with the same dark class of table. As a result, we will have an element bloc bloc-dark and another element table table-dark.

```html
<div class="bloc bloc-dark my-bloc">
  <h3>My Title</h3>
  <ul class="record_actions">
    <li>
      <a class="btn btn-edit"></a>
    </li>
  </ul>
</div>
```

Finally, it is important to define what a block, an action zone, and a button are. These three elements exist independently and are the only ones we tag.

For example, to style the title, we simply specify h3 within the block cascade.

```scss
div.bloc {
    // un bloc générique, utilisé à plusieurs endroits
    &.bloc-dark {
        // la version sombre du bloc
    }
    h3 {}
    ul {
        // une liste standard dans bloc
        li {
            // des items de liste standard dans bloc
        }
    }
}
div.mon-bloc {
    // des exceptions spécifiques à mon-bloc,
    // qui sont des adaptations de bloc
}

ul.record_actions {
    // va uniformiser tous les record_actions de l'application
    li {
        //...
    }
}

.btn {
    // les boutons de bootstrap
    .btn-edit {
        // chill étends les boutons bootstrap pour ses propres besoins
    }
}
</style>
```
## Render box

## URL

## Route Naming Conventions

:::warning
These rules have not always been followed in the past. They are desired for the future.
:::

Routes follow this structure:

`chill_(api|crud)_bundle_(api)_entity_action`

1. First, chill_ (for all Chill modules).
2. Then, crud or api, optional, automatically added if the route is generated by the configuration.
3. Then, a string indicating the bundle (`main`, `person`, `activity`, ...).
4. Then, api, if the route is an API route.
5. Then, a string indicating the entity the route targets, and possibly the sub-entities.
6. Then, an action (`list`, `view`, `edit`, `new`, ...).

Indicating `api` in the fourth position allows distinguishing API routes generated by the configuration (which are all prefixed with `chill_api`) from those generated manually. (For example: `chill_api_household__index` and `chill_person_api_household_members_move`).

If points 4 and 5 are missing, they are replaced by other elements to ensure the uniqueness of the route and its clear understanding.

#### HTML pages

:::warning
These rules have not always been followed in the past. They are desired for the future.
:::

Syntaxe:

```
/{_locale}/bundle/entity/{id}/action
/{_locale}/bundle/entity/sub-entity/{id}/action
```

The following elements should be included in the list:

1. The locale;
2. An identifier for the bundle;
3. The entity it relates to;
4. Any sub-entities that the URL refers to;
5. The action.

```
# list of activities for a person
/fr/activity/person/25/activity/list

# new activity
/fr/activity/activity/new?person_id=25

```

#### Pour les API

:::info
Automatically generated routes are prefixed with `chill_api`
:::

Syntaxe:

```
/api/1.0/bundle/entity/{id}/action
/api/1.0/bundle/entity/sub-entity/{id}/action
```

The following elements should be included in the list:

1. The string `/api/` followed by the version (e.g., 1.0);
2. An identifier for the bundle;
3. The entity it relates to;
4. Any sub-entities that the URL refers to;
5. The action.

These elements may be interspersed with the entity identifier. In this case, the identifier should be placed immediately after the entity it relates to.

#### URLS for admin pages
Same conventions as for other html pages, **but `admin` is added in second position**. Soit:

`/{_locale}/admin/bundle/entity/{id}/action`

## Database table naming convention

When creating a new entity and the corresponding database table, we follow the following naming convention for the database table:

`chill_{bundle_identifier}_{entity_name}`.
Eg. chill_person_spoken_languages

## UI Rules

### Page Titles
#### Every page must have a title (in the head tag and page header).

Each page contains a title in the <head> tag. This title is typically the same as the header title on the page.

Tip: It is possible to use the block function in Twig for this.

Example:

```htmlmixed=
{% block title "Titre de la page" %}

{% block content %}
<h1>
    {{ block('title')}}
</h1>
{% endblock %}
```

### entity_render usage

#### in Twig
Always use chill_entity_render_box for rendering entities like:
* User
* Person
* SocialAction
* SocialIssue
* Address
* Thirdparty
* ...

Example:

```
address|chill_entity_render_box
```
Justification:

1. Customization by installation: Some elements are sometimes customized during installation (for example, the name of each user will be followed by the name of the service).
2. To streamline and make displays consistent: Ensures uniformity in the way information is displayed across different pages or sections.
3. To simplify Twig code: By using blocks and centralizing title logic, it reduces repetition and makes the Twig code easier to maintain.

* Three rendering modes:
  * inline
  * block
  * item.

#### In Vue

There is always a renderbox equivalent in vue.

#### HTML Links to Sections

Always include links/icons for accessing records, such as person or household details, if the user has access.

### Form Guidelines

#### Vocabulary:
* `Create` in a `bt bt-create` for links to the form to create an entity (to access the form).
* `Save` in a `bt bt-save` for "Save" buttons (in either an edit or create form).
* `Save and new`
* `Save and view`
* `Edit` in a `bt bt-edit` for links to the edit form.
* `Duplicate` (specify where it can be seen).
* `Cancel` for leaving an edit page with a link to the list, or the `returnPath`.

#### After Saving:

Redirect to the returnPath if available; otherwise, redirect to the view page.

### Sticky Form Buttons:

Buttons like "Cancel" and "Save" must be within a "sticky-form" bar at the bottom of the form.

If relevant:

* The banner contains a "Cancel" button that returns to the previous page. It is mandatory for forms, but optional for lists or "summary" pages.
* This "Cancel" button is always on the left.

```
<ul class="record_actions sticky-form-buttons">
    <li class="cancel">
        <a href="{{ chill_entity_return_path('route_name' { 'route': 'option' } )}}">{{ return_path_label }}</a>
    </li>
    <li>
        <!-- action 1 -->
    </li>
</ul>
```

### Flash Messages

#### Display a flash message upon entity creation:

Everytime an entity has been created

> "The entity has been created."

The name of the element can be replaced with something more relevant:

> * The activity has been created
> * The appointment has been created
> * ...

#### On Saving an Entity

Each time an entity is saved, a flash message should appear:

> The data has been modified

#### Form Error (Validation Error)

At the top of the form, a flash message should indicate that validations have failed:

> This form contains errors

Errors should appear attached to the field they concern. However, it is acceptable to display errors at the root of the form if it is technically difficult to attach errors.

### Return Links

Each time a link is provided, check whether the function chill_return_path, chill_forward_return_path, or chill_return_path_or should be used.
* From the list page, to the opening of an element, or the creation button => use chill_path_add_return_path
* In these edit pages:
        * use chill_return_path_or in the "Cancel" button;
        * for the "Save and view" and "Save and close" buttons => ?

### Assets for Suggestion Lists

Create a list of suggestions to add (the entire item is clickable)

```html
<ul class="list-suggest add-items">
   <li>
      <span>item</span>
   </li>
</ul>
```

Create a list of suggestions to remove (with a clickable red cross, the anchor <a> is empty)

```html
<ul class="list-suggest remove-items">
    <li>
        <span>
            item
        </span>
    </li>
</ul>
```

Create a removable title (with a clickable red cross, the anchor <a> is empty)

```html
<div class="item-title">
    <span>title</span>
</div>
```

The classes `cols` or `inline` can be added alongside `list-suggest` to modify the layout of the list. In the last example, add a `removable` class to the `<span>` if you want to make the item removable.
