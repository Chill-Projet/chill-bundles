<?php
declare(strict_types=1);


namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\ReferrerFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerFilterTest extends AbstractFilterTest
{
    private string $property;

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $u) {
            $data[] = ['accepted_referrers' => $u, 'date_calc' => new RollingDate(RollingDate::T_TODAY)];
        }

        return $data;
    }

    public function getQueryBuilders(): iterable
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        yield $em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'acp')
            ->select('acp.id');

        $qb = $em->createQueryBuilder();
        $qb
            ->from(AccompanyingPeriod\AccompanyingPeriodWork::class, 'acpw')
            ->join('acpw.accompanyingPeriod', 'acp')
            ->join('acp.participations', 'acppart')
            ->join('acppart.person', 'person');

        $qb->select('COUNT(DISTINCT acpw.id) as export_result');

        yield $qb;
    }
}
-----
<?php
declare(strict_types=1);


namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\ReferrerFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerFilterTest extends AbstractFilterTest
{
    private string $property;

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $array = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();
        $data = [];
        foreach ($array as $u) {
            $data[] = ['accepted_referrers' => $u, 'date_calc' => new RollingDate(RollingDate::T_TODAY)];
        }
        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        yield $em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'acp')
            ->select('acp.id');
        $qb = $em->createQueryBuilder();
        $qb
            ->from(AccompanyingPeriod\AccompanyingPeriodWork::class, 'acpw')
            ->join('acpw.accompanyingPeriod', 'acp')
            ->join('acp.participations', 'acppart')
            ->join('acppart.person', 'person');
        $qb->select('COUNT(DISTINCT acpw.id) as export_result');
        yield $qb;
    }
}
