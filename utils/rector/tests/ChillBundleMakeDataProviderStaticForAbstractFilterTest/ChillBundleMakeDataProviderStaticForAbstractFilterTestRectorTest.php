<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Utils\Rector\Tests\ChillBundleMakeDataProviderStaticForAbstractFilterTest;

use Rector\Testing\PHPUnit\AbstractRectorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ChillBundleMakeDataProviderStaticForAbstractFilterTestRectorTest extends AbstractRectorTestCase
{
    /**
     * @dataProvider provideData
     */
    public function test(string $file): void
    {
        $this->doTestFile($file);
    }

    public static function provideData(): \Iterator
    {
        return self::yieldFilesFromDirectory(__DIR__.'/Fixture');
    }

    public function provideConfigFilePath(): string
    {
        return __DIR__.'/config/config.php';
    }
}
