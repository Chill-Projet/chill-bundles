<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Export\CountPerson;
use Chill\PersonBundle\Repository\PersonRepository;

/**
 * Test CountPerson export.
 *
 * @internal
 *
 * @coversNothing
 */
final class CountPersonTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $personRepository = self::getContainer()->get(PersonRepository::class);

        yield new CountPerson($personRepository, $this->getParameters(true));
        yield new CountPerson($personRepository, $this->getParameters(false));
    }

    public function getFormData()
    {
        return [
            [],
        ];
    }

    public function getModifiersCombination()
    {
        return [['person']];
    }
}
-----
<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Export\CountPerson;
use Chill\PersonBundle\Repository\PersonRepository;

/**
 * Test CountPerson export.
 *
 * @internal
 *
 * @coversNothing
 */
final class CountPersonTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $personRepository = self::getContainer()->get(PersonRepository::class);

        yield new CountPerson($personRepository, $this->getParameters(true));
        yield new CountPerson($personRepository, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [['person']];
    }
}
