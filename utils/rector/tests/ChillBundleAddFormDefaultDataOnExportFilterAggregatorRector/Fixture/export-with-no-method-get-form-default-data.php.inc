<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements AggregatorInterface
{
    public function getLabels($key, array $values, $data)
    {
        // TODO: Implement getLabels() method.
    }

    public function getQueryKeys($data)
    {
        // TODO: Implement getQueryKeys() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
-----
<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements AggregatorInterface
{
    public function getLabels($key, array $values, $data)
    {
        // TODO: Implement getLabels() method.
    }

    public function getQueryKeys($data)
    {
        // TODO: Implement getQueryKeys() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }
    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
