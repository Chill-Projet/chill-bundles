<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\DirectExportInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

class MyClass implements DirectExportInterface
{
    public function generate(array $acl, array $data = []): Response
    {
        // TODO: Implement generate() method.
    }

    public function getDescription(): string
    {
        // TODO: Implement getDescription() method.
    }

    public function requiredRole(): string
    {
        // TODO: Implement requiredRole() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // TODO: Implement buildForm() method.
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }
}
?>
-----
<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\DirectExportInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

class MyClass implements DirectExportInterface
{
    public function generate(array $acl, array $data = []): Response
    {
        // TODO: Implement generate() method.
    }

    public function getDescription(): string
    {
        // TODO: Implement getDescription() method.
    }

    public function requiredRole(): string
    {
        // TODO: Implement requiredRole() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // TODO: Implement buildForm() method.
    }
    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }
}
?>
