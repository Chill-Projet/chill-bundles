<?php

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ClosingDateAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ClosingDateAggregatorTest extends AbstractAggregatorTest
{
    private static ClosingDateAggregator $closingDateAggregator;

    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::bootKernel();

        self::$closingDateAggregator = self::getContainer()->get(ClosingDateAggregator::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function getAggregator()
    {
        return self::$closingDateAggregator;
    }

    public function getFormData()
    {
        yield ['frequency' => 'YYYY'];
        yield ['frequency' => 'YYYY-MM'];
        yield ['frequency' => 'YYYY-IV'];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $data = [
            self::$entityManager->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];

        self::ensureKernelShutdown();

        return $data;
    }
}
-----
<?php

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ClosingDateAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ClosingDateAggregatorTest extends AbstractAggregatorTest
{
    private static ClosingDateAggregator $closingDateAggregator;

    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::bootKernel();

        self::$closingDateAggregator = self::getContainer()->get(ClosingDateAggregator::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function getAggregator()
    {
        return self::$closingDateAggregator;
    }

    public static function getFormData(): array
    {
        yield ['frequency' => 'YYYY'];
        yield ['frequency' => 'YYYY-MM'];
        yield ['frequency' => 'YYYY-IV'];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $data = [
            self::$entityManager->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
        self::ensureKernelShutdown();
        return $data;
    }
}
