<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return static function (Rector\Config\RectorConfig $rectorConfig): void {
    $rectorConfig->rule(Chill\Utils\Rector\Rector\ChillBundleMakeDataProviderStaticForAbstractAggregatorTestRector::class);
};
