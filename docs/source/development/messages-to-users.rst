.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Messages to users, flashbags and buttons
****************************************


.. _flashbags :

Flashbags
==========

The four following levels are defined : 

+-----------+----------------------------------------------------------------------------------------------+
|Key        |Intent                                                                                        |
+===========+==============================================================================================+
|alert      |A message not linked with the user action, but which should require an action or a            |
|           |correction.                                                                                   |
+-----------+----------------------------------------------------------------------------------------------+
|success    |The user action succeeds.                                                                     |
+-----------+----------------------------------------------------------------------------------------------+
|notice     |A simple message to give information to the user. The message may be linked or not linked with|
|           |the user action.                                                                              |
+-----------+----------------------------------------------------------------------------------------------+
|warning    |A message linked with an action, the user should correct.                                     |
+-----------+----------------------------------------------------------------------------------------------+
|error      |The user's action failed: he must correct something to process the action.                    |
+-----------+----------------------------------------------------------------------------------------------+

.. seealso::

   `Flash Messages on Symfony documentation <http://symfony.com/doc/current/book/controller.html#flash-messages>`_
      Learn how to use flash messages in controller.


Buttons
========

Some actions are available to decorate ``a`` links and ``buttons``.

To add the action on button, use them as class along with ``sc-button`` :

.. code-block:: html

   <a class="sc-button bt-create">Create an entity</a>

   <button class="sc-button bt-submit" type="submit">Submit</button>

+-----------+----------------+------------------------------------------------------------------------------+
| Action    |  Class         | Description                                                                  |
+===========+================+==============================================================================+
| Submit    | ``bt-submit``  | Submit a form. Use only if action is not "save".                             |
+-----------+----------------+------------------------------------------------------------------------------+
| Create    | ``bt-create``  | - Link to a form to create an entity (alias: ``bt-new``)                     |
|           | or ``bt-new``  | - Submitting this form will create a new entity                              |
+-----------+----------------+------------------------------------------------------------------------------+
| Reset     | ``bt-reset``   | Reset a form                                                                 |
+-----------+----------------+------------------------------------------------------------------------------+
| Delete    | ``bt-delete``  | - Link to a form to delete an entity                                         |
|           |                | - Submitting this form will remove the entity                                |
+-----------+----------------+------------------------------------------------------------------------------+
| Edit      | ``bt-edit`` or | Link to a form to edit an entity                                             |
|           | ``bt-update``  |                                                                              | 
+-----------+----------------+------------------------------------------------------------------------------+
| Save      | ``bt-save``    | Submitting this form will save change on the entity                          |
+-----------+----------------+------------------------------------------------------------------------------+
| Action    | ``bt-action``  | Generic link to an action                                                    |
+-----------+----------------+------------------------------------------------------------------------------+
| Cancel    | ``bt-cancel``  | Cancel an action and go back to another page                                 |
+-----------+----------------+------------------------------------------------------------------------------+

Styling buttons
---------------

Small buttons, mainly to use inline

.. code-block:: html

   <p><a class="sc-button bt-create bt-small">You button</a></p>

You can omit content and show a button with an icon only :

.. code-block:: html

   <a class="sc-button bt-create"></a>

You can hide content and show it only on hover

.. code-block:: html

   <a class="sc-button bt-create has-hidden"><span class="show-on-hover">Showed when mouse pass on</span></a>

You can customize the icon :

.. code-block:: html

  <a class="sc-button bt-create change-icon"><i class="fa fa-icon"></i>Button with custom icon</a>

Grouping buttons
----------------

Grouping buttons can be done using ``ul.record_actions`` element (an ``ul`` list with class ``record_actions``):

.. code-block:: html

   <ul class="record_actions">

      <li class="cancel">
         <a class="sc-button bt-cancel">Cancel</a>
      <li>

      <li>
         <a class="sc-button bt-save">Save</a>
      </li>

   </ul>

The element with the ``cancel`` class will be set in first position.

Inside table, the space between elements will be shorter.

You can add the class ``record_actions_small`` if you want shorter space between elements.

