Code style, code quality and other tools
########################################

PHP-cs-fixer
============

For development, you will also have to install:

- `php-cs-fixer <https://cs.symfony.com/>`_

We also encourage you to use tools like `phpstan <https://phpstan.org>`_ and `rector <https://getrector.com>`_.

For running php-cs-fixer:

.. code-block:: bash

   symfony composer php-cs-fixer

Execute tests
=============

.. code-block:: bash

   symfony composer exec phpunit -- /path/to_your_test.php

Note that IDE like PhpStorm should be able to run tests, even KernelTestcase or WebTestCase, `from within their interfaces <https://www.jetbrains.com/help/phpstorm/using-phpunit-framework.html#run_phpunit_tests>`_.

Execute rector
==============

.. code-block:: bash

   symfony composer exec rector -- process

