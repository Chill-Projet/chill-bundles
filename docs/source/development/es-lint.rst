ESLint
======

To improve the quality of our JS and VueJS code, ESLint and eslint-plugin-vue are implemented within the chill-bundles project.

Commands
--------

To run ESLint, you can simply use the ``eslint`` command within the chill-bundles directory.
This runs eslint **not** taking the baseline into account, thus showing all existing errors in the project.

A script was also added to package.json allowing you to execute ``yarn run eslint``.
This will run eslint, but **taking the baseline into account**, thus only alerting to newly created errors.

The eslint command is configured to also run ``prettier`` which will simply format the code to look more uniform (takes care indentation for example).

Interesting options that can be used in combination with eslint are:

- ``--quiet`` to only get errors and silence the warnings
- ``--fix`` to have ESLint fix what it can, automatically. This will not fix everything.

Baseline
--------

To allow us the time to fix linting errors/warnings a baseline has been created using the following command
- ``npx eslint-baseline "**/*.{js,vue}"``

The baseline has been commited and the gitlab CI setup to only fail upon new errors/warnings being created.
When fixing errors/warnings manually, please update the baseline.

1. Delete the current baseline file
2. Run the above command locally, this will automatically create a new baseline that should be commited

Rules
-----

We use Vue 3, so the rules can be configured as follows within the ``eslint.config.mjs`` file:

- ``.configs["flat/base"]`` ... Settings and rules to enable correct ESLint parsing.

Configurations for using Vue.js 3.x:

- ``.configs["flat/essential"]`` : Base rules plus rules to prevent errors or unintended behavior.
- ``.configs["flat/strongly-recommended"]`` ... Above, plus rules to considerably improve code readability and/or dev experience.
- ``.configs["flat/recommended"]`` ... Above, plus rules to enforce subjective community defaults to ensure consistency.

Detailed information about which rules each set includes can be found here:
`https://eslint.vuejs.org/rules/ <https://eslint.vuejs.org/rules/>`_

Manual Rule Configuration
-------------------------

We can also manually configure certain rules or override rules that are part of the ruleset specified above.

For example, if we want to turn off a certain rule, we can do so as follows:

.. code-block:: javascript

    rules: {
      'vue/multi-word-component': 'off'
    }

We could also change the severity of a certain rule from 'error' to 'warning', for example.

Within specific ``.js`` or ``.vue`` files, we can also override a certain rule only for that specific file by adding a comment:

.. code-block:: javascript

    /* eslint multi-word-component: "off", no-child-content: "error"
    --------
    Here's a description about why this configuration is necessary. */
