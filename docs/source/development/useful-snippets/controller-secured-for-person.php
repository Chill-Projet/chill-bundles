<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\HealthBundle\Controller;

use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Role\Role;

class ConsultationController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    /**
     * @param int $id personId
     *
     * @throws type
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($id)
    {
        /** @var \Chill\PersonBundle\Entity\Person $person */
        $person = $this->get('chill.person.repository.person')
            ->find($id);

        if (null === $person) {
            throw $this->createNotFoundException('The person is not found');
        }

        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);

        /** @var \Chill\MainBundle\Security\Authorization\AuthorizationHelper $authorizationHelper */
        $authorizationHelper = $this->get('chill.main.security.'
            . 'authorization.helper');

        $circles = $authorizationHelper->getReachableCircles(
            $this->getUser(),
            ConsultationVoter::SEE,
            $person->getCenter()
        );

        // create a query which take circles into account
        $consultations = $this->managerRegistry->getManager()
            ->createQuery('SELECT c FROM ChillHealthBundle:Consultation c '
                . 'WHERE c.patient = :person AND c.circle IN(:circles) '
                . 'ORDER BY c.date DESC')
            ->setParameter('person', $person)
            ->setParameter('circles', $circles)
            ->getResult();

        return $this->render('ChillHealthBundle:Consultation:list.html.twig', [
            'person' => $person,
            'consultations' => $consultations,
        ]);
    }
}
