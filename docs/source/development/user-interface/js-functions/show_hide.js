import { ShowHide } from "ShowHide/show_hide.js";

var div_accompagnement = document.getElementById("form_accompagnement"),
  div_accompagnement_comment = document.getElementById(
    "form_accompagnement_comment",
  ),
  div_caf_id = document.getElementById("cafId"),
  div_caf_inscription_date = document.getElementById("cafInscriptionDate");
// let show/hide the div_accompagnement_comment if the input with value `'autre'` is checked
new ShowHide({
  froms: [div_accompagnement],
  test: function (froms, event) {
    for (let el of froms.values()) {
      for (let input of el.querySelectorAll("input").values()) {
        if (input.value === "autre") {
          return input.checked;
        }
      }
    }

    return false;
  },
  container: [div_accompagnement_comment],
});

// let show the date input only if the the id is filled
new ShowHide({
  froms: [div_caf_id],
  test: function (froms, event) {
    for (let el of froms.values()) {
      return el.querySelector("input").value !== "";
    }
  },
  container: [div_caf_inscription_date],
  // using this option, we use the event `input` instead of `change`
  event_name: "input",
});
