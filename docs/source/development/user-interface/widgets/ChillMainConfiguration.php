<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

use Chill\MainBundle\DependencyInjection\Widget\AddWidgetConfigurationTrait;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Configure the main bundle.
 */
class ChillMainConfiguration implements ConfigurationInterface
{
    use AddWidgetConfigurationTrait;

    public function __construct(
        array $widgetFactories,
        private readonly ContainerBuilder $containerBuilder
    ) {
        // we register here widget factories (see below)
        $this->setWidgetFactories($widgetFactories);
    }

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_main');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()

                // ...
            ->arrayNode('widgets')
            ->canBeDisabled()
            ->children()
                         // we declare here all configuration for homepage place
            ->append($this->addWidgetsConfiguration('homepage', $this->containerBuilder))
            ->end() // end of widgets/children
            ->end() // end of widgets
            ->end() // end of root/children
            ->end() // end of root
        ;

        return $treeBuilder;
    }
}
