<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\Form\FormBuilderInterface;

class CountPerson implements ExportInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->entityManager = $em;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // this export does not add any form
    }
    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription()
    {
        return 'Count peoples by various parameters.';
    }

    public function getLabels($key, array $values, $data)
    {
        // the Closure which will be executed by the formatter.
        return fn($value) => match ($value) {
            '_header' => $this->getTitle(),
            default => $value,
        };
    }

    public function getQueryKeys($data)
    {
        // this array match the result keys in the query. We have only
        // one column.
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle()
    {
        return 'Count peoples';
    }

    public function getType()
    {
        return Declarations::PERSON_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        // we gather all center the user choose.
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('COUNT(person.id) AS export_result')
            ->from('ChillPersonBundle:Person', 'person')
            ->join('person.center', 'center')
            ->andWhere('center IN (:authorized_centers)')
            ->setParameter('authorized_centers', $centers);

        return $qb;
    }

    public function requiredRole(): string
    {
        return PersonVoter::STATS;
    }

    public function supportsModifiers()
    {
        // explain the export manager which formatters and filters are allowed
        return [Declarations::PERSON_TYPE, Declarations::PERSON_IMPLIED_IN];
    }
}
