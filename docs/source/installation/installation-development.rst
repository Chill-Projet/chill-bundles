.. _installation-for-dev:

Installation for development or testing purpose only
====================================================

⚠️ Use this method for development only. ⚠️

You will need:

- `Composer <https://getcomposer.org>`__;
- `Symfony-cli tool <https://symfony.com/download>`__;
- `docker <https://docs.docker.com/engine/install/>`__ and
  `docker-compose <https://docs.docker.com/compose/>`__
- node > 20 and yarn 1.22

First initialization
--------------------

1. clone the repository and move to the cloned directory:

.. code:: bash

   git clone https://gitlab.com/Chill-Projet/chill-bundles.git
   cd chill-bundles

2. install dependencies using composer

.. code:: bash

   composer install

3. Install and compile assets:

.. code:: bash

   yarn install
   yarn run encore production

**note** double check that you have the node version > 20 using the
``node --version`` command.

4. configure your project: create a ``.env.local`` file at the root, and
   add the admin password:

.. code:: dotenv

   # for this installation mode, the environment should always be "dev"
   APP_ENV=dev
   ADMIN_PASSWORD=\$2y\$13\$iyvJLuT4YEa6iWXyQV4/N.hNHpNG8kXlYDkkt5MkYy4FXcSwYAwmm
   # note: if you copy-paste the line above, the password will be "admin".

5. start the stack using ``docker compose up -d``, check the status of
   the start with ``docker compose ps``

6. configure all the needed third-party tools

   .. code:: bash

      # the first start, it may last some seconds, you can check with docker compose ps
      # run migrations
      symfony console doctrine:migrations:migrate
      # setup messenger
      symfony console messenger:setup-transports
      # prepare some views
      symfony console chill:db:sync-views
      # generate jwt token, required for some api features (webdav access, ...)
      symfony console lexik:jwt:generate-keypair

7. add some fixtures

This will truncate all the existing data of the database. But remember,
we are in dev mode !

.. code:: bash

   symfony console doctrine:fixtures:load

8. launch symfony dev-server

.. code:: bash

   symfony server:start -d

And visit the web page it suggest. You can login with user
``center a_social`` and password ``password``, or login ``admin`` with
the password you set.

Stopping the server
-------------------

.. code:: bash

   symfony server:stop

Restart the webserver for subsequent start
------------------------------------------

.. code:: bash

   symfony server:start -d
   # this will automatically starts the full docker compose services
