
Send short messages (SMS) with calendar bundle
==============================================

To activate the sending of messages, you should run this command on a regularly basis (using, for instance, a cronjob):

.. code-block:: bash

   bin/console chill:calendar:send-short-messages

A transporter must be configured for the message to be effectively sent. 

Configure OVH Transporter
-------------------------

Currently, this is the only one transporter available.

For configuring this, simply add this config variable in your environment:

```env
SHORT_MESSAGE_DSN=ovh://applicationKey:applicationSecret@endpoint?consumerKey=xxxx&sender=yyyy&service_name=zzzz
```

In order to generate the application key, secret, and consumerKey, refers to their `documentation <https://docs.ovh.com/gb/en/api/first-steps-with-ovh-api/>`_.

Before to be able to send your first sms, you must enable your account, grab some credits, and configure a sender. The service_name is an internal configuration generated by OVH.

