Document storage
################

You can store document on two different ways:

- on disk
- in the cloud, using object storage: currently only `openstack swift <https://docs.openstack.org/api-ref/object-store/index.html>`_ is supported.

Comparison
==========

Storing documents within the cloud is particularily suitable for "portable" deployments, like in kubernetes, or within container
without having to manage volumes to store documents. But you'll have to subscribe on a commercial offer.

Storing documents on disk is more easy to configure, but more difficult to manage: if you use container, you will have to
manager volumes to attach documents on disk. You'll have to do some backup of the directory. If chill is load-balanced (and
multiple instances of chill are run), you will have to find a way to share the directories in read-write mode for every instance.

On Disk
=======

Configure Chill like this:

.. code-block:: yaml

   # file config/packages/chill_doc_store.yaml
   chill_doc_store:
       use_driver: local_storage
       local_storage:
           storage_path: '%kernel.project_dir%/var/storage'

In this configuration, documents will be stored in :code:`var/storage` within your app directory. But this path can be
elsewhere on the disk. Be aware that the directory must be writable by the user executing the chill app (php-fpm or www-data).

Documents will be stored in subpathes within that directory. The files will be encrypted, the key is stored in the database.

In the cloud, using openstack object store
##########################################

You must subscribe to a commercial offer for object store.

Chill use some features to allow documents to be stored in the cloud without being uploaded first to the chill server:

- `Form POST Middelware <https://docs.openstack.org/swift/latest/api/form_post_middleware.html>`_;
- `Temporary URL Middelware <https://docs.openstack.org/swift/latest/api/temporary_url_middleware.html>`_.

A secret key must be generated and configured, and CORS must be configured depending on the domain you will use to serve Chill.

At first, create a container and get the base path to the container. For instance, on OVH, if you create a container named "mychill",
you will be able to retrieve the base path of the container within the OVH interface, like this:

- base_path: :code:`https://storage.gra.cloud.ovh.net/v1/AUTH_123456789/mychill/` => will be variable :code:`ASYNC_UPLOAD_TEMP_URL_BASE_PATH`
- container: :code:`mychill` => will be variable :code:`ASYNC_UPLOAD_TEMP_URL_CONTAINER`

You can also generate a key, which should have at least 20 characters. This key will go in the variable :code:`ASYNC_UPLOAD_TEMP_URL_KEY`.

.. note::

   See the `documentation of symfony <https://symfony.com/doc/current/configuration.html#config-env-vars>`_ on how to store variables, and how to encrypt them if needed.

Configure the storage like this:

.. code-block:: yaml

   # file config/packages/chill_doc_store.yaml
   chill_doc_store:
       use_driver: openstack
       openstack:
           temp_url:
               temp_url_key:         '%env(resolve:ASYNC_UPLOAD_TEMP_URL_KEY)%'       # Required
               container:            '%env(resolve:ASYNC_UPLOAD_TEMP_URL_CONTAINER)%' # Required
               temp_url_base_path:   '%env(resolve:ASYNC_UPLOAD_TEMP_URL_BASE_PATH)%' # Required

Chill is able to configure the container in order to store document. Grab an Openstack Token (for instance, using :code:`openstack token issue` or
the web interface of your openstack provider), and run this command:

.. code-block:: bash

   symfony console async-upload:configure --os_token=OPENSTACK_TOKEN -d https://mychill.mydomain.example

   # or, without symfony-cli
   bin/console async-upload:configure --os_token=OPENSTACK_TOKEN -d https://mychill.mydomain.example


