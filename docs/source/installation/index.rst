.. chill-doc documentation master file, created by
   sphinx-quickstart on Sun Sep 28 22:04:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (C)  2014-2019 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Installation & Usage
####################


You will learn here how to install a new symfony project with chill, and configure it.

Which can of installation do I need ?
=====================================

I want to run chill in production
---------------------------------

See the :ref:`instructions about installing Chill for production <installation-production>`.

I want to add features to the main chill bundles
------------------------------------------------

If you want to add features to chill bundles itself, **and** you want those features to be merged into the chill bundles,
you can use the "development" installation mode.

See the :ref:`instruction for installation for development <installation-for-dev>`.

I want to add features to Chill, but keep those features for my instance
-------------------------------------------------------------------------

Follow the same instruction than for production, until the end.

Requirements
============

The installation is tested on a Debian-like linux distribution. The installation on other operating systems is not documented.

You have to install the following tools on your computer:

- `PHP <https://www.php.net/>`_, version 8.3+, with the following extensions: pdo_pgsql, intl, mbstring, zip, bcmath, exif, sockets, redis, ast, gd;
- `composer <https://getcomposer.org/>`_;
- `symfony cli <https://symfony.com/download>`_;
- `node, we encourage you to use nvm to configure the correct version <https://github.com/nvm-sh/nvm>`_. The project contains an
  :code:`.nvmrc` file which selects automatically the required version of node (if present).
- `yarn <https://classic.yarnpkg.com/lang/en/docs/install/>`_. We use the version 1.22+ for now.
- `docker and the plugin compose <https://docker.com>`_ to run the database

Chill needs a redis server and a postgresql database, and a few other things like a "relatorio service" which will
generate documents from templates. **All these things are available through docker using the plugin compose**. We do not provide
information on how to run this without docker compose.

Instructions
============

.. toctree::
   :maxdepth: 2

   installation-development.rst
   installation-production.rst
