.. chill-doc documentation master file, created by
   sphinx-quickstart on Sun Sep 28 22:04:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Welcome to Chill documentation!
=====================================

Chill is a free software for social workers.

Chill rely on the php framework `Symfony <http://symfony.com>`_.

Contents of this documentation:

.. toctree::
   :maxdepth: 2

   installation/index.rst
   development/index.rst
   Bundles <bundles/index.rst>

Let's talk together !
======================

You may talk to developers using the matrix room: `https://app.element.io/#/room/#chill-social-admin:matrix.org`_

Contribute
==========


* `Issue tracker <https://gitlab.com/groups/Chill-project/issues>`_ You may want to dispatch the issue in the multiple projects. If you do not know in which project is located your bug / feature request, use the project Chill-Main.


User manual
===========

An user manual exists in French and currently focuses on describing the main concept of the software.

`Read (and contribute) to the manual <https://fr.wikibooks.org/wiki/Chill>`_

Available bundles
=================

* Chill-app | https://gitlab.com/Chill-project/Chill-app This is the skeleton of the project. It does contains only few code, but information about configuration of your instance ;
* Chill-bundle: contains the main bundles, the most used in an instance. This means:
  * chill-main, the main framework,
  * Chill Person, to deal with persons,
  * chill custom fields, to add custom fields to some entities,
  * chill activity: to add activities to people,
  * chill report: to add report to people,
  * chill event: to gather people into events,
  * chill docs store: to store documents to people, but also entities,
  * chill task: to register task with people,
  * chill third party: to register third parties,

You will also found the following projects :

* The website https://chill.social : https://gitlab.com/Chill-project/chill.social

And various project to build docker containers with Chill.

TODO in documentation
=====================

.. todolist::

Licence
========

The project is available under the `GNU AFFERO GENERAL PUBLIC LICENSE v3`_.

This documentation is published under the `GNU Free Documentation License (FDL) v1.3`_


.. _GNU AFFERO GENERAL PUBLIC LICENSE v3: http://www.gnu.org/licenses/agpl-3.0.html
.. _GNU Free Documentation License (FDL) v1.3: http://www.gnu.org/licenses/fdl-1.3.html

