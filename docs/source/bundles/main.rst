.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _main-bundle:
 
Main bundle
###########

This bundle is **required** for running Chill.

This bundle provide :

* Access control model (users, groups, and all concepts)
* ...


.. warning::

   this section is incomplete.

.. _main-bundle-macros:

Macros
******

Address sticker
===============

Macro file
   `ChillMainBundle:Address:macro.html.twig`
Macro name
   :code:`_render`
Macro envelope
   :code:`address`, instance of :class:`Chill\MainBundle\Entity\Address`

When to use this macro ?
   When you want to represent an address.
Example usage :
   .. code-block:: html+jinja

      {% import 'ChillMainBundle:Address:macro.html.twig' as m %}

      {{ m._render(address) }}

