# Main Chill Bundles and Chill framework

Chill is a software for social workers. It allows them to keep track of the social work they do.

See our website for more information https://www.chill.social

## Installation

Chill-bundles is a set of bundles that should be used within a Symfony application.

A symfony application will help you to customize all the configuration options, change the behaviour of some parts of the usual-way that chill works, … without to have to fork this repository !

See [the instructions in the docs](./docs/source/installation/index.rst).

Those instructions are also built [online](https://docs.chill.social).
