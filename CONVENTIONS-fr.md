# Conventions Chill

en cours de rédaction

## Translations

Par bundle, toutes les traductions des pages twig se trouvent dans un seul fichier `translations/messages.fr.yaml`.

## Emplacement des fichiers

Les controllers, form type & templates twig sont placés à la racine des dossiers `Controller`, `Form` & `Ressources/views`, respectivement. Pour les pages Admin, on ne les mets plus dans des sous-dossiers Admin.
## Assets: nommage des entrypoints

Trois types d'entrypoint:

* application vue (souvent spécifique à une page) -> préfixé par `vue_`;
* code js/css qui est réutilisé à plusieurs endroits:
    * ckeditor
    * async_upload (utilisé pour un formulaire)
    * bootstrap
    * chill.js
    * ...

   => on préfixe `mod_`
* code css ou js pour une seule page
    * ré-utilise parfois des "foncitionnalités": ShowHide, ...
   => on préfixe `page_`


Arborescence:

```
# Sous Resources/public

- chill/ => theme (chill)
    - chillmain.scss  -> push dans l'entrypoint chill
- lib/ => ne vont jamais dans un entrypoint, mais sont ré-utilisés par d'autres
    - ShowHide
    - Collection
    - Select2
- module/ => termine dans des entrypoints ré-utilisables (mod_)
    - bootstrap
        - custom.scss
        - custom/
            - variables.scss
            - ..
    - forkawesome
    - AsyncUpload
- vue/ => uniquement application vue (vue_)
    - _components
    - app
- page/ => uniquement pour une seule page (page_)
    - login
    - person
    - personvendee
    - household_edit_metadata
        - index.js
```

## Organisation des feuilles de styles

Comment s'échaffaudent les styles dans Chill ?


1. l'entrypoint **mod_bootstrap** (module bootstrap) est le premier niveau. Toutes les parties(modules) de bootstrap sont convoquées dans le fichier ```bootstrap.js``` situé dans ```ChillMainBundle/Resources/public/module/bootstrap```.
    * Au début, ce fichier importe le fichier ```variables.scss``` qui détermine la plupart des réglages bootstrap tels qu'on les a personnalisés. Ce fichier surcharge l'original, et de nombreuses variables y sont adaptées pour Chill.
        * On veillera à ce qu'on puisse toujours comparer ce fichier à l'original de bootstrap. En cas de mise à jour de bootstrap, il faudra générer un diff, et adapter ce diff sur le fichier variable de la nouvelle version.
    * A la fin on importe le fichier ```custom.scss```, qui comprends des adaptations de bootstrap pour le préparer à notre thème Chill.
        * ce ```custom.scss``` peut être splitté en plus petits fichiers avec des ```@import 'custom/...'```
    * L'idée est que cette première couche bootstrap règle un partie importante des styles de l'application, en particulier ce qui touche aux position du layout, aux points de bascules responsive, aux marges et écarts appliqués par défauts aux éléments qu'on manipule.

2. l'entrypoint **chill** est le second niveau. Il contient le thème Chill qui est reconnaissable à l'application.
    * Chaque bundle a un dossier ```Resources/public/chill``` dans lequel on peut trouver une feuille sass principale, qui est éventuellement splittée avec des ```@imports```. Toutes ces feuilles sont compilées dans un unique entrypoint Chill, c'est le thème de l'application. Celui-ci surcharge bootstrap.
    * La feuille chillmain.scss devrait contenir les cascades de styles les plus générales, celles qui sont appliquées à de nombreux endroits de l'application.
    * La feuille chillperson.scss va aussi retrouver des styles propres aux différents contextes des personnes: person, household et accompanyingcourse.
    * Certains bundles plus secondaires ne contiennent que des styles spécifiques à leur fonctionnement.

3. les entrypoints **vue_** sont utilisés pour des composants vue. Les fichiers vue peuvent contenir un bloc de styles scss. Ce sont des styles qui ne concernent que le composant et son héritage, le tag ```scoped``` précise justement sa portée (voir la doc).

4. les entrypoints **page_** sont utilisés pour ajouter des assets spécifiques à certaines pages, le plus souvent des scripts et des styles.


## Taguer du code html et construire la cascade de styles

L'exemple suivant montre comment taguer sans excès un élément de code. On remarque que:
* il n'est pas nécessaire de taguer toutes les classes intérieures,
* il ne faut pas répéter la classe parent dans toutes les classes enfants. La cascade sass va permettre de saisir le html avec souplesse sans alourdir la structure des balises.
* souvent la première classe sera déclinée par plusieurs classes qui commencent de la même manière: ```bloc-dark``` ajoute juste la version sombre de ```bloc```, on ne met pas ```bloc dark```, car on ne souhaite pas que la classe ```dark``` de ```bloc``` interagisse avec la même classe ```dark``` de ```table```. On aura donc un élément ```bloc bloc-dark``` et un élément ```table table-dark```.

```html
<div class="bloc bloc-dark mon-bloc">
 <h3>mon titre</h3>
 <ul class="record_actions">
    <li>
        <a class="btn btn-edit"></a>
    </li>
    <li></li>
    <li></li>
 </ul>
</div>
```

Finalement, il importe ici de définir ce qu'est un bloc, ce qu'est une zone d'actions et ce qu'est un bouton. Ces 3 éléments existent de manière autonome, ce sont les seuls qu'on tagge.

Par exemple pour mettre un style au titre on précise juste h3 dans la cascade bloc.

```scss
div.bloc {
   // un bloc générique, utilisé à plusieurs endroits
   &.bloc-dark {
      // la version sombre du bloc
   }
   h3 {}
   ul {
      // une liste standard dans bloc
      li {
         // des items de liste standard dans bloc
      }
   }
}
div.mon-bloc {
   // des exceptions spécifiques à mon-bloc,
   // qui sont des adaptations de bloc
}

ul.record_actions {
   // va uniformiser tous les record_actions de l'application
   li {
     //...
   }
}

.btn {
  // les boutons de bootstrap
  .btn-edit {
     // chill étends les boutons bootstrap pour ses propres besoins
  }
}
</style>
```

## Render box

## URL

### Nommage des routes

:::warning
Ces règles n'ont pas toujours été utilisées par le passé. Elles sont souhaitées pour le futur.
:::

Les routes sont nommées de cette manière:

`chill_(api|crud)_bundle_(api)_entite_action`

1. d'abord chill_ (pour tous les modules chill)
2. ensuite `crud` ou `api`, optionnel, automatiquement ajouté si la route est générée par la configuration
3. ensuite une string qui indique le bundle (`main`, `person`, `activity`, ...)
4. ensuite, `api`, si la route est une route d'api.
5. ensuite une string qui indique sur quelle entité porte la route, voire également les sous-entités
6. ensuite une action (`list`, `view`, `edit`, `new`, ...)

Le fait d'indiquer `api` en quatrième position permet de distinguer les routes d'api qui sont générées par la configuration (qui sont toutes préfixées par `chill_api`, de celles générées manuellement. (Exemple: `chill_api_household__index`, et `chill_person_api_household_members_move`)

Si les points 4 et 5 sont inexistants, alors ils sont remplacés par d'autres éléments de manière à garantir l'unicité de la route, et sa bonne compréhension.

### Nommage des URL

Les URL respectent également une convention:

#### Pour les pages html

:::warning
Ces règles n'ont pas toujours été utilisées par le passé. Elles sont souhaitées pour le futur.
:::

Syntaxe:

```
/{_locale}/bundle/entity/{id}/action
/{_locale}/bundle/entity/sub-entity/{id}/action
```

Les éléments suivants devraient se trouver dans la liste:

1. la locale;
2. un identifiant du bundle
3. l'entité auquel il se rapporte
4. les éventuelles sous-entités auxquelles l'url se rapport
5. l'action

Ces éléments peuvent être entrecoupés de l'identifiant d'une entité. Dans ce cas, cet identifiant se place juste après l'entité auquel il se rapporte.

Exemple:

```
# liste des échanges pour une personne
/fr/activity/person/25/activity/list

# nouvelle activité
/fr/activity/activity/new?person_id=25

```

#### Pour les API

:::info
Les routes générées automatiquement sont préfixées par chill_api
:::

Syntaxe:

```
/api/1.0/bundle/entity/{id}/action
/api/1.0/bundle/entity/sub-entity/{id}/action
```

Les éléments suivants devraient se trouver dans la liste:

1. la string `/api/` et puis la version (1.0)
2. un identifiant du bundle
3. l'entité auquel il se rapporte
4. les éventuelles sous-entités auxquelles l'url se rapport
5. l'action

Ces éléments peuvent être entrecoupés de l'identifiant d'une entité. Dans ce cas, cet identifiant se place juste après l'entité auquel il se rapporte.

#### Pour les URL de l'espace Admin

Même conventions que dans les autres pages html de l'application, **mais `admin` est ajouté en deuxième position**. Soit:


`/{_locale}/admin/bundle/entity/{id}/action`

### Nommage des tables de base de donnée

Lors de la création d'une nouvelle entité et de la table de base de données correspondante, nous suivons la convention d'appellation suivante pour la table de base de données :

`chill_{bundle_identifier}_{nom_de_l'entité}`.

Par exemple : chill_person_spoken_languages

## Règles UI chill

### Titre des pages

#### Chaque page contient un titre

Chaque page contient un titre dans la balise head. Ce titre est normalement identique à celui de l'entête de la page.

Astuce: il est possible d'utiliser la fonction `block` de twig pour cela:

```htmlmixed=
{% block title "Titre de la page" %}

{% block content %}
<h1>
    {{ block('title')}}
</h1>
{% endblock %}
```


### Utilisation des `entity_render`

#### En twig

Les templates twig doivent toujours utiliser la fonction chill_entity_render_box pour effectuer le rendu des éléments suivants:

* User
* Person
* SocialIssue
* SocialAction
* Address
* ThirdParty
* ...

Exemple:

```
address|chill_entity_render_box
```

Justification:

* des éléments sont parfois personnalisés par installation (par exemple, le nom de chaque utilisateur sera suivi par le nom du service)
* pour rationaliser et rendre semblable les affichages
* pour simplifier le code twig

A prevoir:

* toujours trois positions:
    * inline
    * block
    * item (dans un tableau, une ligne)

> block et item sont en fait la même option passée au render_box: render: bloc. Il y a aussi ‘raw’ pour le inline, et ‘label’ pour une titraille configurable avec des options.

> quand on passe l’option render: bloc, on peut placer le render_box dans une boucle for plus large qui fonctionne avec la classe flex-table ou la classe flex-bloc, ce qui donnera un affichage en rangée (table) ou en blocs. [name=Mathieu]

#### En vue

Il existe systématiquement une "box" équivalente en vue.

#### Lien vers des sections

A chaque fois qu'on indique le nom d'une personne, un parcours, un ménage, il y a toujours:

* un lien pour accéder à son dossier (pour autant que l'utilisateur ait les droits d'accès);
* à moins qu'il ne soit indiqué dans une phrase, l'icône de son dossier avant ou après (donc un bonhomme pour la personne, une maison pour le ménage, un fa-random pour les parcours);

Ces éléments sont toujours proposé par des `render_box` par défaut. Des options permettent de les désactiver dans des cas particuliers

> à discuter, quelques réflexion:
> quelle est la logique qui domine pour les boutons ? on a symbolisé les 4 actions du crud par des couleurs: bleu(show) orange(edit) vert(create) et rouge(delete).
> Est-ce que c'est ça qui prime, et comment ça s'articule avec la logique des pictos ?
> Par exemple, il pourrait être logique d'utiliser l'oeil bleu pour voir l'objet, qu'il s'agisse d'une personne ou d'un parcours, ce serait plutôt le contexte, et l'infobulle (title) qui préciserait le contexte.
> Je pense que les pictos de boutons doivent faire référence à l'action, mais pas à l'objet. Autrement dit je n'utiliserais jamais l'icone du ménage ou du parcours dans les boutons.
> Pour représenter les ménages et les parcours, je pense qu'il faudrait trouver autre chose que forkawesome. Si c'est des pictos, trouver un motif différents et de tailles différente. Réfléchir à un couplage picto-couleur-forme différent, qui exprime le contexte et qui se distingue bien des boutons.
> Idem pour les badges, il faut une palette de badge qui couvre tous les besoins: socialIssue, socialActions, socialReason, members, etc. [name=Mathieu]

### Formulaires

#### Vocabulaire:

Utiliser toujours:

* `Créer` dans un `bt bt-create` pour les **liens** vers le formulairep pour créer une entité (pour parvenir au formulaire);
* `Enregistrer` dans un `bt bt-save` pour les boutons "Enregistrer" (dans un formulaire édition **ou** création);
* `Enregistrer et nouveau`
* `Enregistrer et voir`
* `Modifier` dans un `bt bt-edit` pour les **liens** vers le formulaire d'édition
* `Dupliquer` (préciser là où on peut le voir)
* `Annuler` pour quitter une page d'édition avec un lien vers la liste, ou le `returnPath`

#### Retour après un enregistrement

Après avoir cliqué sur "Créer" ou "Sauver", la page devrait revenir:

* vers le returnPath, s'il existe;
* sinon, vers la page "vue".


### Bandeaux contenant les boutons d'actions

Les boutons sont toujours dans un bandeau "sticky-form" dans le bas du formulaire ou de la page de liste.

Si pertinent:

* Le bandeau contient un bouton "Annuler" qui retourne à la page précédente. Il est obligatoire pour les formulaires, optionnel pour les listes ou les pages "résumés"
* Ce bouton "annuler" est toujours à gauche

```
<ul class="record_actions sticky-form-buttons">
    <li class="cancel">
        <a href="{{ chill_entity_return_path('route_name' { 'route': 'option' } )}}">{{ return_path_label }}</a>
    </li>
    <li>
        <!-- action 1 -->
    </li>
</ul>
```

### Messages flash

#### A la création d'une entité

A chaque fois qu'un élément est créé par un formulaire, un message flash doit apparaitre. Il indique:

> "L'élément a été créé"

Le nom de l'élément peut être remplacé par quelque chose de plus pertinent:

> * L'activité a été créée
> * Le rendez-vous a été créé
> * ...


#### A l'enregistrement d'une entité

A chaque fois qu'un élément est enregistré, un message flash doit apparaitre:

> * Les données ont été modifiées
>

#### Erreur sur un formulaire (erreur de validation)

En tête d'un formulaire, un message flash doit indiquer que des validations n'ont pas réussi:

> Ce formulaire contient des erreurs

Les erreurs doivent apparaitre attachée au champ qui les concerne. Toutefois, il est acceptable d'afficher les erreurs à la racine du formulaire s'il était complexe, techniquement, d'attacher les erreurs.

### Liens de retour

A chaque fois qu'un lien est indiqué, vérifier si on ne doit pas utiliser la fonction `chill_return_path`, `chill_forward_return_path` ou `chill_return_path_or`.

* depuis la page liste, vers l'ouverture d'un élément, ou le bouton création => utiliser `chill_path_add_return_path`
* dans ces pages d'éditions,
    * utiliser `chill_return_path_or` dans le bouton "Cancel";
    * pour les boutons "enregistrer et voir" et "Enregistrer et fermer" => ?

### Assets pour les listes de suggestion

Créer une liste de suggestions à ajouter (tout l'item est cliquable)
```html
<ul class="list-suggest add-items">
   <li>
      <span>item</span>
   </li>
</ul>
```
Créer une liste de suggestions à enlever (avec une croix rouge cliquable, l'ancre a est vide)
```html
<ul class="list-suggest remove-items">
    <li>
        <span>
            item
        </span>
    </li>
</ul>
```
Créer un titre enlevable (avec une croix rouge cliquable, l'ancre a est vide)
```html
<div class="item-title">
    <span>title</span>
</div>
```
Les classes `cols` ou `inline` peuvent être ajoutées à côté de `list-suggest` pour modifier la disposition de la liste.
Dans le dernier exemple, on met une classe `removable` sur le span, si on veut pouvoir enlever l'élément.
