<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/docs',
        __DIR__ . '/src',
    ]);

    $rectorConfig->skip([
        \Rector\Php55\Rector\String_\StringClassNameToClassConstantRector::class => __DIR__ . 'src/Bundle/ChillMainBundle/Service/Notifier/LegacyOvhCloudFactory.php'
    ]);

    $rectorConfig->symfonyContainerXml(__DIR__ . '/var/cache/dev/test/App_KernelTestDebugContainer.xml  ');
    $rectorConfig->symfonyContainerPhp(__DIR__ . '/tests/symfony-container.php');

    //$rectorConfig->cacheClass(\Rector\Caching\ValueObject\Storage\FileCacheStorage::class);
    //$rectorConfig->cacheDirectory(__DIR__ . '/.cache/rector');

    // register a single rule
    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);
    $rectorConfig->rule(Rector\TypeDeclaration\Rector\ClassMethod\AddParamTypeFromPropertyTypeRector::class);
    $rectorConfig->rule(Rector\TypeDeclaration\Rector\Class_\MergeDateTimePropertyTypeDeclarationRector::class);
    $rectorConfig->rule(Rector\TypeDeclaration\Rector\ClassMethod\AddReturnTypeDeclarationBasedOnParentClassMethodRector::class);

    // part of the symfony 54 rules
    $rectorConfig->rule(\Rector\Symfony\Symfony53\Rector\StaticPropertyFetch\KernelTestCaseContainerPropertyDeprecationRector::class);
    $rectorConfig->rule(\Rector\Symfony\Symfony60\Rector\MethodCall\GetHelperControllerToServiceRector::class);
    $rectorConfig->disableParallel();

    //define sets of rules
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_82,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_40,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_41,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_42,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_43,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_44,
        \Rector\Doctrine\Set\DoctrineSetList::DOCTRINE_CODE_QUALITY,
        \Rector\PHPUnit\Set\PHPUnitSetList::PHPUNIT_90,
    ]);

    $rectorConfig->ruleWithConfiguration(\Rector\Php80\Rector\Class_\AnnotationToAttributeRector::class, [
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Symfony\Component\Serializer\Annotation\Context'),
    ]);

    // migrate for phpunit
    $rectorConfig->rules([
        \Rector\PHPUnit\PHPUnit100\Rector\Class_\StaticDataProviderClassMethodRector::class,
        \Rector\PHPUnit\PHPUnit100\Rector\Class_\PublicDataProviderClassMethodRector::class
    ]);

    // some routes are added twice if it remains activated
    // $rectorConfig->rule(\Rector\Symfony\Configs\Rector\ClassMethod\AddRouteAnnotationRector::class);

    // chill rules
    $rectorConfig->rules([
        \Chill\Utils\Rector\Rector\ChillBundleMakeDataProviderStaticForAbstractExportTestRector::class,
    ]);

    // skip some path...
    $rectorConfig->skip([
        // waiting for fixing this bug: https://github.com/rectorphp/rector-doctrine/issues/342
        \Rector\Doctrine\CodeQuality\Rector\Property\ImproveDoctrineCollectionDocTypeInEntityRector::class,
    ]);

    $rectorConfig->ruleWithConfiguration(AnnotationToAttributeRector::class, [
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\AccompanyingPeriodValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Household\HouseholdMembershipSequential'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Household\MaxHolder'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ConfidentialCourseMustHaveReferrer'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\LocationValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ParticipationOverlap'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod\ResourceDuplicateCheck'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Person\Birthdate'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Person\PersonHasCenter'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\PersonBundle\Validator\Constraints\Relationship\RelationshipNoDuplicate'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\ActivityBundle\Validator\Constraints\ActivityValidity'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\DocStoreBundle\Validator\Constraints\AsyncFileExists'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Validation\Constraint\PhonenumberConstraint'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Validator\Constraints\Entity\UserCircleConsistency'),
        new \Rector\Php80\ValueObject\AnnotationToAttribute('Chill\MainBundle\Workflow\Validator\EntityWorkflowCreation'),
    ]);
};
