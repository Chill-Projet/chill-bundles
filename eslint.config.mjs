import eslintPluginVue from "eslint-plugin-vue";
import ts from "typescript-eslint";
import eslintPluginPrettier from "eslint-plugin-prettier";

export default ts.config(
    ...ts.configs.recommended,
    ...ts.configs.stylistic,
    ...eslintPluginVue.configs["flat/essential"],
    {
        files: ["**/*.vue"],
        languageOptions: {
            parserOptions: {
                parser: "@typescript-eslint/parser",
            },
        },
    },
    {
        ignores: [
            "**/vendor/*",
            "**/import-png.d.ts",
            "**/chill.webpack.config.js",
            "**/var/*",
            "**/docker/*",
            "**/node_modules/*",
            "**/public/build/*"
        ],
    },
    {
        plugins: {
            prettier: eslintPluginPrettier,
        },
        rules: {
            "prettier/prettier": "error",
            // override/add rules settings here, such as:
            "vue/multi-word-component-names": "off",
            "@typescript-eslint/no-require-imports": "off",
        },
    },
);
