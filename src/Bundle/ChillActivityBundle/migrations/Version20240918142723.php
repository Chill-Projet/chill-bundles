<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918142723 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fix not null and default values for activityreason, activityreasoncategory, comments on activity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity ALTER privatecomment_comments SET NOT NULL');
        $this->addSql('ALTER TABLE activityreason ALTER name SET DEFAULT \'{}\'');
        $this->addSql('ALTER TABLE activityreason ALTER name SET NOT NULL');
        $this->addSql('ALTER INDEX idx_654a2fcd12469de2 RENAME TO IDX_AF82522312469DE2');
        $this->addSql('ALTER TABLE activityreasoncategory ALTER name SET DEFAULT \'{}\'');
        $this->addSql('ALTER TABLE activityreasoncategory ALTER name SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activityreason ALTER name DROP DEFAULT');
        $this->addSql('ALTER TABLE activityreason ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE activityreasoncategory ALTER name DROP DEFAULT');
        $this->addSql('ALTER TABLE activityreasoncategory ALTER name DROP NOT NULL');
    }
}
