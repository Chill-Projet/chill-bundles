<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422123846 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE activity_person');
        $this->addSql('DROP TABLE activity_thirdparty');
        $this->addSql('DROP TABLE activity_document');
        $this->addSql('DROP TABLE activity_user');

        $this->addSql('ALTER TABLE activity DROP travelTime');

        $this->addSql('ALTER TABLE activitytype DROP travelTimeVisible');
        $this->addSql('ALTER TABLE activitytype DROP travelTimeLabel');
        $this->addSql('ALTER TABLE activitytype DROP usersVisible');
        $this->addSql('ALTER TABLE activitytype DROP usersLabel');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE activity_person (activity_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(activity_id, person_id))');
        $this->addSql('CREATE INDEX IDX_66AA317681C06096 ON activity_person (activity_id)');
        $this->addSql('CREATE INDEX IDX_66AA3176217BBB47 ON activity_person (person_id)');
        $this->addSql('CREATE TABLE activity_thirdparty (activity_id INT NOT NULL, thirdparty_id INT NOT NULL, PRIMARY KEY(activity_id, thirdparty_id))');
        $this->addSql('CREATE INDEX IDX_C6F0DE0381C06096 ON activity_thirdparty (activity_id)');
        $this->addSql('CREATE INDEX IDX_C6F0DE03C7D3A8E6 ON activity_thirdparty (thirdparty_id)');
        $this->addSql('CREATE TABLE activity_document (activity_id INT NOT NULL, document_id INT NOT NULL, PRIMARY KEY(activity_id, document_id))');
        $this->addSql('CREATE INDEX IDX_78633A7881C06096 ON activity_document (activity_id)');
        $this->addSql('CREATE INDEX IDX_78633A78C33F7837 ON activity_document (document_id)');
        $this->addSql('CREATE TABLE activity_user (activity_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(activity_id, user_id))');
        $this->addSql('CREATE INDEX IDX_8E570DDB81C06096 ON activity_user (activity_id)');
        $this->addSql('CREATE INDEX IDX_8E570DDBA76ED395 ON activity_user (user_id)');
        $this->addSql('ALTER TABLE activity_person ADD CONSTRAINT FK_66AA317681C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_person ADD CONSTRAINT FK_66AA3176217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_thirdparty ADD CONSTRAINT FK_C6F0DE0381C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_thirdparty ADD CONSTRAINT FK_C6F0DE03C7D3A8E6 FOREIGN KEY (thirdparty_id) REFERENCES chill_3party.third_party (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_document ADD CONSTRAINT FK_78633A7881C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        // $this->addSql('ALTER TABLE activity_document ADD CONSTRAINT FK_78633A78C33F7837 FOREIGN KEY (document_id) REFERENCES Document (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_user ADD CONSTRAINT FK_8E570DDB81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_user ADD CONSTRAINT FK_8E570DDBA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE activity ADD travelTime TIME(0) WITHOUT TIME ZONE DEFAULT NULL');

        $this->addSql('ALTER TABLE activitytype ADD travelTimeVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD travelTimeLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD usersVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD usersLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
    }
}
