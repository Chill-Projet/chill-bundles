<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150702093317 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ActivityReasonCategory DROP COLUMN name;');
        $this->addSql('ALTER TABLE ActivityReasonCategory ADD COLUMN label VARCHAR(255) NOT NULL;');
        $this->addSql('ALTER TABLE ActivityReason DROP COLUMN name;');
        $this->addSql('ALTER TABLE ActivityReason ADD COLUMN label VARCHAR(255) NOT NULL;');
        $this->addSql('ALTER TABLE ActivityType DROP COLUMN name;');
        $this->addSql('ALTER TABLE ActivityType ADD COLUMN name VARCHAR(255) NOT NULL;');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ActivityReasonCategory DROP COLUMN label;');
        $this->addSql('ALTER TABLE ActivityReasonCategory ADD COLUMN name JSON;');
        $this->addSql('ALTER TABLE ActivityReason DROP COLUMN label;');
        $this->addSql('ALTER TABLE ActivityReason ADD COLUMN name JSON;');
        $this->addSql('ALTER TABLE ActivityType DROP COLUMN name;');
        $this->addSql('ALTER TABLE ActivityType ADD COLUMN name JSON;');
    }
}
