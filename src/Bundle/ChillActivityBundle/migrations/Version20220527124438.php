<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220527124438 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work DROP privateComment_comments');
    }

    public function getDescription(): string
    {
        return 'add private comment to activity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity ADD privateComment_comments JSON DEFAULT \'{}\'');
    }
}
