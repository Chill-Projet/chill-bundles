<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
class ActivityValidity extends Constraint
{
    final public const IS_REQUIRED_MESSAGE = ' is required';

    final public const ROOT_MESSAGE = 'For this type of activity, ';

    public $noPersonsMessage = 'For this type of activity, you must add at least one person';

    public $noThirdPartiesMessage = 'For this type of activity, you must add at least one third party';

    public $noUsersMessage = 'For this type of activity, you must add at least one user';

    public $socialActionsMessage = 'For this type of activity, you must add at least one social action';

    public $socialIssuesMessage = 'For this type of activity, you must add at least one social issue';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function makeIsRequiredMessage(string $property)
    {
        return self::ROOT_MESSAGE.$property.self::IS_REQUIRED_MESSAGE;
    }
}
