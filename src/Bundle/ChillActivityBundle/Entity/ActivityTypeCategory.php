<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'activitytypecategory')]
class ActivityTypeCategory
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $active = true;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    private array $name = [];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::FLOAT, options: ['default' => '0.0'])]
    private float $ordering = 0.0;

    /**
     * Get active
     * return true if the category type is active.
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name.
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    /**
     * Is active
     * return true if the category type is active.
     */
    public function isActive(): bool
    {
        return $this->getActive();
    }

    /**
     * Set active
     * set to true if the category type is active.
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set name.
     */
    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setOrdering(float $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }
}
