<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * Class ActivityPresence.
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'activitytpresence')]
class ActivityPresence
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $active = true;

    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $name = [];

    /**
     * Get active
     * return true if the category type is active.
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): array
    {
        return $this->name;
    }

    /**
     * Is active
     * return true if the category type is active.
     */
    public function isActive(): bool
    {
        return $this->getActive();
    }

    /**
     * Set active
     * set to true if the category type is active.
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
