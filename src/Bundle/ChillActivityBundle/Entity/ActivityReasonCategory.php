<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ActivityReasonCategory.
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'activityreasoncategory')]
class ActivityReasonCategory implements \Stringable
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $active = true;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;


    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, options: ['default' => '{}', 'jsonb' => true])]
    private array $name = [];

    /**
     * Array of ActivityReason.
     *
     * @var Collection<int, ActivityReason>
     */
    #[ORM\OneToMany(mappedBy: 'category', targetEntity: ActivityReason::class)]
    private Collection $reasons;

    /**
     * ActivityReasonCategory constructor.
     */
    public function __construct()
    {
        $this->reasons = new ArrayCollection();
    }

    public function __toString(): string
    {
        return 'ActivityReasonCategory('.$this->getName('x').')';
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @param mixed|null $locale
     *
     * @return array
     */
    public function getName($locale = null)
    {
        if ($locale) {
            if (isset($this->name[$locale])) {
                return $this->name[$locale];
            }

            foreach ($this->name as $name) {
                if (!empty($name)) {
                    return $name;
                }
            }

            return '';
        }

        return $this->name;
    }

    /**
     * Declare a category as active (or not). When a category is set
     * as unactive, all the reason have this entity as category is also
     * set as unactive.
     *
     * @return ActivityReasonCategory
     */
    public function setActive(bool $active)
    {
        if ($this->active !== $active && !$active) {
            foreach ($this->reasons as $reason) {
                $reason->setActive($active);
            }
        }

        $this->active = $active;

        return $this;
    }

    /**
     * Set name.
     *
     * @return ActivityReasonCategory
     */
    public function setName(array $name)
    {
        $this->name = $name;

        return $this;
    }
}
