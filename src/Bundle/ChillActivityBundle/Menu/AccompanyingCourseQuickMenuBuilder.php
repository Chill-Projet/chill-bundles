<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Menu;

use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;

final readonly class AccompanyingCourseQuickMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private Security $security) {}

    public static function getMenuIds(): array
    {
        return ['accompanying_course_quick_menu'];
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var \Chill\PersonBundle\Entity\AccompanyingPeriod $accompanyingCourse */
        $accompanyingCourse = $parameters['accompanying-course'];

        if ($this->security->isGranted(ActivityVoter::CREATE, $accompanyingCourse)) {
            $menu
                ->addChild('Create a new activity in accompanying course', [
                    'route' => 'chill_activity_activity_new',
                    'routeParameters' => [
                        // 'activityType_id' => '',
                        'accompanying_period_id' => $accompanyingCourse->getId(),
                    ],
                ])
                ->setExtras([
                    'order' => 10,
                    'icon' => 'plus',
                ])
            ;
        }
    }
}
