<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Menu;

use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Entity\Person;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements LocalMenuBuilderInterface<array{person: Person}>
 */
final readonly class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private AuthorizationCheckerInterface $authorizationChecker, private TranslatorInterface $translator) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var Person $person */
        $person = $parameters['person'];

        if ($this->authorizationChecker->isGranted(ActivityVoter::SEE, $person)) {
            $menu->addChild(
                $this->translator->trans('Activities'),
                [
                    'route' => 'chill_activity_activity_list',
                    'routeParameters' => ['person_id' => $person->getId()],
                ]
            )
                ->setExtra('order', 201);
        }
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
