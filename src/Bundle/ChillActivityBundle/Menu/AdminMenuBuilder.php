<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;

/**
 * @implements LocalMenuBuilderInterface<array>
 */
final readonly class AdminMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private Security $security) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Activities', [
            'route' => 'chill_activity_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 5000,
            ]);

        $menu->addChild('Activity Reasons', [
            'route' => 'chill_activity_activityreason',
        ])->setExtras(['order' => 5010]);

        $menu->addChild('Activity Reasons Category', [
            'route' => 'chill_activity_activityreasoncategory',
        ])->setExtras(['order' => 5020]);

        $menu->addChild('Activity type', [
            'route' => 'chill_activity_type_admin',
        ])->setExtras(['order' => 5030]);

        $menu->addChild('Activity Presences', [
            'route' => 'chill_crud_activity_presence_index',
        ])->setExtras(['order' => 5040]);

        $menu->addChild('Activity Types Categories', [
            'route' => 'chill_activity_type_category_admin',
        ])->setExtras(['order' => 5050]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_activity'];
    }
}
