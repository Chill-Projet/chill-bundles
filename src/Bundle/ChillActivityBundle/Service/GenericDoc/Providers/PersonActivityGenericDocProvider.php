<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Service\GenericDoc\Providers;

use Chill\ActivityBundle\Repository\ActivityDocumentACLAwareRepositoryInterface;
use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Chill\DocStoreBundle\GenericDoc\FetchQueryInterface;
use Chill\DocStoreBundle\GenericDoc\GenericDocForPersonProviderInterface;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Security\Core\Security;

final readonly class PersonActivityGenericDocProvider implements GenericDocForPersonProviderInterface
{
    public const KEY = 'person_activity_document';

    public function __construct(
        private Security $security,
        private ActivityDocumentACLAwareRepositoryInterface $personActivityDocumentACLAwareRepository,
    ) {}

    public function buildFetchQueryForPerson(Person $person, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, ?string $origin = null): FetchQueryInterface
    {
        return $this->personActivityDocumentACLAwareRepository->buildFetchQueryActivityDocumentLinkedToPersonFromPersonContext(
            $person,
            $startDate,
            $endDate,
            $content
        );
    }

    public function isAllowedForPerson(Person $person): bool
    {
        return $this->security->isGranted(ActivityVoter::SEE, $person);
    }
}
