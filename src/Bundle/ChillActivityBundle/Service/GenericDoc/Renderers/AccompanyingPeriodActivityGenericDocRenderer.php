<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Service\GenericDoc\Renderers;

use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\ActivityBundle\Service\GenericDoc\Providers\AccompanyingPeriodActivityGenericDocProvider;
use Chill\ActivityBundle\Service\GenericDoc\Providers\PersonActivityGenericDocProvider;
use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\Twig\GenericDocRendererInterface;
use Chill\DocStoreBundle\Repository\StoredObjectRepository;

/**
 * @implements GenericDocRendererInterface<array{row-only?: bool, show-actions?: bool}>
 */
final readonly class AccompanyingPeriodActivityGenericDocRenderer implements GenericDocRendererInterface
{
    public function __construct(private StoredObjectRepository $objectRepository, private ActivityRepository $activityRepository) {}

    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool
    {
        return AccompanyingPeriodActivityGenericDocProvider::KEY === $genericDocDTO->key || PersonActivityGenericDocProvider::KEY === $genericDocDTO->key;
    }

    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string
    {
        return ($options['row-only'] ?? false) ? '@ChillActivity/GenericDoc/activity_document_row.html.twig' :
            '@ChillActivity/GenericDoc/activity_document.html.twig';
    }

    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array
    {
        return [
            'activity' => $this->activityRepository->find($genericDocDTO->identifiers['activity_id']),
            'document' => $this->objectRepository->find($genericDocDTO->identifiers['id']),
            'context' => $genericDocDTO->getContext(),
            'show_actions' => $options['show-actions'] ?? true,
        ];
    }
}
