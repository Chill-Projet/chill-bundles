<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Notification;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatableInterface;

final readonly class ActivityNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private ActivityRepository $activityRepository, private TranslatableStringHelperInterface $translatableStringHelper) {}

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillActivity/Activity/showInNotification.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return [
            'notification' => $notification,
            'activity' => $this->activityRepository->find($notification->getRelatedEntityId()),
        ];
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return Activity::class === $notification->getRelatedEntityClass();
    }

    public function getTitle(Notification $notification, array $options = []): TranslatableInterface
    {
        if (null === $activity = $this->getRelatedEntity($notification)) {
            return new TranslatableMessage('activity.deleted');
        }

        return new TranslatableMessage('activity.title', [
            'date' => $activity->getDate(),
            'type' => $this->translatableStringHelper->localize($activity->getActivityType()->getName()),
        ]);
    }

    public function getAssociatedPersons(Notification $notification, array $options = []): array
    {
        if (null === $activity = $this->getRelatedEntity($notification)) {
            return [];
        }

        return $activity->getPersonsAssociated();
    }

    public function getRelatedEntity(Notification $notification): ?Activity
    {
        return $this->activityRepository->find($notification->getRelatedEntityId());
    }
}
