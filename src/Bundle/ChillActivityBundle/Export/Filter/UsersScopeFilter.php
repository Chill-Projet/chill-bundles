<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UsersScopeFilter implements FilterInterface
{
    private const PREFIX = 'act_filter_user_scope';

    public function __construct(
        private readonly ScopeRepositoryInterface $scopeRepository,
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->andWhere(
                $qb->expr()->exists(
                    'SELECT 1 FROM '.Activity::class." {$p}_act "
                    ."JOIN {$p}_act.users {$p}_user "
                    .'JOIN '.UserScopeHistory::class." {$p}_history WITH {$p}_history.user = {$p}_user "
                    ."WHERE {$p}_act = activity "
                    // scope_at based on activity.date
                    ."AND {$p}_history.startDate <= activity.date "
                    ."AND ({$p}_history.endDate IS NULL OR {$p}_history.endDate > activity.date) "
                    ."AND {$p}_history.scope IN ( :{$p}_scopes )"
                )
            )
            ->setParameter(
                "{$p}_scopes",
                $data['scopes']
            );
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('scopes', EntityType::class, [
                'class' => Scope::class,
                'choices' => $this->scopeRepository->findAllActive(),
                'choice_label' => fn (Scope $s) => $this->translatableStringHelper->localize($s->getName()),
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function describeAction($data, $format = 'string'): array
    {
        return ['export.filter.activity.by_users_scope.Filtered activity by users scope: only %scopes%', [
            '%scopes%' => implode(
                ', ',
                array_map(
                    fn (Scope $s) => $this->translatableStringHelper->localize($s->getName()),
                    $data['scopes'] instanceof Collection ? $data['scopes']->toArray() : $data['scopes']
                )
            ),
        ]];
    }

    public function getFormDefaultData(): array
    {
        return [
            'scopes' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.activity.by_users_scope.Filter by users scope';
    }
}
