<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickLocationTypeType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class LocationTypeFilter implements FilterInterface
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actloc', $qb->getAllAliases(), true)) {
            $qb->join('activity.location', 'actloc');
        }

        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->in('actloc.locationType', ':locationtype');

        if ($where instanceof Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('locationtype', $data['accepted_locationtype']);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_locationtype', PickLocationTypeType::class, [
            'multiple' => true,
            // 'label' => false,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $types = [];

        foreach ($data['accepted_locationtype'] as $type) {
            $types[] = $this->translatableStringHelper->localize(
                $type->getTitle()
            );
        }

        return ['Filtered activity by locationtype: only %types%', [
            '%types%' => implode(', ', $types),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by locationtype';
    }
}
