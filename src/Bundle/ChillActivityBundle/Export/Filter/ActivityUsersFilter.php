<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Templating\Entity\UserRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ActivityUsersFilter implements FilterInterface
{
    public function __construct(private readonly UserRender $userRender) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $orX = $qb->expr()->orX();

        foreach ($data['accepted_users'] as $key => $user) {
            $orX->add($qb->expr()->isMemberOf(':activity_users_filter_u'.$key, 'activity.users'));
            $qb->setParameter('activity_users_filter_u'.$key, $user);
        }

        $qb->andWhere($orX);
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_users', PickUserDynamicType::class, [
            'multiple' => true,
            'label' => 'Users',
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string')
    {
        $users = [];

        foreach ($data['accepted_users'] as $u) {
            $users[] = $this->userRender->renderString($u, []);
        }

        return ['Filtered activity by users: only %users%', [
            '%users%' => implode(', ', $users),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by users';
    }
}
