<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UsersJobFilter implements FilterInterface
{
    private const PREFIX = 'act_filter_user_job';

    public function __construct(
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
        private readonly UserJobRepositoryInterface $userJobRepository,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->andWhere(
                $qb->expr()->exists(
                    'SELECT 1 FROM '.Activity::class." {$p}_act "
                    ."JOIN {$p}_act.users {$p}_user "
                    .'JOIN '.UserJobHistory::class." {$p}_history WITH {$p}_history.user = {$p}_user "
                    ."WHERE {$p}_act = activity "
                    // job_at based on activity.date
                    ."AND {$p}_history.startDate <= activity.date "
                    ."AND ({$p}_history.endDate IS NULL OR {$p}_history.endDate > activity.date) "
                    ."AND {$p}_history.job IN ( :{$p}_jobs )"
                )
            )
            ->setParameter(
                "{$p}_jobs",
                $data['jobs']
            );
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('jobs', EntityType::class, [
                'class' => UserJob::class,
                'choices' => $this->userJobRepository->findAllActive(),
                'choice_label' => fn (UserJob $j) => $this->translatableStringHelper->localize($j->getLabel()),
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function describeAction($data, $format = 'string')
    {
        return ['export.filter.activity.by_users_job.Filtered activity by users job: only %jobs%', [
            '%jobs%' => implode(
                ', ',
                array_map(
                    fn (UserJob $job) => $this->translatableStringHelper->localize($job->getLabel()),
                    $data['jobs'] instanceof Collection ? $data['jobs']->toArray() : $data['jobs']
                )
            ),
        ]];
    }

    public function getFormDefaultData(): array
    {
        return [
            'jobs' => [],
        ];
    }

    public function getTitle()
    {
        return 'export.filter.activity.by_users_job.Filter by users job';
    }
}
