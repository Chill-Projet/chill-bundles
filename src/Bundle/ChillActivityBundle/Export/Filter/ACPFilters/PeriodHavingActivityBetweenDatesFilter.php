<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class PeriodHavingActivityBetweenDatesFilter implements FilterInterface
{
    public function __construct(
        private RollingDateConverterInterface $rollingDateConverter,
    ) {}

    public function getTitle()
    {
        return 'export.filter.activity.course_having_activity_between_date.Title';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('start_date', PickRollingDateType::class, [
                'label' => 'export.filter.activity.course_having_activity_between_date.Receiving an activity after',
            ])
            ->add('end_date', PickRollingDateType::class, [
                'label' => 'export.filter.activity.course_having_activity_between_date.Receiving an activity before',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
            'end_date' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'export.filter.activity.course_having_activity_between_date.Only course having an activity between from and to',
            [
                'from' => $this->rollingDateConverter->convert($data['start_date']),
                'to' => $this->rollingDateConverter->convert($data['end_date']),
            ],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $alias = 'act_period_having_act_betw_date_alias';
        $from = 'act_period_having_act_betw_date_start';
        $to = 'act_period_having_act_betw_date_end';

        $qb->andWhere(
            $qb->expr()->exists(
                'SELECT 1 FROM '.Activity::class." {$alias} WHERE {$alias}.date >= :{$from} AND {$alias}.date < :{$to} AND {$alias}.accompanyingPeriod = acp"
            )
        );

        $qb
            ->setParameter($from, $this->rollingDateConverter->convert($data['start_date']))
            ->setParameter($to, $this->rollingDateConverter->convert($data['end_date']));
    }

    public function applyOn()
    {
        return \Chill\PersonBundle\Export\Declarations::ACP_TYPE;
    }
}
