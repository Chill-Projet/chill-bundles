<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Entity\ActivityPresence;
use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ActivityPresenceFilter implements FilterInterface
{
    public function __construct(
        private TranslatableStringHelperInterface $translatableStringHelper,
        private TranslatorInterface $translator,
    ) {}

    public function getTitle()
    {
        return 'export.filter.activity.by_presence.Filter activity by activity presence';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('presences', EntityType::class, [
            'class' => ActivityPresence::class,
            'choice_label' => fn (ActivityPresence $presence) => $this->translatableStringHelper->localize($presence->getName())
                .($presence->isActive() ? '' : ' ('.$this->translator->trans('inactive').')'),
            'multiple' => true,
            'expanded' => true,
            'label' => 'export.filter.activity.by_presence.presences',
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string')
    {
        $presences = array_map(
            fn (ActivityPresence $presence) => $this->translatableStringHelper->localize($presence->getName()),
            $data['presences'] instanceof Collection ? $data['presences']->toArray() : $data['presences']
        );

        return [
            'export.filter.activity.by_presence.Filtered by activity presence: only %presences%',
            ['%presences%' => implode(', ', $presences)],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->andWhere('activity.attendee IN (:activity_presence_filter_presences)')
            ->setParameter('activity_presence_filter_presences', $data['presences']);
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY;
    }
}
