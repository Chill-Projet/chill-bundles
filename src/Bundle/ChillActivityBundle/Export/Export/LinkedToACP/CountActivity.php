<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Security\Authorization\ActivityStatsVoter;
use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class CountActivity implements ExportInterface, GroupedExportInterface
{
    protected EntityRepository $repository;

    private readonly bool $filterStatsByCenters;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $parameterBag,
    ) {
        $this->repository = $em->getRepository(Activity::class);
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'Count activities linked to an accompanying period by various parameters.';
    }

    public function getGroup(): string
    {
        return 'Exports of activities linked to an accompanying period';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_count_activity' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return static fn ($value) => '_header' === $value ? 'Number of activities linked to an accompanying period' : $value;
    }

    public function getQueryKeys($data): array
    {
        return ['export_count_activity'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'Count activities linked to an accompanying period';
    }

    public function getType(): string
    {
        return Declarations::ACTIVITY;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->repository
            ->createQueryBuilder('activity')
            ->join('activity.accompanyingPeriod', 'acp');

        if ($this->filterStatsByCenters) {
            $qb
                ->andWhere(
                    $qb->expr()->exists(
                        'SELECT 1 FROM '.AccompanyingPeriodParticipation::class.' acl_count_part
                    JOIN '.PersonCenterHistory::class.' acl_count_person_history WITH IDENTITY(acl_count_person_history.person) = IDENTITY(acl_count_part.person)
                    WHERE acl_count_part.accompanyingPeriod = acp.id AND acl_count_person_history.center IN (:authorized_centers)
                    '
                    )
                )
                ->setParameter('authorized_centers', $centers);
        }

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        $qb->select('COUNT(DISTINCT activity.id) as export_count_activity');

        return $qb;
    }

    public function requiredRole(): string
    {
        return ActivityStatsVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::ACTIVITY,
            Declarations::ACTIVITY_ACP,
            PersonDeclarations::ACP_TYPE,
        ];
    }
}
