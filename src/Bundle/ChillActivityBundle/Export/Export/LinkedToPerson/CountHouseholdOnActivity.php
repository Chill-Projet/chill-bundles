<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\ActivityBundle\Security\Authorization\ActivityStatsVoter;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class CountHouseholdOnActivity implements ExportInterface, GroupedExportInterface
{
    private bool $filterStatsByCenters;

    public function __construct(
        private ActivityRepository $activityRepository,
        ParameterBagInterface $parameterBag,
    ) {
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription()
    {
        return 'export.export.count_household_on_activity_person.description';
    }

    public function getGroup(): string
    {
        return 'Exports of activities linked to a person';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_count_activity' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return static fn ($value) => '_header' === $value ? 'export.export.count_household_on_activity_person.header' : $value;
    }

    public function getQueryKeys($data)
    {
        return ['export_count_activity'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle()
    {
        return 'export.export.count_household_on_activity_person.title';
    }

    public function getType(): string
    {
        return Declarations::ACTIVITY;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->activityRepository
            ->createQueryBuilder('activity')
            ->join('activity.person', 'person')
            ->join(
                HouseholdMember::class,
                'householdmember',
                Query\Expr\Join::WITH,
                'person = householdmember.person AND householdmember.startDate <= activity.date AND (householdmember.endDate IS NULL OR householdmember.endDate > activity.date)'
            )
            ->join('householdmember.household', 'household');

        $qb->select('COUNT(DISTINCT household.id) as export_count_activity');

        if ($this->filterStatsByCenters) {
            $qb
                ->join('person.centerHistory', 'centerHistory')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->lte('centerHistory.startDate', 'activity.date'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull('centerHistory.endDate'),
                            $qb->expr()->gt('centerHistory.endDate', 'activity.date')
                        )
                    )
                )
                ->andWhere($qb->expr()->in('centerHistory.center', ':centers'))
                ->setParameter('centers', $centers);
        }

        return $qb;
    }

    public function requiredRole(): string
    {
        return ActivityStatsVoter::STATS;
    }

    public function supportsModifiers()
    {
        return [
            Declarations::ACTIVITY,
            Declarations::ACTIVITY_PERSON,
            PersonDeclarations::PERSON_TYPE,
            PersonDeclarations::HOUSEHOLD_TYPE,
        ];
    }
}
