<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\ActivityBundle\Security\Authorization\ActivityStatsVoter;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class CountActivity implements ExportInterface, GroupedExportInterface
{
    private readonly bool $filterStatsByCenters;

    public function __construct(
        private readonly ActivityRepository $activityRepository,
        ParameterBagInterface $parameterBag,
    ) {
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes()
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription()
    {
        return 'Count activities linked to a person by various parameters.';
    }

    public function getGroup(): string
    {
        return 'Exports of activities linked to a person';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_count_activity' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return static fn ($value) => '_header' === $value ? 'Number of activities linked to a person' : $value;
    }

    public function getQueryKeys($data)
    {
        return ['export_count_activity'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle()
    {
        return 'Count activities linked to a person';
    }

    public function getType(): string
    {
        return Declarations::ACTIVITY;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->activityRepository
            ->createQueryBuilder('activity')
            ->join('activity.person', 'person');

        $qb->select('COUNT(activity.id) as export_count_activity');

        if ($this->filterStatsByCenters) {
            $qb
                ->join('person.centerHistory', 'centerHistory')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->lte('centerHistory.startDate', 'activity.date'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull('centerHistory.endDate'),
                            $qb->expr()->gt('centerHistory.endDate', 'activity.date')
                        )
                    )
                )
                ->andWhere($qb->expr()->in('centerHistory.center', ':centers'))
                ->setParameter('centers', $centers);
        }

        return $qb;
    }

    public function requiredRole(): string
    {
        return ActivityStatsVoter::STATS;
    }

    public function supportsModifiers()
    {
        return [
            Declarations::ACTIVITY,
            Declarations::ACTIVITY_PERSON,
            PersonDeclarations::PERSON_TYPE,
        ];
    }
}
