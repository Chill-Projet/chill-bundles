<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Repository\ActivityTypeRepositoryInterface;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ActivityTypeAggregator implements AggregatorInterface
{
    final public const KEY = 'activity_type_aggregator';

    public function __construct(protected ActivityTypeRepositoryInterface $activityTypeRepository, protected TranslatableStringHelperInterface $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acttype', $qb->getAllAliases(), true)) {
            $qb->leftJoin('activity.activityType', 'acttype');
        }

        $qb->addSelect(sprintf('IDENTITY(activity.activityType) AS %s', self::KEY));
        $qb->addGroupBy(self::KEY);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form required for this aggregator
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function (int|string|null $value): string {
            if ('_header' === $value) {
                return 'Activity type';
            }

            if (null === $value || '' === $value || null === $t = $this->activityTypeRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize($t->getName());
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::KEY];
    }

    public function getTitle()
    {
        return 'Aggregate by activity type';
    }
}
