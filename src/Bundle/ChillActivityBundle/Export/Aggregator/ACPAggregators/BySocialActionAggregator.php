<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator\ACPAggregators;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class BySocialActionAggregator implements AggregatorInterface
{
    public function __construct(private readonly SocialActionRender $actionRender, private readonly SocialActionRepository $actionRepository) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actsocialaction', $qb->getAllAliases(), true)) {
            $qb->leftJoin('activity.socialActions', 'actsocialaction');
        }

        $qb->addSelect('actsocialaction.id AS socialaction_aggregator');
        $qb->addGroupBy('socialaction_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY_ACP;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) {
            if ('_header' === $value) {
                return 'Social action';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $sa = $this->actionRepository->find($value);

            return $this->actionRender->renderString($sa, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['socialaction_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group activity by linked socialaction';
    }
}
