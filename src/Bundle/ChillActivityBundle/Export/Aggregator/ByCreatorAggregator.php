<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserRepositoryInterface;
use Chill\MainBundle\Templating\Entity\UserRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByCreatorAggregator implements AggregatorInterface
{
    public function __construct(private readonly UserRepositoryInterface $userRepository, private readonly UserRender $userRender) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('IDENTITY(activity.createdBy) AS creator_aggregator');
        $qb->addGroupBy('creator_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Created by';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $u = $this->userRepository->find($value);

            return $this->userRender->renderString($u, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['creator_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group activity by creator';
    }
}
