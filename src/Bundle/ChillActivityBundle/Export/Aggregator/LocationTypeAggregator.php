<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\LocationTypeRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class LocationTypeAggregator implements AggregatorInterface
{
    public function __construct(private readonly LocationTypeRepository $locationTypeRepository, private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actloc', $qb->getAllAliases(), true)) {
            $qb->leftJoin('activity.location', 'actloc');
        }

        $qb->addSelect('IDENTITY(actloc.locationType) AS locationtype_aggregator');
        $qb->addGroupBy('locationtype_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Accepted locationtype';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            if (null === $lt = $this->locationTypeRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize(
                $lt->getTitle()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['locationtype_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group activity by locationtype';
    }
}
