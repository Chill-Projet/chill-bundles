<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\ThirdPartyBundle\Repository\ThirdPartyRepository;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByThirdpartyAggregator implements AggregatorInterface
{
    public function __construct(private readonly ThirdPartyRepository $thirdPartyRepository, private readonly ThirdPartyRender $thirdPartyRender) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acttparty', $qb->getAllAliases(), true)) {
            $qb->leftJoin('activity.thirdParties', 'acttparty');
        }

        $qb->addSelect('acttparty.id AS thirdparty_aggregator');
        $qb->addGroupBy('thirdparty_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Accepted thirdparty';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $tp = $this->thirdPartyRepository->find($value);

            return $this->thirdPartyRender->renderString($tp, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['thirdparty_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group activity by linked thirdparties';
    }
}
