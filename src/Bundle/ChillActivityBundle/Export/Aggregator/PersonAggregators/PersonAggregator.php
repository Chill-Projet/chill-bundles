<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator\PersonAggregators;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class PersonAggregator implements AggregatorInterface
{
    public function __construct(private LabelPersonHelper $labelPersonHelper) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to add here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return $this->labelPersonHelper->getLabel($key, $values, 'export.aggregator.person.by_person.person');
    }

    public function getQueryKeys($data)
    {
        return ['activity_by_person_agg'];
    }

    public function getTitle()
    {
        return 'export.aggregator.person.by_person.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->addSelect('IDENTITY(activity.person) AS activity_by_person_agg')
            ->addGroupBy('activity_by_person_agg');
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY_PERSON;
    }
}
