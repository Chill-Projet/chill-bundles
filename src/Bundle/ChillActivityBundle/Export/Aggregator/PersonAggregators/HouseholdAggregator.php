<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator\PersonAggregators;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Repository\Household\HouseholdRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class HouseholdAggregator implements AggregatorInterface
{
    public function __construct(private HouseholdRepository $householdRepository) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to add here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (int|string|null $value): string|int {
            if ('_header' === $value) {
                return 'export.aggregator.person.by_household.household';
            }

            if ('' === $value || null === $value || null === $household = $this->householdRepository->find($value)) {
                return '';
            }

            return $household->getId();
        };
    }

    public function getQueryKeys($data)
    {
        return ['activity_household_agg'];
    }

    public function getTitle()
    {
        return 'export.aggregator.person.by_household.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->join(
            HouseholdMember::class,
            'activity_household_agg_household_member',
            Join::WITH,
            $qb->expr()->andX(
                $qb->expr()->eq('activity_household_agg_household_member.person', 'activity.person'),
                $qb->expr()->lte('activity_household_agg_household_member.startDate', 'activity.date'),
                $qb->expr()->orX(
                    $qb->expr()->gte('activity_household_agg_household_member.endDate', 'activity.date'),
                    $qb->expr()->isNull('activity_household_agg_household_member.endDate')
                )
            )
        );

        $qb->join(
            Household::class,
            'activity_household_agg_household',
            Join::WITH,
            $qb->expr()->eq('activity_household_agg_household_member.household', 'activity_household_agg_household')
        );

        $qb
            ->addSelect('activity_household_agg_household.id AS activity_household_agg')
            ->addGroupBy('activity_household_agg');
    }

    public function applyOn()
    {
        return Declarations::ACTIVITY_PERSON;
    }
}
