<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SentReceivedAggregator implements AggregatorInterface
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $qb->addSelect('activity.sentReceived AS activity_sentreceived_aggregator')
            ->addGroupBy('activity_sentreceived_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        // No form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): callable
    {
        return function (?string $value): string {
            if ('_header' === $value) {
                return 'export.aggregator.activity.by_sent_received.Sent or received';
            }

            switch ($value) {
                case null:
                case '':
                    return '';

                case 'sent':
                    return $this->translator->trans('export.aggregator.activity.by_sent_received.is sent');

                case 'received':
                    return $this->translator->trans('export.aggregator.activity.by_sent_received.is received');

                default:
                    throw new \LogicException(sprintf('The value %s is not valid', $value));
            }
        };
    }

    public function getQueryKeys($data): array
    {
        return ['activity_sentreceived_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.activity.by_sent_received.Group activity by sentreceived';
    }
}
