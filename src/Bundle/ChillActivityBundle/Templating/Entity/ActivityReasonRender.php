<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Templating\Entity;

use Chill\ActivityBundle\Entity\ActivityReason;
use Chill\MainBundle\Templating\Entity\BoxUtilsChillEntityRenderTrait;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * Render activity reason.
 *
 * @implements ChillEntityRenderInterface<ActivityReason>
 */
class ActivityReasonRender implements ChillEntityRenderInterface
{
    use BoxUtilsChillEntityRenderTrait;
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function renderBox($entity, array $options): string
    {
        return
            $this->getDefaultOpeningBox('activity-reason').
            '<span class="badge bg-chill-pink">'.
            '<i class="fa fa-question-circle"></i>&nbsp;'.
            '<span class="category">'.
            $this->translatableStringHelper->localize(
                $entity->getCategory()->getName()
            ).
            '</span>'.
            '<span class="separator">&nbsp;>&nbsp;</span>'.
            '<span class="reason">'.
            $this->translatableStringHelper->localize(
                $entity->getName()
            ).
            '</span>'.
            '</span>'.
            $this->getDefaultClosingBox();
    }

    public function renderString($entity, array $options): string
    {
        $category = '';

        if (null !== $entity->getCategory()) {
            $category = $this->translatableStringHelper->localize(
                $entity->getCategory()->getName()
            ).' > ';
        }

        return $category.
            $this->translatableStringHelper->localize(
                $entity->getName()
            );
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof ActivityReason;
    }
}
