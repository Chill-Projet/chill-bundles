<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Security\Authorization;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectVoter\AbstractStoredObjectVoter;
use Chill\MainBundle\Workflow\Helper\WorkflowRelatedEntityPermissionHelper;
use Symfony\Component\Security\Core\Security;

class ActivityStoredObjectVoter extends AbstractStoredObjectVoter
{
    public function __construct(
        private readonly ActivityRepository $repository,
        Security $security,
        WorkflowRelatedEntityPermissionHelper $workflowDocumentService,
    ) {
        parent::__construct($security, $workflowDocumentService);
    }

    protected function getRepository(): AssociatedEntityToStoredObjectInterface
    {
        return $this->repository;
    }

    protected function getClass(): string
    {
        return Activity::class;
    }

    protected function attributeToRole(StoredObjectRoleEnum $attribute): string
    {
        return match ($attribute) {
            StoredObjectRoleEnum::EDIT => ActivityVoter::UPDATE,
            StoredObjectRoleEnum::SEE => ActivityVoter::SEE_DETAILS,
        };
    }

    protected function canBeAssociatedWithWorkflow(): bool
    {
        return false;
    }
}
