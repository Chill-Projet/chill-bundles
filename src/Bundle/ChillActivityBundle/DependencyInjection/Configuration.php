<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use function is_int;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_activity');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->arrayNode('form')
            ->canBeEnabled()
            ->children()
            ->arrayNode('time_duration')
            ->isRequired()
            ->requiresAtLeastOneElement()
            ->defaultValue(
                [
                    ['label' => '5 minutes',  'seconds' => 300],
                    ['label' => '10 minutes', 'seconds' => 600],
                    ['label' => '15 minutes', 'seconds' => 900],
                    ['label' => '20 minutes', 'seconds' => 1200],
                    ['label' => '25 minutes', 'seconds' => 1500],
                    ['label' => '30 minutes', 'seconds' => 1800],
                    ['label' => '45 minutes', 'seconds' => 2700],
                    ['label' => '1 hour',     'seconds' => 3600],
                    ['label' => '1 hour 15',  'seconds' => 4500],
                    ['label' => '1 hour 30',  'seconds' => 5400],
                    ['label' => '1 hour 45',  'seconds' => 6300],
                    ['label' => '2 hours',    'seconds' => 7200],
                ]
            )
            ->info('The intervals of time to show in activity form')
            ->prototype('array')
            ->children()
            ->scalarNode('seconds')
            ->info('The number of seconds of this duration. Must be an integer.')
            ->cannotBeEmpty()
            ->validate()
            ->ifTrue(static fn ($data) => !\is_int($data))->thenInvalid('The value %s is not a valid integer')
            ->end()
            ->end()
            ->scalarNode('label')
            ->cannotBeEmpty()
            ->info('The label to show into fields')
            ->end()
            ->end()
            ->end()
//                            ->validate()
//
//                                ->ifTrue(function ($data) {
//                                    // test this is an array
//                                    if (!is_array($data)) {
//                                        return true;
//                                    }
//
//                                    foreach ($data as $k => $v) {
//                                        if (!is_string($k)) {
//                                            return true;
//                                        }
//                                        if (!is_int($v)) {
//                                            return true;
//                                        }
//                                    }
//
//                                    })
//                                    ->thenInvalid("The data are invalid. The keys must be a string and the value integers")
//                            ->end()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
