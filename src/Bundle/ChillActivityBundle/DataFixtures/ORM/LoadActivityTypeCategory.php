<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Entity\ActivityTypeCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixtures for ActivityTypeCategory.
 */
class LoadActivityTypeCategory extends Fixture implements OrderedFixtureInterface
{
    public static $references = [];

    public function getOrder(): int
    {
        return 16050;
    }

    public function load(ObjectManager $manager): void
    {
        $categories = [
            [
                'name' => ['fr' => 'Échange avec usager', 'en' => 'Exchange with user'],
                'ref' => 'exchange',
            ],
            [
                'name' => ['fr' => 'Réunion', 'en' => 'Meeting'],
                'ref' => 'meeting',
            ],
        ];

        foreach ($categories as $cat) {
            echo 'Creating activity type category : '.$cat['ref']."\n";

            $newCat = (new ActivityTypeCategory())
                ->setName($cat['name']);

            $manager->persist($newCat);
            $reference = 'activity_type_cat_'.$cat['ref'];

            $this->addReference($reference, $newCat);
            static::$references[] = $reference;
        }

        $manager->flush();
    }
}
