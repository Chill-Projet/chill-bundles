<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Entity\ActivityReasonCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Description of LoadActivityReasonCategory.
 */
class LoadActivityReasonCategory extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 16200;
    }

    public function load(ObjectManager $manager): void
    {
        $categs = [
            ['name' => ['fr' => 'Logement', 'en' => 'Housing', 'nl' => 'Woning']],
            ['name' => ['fr' => 'Démarches chômage', 'en' => 'Unemployment procedure', 'nl' => 'Werkloosheid werkwijze']],
        ];

        foreach ($categs as $c) {
            echo 'Creating activity reason category : '.$c['name']['en']."\n";
            $activityReasonCategory = (new ActivityReasonCategory())
                ->setName($c['name'])
                ->setActive(true);
            $manager->persist($activityReasonCategory);
            $this->addReference(
                'cat_'.$c['name']['en'],
                $activityReasonCategory
            );
        }

        $manager->flush();
    }
}
