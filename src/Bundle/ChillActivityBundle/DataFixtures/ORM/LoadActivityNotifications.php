<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Entity\Activity;
use Chill\MainBundle\DataFixtures\ORM\LoadAbstractNotificationsTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Load notififications into database.
 */
class LoadActivityNotifications extends AbstractFixture implements DependentFixtureInterface
{
    use LoadAbstractNotificationsTrait;

    public $notifs = [
        [
            'message' => 'Hello !',
            'entityClass' => Activity::class,
            'entityRef' => 'activity_gerard depardieu',
            'sender' => 'center a_social',
            'addressees' => [
                'center a_social',
                'center a_administrative',
                'center a_direction',
                'multi_center',
            ],
        ],
    ];

    public function getDependencies(): array
    {
        return [
            LoadActivity::class,
        ];
    }
}
