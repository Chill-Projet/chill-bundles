<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\DataFixtures\ORM;

use Chill\ActivityBundle\Entity\ActivityReason;
use Chill\ActivityBundle\Entity\ActivityReasonCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Description of LoadActivityReason.
 */
class LoadActivityReason extends AbstractFixture implements OrderedFixtureInterface
{
    public static $references = [];

    public function getOrder(): int
    {
        return 16300;
    }

    public function load(ObjectManager $manager): void
    {
        $reasons = [
            [
                'name' => ['fr' => 'Recherche logement', 'en' => 'Housing research', 'nl' => 'Woning zoektoch'],
                'category' => 'cat_Housing', ],
            [
                'name' => ['fr' => 'Problème avec propriétaire', 'en' => 'Landlord problems', 'nl' => 'Huisbaas problemen'],
                'category' => 'cat_Housing', ],
            [
                'name' => ['fr' => 'Retard de payement', 'en' => 'Payement problems', 'nl' => 'Betalings vertragingen'],
                'category' => 'cat_Housing', ],
            [
                'name' => ['fr' => 'Explication législation', 'en' => 'Legislation explanation', 'nl' => 'Legislative uitleg'],
                'category' => 'cat_Unemployment procedure', ],
            [
                'name' => ['fr' => 'Coaching entretien d\'activation', 'en' => 'Interview coaching', 'nl' => 'Interview coaching'],
                'category' => 'cat_Unemployment procedure', ],
            [
                'name' => ['fr' => 'Récupération des allocations', 'en' => 'Allowance recovery', 'nl' => 'Terugwinning van de uitkeringen'],
                'category' => 'cat_Unemployment procedure', ],
        ];

        foreach ($reasons as $r) {
            echo 'Creating activity reason : '.$r['name']['en']."\n";
            $activityReason = (new ActivityReason())
                ->setName($r['name'])
                ->setActive(true)
                ->setCategory($this->getReference($r['category'], ActivityReasonCategory::class));
            $manager->persist($activityReason);
            $reference = 'activity_reason_'.$r['name']['en'];
            $this->addReference($reference, $activityReason);
            static::$references[] = $reference;
        }

        $manager->flush();
    }
}
