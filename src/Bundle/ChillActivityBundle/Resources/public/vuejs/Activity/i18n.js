import { personMessages } from "ChillPersonAssets/vuejs/_js/i18n";
import { multiSelectMessages } from "ChillMainAssets/vuejs/_js/i18n";

const activityMessages = {
  fr: {
    activity: {
      //
      errors: "Le formulaire contient des erreurs",
      social_issues: "Problématiques sociales",
      choose_other_social_issue: "Ajouter une autre problématique sociale...",
      social_actions: "Actions d'accompagnement",
      select_first_a_social_issue:
        "Sélectionnez d'abord une problématique sociale",
      social_action_list_empty: "Aucune action sociale disponible",

      //
      add_persons: "Ajouter des personnes concernées",
      bloc_persons: "Usagers",
      bloc_persons_associated: "Usagers du parcours",
      bloc_persons_not_associated: "Tiers non-pro.",
      bloc_thirdparty: "Tiers professionnels",
      bloc_users: "T(M)S",

      //
      location: "Localisation",
      choose_location: "Choisissez une localisation",
      choose_location_type: "Choisissez un type de localisation",
      create_new_location: "Créer une nouvelle localisation",
      location_fields: {
        name: "Nom",
        type: "Type",
        phonenumber1: "Téléphone",
        phonenumber2: "Autre téléphone",
        email: "Adresse courriel",
      },
      create_address: "Créer une adresse",
      edit_address: "Modifier l'adresse",
    },
  },
};

Object.assign(activityMessages.fr, personMessages.fr, multiSelectMessages.fr);

export { activityMessages };
