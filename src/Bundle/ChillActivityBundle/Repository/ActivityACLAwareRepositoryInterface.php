<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Entity\ActivityType;
use Chill\MainBundle\Entity\UserJob;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

interface ActivityACLAwareRepositoryInterface
{
    /**
     * Return all the activities associated to an accompanying period and that the user is allowed to apply the given role.
     *
     * @param array{my_activities?: bool, types?: array<ActivityType>, jobs?: array<UserJob>, after?: \DateTimeImmutable|null, before?: \DateTimeImmutable|null} $filters
     *
     * @return array<Activity>
     */
    public function findByAccompanyingPeriod(AccompanyingPeriod $period, string $role, ?int $start = 0, ?int $limit = 1000, array $orderBy = ['date' => 'DESC'], array $filters = []): array;

    /**
     * @param array{my_activities?: bool, types?: array<ActivityType>, jobs?: array<UserJob>, after?: \DateTimeImmutable|null, before?: \DateTimeImmutable|null} $filters
     */
    public function countByAccompanyingPeriod(AccompanyingPeriod $period, string $role, array $filters = []): int;

    /**
     * @param array{my_activities?: bool, types?: array<ActivityType>, jobs?: array<UserJob>, after?: \DateTimeImmutable|null, before?: \DateTimeImmutable|null} $filters
     */
    public function countByPerson(Person $person, string $role, array $filters = []): int;

    /**
     * Return a list of activities, simplified as array (not object).
     *
     * The aim of this method is to get a long list of activities and keep performance.
     *
     * @return array an array of array, each item representing an activity
     */
    public function findByAccompanyingPeriodSimplified(AccompanyingPeriod $period, ?int $limit = 1000): array;

    /**
     * @param array{my_activities?: bool, types?: array<ActivityType>, jobs?: array<UserJob>, after?: \DateTimeImmutable|null, before?: \DateTimeImmutable|null} $filters
     *
     * @return array<Activity>
     */
    public function findByPerson(Person $person, string $role, ?int $start = 0, ?int $limit = 1000, array $orderBy = ['date' => 'DESC'], array $filters = []): array;

    /**
     * Return a list of the type for the activities associated to person or accompanying period.
     *
     * @return array<ActivityType>
     */
    public function findActivityTypeByAssociated(AccompanyingPeriod|Person $associated): array;

    /**
     * Return a list of the user job for the activities associated to person or accompanying period.
     *
     * Associated mean the job:
     * - of the creator;
     * - of the user (activity.user)
     * - of all the users
     *
     * @return array<UserJob>
     */
    public function findUserJobByAssociated(AccompanyingPeriod|Person $associated): array;
}
