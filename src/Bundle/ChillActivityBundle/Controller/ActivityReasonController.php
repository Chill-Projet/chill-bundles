<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Controller;

use Chill\ActivityBundle\Entity\ActivityReason;
use Chill\ActivityBundle\Form\ActivityReasonType;
use Chill\ActivityBundle\Repository\ActivityReasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ActivityReason controller.
 */
class ActivityReasonController extends AbstractController
{
    public function __construct(private readonly ActivityReasonRepository $activityReasonRepository, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry) {}

    /**
     * Creates a new ActivityReason entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/create', name: 'chill_activity_activityreason_create', methods: ['POST'])]
    public function createAction(Request $request)
    {
        $entity = new ActivityReason();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('chill_activity_activityreason', ['id' => $entity->getId()]);
        }

        return $this->render('@ChillActivity/ActivityReason/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing ActivityReason entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/{id}/edit', name: 'chill_activity_activityreason_edit')]
    public function editAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(ActivityReason::class)->find($id);

        if (null === $entity) {
            throw new NotFoundHttpException('Unable to find ActivityReason entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('@ChillActivity/ActivityReason/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Lists all ActivityReason entities.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/', name: 'chill_activity_activityreason')]
    public function indexAction()
    {
        $em = $this->managerRegistry->getManager();

        $entities = $this->activityReasonRepository->findAll();

        return $this->render('@ChillActivity/ActivityReason/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new ActivityReason entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/new', name: 'chill_activity_activityreason_new')]
    public function newAction()
    {
        $entity = new ActivityReason();
        $form = $this->createCreateForm($entity);

        return $this->render('@ChillActivity/ActivityReason/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a ActivityReason entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/{id}/show', name: 'chill_activity_activityreason_show')]
    public function showAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(ActivityReason::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityReason entity.');
        }

        return $this->render('@ChillActivity/ActivityReason/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Edits an existing ActivityReason entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/activityreason/{id}/update', name: 'chill_activity_activityreason_update', methods: ['POST', 'PUT'])]
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(ActivityReason::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityReason entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('chill_activity_activityreason', ['id' => $id]);
        }

        return $this->render('@ChillActivity/ActivityReason/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a ActivityReason entity.
     *
     * @param ActivityReason $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ActivityReason $entity)
    {
        $form = $this->createForm(ActivityReasonType::class, $entity, [
            'action' => $this->generateUrl('chill_activity_activityreason_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to edit a ActivityReason entity.
     *
     * @param ActivityReason $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ActivityReason $entity)
    {
        $form = $this->createForm(ActivityReasonType::class, $entity, [
            'action' => $this->generateUrl('chill_activity_activityreason_update', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
