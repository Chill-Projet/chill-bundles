<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Filter\ACPFilters\BySocialIssueFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class BySocialIssueFilterTest extends AbstractFilterTest
{
    private BySocialIssueFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get(BySocialIssueFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $array = $em->createQueryBuilder()
            ->from(SocialIssue::class, 'si')
            ->select('si')
            ->getQuery()
            ->setMaxResults(2)
            ->getResult();
        $data = [];
        foreach ($array as $a) {
            $data[] = [
                'accepted_socialissues' => new ArrayCollection([$a]),
            ];
        }

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        if (null === self::$kernel) {
            self::bootKernel();
        }
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity')
                ->join('activity.socialIssues', 'actsocialissue'),
        ];
    }
}
