<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Filter\CreatorJobFilter;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CreatorJobFilterTest extends AbstractFilterTest
{
    private EntityManagerInterface $entityManager;
    private TranslatableStringHelperInterface $translatableStringHelper;
    private TranslatorInterface $translator;
    private UserJobRepositoryInterface $userJobRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $this->translatableStringHelper = self::getContainer()->get(TranslatableStringHelperInterface::class);
        $this->translator = self::getContainer()->get(TranslatorInterface::class);
        $this->userJobRepository = self::getContainer()->get(UserJobRepositoryInterface::class);
    }

    public function getFilter()
    {
        return new CreatorJobFilter(
            $this->translatableStringHelper,
            $this->translator,
            $this->userJobRepository
        );
    }

    public static function getFormData(): array
    {
        $this->setUp();
        $jobs = $this->userJobRepository->findAll();

        return [
            ['jobs' => $jobs],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::setUp();

        return [
            $this->entityManager->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity')
                ->join('activity.user', 'actuser'),
        ];
    }
}
