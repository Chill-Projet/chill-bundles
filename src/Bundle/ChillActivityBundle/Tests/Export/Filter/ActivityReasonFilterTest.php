<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Export\Filter\PersonFilters\ActivityReasonFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @internal
 *
 * @coversNothing
 */
final class ActivityReasonFilterTest extends AbstractFilterTest
{
    use ProphecyTrait;

    private ActivityReasonFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.activity.export.reason_filter');

        $request = $this->prophesize()
            ->willExtend(\Symfony\Component\HttpFoundation\Request::class);

        $request->getLocale()->willReturn('fr');

        self::getContainer()->get(RequestStack::class)
            ->push($request->reveal());
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $data = [];
        $em = self::getContainer()
            ->get(EntityManagerInterface::class);
        $reasons = $em->createQuery('SELECT reason '
                .'FROM ChillActivityBundle:ActivityReason reason')
            ->getResult();
        // generate an array of 5 different combination of results
        for ($i = 0; 5 > $i; ++$i) {
            $data[] = ['reasons' => new ArrayCollection(array_splice($reasons, ($i + 1) * -1))];
            $data[] = ['reasons' => array_splice($reasons, ($i + 1) * -1)];
        }
        self::ensureKernelShutdown();

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from('ChillActivityBundle:Activity', 'activity');
        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from('ChillActivityBundle:Activity', 'activity')
            ->join('activity.reasons', 'reasons');
        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from('ChillActivityBundle:Activity', 'activity')
            ->join('activity.reasons', 'reasons')
            ->join('reasons.category', 'category');
        self::ensureKernelShutdown();
    }
}
