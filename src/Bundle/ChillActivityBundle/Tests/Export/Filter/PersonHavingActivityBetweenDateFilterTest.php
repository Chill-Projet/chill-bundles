<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Export\Filter\PersonFilters\PersonHavingActivityBetweenDateFilter;
use Chill\ActivityBundle\Repository\ActivityReasonRepository;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonHavingActivityBetweenDateFilterTest extends AbstractFilterTest
{
    private PersonHavingActivityBetweenDateFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.activity.export.person_having_an_activity_between_date_filter');

        $request = $this->prophesize()
            ->willExtend(Request::class);

        $request->getLocale()->willReturn('fr');

        self::getContainer()->get(RequestStack::class)
            ->push($request->reveal());
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        $date_from = \DateTime::createFromFormat('Y-m-d', '2015-01-15');
        $date_to = new \DateTime();
        // today
        $reasons = $this->getActivityReasons();
        $data = [];
        for ($i = 0; 4 > $i; ++$i) {
            $data[] = [
                'date_from' => $date_from,
                'date_to' => $date_to,
                'reasons' => \array_slice($reasons, 0, 1 + $i),
            ];
        }

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()
            ->get(EntityManagerInterface::class);
        yield $em->createQueryBuilder()
            ->select('count(person.id)')
            ->from('ChillPersonBundle:Person', 'person')
            // add a fake where clause
            ->where('person.id > 0');
        yield $em->createQueryBuilder()
            ->select('count(person.id)')
            ->from('ChillActivityBundle:Activity', 'activity')
            ->join('activity.person', 'person')
            // add a fake where clause
            ->where('person.id > 0');
        self::ensureKernelShutdown();
    }

    /**
     * Return all activity reasons.
     *
     * @return \Chill\ActivityBundle\Entity\ActivityReason[]
     */
    private function getActivityReasons()
    {
        self::bootKernel();

        $managerRegistry = self::getContainer()->get(ManagerRegistry::class);
        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        $repository = new ActivityReasonRepository($managerRegistry, $requestStack);

        return $repository->findAll();
    }
}
