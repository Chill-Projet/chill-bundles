<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Entity\ActivityType;
use Chill\ActivityBundle\Export\Filter\ActivityTypeFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ActivityTypeFilterTest extends AbstractFilterTest
{
    private ActivityTypeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.activity.export.type_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $array = $em->createQueryBuilder()
            ->from(ActivityType::class, 'at')
            ->select('at')
            ->getQuery()
            ->getResult();
        $data = [];
        foreach ($array as $a) {
            $data[] = [
                'types' => new ArrayCollection([$a]),
            ];
            /*$data[] = [
                'types' => [$a],
            ];*/
        }

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from(Activity::class, 'activity');
        self::ensureKernelShutdown();
    }
}
