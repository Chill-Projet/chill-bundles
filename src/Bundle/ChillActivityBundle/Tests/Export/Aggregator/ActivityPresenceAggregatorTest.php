<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Aggregator\ActivityPresenceAggregator;
use Chill\ActivityBundle\Repository\ActivityPresenceRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ActivityPresenceAggregatorTest extends AbstractAggregatorTest
{
    private TranslatableStringHelperInterface $translatableStringHelper;
    private ActivityPresenceRepositoryInterface $activityPresenceRepository;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->translatableStringHelper = self::getContainer()->get(TranslatableStringHelperInterface::class);
        $this->activityPresenceRepository = self::getContainer()->get(ActivityPresenceRepositoryInterface::class);
    }

    public function getAggregator()
    {
        return new ActivityPresenceAggregator($this->activityPresenceRepository, $this->translatableStringHelper);
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity'),
        ];
    }
}
