<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Aggregator\ActivityUserAggregator;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Add tests for ActivityUsernAggregator.
 *
 * @internal
 *
 * @coversNothing
 */
final class ActivityUserAggregatorTest extends AbstractAggregatorTest
{
    use ProphecyTrait;

    private ActivityUserAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::getContainer()->get('chill.activity.export.user_aggregator');

        $request = $this->prophesize()
            ->willExtend(\Symfony\Component\HttpFoundation\Request::class);

        $request->getLocale()->willReturn('fr');

        self::getContainer()->get('request_stack')
            ->push($request->reveal());
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity'),
        ];
    }
}
