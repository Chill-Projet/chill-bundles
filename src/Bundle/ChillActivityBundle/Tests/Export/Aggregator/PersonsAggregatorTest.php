<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Aggregator\PersonsAggregator;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class PersonsAggregatorTest extends AbstractAggregatorTest
{
    private LabelPersonHelper $labelPersonHelper;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->labelPersonHelper = self::getContainer()->get(LabelPersonHelper::class);
    }

    public function getAggregator()
    {
        return new PersonsAggregator($this->labelPersonHelper);
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity'),
        ];
    }
}
