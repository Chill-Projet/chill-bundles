<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator\ACPAggregators;

use Chill\ActivityBundle\Export\Aggregator\ACPAggregators\ByActivityTypeAggregator;
use Chill\ActivityBundle\Repository\ActivityTypeRepositoryInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ByActivityTypeAggregatorTest extends AbstractAggregatorTest
{
    private RollingDateConverterInterface $rollingDateConverter;
    private ActivityTypeRepositoryInterface $activityTypeRepository;
    private TranslatableStringHelperInterface $translatableStringHelper;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->rollingDateConverter = self::getContainer()->get(RollingDateConverterInterface::class);
        $this->activityTypeRepository = self::getContainer()->get(ActivityTypeRepositoryInterface::class);
        $this->translatableStringHelper = self::getContainer()->get(TranslatableStringHelperInterface::class);
    }

    public function getAggregator()
    {
        return new ByActivityTypeAggregator(
            $this->rollingDateConverter,
            $this->activityTypeRepository,
            $this->translatableStringHelper,
        );
    }

    public static function getFormData(): array
    {
        return [
            [
                'after_date' => null,
                'before_date' => null,
            ],
            [
                'after_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'before_date' => null,
            ],
            [
                'after_date' => null,
                'before_date' => new RollingDate(RollingDate::T_TODAY),
            ],
            [
                'after_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'before_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(distinct acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
