<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Export\LinkedToPerson\ListActivity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ListActivityTest extends AbstractExportTest
{
    use ProphecyTrait;

    private readonly ListActivity $export;

    protected function setUp(): void
    {
        self::bootKernel();

        $request = $this->prophesize()
            ->willExtend(\Symfony\Component\HttpFoundation\Request::class);

        $request->getLocale()->willReturn('fr');

        self::getContainer()->get('request_stack')
            ->push($request->reveal());
    }

    public function getExport()
    {
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $translator = self::getContainer()->get(TranslatorInterface::class);
        $translatableStringHelper = self::getContainer()->get(TranslatableStringHelperInterface::class);
        $activityRepository = self::getContainer()->get(ActivityRepository::class);

        yield new ListActivity(
            $em,
            $translator,
            $translatableStringHelper,
            $activityRepository,
            $this->getParameters(true)
        );

        yield new ListActivity(
            $em,
            $translator,
            $translatableStringHelper,
            $activityRepository,
            $this->getParameters(false)
        );
    }

    public static function getFormData(): array
    {
        return [
            ['fields' => [
                'id',
                'date',
                'durationTime',
                'attendee',
                'user_username',
                'circle_name',
                'type_name',
                'person_firstname',
                'person_lastname',
                'person_id',
            ]],
            ['fields' => [
                'id',
                'list_reasons',
            ]],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'person'],
        ];
    }
}
