<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Export\Export\LinkedToPerson\CountHouseholdOnActivity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;

/**
 * @internal
 *
 * @coversNothing
 */
class CountHouseholdOnActivityTest extends AbstractExportTest
{
    private ActivityRepository $activityRepository;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->activityRepository = self::getContainer()->get(ActivityRepository::class);
    }

    public function getExport()
    {
        yield new CountHouseholdOnActivity($this->activityRepository, $this->getParameters(true));
        yield new CountHouseholdOnActivity($this->activityRepository, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [
            [
                Declarations::ACTIVITY,
                Declarations::ACTIVITY_PERSON,
                PersonDeclarations::PERSON_TYPE,
                PersonDeclarations::HOUSEHOLD_TYPE,
            ],
        ];
    }
}
