<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Export\LinkedToPerson\CountActivity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Test\Export\AbstractExportTest;

/**
 * @internal
 *
 * @coversNothing
 */
final class CountActivityTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $activityRepository = self::getContainer()->get(ActivityRepository::class);

        yield new CountActivity($activityRepository, $this->getParameters(true));
        yield new CountActivity($activityRepository, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'person'],
        ];
    }
}
