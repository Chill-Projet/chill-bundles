<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Export\Export\LinkedToACP\SumActivityDuration;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class SumActivityDurationTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::getContainer()->get(EntityManagerInterface::class);

        yield new SumActivityDuration($em, $this->getParameters(true));
        yield new SumActivityDuration($em, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'accompanying_period'],
        ];
    }
}
