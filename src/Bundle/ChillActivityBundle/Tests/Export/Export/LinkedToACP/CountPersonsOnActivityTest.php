<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Export\Declarations;
use Chill\ActivityBundle\Export\Export\LinkedToACP\CountPersonsOnActivity;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations as PersonDeclarations;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CountPersonsOnActivityTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::getContainer()->get(EntityManagerInterface::class);

        yield new CountPersonsOnActivity($em, $this->getParameters(true));
        yield new CountPersonsOnActivity($em, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [[]];
    }

    public static function getModifiersCombination(): array
    {
        return [[
            Declarations::ACTIVITY,
            Declarations::ACTIVITY_ACP,
            PersonDeclarations::ACP_TYPE,
            PersonDeclarations::PERSON_TYPE,
        ]];
    }
}
