<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Form;

use Chill\ActivityBundle\Entity\ActivityTypeCategory;
use Chill\ActivityBundle\Form\Type\ActivityFieldPresence;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivityTypeType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TranslatableStringFormType::class)
            ->add('active', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false,
                ],
                'expanded' => true,
            ])
            ->add('category', EntityType::class, [
                'class' => ActivityTypeCategory::class,
                'choice_label' => fn (ActivityTypeCategory $activityTypeCategory) => $this->translatableStringHelper->localize($activityTypeCategory->getName()),
            ])
            ->add('ordering', NumberType::class, [
                'required' => true,
                'scale' => 5,
            ]);

        $fields = [
            'persons', 'user', 'date', 'location', 'persons',
            'thirdParties', 'durationTime', 'travelTime', 'attendee',
            'reasons', 'comment', 'privateComment', 'sentReceived', 'documents',
            'emergency', 'socialIssues', 'socialActions', 'users',
        ];

        foreach ($fields as $field) {
            $builder
                ->add($field.'Visible', ActivityFieldPresence::class)
                ->add($field.'Label', TextType::class, [
                    'required' => false,
                    'empty_data' => '',
                ]);
        }

        $builder
            ->add('commentVisible', ActivityFieldPresence::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \Chill\ActivityBundle\Entity\ActivityType::class,
        ]);
    }
}
