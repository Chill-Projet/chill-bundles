<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Test;

use Chill\ActivityBundle\Entity\Activity;
use Chill\MainBundle\Entity\Scope;
use Chill\PersonBundle\Entity\Person;

/**
 * Prepare entities useful in tests.
 */
trait PrepareActivityTrait
{
    /**
     * Return an activity with a scope and a person inside.
     *
     * @return Activity
     */
    public function prepareActivity(Scope $scope, Person $person)
    {
        return (new Activity())
            ->setScope($scope)
            ->setPerson($person);
    }
}
