<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\AccompanyingPeriod;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
class AccompanyingPeriodValidity extends Constraint
{
    public $messageSocialIssueCannotBeDeleted = 'The social %name% issue cannot be deleted because it is associated with an activity or an action';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
