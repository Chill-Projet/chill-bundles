<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\Household;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
class MaxHolder extends Constraint
{
    public $message = 'household.max_holder_overflowed';

    public $messageInfinity = 'household.max_holder_overflowed_infinity';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
