<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\Relationship;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
class RelationshipNoDuplicate extends Constraint
{
    public $message = 'relationship.duplicate';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
