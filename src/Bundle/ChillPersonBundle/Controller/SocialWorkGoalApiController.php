<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Serializer\Model\Collection;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Repository\SocialWork\GoalRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SocialWorkGoalApiController extends ApiController
{
    public function __construct(private readonly GoalRepository $goalRepository, private readonly PaginatorFactory $paginator) {}

    public function listBySocialAction(Request $request, SocialAction $action): Response
    {
        $totalItems = $this->goalRepository->countBySocialActionWithDescendants($action);
        $paginator = $this->getPaginatorFactory()->create($totalItems);

        $entities = $this->goalRepository->findBySocialActionWithDescendants(
            $action,
            ['id' => 'ASC'],
            $paginator->getItemsPerPage(),
            $paginator->getCurrentPageFirstItemNumber()
        );

        $model = new Collection($entities, $paginator);

        return $this->json($model, Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
