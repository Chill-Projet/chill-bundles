<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Service\SocialWork\SocialActionCSVExportService;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\UnavailableStream;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class SocialActionCSVExportController extends AbstractController
{
    public function __construct(
        private readonly SocialActionRepository $socialActionRepository,
        private readonly Security $security,
        private readonly SocialActionCSVExportService $socialActionCSVExportService,
    ) {}

    /**
     * @throws UnavailableStream
     * @throws CannotInsertRecord
     * @throws Exception
     */
    #[Route(path: '/{_locale}/admin/social-work/social-action/export/list.{_format}', name: 'chill_person_social_action_export_list', requirements: ['_format' => 'csv'])]
    public function socialActionList(Request $request, string $_format = 'csv'): StreamedResponse
    {
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Only ROLE_ADMIN can export this list');
        }

        $actions = $this->socialActionRepository->findAllOrdered();

        $csv = $this->socialActionCSVExportService->generateCsv($actions);

        return new StreamedResponse(
            function () use ($csv) {
                foreach ($csv->chunk(1024) as $chunk) {
                    echo $chunk;
                    flush();
                }
            },
            Response::HTTP_OK,
            [
                'Content-Encoding' => 'none',
                'Content-Type' => 'text/csv; charset=UTF-8',
                'Content-Disposition' => 'attachment; filename=results.csv',
            ]
        );
    }
}
