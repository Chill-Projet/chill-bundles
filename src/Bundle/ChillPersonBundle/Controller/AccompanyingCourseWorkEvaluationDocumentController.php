<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class AccompanyingCourseWorkEvaluationDocumentController extends AbstractController
{
    public function __construct(private readonly Security $security) {}

    #[Route(path: '{_locale}/person/accompanying-period/work/evaluation/document/{id}/show', name: 'chill_person_accompanying_period_work_evaluation_document_show', methods: ['GET'])]
    public function showAccompanyingCourseWork(AccompanyingPeriodWorkEvaluationDocument $document): Response
    {
        $work = $document->getAccompanyingPeriodWorkEvaluation()->getAccompanyingPeriodWork();

        return $this->redirectToRoute(
            $this->security->isGranted(AccompanyingPeriodWorkVoter::UPDATE, $work) ?
                'chill_person_accompanying_period_work_edit' : 'chill_person_accompanying_period_work_show',
            [
                'id' => $work->getId(),
            ]
        );
    }
}
