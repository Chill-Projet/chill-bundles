<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Form\HouseholdMemberType;
use Chill\PersonBundle\Household\MembersEditor;
use Chill\PersonBundle\Repository\AccompanyingPeriodRepository;
use Chill\PersonBundle\Repository\Household\HouseholdRepository;
use Chill\PersonBundle\Repository\Household\PositionRepository;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Security\Authorization\HouseholdVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Exception;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HouseholdMemberController extends ApiController
{
    private array $household_fields_visibility;

    public function __construct(
        private readonly UrlGeneratorInterface $generator,
        private readonly TranslatorInterface $translator,
        private readonly AccompanyingPeriodRepository $periodRepository,
        private readonly PersonRepository $personRepository,
        private readonly HouseholdRepository $householdRepository,
        private readonly Security $security,
        private readonly PositionRepository $positionRepository,
        private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry,
        protected ParameterBagInterface $parameterBag,
    ) {
        $this->household_fields_visibility = $parameterBag->get('chill_person.household_fields');
    }

    #[Route(path: '/{_locale}/person/household/member/{id}/edit', name: 'chill_person_household_member_edit')]
    public function editMembership(Request $request, HouseholdMember $member): Response
    {
        // TODO ACL

        $form = $this->createForm(HouseholdMemberType::class, $member, [
            'validation_groups' => ['household_memberships'],
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            $this->addFlash('success', $this->translator
                ->trans('household.successfully saved member'));

            return $this->redirect(
                $request->get('returnPath', null) ??
                $this->generator->generate('chill_person_household_summary', ['household_id' => $member->getHousehold()->getId()])
            );
        }

        return $this->render('@ChillPerson/Household/Member/edit.html.twig', [
            'household' => $member->getHousehold(),
            'member' => $member,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Route for showing an editor to leave a household.
     *
     * Possibles arguments are:
     *
     * * persons[]: an id of the person to add to the form
     * * household: the id of the destination household
     * * allow_leave_without_household: if present, the editor will allow
     *     to leave household without joining another
     */
    #[Route(path: '/{_locale}/person/household/members/editor', name: 'chill_person_household_members_editor')]
    public function editor(Request $request)
    {
        $ids = $request->query->all('persons');

        if ([] !== $ids) {
            $persons = [];

            foreach ($ids as $id) {
                if (!is_numeric($id)) {
                    throw new BadRequestHttpException(sprintf('persons with id %s is not numeric', $id));
                }

                $person = $this->personRepository->find((int) $id);

                if (null === $person) {
                    throw new NotFoundHttpException(sprintf('person with id %d not found', $id));
                }

                $this->denyAccessUnlessGranted(
                    PersonVoter::SEE,
                    $person,
                    "You are not allowed to see person with id {$person->getId()}"
                );

                $persons[] = $person;
            }
        }

        if ($request->query->has('household')) {
            $householdId = $request->query->get('household', false);
            $household = $this->householdRepository
                ->find($householdId);
            $allowHouseholdCreate = false;
            $allowHouseholdSearch = false;
            $allowLeaveWithoutHousehold = false;

            if (null === $household) {
                throw new NotFoundHttpException('household not found');
            }

            if (!$this->security->isGranted(HouseholdVoter::EDIT, $household)) {
                throw new AccessDeniedHttpException('not allowed to edit this household');
            }
        }

        $positions = $this->positionRepository
            ->findAll();

        $data = [
            'persons' => $persons ?? false ?
                $this->getSerializer()->normalize($persons, 'json', ['groups' => ['read']]) : [],
            'household' => $household ?? false ?
                $this->getSerializer()->normalize($household, 'json', ['groups' => ['read']]) : null,
            'positions' => $this->getSerializer()->normalize($positions, 'json', ['groups' => ['read']]),
            'allowHouseholdCreate' => $allowHouseholdCreate ?? true,
            'allowHouseholdSearch' => $allowHouseholdSearch ?? true,
            'allowLeaveWithoutHousehold' => $allowLeaveWithoutHousehold ?? $request->query->has('allow_leave_without_household'),
            'displayDependents' => ('visible' == $this->household_fields_visibility['number_of_dependents']) ? true : false,
        ];

        // context
        if ($request->query->has('accompanying_period_id')) {
            $period = $this->periodRepository->find(
                $request->query->getInt('accompanying_period_id')
            );

            if (null === $period) {
                throw new NotFoundHttpException('accompanying period not found');
            }
        }

        return $this->render('@ChillPerson/Household/members_editor.html.twig', [
            'data' => $data,
            'expandSuggestions' => (int) $request->query->getBoolean('expand_suggestions', false),
            'accompanyingCourse' => $period ?? null,
        ]);
    }

    #[Route(path: '/api/1.0/person/household/members/move.{_format}', name: 'chill_api_person_household_members_move')]
    public function move(Request $request, mixed $_format): Response
    {
        try {
            /** @var MembersEditor $editor */
            $editor = $this->getSerializer()
                ->deserialize(
                    $request->getContent(),
                    MembersEditor::class,
                    $_format,
                    ['groups' => ['read']]
                );
        } catch (Exception\InvalidArgumentException|Exception\UnexpectedValueException $e) {
            throw new BadRequestHttpException("Deserialization error: {$e->getMessage()}", 45896, $e);
        }

        // TODO ACL
        //
        $errors = $editor->validate();

        if (\count($errors) > 0) {
            return $this->json($errors, 422);
        }

        // launch events on post move
        $editor->postMove();

        $em = $this->managerRegistry->getManager();

        // if new household, persist it
        if (
            $editor->hasHousehold()
            && false === $em->contains($editor->getHousehold())
        ) {
            $em->persist($editor->getHousehold());
        }

        foreach ($editor->getPersistable() as $el) {
            $em->persist($el);
        }
        $em->flush();

        return $this->json($editor->getHousehold(), Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
