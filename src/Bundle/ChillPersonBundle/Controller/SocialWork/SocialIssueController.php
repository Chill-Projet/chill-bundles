<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller\SocialWork;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class SocialIssueController extends CRUDController
{
    protected function createFormFor(string $action, $entity, ?string $formClass = null, array $formOptions = []): FormInterface
    {
        if ('new' === $action) {
            return parent::createFormFor($action, $entity, $formClass, ['step' => 'create']);
        }

        if ('edit' === $action) {
            return parent::createFormFor($action, $entity, $formClass, ['step' => 'edit']);
        }

        throw new \LogicException('action is not supported: '.$action);
    }

    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        $query->addOrderBy('e.ordering', 'ASC');

        return parent::orderQuery($action, $query, $request, $paginator);
    }
}
