<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController.
 */
class AdminController extends AbstractController
{
    #[Route(path: '/{_locale}/admin/accompanying-course', name: 'chill_accompanying-course_admin_index')]
    public function indexAccompanyingCourseAdminAction()
    {
        return $this->render('@ChillPerson/Admin/indexAccompanyingCourse.html.twig');
    }

    #[Route(path: '/{_locale}/admin/household', name: 'chill_household_admin_index')]
    public function indexHouseholdAdminAction()
    {
        return $this->render('@ChillPerson/Admin/indexHousehold.html.twig');
    }

    #[Route(path: '/{_locale}/admin/person', name: 'chill_person_admin_index')]
    public function indexPersonAdminAction()
    {
        return $this->render('@ChillPerson/Admin/indexPerson.html.twig');
    }

    #[Route(path: '/{_locale}/admin/social-work', name: 'chill_social-work_admin_index')]
    public function indexSocialWorkAdminAction()
    {
        return $this->render('@ChillPerson/Admin/indexSocialWork.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    #[Route(path: '/{_locale}/admin/person_redirect_to_main', name: 'chill_person_admin_redirect_to_admin_index', options: [null])]
    public function redirectToAdminIndexAction()
    {
        return $this->redirectToRoute('chill_main_admin_central');
    }
}
