<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle;

use Chill\PersonBundle\Actions\Remove\PersonMoveSqlHandlerInterface;
use Chill\PersonBundle\DependencyInjection\CompilerPass\AccompanyingPeriodTimelineCompilerPass;
use Chill\PersonBundle\Export\Helper\CustomizeListPersonHelperInterface;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;
use Chill\PersonBundle\Widget\PersonListWidgetFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ChillPersonBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->getExtension('chill_main')
            ->addWidgetFactory(new PersonListWidgetFactory());

        $container->addCompilerPass(new AccompanyingPeriodTimelineCompilerPass(), \Symfony\Component\DependencyInjection\Compiler\PassConfig::TYPE_BEFORE_OPTIMIZATION, 0);
        $container->registerForAutoconfiguration(AccompanyingPeriodInfoUnionQueryPartInterface::class)
            ->addTag('chill_person.accompanying_period_info_part');
        $container->registerForAutoconfiguration(PersonMoveSqlHandlerInterface::class)
            ->addTag('chill_person.person_move_handler');
        $container->registerForAutoconfiguration(CustomizeListPersonHelperInterface::class)
            ->addTag('chill_person.list_person_customizer');
    }
}
