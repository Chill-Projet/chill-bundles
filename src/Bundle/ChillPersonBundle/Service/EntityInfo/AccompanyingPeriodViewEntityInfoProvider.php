<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo;

use Chill\MainBundle\Service\EntityInfo\ViewEntityInfoProviderInterface;

class AccompanyingPeriodViewEntityInfoProvider implements ViewEntityInfoProviderInterface
{
    public function __construct(
        /**
         * @var AccompanyingPeriodInfoUnionQueryPartInterface[]
         */
        private readonly iterable $unions,
        private readonly AccompanyingPeriodInfoQueryBuilder $builder,
    ) {}

    public function getViewQuery(): string
    {
        return implode(
            ' UNION ',
            array_map(
                fn (AccompanyingPeriodInfoUnionQueryPartInterface $part) => $this->builder->buildQuery($part),
                iterator_to_array($this->unions)
            )
        );
    }

    public function getViewName(): string
    {
        return 'view_chill_person_accompanying_period_info';
    }
}
