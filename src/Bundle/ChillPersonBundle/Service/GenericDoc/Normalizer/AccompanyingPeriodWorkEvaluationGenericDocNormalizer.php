<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\GenericDoc\Normalizer;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\GenericDocNormalizerInterface;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Chill\PersonBundle\Service\GenericDoc\Providers\AccompanyingPeriodWorkEvaluationGenericDocProvider;
use Chill\PersonBundle\Service\GenericDoc\Renderer\AccompanyingPeriodWorkEvaluationGenericDocRenderer;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

final readonly class AccompanyingPeriodWorkEvaluationGenericDocNormalizer implements GenericDocNormalizerInterface
{
    public function __construct(
        private AccompanyingPeriodWorkEvaluationDocumentRepository $accompanyingPeriodWorkEvaluationDocumentRepository,
        private Environment $twig,
        private AccompanyingPeriodWorkEvaluationGenericDocRenderer $renderer,
        private TranslatorInterface $translator,
    ) {}

    public function supportsNormalization(GenericDocDTO $genericDocDTO, string $format, array $context = []): bool
    {
        return AccompanyingPeriodWorkEvaluationGenericDocProvider::KEY === $genericDocDTO->key;
    }

    public function normalize(GenericDocDTO $genericDocDTO, string $format, array $context = []): array
    {
        if (null === $evaluationDoc = $this->accompanyingPeriodWorkEvaluationDocumentRepository->find($genericDocDTO->identifiers['id'])) {
            return ['title' => $this->translator->trans('generic_doc.document removed'), 'isPresent' => false];
        }

        return [
            'title' => $evaluationDoc->getTitle(),
            'html' => $this->twig->render(
                $this->renderer->getTemplate($genericDocDTO, ['show-actions' => false, 'row-only' => true]),
                $this->renderer->getTemplateData($genericDocDTO, ['show-actions' => false, 'row-only' => true])
            ),
            'isPresent' => true,
        ];
    }
}
