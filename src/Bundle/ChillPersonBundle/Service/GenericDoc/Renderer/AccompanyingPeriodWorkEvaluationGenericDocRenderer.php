<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\GenericDoc\Renderer;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\Twig\GenericDocRendererInterface;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Chill\PersonBundle\Service\GenericDoc\Providers\AccompanyingPeriodWorkEvaluationGenericDocProvider;

/**
 * @implements GenericDocRendererInterface<array{row-only?: bool, show-actions?: bool}>
 */
final readonly class AccompanyingPeriodWorkEvaluationGenericDocRenderer implements GenericDocRendererInterface
{
    public function __construct(
        private AccompanyingPeriodWorkEvaluationDocumentRepository $accompanyingPeriodWorkEvaluationDocumentRepository,
    ) {}

    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluationGenericDocProvider::KEY === $genericDocDTO->key;
    }

    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string
    {
        return ($options['row-only'] ?? false) ? '@ChillPerson/GenericDoc/evaluation_document_row.html.twig'
            : '@ChillPerson/GenericDoc/evaluation_document.html.twig';
    }

    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array
    {
        return [
            'document' => $this->accompanyingPeriodWorkEvaluationDocumentRepository->find($genericDocDTO->identifiers['id']),
            'context' => $genericDocDTO->getContext(),
            'show_actions' => $options['show-actions'] ?? true,
        ];
    }
}
