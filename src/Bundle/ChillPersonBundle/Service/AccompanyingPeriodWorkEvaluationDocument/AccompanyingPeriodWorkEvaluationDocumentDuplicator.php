<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\AccompanyingPeriodWorkEvaluationDocument;

use Chill\DocStoreBundle\Service\StoredObjectDuplicate;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Symfony\Component\Clock\ClockInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccompanyingPeriodWorkEvaluationDocumentDuplicator
{
    public function __construct(
        private readonly StoredObjectDuplicate $storedObjectDuplicate,
        private readonly TranslatorInterface $translator,
        private readonly ClockInterface $clock,
    ) {}

    public function duplicate(AccompanyingPeriodWorkEvaluationDocument $document): AccompanyingPeriodWorkEvaluationDocument
    {
        $newDocument = new AccompanyingPeriodWorkEvaluationDocument();
        $newDocument
            ->setTitle($document->getTitle().' ('.$this->translator->trans('accompanying_course_evaluation_document.duplicated_at', ['at' => $this->clock->now()]).')')
            ->setStoredObject($this->storedObjectDuplicate->duplicate($document->getStoredObject()))
        ;

        $document->getAccompanyingPeriodWorkEvaluation()->addDocument($newDocument);

        return $newDocument;
    }
}
