<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\AccompanyingPeriodWork;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvidePersonsAssociated as ProvidePersonsAssociatedAccompanyingPeriod;

/**
 * Provide persons associated with an accompanying period work.
 */
class ProvidePersonsAssociated
{
    public function __construct(private readonly ProvidePersonsAssociatedAccompanyingPeriod $providePersonsAssociated) {}

    /**
     * @return list<Person>
     */
    public function getPersonsAssociated(AccompanyingPeriod\AccompanyingPeriodWork $work): array
    {
        return $this->providePersonsAssociated->getPersonsAssociated($work->getAccompanyingPeriod());
    }
}
