<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\DocGenerator;

use Chill\DocGeneratorBundle\Context\DocGeneratorContextWithAdminFormInterface;
use Chill\DocGeneratorBundle\Context\DocGeneratorContextWithPublicFormInterface;
use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @template-extends DocGeneratorContextWithPublicFormInterface<Person>
 * @template-extends DocGeneratorContextWithAdminFormInterface<Person>
 */
interface PersonContextInterface extends DocGeneratorContextWithAdminFormInterface, DocGeneratorContextWithPublicFormInterface
{
    public function adminFormReverseTransform(array $data): array;

    public function adminFormTransform(array $data): array;

    public function buildAdminForm(FormBuilderInterface $builder): void;

    public function buildPublicForm(FormBuilderInterface $builder, DocGeneratorTemplate $template, mixed $entity): void;

    public function getData(DocGeneratorTemplate $template, $entity, array $contextGenerationData = []): array;

    public function getDescription(): string;

    public function getEntityClass(): string;

    public function getFormData(DocGeneratorTemplate $template, mixed $entity): array;

    public function getName(): string;

    public function hasAdminForm(): bool;

    /**
     * @param Person $entity
     */
    public function hasPublicForm(DocGeneratorTemplate $template, $entity): bool;

    public function contextGenerationDataNormalize(DocGeneratorTemplate $template, $entity, array $data): array;

    public function contextGenerationDataDenormalize(DocGeneratorTemplate $template, $entity, array $data): array;

    public function storeGenerated(DocGeneratorTemplate $template, StoredObject $storedObject, object $entity, array $contextGenerationData): void;
}
