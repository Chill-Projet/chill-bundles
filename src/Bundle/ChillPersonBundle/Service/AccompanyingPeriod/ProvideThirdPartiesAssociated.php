<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriod\Resource;
use Chill\ThirdPartyBundle\Entity\ThirdParty;

/**
 * Provide third parties participating to an AccompanyingPeriod.
 */
class ProvideThirdPartiesAssociated
{
    /**
     * @return list<ThirdParty>
     */
    public function getThirdPartiesAssociated(AccompanyingPeriod $period): array
    {
        $thirdParties = [];

        foreach (
            $period
                ->getResources()->filter(fn (Resource $resource) => null !== $resource->getThirdParty())
                ->map(fn (Resource $resource) => $resource->getThirdParty()) as $thirdParty) {
            $thirdParties[] = $thirdParty;
        }

        if (null !== $requestor = $period->getRequestorThirdParty()) {
            $thirdParties[] = $requestor;
        }

        return array_values(
            // filter objects to remove duplicates
            array_filter(
                $thirdParties,
                fn ($o, $k) => array_search($o, $thirdParties, true) === $k,
                ARRAY_FILTER_USE_BOTH
            )
        );
    }
}
