<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Chill\PersonBundle\Entity\Person;

/**
 * Provide persons associated with an accompanying period.
 */
class ProvidePersonsAssociated
{
    /**
     * @return list<Person>
     */
    public function getPersonsAssociated(AccompanyingPeriod $period): array
    {
        return array_values(
            $period->getCurrentParticipations()
                ->map(fn (AccompanyingPeriodParticipation $participation) => $participation->getPerson())
                ->toArray()
        );
    }
}
