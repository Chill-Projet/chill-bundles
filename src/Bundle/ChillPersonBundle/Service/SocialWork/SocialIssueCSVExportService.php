<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\UnavailableStream;
use League\Csv\Writer;
use Symfony\Contracts\Translation\TranslatorInterface;

readonly class SocialIssueCSVExportService
{
    public function __construct(
        private SocialIssueRender $socialIssueRender,
        private TranslatableStringHelperInterface $stringHelper,
        private TranslatorInterface $translator,
    ) {}

    /**
     * @throws UnavailableStream
     * @throws CannotInsertRecord
     * @throws Exception
     */
    public function generateCsv(array $issues): Writer
    {
        // CSV headers
        $csv = Writer::createFromPath('php://temp', 'r+');
        $csv->insertOne(
            array_map(
                fn (string $e) => $this->translator->trans($e),
                [
                    'Id',
                    'Label',
                    'Social issue',
                    'socialIssue.isParent?',
                    'socialIssue.Parent id',
                ]
            )
        );

        foreach ($issues as $issue) {
            $csv->insertOne($this->formatRow($issue));
        }


        return $csv;
    }

    private function formatRow(
        SocialIssue $issue,
    ): array {
        return
            [
                'id' => $issue->getId(),
                'label' => $this->stringHelper->localize($issue->getTitle()),
                'title' => $this->socialIssueRender->renderString($issue, []),
                'isParent' => $issue->hasChildren() ? 'X' : '',
                'parent_id' => null !== $issue->getParent() ? $issue->getParent()->getId() : '',
            ];
    }
}
