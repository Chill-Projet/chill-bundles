<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\EventListener;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Symfony\Component\Security\Core\Security;

class AccompanyingPeriodWorkEventListener
{
    public function __construct(private readonly Security $security) {}

    public function prePersistAccompanyingPeriodWork(AccompanyingPeriodWork $work): void
    {
        if ($this->security->getUser() instanceof User) {
            $work->addReferrer($this->security->getUser());
        }
    }
}
