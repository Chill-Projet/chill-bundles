<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\EventListener;

use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\PersonAltName;

class PersonEventListener
{
    public function prePersistAltName(PersonAltName $altname)
    {
        $altnameCaps = mb_strtoupper($altname->getLabel(), 'UTF-8');
        $altname->setLabel($altnameCaps);
    }

    public function prePersistPerson(Person $person): void
    {
        $firstnameCaps = mb_convert_case(mb_strtolower($person->getFirstName()), \MB_CASE_TITLE, 'UTF-8');
        $firstnameCaps = ucwords(strtolower($firstnameCaps), " \t\r\n\f\v'-");
        $person->setFirstName($firstnameCaps);

        $lastnameCaps = mb_strtoupper($person->getLastName(), 'UTF-8');
        $person->setLastName($lastnameCaps);
    }
}
