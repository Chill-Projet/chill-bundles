<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminHouseholdMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Household', [
            'route' => 'chill_household_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 2100,
            ]);

        $menu->addChild('Position', [
            'route' => 'chill_crud_person_household_position_index',
        ])->setExtras(['order' => 2110]);

        $menu->addChild('Composition', [
            'route' => 'chill_crud_person_household_composition_type_index',
        ])->setExtras(['order' => 2120]);

        $menu->addChild('person_admin.relation', [
            'route' => 'chill_crud_person_relation_index',
        ])->setExtras(['order' => 2130]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_household'];
    }
}
