<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final readonly class PersonQuickMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private AuthorizationCheckerInterface $authorizationChecker) {}

    public static function getMenuIds(): array
    {
        return ['person_quick_menu'];
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var \Chill\PersonBundle\Entity\Person $person */
        $person = $parameters['person'];

        if ($this->authorizationChecker->isGranted(AccompanyingPeriodVoter::CREATE, $person)) {
            $menu->addChild(
                'Create Accompanying Course',
                [
                    'route' => 'chill_person_accompanying_course_new',
                    'routeParameters' => [
                        'person_id' => [$person->getId()],
                    ],
                ]
            )
                ->setExtras([
                    'order' => 10,
                    'icon' => 'plus',
                ]);
        }
    }
}
