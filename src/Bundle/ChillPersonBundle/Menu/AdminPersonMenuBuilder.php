<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminPersonMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    private array $fields_visibility;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        ParameterBagInterface $parameterBag,
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->fields_visibility = $parameterBag->get('chill_person.person_fields');
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Person', [
            'route' => 'chill_person_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 2000,
            ]);

        $menu->addChild('Civility', [
            'route' => 'chill_crud_main_civility_index',
        ])->setExtras(['order' => 2010]);

        $menu->addChild('Gender', [
            'route' => 'chill_crud_main_gender_index',
        ])->setExtras(['order' => 2020]);

        $menu->addChild('Marital status', [
            'route' => 'chill_crud_person_marital-status_index',
        ])->setExtras(['order' => 2030]);

        if ('visible' == $this->fields_visibility['employment_status']) {
            $menu->addChild('Employment status', [
                'route' => 'chill_crud_employment_status_index',
            ])->setExtras(['order' => 2035]);
        }

        if ('visible' == $this->fields_visibility['administrative_status']) {
            $menu->addChild('Administrative status', [
                'route' => 'chill_crud_administrative_status_index',
            ])->setExtras(['order' => 2036]);
        }

        $menu->addChild('person_admin.person_resource_kind', [
            'route' => 'chill_crud_person_resource-kind_index',
        ])->setExtras(['order' => 2040]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_person'];
    }
}
