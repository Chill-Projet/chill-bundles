<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\CenterResolver;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Security\Resolver\CenterResolverInterface;
use Chill\MainBundle\Security\Resolver\ManagerAwareCenterResolverInterface;
use Chill\MainBundle\Security\Resolver\ManagerAwareCenterResolverTrait;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;

final class AccompanyingPeriodWorkEvaluationDocumentCenterResolver implements CenterResolverInterface, ManagerAwareCenterResolverInterface
{
    use ManagerAwareCenterResolverTrait;

    public static function getDefaultPriority(): int
    {
        return 0;
    }

    public function resolveCenter($entity, ?array $options = []): Center|array
    {
        /* @var $entity AccompanyingPeriodWorkEvaluationDocument */
        return $this->centerResolverManager->resolveCenters($entity->getAccompanyingPeriodWorkEvaluation()
            ->getAccompanyingPeriodWork()->getAccompanyingPeriod(), $options);
    }

    public function supports($entity, ?array $options = []): bool
    {
        return $entity instanceof AccompanyingPeriodWorkEvaluationDocument;
    }
}
