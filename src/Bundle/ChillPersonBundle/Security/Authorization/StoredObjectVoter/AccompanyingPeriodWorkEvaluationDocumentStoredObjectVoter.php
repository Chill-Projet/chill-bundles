<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\Authorization\StoredObjectVoter;

use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectVoter\AbstractStoredObjectVoter;
use Chill\MainBundle\Workflow\Helper\WorkflowRelatedEntityPermissionHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkEvaluationDocumentVoter;
use Symfony\Component\Security\Core\Security;

class AccompanyingPeriodWorkEvaluationDocumentStoredObjectVoter extends AbstractStoredObjectVoter
{
    public function __construct(
        private readonly AccompanyingPeriodWorkEvaluationDocumentRepository $repository,
        Security $security,
        WorkflowRelatedEntityPermissionHelper $workflowDocumentService,
    ) {
        parent::__construct($security, $workflowDocumentService);
    }

    protected function getRepository(): AssociatedEntityToStoredObjectInterface
    {
        return $this->repository;
    }

    protected function getClass(): string
    {
        return AccompanyingPeriodWorkEvaluationDocument::class;
    }

    protected function attributeToRole(StoredObjectRoleEnum $attribute): string
    {
        return match ($attribute) {
            StoredObjectRoleEnum::SEE => AccompanyingPeriodWorkEvaluationDocumentVoter::SEE,
            StoredObjectRoleEnum::EDIT => AccompanyingPeriodWorkEvaluationDocumentVoter::SEE_AND_EDIT,
        };
    }

    protected function canBeAssociatedWithWorkflow(): bool
    {
        return true;
    }
}
