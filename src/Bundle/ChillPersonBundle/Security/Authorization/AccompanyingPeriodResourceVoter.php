<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\Authorization;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Resource;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccompanyingPeriodResourceVoter extends Voter
{
    final public const EDIT = 'CHILL_PERSON_ACCOMPANYING_PERIOD_RESOURCE_EDIT';

    public function __construct(private readonly AccessDecisionManagerInterface $accessDecisionManager) {}

    protected function supports($attribute, $subject)
    {
        return $subject instanceof Resource && self::EDIT === $attribute;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return match ($attribute) {
            self::EDIT => $this->accessDecisionManager->decide(
                $token,
                [AccompanyingPeriodVoter::EDIT],
                $subject->getAccompanyingPeriod()
            ),
            default => throw new \UnexpectedValueException("attribute not supported: {$attribute}"),
        };
    }
}
