<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\ChillVoterInterface;
use Chill\MainBundle\Security\Authorization\VoterHelperFactoryInterface;
use Chill\MainBundle\Security\Authorization\VoterHelperInterface;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AccompanyingPeriodWorkVoter extends Voter implements ProvideRoleHierarchyInterface, ChillVoterInterface
{
    final public const ALL = [
        self::SEE,
        self::CREATE,
        self::UPDATE,
        self::DELETE,
    ];

    final public const CREATE = 'CHILL_MAIN_ACCOMPANYING_PERIOD_WORK_CREATE';

    final public const DELETE = 'CHILL_MAIN_ACCOMPANYING_PERIOD_WORK_DELETE';

    final public const SEE = 'CHILL_MAIN_ACCOMPANYING_PERIOD_WORK_SEE';

    final public const UPDATE = 'CHILL_MAIN_ACCOMPANYING_PERIOD_WORK_UPDATE';

    private readonly VoterHelperInterface $voterHelper;

    public function __construct(
        private readonly Security $security,
        VoterHelperFactoryInterface $voterHelperFactory,
    ) {
        $this->voterHelper = $voterHelperFactory
            ->generate(self::class)
            ->addCheckFor(null, [self::CREATE])
            ->addCheckFor(AccompanyingPeriod::class, [self::ALL])
            ->addCheckFor(Person::class, [self::SEE, self::CREATE])
            ->build();
    }

    public function getRoles(): array
    {
        return [
            self::SEE,
            self::CREATE,
            self::UPDATE,
            self::DELETE,
        ];
    }

    public function getRolesWithHierarchy(): array
    {
        return ['Social actions' => $this->getRoles()];
    }

    public function getRolesWithoutScope(): array
    {
        return [];
    }

    protected function supports($attribute, $subject): bool
    {
        return
            (
                $subject instanceof AccompanyingPeriodWork
                && \in_array($attribute, $this->getRoles(), true)
            ) || (
                $subject instanceof AccompanyingPeriod
                && \in_array($attribute, [self::SEE, self::CREATE], true)
            );
    }

    /**
     * @param string                 $attribute
     * @param AccompanyingPeriodWork $subject
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if ($subject instanceof AccompanyingPeriodWork) {
            return match ($attribute) {
                self::SEE => $this->security->isGranted(AccompanyingPeriodVoter::SEE_DETAILS, $subject->getAccompanyingPeriod()),
                self::CREATE, self::UPDATE, self::DELETE => $this->security->isGranted(AccompanyingPeriodVoter::EDIT, $subject->getAccompanyingPeriod()),
                default => throw new \UnexpectedValueException("attribute {$attribute} is not supported"),
            };
        } elseif ($subject instanceof AccompanyingPeriod) {
            return match ($attribute) {
                self::SEE => $this->security->isGranted(AccompanyingPeriodVoter::SEE_DETAILS, $subject),
                self::CREATE => $this->security->isGranted(AccompanyingPeriodVoter::CREATE, $subject),
                default => throw new \UnexpectedValueException(sprintf("attribute {$attribute} is not supported on instance %s", AccompanyingPeriod::class)),
            };
        }

        throw new \UnexpectedValueException(sprintf("attribute {$attribute} on instance %s is not supported", $subject::class));
    }
}
