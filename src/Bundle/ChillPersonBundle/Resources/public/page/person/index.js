import { ShowHide } from "ShowHide";

const maritalStatus = document.getElementById("maritalStatus");
const maritalStatusDate = document.getElementById("maritalStatusDate");
const personEmail = document.getElementById("personEmail");
const personAcceptEmail = document.getElementById("personAcceptEmail");
const personPhoneNumber = document.getElementById("personPhoneNumber");
const personAcceptSMS = document.getElementById("personAcceptSMS");

new ShowHide({
  froms: [maritalStatus],
  container: [maritalStatusDate],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("select").values()) {
        if (input.value) {
          return true;
        }
      }
    }
    return false;
  },
  event_name: "change",
});

if (personAcceptEmail) {
  new ShowHide({
    froms: [personEmail],
    container: [personAcceptEmail],
    test: function (froms) {
      for (let f of froms.values()) {
        for (let input of f.querySelectorAll("input").values()) {
          if (input.value) {
            return true;
          }
        }
      }
      return false;
    },
    event_name: "input",
  });
}

new ShowHide({
  froms: [personPhoneNumber],
  container: [personAcceptSMS],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input").values()) {
        if (input.value) {
          return true;
        }
      }
    }
    return false;
  },
  event_name: "input",
});
