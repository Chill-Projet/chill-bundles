const onSubmit = function (e) {
  e.preventDefault();
  let form = e.target,
    formData = new FormData(form),
    url = form.action,
    payload = {
      type: "accompanying_period",
      id: Number.parseInt(formData.get("periodId"), 10),
      personLocation: {
        type: "person",
        id: Number.parseInt(formData.get("personLocation"), 10),
      },
    };
  console.log("event", e);
  console.log("form", form);
  console.log("formData", formData);
  console.log("url", url);
  console.log("payload", payload);

  window
    .fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
    .then((r) => {
      if (r.ok) {
        console.log("response ok");
        window.location.reload();
      } else {
        console.err("could not patch accompanying course");
      }
    });
};

window.addEventListener("DOMContentLoaded", function () {
  let forms = document.querySelectorAll(".quickLocationForm");

  console.log(forms);

  forms.forEach(function (form) {
    console.log("form quickLocation found", form);
    form.addEventListener("submit", onSubmit);
  });
});
