import { ShowHide } from "ChillMainAssets/lib/show_hide/show_hide.js";

window.addEventListener("DOMContentLoaded", function () {
  let personContainer = document.querySelector("#person-entity"),
    entitySelector = document.querySelector("#entity-selector"),
    freetextContainer = document.querySelector("#freetext-entity"),
    thirdpartyContainer = document.querySelector("#thirdparty-entity");
  if (null === entitySelector) {
    return;
  }

  new ShowHide({
    debug: false,
    load_event: null,
    froms: [entitySelector],
    container: [personContainer],
    test: function (froms) {
      for (let container of froms) {
        return container.querySelector('input[value="person"]').checked;
      }
      console.log("we couldnt find the input");
      return false;
    },
  });

  new ShowHide({
    debug: false,
    load_event: null,
    froms: [entitySelector],
    container: [thirdpartyContainer],
    test: function (froms) {
      for (let container of froms) {
        return container.querySelector('input[value="thirdparty"]').checked;
      }
      console.log("we couldnt find the input");
      return false;
    },
  });

  new ShowHide({
    debug: false,
    load_event: null,
    froms: [entitySelector],
    container: [freetextContainer],
    test: function (froms) {
      for (let container of froms) {
        return container.querySelector('input[value="freetext"]').checked;
      }
      console.log("we couldnt find the input");
      return false;
    },
  });
});
