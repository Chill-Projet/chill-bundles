import { ShowHide } from "ShowHide";

let k = document.getElementById("waitingForBirthContainer"),
  waitingForBirthDate = document.getElementById("waitingForBirthDateContainer");
console.log(k);

new ShowHide({
  container: [waitingForBirthDate],
  froms: [k],
  event_name: "input",
  debug: true,
  test: function (froms) {
    for (let f of froms.values()) {
      console.log(f);
      for (let input of f.querySelectorAll("input").values()) {
        return input.checked;
      }
    }

    return false;
  },
});
