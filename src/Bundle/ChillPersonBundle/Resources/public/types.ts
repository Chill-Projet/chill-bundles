import {
  Address,
  Center,
  Civility,
  DateTime,
  User,
  WorkflowAvailable,
} from "../../../ChillMainBundle/Resources/public/types";
import { StoredObject } from "../../../ChillDocStoreBundle/Resources/public/types";

export interface Person {
  id: number;
  type: "person";
  text: string;
  textAge: string;
  firstName: string;
  lastName: string;
  current_household_address: Address | null;
  birthdate: DateTime | null;
  deathdate: DateTime | null;
  age: number;
  phonenumber: string;
  mobilenumber: string;
  email: string;
  gender: "woman" | "man" | "other";
  centers: Center[];
  civility: Civility | null;
  current_household_id: number;
  current_residential_addresses: Address[];
}

export interface AccompanyingPeriodWorkEvaluationDocument {
  id: number;
  type: "accompanying_period_work_evaluation_document";
  storedObject: StoredObject;
  title: string;
  createdAt: DateTime | null;
  createdBy: User | null;
  updatedAt: DateTime | null;
  updatedBy: User | null;
  workflows_availables: WorkflowAvailable[];
  workflows: object[];
}
