import { fetchResults } from "ChillMainAssets/lib/api/apiMethods.ts";

const fetchHouseholdByAddressReference = async (reference) => {
  const url = `/api/1.0/person/household/by-address-reference/${reference.id}.json`;
  return fetchResults(url);
};

export { fetchHouseholdByAddressReference };
