import { createApp } from "vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import App from "./App.vue";

if (
  null !==
  document.getElementById("export_filters_social_work_type_filter_enabled")
) {
  const i18n = _createI18n({});
  const form = document.getElementById(
    "export_filters_social_work_type_filter_form",
  );
  const after = form.appendChild(document.createElement("div"));

  /* exported app */
  const app = createApp({
    template: `<app></app>`,
  })
    .use(i18n)
    .component("app", App)
    .mount(after);
}
