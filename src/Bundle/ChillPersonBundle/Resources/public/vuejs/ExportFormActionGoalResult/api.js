import { fetchResults } from "ChillMainAssets/lib/api/apiMethods";

const getSocialActions = () =>
  fetchResults("/api/1.0/person/social/social-action.json", {
    item_per_page: 200,
  });

const getGoalByAction = (id) => {
  let url = `/api/1.0/person/social-work/goal/by-social-action/${id}.json`;
  return fetchResults(url);
};

const getResultByAction = (id) => {
  let url = `/api/1.0/person/social-work/result/by-social-action/${id}.json`;
  return fetchResults(url);
};

const getResultByGoal = (id) => {
  let url = `/api/1.0/person/social-work/result/by-goal/${id}.json`;
  return fetchResults(url);
};

export {
  getSocialActions,
  getGoalByAction,
  getResultByAction,
  getResultByGoal,
};
