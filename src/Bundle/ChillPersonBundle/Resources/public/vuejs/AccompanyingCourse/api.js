import { fetchResults } from "ChillMainAssets/lib/api/apiMethods.ts";

/*
 * Endpoint v.2 chill_api_single_accompanying_course__entity
 * method GET/HEAD, get AccompanyingCourse Instance
 *
 *  @id   integer - id of accompanyingCourse
 */
const getAccompanyingCourse = (id) => {
  const url = `/api/1.0/person/accompanying-course/${id}.json`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw {
      msg: "Error while retriving AccompanyingPeriod Course.",
      sta: response.status,
      txt: response.statusText,
      err: new Error(),
      body: response.body,
    };
  });
};

const getUsers = () => fetchResults("/api/1.0/main/user.json");

const getReferrersSuggested = (course) => {
  const url = `/api/1.0/person/accompanying-course/${course.id}/referrers-suggested.json`;
  return fetchResults(url);
};

const getUserJobs = () => fetchResults("/api/1.0/main/user-job.json");

const getSocialIssues = () => {
  const url = `/api/1.0/person/social-work/social-issue.json`;
  return fetchResults(url);
};

const whoami = () => {
  const url = `/api/1.0/main/whoami.json`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw {
      msg: "Error while getting whoami.",
      sta: response.status,
      txt: response.statusText,
      err: new Error(),
      body: response.body,
    };
  });
};

export {
  whoami,
  getSocialIssues,
  getAccompanyingCourse,
  getUsers,
  getReferrersSuggested,
  getUserJobs,
};
