import { createApp } from "vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import { store } from "./store";
import { personMessages } from "ChillPersonAssets/vuejs/_js/i18n";
import "vue-toast-notification/dist/theme-sugar.css";
import App from "./App.vue";
import ToastPlugin from "vue-toast-notification";

const i18n = _createI18n(personMessages);

/* exported app */
const app = createApp({
  template: `<app></app>`,
})
  .use(store)
  .use(ToastPlugin, {
    position: "bottom-right",
    type: "error",
    duration: 10000,
    dismissible: true,
  })
  .use(i18n)
  .component("app", App)
  .mount("#accompanying_course_work_edit");
