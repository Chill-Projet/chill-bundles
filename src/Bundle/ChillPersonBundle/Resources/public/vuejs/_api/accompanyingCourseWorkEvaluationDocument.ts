import { AccompanyingPeriodWorkEvaluationDocument } from "../../types";
import { makeFetch } from "../../../../../ChillMainBundle/Resources/public/lib/api/apiMethods";

export const duplicate = async (
  id: number,
): Promise<AccompanyingPeriodWorkEvaluationDocument> => {
  return makeFetch<null, AccompanyingPeriodWorkEvaluationDocument>(
    "POST",
    `/api/1.0/person/accompanying-course-work-evaluation-document/${id}/duplicate`,
  );
};
