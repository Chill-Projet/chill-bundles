const findSocialActionsBySocialIssue = (id) => {
  const url = `/api/1.0/person/social/social-action/by-social-issue/${id}.json`;

  return fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          "Error while retrieving social actions " +
            response.status +
            " " +
            response.statusText,
        );
      }
      return response.json();
    })
    .catch((err) => {
      throw err;
    });
};

export { findSocialActionsBySocialIssue };
