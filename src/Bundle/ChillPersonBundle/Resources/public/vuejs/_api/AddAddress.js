/*
 * Endpoint chill_api_single_person_address
 * method POST, post Person instance
 *
 *  @id     integer - id of Person
 *  @body   Object - dictionary with changes to post
 */
const postAddressToPerson = (personId, addressId) => {
  //console.log(personId);
  //console.log(addressId);
  const body = {
    id: addressId,
  };
  const url = `/api/1.0/person/person/${personId}/address.json`;
  return fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json;charset=utf-8" },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint household
 * method POST, post Household instance
 *
 *  @id     integer - id of household
 *  @body   Object - dictionary with changes to post
 */
const postAddressToHousehold = (householdId, addressId) => {
  const body = {
    id: addressId,
  };
  const url = `/api/1.0/person/household/${householdId}/address.json`;
  return fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json;charset=utf-8" },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

export { postAddressToPerson, postAddressToHousehold };
