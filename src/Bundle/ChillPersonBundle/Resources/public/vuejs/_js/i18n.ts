const personMessages = {
  fr: {
    add_persons: {
      title: "Ajouter des usagers",
      suggested_counter: "Pas de résultats | 1 résultat | {count} résultats",
      selected_counter: " 1 sélectionné | {count} sélectionnés",
      search_some_persons: "Rechercher des personnes..",
    },
    item: {
      type_person: "Usager",
      type_user: "TMS",
      type_thirdparty: "Tiers professionnel",
      type_household: "Ménage",
    },
    person: {
      firstname: "Prénom",
      lastname: "Nom",
      born: (ctx: { gender: "man" | "woman" | "neutral" }) => {
        if (ctx.gender === "man") {
          return "Né le";
        } else if (ctx.gender === "woman") {
          return "Née le";
        } else {
          return "Né·e le";
        }
      },
      center_id: "Identifiant du centre",
      center_type: "Type de centre",
      center_name: "Territoire", // vendée
      phonenumber: "Téléphone",
      mobilenumber: "Mobile",
      altnames: "Autres noms",
      email: "Courriel",
      gender: {
        title: "Genre",
        placeholder: "Choisissez le genre de l'usager",
        woman: "Féminin",
        man: "Masculin",
        neutral: "Neutre, non binaire",
        unknown: "Non renseigné",
        undefined: "Non renseigné",
      },
      civility: {
        title: "Civilité",
        placeholder: "Choisissez la civilité",
      },
      address: {
        create_address: "Ajouter une adresse",
        show_address_form:
          "Ajouter une adresse pour un usager non suivi et seul dans un ménage",
        warning:
          "Un nouveau ménage va être créé. L'usager sera membre de ce ménage.",
      },
      center: {
        placeholder: "Choisissez un centre",
        title: "Centre",
      },
    },
    error_only_one_person: "Une seule personne peut être sélectionnée !",
  },
};

export { personMessages };
