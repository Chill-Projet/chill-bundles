import { splitId } from "./vis-network";
import { makeFetch } from "ChillMainAssets/lib/api/apiMethods.ts";
/**
 * @function postFetch
 * @param url
 * @param body
 * @returns {Promise<Response>}
 */
const postFetch = (url, body) => {
  return makeFetch("POST", url, body);
};

/**
 * @function patchFetch
 * @param url
 * @param body
 * @returns {Promise<Response>}
 */
const patchFetch = (url, body) => {
  return makeFetch("PATCH", url, body);
};

/**
 * @function deleteFetch
 * @param url
 * @param body
 * @returns {Promise<Response>}
 */
const deleteFetch = (url) => {
  return makeFetch("DELETE", url, null);
};

/**
 * @function getHouseholdByPerson
 * @param person
 * @returns {Promise<Response>}
 */
const getHouseholdByPerson = (person) => {
  //console.log('getHouseholdByPerson', person.id)
  if (person.current_household_id === null) {
    throw "Currently the person has not household!";
  }
  return makeFetch(
    "GET",
    `/api/1.0/person/household/${person.current_household_id}.json`,
  );
};

/**
 * @function getCoursesByPerson
 * @param person
 * @returns {Promise<Response>}
 */
const getCoursesByPerson = (person) => {
  //console.log('getCoursesByPerson', person._id)
  return makeFetch(
    "GET",
    `/api/1.0/person/accompanying-course/by-person/${person._id}.json`,
  );
};

/**
 * @function getRelationshipsByPerson
 * @param person
 * @returns {Promise<Response>}
 */
const getRelationshipsByPerson = (person) => {
  //console.log('getRelationshipsByPerson', person.id)
  return makeFetch(
    "GET",
    `/api/1.0/relations/relationship/by-person/${person._id}.json`,
  );
};

/**
 * Return list of relations
 * @returns {Promise<Response>}
 */
const getRelationsList = () => {
  return makeFetch("GET", `/api/1.0/relations/relation.json`);
};

/**
 * @function postRelationship
 * @param relationship
 * @returns {Promise<Response>}
 */
const postRelationship = (relationship) => {
  //console.log(relationship);
  return postFetch(`/api/1.0/relations/relationship.json`, {
    type: "relationship",
    fromPerson: { type: "person", id: splitId(relationship.from, "id") },
    toPerson: { type: "person", id: splitId(relationship.to, "id") },
    relation: { type: "relation", id: relationship.relation.id },
    reverse: relationship.reverse,
  });
};

/**
 * @function patchRelationship
 * @param relationship
 * @returns {Promise<Response>}
 */
const patchRelationship = (relationship) => {
  //console.log(relationship)
  let linkType = splitId(relationship.id, "link");
  let id = splitId(linkType, "id");
  return patchFetch(`/api/1.0/relations/relationship/${id}.json`, {
    type: "relationship",
    fromPerson: { type: "person", id: splitId(relationship.from, "id") },
    toPerson: { type: "person", id: splitId(relationship.to, "id") },
    relation: { type: "relation", id: relationship.relation.id },
    reverse: relationship.reverse,
  });
};

/**
 * @function deleteRelationship
 * @param relationship
 * @returns {Promise<Response>}
 */
const deleteRelationship = (relationship) => {
  //console.log(relationship)
  let linkType = splitId(relationship.id, "link");
  let id = splitId(linkType, "id");
  return deleteFetch(`/api/1.0/relations/relationship/${id}.json`);
};

export {
  getHouseholdByPerson,
  getCoursesByPerson,
  getRelationshipsByPerson,
  getRelationsList,
  postRelationship,
  patchRelationship,
  deleteRelationship,
};
