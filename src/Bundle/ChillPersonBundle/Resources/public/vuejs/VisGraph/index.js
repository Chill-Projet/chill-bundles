import { createApp } from "vue";
import { store } from "./store.js";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import { visMessages } from "./i18n";
import App from "./App.vue";
import "vue-toast-notification/dist/theme-sugar.css";
import "./vis-network";
import ToastPlugin from "vue-toast-notification";

const i18n = _createI18n(visMessages);
const container = document.getElementById("relationship-graph");
const persons = JSON.parse(container.dataset.persons);

persons.forEach((person) => {
  store.dispatch("addPerson", person);
  store.commit("markInWhitelist", person);
});

/* exported app */
const app = createApp({
  template: `<app :household_id="this.household_id"></app>`,
  data() {
    return {
      household_id: JSON.parse(container.dataset.householdId),
    };
  },
})
  .use(store)
  .use(i18n)
  .use(ToastPlugin, {
    position: "bottom-right",
    type: "error",
    duration: 5000,
    dismissible: true,
  })
  .component("app", App)
  .mount("#relationship-graph");
