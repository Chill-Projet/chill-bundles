<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Entity\AccompanyingPeriod;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkReferrerHistory;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodWorkTest extends TestCase
{
    public function testReferrerHistory(): void
    {
        $work = new AccompanyingPeriodWork();
        $userA = new User();
        $userB = new User();
        $userC = new User();

        self::assertCount(0, $work->getReferrers());

        $work->addReferrer($userA);

        self::assertCount(1, $work->getReferrers());
        self::assertContains($userA, $work->getReferrers());

        $work->addReferrer($userB);

        self::assertCount(2, $work->getReferrers());
        self::assertContains($userA, $work->getReferrers());
        self::assertContains($userB, $work->getReferrers());

        $work->addReferrer($userC);
        $work->removeReferrer($userB);

        self::assertCount(2, $work->getReferrers());
        self::assertContains($userA, $work->getReferrers());
        self::assertNotContains($userB, $work->getReferrers());
        self::assertContains($userC, $work->getReferrers());

        $work->removeReferrer($userA);
        self::assertNotContains($userA, $work->getReferrers());
        self::assertNotContains($userB, $work->getReferrers());
        self::assertContains($userC, $work->getReferrers());

        self::assertTrue(array_is_list($work->getReferrers()->toArray()));
    }

    public function testReferrerHistoryOnDifferentDays(): void
    {
        $work = new AccompanyingPeriodWork();
        $userA = new User();
        $userB = new User();
        $userC = new User();

        $work->addReferrer($userA);

        $historyA = $work->getReferrersHistory()->first();
        $reflection = new \ReflectionClass($historyA);
        $startDateReflection = $reflection->getProperty('startDate');
        $startDateReflection->setAccessible(true);
        $startDateReflection->setValue($historyA, new \DateTimeImmutable('1 year ago'));

        $work->addReferrer($userB);
        $work->addReferrer($userC);

        $work->removeReferrer($userB);
        $work->removeReferrer($userA);

        self::assertCount(1, $work->getReferrers());
        self::assertNotContains($userA, $work->getReferrers());
        self::assertNotContains($userB, $work->getReferrers());
        self::assertContains($userC, $work->getReferrers());

        self::assertCount(2, $work->getReferrersHistory());

        $historyA = $work->getReferrersHistory()
            ->filter(fn (AccompanyingPeriodWorkReferrerHistory $h) => $userA === $h->getUser())
            ->first();

        self::assertNotFalse($historyA);
        self::assertSame($userA, $historyA->getUser());
        self::assertEquals((new \DateTimeImmutable())->format('Y-m-d'), $historyA->getEndDate()->format('Y-m-d'));

        self::assertTrue(array_is_list($work->getReferrers()->toArray()));
    }
}
