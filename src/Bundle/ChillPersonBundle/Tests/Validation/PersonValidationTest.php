<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Validation;

use Chill\PersonBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class PersonValidationTest extends KernelTestCase
{
    private static ValidatorInterface $validator;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();

        self::$validator = self::getContainer()->get(ValidatorInterface::class);
    }

    /**
     * @dataProvider provideDataValidation
     */
    public function testFirstName(Person $person, string $path, string $code, int $expectedCountErrors): void
    {
        $errors = self::$validator->validate($person);

        $errorsFiltered = array_filter(iterator_to_array($errors), fn (ConstraintViolationInterface $e) => $path === $e->getPropertyPath() /* && $code === $e->getCode() */);

        self::assertCount($expectedCountErrors, $errorsFiltered);
    }

    public static function provideDataValidation(): iterable
    {
        $person = new Person();

        yield [$person, 'firstName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 1];
        yield [$person, 'lastName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 1];

        $person = (new Person())->setFirstName('')->setLastName('');

        yield [$person, 'firstName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 1];
        yield [$person, 'lastName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 1];

        $person = (new Person())->setFirstName('Valid')->setLastName('valid');

        yield [$person, 'firstName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 0];
        yield [$person, 'lastName', 'c1051bb4-d103-4f74-8988-acbcafc7fdc3', 0];

        $person = (new Person())->setBirthdate(new \DateTime('next year'));

        yield [$person, 'birthdate', '3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4', 1];

        $person = (new Person())->setBirthdate(new \DateTime('1700-01-01T00:00:00'));

        yield [$person, 'birthdate', '3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4', 1];

        $person = (new Person())->setBirthdate(new \DateTime('yesterday'));

        yield [$person, 'birthdate', '3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4', 0];

        $person = (new Person())->setEmail('blabla');

        yield [$person, 'email', 'bd79c0ab-ddba-46cc-a703-a7a4b08de310', 1];

        $person = (new Person())->setEmail('chillfake@gmail.com');

        yield [$person, 'email', 'bd79c0ab-ddba-46cc-a703-a7a4b08de310', 0];
    }
}
