<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Templating\Entity;

use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Chill\PersonBundle\Templating\Entity\ClosingMotiveRender;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ClosingMotiveRenderTest extends KernelTestCase
{
    private static ClosingMotiveRender $closingMotiveRender;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        self::$closingMotiveRender = self::getContainer()->get(ClosingMotiveRender::class);
    }

    /**
     * @dataProvider provideClosingMotiveWithRenderString
     */
    public function testRenderString(ClosingMotive $closingMotive, string $expected): void
    {
        $actual = self::$closingMotiveRender->renderString($closingMotive, []);

        self::assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideClosingMotiveWithRenderString
     */
    public function testRenderBox(ClosingMotive $closingMotive, string $expectedContent): void
    {
        $actual = strip_tags(self::$closingMotiveRender->renderBox($closingMotive, []));

        self::assertStringContainsString($expectedContent, $actual);
    }

    public static function provideClosingMotiveWithRenderString(): iterable
    {
        $closingMotive = (new ClosingMotive())->setName(['fr' => 'Left']);

        yield [$closingMotive, 'Left'];

        $closingMotive = (new ClosingMotive())->setName(['fr' => 'Left'])->setIsCanceledAccompanyingPeriod(true);

        yield [$closingMotive, 'Left (annulé)'];

        $closingMotiveParent = (new ClosingMotive())->setName(['fr' => 'Parent']);
        $closingMotive = (new ClosingMotive())->setName(['fr' => 'Children']);
        $closingMotiveParent->addChildren($closingMotive);

        yield [$closingMotive, 'Parent > Children'];

        $closingMotiveParent = (new ClosingMotive())->setName(['fr' => 'Parent'])->setIsCanceledAccompanyingPeriod(true);
        $closingMotive = (new ClosingMotive())->setName(['fr' => 'Children']);
        $closingMotiveParent->addChildren($closingMotive);

        yield [$closingMotive, 'Parent > Children (annulé)'];
    }
}
