<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Form\Type;

use Chill\PersonBundle\Form\Type\PickPersonType;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @internal
 *
 * @coversNothing
 */
final class PickPersonTypeTest extends KernelTestCase
{
    /**
     * @var \Symfony\Component\Form\FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var \Chill\MainBundle\Entity\User
     */
    protected $user;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->user = self::getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository(\Chill\MainBundle\Entity\User::class)
            ->findOneBy(['username' => 'multi_center']);

        $this->formFactory = self::getContainer()->get('form.factory');

        $token = (new UsernamePasswordToken($this->user, 'password', 'firewall'));
        self::getContainer()->get('security.token_storage')
            ->setToken($token);
    }

    /**
     * test with an invalid center type in the option 'centers' (in an array).
     */
    public function testWithInvalidOptionCenters(): never
    {
        $this->expectException(\RuntimeException::class);

        $this->markTestSkipped('need to inject locale into url generator without request');
        $this->formFactory
            ->createBuilder(PickPersonType::class, null, [
                'centers' => ['string'],
            ])
            ->getForm();
    }

    /**
     * Test the form with an option 'centers' with an unique center
     * entity (not in an array).
     */
    public function testWithOptionCenter()
    {
        $this->markTestSkipped('need to inject locale into url generator without request');
        $center = self::getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository(\Chill\MainBundle\Entity\Center::class)
            ->findOneBy(['name' => 'Center A']);

        $form = $this->formFactory
            ->createBuilder(PickPersonType::class, null, [
                'centers' => $center,
            ])
            ->getForm();

        // transform into a view to have data-center attr
        $view = $form->createView();

        /* @var $centerIds \Symfony\Component\Form\ChoiceList\View\ChoiceView */
        foreach ($view->vars['choices'] as $choice) {
            $centerIds[] = $choice->attr['data-center'];
        }

        $this->assertEquals(
            1,
            \count(array_unique($centerIds)),
            'test that the form contains people from only one centers'
        );

        $this->assertEquals($center->getId(), array_unique($centerIds)[0]);
    }

    /**
     * Test the form with multiple centers.
     */
    public function testWithOptionCenters()
    {
        $this->markTestSkipped('need to inject locale into url generator without request');
        $centers = self::getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository(\Chill\MainBundle\Entity\Center::class)
            ->findAll();

        $form = $this->formFactory
            ->createBuilder(PickPersonType::class, null, [
                'centers' => $centers,
            ])
            ->getForm();

        // transform into a view to have data-center attr
        $view = $form->createView();

        /* @var $centerIds \Symfony\Component\Form\ChoiceList\View\ChoiceView */
        foreach ($view->vars['choices'] as $choice) {
            $centerIds[] = $choice->attr['data-center'];
        }

        $this->assertEquals(
            2,
            \count(array_unique($centerIds)),
            'test that the form contains people from only one centers'
        );
    }

    public function testWithOptionRoleInvalid(): never
    {
        $this->markTestSkipped('need to inject locale into url generator without request');
        $form = $this->formFactory
            ->createBuilder(PickPersonType::class, null, [
                'role' => 'INVALID',
            ])
            ->getForm();

        // transform into a view to have data-center attr
        $view = $form->createView();

        $this->assertEquals(0, \count($view->vars['choices']));
    }

    public function testWithoutOption()
    {
        $this->markTestSkipped('need to inject locale into url generator without request');

        $form = $this->formFactory
            ->createBuilder(PickPersonType::class, null, [])
            ->getForm();

        $this->assertInstanceOf(
            \Symfony\Component\Form\FormInterface::class,
            $form
        );

        // transform into a view to have data-center attr
        $view = $form->createView();

        $centerIds = [];

        /* @var $centerIds \Symfony\Component\Form\ChoiceList\View\ChoiceView */
        foreach ($view->vars['choices'] as $choice) {
            $centerIds[] = $choice->attr['data-center'];
        }

        $this->assertEquals(
            2,
            \count(array_unique($centerIds)),
            'test that the form contains people from 2 centers'
        );
    }
}
