<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

use Chill\MainBundle\Repository\CenterRepositoryInterface;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonDuplicateControllerViewTest extends WebTestCase
{
    public static function providePersonData(): iterable
    {
        self::bootKernel();

        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);
        /** @var CenterRepositoryInterface $centerRepository */
        $centerRepository = self::getContainer()->get(CenterRepositoryInterface::class);

        $center = $centerRepository->findOneBy(['name' => 'Center A']);
        $person = (new Person())
            ->setLastName('Tested Persan')
            ->setFirstName('Réginal')
            ->setCenter($center)
            ->setGender(Person::MALE_GENDER);
        $em->persist($person);

        foreach ($person->getCenterHistory() as $centerHistory) {
            $em->persist($centerHistory);
        }

        $person2 = (new Person())
            ->setLastName('Tested Person')
            ->setFirstName('Réginald')
            ->setCenter($center)
            ->setGender(Person::MALE_GENDER);
        $em->persist($person2);

        foreach ($person2->getCenterHistory() as $centerHistory) {
            $em->persist($centerHistory);
        }

        $em->flush();
        $em->clear();

        yield [$person->getId(), $person2->getId()];
    }

    /**
     * @dataProvider providePersonData
     */
    public function testViewDuplicatePerson(int $personId, int $person2Id): void
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'center a_social',
            'PHP_AUTH_PW' => 'password',
        ]);

        $crawler = $client->request('GET', '/en/person/'.$personId.'/duplicate/view');
        $response = $client->getResponse();
        $this->assertTrue($response->isSuccessful());

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Find duplicate")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Réginal")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Réginald")')->count());

        $crawler = $client->request('GET', '/en/person/'.$personId.'/duplicate/'.$person2Id.'/confirm');
        $response = $client->getResponse();
        $this->assertTrue($response->isSuccessful());

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Old person")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("New person")')->count());

        $crawler = $client->request('POST', '/en/person/'.$personId.'/duplicate/'.$person2Id.'/confirm', [
            'chill_personbundle_person_confirm_duplicate[confirm]' => 1,
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isSuccessful());
    }
}
