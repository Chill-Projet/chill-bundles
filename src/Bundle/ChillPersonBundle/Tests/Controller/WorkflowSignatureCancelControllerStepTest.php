<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

use Chill\MainBundle\Controller\WorkflowSignatureStateChangeController;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Routing\ChillUrlGeneratorInterface;
use Chill\MainBundle\Security\Authorization\EntityWorkflowStepSignatureVoter;
use Chill\MainBundle\Workflow\SignatureStepStateChanger;
use Chill\MainBundle\Workflow\WorkflowTransitionContextDTO;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

/**
 * @internal
 *
 * @coversNothing
 */
class WorkflowSignatureCancelControllerStepTest extends WebTestCase
{
    private FormFactoryInterface $formFactory;
    private SignatureStepStateChanger $signatureStepStateChanger;
    private ChillUrlGeneratorInterface $chillUrlGenerator;
    private RequestStack $requestStack;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->formFactory = self::getContainer()->get('form.factory');
        $this->signatureStepStateChanger = self::getContainer()->get(SignatureStepStateChanger::class);
        $this->chillUrlGenerator = self::getContainer()->get(ChillUrlGeneratorInterface::class);

        $requestContext = self::getContainer()->get(RequestContext::class);
        $requestContext->setParameter('_locale', 'fr');

        $this->requestStack = self::getContainer()->get(RequestStack::class);

    }

    public function testCancelSignatureGet(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->futureUserSignature = new User();
        $entityWorkflow->setStep('signature', $dto, 'to_signature', new \DateTimeImmutable(), new User());
        $signature = $entityWorkflow->getCurrentStep()->getSignatures()->first();

        $security = $this->createMock(Security::class);
        $security->expects($this->once())->method('isGranted')
            ->with(EntityWorkflowStepSignatureVoter::CANCEL, $signature)->willReturn(true);

        $entityManager = $this->createMock(EntityManager::class);

        $twig = $this->createMock(Environment::class);
        $twig->expects($this->once())->method('render')->withAnyParameters()
            ->willReturn('template');

        $controller = new WorkflowSignatureStateChangeController($entityManager, $security, $this->formFactory, $twig, $this->signatureStepStateChanger, $this->chillUrlGenerator);

        $request = new Request();
        $request->setMethod('GET');

        $this->requestStack->push($request);

        $response = $controller->cancelSignature($signature, $request);

        self::assertEquals(200, $response->getStatusCode());
    }
}
