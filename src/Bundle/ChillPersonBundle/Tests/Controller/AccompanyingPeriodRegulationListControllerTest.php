<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingPeriodRegulationListControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public function testRegulationList(): void
    {
        $client = $this->getClientAuthenticated();

        $client->request('GET', '/fr/person/periods/undispatched');

        $this->assertResponseIsSuccessful();

        self::ensureKernelShutdown();
    }
}
