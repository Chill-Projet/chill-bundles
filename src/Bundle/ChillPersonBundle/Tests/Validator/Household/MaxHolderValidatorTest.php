<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Validator\Household;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Household\Position;
use Chill\PersonBundle\Validator\Constraints\Household\MaxHolder;
use Chill\PersonBundle\Validator\Constraints\Household\MaxHolderValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class MaxHolderValidatorTest extends ConstraintValidatorTestCase
{
    public static function provideInvalidHousehold()
    {
        $household = new Household();
        $position = (new Position())
            ->setAllowHolder(true);
        $household
            ->addMember(
                (new HouseholdMember())
                    ->setHolder(true)
                    ->setStartDate(new \DateTimeImmutable('2010-01-01'))
                    ->setEndDate(new \DateTimeImmutable('2010-12-01'))
            )
            ->addMember(
                (new HouseholdMember())
                    ->setHolder(true)
                    ->setStartDate(new \DateTimeImmutable('2010-06-01'))
                    ->setEndDate(new \DateTimeImmutable('2010-07-01'))
            )
            ->addMember(
                (new HouseholdMember())
                    ->setHolder(true)
                    ->setStartDate(new \DateTimeImmutable('2010-01-01'))
                    ->setEndDate(new \DateTimeImmutable('2010-12-01'))
            );

        yield [
            $household,
            [
                '{{ start }}' => '01-06-2010',
                '{{ end }}' => '01-07-2010',
            ],
        ];
    }

    /**
     * @dataProvider provideInvalidHousehold
     */
    public function testHouseholdInvalid(Household $household, mixed $parameters)
    {
        $constraint = $this->getConstraint();

        $this->validator->validate($household, $constraint);

        $this->buildViolation('msg')
            ->setParameters($parameters)
            ->assertRaised();
    }

    protected function createValidator()
    {
        return new MaxHolderValidator();
    }

    protected function getConstraint()
    {
        return new MaxHolder([
            'message' => 'msg',
            'messageInfinity' => 'msgInfinity',
        ]);
    }
}
