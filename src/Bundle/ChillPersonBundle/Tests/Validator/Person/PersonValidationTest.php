<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Validator\Person;

use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Validator\Constraints\Person\Birthdate;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonValidationTest extends KernelTestCase
{
    private ValidatorInterface $validator;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->validator = self::getContainer()->get(ValidatorInterface::class);
    }

    public function testBirthdateInFuture()
    {
        $person = (new Person())
            ->setBirthdate(new \DateTime('+2 months'));
        $errors = $this->validator->validate($person, null);

        foreach ($errors->getIterator() as $error) {
            if (Birthdate::BIRTHDATE_INVALID_CODE === $error->getCode()) {
                $this->assertTrue(
                    true,
                    'error code for birthdate invalid is present'
                );

                return;
            }
        }
        $this->assertTrue(
            false,
            'error code for birthdate invalid is present'
        );
    }

    public function testFirstnameValidation()
    {
        $person = (new Person())
            ->setFirstname(\str_repeat('a', 500));
        $errors = $this->validator->validate($person, null);

        foreach ($errors->getIterator() as $error) {
            if (Length::TOO_LONG_ERROR === $error->getCode()) {
                $this->assertTrue(
                    true,
                    'error code for firstname too long is present'
                );

                return;
            }
        }
        $this->assertTrue(
            false,
            'error code for fistname too long is present'
        );
    }
}
