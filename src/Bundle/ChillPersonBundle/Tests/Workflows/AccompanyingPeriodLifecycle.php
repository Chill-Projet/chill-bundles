<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Workflows;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Workflow\Registry;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingPeriodLifecycle extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testConfirm()
    {
        $registry = self::getContainer()->get(Registry::class);
        $period = new AccompanyingPeriod(new \DateTime('now'));
        $workflow = $registry->get($period);

        $this->assertArrayHasKey('DRAFT', $workflow->getMarking($period)->getPlaces());
        $this->assertTrue($workflow->can($period, 'confirm'));
    }
}
