<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Aggregator\PersonAggregators\MaritalStatusAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class MaritalStatusAggregatorTest extends AbstractAggregatorTest
{
    private MaritalStatusAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::getContainer()->get('chill.person.export.aggregator_marital_status');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from(Person::class, 'person')
                ->join('person.maritalStatus', 'personmarital'),
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from(Person::class, 'person'),
        ];
    }
}
