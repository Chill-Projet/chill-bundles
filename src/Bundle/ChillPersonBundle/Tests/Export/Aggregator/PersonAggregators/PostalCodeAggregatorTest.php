<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Aggregator\PersonAggregators\PostalCodeAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class PostalCodeAggregatorTest extends AbstractAggregatorTest
{
    private RollingDateConverterInterface $rollingDateConverter;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->rollingDateConverter = self::getContainer()->get(RollingDateConverterInterface::class);
    }

    public function getAggregator()
    {
        return new PostalCodeAggregator($this->rollingDateConverter);
    }

    public static function getFormData(): array
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from(Person::class, 'person'),
        ];
    }
}
