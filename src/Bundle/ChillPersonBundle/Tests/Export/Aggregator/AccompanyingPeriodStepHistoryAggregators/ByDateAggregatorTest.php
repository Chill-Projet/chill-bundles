<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodStepHistory;
use Chill\PersonBundle\Export\Enum\DateGroupingChoiceEnum;
use Doctrine\ORM\EntityManagerInterface;
use Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators\ByDateAggregator;

/**
 * @internal
 *
 * @coversNothing
 */
class ByDateAggregatorTest extends AbstractAggregatorTest
{
    public function getAggregator()
    {
        return new ByDateAggregator();
    }

    public static function getFormData(): array
    {
        return [
            [
                'frequency' => DateGroupingChoiceEnum::YEAR->value,
            ],
            [
                'frequency' => DateGroupingChoiceEnum::WEEK->value,
            ],
            [
                'frequency' => DateGroupingChoiceEnum::MONTH->value,
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $qb = $em->createQueryBuilder()
            ->select('COUNT(DISTINCT acpstephistory.id) As export_result')
            ->from(AccompanyingPeriodStepHistory::class, 'acpstephistory')
            ->join('acpstephistory.period', 'acp');

        return [
            $qb,
        ];
    }
}
