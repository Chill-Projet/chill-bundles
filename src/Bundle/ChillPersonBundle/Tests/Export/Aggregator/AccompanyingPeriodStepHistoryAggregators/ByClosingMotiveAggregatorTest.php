<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodStepHistory;
use Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators\ByClosingMotiveAggregator;
use Chill\PersonBundle\Repository\AccompanyingPeriod\ClosingMotiveRepositoryInterface;
use Chill\PersonBundle\Templating\Entity\ClosingMotiveRender;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ByClosingMotiveAggregatorTest extends AbstractAggregatorTest
{
    private ClosingMotiveRender $closingMotiveRender;
    private ClosingMotiveRepositoryInterface $closingMotiveRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->closingMotiveRender = self::getContainer()->get(ClosingMotiveRender::class);
        $this->closingMotiveRepository = self::getContainer()->get(ClosingMotiveRepositoryInterface::class);
    }

    public function getAggregator()
    {
        return new ByClosingMotiveAggregator(
            $this->closingMotiveRepository,
            $this->closingMotiveRender
        );
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $qb = $em->createQueryBuilder()
            ->select('COUNT(DISTINCT acpstephistory.id) As export_result')
            ->from(AccompanyingPeriodStepHistory::class, 'acpstephistory')
            ->join('acpstephistory.period', 'acp');

        return [
            $qb,
        ];
    }
}
