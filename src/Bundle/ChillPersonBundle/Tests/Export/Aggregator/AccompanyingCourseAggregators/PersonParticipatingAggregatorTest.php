<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Doctrine\ORM\EntityManagerInterface;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\PersonParticipatingAggregator;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonParticipatingAggregatorTest extends AbstractAggregatorTest
{
    private LabelPersonHelper $labelPersonHelper;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->labelPersonHelper = self::getContainer()->get(LabelPersonHelper::class);
    }

    public function getAggregator()
    {
        return new PersonParticipatingAggregator($this->labelPersonHelper);
    }

    public static function getFormData(): array
    {
        return [[]];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.participations', 'acppart'),
        ];
    }
}
