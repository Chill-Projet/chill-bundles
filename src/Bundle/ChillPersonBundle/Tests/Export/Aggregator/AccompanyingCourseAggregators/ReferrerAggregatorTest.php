<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ReferrerAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerAggregatorTest extends AbstractAggregatorTest
{
    private ReferrerAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::getContainer()->get('chill.person.export.aggregator_referrer');
    }

    /**
     * @dataProvider provideBeforeData
     */
    public function testDataTransformer(?array $before, array $expected): void
    {
        $actual = $this->getAggregator()->transformData($before);

        self::assertEqualsCanonicalizing(array_keys($expected), array_keys($actual));
        foreach (['start_date', 'end_date'] as $key) {
            self::assertInstanceOf(RollingDate::class, $actual[$key]);
            self::assertEquals($expected[$key]->getRoll(), $actual[$key]->getRoll(), "Check that the roll is the same for {$key}");
        }
    }

    public static function provideBeforeData(): iterable
    {
        yield [
            ['date_calc' => new RollingDate(RollingDate::T_TODAY)],
            ['start_date' => new RollingDate(RollingDate::T_TODAY), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];

        yield [
            ['start_date' => new RollingDate(RollingDate::T_WEEK_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
            ['start_date' => new RollingDate(RollingDate::T_WEEK_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];

        yield [
            null,
            // this is the default configuration
            ['start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getAggregator(): ReferrerAggregator
    {
        return $this->aggregator;
    }

    public static function getFormData(): array
    {
        return [
            [
                'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.user', 'acpuser'),
        ];
    }
}
