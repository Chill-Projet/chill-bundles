<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\OpeningDateAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class OpeningDateAggregatorTest extends AbstractAggregatorTest
{
    private static OpeningDateAggregator $openingDateAggregator;

    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::bootKernel();

        self::$openingDateAggregator = self::getContainer()->get(OpeningDateAggregator::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function getAggregator()
    {
        return self::$openingDateAggregator;
    }

    public static function getFormData(): array
    {
        return [['frequency' => 'YYYY'],
            ['frequency' => 'YYYY-MM'],
            ['frequency' => 'YYYY-IV']];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $data = [
            self::$entityManager->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
        self::ensureKernelShutdown();

        return $data;
    }
}
