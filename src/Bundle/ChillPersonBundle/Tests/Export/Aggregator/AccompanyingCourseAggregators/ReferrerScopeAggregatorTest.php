<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverter;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\ReferrerScopeAggregator;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerScopeAggregatorTest extends AbstractAggregatorTest
{
    use ProphecyTrait;

    public function getAggregator()
    {
        $translatableStringHelper = $this->prophesize(TranslatableStringHelperInterface::class);
        $translatableStringHelper->localize(Argument::type('array'))->willReturn('localized');

        $scopeRepository = $this->prophesize(ScopeRepositoryInterface::class);
        $scopeRepository->find(Argument::type('int'))->willReturn(
            (new Scope())->setName(['fr' => 'scope'])
        );

        $dateConverter = $this->prophesize(RollingDateConverterInterface::class);
        $dateConverter->convert(Argument::type(RollingDate::class))->willReturn(new \DateTimeImmutable());

        return new ReferrerScopeAggregator(
            $scopeRepository->reveal(),
            $translatableStringHelper->reveal(),
            new RollingDateConverter(),
        );
    }

    public static function getFormData(): array
    {
        return [
            ['start_date' => new RollingDate(RollingDate::T_WEEK_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    /**
     * @dataProvider provideBeforeData
     */
    public function testDataTransformer(?array $before, array $expected): void
    {
        $actual = $this->getAggregator()->transformData($before);

        self::assertEqualsCanonicalizing(array_keys($expected), array_keys($actual));
        foreach (['start_date', 'end_date'] as $key) {
            self::assertInstanceOf(RollingDate::class, $actual[$key]);
            self::assertEquals($expected[$key]->getRoll(), $actual[$key]->getRoll(), "Check that the roll is the same for {$key}");
        }
    }

    public static function provideBeforeData(): iterable
    {
        yield [
            null,
            ['start_date' => new RollingDate(RollingDate::T_FIXED_DATE, new \DateTimeImmutable('1970-01-01')), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];

        yield [
            [],
            ['start_date' => new RollingDate(RollingDate::T_FIXED_DATE, new \DateTimeImmutable('1970-01-01')), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];

        yield [
            ['start_date' => new RollingDate(RollingDate::T_WEEK_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
            ['start_date' => new RollingDate(RollingDate::T_WEEK_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
