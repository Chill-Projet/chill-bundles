<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators\ResultAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResultAggregatorTest extends AbstractAggregatorTest
{
    private ResultAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::getContainer()->get('chill.person.export.aggregator_result');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public static function getFormData(): array
    {
        return [
            [],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acpw.id)')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
