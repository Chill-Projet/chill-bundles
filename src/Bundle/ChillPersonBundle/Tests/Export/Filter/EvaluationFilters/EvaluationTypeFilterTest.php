<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\EvaluationFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Chill\PersonBundle\Export\Filter\EvaluationFilters\EvaluationTypeFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class EvaluationTypeFilterTest extends AbstractFilterTest
{
    private EvaluationTypeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_evaluationtype');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $array = $em->createQueryBuilder()
            ->from(Evaluation::class, 'e')
            ->select('e')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();
        $data = [];
        foreach ($array as $r) {
            $data[] = [
                'accepted_evaluationtype' => $r,
            ];
        }

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('workeval.id')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.works', 'acpw')
                ->join('acpw.accompanyingPeriodWorkEvaluations', 'workeval'),
        ];
    }
}
