<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\IntensityFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class IntensityFilterTest extends AbstractFilterTest
{
    private IntensityFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_intensity');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            ['accepted_intensities' => 'occasional'],
            ['accepted_intensities' => 'regular'],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
