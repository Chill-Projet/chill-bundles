<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\UserScopeFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserScopeFilterTest extends AbstractFilterTest
{
    private UserScopeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get(UserScopeFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $scopes = self::getContainer()->get(EntityManagerInterface::class)
            ->createQuery('SELECT s FROM '.Scope::class.' s')
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
                'scopes' => new ArrayCollection($scopes),
            ],
            [
                'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
                'scopes' => $scopes,
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
