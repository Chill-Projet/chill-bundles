<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\SocialWorkFilters\ReferrerFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerFilterTest extends AbstractFilterTest
{
    private ReferrerFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_treatingagent');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $users = array_slice($em->getRepository(User::class)->findAll(), 0, 1);
        $data = [];
        foreach ($users as $u) {
            $data[] = [
                'accepted_agents' => $u, // some saved export does not have the parameter "agent_at"
            ];

            $data[] = [
                'accepted_agents' => $u,
                'agent_at' => new RollingDate(RollingDate::T_TODAY),
            ];
        }

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.works', 'acpw'),
        ];
    }
}
