<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\HandlingThirdPartyFilter;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class HandlingThirdPartyFilterTest extends AbstractFilterTest
{
    private ThirdPartyRender $thirdPartyRender;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->thirdPartyRender = self::getContainer()->get(ThirdPartyRender::class);
    }

    public function getFilter()
    {
        return new HandlingThirdPartyFilter($this->thirdPartyRender);
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $thirdParties = $em->createQuery('SELECT tp FROM '.ThirdParty::class.' tp')
            ->setMaxResults(2)
            ->getResult();

        return [
            [
                'handling_3parties' => $thirdParties,
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
