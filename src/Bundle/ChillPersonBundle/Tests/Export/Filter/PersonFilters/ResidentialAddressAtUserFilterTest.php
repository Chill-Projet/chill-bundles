<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\PersonFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Filter\PersonFilters\ResidentialAddressAtUserFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResidentialAddressAtUserFilterTest extends AbstractFilterTest
{
    private ResidentialAddressAtUserFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_residential_address_at_user');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            ['date_calc' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('p.id')
                ->from(Person::class, 'p')
                ->join(Person\ResidentialAddress::class, 'resaddr', Expr\Join::WITH, 'resaddr.person = p')
                ->join('p.center', 'center'),
        ];
    }
}
