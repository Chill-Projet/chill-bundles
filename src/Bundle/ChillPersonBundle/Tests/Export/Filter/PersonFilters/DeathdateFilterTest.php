<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\PersonFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Filter\PersonFilters\DeathdateFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class DeathdateFilterTest extends AbstractFilterTest
{
    private DeathdateFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_deathdate');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            [
                'date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'date_to' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('person.id')
                ->from(Person::class, 'person'),
        ];
    }
}
