<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\Filter\PersonFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Filter\PersonFilters\WithParticipationBetweenDatesFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class WithParticipationBetweenDatesFilterTest extends AbstractFilterTest
{
    private WithParticipationBetweenDatesFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get(WithParticipationBetweenDatesFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            [
                'date_after' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'date_before' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('person.id')
                ->from(Person::class, 'person'),
        ];
    }
}
