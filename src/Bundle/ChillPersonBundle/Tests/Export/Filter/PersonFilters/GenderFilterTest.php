<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\PersonFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Filter\PersonFilters\GenderFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class GenderFilterTest extends AbstractFilterTest
{
    private GenderFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get('chill.person.export.filter_gender');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            [
                'accepted_genders' => ['man'],
            ],
            [
                'accepted_genders' => ['woman'],
            ],
            [
                'accepted_genders' => ['man', 'both'],
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('person.firstName')
                ->from(Person::class, 'person'),
        ];
    }
}
