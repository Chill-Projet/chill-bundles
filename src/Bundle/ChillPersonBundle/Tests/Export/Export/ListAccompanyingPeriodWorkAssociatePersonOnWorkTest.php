<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Export\Helper\AggregateStringHelper;
use Chill\MainBundle\Export\Helper\DateTimeHelper;
use Chill\MainBundle\Export\Helper\TranslatableStringExportLabelHelper;
use Chill\MainBundle\Export\Helper\UserHelper;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListAccompanyingPeriodWorkAssociatePersonOnWork;
use Chill\PersonBundle\Export\Helper\FilterListAccompanyingPeriodHelperInterface;
use Chill\PersonBundle\Export\Helper\LabelPersonHelper;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Repository\SocialWork\SocialIssueRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Chill\ThirdPartyBundle\Export\Helper\LabelThirdPartyHelper;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class ListAccompanyingPeriodWorkAssociatePersonOnWorkTest extends AbstractExportTest
{
    use ProphecyTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function getExport()
    {
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $dateTimeHelper = self::getContainer()->get(DateTimeHelper::class);
        $userHelper = self::getContainer()->get(UserHelper::class);
        $personHelper = self::getContainer()->get(LabelPersonHelper::class);
        $thirdPartyHelper = self::getContainer()->get(LabelThirdPartyHelper::class);
        $translatableStringExportLabelHelper = self::getContainer()->get(TranslatableStringExportLabelHelper::class);
        $socialIssueRender = self::getContainer()->get(SocialIssueRender::class);
        $socialIssueRepository = self::getContainer()->get(SocialIssueRepository::class);
        $socialActionRender = self::getContainer()->get(SocialActionRender::class);
        $rollingDateConverter = self::getContainer()->get(RollingDateConverterInterface::class);
        $aggregateStringHelper = self::getContainer()->get(AggregateStringHelper::class);
        $socialActionRepository = self::getContainer()->get(SocialActionRepository::class);
        $filterHelper = $this->prophesize(FilterListAccompanyingPeriodHelperInterface::class);

        yield new ListAccompanyingPeriodWorkAssociatePersonOnWork(
            $entityManager,
            $dateTimeHelper,
            $userHelper,
            $personHelper,
            $thirdPartyHelper,
            $translatableStringExportLabelHelper,
            $socialIssueRender,
            $socialIssueRepository,
            $socialActionRender,
            $rollingDateConverter,
            $aggregateStringHelper,
            $socialActionRepository,
            $filterHelper->reveal(),
        );
    }

    public static function getFormData(): array
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public static function getModifiersCombination(): array
    {
        return [[Declarations::ACP_TYPE]];
    }
}
