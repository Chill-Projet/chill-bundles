<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Export\ListPerson;
use Chill\PersonBundle\Export\Helper\ListPersonHelper;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Test the export "ListPerson".
 *
 * @internal
 *
 * @coversNothing
 */
final class ListPersonTest extends AbstractExportTest
{
    use ProphecyTrait;

    private ListPerson $export;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->export = self::getContainer()->get(ListPerson::class);

        $request = $this->prophesize()
            ->willExtend(\Symfony\Component\HttpFoundation\Request::class);

        $request->getLocale()->willReturn('fr');

        self::getContainer()->get('request_stack')
            ->push($request->reveal());
    }

    public function getExport()
    {
        $customFieldProvider = self::getContainer()->get(CustomFieldProvider::class);
        $listPersonHelper = self::getContainer()->get(ListPersonHelper::class);
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $translatableStringHelper = self::getContainer()->get(TranslatableStringHelper::class);

        yield new ListPerson(
            $customFieldProvider,
            $listPersonHelper,
            $entityManager,
            $translatableStringHelper,
            $this->getParameters(true),
        );

        yield new ListPerson(
            $customFieldProvider,
            $listPersonHelper,
            $entityManager,
            $translatableStringHelper,
            $this->getParameters(false),
        );
    }

    public static function getFormData(): array
    {
        return [['address_date' => new \DateTimeImmutable('today')]];
    }

    public static function getModifiersCombination(): array
    {
        return [
            ['person'],
        ];
    }
}
