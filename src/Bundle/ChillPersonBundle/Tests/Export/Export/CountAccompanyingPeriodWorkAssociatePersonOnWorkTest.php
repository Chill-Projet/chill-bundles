<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\CountAccompanyingPeriodWorkAssociatePersonOnWork;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class CountAccompanyingPeriodWorkAssociatePersonOnWorkTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::getContainer()->get(EntityManagerInterface::class);

        yield new CountAccompanyingPeriodWorkAssociatePersonOnWork($em, $this->getParameters(true));
        yield new CountAccompanyingPeriodWorkAssociatePersonOnWork($em, $this->getParameters(false));
    }

    public static function getFormData(): array
    {
        return [[]];
    }

    public static function getModifiersCombination(): array
    {
        return [[Declarations::SOCIAL_WORK_ACTION_TYPE]];
    }
}
