<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Export\Helper\AggregateStringHelper;
use Chill\MainBundle\Export\Helper\ExportAddressHelper;
use Chill\MainBundle\Export\Helper\TranslatableStringExportLabelHelper;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListHouseholdInPeriod;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ListHouseholdInPeriodTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $addressHelper = self::getContainer()->get(ExportAddressHelper::class);
        $aggregateStringHelper = self::getContainer()->get(AggregateStringHelper::class);
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $rollingDateConverter = self::getContainer()->get(RollingDateConverterInterface::class);
        $translatableStringHelper = self::getContainer()->get(TranslatableStringExportLabelHelper::class);

        yield new ListHouseholdInPeriod(
            $addressHelper,
            $aggregateStringHelper,
            $entityManager,
            $rollingDateConverter,
            $translatableStringHelper,
            $this->getParameters(true),
        );

        yield new ListHouseholdInPeriod(
            $addressHelper,
            $aggregateStringHelper,
            $entityManager,
            $rollingDateConverter,
            $translatableStringHelper,
            $this->getParameters(false),
        );
    }

    public static function getFormData(): array
    {
        return [['calc_date' => new RollingDate(RollingDate::T_TODAY)]];
    }

    public static function getModifiersCombination(): array
    {
        return [[Declarations::PERSON_TYPE]];
    }
}
