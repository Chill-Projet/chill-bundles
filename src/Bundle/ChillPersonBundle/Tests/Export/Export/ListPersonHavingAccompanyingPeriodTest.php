<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListPersonHavingAccompanyingPeriod;
use Chill\PersonBundle\Export\Helper\ListPersonHelper;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ListPersonHavingAccompanyingPeriodTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function getExport()
    {
        $listPersonHelper = self::getContainer()->get(ListPersonHelper::class);
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $rollingDateconverter = self::getContainer()->get(RollingDateConverterInterface::class);

        yield new ListPersonHavingAccompanyingPeriod(
            $listPersonHelper,
            $entityManager,
            $rollingDateconverter,
            $this->getParameters(true),
        );

        yield new ListPersonHavingAccompanyingPeriod(
            $listPersonHelper,
            $entityManager,
            $rollingDateconverter,
            $this->getParameters(false),
        );
    }

    public static function getFormData(): array
    {
        return [['address_date_rolling' => new RollingDate(RollingDate::T_TODAY)]];
    }

    public static function getModifiersCombination(): array
    {
        return [[Declarations::PERSON_TYPE, Declarations::ACP_TYPE]];
    }
}
