<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Workflow;

use Chill\DocStoreBundle\Workflow\WorkflowWithPublicViewDocumentHelper;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvidePersonsAssociated;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvideThirdPartiesAssociated;
use Chill\PersonBundle\Workflow\AccompanyingPeriodWorkEvaluationDocumentWorkflowHandler;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodWorkEvaluationDocumentWorkflowHandlerTest extends TestCase
{
    use ProphecyTrait;

    public function testGetSuggestedUsers()
    {
        $accompanyingCourse = new AccompanyingPeriod();
        $accompanyingCourse->setUser($referrer = new User());
        $accompanyingCourse->addWork($work = new AccompanyingPeriod\AccompanyingPeriodWork());
        $work->addReferrer($workReferrer1 = new User());
        $work->addReferrer($workReferrer2 = new User());
        $work->addReferrer($referrer);
        $work->addAccompanyingPeriodWorkEvaluation($eval = new AccompanyingPeriod\AccompanyingPeriodWorkEvaluation());
        $eval->addDocument($doc = new AccompanyingPeriodWorkEvaluationDocument());
        $entityWorkflow = new EntityWorkflow();

        // Prophesize each dependency
        $workflowRepositoryProphecy = $this->prophesize(EntityWorkflowRepository::class);
        $translatableStringHelperProphecy = $this->prophesize(TranslatableStringHelperInterface::class);
        $translatorProphecy = $this->prophesize(TranslatorInterface::class);
        $twig = $this->prophesize(Environment::class);

        // Create an instance of the class under test using revealed prophecies directly
        $handler = new AccompanyingPeriodWorkEvaluationDocumentWorkflowHandler(
            $this->buildRepository($doc, 1),
            $workflowRepositoryProphecy->reveal(),
            $translatableStringHelperProphecy->reveal(),
            $translatorProphecy->reveal(),
            new WorkflowWithPublicViewDocumentHelper($twig->reveal()),
            $this->prophesize(ProvideThirdPartiesAssociated::class)->reveal(),
            $this->prophesize(ProvidePersonsAssociated::class)->reveal(),
        );

        $entityWorkflow->setRelatedEntityId(1);
        $entityWorkflow->setRelatedEntityClass(AccompanyingPeriodWorkEvaluationDocument::class);

        $users = $handler->getSuggestedUsers($entityWorkflow);

        self::assertContains($referrer, $users);
        self::assertContains($workReferrer1, $users);
        self::assertContains($workReferrer2, $users);
    }

    private function buildRepository(AccompanyingPeriodWorkEvaluationDocument $document, int $id): AccompanyingPeriodWorkEvaluationDocumentRepository
    {
        $repository = $this->prophesize(AccompanyingPeriodWorkEvaluationDocumentRepository::class);

        $repository->find($id)->willReturn($document);

        return $repository->reveal();
    }
}
