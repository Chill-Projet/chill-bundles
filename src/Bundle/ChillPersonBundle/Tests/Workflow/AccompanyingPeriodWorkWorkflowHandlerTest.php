<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Workflow;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkRepository;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvidePersonsAssociated;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvideThirdPartiesAssociated;
use Chill\PersonBundle\Workflow\AccompanyingPeriodWorkWorkflowHandler;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodWorkWorkflowHandlerTest extends TestCase
{
    use ProphecyTrait;

    public function testGetSuggestedUsers()
    {
        $accompanyingCourse = new AccompanyingPeriod();
        $accompanyingCourse->setUser($referrer = new User());
        $accompanyingCourse->addWork($work = new AccompanyingPeriod\AccompanyingPeriodWork());
        $work->addReferrer($workReferrer1 = new User());
        $work->addReferrer($workReferrer2 = new User());
        $work->addReferrer($referrer);
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setRelatedEntityId(1);

        // Prophesize each dependency
        $workflowRepositoryProphecy = $this->prophesize(EntityWorkflowRepository::class);
        $translatableStringHelperProphecy = $this->prophesize(TranslatableStringHelperInterface::class);
        $translatorProphecy = $this->prophesize(TranslatorInterface::class);

        // Create an instance of the class under test using revealed prophecies directly
        $handler = new AccompanyingPeriodWorkWorkflowHandler(
            $this->buildRepository($work, 1),
            $workflowRepositoryProphecy->reveal(),
            $translatableStringHelperProphecy->reveal(),
            $translatorProphecy->reveal(),
            $this->prophesize(ProvideThirdPartiesAssociated::class)->reveal(),
            $this->prophesize(ProvidePersonsAssociated::class)->reveal(),
        );

        $users = $handler->getSuggestedUsers($entityWorkflow);

        self::assertContains($referrer, $users);
        self::assertContains($workReferrer1, $users);
        self::assertContains($workReferrer2, $users);
    }

    private function buildRepository(AccompanyingPeriod\AccompanyingPeriodWork $work, int $int): AccompanyingPeriodWorkRepository
    {
        $accompanyingPeriodWorkRepositoryProphecy = $this->prophesize(AccompanyingPeriodWorkRepository::class);
        $accompanyingPeriodWorkRepositoryProphecy
            ->find($int)
            ->willReturn($work);

        return $accompanyingPeriodWorkRepositoryProphecy->reveal();
    }
}
