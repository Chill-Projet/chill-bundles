<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace AccompanyingPeriod\Lifecycle;

use Chill\MainBundle\Entity\CronJobExecution;
use Chill\PersonBundle\AccompanyingPeriod\Lifecycle\AccompanyingPeriodStepChangeCronjob;
use Chill\PersonBundle\AccompanyingPeriod\Lifecycle\AccompanyingPeriodStepChangeRequestor;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Clock\MockClock;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodStepChangeCronjobTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @dataProvider provideRunTimes
     */
    public function testCanRun(string $datetime, \DateTimeImmutable $lastExecutionStart, bool $canRun): void
    {
        $requestor = $this->prophesize(AccompanyingPeriodStepChangeRequestor::class);
        $clock = new MockClock($datetime);

        $cronJob = new AccompanyingPeriodStepChangeCronjob($clock, $requestor->reveal());
        $cronJobExecution = (new CronJobExecution($cronJob->getKey()))->setLastStart($lastExecutionStart);

        $this->assertEquals($canRun, $cronJob->canRun($cronJobExecution));
    }

    public static function provideRunTimes(): iterable
    {
        // can run, during the night
        yield ['2023-01-15T01:00:00+02:00', new \DateTimeImmutable('2023-01-14T00:00:00+02:00'), true];

        // can not run, not during the night
        yield ['2023-01-15T10:00:00+02:00', new \DateTimeImmutable('2023-01-14T00:00:00+02:00'), false];

        // can not run: not enough elapsed time
        yield ['2023-01-15T01:00:00+02:00', new \DateTimeImmutable('2023-01-15T00:30:00+02:00'), false];
    }
}
