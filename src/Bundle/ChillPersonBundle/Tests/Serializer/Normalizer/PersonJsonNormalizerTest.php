<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\MainBundle\Phonenumber\PhoneNumberHelperInterface;
use Chill\MainBundle\Security\Resolver\CenterResolverManagerInterface;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderExtension;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Repository\ResidentialAddressRepository;
use Chill\PersonBundle\Serializer\Normalizer\PersonJsonNormalizer;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonJsonNormalizerTest extends KernelTestCase
{
    use ProphecyTrait;

    private PersonJsonNormalizer $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();

        $residentialAddressRepository = $this->prophesize(ResidentialAddressRepository::class);
        $residentialAddressRepository
            ->findCurrentResidentialAddressByPerson(Argument::type(Person::class), Argument::any())
            ->willReturn([]);

        $this->normalizer = $this->buildPersonJsonNormalizer(
            self::getContainer()->get(ChillEntityRenderExtension::class),
            self::getContainer()->get(PersonRepository::class),
            self::getContainer()->get(CenterResolverManagerInterface::class),
            $residentialAddressRepository->reveal(),
            self::getContainer()->get(PhoneNumberHelperInterface::class),
            self::getContainer()->get(NormalizerInterface::class)
        );
    }

    public function testNormalization()
    {
        $person = new Person();
        $result = $this->normalizer->normalize($person, 'json', [AbstractNormalizer::GROUPS => ['read']]);

        $this->assertIsArray($result);
    }

    private function buildPersonJsonNormalizer(
        ChillEntityRenderExtension $render,
        PersonRepository $repository,
        CenterResolverManagerInterface $centerResolverManager,
        ResidentialAddressRepository $residentialAddressRepository,
        PhoneNumberHelperInterface $phoneNumberHelper,
        NormalizerInterface $normalizer,
    ): PersonJsonNormalizer {
        $personJsonNormalizer = new PersonJsonNormalizer(
            $render,
            $repository,
            $centerResolverManager,
            $residentialAddressRepository,
            $phoneNumberHelper
        );
        $personJsonNormalizer->setNormalizer($normalizer);

        return $personJsonNormalizer;
    }
}
