<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Resource;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ResourceJsonNormalizerTest extends KernelTestCase
{
    private DenormalizerInterface $denormalizer;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->denormalizer = self::getContainer()->get(DenormalizerInterface::class);
    }

    public function testDenormalize()
    {
        $resource = new Resource();

        $context = [
            AbstractNormalizer::GROUPS => ['write'],
            AbstractNormalizer::OBJECT_TO_POPULATE => $resource,
        ];

        $json = [
            'comment' => 'bloup',
            'type' => 'accompanying_period_resource',
        ];

        $resource = $this->denormalizer->denormalize($json, Resource::class, 'json', $context);

        $this->assertEquals('bloup', $resource->getComment());
    }
}
