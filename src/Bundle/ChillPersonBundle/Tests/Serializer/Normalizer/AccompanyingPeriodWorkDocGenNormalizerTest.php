<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\DocGeneratorBundle\Test\DocGenNormalizerTestAbstract;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkGoal;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 *
 * @template-extends DocGenNormalizerTestAbstract<AccompanyingPeriodWork>
 */
final class AccompanyingPeriodWorkDocGenNormalizerTest extends DocGenNormalizerTestAbstract
{
    private NormalizerInterface $normalizer;

    protected function setUp(): void
    {
        parent::bootKernel();
        $this->normalizer = self::getContainer()->get(NormalizerInterface::class);
    }

    public function provideNotNullObject(): object
    {
        $work = new AccompanyingPeriodWork();
        $work
            ->addPerson((new Person())->setFirstName('hello')->setLastName('name'))
            ->addGoal($g = new AccompanyingPeriodWorkGoal())
            ->addResult($r = new Result())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setCreatedBy($user = new User())
            ->setUpdatedBy($user);
        $g->addResult($r)->setGoal($goal = new Goal());
        $goal->addResult($r);

        return $work;
    }

    public function provideDocGenExpectClass(): string
    {
        return AccompanyingPeriodWork::class;
    }

    public function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    public function testNormalize()
    {
        $work = new AccompanyingPeriodWork();
        $work
            ->addPerson((new Person())->setFirstName('hello')->setLastName('name'))
            ->addGoal($g = new AccompanyingPeriodWorkGoal())
            ->addResult($r = new Result())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setCreatedBy($user = new User())
            ->setUpdatedBy($user);
        $g->addResult($r)->setGoal($goal = new Goal());
        $goal->addResult($r);

        $actual = $this->normalizer->normalize($work, 'docgen', [
            'docgen:expects' => AccompanyingPeriodWork::class,
            AbstractNormalizer::GROUPS => ['docgen:read'],
        ]);

        $this->assertIsArray($actual);
    }
}
