<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Service\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvideThirdPartiesAssociated;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ProvideThirdPartiesAssociatedTest extends TestCase
{
    public function testGetThirdPartiesAssociated()
    {
        $period = new AccompanyingPeriod();
        $period->setRequestor($tp1 = new ThirdParty());
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($tp1));
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($tp2 = new ThirdParty()));
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($p1 = new Person()));

        $provider = new ProvideThirdPartiesAssociated();

        $thirdParties = $provider->getThirdPartiesAssociated($period);

        self::assertCount(2, $thirdParties);
        self::assertContains($tp1, $thirdParties);
        self::assertContains($tp2, $thirdParties);
        self::assertNotContains($p1, $thirdParties);
        self::assertNotContains(null, $thirdParties);
    }
}
