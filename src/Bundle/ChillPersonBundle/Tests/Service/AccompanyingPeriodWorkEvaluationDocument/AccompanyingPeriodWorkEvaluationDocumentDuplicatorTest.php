<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Service\AccompanyingPeriodWorkEvaluationDocument;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Service\StoredObjectDuplicate;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Service\AccompanyingPeriodWorkEvaluationDocument\AccompanyingPeriodWorkEvaluationDocumentDuplicator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Clock\MockClock;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodWorkEvaluationDocumentDuplicatorTest extends TestCase
{
    public function testDuplicate()
    {
        $storedObject = new StoredObject();
        $evaluation = new AccompanyingPeriodWorkEvaluation();
        $document = new AccompanyingPeriodWorkEvaluationDocument();
        $document
            ->setStoredObject($storedObject)
            ->setTitle('test title')
            ->setAccompanyingPeriodWorkEvaluation($evaluation);

        $storedObjectDuplicator = $this->createMock(StoredObjectDuplicate::class);
        $storedObjectDuplicator->expects($this->once())->method('duplicate')->with($storedObject)->willReturn($newStoredObject = new StoredObject());

        $translator = $this->createMock(TranslatorInterface::class);
        $translator->method('trans')->willReturn('test translated');

        $clock = new MockClock();

        $duplicator = new AccompanyingPeriodWorkEvaluationDocumentDuplicator($storedObjectDuplicator, $translator, $clock);

        $actual = $duplicator->duplicate($document);

        self::assertNotSame($document, $actual);
        self::assertStringStartsWith('test title', $actual->getTitle());
        self::assertSame($newStoredObject, $actual->getStoredObject());
        self::assertSame($evaluation, $actual->getAccompanyingPeriodWorkEvaluation());
    }
}
