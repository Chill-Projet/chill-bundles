<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Service\AccompanyingPeriodWork;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvideThirdPartiesAssociated;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ProvideThirdPartiesAssociatedTest extends TestCase
{
    public function testGetThirdPartiesAssociated()
    {
        $period = new AccompanyingPeriod();
        $period->setRequestor($tp1 = new ThirdParty());
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($tp1));
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($tp2 = new ThirdParty()));
        $period->addResource((new AccompanyingPeriod\Resource())->setResource($p1 = new Person()));
        $period->addWork($work = new AccompanyingPeriod\AccompanyingPeriodWork());
        $work->addThirdParty($tp3 = new ThirdParty());
        $work->addThirdParty($tp1);
        $work->setHandlingThierParty($tp4 = new ThirdParty());

        $providerAccPeriod = new \Chill\PersonBundle\Service\AccompanyingPeriod\ProvideThirdPartiesAssociated();
        $provider = new ProvideThirdPartiesAssociated($providerAccPeriod);


        $thirdParties = $provider->getThirdPartiesAssociated($work);

        self::assertContains($tp1, $thirdParties);
        self::assertContains($tp2, $thirdParties);
        self::assertContains($tp3, $thirdParties);
        self::assertContains($tp4, $thirdParties);
        self::assertNotContains($p1, $thirdParties);
        self::assertCount(4, $thirdParties);
    }
}
