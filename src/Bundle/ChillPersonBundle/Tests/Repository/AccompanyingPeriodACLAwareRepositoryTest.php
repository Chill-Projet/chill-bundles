<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Repository;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\CenterRepositoryInterface;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelperForCurrentUserInterface;
use Chill\MainBundle\Security\Resolver\CenterResolverManagerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\AccompanyingPeriodACLAwareRepository;
use Chill\PersonBundle\Repository\AccompanyingPeriodRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Registry;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingPeriodACLAwareRepositoryTest extends KernelTestCase
{
    use ProphecyTrait;

    private AccompanyingPeriodRepository $accompanyingPeriodRepository;

    private CenterResolverManagerInterface $centerResolverManager;

    private CenterRepositoryInterface $centerRepository;

    private EntityManagerInterface $entityManager;

    private ScopeRepositoryInterface $scopeRepository;

    private Registry $registry;

    private static array $periodsIdsToDelete = [];

    /**
     * @var list<array<class-string, int>>
     */
    private static array $entitiesToDelete = [];

    protected function setUp(): void
    {
        self::bootKernel();
        $this->accompanyingPeriodRepository = self::getContainer()->get(AccompanyingPeriodRepository::class);
        $this->centerRepository = self::getContainer()->get(CenterRepositoryInterface::class);
        $this->centerResolverManager = self::getContainer()->get(CenterResolverManagerInterface::class);
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $this->scopeRepository = self::getContainer()->get(ScopeRepositoryInterface::class);
        $this->registry = self::getContainer()->get(Registry::class);
    }

    public static function tearDownAfterClass(): void
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $repository = self::getContainer()->get(AccompanyingPeriodRepository::class);

        foreach (self::$periodsIdsToDelete as $id) {
            if (null === $period = $repository->find($id)) {
                throw new \RuntimeException('period not found while trying to delete it');
            }

            foreach ($period->getParticipations() as $participation) {
                $em->remove($participation);
            }
            $em->remove($period);
        }

        foreach (self::$entitiesToDelete as [$class, $id]) {
            if (null === $entity = $em->find($class, $id)) {
                throw new \RuntimeException(sprintf('entity %s with id %d not found', $class, $id));
            }
            $em->remove($entity);
        }

        $em->flush();
    }

    /**
     * @dataProvider provideDataFindByUserAndPostalCodesOpenedAccompanyingPeriod
     *
     * @param list<array{center: Center, scopeOnRole: list<Scope>, scopeCanSeeConfidential: list<Scope>}> $centerScopes
     * @param list<AccompanyingPeriod>                                                                    $expectedContains
     * @param list<AccompanyingPeriod>                                                                    $expectedNotContains
     */
    public function testFindByUserAndPostalCodesOpenedAccompanyingPeriod(User $user, User $searched, array $centerScopes, array $expectedContains, array $expectedNotContains, string $message): void
    {
        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        $authorizationHelper = $this->prophesize(AuthorizationHelperForCurrentUserInterface::class);
        $centers = [];

        foreach ($centerScopes as ['center' => $center, 'scopeOnRole' => $scopes, 'scopeCanSeeConfidential' => $scopesCanSeeConfidential]) {
            $centers[spl_object_hash($center)] = $center;
            $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE, $center)
                ->willReturn($scopes);
            $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL, $center)
                ->willReturn($scopesCanSeeConfidential);
        }
        $authorizationHelper->getReachableCenters(AccompanyingPeriodVoter::SEE)->willReturn(array_values($centers));

        $repository = new AccompanyingPeriodACLAwareRepository(
            $this->accompanyingPeriodRepository,
            $security->reveal(),
            $authorizationHelper->reveal(),
            $this->centerResolverManager
        );

        $actual = array_map(
            fn (AccompanyingPeriod $period) => $period->getId(),
            $repository->findByUserAndPostalCodesOpenedAccompanyingPeriod($searched, [], ['id' => 'DESC'], 20, 0)
        );

        foreach ($expectedContains as $expected) {
            self::assertContains($expected->getId(), $actual, $message);
        }
        foreach ($expectedNotContains as $expected) {
            self::assertNotContains($expected->getId(), $actual, $message);
        }
    }

    public static function provideDataFindByUserAndPostalCodesOpenedAccompanyingPeriod(): iterable
    {
        self::bootKernel();
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $scopeRepository = self::getContainer()->get(ScopeRepositoryInterface::class);
        $centerRepository = self::getContainer()->get(CenterRepositoryInterface::class);

        if (null === $user = $entityManager->createQuery('SELECT u FROM '.User::class.' u')->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        if (null === $anotherUser = $entityManager->createQuery('SELECT u FROM '.User::class.' u WHERE u.id != :uid')->setParameter('uid', $user->getId())
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        $persons = $entityManager
            ->createQuery('SELECT p FROM '.Person::class.' p JOIN p.centerCurrent current_center')
            ->setMaxResults(4)
            ->getResult();

        if (4 > count($persons)) {
            throw new \RuntimeException('no person found');
        }

        /** @var Person $person */
        [$person, $anotherPerson, $person2, $person3] = $persons;

        $scopes = $scopeRepository->findAll();

        if (3 > count($scopes)) {
            throw new \RuntimeException('not enough scopes for this test');
        }
        $scopesCanSee = [$scopes[0]];
        $scopesGroup2 = [$scopes[1]];

        $centers = $centerRepository->findActive();
        $aCenterNotAssociatedToPerson = array_values(array_filter($centers, fn (Center $c) => $c !== $person->getCenter()))[0];

        if (2 > count($centers)) {
            throw new \RuntimeException('not enough centers for this test');
        }

        $period = self::buildPeriod($person, $scopesCanSee, $user, true);
        $period->setUser($user);

        yield [
            $anotherUser,
            $user,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [$period],
            [],
            'period should be visible with expected scopes',
        ];

        yield [
            $anotherUser,
            $user,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesGroup2,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [],
            [$period],
            'period should not be visible without expected scopes',
        ];

        yield [
            $anotherUser,
            $user,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesGroup2,
                    'scopeCanSeeConfidential' => [],
                ],
                [
                    'center' => $aCenterNotAssociatedToPerson,
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [],
            [$period],
            'period should not be visible for user having right in another scope (with multiple centers)',
        ];

        $period = self::buildPeriod($person, $scopesCanSee, $user, true);
        $period->setUser($user);
        $period->setConfidential(true);

        yield [
            $anotherUser,
            $user,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [],
            [$period],
            'period confidential should not be visible',
        ];

        yield [
            $anotherUser,
            $user,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => $scopesCanSee,
                ],
            ],
            [$period],
            [],
            'period confidential be visible if user has required scopes',
        ];

        $entityManager->flush();
    }

    /**
     * @dataProvider provideDataFindByUndispatched
     *
     * @param list<array{center: Center, scopeOnRole: list<Scope>, scopeCanSeeConfidential: list<Scope>}> $centerScopes
     * @param list<AccompanyingPeriod>                                                                    $expectedContains
     * @param list<AccompanyingPeriod>                                                                    $expectedNotContains
     */
    public function testFindByUndispatched(User $user, array $centerScopes, array $expectedContains, array $expectedNotContains, string $message): void
    {
        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        $authorizationHelper = $this->prophesize(AuthorizationHelperForCurrentUserInterface::class);
        $centers = [];

        foreach ($centerScopes as ['center' => $center, 'scopeOnRole' => $scopes, 'scopeCanSeeConfidential' => $scopesCanSeeConfidential]) {
            $centers[spl_object_hash($center)] = $center;
            $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE, $center)
                ->willReturn($scopes);
            $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL, $center)
                ->willReturn($scopesCanSeeConfidential);
        }
        $authorizationHelper->getReachableCenters(AccompanyingPeriodVoter::SEE)->willReturn(array_values($centers));

        $repository = new AccompanyingPeriodACLAwareRepository(
            $this->accompanyingPeriodRepository,
            $security->reveal(),
            $authorizationHelper->reveal(),
            $this->centerResolverManager
        );

        $actual = array_map(
            fn (AccompanyingPeriod $period) => $period->getId(),
            $repository->findByUnDispatched([], [], [], ['id' => 'DESC'], 20, 0)
        );

        foreach ($expectedContains as $expected) {
            self::assertContains($expected->getId(), $actual, $message);
        }
        foreach ($expectedNotContains as $expected) {
            self::assertNotContains($expected->getId(), $actual, $message);
        }
    }

    public static function provideDataFindByUndispatched(): iterable
    {
        self::bootKernel();
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $scopeRepository = self::getContainer()->get(ScopeRepositoryInterface::class);
        $centerRepository = self::getContainer()->get(CenterRepositoryInterface::class);

        if (null === $user = $entityManager->createQuery('SELECT u FROM '.User::class.' u')->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        if (null === $anotherUser = $entityManager->createQuery('SELECT u FROM '.User::class.' u WHERE u.id != :uid')->setParameter('uid', $user->getId())
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        /** @var Person $person */
        $persons = $entityManager
            ->createQuery('SELECT p FROM '.Person::class.' p ')
            ->setMaxResults(4)
            ->getResult();

        if (4 > count($persons)) {
            throw new \RuntimeException('no person found');
        }

        [$person, $anotherPerson, $person2, $person3] = $persons;

        $scopes = $scopeRepository->findAll();

        if (3 > count($scopes)) {
            throw new \RuntimeException('not enough scopes for this test');
        }
        $scopesCanSee = [$scopes[0]];
        $scopesGroup2 = [$scopes[1]];

        $centers = $centerRepository->findActive();

        if (2 > count($centers)) {
            throw new \RuntimeException('not enough centers for this test');
        }

        $period = self::buildPeriod($person, $scopesCanSee, $user, true);

        // expected scope: can see the period
        yield [
            $anotherUser,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [$period],
            [],
            'period should be visible with expected scopes',
        ];

        // no scope visible
        yield [
            $anotherUser,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesGroup2,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [],
            [$period],
            'period should not be visible without expected scopes',
        ];

        // another center
        yield [
            $anotherUser,
            [
                [
                    'center' => $person->getCenter(),
                    'scopeOnRole' => $scopesGroup2,
                    'scopeCanSeeConfidential' => [],
                ],
                [
                    'center' => array_values(array_filter($centers, fn (Center $c) => $c !== $person->getCenter()))[0],
                    'scopeOnRole' => $scopesCanSee,
                    'scopeCanSeeConfidential' => [],
                ],
            ],
            [],
            [$period],
            'period should not be visible for user having right in another scope (with multiple centers)',
        ];

        $entityManager->flush();
    }

    /**
     * For testing this method, we mock the authorization helper to return different Scope that a user
     * can see, or that a user can see confidential periods.
     *
     * @param array<Scope>              $scopeUserCanSee
     * @param array<Scope>              $scopeUserCanSeeConfidential
     * @param array<AccompanyingPeriod> $expectedPeriod
     *
     * @dataProvider provideDataForFindByPerson
     */
    public function testFindByPersonTestUser(User $user, Person $person, array $scopeUserCanSee, array $scopeUserCanSeeConfidential, array $expectedPeriod, string $message): void
    {
        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        $authorizationHelper = $this->prophesize(AuthorizationHelperForCurrentUserInterface::class);
        $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE, Argument::any())
            ->willReturn($scopeUserCanSee);
        $authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL, Argument::any())
            ->willReturn($scopeUserCanSeeConfidential);

        $repository = new AccompanyingPeriodACLAwareRepository(
            $this->accompanyingPeriodRepository,
            $security->reveal(),
            $authorizationHelper->reveal(),
            $this->centerResolverManager
        );

        $actuals = $repository->findByPerson($person, AccompanyingPeriodVoter::SEE);
        $expectedIds = array_map(fn (AccompanyingPeriod $period) => $period->getId(), $expectedPeriod);

        self::assertCount(count($expectedPeriod), $actuals, $message);
        foreach ($actuals as $actual) {
            self::assertContains($actual->getId(), $expectedIds);
        }
    }

    public static function provideDataForFindByPerson(): iterable
    {
        self::bootKernel();
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $scopeRepository = self::getContainer()->get(ScopeRepositoryInterface::class);

        if (null === $user = $entityManager->createQuery('SELECT u FROM '.User::class.' u')->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        if (null === $anotherUser = $entityManager->createQuery('SELECT u FROM '.User::class.' u WHERE u.id != :uid')->setParameter('uid', $user->getId())
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('no user found');
        }

        if (null === $centerA = $entityManager->createQuery('SELECT c FROM '.Center::class.' c WHERE c.name LIKE :cn')->setParameter('cn', 'Center A')
            ->setMaxResults(1)->getSingleResult()) {
            throw new \RuntimeException('Center A not found');
        }
        $persons = [new Person(), new Person(), new Person(), new Person()];
        $firstnames = ['alpha', 'beta', 'gamma', 'delta'];
        $lastnames = ['primo', 'secundo', 'tertio', 'quartro'];

        foreach ($persons as $k => $p) {
            $p
                ->setFirstName($firstnames[$k])
                ->setLastName($lastnames[$k])
                ->setBirthdate((new \DateTime('today'))->sub(new \DateInterval('P'.random_int(18, 100).'Y')))
                ->setCenter($centerA)
            ;
            $entityManager->persist($p);
            self::$entitiesToDelete[] = [Person::class, $p->getId()];
        }

        [$person, $anotherPerson, $person2, $person3] = $persons;

        $scopes = $scopeRepository->findAll();

        if (3 > count($scopes)) {
            throw new \RuntimeException('not enough scopes for this test');
        }
        $scopesCanSee = [$scopes[0]];
        $scopesGroup2 = [$scopes[1]];

        // case: a period is in draft state
        $period = self::buildPeriod($person, $scopesCanSee, $user, false);

        yield [$user, $person, $scopesCanSee, [], [$period], 'a user can see his period during draft state'];

        // another user is not allowed to see this period, because it is in DRAFT state
        yield [$anotherUser, $person, $scopesCanSee, [], [], 'another user is not allowed to see the period of someone else in draft state'];

        // the period is confirmed
        $period = self::buildPeriod($anotherPerson, $scopesCanSee, $user, true);

        // the other user can now see it
        yield [$user, $anotherPerson, $scopesCanSee, [], [$period], 'a user see his period when confirmed'];
        yield [$anotherUser, $anotherPerson, $scopesCanSee, [], [$period], 'another user with required scopes is allowed to see the period when not draft'];
        yield [$anotherUser, $anotherPerson, $scopesGroup2, [], [], 'another user without the required scopes is not allowed to see the period when not draft'];

        // this period will be confidential
        $period = self::buildPeriod($person2, $scopesCanSee, $user, true);
        $period->setConfidential(true)->setUser($user, true);

        yield [$user, $person2, $scopesCanSee, [], [$period], 'a user see his period when confirmed and confidential with required scopes'];
        yield [$user, $person2, $scopesGroup2, [], [$period], 'a user see his period when confirmed and confidential without required scopes'];
        yield [$anotherUser, $person2, $scopesCanSee, [], [], "a user don't see a confidential period, even if he has required scopes"];
        yield [$anotherUser, $person2, $scopesCanSee, $scopesCanSee, [$period], 'a user see the period when confirmed and confidential if he has required scope to see the period'];

        // period draft with creator = null
        $period = self::buildPeriod($person3, $scopesCanSee, null, false);
        yield [$user, $person3, $scopesCanSee, [], [$period], 'a user see a period when draft if no creator on the period'];
        $entityManager->flush();
    }

    /**
     * @param array<Scope> $scopes
     */
    private static function buildPeriod(Person $person, array $scopes, ?User $creator, bool $confirm): AccompanyingPeriod
    {
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $registry = self::getContainer()->get(Registry::class);

        $period = new AccompanyingPeriod();
        $period->addPerson($person);
        if (null !== $creator) {
            $period->setCreatedBy($creator);
        }

        foreach ($scopes as $scope) {
            $period->addScope($scope);
        }

        $entityManager->persist($period);
        self::$periodsIdsToDelete[] = $period->getId();

        if ($confirm) {
            $workflow = $registry->get($period, 'accompanying_period_lifecycle');
            $workflow->apply($period, 'confirm');
        }

        return $period;
    }
}
