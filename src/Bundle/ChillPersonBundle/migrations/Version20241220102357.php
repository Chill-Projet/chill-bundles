<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241220102357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add numberOfDependents and numberOfDependentsWithDisabilities';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_composition ADD numberOfDependents INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_household_composition ADD numberOfDependentsWithDisabilities INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_composition DROP numberOfDependents');
        $this->addSql('ALTER TABLE chill_person_household_composition DROP numberOfDependentsWithDisabilities');
    }
}
