<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * fix deprecated json array.
 */
final class Version20210419112619 extends AbstractMigration
{
    public function down(Schema $schema): void {}

    public function getDescription(): string
    {
        return 'fix deprecated json_array';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_closingmotive.name IS NULL');
    }
}
