<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add house holder on membership.
 */
final class Version20210528142121 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_members DROP COLUMN holder');
    }

    public function getDescription(): string
    {
        return 'Add house holder on membership';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_members ADD holder BOOLEAN DEFAULT FALSE  NOT NULL');
    }
}
