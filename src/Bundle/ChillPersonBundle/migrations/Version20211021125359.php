<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Custom constraint added to database to prevent identical participations.
 */
final class Version20211021125359 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX participation_unique ON chill_person_accompanying_period_participation (accompanyingperiod_id, person_id)');
    }

    public function getDescription(): string
    {
        return 'Custom constraint added to database to prevent identical participations.';
    }

    public function up(Schema $schema): void
    {
        // creates a constraint 'participations may not overlap'
        $this->addSql('ALTER TABLE chill_person_accompanying_period_participation ADD CONSTRAINT '.
            'participations_no_overlap EXCLUDE USING GIST(
            -- extension btree_gist required to include comparaison with integer
            person_id WITH =, accompanyingperiod_id WITH =,
            daterange(startdate, enddate) WITH &&
        )
        INITIALLY DEFERRED');
    }
}
