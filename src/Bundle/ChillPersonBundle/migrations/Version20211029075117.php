<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * isActive property added to Relation entity.
 */
final class Version20211029075117 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_relations DROP isActive');
    }

    public function getDescription(): string
    {
        return 'isActive property added to Relation entity.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_relations ADD isActive BOOLEAN DEFAULT true NOT NULL');
    }
}
