<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241127160628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add administrative status';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_administrative_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_administrative_status (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, ordering DOUBLE PRECISION DEFAULT \'0.0\', PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_person_person ADD administrativeStatus_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD CONSTRAINT FK_BF210A146E64B00C FOREIGN KEY (administrativeStatus_id) REFERENCES chill_person_administrative_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BF210A146E64B00C ON chill_person_person (administrativeStatus_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_person_administrative_status_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_person_person DROP CONSTRAINT FK_BF210A146E64B00C');
        $this->addSql('DROP TABLE chill_person_administrative_status');
        $this->addSql('ALTER TABLE chill_person_person DROP administrativeStatus_id');
    }
}
