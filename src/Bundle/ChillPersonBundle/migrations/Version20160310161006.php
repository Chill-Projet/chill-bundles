<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a many-to-many relationship between person and addresses.
 */
class Version20160310161006 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE chill_person_persons_to_addresses');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE chill_person_persons_to_addresses ('
              .'person_id INT NOT NULL, '
              .'address_id INT NOT NULL, '
              .'PRIMARY KEY(person_id, address_id))');
        $this->addSql('CREATE INDEX IDX_4655A196217BBB47 '
              .'ON chill_person_persons_to_addresses (person_id)');
        $this->addSql('CREATE INDEX IDX_4655A196F5B7AF75 '
              .'ON chill_person_persons_to_addresses (address_id)');
        $this->addSql('ALTER TABLE chill_person_persons_to_addresses '
              .'ADD CONSTRAINT FK_4655A196217BBB47 '
              .'FOREIGN KEY (person_id) '
              .'REFERENCES Person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_persons_to_addresses '
              .'ADD CONSTRAINT FK_4655A196F5B7AF75 '
              .'FOREIGN KEY (address_id) '
              .'REFERENCES chill_main_address (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
