<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241104142216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Clean migration diverse';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_marital_status ALTER id TYPE VARCHAR(7)');
        $this->addSql('ALTER TABLE chill_person_person DROP cfdata_old');
        $this->addSql('ALTER TABLE chill_person_person ALTER maritalstatus_id TYPE VARCHAR(7)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ADD cfdata_old TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER cFData DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER maritalStatus_id TYPE VARCHAR(10)');
        $this->addSql('COMMENT ON COLUMN chill_person_person.cfdata_old IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE chill_person_marital_status ALTER id TYPE VARCHAR(10)');
    }
}
