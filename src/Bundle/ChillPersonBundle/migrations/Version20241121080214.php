<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241121080214 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add employment status';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_employment_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_employment_status (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, ordering DOUBLE PRECISION DEFAULT \'0.0\', PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_person_person ADD employmentstatus_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD CONSTRAINT FK_BF210A14BAE6AEE2 FOREIGN KEY (employmentstatus_id) REFERENCES chill_person_employment_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX BF210A14BAE6AEE2 ON chill_person_person (employmentstatus_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_person_employment_status_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_person_person DROP CONSTRAINT FK_BF210A14BAE6AEE2');
        $this->addSql('ALTER TABLE chill_person_person DROP employmentstatus_id');
        $this->addSql('DROP TABLE chill_person_employment_status');
    }
}
