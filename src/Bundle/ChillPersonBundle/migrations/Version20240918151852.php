<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918151852 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set not null and defaults on columns';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_participation ALTER startdate DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_user_history ALTER startdate SET DEFAULT \'now()\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER privatecomment_comments SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label SET DEFAULT \'{}\'');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_relationships ALTER createdby_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_relationships ALTER createdat DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdby_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdat DROP NOT NULL');
    }

    public function down(Schema $schema): void {}
}
