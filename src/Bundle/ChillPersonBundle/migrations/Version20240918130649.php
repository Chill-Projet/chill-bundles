<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918130649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add comments on columns';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN chill_person_household_composition.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_household_composition.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_household_members.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_household_members.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_person.phonenumber IS \'(DC2Type:phone_number)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_person.mobilenumber IS \'(DC2Type:phone_number)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_phone.phonenumber IS \'(DC2Type:phone_number)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_resource.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_resource.updatedAt IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void {}
}
