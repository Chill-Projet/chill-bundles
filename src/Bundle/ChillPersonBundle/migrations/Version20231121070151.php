<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Change gender instances of 'neuter' to 'both'.
 */
final class Version20231121070151 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Change gender instances of "neuter" to "both"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE chill_person_person SET gender = 'both' WHERE chill_person_person.gender = 'neuter'");
    }

    public function down(Schema $schema): void {}
}
