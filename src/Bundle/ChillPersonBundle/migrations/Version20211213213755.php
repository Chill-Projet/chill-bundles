<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211213213755 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_social_action DROP ordering');
        $this->addSql('ALTER TABLE chill_person_social_issue DROP ordering');
    }

    public function getDescription(): string
    {
        return 'Add ordering to social issue and social actions';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_social_action ADD ordering DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE chill_person_social_issue ADD ordering DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
    }
}
