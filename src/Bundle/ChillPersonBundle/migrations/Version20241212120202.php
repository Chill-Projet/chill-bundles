<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241212120202 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Move the title from accompanying period work evaluation document to stored object';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE chill_doc.stored_object SET title = doc.title FROM chill_person_accompanying_period_work_evaluation_document doc WHERE storedobject_id = stored_object.id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document DROP title');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation_document ADD title TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('UPDATE chill_person_accompanying_period_work_evaluation_document  SET title = so.title FROM chill_doc.stored_object so WHERE storedobject_id = so.id');
    }
}
