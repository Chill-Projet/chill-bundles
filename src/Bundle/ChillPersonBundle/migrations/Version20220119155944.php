<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Creation of person resource and person resource kind.
 */
final class Version20220119155944 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_person_resource_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_resource_kind_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_person_resource');
        $this->addSql('DROP TABLE chill_person_resource_kind');
    }

    public function getDescription(): string
    {
        return 'Creation of person resource and person resource kind';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_resource_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_resource_kind_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_resource (id INT NOT NULL, person_id INT DEFAULT NULL, kind_id INT DEFAULT NULL, freeText TEXT DEFAULT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, comment_comment TEXT DEFAULT NULL, comment_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, comment_userId INT DEFAULT NULL, personOwner_id INT NOT NULL, thirdParty_id INT DEFAULT NULL, createdBy_id INT NOT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FA5B7D92D19C515B ON chill_person_resource (personOwner_id)');
        $this->addSql('CREATE INDEX IDX_FA5B7D92217BBB47 ON chill_person_resource (person_id)');
        $this->addSql('CREATE INDEX IDX_FA5B7D923EA5CAB0 ON chill_person_resource (thirdParty_id)');
        $this->addSql('CREATE INDEX IDX_FA5B7D9230602CA9 ON chill_person_resource (kind_id)');
        $this->addSql('CREATE INDEX IDX_FA5B7D923174800F ON chill_person_resource (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_FA5B7D9265FF1AEC ON chill_person_resource (updatedBy_id)');
        $this->addSql('CREATE TABLE chill_person_resource_kind (id INT NOT NULL, title JSON NOT NULL, isActive BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D92D19C515B FOREIGN KEY (personOwner_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D92217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D923EA5CAB0 FOREIGN KEY (thirdParty_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D9230602CA9 FOREIGN KEY (kind_id) REFERENCES chill_person_resource_kind (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D923174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_resource ADD CONSTRAINT FK_FA5B7D9265FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
