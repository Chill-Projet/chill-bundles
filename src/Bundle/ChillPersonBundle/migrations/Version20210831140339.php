<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Allow to create persons without center.
 */
final class Version20210831140339 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ALTER center_id SET NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Allow to create persons without center';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ALTER center_id DROP NOT NULL');
    }
}
