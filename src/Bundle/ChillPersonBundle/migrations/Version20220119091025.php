<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add userJob to AccompanyingPeriod.
 */
final class Version20220119091025 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP CONSTRAINT FK_E260A868BE04EA9');
        $this->addSql('DROP INDEX IDX_E260A868BE04EA9');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP job_id');
    }

    public function getDescription(): string
    {
        return 'Add userJob to AccompanyingPeriod';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A868BE04EA9 FOREIGN KEY (job_id) REFERENCES chill_main_user_job (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E260A868BE04EA9 ON chill_person_accompanying_period (job_id)');
    }
}
