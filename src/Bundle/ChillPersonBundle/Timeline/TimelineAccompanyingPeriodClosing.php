<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Timeline;

use Chill\PersonBundle\Entity\AccompanyingPeriod;

/**
 * Provide information for opening periods to timeline.
 */
class TimelineAccompanyingPeriodClosing extends AbstractTimelineAccompanyingPeriod
{
    public function fetchQuery($context, array $args)
    {
        $metadata = $this->em
            ->getClassMetadata(AccompanyingPeriod::class);

        $query = $this->basicFetchQuery($context, $args);
        [$where, $parameters] = $this->buildWhereClause($context, $args);
        $query->setKey('accompanying_period_closing')
            ->setDate($metadata->getColumnName('closingDate'))
            ->setWhere($where)
            ->setParameters($parameters);

        return $query;
    }

    public function getEntityTemplate($entity, $context, array $args)
    {
        return $this->getBasicEntityTemplate(
            '@ChillPerson/Timeline/closing_period.html.twig',
            $entity,
            $context,
            $args
        );
    }

    public function supportsType($type)
    {
        return 'accompanying_period_closing' === $type;
    }

    protected function buildWhereClause($context, array $args): array
    {
        [$query, $params] = parent::buildWhereClause($context, $args);
        $period = $this->em->getClassMetadata(AccompanyingPeriod::class);

        $query .= " AND {$period->getColumnName('closingDate')} IS NOT NULL ";

        return [$query, $params];
    }
}
