<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Add a role CHILL_PERSON_UPDATE & CHILL_PERSON_CREATE for all groups except administrative,
 * and a role CHILL_PERSON_SEE for administrative.
 */
class LoadPersonACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 9600;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef, PermissionsGroup::class);
            $scopeSocial = $this->getReference('scope_social', Scope::class);

            // create permission group
            switch ($permissionsGroup->getName()) {
                case 'social':
                case 'direction':
                    $permissionsGroup->addRoleScope(
                        (new RoleScope())
                            ->setRole(AccompanyingPeriodVoter::FULL)
                            ->setScope($scopeSocial)
                    );

                    $roleScopeUpdate = (new RoleScope())
                        ->setRole('CHILL_PERSON_UPDATE')
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeUpdate);

                    $roleScopeCreate = (new RoleScope())
                        ->setRole('CHILL_PERSON_CREATE')
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeCreate);

                    $roleScopeDuplicate = (new RoleScope())
                        ->setRole('CHILL_PERSON_DUPLICATE')
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeDuplicate);

                    $roleScopeList = (new RoleScope())
                        ->setRole(PersonVoter::LISTS)
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeList);

                    $roleScopeStats = (new RoleScope())
                        ->setRole(PersonVoter::STATS)
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeStats);

                    $manager->persist($roleScopeUpdate);
                    $manager->persist($roleScopeCreate);
                    $manager->persist($roleScopeDuplicate);

                    break;

                case 'administrative':
                    $roleScopeSee = (new RoleScope())
                        ->setRole('CHILL_PERSON_SEE')
                        ->setScope(null);
                    $permissionsGroup->addRoleScope($roleScopeSee);
                    $manager->persist($roleScopeSee);

                    break;
            }
        }

        $manager->flush();
    }
}
