<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\Household\Position;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LoadHouseholdPosition extends Fixture
{
    final public const ADULT = 'position_adulte';

    final public const CHILD = 'position_enfant';

    final public const CHILD_OUT = 'position_enfant_hors';

    final public const POSITIONS_DATA = [
        ['Adulte', true, true, 1.0, self::ADULT],
        ['Enfant', true, false, 2.0, self::CHILD],
        ['Enfant hors ménage', false, false, 3.0, self::CHILD_OUT],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (
            self::POSITIONS_DATA as [$name, $share, $allowHolder,
                $ordering, $ref, ]
        ) {
            $position = (new Position())
                ->setLabel(['fr' => $name])
                ->setAllowHolder($allowHolder)
                ->setShareHousehold($share)
                ->setOrdering($ordering);

            $manager->persist($position);
            $this->addReference($ref, $position);
        }

        $manager->flush();
    }
}
