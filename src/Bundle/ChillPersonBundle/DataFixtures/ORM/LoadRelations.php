<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\Relationships\Relation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadRelations extends Fixture implements FixtureGroupInterface
{
    final public const RELATION_KEY = 'relations';

    final public const RELATIONS = [
        ['title' => ['fr' => 'Parent'], 'reverseTitle' => ['fr' => 'Enfant']],
        ['title' => ['fr' => 'En couple'], 'reverseTitle' => ['fr' => 'En couple']],
        ['title' => ['fr' => 'Beau parent'], 'reverseTitle' => ['fr' => 'Belle-fille·beau-fils']],
        ['title' => ['fr' => 'Frère·Sœur'], 'reverseTitle' => ['fr' => 'Frère·Sœur']],
        ['title' => ['fr' => 'Demi-frère·sœur'], 'reverseTitle' => ['fr' => 'Demi-frère·sœur']],
        ['title' => ['fr' => 'Grand-parent'], 'reverseTitle' => ['fr' => 'Petit-enfant']],
        ['title' => ['fr' => 'Oncle·Tante'], 'reverseTitle' => ['fr' => 'Neveu·Nièce']],
    ];

    public static function getGroups(): array
    {
        return ['person_relations'];
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::RELATIONS as $key => $value) {
            echo 'Creating a new relation type: relation'.$value['title']['fr'].'reverse relation: '.$value['reverseTitle']['fr']."\n";
            $relation = new Relation();
            $relation->setTitle($value['title'])
                ->setReverseTitle($value['reverseTitle']);
            $manager->persist($relation);

            $this->addReference(self::RELATION_KEY.$key, $relation);
        }

        $manager->flush();
    }
}
