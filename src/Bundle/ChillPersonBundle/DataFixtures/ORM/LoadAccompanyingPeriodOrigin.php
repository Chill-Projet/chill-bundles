<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Origin;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Description of LoadAccompanyingPeriodOrigin.
 */
class LoadAccompanyingPeriodOrigin extends AbstractFixture implements OrderedFixtureInterface
{
    final public const ACCOMPANYING_PERIOD_ORIGIN = 'accompanying_period_origin';

    public static $references = [];

    private array $phoneCall = ['en' => 'phone call', 'fr' => 'appel téléphonique'];

    public function getOrder(): int
    {
        return 9000;
    }

    public function load(ObjectManager $manager): void
    {
        $o = new Origin();
        $o->setLabel($this->phoneCall);

        $manager->persist($o);

        $this->addReference(self::ACCOMPANYING_PERIOD_ORIGIN, $o);
        echo "Adding one AccompanyingPeriod Origin\n";

        $manager->flush();
    }
}
