<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\AdministrativeStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadAdministrativeStatus extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['administrative_status'];
    }

    public function load(ObjectManager $manager): void
    {
        $status = [
            ['name' => ['fr' => 'situation administrative régulière']],
            ['name' => ['fr' => 'sans papier']],
            ['name' => ['fr' => 'séjour provisoire']],
        ];

        foreach ($status as $val) {
            $administrativeStatus = (new AdministrativeStatus())
                ->setName($val['name'])
                ->setActive(true);
            $manager->persist($administrativeStatus);
        }

        $manager->flush();
    }
}
