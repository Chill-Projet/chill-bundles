<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\MainBundle\DataFixtures\ORM\LoadAbstractNotificationsTrait;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Load notififications into database.
 */
class LoadAccompanyingPeriodNotifications extends AbstractFixture implements DependentFixtureInterface
{
    use LoadAbstractNotificationsTrait;

    public $notifs = [
        [
            'message' => 'Hello !',
            'entityClass' => AccompanyingPeriod::class,
            'entityRef' => null,
            'sender' => 'center a_social',
            'addressees' => [
                'center a_social',
                'center a_administrative',
                'center a_direction',
                'multi_center',
            ],
        ],
    ];

    public function getDependencies(): array
    {
        return [
            LoadPeople::class,
        ];
    }

    protected function getEntityRef()
    {
        return null;
    }
}
