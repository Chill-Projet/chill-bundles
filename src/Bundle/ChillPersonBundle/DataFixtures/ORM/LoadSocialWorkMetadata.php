<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Service\Import\SocialWorkMetadata;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;

class LoadSocialWorkMetadata extends Fixture implements OrderedFixtureInterface
{
    public function __construct(private readonly SocialWorkMetadata $importer) {}

    public function getOrder(): int
    {
        return 9500;
    }

    public function load(ObjectManager $manager): void
    {
        try {
            $csv = Reader::createFromPath(__DIR__.'/data/social_work_metadata.csv');
        } catch (\Throwable $e) {
            throw new \Exception('Error while loading CSV.', 0, $e);
        }

        $csv->setDelimiter(';');

        $this->importer->import($csv);
    }
}
