<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\EmploymentStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadEmploymentStatus extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['employment_status'];
    }

    public function load(ObjectManager $manager): void
    {
        $status = [
            ['name' => ['fr' => 'Salarié·e']],
            ['name' => ['fr' => 'Indépendant·e']],
            ['name' => ['fr' => 'Chômeur·euse']],
            ['name' => ['fr' => 'Bénéficiaire du CPAS']],
            ['name' => ['fr' => 'Pensionsé·e']],
        ];

        foreach ($status as $val) {
            $employmentStatus = (new EmploymentStatus())
                ->setName($val['name'])
                ->setActive(true);
            $manager->persist($employmentStatus);
        }

        $manager->flush();
    }
}
