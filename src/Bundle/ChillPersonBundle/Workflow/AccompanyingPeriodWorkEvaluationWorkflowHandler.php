<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkEvaluationVoter;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvidePersonsAssociated;
use Chill\PersonBundle\Service\AccompanyingPeriodWork\ProvideThirdPartiesAssociated;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements EntityWorkflowHandlerInterface<AccompanyingPeriodWorkEvaluation>
 */
readonly class AccompanyingPeriodWorkEvaluationWorkflowHandler implements EntityWorkflowHandlerInterface
{
    public function __construct(
        private AccompanyingPeriodWorkEvaluationRepository $repository,
        private EntityWorkflowRepository $workflowRepository,
        private TranslatableStringHelperInterface $translatableStringHelper,
        private TranslatorInterface $translator,
        private ProvideThirdPartiesAssociated $provideThirdPartiesAssociated,
        private ProvidePersonsAssociated $providePersonsAssociated,
    ) {}

    public function getDeletionRoles(): array
    {
        return ['_'];
    }

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $evaluation = $this->getRelatedEntity($entityWorkflow);

        return [
            'persons' => $evaluation->getAccompanyingPeriodWork()->getPersons(),
        ];
    }

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        $evaluation = $this->getRelatedEntity($entityWorkflow);

        return $this->translator->trans(
            'entity_display_title.Evaluation (n°%eval%)',
            ['%eval%' => $entityWorkflow->getRelatedEntityId()]
        ).' - '.$this->translatableStringHelper->localize($evaluation->getEvaluation()->getTitle());
    }

    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?AccompanyingPeriodWorkEvaluation
    {
        return $this->repository->find($entityWorkflow->getRelatedEntityId());
    }

    public function getRelatedAccompanyingPeriod(EntityWorkflow $entityWorkflow): ?AccompanyingPeriod
    {
        return $this->getRelatedEntity($entityWorkflow)?->getAccompanyingPeriodWork()->getAccompanyingPeriod();
    }

    public function getRelatedObjects(object $object): array
    {
        $relateds = [];
        $relateds[] = ['entityClass' => AccompanyingPeriodWorkEvaluation::class, 'entityId' => $object->getId()];

        foreach ($object->getDocuments() as $doc) {
            $relateds[] = ['entityClass' => AccompanyingPeriodWorkEvaluationDocument::class, 'entityId' => $doc->getId()];
        }

        return $relateds;
    }

    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string
    {
        return AccompanyingPeriodWorkEvaluationVoter::SEE;
    }

    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        $users = [];
        if (null !== $referrer = $related->getAccompanyingPeriodWork()
            ->getAccompanyingPeriod()
            ->getUser()
        ) {
            $users[] = $referrer;
        }

        foreach ($related->getAccompanyingPeriodWork()
            ->getReferrersHistoryCurrent() as $referrerHistory
        ) {
            $users[] = $referrerHistory->getUser();
        }

        return array_values(
            // filter objects to remove duplicates
            array_filter(
                $users,
                fn ($o, $k) => array_search($o, $users, true) === $k,
                ARRAY_FILTER_USE_BOTH
            )
        );
    }

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        return '@ChillPerson/Workflow/_evaluation.html.twig';
    }

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        return [
            'entity_workflow' => $entityWorkflow,
            'evaluation' => $this->getRelatedEntity($entityWorkflow),
        ];
    }

    public function isObjectSupported(object $object): bool
    {
        return $object instanceof AccompanyingPeriodWorkEvaluation;
    }

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluation::class === $entityWorkflow->getRelatedEntityClass();
    }

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return false;
    }

    public function findByRelatedEntity(object $object): array
    {
        if (!$object instanceof AccompanyingPeriodWorkEvaluation) {
            return [];
        }

        return $this->workflowRepository->findByRelatedEntity(AccompanyingPeriodWorkEvaluation::class, $object->getId());
    }

    public function getSuggestedPersons(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        return $this->providePersonsAssociated->getPersonsAssociated($related->getAccompanyingPeriodWork());
    }

    public function getSuggestedThirdParties(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        return $this->provideThirdPartiesAssociated->getThirdPartiesAssociated($related->getAccompanyingPeriodWork());
    }
}
