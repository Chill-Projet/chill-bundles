<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Serializer\Normalizer;

use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AccompanyingPeriodParticipationNormalizer implements NormalizerAwareInterface, NormalizerInterface
{
    private ?NormalizerInterface $normalizer = null;

    /**
     * @param AccompanyingPeriodParticipation $participation
     * @param string|null                     $format
     */
    public function normalize($participation, $format = null, array $context = [])
    {
        return [
            'id' => $participation->getId(),
            'startDate' => $this->normalizer->normalize($participation->getStartDate(), $format),
            'endDate' => $this->normalizer->normalize($participation->getEndDate(), $format),
            'person' => $this->normalizer->normalize($participation->getPerson(), $format),
        ];
    }

    public function setNormalizer(NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        // @TODO Fix this.
        return false;

        return $data instanceof AccompanyingPeriodParticipation;
    }
}
