<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Serializer\Normalizer;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\ObjectToPopulateTrait;

/**
 * This denormalizer rely on AbstractNormalizer for most of the job, and
 * add some logic for synchronizing collection.
 */
class AccompanyingPeriodWorkEvaluationDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    use ObjectToPopulateTrait;

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $evaluation = $this->denormalizer->denormalize($data, $type, $format, \array_merge(
            $context,
            ['skip' => self::class]
        ));

        $this->handleDocumentCollection($data, $evaluation, $format, $context);

        return $evaluation;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return AccompanyingPeriodWorkEvaluation::class === $type
            && self::class !== ($context['skip'] ?? null)
            && \is_array($data)
            && \array_key_exists('type', $data)
            && 'accompanying_period_work_evaluation' === $data['type'];
    }

    private function handleDocumentCollection(array $data, AccompanyingPeriodWorkEvaluation $evaluation, string $format, array $context)
    {
        $dataById = [];
        $dataWithoutId = [];

        foreach ($data['documents'] as $e) {
            if (\array_key_exists('id', $e)) {
                $dataById[$e['id']] = $e;
            } else {
                $dataWithoutId[] = $e;
            }
        }

        // partition the separate kept documents and removed one
        [$kept, $removed] = $evaluation->getDocuments()
            ->partition(
                static fn (int $key, AccompanyingPeriodWorkEvaluationDocument $a) => \array_key_exists($a->getId(), $dataById)
            );

        // remove the document from evaluation
        foreach ($removed as $r) {
            $evaluation->removeDocument($r);
        }

        // handle the documents kept
        foreach ($kept as $k) {
            $this->denormalizer->denormalize(
                $dataById[$k->getId()],
                AccompanyingPeriodWorkEvaluationDocument::class,
                $format,
                \array_merge(
                    $context,
                    [
                        'groups' => ['write'],
                        AbstractNormalizer::OBJECT_TO_POPULATE => $k,
                    ]
                )
            );
        }
        // create new document
        foreach ($dataWithoutId as $newData) {
            $document = $this->denormalizer->denormalize(
                $newData,
                AccompanyingPeriodWorkEvaluationDocument::class,
                $format,
                \array_merge(
                    $context,
                    ['groups' => ['accompanying_period_work_evaluation:create']]
                )
            );
            $evaluation->addDocument($document);
        }
    }
}
