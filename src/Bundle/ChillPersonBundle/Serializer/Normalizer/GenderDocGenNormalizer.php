<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Serializer\Normalizer;

use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

class GenderDocGenNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return $data instanceof Gender;
    }

    public function normalize($gender, ?string $format = null, array $context = [])
    {
        return [
            'id' => $gender->getId(),
            'label' => $this->translatableStringHelper->localize($gender->getLabel()),
            'genderTranslation' => $gender->getGenderTranslation(),
        ];
    }
}
