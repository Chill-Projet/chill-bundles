<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\CommentType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Repository\Household\HouseholdCompositionTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HouseholdCompositionType extends AbstractType
{
    private array $household_fields_visibility;

    public function __construct(
        private readonly HouseholdCompositionTypeRepository $householdCompositionTypeRepository,
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
        protected ParameterBagInterface $parameterBag,
    ) {
        $this->household_fields_visibility = $parameterBag->get('chill_person.household_fields');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = $this->householdCompositionTypeRepository->findAllActive();

        $builder
            ->add('householdCompositionType', EntityType::class, [
                'class' => \Chill\PersonBundle\Entity\Household\HouseholdCompositionType::class,
                'choices' => $types,
                'choice_label' => fn (\Chill\PersonBundle\Entity\Household\HouseholdCompositionType $type) => $this->translatableStringHelper->localize($type->getLabel()),
                'label' => 'household_composition.Household composition',
            ])
            ->add('startDate', ChillDateType::class, [
                'required' => true,
                'input' => 'datetime_immutable',
            ])
            ->add('numberOfChildren', IntegerType::class, [
                'required' => true,
                'label' => 'household_composition.numberOfChildren',
            ]);
        if ('visible' == $this->household_fields_visibility['number_of_dependents']) {
            $builder
                ->add('numberOfDependents', IntegerType::class, [
                    'required' => true,
                    'label' => 'household_composition.numberOfDependents',
                ])
                ->add('numberOfDependentsWithDisabilities', IntegerType::class, [
                    'required' => true,
                    'label' => 'household_composition.numberOfDependentsWithDisabilities',
                ]);
        }
        $builder
            ->add('comment', CommentType::class, [
                'required' => false,
            ]);
    }
}
