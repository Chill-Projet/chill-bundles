<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\Type;

use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A type to select the civil union state.
 */
class PickGenderType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('label', 'Gender')
            ->setDefault(
                'choice_label',
                fn (Gender $gender): string => $this->translatableStringHelper->localize($gender->getLabel())
            )
            ->setDefault(
                'query_builder',
                static fn (EntityRepository $er): QueryBuilder => $er->createQueryBuilder('g')
                    ->where('g.active = true')
                    ->orderBy('g.order'),
            )
            ->setDefault('placeholder', 'choose gender')
            ->setDefault('class', Gender::class);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
