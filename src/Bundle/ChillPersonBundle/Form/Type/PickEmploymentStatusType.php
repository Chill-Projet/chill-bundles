<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\EmploymentStatus;

class PickEmploymentStatusType extends AbstractType
{
    public function __construct(
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
    ) {}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('label', 'Employment status')
            ->setDefault(
                'choice_label',
                fn (EmploymentStatus $employmentStatus): string => $this->translatableStringHelper->localize($employmentStatus->getName())
            )
            ->setDefault(
                'query_builder',
                static fn (EntityRepository $er): QueryBuilder => $er->createQueryBuilder('c')
                    ->where('c.active = true')
                    ->orderBy('c.order'),
            )
            ->setDefault('placeholder', $this->translatableStringHelper->localize(['Select an option…']))
            ->setDefault('class', EmploymentStatus::class);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
