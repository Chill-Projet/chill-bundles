<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\CommentType;
use Chill\MainBundle\Form\Type\PickAddressType;
use Chill\PersonBundle\Entity\Person\ResidentialAddress;
use Chill\PersonBundle\Form\Type\PickPersonDynamicType;
use Chill\ThirdPartyBundle\Form\Type\PickThirdpartyDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ResidentialAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, [
                'required' => true,
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
            ])
            ->add('endDate', DateType::class, [
                'required' => false,
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
            ])
            ->add('comment', CommentType::class, [
                'required' => false,
            ]);

        if ('person' === $options['kind']) {
            $builder
                ->add('hostPerson', PickPersonDynamicType::class, [
                    'label' => 'Person',
                ]);
        }

        if ('thirdparty' === $options['kind']) {
            $builder
                ->add('hostThirdParty', PickThirdpartyDynamicType::class, [
                    'label' => 'Third party',
                ]);
        }

        if ('address' === $options['kind']) {
            $builder
                ->add('address', PickAddressType::class, [
                    'required' => false,
                    'label' => 'Address',
                    'use_valid_from' => false,
                    'use_valid_to' => false,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResidentialAddress::class,
            'kind' => null,
        ]);
    }
}
