<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\SocialWork;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResultType.
 */
class ResultType extends AbstractType
{
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TranslatableStringFormType::class, [
                'label' => 'Nom',
            ])

            ->add('desactivationDate', ChillDateType::class, [
                'required' => false,
                'label' => 'goal.desactivationDate',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', Result::class);
    }
}
