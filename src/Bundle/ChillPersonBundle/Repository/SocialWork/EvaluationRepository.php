<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class EvaluationRepository implements EvaluationRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Evaluation::class);
    }

    public function find($id): ?Evaluation
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<int, Evaluation>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllActive(): array
    {
        return $this->findBy(['active' => true]);
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, Evaluation>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Evaluation
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return Evaluation::class;
    }
}
