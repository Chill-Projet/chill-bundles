<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

final readonly class SocialIssueRepository implements ObjectRepository
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(SocialIssue::class);
    }

    public function find($id): ?SocialIssue
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<int, SocialIssue>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllOrdered(): array
    {
        return $this->repository->createQueryBuilder('si')
            ->orderBy('si.ordering', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array|SocialIssue[]
     */
    public function findAllActive(): array
    {
        return $this->buildQueryWithDesactivatedDateCriteria()->getQuery()->getResult();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, SocialIssue>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?SocialIssue
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return SocialIssue::class;
    }

    private function buildQueryWithDesactivatedDateCriteria(): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('si');

        $qb->where('si.desactivationDate is null')
            ->orWhere('si.desactivationDate > :now')
            ->orderBy('si.ordering', 'ASC')
            ->setParameter('now', new \DateTime('now'));

        return $qb;
    }
}
