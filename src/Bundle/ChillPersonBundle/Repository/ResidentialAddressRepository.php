<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\Person\ResidentialAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResidentialAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResidentialAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResidentialAddress[]    findAll()
 * @method ResidentialAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResidentialAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResidentialAddress::class);
    }

    public function countByPerson(Person $person): int
    {
        return $this->createQueryBuilder('ra')
            ->select('COUNT(ra)')
            ->where('ra.person = :person')
            ->setParameter('person', $person)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function buildQueryFindCurrentResidentialAddresses(Person $person, ?\DateTimeImmutable $at = null): QueryBuilder
    {
        $date = $at ?? new \DateTimeImmutable('today');
        $qb = $this->createQueryBuilder('ra');

        $dateFilter = $qb->expr()->andX(
            $qb->expr()->lte('ra.startDate', ':dateIn'),
            $qb->expr()->orX(
                $qb->expr()->isNull('ra.endDate'),
                $qb->expr()->gte('ra.endDate', ':dateIn')
            )
        );

        $qb
            ->where($dateFilter)
            ->setParameter('dateIn', $date, Types::DATE_IMMUTABLE)
            ->andWhere('ra.person = :person')
            ->setParameter('person', $person);

        return $qb;
    }

    /**
     * @return array|ResidentialAddress[]|null
     */
    public function findCurrentResidentialAddressByPerson(Person $person, ?\DateTimeImmutable $at = null): array
    {
        return $this->buildQueryFindCurrentResidentialAddresses($person, $at)
            ->select('ra')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return ResidentialAddress[] Returns an array of ResidentialAddress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
     */

    /*
    public function findOneBySomeField($value): ?ResidentialAddress
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
     */
}
