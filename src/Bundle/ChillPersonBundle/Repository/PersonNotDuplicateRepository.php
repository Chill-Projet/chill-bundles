<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\PersonNotDuplicate;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class PersonNotDuplicateRepository
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(PersonNotDuplicate::class);
    }

    public function findNotDuplicatePerson(Person $person): array
    {
        $qb = $this->repository->createQueryBuilder('pnd');
        $qb->select('pnd')
            ->where('pnd.person1 = :person OR pnd.person2 = :person');
        $qb->setParameter('person', $person);
        $result = $qb->getQuery()->getResult();

        $persons = [];

        foreach ($result as $row) {
            if ($row->getPerson1() === $person) {
                $persons[] = $row->getPerson2();
            } elseif ($row->getPerson2() === $person) {
                $persons[] = $row->getPerson1();
            }
        }

        return $persons;
    }
}
