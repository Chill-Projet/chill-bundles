<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Security\Authorization\AuthorizationHelperForCurrentUserInterface;
use Chill\MainBundle\Security\Resolver\CenterResolverManagerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Repository\AccompanyingPeriodACLAwareRepositoryTest;
use Symfony\Component\Security\Core\Security;

/**
 * @see AccompanyingPeriodACLAwareRepositoryTest
 */
final readonly class AccompanyingPeriodACLAwareRepository implements AccompanyingPeriodACLAwareRepositoryInterface
{
    public function __construct(
        private AccompanyingPeriodRepository $accompanyingPeriodRepository,
        private Security $security,
        private AuthorizationHelperForCurrentUserInterface $authorizationHelper,
        private CenterResolverManagerInterface $centerResolver,
    ) {}

    public function buildQueryOpenedAccompanyingCourseByUserAndPostalCodes(?User $user, array $postalCodes = []): QueryBuilder
    {
        $qb = $this->accompanyingPeriodRepository->createQueryBuilder('ap');

        $qb->where($qb->expr()->eq('ap.user', ':user'))
            ->andWhere(
                $qb->expr()->neq('ap.step', ':draft'),
                $qb->expr()->neq('ap.step', ':closed'),
            )
            ->setParameter('user', $user)
            ->setParameter('draft', AccompanyingPeriod::STEP_DRAFT)
            ->setParameter('closed', AccompanyingPeriod::STEP_CLOSED);

        if ([] !== $postalCodes) {
            $qb->join('ap.locationHistories', 'location_history', Join::WITH, 'location_history.endDate IS NULL')
                ->leftJoin(Person\PersonCurrentAddress::class, 'person_address', Join::WITH, 'IDENTITY(location_history.personLocation) = IDENTITY(person_address.person)')
                ->join(
                    Address::class,
                    'address',
                    Join::WITH,
                    'COALESCE(IDENTITY(person_address.address), IDENTITY(location_history.addressLocation)) = address.id'
                )
                ->andWhere(
                    $qb->expr()->in('address.postcode', ':postal_codes')
                )
                ->setParameter('postal_codes', $postalCodes);
        }

        return $qb;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countByUnDispatched(array $jobs, array $services, array $administrativeLocations): int
    {
        $qb = $this->addACLMultiCenterOnQuery(
            $this->buildQueryUnDispatched($jobs, $services, $administrativeLocations),
            $this->buildCenterOnScope()
        );

        $qb->select('COUNT(ap)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function countByUserAndPostalCodesOpenedAccompanyingPeriod(?User $user, array $postalCodes): int
    {
        if (null === $user) {
            return 0;
        }

        $qb = $this->buildQueryOpenedAccompanyingCourseByUserAndPostalCodes($user, $postalCodes);
        $qb = $this->addACLMultiCenterOnQuery($qb, $this->buildCenterOnScope(), false);

        $qb->select('COUNT(DISTINCT ap)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByPerson(
        Person $person,
        string $role,
        ?array $orderBy = [],
        ?int $limit = null,
        ?int $offset = null,
    ): array {
        $qb = $this->accompanyingPeriodRepository->createQueryBuilder('ap');
        $scopes = $this->authorizationHelper
            ->getReachableScopes(
                $role,
                $this->centerResolver->resolveCenters($person)
            );
        $scopesCanSeeConfidential = $this->authorizationHelper
            ->getReachableScopes(
                AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL,
                $this->centerResolver->resolveCenters($person)
            );

        if (0 === \count($scopes)) {
            return [];
        }

        $qb
            ->join('ap.participations', 'participation')
            ->where($qb->expr()->eq('participation.person', ':person'))
            ->setParameter('person', $person);

        $qb = $this->addACLClauses($qb, $scopes, $scopesCanSeeConfidential);
        $qb = $this->addOrderLimitClauses($qb, $orderBy, $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    public function addOrderLimitClauses(QueryBuilder $qb, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        if (null !== $orderBy) {
            foreach ($orderBy as $field => $order) {
                $qb->addOrderBy('ap.'.$field, $order);
            }
        }

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb;
    }

    /**
     * Add clause for scope on a query, based on no.
     *
     * @param QueryBuilder $qb                       where the accompanying period have the `ap` alias
     * @param array<Scope> $scopesCanSee
     * @param array<Scope> $scopesCanSeeConfidential
     */
    public function addACLClauses(QueryBuilder $qb, array $scopesCanSee, array $scopesCanSeeConfidential): QueryBuilder
    {
        $qb
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->neq('ap.step', ':draft'),
                    $qb->expr()->orX(
                        $qb->expr()->eq('ap.createdBy', ':creator'),
                        $qb->expr()->isNull('ap.createdBy')
                    )
                )
            )
            ->setParameter('draft', AccompanyingPeriod::STEP_DRAFT)
            ->setParameter('user', $this->security->getUser())
            ->setParameter('creator', $this->security->getUser());

        // add join condition for scopes
        $orx = $qb->expr()->orX(
            // even if the scope is not in one authorized, the user can see the course if it is in DRAFT state
            $qb->expr()->eq('ap.step', ':draft')
        );

        foreach ($scopesCanSee as $key => $scope) {
            // for each scope:
            // - either the user is the referrer of the course
            // - or the accompanying course is one of the reachable scopes
            // - and the parcours is not confidential OR the user is the referrer OR the user can see the confidential course

            $orOnScope = $qb->expr()->orX(
                $qb->expr()->isMemberOf(':scope_'.$key, 'ap.scopes'),
                $qb->expr()->eq('ap.user', ':user')
            );

            if (in_array($scope, $scopesCanSeeConfidential, true)) {
                $orx->add($orOnScope);
            } else {
                // we must add a condition: the course is not confidential or the user is the referrer
                $andXOnScope = $qb->expr()->andX(
                    $orOnScope,
                    $qb->expr()->orX(
                        'ap.confidential = FALSE',
                        $qb->expr()->eq('ap.user', ':user')
                    )
                );
                $orx->add($andXOnScope);
            }
            $qb->setParameter('scope_'.$key, $scope);
        }
        $qb->andWhere($orx);

        return $qb;
    }

    public function buildCenterOnScope(): array
    {
        $centerOnScopes = [];
        foreach ($this->authorizationHelper->getReachableCenters(AccompanyingPeriodVoter::SEE) as $center) {
            $centerOnScopes[] = [
                'center' => $center,
                'scopeOnRole' => $this->authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE, $center),
                'scopeCanSeeConfidential' => $this->authorizationHelper->getReachableScopes(AccompanyingPeriodVoter::SEE_CONFIDENTIAL_ALL, $center),
            ];
        }

        return $centerOnScopes;
    }

    public function findByUnDispatched(array $jobs, array $services, array $administrativeAdministrativeLocations, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->buildQueryUnDispatched($jobs, $services, $administrativeAdministrativeLocations);
        $qb->select('ap');

        $qb = $this->addACLMultiCenterOnQuery($qb, $this->buildCenterOnScope(), false);
        $qb = $this->addOrderLimitClauses($qb, $orderBy, $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    public function findByUserAndPostalCodesOpenedAccompanyingPeriod(?User $user, array $postalCodes, array $orderBy = [], int $limit = 0, int $offset = 50): array
    {
        if (null === $user) {
            return [];
        }

        $qb = $this->buildQueryOpenedAccompanyingCourseByUserAndPostalCodes($user, $postalCodes);
        $qb = $this->addACLMultiCenterOnQuery($qb, $this->buildCenterOnScope(), false);
        $qb = $this->addOrderLimitClauses($qb, $orderBy, $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param list<array{center: Center, scopeOnRole: list<Scope>, scopeCanSeeConfidential: list<Scope>}> $centerScopes
     * @param bool                                                                                        $allowNoCenter if true, will allow to see the periods linked to person which does not have any center. Very few edge case when some Person are not associated to a center.
     */
    public function addACLMultiCenterOnQuery(QueryBuilder $qb, array $centerScopes, bool $allowNoCenter = false): QueryBuilder
    {
        $user = $this->security->getUser();

        if (0 === \count($centerScopes) || !$user instanceof User) {
            return $qb->andWhere("'FALSE' = 'TRUE'");
        }

        $orX = $qb->expr()->orX();

        $idx = 0;
        foreach ($centerScopes as ['center' => $center, 'scopeOnRole' => $scopes, 'scopeCanSeeConfidential' => $scopesCanSeeConfidential]) {
            $and = $qb->expr()->andX(
                $qb->expr()->exists(
                    'SELECT 1 FROM '.AccompanyingPeriodParticipation::class." part_{$idx} ".
                    "JOIN part_{$idx}.person p{$idx} LEFT JOIN p{$idx}.centerCurrent centerCurrent_{$idx} ".
                    "WHERE part_{$idx}.accompanyingPeriod = ap.id AND (centerCurrent_{$idx}.center = :center_{$idx}"
                    .($allowNoCenter ? " OR centerCurrent_{$idx}.id IS NULL)" : ')')
                )
            );
            $qb->setParameter('center_'.$idx, $center);

            $orScopeInsideCenter = $qb->expr()->orX(
                // even if the scope is not in one authorized, the user can see the course if it is in DRAFT state
                $qb->expr()->eq('ap.step', ':draft')
            );

            ++$idx;
            foreach ($scopes as $scope) {
                // for each scope:
                // - either the user is the referrer of the course
                // - or the accompanying course is one of the reachable scopes
                // - and the parcours is not confidential OR the user is the referrer OR the user can see the confidential course
                $orOnScope = $qb->expr()->orX(
                    $qb->expr()->isMemberOf(':scope_'.$idx, 'ap.scopes'),
                    $qb->expr()->eq('ap.user', ':user_executing')
                );
                $qb->setParameter('user_executing', $user);

                if (in_array($scope, $scopesCanSeeConfidential, true)) {
                    $orScopeInsideCenter->add($orOnScope);
                } else {
                    // we must add a condition: the course is not confidential or the user is the referrer
                    $andXOnScope = $qb->expr()->andX(
                        $orOnScope,
                        $qb->expr()->orX(
                            'ap.confidential = FALSE',
                            $qb->expr()->eq('ap.user', ':user_executing')
                        )
                    );
                    $orScopeInsideCenter->add($andXOnScope);
                }
                $qb->setParameter('scope_'.$idx, $scope);

                ++$idx;
            }

            $and->add($orScopeInsideCenter);
            $orX->add($and);

            ++$idx;
        }

        return $qb->andWhere($orX);
    }

    /**
     * @param array|UserJob[]  $jobs
     * @param array|Scope[]    $services
     * @param array|Location[] $locations
     */
    public function buildQueryUnDispatched(array $jobs, array $services, array $locations): QueryBuilder
    {
        $qb = $this->accompanyingPeriodRepository->createQueryBuilder('ap');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->isNull('ap.user'),
                $qb->expr()->neq('ap.step', ':draft'),
                $qb->expr()->neq('ap.step', ':closed')
            )
        )
            ->setParameter('draft', AccompanyingPeriod::STEP_DRAFT)
            ->setParameter('closed', AccompanyingPeriod::STEP_CLOSED);

        if (0 < \count($jobs)) {
            $qb->andWhere($qb->expr()->in('ap.job', ':jobs'))
                ->setParameter('jobs', $jobs);
        }

        if (0 < \count($locations)) {
            $qb->andWhere($qb->expr()->in('ap.administrativeLocation', ':locations'))
                ->setParameter('locations', $locations);
        }

        if (0 < \count($services)) {
            $or = $qb->expr()->orX();

            foreach ($services as $key => $service) {
                $or->add($qb->expr()->isMemberOf(':scopef_'.$key, 'ap.scopes'));
                $qb->setParameter('scopef_'.$key, $service);
            }
            $qb->andWhere($or);
        }

        return $qb;
    }
}
