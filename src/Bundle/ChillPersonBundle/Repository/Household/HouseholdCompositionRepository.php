<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdComposition;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class HouseholdCompositionRepository implements HouseholdCompositionRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(HouseholdComposition::class);
    }

    public function countByHousehold(Household $household): int
    {
        return $this->repository->count(['household' => $household]);
    }

    public function find($id): ?HouseholdComposition
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|HouseholdComposition[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return array|object[]|HouseholdComposition[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return array|HouseholdComposition[]|object[]
     */
    public function findByHousehold(Household $household, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findBy(['household' => $household], $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?HouseholdComposition
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return HouseholdComposition::class;
    }
}
