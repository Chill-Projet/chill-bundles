<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\PersonBundle\Entity\Household\Position;
// use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
// use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

final readonly class PositionRepository implements ObjectRepository
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Position::class);
    }

    /**
     * @return Position
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return Position[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return Position[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return Position[]
     */
    public function findByActiveOrdered(): array
    {
        return $this->repository->createQueryBuilder('p')
            ->select('p')
            ->orderBy('p.ordering', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Position[]
     */
    public function findOneBy(array $criteria): array
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return Position::class;
    }
}
