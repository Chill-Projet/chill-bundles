<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkGoal;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @method AccompanyingPeriodWorkGoal|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccompanyingPeriodWorkGoal|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccompanyingPeriodWorkGoal[]    findAll()
 * @method AccompanyingPeriodWorkGoal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final readonly class AccompanyingPeriodWorkGoalRepository
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(AccompanyingPeriodWorkGoal::class);
    }
}
