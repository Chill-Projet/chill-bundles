<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\AccompanyingPeriod;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ObjectRepository;

class AccompanyingPeriodWorkEvaluationDocumentRepository implements ObjectRepository, AssociatedEntityToStoredObjectInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(AccompanyingPeriodWorkEvaluationDocument::class);
    }

    public function find($id): ?AccompanyingPeriodWorkEvaluationDocument
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|object[]|AccompanyingPeriodWorkEvaluationDocument[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array|object[]|AccompanyingPeriodWorkEvaluationDocument[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?AccompanyingPeriodWorkEvaluationDocument
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return AccompanyingPeriodWorkEvaluationDocument::class;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findAssociatedEntityToStoredObject(StoredObject $storedObject): ?AccompanyingPeriodWorkEvaluationDocument
    {
        $qb = $this->repository->createQueryBuilder('acpwed');
        $query = $qb
            ->where('acpwed.storedObject = :storedObject')
            ->setParameter('storedObject', $storedObject)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}
