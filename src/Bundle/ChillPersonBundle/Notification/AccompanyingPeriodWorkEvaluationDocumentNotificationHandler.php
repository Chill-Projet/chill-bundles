<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Notification;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatableInterface;

final readonly class AccompanyingPeriodWorkEvaluationDocumentNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private AccompanyingPeriodWorkEvaluationDocumentRepository $accompanyingPeriodWorkEvaluationDocumentRepository) {}

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillPerson/AccompanyingCourseWork/showEvaluationDocumentInNotification.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return [
            'notification' => $notification,
            'document' => $doc = $this->accompanyingPeriodWorkEvaluationDocumentRepository->find($notification->getRelatedEntityId()),
            'evaluation' => $doc?->getAccompanyingPeriodWorkEvaluation(),
        ];
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluationDocument::class === $notification->getRelatedEntityClass();
    }

    public function getTitle(Notification $notification, array $options = []): TranslatableInterface
    {
        if (null === $eval = $this->getRelatedEntity($notification)) {
            return new TranslatableMessage('evaluation.deleted');
        }

        return new TranslatableMessage(
            'accompanying_course_evaluation_document.title',
            [
                'id' => $eval->getId(),
                'doc_title' => $eval->getTitle(),
            ]
        );
    }

    public function getAssociatedPersons(Notification $notification, array $options = []): array
    {
        if (null === $eval = $this->getRelatedEntity($notification)) {
            return [];
        }

        return $eval->getAccompanyingPeriodWorkEvaluation()->getAccompanyingPeriodWork()->getPersons()->getValues();
    }

    public function getRelatedEntity(Notification $notification): ?AccompanyingPeriodWorkEvaluationDocument
    {
        return $this->accompanyingPeriodWorkEvaluationDocumentRepository->find($notification->getRelatedEntityId());
    }
}
