<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Entity\Person\PersonCenterHistory;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormBuilderInterface;

class AvgDurationAPWorkPersonAssociatedOnWork implements ExportInterface, GroupedExportInterface
{
    private readonly bool $filterStatsByCenters;

    public function __construct(
        ParameterBagInterface $parameterBag,
        private readonly AccompanyingPeriodWorkRepository $accompanyingPeriodWorkRepository,
    ) {
        $this->filterStatsByCenters = $parameterBag->get('chill_main')['acl']['filter_stats_by_center'];
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'export.export.avg_duration_acpw_associate_on_work.description';
    }

    public function getGroup(): string
    {
        return 'Exports of social work actions';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return static fn ($value) => '_header' === $value ? 'export.export.avg_duration_acpw_associate_on_work.header' : $value;
    }

    public function getQueryKeys($data): array
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'export.export.avg_duration_acpw_associate_on_work.title';
    }

    public function getType(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->accompanyingPeriodWorkRepository->createQueryBuilder('acpw');

        $qb
            ->join('acpw.accompanyingPeriod', 'acp')
            ->join('acpw.persons', 'person');

        if ($this->filterStatsByCenters) {
            $qb
                ->andWhere(
                    $qb->expr()->exists(
                        'SELECT 1 FROM '.PersonCenterHistory::class.' acl_count_person_history WHERE acl_count_person_history.person = person
                    AND acl_count_person_history.center IN (:authorized_centers)
                    '
                    )
                )
                ->setParameter('authorized_centers', $centers);
        }

        $qb->select('AVG(DATE_DIFF(COALESCE(acpw.endDate, CURRENT_DATE()), acpw.startDate)) AS export_result');

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        return $qb;
    }

    public function requiredRole(): string
    {
        return AccompanyingPeriodVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::SOCIAL_WORK_ACTION_TYPE,
            Declarations::ACP_TYPE,
            Declarations::PERSON_TYPE,
        ];
    }
}
