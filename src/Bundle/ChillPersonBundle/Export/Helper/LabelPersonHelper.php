<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Helper;

use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Templating\Entity\PersonRenderInterface;

class LabelPersonHelper
{
    public function __construct(private readonly PersonRepository $personRepository, private readonly PersonRenderInterface $personRender) {}

    public function getLabel(string $key, array $values, string $header): callable
    {
        return function (int|string|null $value) use ($header): string {
            if ('_header' === $value) {
                return $header;
            }

            if ('' === $value || null === $value || null === $person = $this->personRepository->find($value)) {
                return '';
            }

            return $this->personRender->renderString($person, []);
        };
    }

    public function getLabelMulti(string $key, array $values, string $header): callable
    {
        return function ($value) use ($header) {
            if ('_header' === $value) {
                return $header;
            }

            if (null === $value) {
                return '';
            }

            $decoded = json_decode((string) $value, null, 512, JSON_THROW_ON_ERROR);

            if (0 === \count($decoded)) {
                return '';
            }

            return
                implode(
                    '|',
                    array_map(
                        function (int $personId) {
                            $person = $this->personRepository->find($personId);

                            if (null === $person) {
                                return '';
                            }

                            return $this->personRender->renderString($person, []);
                        },
                        array_unique(
                            array_filter($decoded, static fn (?int $id) => null !== $id),
                            \SORT_NUMERIC
                        )
                    )
                );
        };
    }
}
