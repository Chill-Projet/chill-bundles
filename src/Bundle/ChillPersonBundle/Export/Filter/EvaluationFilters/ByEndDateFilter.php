<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\EvaluationFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByEndDateFilter implements FilterInterface
{
    public function __construct(private readonly RollingDateConverterInterface $rollingDateConverter) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $qb
            ->andWhere('workeval.endDate BETWEEN :work_eval_by_end_date_start_date and :work_eval_by_end_date_end_date')
            ->setParameter(
                'work_eval_by_end_date_start_date',
                $this->rollingDateConverter->convert($data['start_date'])
            )
            ->setParameter(
                'work_eval_by_end_date_end_date',
                $this->rollingDateConverter->convert($data['end_date'])
            );
    }

    public function applyOn(): string
    {
        return Declarations::EVAL_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        $builder
            ->add('start_date', PickRollingDateType::class, [
                'label' => 'start period date',
            ])
            ->add('end_date', PickRollingDateType::class, [
                'label' => 'end period date',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return ['start_date' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function describeAction($data, $format = 'string'): array
    {
        return ['Filtered by end date: between %start_date% and %end_date%', [
            '%start_date%' => $this->rollingDateConverter->convert($data['start_date'])->format('d-m-Y'),
            '%end_date%' => $this->rollingDateConverter->convert($data['end_date'])->format('d-m-Y'),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter by end date evaluations';
    }
}
