<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod\Origin;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class OriginFilter implements FilterInterface
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->in('acp.origin', ':origin');

        if ($where instanceof Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('origin', $data['accepted_origins']);
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_origins', EntityType::class, [
            'class' => Origin::class,
            'choice_label' => fn (Origin $o) => $this->translatableStringHelper->localize($o->getLabel()),
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $origins = [];

        foreach ($data['accepted_origins'] as $v) {
            $origins[] = $this->translatableStringHelper->localize($v->getLabel());
        }

        return ['Filtered by origins: only %origins%', [
            '%origins%' => implode(', ', $origins),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter by origin';
    }
}
