<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class StepFilterBetweenDates implements FilterInterface
{
    private const DEFAULT_CHOICE = [
        AccompanyingPeriod::STEP_CONFIRMED,
        AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_SHORT,
        AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_LONG,
    ];

    private const STEPS = [
        'course.draft' => AccompanyingPeriod::STEP_DRAFT,
        'course.confirmed' => AccompanyingPeriod::STEP_CONFIRMED,
        'course.closed' => AccompanyingPeriod::STEP_CLOSED,
        'course.inactive_short' => AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_SHORT,
        'course.inactive_long' => AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_LONG,
    ];

    public function __construct(private readonly RollingDateConverterInterface $rollingDateConverter, private readonly TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $alias = 'acp_filter_by_step_between_dat_alias';
        $steps = 'acp_filter_by_step_between_dat_steps';
        $from = 'acp_filter_by_step_between_dat_from';
        $to = 'acp_filter_by_step_between_dat_to';

        $qb
            ->andWhere(
                $qb->expr()->exists(
                    'SELECT 1 FROM '.AccompanyingPeriod\AccompanyingPeriodStepHistory::class." {$alias} ".
                    "WHERE {$alias}.step IN (:{$steps}) AND OVERLAPSI ({$alias}.startDate, {$alias}.endDate),(:{$from}, :{$to}) = TRUE ".
                    "AND {$alias}.period = acp"
                )
            )
            ->setParameter($from, $this->rollingDateConverter->convert($data['date_from']))
            ->setParameter($to, $this->rollingDateConverter->convert($data['date_to']))
            ->setParameter($steps, $data['accepted_steps_multi']);
    }

    public function applyOn()
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('accepted_steps_multi', ChoiceType::class, [
                'label' => 'export.filter.course.by_step.steps',
                'choices' => self::STEPS,
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('date_from', PickRollingDateType::class, [
                'label' => 'export.filter.course.by_step.date_from',
            ])
            ->add('date_to', PickRollingDateType::class, [
                'label' => 'export.filter.course.by_step.date_to',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'accepted_steps_multi' => self::DEFAULT_CHOICE,
            'date_from' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
            'date_to' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function describeAction($data, $format = 'string')
    {
        $steps = array_map(
            fn (string $step) => $this->translator->trans(array_flip(self::STEPS)[$step]),
            $data['accepted_steps_multi'] instanceof Collection ? $data['accepted_steps_multi']->toArray() : $data['accepted_steps_multi']
        );

        return ['export.filter.course.by_step.Filtered by steps: only %step% and between %date_from% and %date_to%', [
            '%step%' => implode(', ', $steps),
            '%date_from%' => $this->rollingDateConverter->convert($data['date_from'])->format('d-m-Y'),
            '%date_to%' => $this->rollingDateConverter->convert($data['date_to'])->format('d-m-Y'),
        ]];
    }

    public function getTitle()
    {
        return 'export.filter.course.by_step.Filter by step between dates';
    }
}
