<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\UserJobRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class CreatorJobFilter implements FilterInterface
{
    private const PREFIX = 'acpw_filter_creator_job';

    public function __construct(
        private readonly UserJobRepository $userJobRepository,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_history.user", 'acpw.createdBy'),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_history.startDate", 'acpw.createdAt'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_history.endDate"),
                            $qb->expr()->gt("{$p}_history.endDate", 'acpw.createdAt')
                        )
                    )
                )
            )
            ->andWhere($qb->expr()->in("{$p}_history.job", ":{$p}_jobs"))
            ->setParameter("{$p}_jobs", $data['jobs']);
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('jobs', EntityType::class, [
                'class' => UserJob::class,
                'choices' => $this->userJobRepository->findAllActive(),
                'multiple' => true,
                'expanded' => true,
                'choice_label' => fn (UserJob $job) => $this->translatableStringHelper->localize($job->getLabel()),
                'label' => 'Job',
            ]);
    }

    public function describeAction($data, $format = 'string'): array
    {
        $creatorJobs = [];

        foreach ($data['jobs'] as $j) {
            $creatorJobs[] = $this->translatableStringHelper->localize(
                $j->getLabel()
            );
        }

        return ['export.filter.work.by_creator_job.Filtered by creator job: only %jobs%', [
            '%jobs%' => implode(', ', $creatorJobs),
        ]];
    }

    public function getFormDefaultData(): array
    {
        return [
            'jobs' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.work.by_creator_job.title';
    }
}
