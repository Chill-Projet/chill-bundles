<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingPeriodStepHistoryFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ByStepFilter implements FilterInterface
{
    public function __construct(
        private TranslatorInterface $translator,
    ) {}

    public function getTitle()
    {
        return 'export.filter.step_history.by_step.title';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $steps = [
            AccompanyingPeriod::STEP_CONFIRMED,
            AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_SHORT,
            AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_LONG,
            AccompanyingPeriod::STEP_CLOSED,
        ];

        $builder->add('steps', ChoiceType::class, [
            'choices' => array_combine(
                array_map(static fn (string $step): string => 'accompanying_period.'.$step, $steps),
                $steps
            ),
            'label' => 'export.filter.step_history.by_step.pick_steps',
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'steps' => [],
        ];
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'export.filter.step_history.by_step.description',
            [
                '%steps%' => implode(', ', array_map(fn (string $step) => $this->translator->trans('accompanying_period.'.$step), $data['steps'])),
            ],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->andWhere('acpstephistory.step IN (:acpstephistory_by_step_filter_steps)')
            ->setParameter('acpstephistory_by_step_filter_steps', $data['steps']);
    }

    public function applyOn()
    {
        return Declarations::ACP_STEP_HISTORY;
    }
}
