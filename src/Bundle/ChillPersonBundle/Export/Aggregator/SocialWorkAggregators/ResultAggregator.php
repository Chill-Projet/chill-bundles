<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\ResultRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ResultAggregator implements AggregatorInterface
{
    public function __construct(private ResultRepository $resultRepository, private TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('result', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpw.results', 'result');
        }

        $qb->addSelect('result.id AS acpw_result_aggregator');
        $qb->addGroupBy('acpw_result_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Result Type';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $r = $this->resultRepository->find($value);

            return $this->translatableStringHelper->localize(
                $r->getTitle()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['acpw_result_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group social work actions by result';
    }
}
