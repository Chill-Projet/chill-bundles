<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CurrentActionAggregator implements AggregatorInterface
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $qb
            ->addSelect('
                (CASE WHEN acpw.endDate IS NULL THEN true ELSE false END)
                AS acpw_current_action_aggregator
            ')
            ->addGroupBy('acpw_current_action_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        // No form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.course_work.by_current_action.Current action ?';
            }

            return match ($value) {
                true => $this->translator->trans('export.aggregator.course_work.by_current_action.Current action'),
                false => $this->translator->trans('export.aggregator.course_work.by_current_action.Not current action'),
                default => throw new \LogicException(sprintf('The value %s is not valid', $value)),
            };
        };
    }

    public function getQueryKeys($data): array
    {
        return ['acpw_current_action_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course_work.by_current_action.Group by current actions';
    }
}
