<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\GoalRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class GoalAggregator implements AggregatorInterface
{
    public function __construct(private GoalRepository $goalRepository, private TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('goal', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpw.goals', 'goal');
        }

        $qb->addSelect('IDENTITY(goal.goal) as acpw_goal_aggregator');
        $qb->addGroupBy('acpw_goal_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Goal Type';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $g = $this->goalRepository->find($value);

            return $this->translatableStringHelper->localize(
                $g->getTitle()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['acpw_goal_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group social work actions by goal';
    }
}
