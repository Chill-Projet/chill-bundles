<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Tests\Export\Aggregator\SocialWorkAggregators\HandlingThirdPartyAggregatorTest;
use Chill\ThirdPartyBundle\Export\Helper\LabelThirdPartyHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @see HandlingThirdPartyAggregatorTest
 */
final readonly class HandlingThirdPartyAggregator implements AggregatorInterface
{
    private const PREFIX = 'acpw_handling3party_agg';

    public function __construct(private LabelThirdPartyHelper $labelThirdPartyHelper) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form needed here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return $this->labelThirdPartyHelper->getLabel($key, $values, 'export.aggregator.course_work.by_handling_third_party.header');
    }

    public function getQueryKeys($data)
    {
        return [self::PREFIX.'_h3party'];
    }

    public function getTitle()
    {
        return 'export.aggregator.course_work.by_handling_third_party.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->addSelect("IDENTITY(acpw.handlingThierParty) AS {$p}_h3party")
            ->addGroupBy("{$p}_h3party");
    }

    public function applyOn()
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }
}
