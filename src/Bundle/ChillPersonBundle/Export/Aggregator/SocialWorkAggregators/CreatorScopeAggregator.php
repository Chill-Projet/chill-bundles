<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\ScopeRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class CreatorScopeAggregator implements AggregatorInterface
{
    private const PREFIX = 'acpw_aggr_creator_scope';

    public function __construct(
        private readonly ScopeRepository $scopeRepository,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                UserScopeHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_history.user", 'acpw.createdBy'),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_history.startDate", 'acpw.createdAt'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_history.endDate"),
                            $qb->expr()->gt("{$p}_history.endDate", 'acpw.createdAt')
                        )
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_history.scope) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.course_work.by_creator_scope.Creator\'s scope';
            }

            if (null === $value || '' === $value || null === $s = $this->scopeRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize(
                $s->getName()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course_work.by_creator_scope.title';
    }
}
