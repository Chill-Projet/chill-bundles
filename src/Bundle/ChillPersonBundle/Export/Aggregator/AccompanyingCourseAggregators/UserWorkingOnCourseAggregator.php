<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserRepositoryInterface;
use Chill\MainBundle\Templating\Entity\UserRender;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodInfo;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class UserWorkingOnCourseAggregator implements AggregatorInterface
{
    private const COLUMN_NAME = 'user_working_on_course_user_id';

    public function __construct(
        private UserRender $userRender,
        private UserRepositoryInterface $userRepository,
    ) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to add here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function (int|string|null $userId) {
            if (null === $userId || '' === $userId) {
                return '';
            }

            if ('_header' === $userId) {
                return 'export.aggregator.course.by_user_working.user';
            }

            if (null === $user = $this->userRepository->find((int) $userId)) {
                return '';
            }

            return $this->userRender->renderString($user, []);
        };
    }

    public function getQueryKeys($data)
    {
        return [self::COLUMN_NAME];
    }

    public function getTitle()
    {
        return 'export.aggregator.course.by_user_working.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!in_array('acpinfo', $qb->getAllAliases(), true)) {
            $qb->leftJoin(
                AccompanyingPeriodInfo::class,
                'acpinfo',
                Join::WITH,
                'acp.id = IDENTITY(acpinfo.accompanyingPeriod)'
            );
        }

        if (!in_array('acpinfo_user', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpinfo.user', 'acpinfo_user');
        }

        $qb->addSelect('acpinfo_user.id AS '.self::COLUMN_NAME);
        $qb->addGroupBy('acpinfo_user.id');
    }

    public function applyOn()
    {
        return Declarations::ACP_TYPE;
    }
}
