<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserJobRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class CreatorJobAggregator implements AggregatorInterface
{
    private const PREFIX = 'acp_agg_creator_job';

    public function __construct(
        private readonly UserJobRepository $jobRepository,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                'acp.userHistories',
                "{$p}_userHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_userHistory.accompanyingPeriod", 'acp.id'),
                    $qb->expr()->andX(
                        $qb->expr()->gte('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_userHistory.endDate"),
                            $qb->expr()->lt('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.endDate")
                        )
                    )
                )
            )
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_jobHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_jobHistory.user", "{$p}_userHistory.createdBy"), // et si il est null ?
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_jobHistory.startDate", "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_jobHistory.endDate"),
                            $qb->expr()->gt("{$p}_jobHistory.endDate", "{$p}_userHistory.startDate")
                        )
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_jobHistory.job) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.course.by_creator_job.Creator\'s job';
            }

            if (null === $value || '' === $value || null === $j = $this->jobRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize(
                $j->getLabel()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course.by_creator_job.Group by creator job';
    }
}
