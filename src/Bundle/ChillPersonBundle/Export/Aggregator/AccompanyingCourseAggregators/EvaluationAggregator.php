<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\EvaluationRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class EvaluationAggregator implements AggregatorInterface
{
    public function __construct(private EvaluationRepository $evaluationRepository, private TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acpw', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acp.works', 'acpw');
        }

        if (!\in_array('workeval', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acpw.accompanyingPeriodWorkEvaluations', 'workeval');
        }

        $qb->addSelect('IDENTITY(workeval.evaluation) AS evaluation_aggregator');
        $qb->addGroupBy('evaluation_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Evaluation';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $e = $this->evaluationRepository->find($value);

            return $this->translatableStringHelper->localize(
                $e->getTitle()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['evaluation_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by evaluation';
    }
}
