<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod\Origin;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class OriginAggregator implements AggregatorInterface
{
    private EntityRepository $repository;

    public function __construct(
        EntityManagerInterface $em,
        private TranslatableStringHelper $translatableStringHelper,
    ) {
        $this->repository = $em->getRepository(Origin::class);
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acporigin', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acp.origin', 'acporigin');
        }

        $qb->addSelect('acporigin.id AS origin_aggregator');
        $qb->addGroupBy('origin_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Origin';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $o = $this->repository->find($value);

            return $this->translatableStringHelper->localize(
                $o->getLabel()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['origin_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by origin';
    }
}
