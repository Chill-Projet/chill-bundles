<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodInfo;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class JobWorkingOnCourseAggregator implements AggregatorInterface
{
    private const PREFIX = 'acp_agg_user_job_working_on_course';

    public function __construct(
        private UserJobRepositoryInterface $userJobRepository,
        private TranslatableStringHelperInterface $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                AccompanyingPeriodInfo::class,
                'acpinfo',
                Join::WITH,
                $qb->expr()->eq('IDENTITY(acpinfo.accompanyingPeriod)', 'acp.id')
            )
            ->leftJoin('acpinfo.user', "{$p}_user")
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->eq("{$p}_history.user", "{$p}_user")
            )
            // job_at based on _info.infoDate
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->lte("{$p}_history.startDate", 'acpinfo.infoDate'),
                    $qb->expr()->orX(
                        $qb->expr()->isNull("{$p}_history.endDate"),
                        $qb->expr()->gt("{$p}_history.endDate", 'acpinfo.infoDate')
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_history.job) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function (int|string|null $jobId) {
            if (null === $jobId || '' === $jobId) {
                return '';
            }

            if ('_header' === $jobId) {
                return 'export.aggregator.course.by_job_working.job';
            }

            if (null === $job = $this->userJobRepository->find((int) $jobId)) {
                return '';
            }

            return $this->translatableStringHelper->localize($job->getLabel());
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course.by_job_working.title';
    }
}
