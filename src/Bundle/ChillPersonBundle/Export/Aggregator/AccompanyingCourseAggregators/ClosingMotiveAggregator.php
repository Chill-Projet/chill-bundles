<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\AccompanyingPeriod\ClosingMotiveRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ClosingMotiveAggregator implements AggregatorInterface
{
    public function __construct(private readonly ClosingMotiveRepositoryInterface $motiveRepository, private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('IDENTITY(acp.closingMotive) AS closingmotive_aggregator');
        $qb->addGroupBy('closingmotive_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Closing motive';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $cm = $this->motiveRepository->find($value);

            return $this->translatableStringHelper->localize(
                $cm->getName()
            );
        };
    }

    public function getQueryKeys($data)
    {
        return ['closingmotive_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by closing motive';
    }
}
