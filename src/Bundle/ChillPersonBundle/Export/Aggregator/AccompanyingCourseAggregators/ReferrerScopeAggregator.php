<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Export\DataTransformerInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

readonly class ReferrerScopeAggregator implements AggregatorInterface, DataTransformerInterface
{
    private const PREFIX = 'acp_agg_referrer_scope';

    public function __construct(
        private ScopeRepositoryInterface $scopeRepository,
        private TranslatableStringHelperInterface $translatableStringHelper,
        private RollingDateConverterInterface $rollingDateConverter,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                'acp.userHistories',
                "{$p}_userHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_userHistory.accompanyingPeriod", 'acp.id'),
                    // check that the user is referrer when the accompanying period is opened
                    "OVERLAPSI (acp.openingDate, acp.closingDate), ({$p}_userHistory.startDate, {$p}_userHistory.endDate) = TRUE",
                    "OVERLAPSI (:{$p}_startDate, :{$p}_endDate), ({$p}_userHistory.startDate, {$p}_userHistory.endDate) = TRUE"
                )
            )
            ->leftJoin(
                UserScopeHistory::class,
                "{$p}_scopeHistory",
                Join::WITH,
                $qb->expr()->andX(
                    "{$p}_scopeHistory.user = {$p}_userHistory.user",
                    "OVERLAPSI ({$p}_scopeHistory.startDate, {$p}_scopeHistory.endDate), ({$p}_userHistory.startDate, {$p}_userHistory.endDate) = TRUE",
                    "OVERLAPSI (:{$p}_startDate, :{$p}_endDate), ({$p}_userHistory.startDate, {$p}_userHistory.endDate) = TRUE"
                )
            )
            ->setParameter("{$p}_startDate", $this->rollingDateConverter->convert($data['start_date']))
            ->setParameter("{$p}_endDate", $this->rollingDateConverter->convert($data['end_date']))
            ->addSelect("IDENTITY({$p}_scopeHistory.scope) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('start_date', PickRollingDateType::class, [
                'label' => 'export.aggregator.course.by_referrer_scope.Referrer and scope after',
                'required' => true,
            ])
            ->add('end_date', PickRollingDateType::class, [
                'label' => 'export.aggregator.course.by_referrer_scope.Until',
                'required' => true,
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
            'end_date' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function transformData(?array $before): array
    {
        $default = $this->getFormDefaultData();
        $data = [];

        $data['start_date'] = $before['start_date'] ?? new RollingDate(RollingDate::T_FIXED_DATE, new \DateTimeImmutable('1970-01-01'));
        $data['end_date'] = $before['end_date'] ?? $default['end_date'];

        return $data;
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) {
            if ('_header' === $value) {
                return 'export.aggregator.course.by_user_scope.Referrer\'s scope';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $scope = $this->scopeRepository->find($value);

            if (null === $scope) {
                throw new \LogicException('no scope found with this id: '.$value);
            }

            return $this->translatableStringHelper->localize($scope->getName());
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course.by_user_scope.Group course by referrer\'s scope';
    }
}
