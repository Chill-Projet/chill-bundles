<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\LocationRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class AdministrativeLocationAggregator implements AggregatorInterface
{
    public function __construct(private readonly LocationRepository $locationRepository, private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acploc', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acp.administrativeLocation', 'acploc');
        }

        $qb->addSelect('IDENTITY(acp.administrativeLocation) AS location_aggregator');
        $qb->addGroupBy('location_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Administrative location';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $l = $this->locationRepository->find($value);

            return $l->getName().' ('.$this->translatableStringHelper->localize($l->getLocationType()->getTitle()).')';
        };
    }

    public function getQueryKeys($data): array
    {
        return ['location_aggregator'];
    }

    public function getTitle()
    {
        return 'Group by administrative location';
    }
}
