<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Export\ExportElementValidatedInterface;
use Chill\MainBundle\Repository\CountryRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Util\CountriesInfo;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class CountryOfBirthAggregator implements AggregatorInterface, ExportElementValidatedInterface
{
    public function __construct(private CountryRepository $countriesRepository, private TranslatableStringHelper $translatableStringHelper, private TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        // add a clause in select part
        if ('country' === $data['group_by_level']) {
            $qb->addSelect('countryOfBirth.countryCode as country_of_birth_aggregator');
        } elseif ('continent' === $data['group_by_level']) {
            $clause = 'CASE '
                    .'WHEN countryOfBirth.countryCode IN(:cob_africa_codes) THEN \'AF\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_asia_codes) THEN \'AS\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_europe_codes) THEN \'EU\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_north_america_codes) THEN \'NA\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_south_america_codes) THEN \'SA\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_oceania_codes) THEN \'OC\' '
                    .'WHEN countryOfBirth.countryCode IN(:cob_antartica_codes) THEN \'AN\' '
                    .'ELSE \'\' '
                    .'END as country_of_birth_aggregator ';
            $qb->addSelect($clause);
            $params =
                    [
                        'cob_africa_codes' => CountriesInfo::getCountriesCodeByContinent('AF'),
                        'cob_asia_codes' => CountriesInfo::getCountriesCodeByContinent('AS'),
                        'cob_europe_codes' => CountriesInfo::getCountriesCodeByContinent('EU'),
                        'cob_north_america_codes' => CountriesInfo::getCountriesCodeByContinent('NA'),
                        'cob_south_america_codes' => CountriesInfo::getCountriesCodeByContinent('SA'),
                        'cob_oceania_codes' => CountriesInfo::getCountriesCodeByContinent('OC'),
                        'cob_antartica_codes' => CountriesInfo::getCountriesCodeByContinent('AN'),
                    ];

            foreach ($params as $k => $v) {
                $qb->setParameter($k, $v);
            }
        } else {
            throw new \LogicException("The group_by_level '".$data['group_by_level'].' is not known.');
        }

        if (!\in_array('countryOfBirth', $qb->getAllAliases(), true)) {
            $qb->leftJoin('person.countryOfBirth', 'countryOfBirth');
        }

        // add group by
        $qb->addGroupBy('country_of_birth_aggregator');
    }

    public function applyOn()
    {
        return 'person';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('group_by_level', ChoiceType::class, [
            'choices' => [
                'Group by continents' => 'continent',
                'Group by country' => 'country',
            ],
            'expanded' => true,
            'multiple' => false,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        $labels = [];

        if ('country' === $data['group_by_level']) {
            $qb = $this->countriesRepository->createQueryBuilder('c');

            $countries = $qb
                ->andWhere($qb->expr()->in('c.countryCode', ':countries'))
                ->setParameter('countries', $values)
                ->getQuery()
                ->getResult(Query::HYDRATE_SCALAR);

            // initialize array and add blank key for null values
            $labels = [
                '' => $this->translator->trans('without data'),
                '_header' => $this->translator->trans('Country of birth'),
            ];

            foreach ($countries as $row) {
                $labels[$row['c_countryCode']] = $this->translatableStringHelper->localize($row['c_name']);
            }
        }

        if ('continent' === $data['group_by_level']) {
            $labels = [
                'EU' => $this->translator->trans('Europe'),
                'AS' => $this->translator->trans('Asia'),
                'AN' => $this->translator->trans('Antartica'),
                'AF' => $this->translator->trans('Africa'),
                'SA' => $this->translator->trans('South America'),
                'NA' => $this->translator->trans('North America'),
                'OC' => $this->translator->trans('Oceania'),
                '' => $this->translator->trans('without data'),
                '_header' => $this->translator->trans('Continent of birth'),
            ];
        }

        return static fn (?string $value): string => $labels[(string) $value];
    }

    public function getQueryKeys($data)
    {
        return ['country_of_birth_aggregator'];
    }

    public function getTitle()
    {
        return 'Group people by country of birth';
    }

    public function validateForm($data, ExecutionContextInterface $context)
    {
        if (null === $data['group_by_level']) {
            $context->buildViolation('You should select an option')
                ->addViolation();
        }
    }
}
