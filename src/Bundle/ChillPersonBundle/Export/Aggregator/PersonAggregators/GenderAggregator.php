<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\GenderRepository;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class GenderAggregator implements AggregatorInterface
{
    public function __construct(private TranslatorInterface $translator, private TranslatableStringHelperInterface $translatableStringHelper, private GenderRepository $repository) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->leftJoin('person.gender', 'g');
        $qb->addSelect('g.id as gender');

        $qb->addGroupBy('gender');
    }

    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function (int|string|null $value) {
            if (null === $value || '' === $value) {
                return '';
            }

            if ('_header' === $value) {
                return $this->translator->trans('Gender');
            }

            if (null === $gender = $this->repository->find((int) $value)) {
                return '';
            }

            return (string) $this->translatableStringHelper->localize($gender->getLabel());
        };
    }

    public function getQueryKeys($data)
    {
        return ['gender'];
    }

    public function getTitle()
    {
        return 'Group people by gender';
    }
}
