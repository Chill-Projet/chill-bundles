<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\EmploymentStatusRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class EmploymentStatusAggregator implements AggregatorInterface
{
    public function __construct(private EmploymentStatusRepository $employmentStatusRepository, private TranslatableStringHelper $translatableStringHelper) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->leftJoin('person.employmentStatus', 'es');
        $qb->addSelect('es.id as employment_status_aggregator');

        $qb->addGroupBy('employment_status_aggregator');
    }

    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Employment status';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $g = $this->employmentStatusRepository->find($value);

            return $this->translatableStringHelper->localize($g->getName());
        };
    }

    public function getQueryKeys($data)
    {
        return ['employment_status_aggregator'];
    }

    public function getTitle()
    {
        return 'Group people by employment status';
    }
}
