<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\AccompanyingPeriod\ClosingMotiveRepositoryInterface;
use Chill\PersonBundle\Templating\Entity\ClosingMotiveRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ByClosingMotiveAggregator implements AggregatorInterface
{
    private const KEY = 'acpstephistory_by_closing_motive_agg';

    public function __construct(
        private ClosingMotiveRepositoryInterface $closingMotiveRepository,
        private ClosingMotiveRender $closingMotiveRender,
    ) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to add here
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (int|string|null $value): string {
            if ('_header' === $value) {
                return 'export.aggregator.step_history.by_closing_motive.header';
            }

            if (null === $value || '' === $value || null === $closingMotive = $this->closingMotiveRepository->find((int) $value)) {
                return '';
            }

            return $this->closingMotiveRender->renderString($closingMotive, []);
        };
    }

    public function getQueryKeys($data)
    {
        return [
            self::KEY,
        ];
    }

    public function getTitle()
    {
        return 'export.aggregator.step_history.by_closing_motive.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->addSelect('IDENTITY(acpstephistory.closingMotive) AS '.self::KEY)
            ->addGroupBy(self::KEY);
    }

    public function applyOn()
    {
        return Declarations::ACP_STEP_HISTORY;
    }
}
