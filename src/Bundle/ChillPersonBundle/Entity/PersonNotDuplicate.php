<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * PersonNotDuplicate.
 */
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_not_duplicate')]
class PersonNotDuplicate
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE)]
    private \DateTime $date;

    /**
     * The person's id.
     */
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person1 = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person2 = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $user = null;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPerson1()
    {
        return $this->person1;
    }

    public function getPerson2()
    {
        return $this->person2;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function setId(?int $id)
    {
        $this->id = $id;
    }

    public function setPerson1(Person $person1)
    {
        $this->person1 = $person1;
    }

    public function setPerson2(Person $person2)
    {
        $this->person2 = $person2;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }
}
