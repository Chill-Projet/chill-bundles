<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\AccompanyingPeriod\SocialIssueConsistency\AccompanyingPeriodLinkedWithSocialIssuesEntityInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['accompanying_period_work' => AccompanyingPeriodWork::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_accompanying_period_work')]
class AccompanyingPeriodWork implements AccompanyingPeriodLinkedWithSocialIssuesEntityInterface, TrackCreationInterface, TrackUpdateInterface
{
    #[Serializer\Groups(['read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriod::class)]
    #[Serializer\Context(normalizationContext: ['groups' => ['read']], groups: ['read:accompanyingPeriodWork:light'])]
    private ?AccompanyingPeriod $accompanyingPeriod = null;

    /**
     * @var Collection<int, AccompanyingPeriodWorkEvaluation>
     *
     * @internal the serialization for write evaluations is handled in `accompanyingperiodworkdenormalizer`
     * @internal the serialization for context docgen:read is handled in `accompanyingperiodworknormalizer`
     */
    #[Serializer\Groups(['read'])]
    #[ORM\OneToMany(mappedBy: 'accompanyingPeriodWork', targetEntity: AccompanyingPeriodWorkEvaluation::class, cascade: ['remove', 'persist'], orphanRemoval: true)]
    #[ORM\OrderBy(['startDate' => \Doctrine\Common\Collections\Criteria::DESC, 'id' => 'DESC'])]
    private Collection $accompanyingPeriodWorkEvaluations;

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $createdAt = null;

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $createdAutomatically = false;

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $createdAutomaticallyReason = '';

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $createdBy = null;

    #[Serializer\Groups(['accompanying_period_work:create', 'accompanying_period_work:edit', 'read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[Assert\GreaterThanOrEqual(propertyPath: 'startDate', message: 'accompanying_course_work.The endDate should be greater or equal than the start date')]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @var Collection<int, AccompanyingPeriodWorkGoal>
     */
    #[Serializer\Groups(['read', 'docgen:read', 'accompanying_period_work:edit'])]
    #[ORM\OneToMany(mappedBy: 'accompanyingPeriodWork', targetEntity: AccompanyingPeriodWorkGoal::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $goals;

    #[Serializer\Groups(['read', 'docgen:read', 'accompanying_period_work:edit'])]
    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    private ?ThirdParty $handlingThierParty = null;

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light', 'read:evaluation:include-work'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'accompanying_period_work:edit', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $note = '';

    /**
     * @var Collection<int, Person>
     */
    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light', 'accompanying_period_work:edit', 'accompanying_period_work:create'])]
    #[ORM\ManyToMany(targetEntity: Person::class)]
    #[ORM\JoinTable(name: 'chill_person_accompanying_period_work_person')]
    private Collection $persons;

    #[Serializer\Groups(['read', 'accompanying_period_work:edit'])]
    #[ORM\Embedded(class: PrivateCommentEmbeddable::class, columnPrefix: 'privateComment_')]
    private PrivateCommentEmbeddable $privateComment;

    /**
     * @var Collection<int, AccompanyingPeriodWorkReferrerHistory>
     */
    #[ORM\OneToMany(targetEntity: AccompanyingPeriodWorkReferrerHistory::class, cascade: ['persist', 'remove'], mappedBy: 'accompanyingPeriodWork', orphanRemoval: true)]
    private Collection $referrersHistory;

    /**
     * @var Collection<int, Result>
     */
    #[Serializer\Groups(['read', 'docgen:read', 'accompanying_period_work:edit'])]
    #[ORM\ManyToMany(targetEntity: Result::class, inversedBy: 'accompanyingPeriodWorks')]
    #[ORM\JoinTable(name: 'chill_person_accompanying_period_work_result')]
    private Collection $results;

    #[Serializer\Groups(['read', 'docgen:read', 'read:accompanyingPeriodWork:light', 'accompanying_period_work:create'])]
    #[ORM\ManyToOne(targetEntity: SocialAction::class)]
    #[Serializer\Context(normalizationContext: ['groups' => ['read']], groups: ['read:accompanyingPeriodWork:light'])]
    private ?SocialAction $socialAction = null;

    #[Serializer\Groups(['accompanying_period_work:create', 'accompanying_period_work:edit', 'read', 'docgen:read', 'read:accompanyingPeriodWork:light'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE)]
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @var Collection<int, ThirdParty>
     */
    #[Serializer\Groups(['read', 'docgen:read', 'accompanying_period_work:edit'])]
    #[ORM\ManyToMany(targetEntity: ThirdParty::class)]
    #[ORM\JoinTable(name: 'chill_person_accompanying_period_work_third_party')] // In schema : intervenants
    private Collection $thirdParties;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $updatedBy = null;

    #[Serializer\Groups(['read', 'accompanying_period_work:edit'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: false, options: ['default' => 1])]
    #[ORM\Version]
    private int $version = 1;

    public function __construct()
    {
        $this->goals = new ArrayCollection();
        $this->privateComment = new PrivateCommentEmbeddable();
        $this->results = new ArrayCollection();
        $this->thirdParties = new ArrayCollection();
        $this->persons = new ArrayCollection();
        $this->accompanyingPeriodWorkEvaluations = new ArrayCollection();
        $this->referrersHistory = new ArrayCollection();
    }

    public function addAccompanyingPeriodWorkEvaluation(AccompanyingPeriodWorkEvaluation $evaluation): self
    {
        if (!$this->accompanyingPeriodWorkEvaluations->contains($evaluation)) {
            $this->accompanyingPeriodWorkEvaluations[] = $evaluation;
            $evaluation->setAccompanyingPeriodWork($this);
        }

        return $this;
    }

    public function addGoal(AccompanyingPeriodWorkGoal $goal): self
    {
        if (!$this->goals->contains($goal)) {
            $this->goals[] = $goal;
            $goal->setAccompanyingPeriodWork($this);
        }

        return $this;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function addReferrer(User $referrer): self
    {
        if (!$this->getReferrers()->contains($referrer)) {
            $this->referrersHistory[] =
                new AccompanyingPeriodWorkReferrerHistory($this, $referrer, new \DateTimeImmutable('today'));
        }

        return $this;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    public function addThirdParty(ThirdParty $thirdParty): self
    {
        if (!$this->thirdParties->contains($thirdParty)) {
            $this->thirdParties[] = $thirdParty;
        }

        return $this;
    }

    public function getAccompanyingPeriod(): ?AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    /**
     * @return Collection<AccompanyingPeriodWorkEvaluation>
     */
    public function getAccompanyingPeriodWorkEvaluations(): Collection
    {
        return $this->accompanyingPeriodWorkEvaluations;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedAutomatically(): ?bool
    {
        return $this->createdAutomatically;
    }

    public function getCreatedAutomaticallyReason(): ?string
    {
        return $this->createdAutomaticallyReason;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @return AccompanyingPeriodWorkGoal[]|Collection
     */
    public function getGoals(): Collection
    {
        return $this->goals;
    }

    public function getHandlingThierParty(): ?ThirdParty
    {
        return $this->handlingThierParty;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function getPrivateComment(): PrivateCommentEmbeddable
    {
        return $this->privateComment;
    }

    /**
     * Retrieves a collection of current referrers.
     *
     * This method filters the referrer history to get only those entries
     * where the end date is null, maps them to their associated users,
     * and returns them as a new ReadableCollection.
     *
     * @return ReadableCollection<int, User> collection of active referrers
     */
    #[Serializer\Groups(['accompanying_period_work:edit'])]
    public function getReferrers(): ReadableCollection
    {
        $users = $this->referrersHistory
            ->filter(fn (AccompanyingPeriodWorkReferrerHistory $h) => null === $h->getEndDate())
            ->map(fn (AccompanyingPeriodWorkReferrerHistory $h) => $h->getUser())
            ->getValues();

        return new ArrayCollection(array_values($users));
    }

    /**
     * @return ReadableCollection<int, AccompanyingPeriodWorkReferrerHistory>
     */
    public function getReferrersHistoryCurrent(): ReadableCollection
    {
        return new ArrayCollection(
            $this->getReferrersHistory()
                ->filter(fn (AccompanyingPeriodWorkReferrerHistory $h) => null === $h->getEndDate())
                ->getValues()
        );
    }

    /**
     * @return Collection<int, AccompanyingPeriodWorkReferrerHistory>
     */
    public function getReferrersHistory(): Collection
    {
        return $this->referrersHistory;
    }

    /**
     * @return Collection<int, Result>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function getSocialAction(): ?SocialAction
    {
        return $this->socialAction;
    }

    public function getSocialIssues(): Collection
    {
        return new ArrayCollection([$this->getSocialAction()->getIssue()]);
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @return Collection|ThirdParty[]
     */
    public function getThirdParties(): Collection
    {
        return $this->thirdParties;
    }

    public function getThirdPartys(): Collection
    {
        return $this->getThirdParties();
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function removeAccompanyingPeriodWorkEvaluation(AccompanyingPeriodWorkEvaluation $evaluation): self
    {
        $this->accompanyingPeriodWorkEvaluations
            ->removeElement($evaluation);
        $evaluation->setAccompanyingPeriodWork(null);

        return $this;
    }

    public function removeGoal(AccompanyingPeriodWorkGoal $goal): self
    {
        if ($this->goals->removeElement($goal)) {
            // set the owning side to null (unless already changed)
            if ($goal->getAccompanyingPeriodWork() === $this) {
                $goal->setAccompanyingPeriodWork(null);
            }
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        $this->persons->removeElement($person);

        return $this;
    }

    public function removeReferrer(User $referrer): self
    {
        foreach ($this->referrersHistory as $history) {
            if ($history->isOpen() && $referrer === $history->getUser()) {
                $history->setEndDate(new \DateTimeImmutable('today'));

                if ($history->isDateRangeEmpty()) {
                    $history->removeAccompanyingPeriodWork();
                    $this->referrersHistory->removeElement($history);
                }
            }
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }

    public function removeSocialIssue(SocialIssue $issue): AccompanyingPeriodLinkedWithSocialIssuesEntityInterface
    {
        $this->getSocialIssues()->removeElement($issue);

        return $this;
    }

    public function removeThirdParty(ThirdParty $thirdParty): self
    {
        $this->thirdParties->removeElement($thirdParty);

        return $this;
    }

    /**
     * Internal: you should use `$accompanyingPeriod->removeWork($work);` or
     * `$accompanyingPeriod->addWork($work);`.
     */
    public function setAccompanyingPeriod(?AccompanyingPeriod $accompanyingPeriod): self
    {
        if (
            $this->accompanyingPeriod instanceof AccompanyingPeriod
            && $accompanyingPeriod !== $this->accompanyingPeriod
        ) {
            throw new \LogicException('A work cannot change accompanyingPeriod');
        }

        $this->accompanyingPeriod = $accompanyingPeriod;

        return $this;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedAutomatically(bool $createdAutomatically): self
    {
        $this->createdAutomatically = $createdAutomatically;

        return $this;
    }

    public function setCreatedAutomaticallyReason(string $createdAutomaticallyReason): self
    {
        $this->createdAutomaticallyReason = $createdAutomaticallyReason;

        return $this;
    }

    public function setCreatedBy(?User $user): self
    {
        $this->createdBy = $user;

        return $this;
    }

    public function setEndDate(?\DateTimeInterface $endDate = null): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setHandlingThierParty(?ThirdParty $handlingThierParty): self
    {
        $this->handlingThierParty = $handlingThierParty;

        return $this;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function setPrivateComment(PrivateCommentEmbeddable $privateComment): self
    {
        $this->privateComment->merge($privateComment);

        return $this;
    }

    public function setSocialAction(?SocialAction $socialAction): self
    {
        $this->socialAction = $socialAction;

        return $this;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate instanceof \DateTime ? \DateTimeImmutable::createFromMutable($startDate) : $startDate;

        return $this;
    }

    public function setUpdatedAt(\DateTimeInterface $datetime): TrackUpdateInterface
    {
        $this->updatedAt = $datetime instanceof \DateTime ? \DateTimeImmutable::createFromMutable($datetime) : $datetime;

        return $this;
    }

    public function setUpdatedBy(User $user): TrackUpdateInterface
    {
        $this->updatedBy = $user;

        return $this;
    }
}
