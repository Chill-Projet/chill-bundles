<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['origin' => Origin::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_accompanying_period_origin')]
class Origin
{
    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $label = [];

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $noActiveAfter = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getNoActiveAfter(): ?\DateTimeImmutable
    {
        return $this->noActiveAfter;
    }

    public function setLabel(array $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function setNoActiveAfter(?\DateTimeImmutable $noActiveAfter): self
    {
        $this->noActiveAfter = $noActiveAfter;

        return $this;
    }
}
