<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['accompanying_period_work_evaluation' => AccompanyingPeriodWorkEvaluation::class])]
#[ORM\Entity]
#[ORM\Table('chill_person_accompanying_period_work_evaluation')]
class AccompanyingPeriodWorkEvaluation implements TrackCreationInterface, TrackUpdateInterface
{
    #[Serializer\Groups(['read:evaluation:include-work'])]
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriodWork::class, inversedBy: 'accompanyingPeriodWorkEvaluations')]
    #[Serializer\Context(normalizationContext: ['groups' => ['read:accompanyingPeriodWork:light']], groups: ['read:evaluation:include-work'])]
    private ?AccompanyingPeriodWork $accompanyingPeriodWork = null;

    #[Serializer\Groups(['read', 'docgen:read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $comment = '';

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $createdAt = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $createdBy = null;

    /**
     * **Note on deserialization/denormalization**: denormalization of documents is handled by.
     *
     * @see{Chill\PersonBundle\Serializer\Normalizer\AccompanyingPeriodWorkEvaluationDenormalizer}
     *
     * @var Collection<int, AccompanyingPeriodWorkEvaluationDocument>
     */
    #[Serializer\Groups(['read'])]
    #[ORM\OneToMany(mappedBy: 'accompanyingPeriodWorkEvaluation', targetEntity: AccompanyingPeriodWorkEvaluationDocument::class, cascade: ['remove', 'persist'], orphanRemoval: true)]
    #[ORM\OrderBy(['createdAt' => \Doctrine\Common\Collections\Criteria::DESC, 'id' => 'DESC'])]
    private Collection $documents;

    #[Serializer\Groups(['read', 'docgen:read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $endDate = null;

    #[Serializer\Groups(['read', 'docgen:read', 'accompanying_period_work_evaluation:create'])]
    #[ORM\ManyToOne(targetEntity: Evaluation::class)]
    private ?Evaluation $evaluation = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    /**
     * This is a workaround for client, to allow them to assign arbitrary data
     * dedicated to their job.
     *
     * This data is not persisted into database, but will appears on the data
     * normalized during the same request (like PUT/PATCH request)
     */
    #[Serializer\Groups(['read', 'write', 'accompanying_period_work_evaluation:create'])]
    private $key;

    #[Serializer\Groups(['read', 'docgen:read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $maxDate = null;

    #[Serializer\Groups(['read', 'docgen:read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $startDate = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $updatedAt = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $updatedBy = null;

    #[Serializer\Groups(['read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATEINTERVAL, nullable: true, options: ['default' => null])]
    private ?\DateInterval $warningInterval = null;

    #[Serializer\Groups(['read', 'docgen:read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: true)]
    private ?int $timeSpent = null;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function addDocument(AccompanyingPeriodWorkEvaluationDocument $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setAccompanyingPeriodWorkEvaluation($this);
        }

        return $this;
    }

    public function getAccompanyingPeriodWork(): ?AccompanyingPeriodWork
    {
        return $this->accompanyingPeriodWork;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @return Collection<AccompanyingPeriodWorkEvaluationDocument>
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Arbitrary data, used for client.
     */
    public function getKey()
    {
        return $this->key;
    }

    public function getMaxDate(): ?\DateTimeImmutable
    {
        return $this->maxDate;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    #[Serializer\Groups(['docgen:read'])]
    public function getWarningDate(): ?\DateTimeImmutable
    {
        if (null === $this->getEndDate() || null === $this->getWarningInterval()) {
            return null;
        }

        return $this->getEndDate()->sub($this->getWarningInterval());
    }

    public function getWarningInterval(): ?\DateInterval
    {
        return $this->warningInterval;
    }

    public function getTimeSpent(): ?int
    {
        return $this->timeSpent;
    }

    public function removeDocument(AccompanyingPeriodWorkEvaluationDocument $document): self
    {
        $this->documents->removeElement($document);
        $document->setAccompanyingPeriodWorkEvaluation(null);

        return $this;
    }

    public function setAccompanyingPeriodWork(?AccompanyingPeriodWork $accompanyingPeriodWork): AccompanyingPeriodWorkEvaluation
    {
        if (
            $accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $this->accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $this->accompanyingPeriodWork->getId() !== $accompanyingPeriodWork->getId()
        ) {
            throw new \RuntimeException('Changing the accompanyingPeriodWork is not allowed');
        }

        $this->accompanyingPeriodWork = $accompanyingPeriodWork;

        return $this;
    }

    public function setComment(string $comment): AccompanyingPeriodWorkEvaluation
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @param \DateTimeImmutable|null $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedBy(?User $createdBy): AccompanyingPeriodWorkEvaluation
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): AccompanyingPeriodWorkEvaluation
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setTimeSpent(?int $timeSpent): self
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    public function setEvaluation(?Evaluation $evaluation): AccompanyingPeriodWorkEvaluation
    {
        if (
            ($evaluation instanceof Evaluation
                && $this->evaluation instanceof Evaluation
                && $evaluation->getId() !== $this->evaluation->getId())
            || ($this->evaluation instanceof Evaluation
                && null === $evaluation)
        ) {
            $cl = AccompanyingPeriodWorkEvaluation::class;

            throw new \LogicException("once set, an {$cl}  cannot
                change or remove the linked Evaluation::class");
        }

        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Arbitrary data, used for client.
     */
    public function setKey(mixed $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function setMaxDate(?\DateTimeImmutable $maxDate): AccompanyingPeriodWorkEvaluation
    {
        $this->maxDate = $maxDate;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): AccompanyingPeriodWorkEvaluation
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @param \DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setUpdatedBy(?User $updatedBy): AccompanyingPeriodWorkEvaluation
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function setWarningInterval(?\DateInterval $warningInterval): AccompanyingPeriodWorkEvaluation
    {
        $this->warningInterval = $warningInterval;

        return $this;
    }
}
