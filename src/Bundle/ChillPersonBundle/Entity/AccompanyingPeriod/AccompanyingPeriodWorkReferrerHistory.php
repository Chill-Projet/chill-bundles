<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'chill_person_accompanying_period_work_referrer')]
class AccompanyingPeriodWorkReferrerHistory implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;
    use TrackUpdateTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $endDate = null;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: AccompanyingPeriodWork::class, inversedBy: 'referrersHistory')]
        private ?AccompanyingPeriodWork $accompanyingPeriodWork,
        /**
         * @var User
         */
        #[ORM\ManyToOne(targetEntity: User::class)]
        private User $user,
        /**
         * @var \DateTimeImmutable
         */
        #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: false)]
        private \DateTimeImmutable $startDate,
    ) {}

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): AccompanyingPeriodWorkReferrerHistory
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getAccompanyingPeriodWork(): AccompanyingPeriodWork
    {
        return $this->accompanyingPeriodWork;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * to be used when the history is removed (when startDate = endDate).
     */
    public function removeAccompanyingPeriodWork(): self
    {
        $this->accompanyingPeriodWork = null;

        return $this;
    }

    /**
     * @return bool true if the endDate is null
     */
    public function isOpen(): bool
    {
        return null === $this->getEndDate();
    }

    /**
     * return true if the date range is empty (start date and end date are equal).
     *
     * @return bool true if the start date and end date are equal
     */
    public function isDateRangeEmpty(): bool
    {
        return $this->getStartDate()->format('Y-m-d') === $this->getEndDate()?->format('Y-m-d');
    }
}
