<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['accompanying_period_work_goal' => AccompanyingPeriodWorkGoal::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_accompanying_period_work_goal')]
class AccompanyingPeriodWorkGoal
{
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriodWork::class, inversedBy: 'goals')]
    private ?AccompanyingPeriodWork $accompanyingPeriodWork = null;

    #[Serializer\Groups(['accompanying_period_work:edit', 'read', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: Goal::class)]
    private ?Goal $goal = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['accompanying_period_work:edit', 'read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $note = '';

    /**
     * @var Collection<int, Result>
     */
    #[Serializer\Groups(['accompanying_period_work:edit', 'read', 'docgen:read'])]
    #[ORM\ManyToMany(targetEntity: Result::class, inversedBy: 'accompanyingPeriodWorkGoals')]
    #[ORM\JoinTable(name: 'chill_person_accompanying_period_work_goal_result')]
    private Collection $results;

    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    public function getAccompanyingPeriodWork(): ?AccompanyingPeriodWork
    {
        return $this->accompanyingPeriodWork;
    }

    public function getGoal(): ?Goal
    {
        return $this->goal;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @return Collection|Result[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function removeResult(Result $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }

    public function setAccompanyingPeriodWork(?AccompanyingPeriodWork $accompanyingPeriodWork): self
    {
        if (
            $this->accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $accompanyingPeriodWork !== $this->accompanyingPeriodWork
            && null !== $accompanyingPeriodWork
        ) {
            throw new \LogicException('Change accompanying period work is not allowed');
        }

        $this->accompanyingPeriodWork = $accompanyingPeriodWork;

        return $this;
    }

    public function setGoal(?Goal $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
