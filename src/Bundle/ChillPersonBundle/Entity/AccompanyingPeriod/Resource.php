<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * **About denormalization**: this operation is operated by @see{AccompanyingPeriodResourdeNormalizer}.
 */
#[DiscriminatorMap(typeProperty: 'type', mapping: ['accompanying_period_resource' => Resource::class])]
#[ORM\Entity]
#[ORM\UniqueConstraint(name: 'person_unique', columns: ['person_id', 'accompanyingperiod_id'])] // ,
#[ORM\UniqueConstraint(name: 'thirdparty_unique', columns: ['thirdparty_id', 'accompanyingperiod_id'])]
#[ORM\Table(name: 'chill_person_accompanying_period_resource')]
#[ORM\UniqueConstraint(name: 'person_unique', columns: ['person_id', 'accompanyingperiod_id'])]
#[ORM\UniqueConstraint(name: 'thirdparty_unique', columns: ['thirdparty_id', 'accompanyingperiod_id'])]
class Resource
{
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriod::class, inversedBy: 'resources')]
    #[ORM\JoinColumn(nullable: false)]
    private ?AccompanyingPeriod $accompanyingPeriod = null;

    #[Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $comment = '';

    #[Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Groups(['docgen:read'])]
    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Person $person = null;

    #[Groups(['docgen:read'])]
    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?ThirdParty $thirdParty = null;

    public function getAccompanyingPeriod(): ?AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    #[Groups(['read'])]
    public function getResource(): Person|ThirdParty|null
    {
        return $this->person ?? $this->thirdParty;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setAccompanyingPeriod(?AccompanyingPeriod $accompanyingPeriod): self
    {
        $this->accompanyingPeriod = $accompanyingPeriod;

        return $this;
    }

    public function setComment(?string $comment = null): self
    {
        $this->comment = (string) $comment;

        return $this;
    }

    /**
     * @param $resource Person|ThirdParty
     */
    public function setResource($resource): self
    {
        if ($resource instanceof ThirdParty) {
            $this->setThirdParty($resource);
            $this->setPerson(null);
        } elseif ($resource instanceof Person) {
            $this->setPerson($resource);
            $this->setThirdParty(null);
        } elseif (null === $resource) {
            $this->setPerson(null);
            $this->setThirdParty(null);
        } else {
            throw new \UnexpectedValueException(sprintf('the resource should be an instance of %s or %s', Person::class, ThirdParty::class));
        }

        return $this;
    }

    private function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    private function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }
}
