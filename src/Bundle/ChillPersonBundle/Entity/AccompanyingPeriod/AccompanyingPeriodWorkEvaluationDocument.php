<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['accompanying_period_work_evaluation_document' => AccompanyingPeriodWorkEvaluationDocument::class])]
#[ORM\Entity]
#[ORM\Table('chill_person_accompanying_period_work_evaluation_document')]
class AccompanyingPeriodWorkEvaluationDocument implements \Chill\MainBundle\Doctrine\Model\TrackCreationInterface, \Chill\MainBundle\Doctrine\Model\TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    #[ORM\ManyToOne(targetEntity: AccompanyingPeriodWorkEvaluation::class, inversedBy: 'documents')]
    private ?AccompanyingPeriodWorkEvaluation $accompanyingPeriodWorkEvaluation = null;

    /**
     * @internal the default name exceeds 64 characters, we must set manually:
     */
    #[Serializer\Groups(['read', 'accompanying_period_work_evaluation:create'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\SequenceGenerator(sequenceName: 'chill_person_social_work_eval_doc_id_seq', allocationSize: 1, initialValue: 1000)]
    private ?int $id = null;

    /**
     * This is a workaround for client, to allow them to assign arbitrary data
     * dedicated to their job.
     *
     * This data is not persisted into database, but will appears on the data
     * normalized during the same request (like PUT/PATCH request)
     */
    #[Serializer\Groups(['read', 'write', 'accompanying_period_work_evaluation:create'])]
    private $key;

    #[Serializer\Groups(['read', 'write', 'accompanying_period_work_evaluation:create'])]
    #[Assert\Valid]
    #[ORM\ManyToOne(targetEntity: StoredObject::class)]
    private ?StoredObject $storedObject = null;

    #[Serializer\Groups(['read', 'accompanying_period_work_evaluation:create'])]
    #[ORM\ManyToOne(targetEntity: DocGeneratorTemplate::class)]
    private ?DocGeneratorTemplate $template = null;

    /**
     * Store the title only if the storedObject is not yet associated with the instance.
     */
    private string $proxyTitle = '';

    public function getAccompanyingPeriodWorkEvaluation(): ?AccompanyingPeriodWorkEvaluation
    {
        return $this->accompanyingPeriodWorkEvaluation;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getStoredObject(): ?StoredObject
    {
        return $this->storedObject;
    }

    public function getTemplate(): ?DocGeneratorTemplate
    {
        return $this->template;
    }

    #[Serializer\Groups(['read'])]
    public function getTitle(): ?string
    {
        return (string) $this->getStoredObject()?->getTitle();
    }

    public function setAccompanyingPeriodWorkEvaluation(?AccompanyingPeriodWorkEvaluation $accompanyingPeriodWorkEvaluation): AccompanyingPeriodWorkEvaluationDocument
    {
        // if an evaluation is already associated, we cannot change the association (removing the association,
        // by setting a null value, is allowed.
        if (
            $this->accompanyingPeriodWorkEvaluation instanceof AccompanyingPeriodWorkEvaluation
            && $accompanyingPeriodWorkEvaluation instanceof AccompanyingPeriodWorkEvaluation
        ) {
            if ($this->accompanyingPeriodWorkEvaluation !== $accompanyingPeriodWorkEvaluation) {
                throw new \RuntimeException('It is not allowed to change the evaluation for a document');
            }
        }
        $this->accompanyingPeriodWorkEvaluation = $accompanyingPeriodWorkEvaluation;

        return $this;
    }

    /**
     * @return AccompanyingPeriodWorkEvaluationDocument
     */
    public function setKey(mixed $key)
    {
        $this->key = $key;

        return $this;
    }

    public function setStoredObject(?StoredObject $storedObject): AccompanyingPeriodWorkEvaluationDocument
    {
        $this->storedObject = $storedObject;

        if ('' !== $this->proxyTitle) {
            $this->storedObject->setTitle($this->proxyTitle);
        }

        return $this;
    }

    public function setTemplate(?DocGeneratorTemplate $template): AccompanyingPeriodWorkEvaluationDocument
    {
        $this->template = $template;

        return $this;
    }

    #[Serializer\Groups(['write', 'accompanying_period_work_evaluation:create'])]
    public function setTitle(?string $proxyTitle): AccompanyingPeriodWorkEvaluationDocument
    {
        if (null !== $this->storedObject) {
            $this->storedObject->setTitle((string) $proxyTitle);
        } else {
            $this->proxyTitle = (string) $proxyTitle;
        }

        return $this;
    }
}
