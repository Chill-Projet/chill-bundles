<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Entity\Address;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table('chill_person_accompanying_period_location_history')]
class AccompanyingPeriodLocationHistory implements TrackCreationInterface
{
    use TrackCreationTrait;

    #[ORM\ManyToOne(targetEntity: Address::class, cascade: ['persist'])]
    private ?Address $addressLocation = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $endDate = null;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: AccompanyingPeriod::class)]
    private AccompanyingPeriod $period;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $personLocation = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE)]
    private ?\DateTimeImmutable $startDate = null;

    public function getAddressLocation(): ?Address
    {
        return $this->addressLocation;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriod(): AccompanyingPeriod
    {
        return $this->period;
    }

    public function getPersonLocation(): ?Person
    {
        return $this->personLocation;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setAddressLocation(?Address $addressLocation): AccompanyingPeriodLocationHistory
    {
        $this->addressLocation = $addressLocation;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): AccompanyingPeriodLocationHistory
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @internal use AccompanyingPeriod::addLocationHistory
     */
    public function setPeriod(AccompanyingPeriod $period): AccompanyingPeriodLocationHistory
    {
        $this->period = $period;

        return $this;
    }

    public function setPersonLocation(?Person $personLocation): AccompanyingPeriodLocationHistory
    {
        $this->personLocation = $personLocation;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): AccompanyingPeriodLocationHistory
    {
        $this->startDate = $startDate;

        return $this;
    }
}
