<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Relationships;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

#[DiscriminatorMap(typeProperty: 'type', mapping: ['relation' => Relation::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_relations')]
class Relation
{
    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $isActive = true;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $reverseTitle = [];

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $title = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getReverseTitle(): ?array
    {
        return $this->reverseTitle;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setReverseTitle(?array $reverseTitle): self
    {
        $this->reverseTitle = $reverseTitle;

        return $this;
    }

    public function setTitle(?array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
