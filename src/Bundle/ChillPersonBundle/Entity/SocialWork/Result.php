<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\SocialWork;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkGoal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['social_work_result' => Result::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_social_work_result')]
class Result
{
    /**
     * @var Collection<int, AccompanyingPeriodWorkGoal>
     */
    #[ORM\ManyToMany(targetEntity: AccompanyingPeriodWorkGoal::class, mappedBy: 'results')]
    private Collection $accompanyingPeriodWorkGoals;

    /**
     * @var Collection<int, AccompanyingPeriodWork>
     */
    #[ORM\ManyToMany(targetEntity: AccompanyingPeriodWork::class, mappedBy: 'results')]
    private Collection $accompanyingPeriodWorks;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $desactivationDate = null;

    /**
     * @var Collection<int, Goal>
     */
    #[ORM\ManyToMany(targetEntity: Goal::class, mappedBy: 'results')]
    private Collection $goals;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    /**
     * @var Collection<int, SocialAction>
     */
    #[ORM\ManyToMany(targetEntity: SocialAction::class, mappedBy: 'results')]
    private Collection $socialActions;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $title = [];

    public function __construct()
    {
        $this->socialActions = new ArrayCollection();
        $this->goals = new ArrayCollection();
        $this->accompanyingPeriodWorks = new ArrayCollection();
        $this->accompanyingPeriodWorkGoals = new ArrayCollection();
    }

    public function addAccompanyingPeriodWork(AccompanyingPeriodWork $accompanyingPeriod): self
    {
        if (!$this->accompanyingPeriodWorks->contains($accompanyingPeriod)) {
            $this->accompanyingPeriodWorks[] = $accompanyingPeriod;
        }

        return $this;
    }

    public function addAccompanyingPeriodWorkGoal(AccompanyingPeriodWorkGoal $accompanyingPeriodWorkGoal): self
    {
        if (!$this->accompanyingPeriodWorkGoals->contains($accompanyingPeriodWorkGoal)) {
            $this->accompanyingPeriodWorkGoals[] = $accompanyingPeriodWorkGoal;
        }

        return $this;
    }

    public function addGoal(Goal $goal): self
    {
        if (!$this->goals->contains($goal)) {
            $this->goals[] = $goal;
        }

        return $this;
    }

    public function addSocialAction(SocialAction $socialAction): self
    {
        if (!$this->socialActions->contains($socialAction)) {
            $this->socialActions[] = $socialAction;
        }

        return $this;
    }

    /**
     * @return AccompanyingPeriodWorkGoal[]|Collection
     */
    public function getAccompanyingPeriodWorkGoals(): Collection
    {
        return $this->accompanyingPeriodWorkGoals;
    }

    /**
     * @return AccompanyingPeriodWork[]|Collection
     */
    public function getAccompanyingPeriodWorks(): Collection
    {
        return $this->accompanyingPeriodWorks;
    }

    public function getDesactivationDate(): ?\DateTimeInterface
    {
        return $this->desactivationDate;
    }

    /**
     * @return Collection|Goal[]
     */
    public function getGoals(): Collection
    {
        return $this->goals;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|SocialAction[]
     */
    public function getSocialActions(): Collection
    {
        return $this->socialActions;
    }

    public function getTitle(): array
    {
        return $this->title;
    }

    public function removeAccompanyingPeriodWork(AccompanyingPeriodWork $accompanyingPeriod): self
    {
        $this->accompanyingPeriodWorks->removeElement($accompanyingPeriod);

        return $this;
    }

    public function removeAccompanyingPeriodWorkGoal(AccompanyingPeriodWorkGoal $accompanyingPeriodWorkGoal): self
    {
        $this->accompanyingPeriodWorkGoals->removeElement($accompanyingPeriodWorkGoal);

        return $this;
    }

    public function removeGoal(Goal $goal): self
    {
        $this->goals->removeElement($goal);

        return $this;
    }

    public function removeSocialAction(SocialAction $socialAction): self
    {
        $this->socialActions->removeElement($socialAction);

        return $this;
    }

    public function setDesactivationDate(?\DateTimeInterface $desactivationDate): self
    {
        $this->desactivationDate = $desactivationDate;

        return $this;
    }

    public function setTitle(array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
