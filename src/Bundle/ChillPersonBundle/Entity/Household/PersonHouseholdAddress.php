<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Chill\MainBundle\Entity\Address;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class links a person to the history of his addresses, through
 * household membership.
 *
 * It is optimized on DB side, and compute the start date and end date
 * of each address by the belonging of household.
 *
 * **note**: the start date and end date are the date of belonging to the address,
 * not the belonging of the household.
 *
 * Example:
 *
 * * person A is member of household W from 2021-01-01 to 2021-12-01
 * * person A is member of household V from 2021-12-01, still present after
 * * household W lives in address Q from 2020-06-01 to 2021-06-01
 * * household W lives in address R from 2021-06-01 to 2022-06-01
 * * household V lives in address T from 2021-12-01 to still living there after
 *
 * The person A will have those 3 entities:
 *
 * 1. 1st entity: from 2021-01-01 to 2021-06-01, household W, address Q;
 * 2. 2st entity: from 2021-06-01 to 2021-12-01, household W, address R;
 * 3. 3st entity: from 2021-12-01 to NULL, household V, address T;
 */
#[ORM\Entity(readOnly: true)]
#[ORM\Table(name: 'view_chill_person_household_address')]
class PersonHouseholdAddress
{
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Address::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Address $address = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Household::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Household $household = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Person $person = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE)]
    private $validFrom;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE, nullable: true)]
    private $validTo;

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getHousehold(): ?Household
    {
        return $this->household;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * The start date of the intersection address/household.
     *
     * (this is not the startdate of the household, not
     * the startdate of the address)
     */
    public function getValidFrom(): ?\DateTimeInterface
    {
        return $this->validFrom;
    }

    /**
     * The end date of the intersection address/household.
     *
     * (this is not the enddate of the household, not
     * the enddate of the address)
     */
    public function getValidTo(): ?\DateTimeImmutable
    {
        return $this->validTo;
    }
}
