<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['household_position' => Position::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_household_position')]
class Position
{
    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $allowHolder = false;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $label = [];

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::FLOAT)]
    private float $ordering = 0.00;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $shareHouseHold = true;

    public function getAllowHolder(): bool
    {
        return $this->allowHolder;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    public function getShareHousehold(): bool
    {
        return $this->shareHouseHold;
    }

    public function isAllowHolder(): bool
    {
        return $this->getAllowHolder();
    }

    public function setAllowHolder(bool $allowHolder): self
    {
        $this->allowHolder = $allowHolder;

        return $this;
    }

    public function setLabel(array $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function setOrdering(float $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function setShareHousehold(bool $shareHouseHold): self
    {
        $this->shareHouseHold = $shareHouseHold;

        return $this;
    }
}
