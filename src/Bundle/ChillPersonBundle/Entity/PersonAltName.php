<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * PersonAltName.
 */
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_alt_name')]
class PersonAltName
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[Groups(['write'])]
    #[ORM\Column(name: 'key', type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private string $key = '';

    #[Groups(['write'])]
    #[ORM\Column(name: 'label', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $label = '';

    #[ORM\ManyToOne(targetEntity: Person::class, inversedBy: 'altNames')]
    private ?Person $person = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * Set key.
     *
     * @return PersonAltName
     */
    public function setKey(?string $key)
    {
        $this->key = (string) $key;

        return $this;
    }

    /**
     * Set label.
     *
     * @return PersonAltName
     */
    public function setLabel(?string $label)
    {
        $this->label = (string) $label;

        return $this;
    }

    /**
     * @return $this
     */
    public function setPerson(?Person $person = null)
    {
        $this->person = $person;

        return $this;
    }
}
