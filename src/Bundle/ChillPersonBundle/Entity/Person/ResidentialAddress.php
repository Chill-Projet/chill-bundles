<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Person;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\Embeddable\CommentEmbeddable;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\ResidentialAddressRepository;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ResidentialAddressRepository::class)]
#[ORM\Table(name: 'chill_person_residential_address')]
class ResidentialAddress
{
    #[Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: Address::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Address $address = null;

    #[ORM\Embedded(class: CommentEmbeddable::class, columnPrefix: 'residentialAddressComment_')]
    private CommentEmbeddable $comment;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $endDate = null;

    #[Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Context(normalizationContext: ['groups' => ['minimal']])]
    private ?Person $hostPerson = null;

    #[Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?ThirdParty $hostThirdParty = null;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Person $person;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $startDate = null;

    public function __construct()
    {
        $this->comment = new CommentEmbeddable();
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getComment(): CommentEmbeddable
    {
        return $this->comment;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getHostPerson(): ?Person
    {
        return $this->hostPerson;
    }

    public function getHostThirdParty(): ?ThirdParty
    {
        return $this->hostThirdParty;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setComment(CommentEmbeddable $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setHostPerson(?Person $hostPerson): self
    {
        $this->hostPerson = $hostPerson;

        return $this;
    }

    public function setHostThirdParty(?ThirdParty $hostThirdParty): self
    {
        $this->hostThirdParty = $hostThirdParty;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }
}
