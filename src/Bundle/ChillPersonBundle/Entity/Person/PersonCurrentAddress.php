<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Person;

use Chill\MainBundle\Entity\Address;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entity which associate person with his current address, through
 * household participation.
 *
 * The computation is optimized on database side.
 *
 * The validFrom and validTo properties are the intersection of
 * household membership and address validity. See @see{PersonHouseholdAddress}
 */
#[ORM\Entity(readOnly: true)]
#[ORM\Table('view_chill_person_current_address')]
class PersonCurrentAddress
{
    #[ORM\OneToOne(targetEntity: Address::class)]
    protected Address $address;

    #[ORM\Id]
    #[ORM\OneToOne(targetEntity: Person::class, inversedBy: 'currentPersonAddress')]
    #[ORM\JoinColumn(name: 'person_id', referencedColumnName: 'id')]
    protected Person $person;

    #[ORM\Column(name: 'valid_from', type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE)]
    protected \DateTimeImmutable $validFrom;

    #[ORM\Column(name: 'valid_to', type: \Doctrine\DBAL\Types\Types::DATE_IMMUTABLE)]
    protected ?\DateTimeImmutable $validTo = null;

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * This date is the intersection of household membership
     * and address validity.
     */
    public function getValidFrom(): \DateTimeImmutable
    {
        return $this->validFrom;
    }

    /**
     * This date is the intersection of household membership
     * and address validity.
     */
    public function getValidTo(): ?\DateTimeImmutable
    {
        return $this->validTo;
    }
}
