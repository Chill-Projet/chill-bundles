<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;

/**
 * Person Phones.
 */
#[ORM\Entity]
#[ORM\Table(name: 'chill_person_phone')]
#[ORM\Index(name: 'phonenumber', columns: ['phonenumber'])]
class PersonPhone
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: false)]
    private \DateTime $date;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Person::class, inversedBy: 'otherPhoneNumbers')]
    private Person $person;

    /**
     * The phonenumber.
     *
     * This phonenumber is nullable: this allow user to store some notes instead of a phonenumber
     */
    #[ORM\Column(type: 'phone_number', nullable: true)]
    private ?PhoneNumber $phonenumber = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, length: 40, nullable: true)]
    private ?string $type = null;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getPhonenumber(): ?PhoneNumber
    {
        return $this->phonenumber;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isEmpty(): bool
    {
        return ('' === $this->getDescription() || null === $this->getDescription())
            && null === $this->getPhonenumber();
    }

    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function setPerson(Person $person): void
    {
        $this->person = $person;
    }

    public function setPhonenumber(?PhoneNumber $phonenumber): void
    {
        $this->phonenumber = $phonenumber;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
