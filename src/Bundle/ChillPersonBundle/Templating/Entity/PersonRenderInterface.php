<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\PersonBundle\Entity\Person;

/**
 * Render a Person.
 *
 * @extends ChillEntityRenderInterface<Person>
 */
interface PersonRenderInterface extends ChillEntityRenderInterface {}
