<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Tests\Controller;

use Chill\DocStoreBundle\Controller\GenericDocForAccompanyingPeriodListApiController;
use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\ManagerInterface;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\MainBundle\Pagination\Paginator;
use Chill\MainBundle\Pagination\PaginatorFactoryInterface;
use Chill\MainBundle\Serializer\Model\Collection;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class GenericDocForAccompanyingPeriodListApiControllerTest extends TestCase
{
    public function testSmokeTest(): void
    {
        $accompanyingPeriod = new AccompanyingPeriod();

        $docs = [
            new GenericDocDTO('dummy', ['id' => 9], new \DateTimeImmutable('2024-08-01'), $accompanyingPeriod),
            new GenericDocDTO('dummy', ['id' => 1], new \DateTimeImmutable('2024-09-01'), $accompanyingPeriod),
        ];


        $manager = $this->createMock(ManagerInterface::class);
        $manager->method('findDocForAccompanyingPeriod')->with($accompanyingPeriod)->willReturn($docs);
        $manager->method('countDocForAccompanyingPeriod')->with($accompanyingPeriod)->willReturn(2);

        $paginatorFactory = $this->createMock(PaginatorFactoryInterface::class);
        $paginatorFactory->method('create')->with(2)->willReturn(new Paginator(
            2,
            20,
            1,
            '/route',
            [],
            $this->createMock(UrlGeneratorInterface::class),
            'page',
            'item-per-page'
        ));

        $serializer = $this->createMock(SerializerInterface::class);
        $serializer->method('serialize')->with($this->isInstanceOf(Collection::class))->willReturn(
            json_encode(['docs' => []])
        );

        $security = $this->createMock(Security::class);
        $security->expects($this->once())->method('isGranted')
            ->with(AccompanyingCourseDocumentVoter::SEE, $accompanyingPeriod)->willReturn(true);

        $controller = new GenericDocForAccompanyingPeriodListApiController(
            $manager,
            $security,
            $paginatorFactory,
            $serializer,
        );

        $response = $controller($accompanyingPeriod);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals('{"docs":[]}', $response->getContent());
    }
}
