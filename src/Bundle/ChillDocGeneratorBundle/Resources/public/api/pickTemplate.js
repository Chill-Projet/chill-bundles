import { fetchResults } from "ChillMainAssets/lib/api/apiMethods.ts";

const fetchTemplates = (entityClass) => {
  let fqdnEntityClass = encodeURI(entityClass);
  return fetchResults(`/api/1.0/docgen/templates/by-entity/${fqdnEntityClass}`);
};

export { fetchTemplates };
