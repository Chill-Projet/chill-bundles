const buildLink = function (templateId, entityId, entityClass) {
  const entityIdEncoded = encodeURI(entityId),
    returnPath = encodeURIComponent(
      window.location.pathname + window.location.search + window.location.hash,
    ),
    entityClassEncoded = encodeURI(entityClass),
    url = `/fr/doc/gen/generate/from/${templateId}/for/${entityClassEncoded}/${entityIdEncoded}?returnPath=${returnPath}`;
  console.log("computed Url");
  return url;
};

export { buildLink };
