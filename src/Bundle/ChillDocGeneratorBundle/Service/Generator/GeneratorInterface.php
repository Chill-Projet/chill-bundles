<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Generator;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Exception\StoredObjectManagerException;
use Chill\MainBundle\Entity\User;

interface GeneratorInterface
{
    /**
     * Generate a document and store the document on disk.
     *
     * The given $destinationStoredObject will be updated with filename, status, and eventually errors will be stored
     * into the object. The number of generation trial will also be incremented.
     *
     * This process requires a huge amount of data. For this reason, the entity manager will be cleaned during the process,
     * unless the paarameter `$clearEntityManagerDuringProcess` is set on false.
     *
     * As the entity manager might be cleaned, the new instance of the stored object will be returned by this method.
     *
     * Ensure to store change in the database after each generation trial (call `EntityManagerInterface::flush`).
     *
     * @phpstan-impure
     *
     * @param StoredObject $destinationStoredObject will be update with filename, status and incremented of generation trials
     *
     * @throws StoredObjectManagerException if unable to decrypt the template or store the document
     */
    public function generateDocFromTemplate(
        DocGeneratorTemplate $template,
        int $entityId,
        array $contextGenerationDataNormalized,
        StoredObject $destinationStoredObject,
        User $creator,
        bool $clearEntityManagerDuringProcess = true,
    ): StoredObject;

    /**
     * Generate a data dump, and store it within the `$destinationStoredObject`.
     */
    public function generateDataDump(
        DocGeneratorTemplate $template,
        int $entityId,
        array $contextGenerationDataNormalized,
        StoredObject $destinationStoredObject,
        User $creator,
        bool $clearEntityManagerDuringProcess = true,
    ): StoredObject;
}
