<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Generator;

class ObjectReadyException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('object is already ready', 6_698_856);
    }
}
