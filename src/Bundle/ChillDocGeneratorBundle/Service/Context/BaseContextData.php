<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Context;

use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BaseContextData
{
    public function __construct(private readonly NormalizerInterface $normalizer) {}

    public function getData(?User $user = null): array
    {
        $data = [];

        $data['creator'] = $this->normalizer->normalize(
            $user instanceof User ? $user : null,
            'docgen',
            ['docgen:expects' => User::class, 'groups' => ['docgen:read']]
        );
        $data['createdAt'] = $this->normalizer->normalize(new \DateTimeImmutable(), 'docgen', [
            'docgen:expects' => \DateTimeImmutable::class, 'groups' => ['docgen:read'],
        ]);
        $data['createdAtDate'] = $this->normalizer->normalize(new \DateTimeImmutable('today'), 'docgen', [
            'docgen:expects' => \DateTimeImmutable::class, 'groups' => ['docgen:read'],
        ]);
        $data['location'] = $this->normalizer->normalize(
            $user instanceof User ? $user->getCurrentLocation() : null,
            'docgen',
            ['docgen:expects' => Location::class, 'groups' => ['docgen:read']]
        );

        return $data;
    }
}
