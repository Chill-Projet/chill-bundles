<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211119173556 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'remove comment on deprecated json_array type';
    }

    public function up(Schema $schema): void
    {
        $columns = [
            'chill_docgen_template.name',
        ];

        foreach ($columns as $col) {
            $this->addSql("COMMENT ON COLUMN {$col} IS NULL");
        }
    }
}
