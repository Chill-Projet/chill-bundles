<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\tests\Service\Context;

use Chill\DocGeneratorBundle\Service\Context\BaseContextData;
use Chill\MainBundle\Entity\User;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class BaseContextDataTest extends KernelTestCase
{
    use ProphecyTrait;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testGenerateNoContext()
    {
        $context = $this->buildBaseContext();

        $actual = $context->getData();

        $this->assertIsArray($actual);
        $this->assertArrayHasKey('creator', $actual);
        $this->assertArrayHasKey('createdAt', $actual);
        $this->assertArrayHasKey('location', $actual);
    }

    public function testGenerateWithUser()
    {
        $context = $this->buildBaseContext();

        $actual = $context->getData(new User());

        $this->assertIsArray($actual);
        $this->assertArrayHasKey('creator', $actual);
        $this->assertArrayHasKey('createdAt', $actual);
        $this->assertArrayHasKey('location', $actual);
    }

    private function buildBaseContext(
        ?NormalizerInterface $normalizer = null,
    ): BaseContextData {
        return new BaseContextData(
            $normalizer ?? self::getContainer()->get(NormalizerInterface::class)
        );
    }
}
