<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\tests\Serializer\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CollectionDocGenNormalizerTest extends KernelTestCase
{
    private NormalizerInterface $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->normalizer = self::getContainer()->get(NormalizerInterface::class);
    }

    public function testNormalizeFilteredArray(): void
    {
        $coll = new ArrayCollection([
            (object) ['v' => 'foo'],
            (object) ['v' => 'bar'],
            (object) ['v' => 'baz'],
        ]);

        // filter to get non continuous indexes
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->neq('v', 'bar'));

        $filtered = $coll->matching($criteria);
        $normalized = $this->normalizer->normalize($filtered, 'docgen', []);

        self::assertIsArray($normalized);
        self::assertArrayHasKey(0, $normalized);
        self::assertArrayHasKey(1, $normalized);
    }
}
