// this file loads all assets from the Chill DocGenerator bundle
module.exports = function (encore, entries) {
  encore.addAliases({
    ChillDocGeneratorAssets: __dirname + "/Resources/public",
  });

  encore.addEntry(
    "mod_docgen_picktemplate",
    __dirname + "/Resources/public/module/PickTemplate/index.js",
  );
};
