<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillDocGeneratorExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('chill_doc_generator', $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/form.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->preprendRoutes($container);
        $this->prependCruds($container);
        $this->prependClientConfig($container);
    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => \Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate::class,
                    'name' => 'docgen_template',
                    'base_path' => '/admin/docgen/template',
                    'form_class' => \Chill\DocGeneratorBundle\Form\DocGeneratorTemplateType::class,
                    'controller' => \Chill\DocGeneratorBundle\Controller\AdminDocGeneratorTemplateController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillDocGenerator/DocGeneratorTemplate/index.html.twig',
                            'role' => 'ROLE_ADMIN',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillDocGenerator/DocGeneratorTemplate/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillDocGenerator/DocGeneratorTemplate/edit.html.twig',
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function preprendRoutes(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillDocGeneratorBundle/config/routes.yaml',
                ],
            ],
        ]);
    }

    private function prependClientConfig(ContainerBuilder $container)
    {
        $configs = $container->getExtensionConfig($this->getAlias());
        $resolvingBag = $container->getParameterBag();
        $configs = $resolvingBag->resolveValue($configs);

        $config = $this->processConfiguration(new Configuration(), $configs);

        $container->prependExtensionConfig('framework', [
            'http_client' => [
                'scoped_clients' => [
                    'relatorio.client' => [
                        'scope' => $config['driver']['relatorio']['url'],
                    ],
                ],
            ],
        ]);
    }
}
