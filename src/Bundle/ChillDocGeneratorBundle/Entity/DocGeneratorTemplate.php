<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Entity;

use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['docgen_template' => DocGeneratorTemplate::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_docgen_template')]
class DocGeneratorTemplate
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $active = true;

    /**
     * Class name of the context to use.
     *
     * so if $context = ''
     * this template will use '' as context
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private string $context;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $description = null;

    /**
     * Class name of the entity for which this template can be used.
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, options: ['default' => ''])]
    private string $entity = '';

    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $file = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    private array $name = [];

    /**
     * Options for the template.
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, name: 'template_options', options: ['default' => '[]'])]
    private array $options = [];

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getEntity(): string
    {
        return $this->entity;
    }

    public function getFile(): ?StoredObject
    {
        return $this->file;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): DocGeneratorTemplate
    {
        $this->active = $active;

        return $this;
    }

    public function setContext(string $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function setFile(StoredObject $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }
}
