import { ShowHide } from "ShowHide/show_hide.js";
var ressources = document.getElementById("ressources"),
  ressources_comment = document.getElementById("ressourcesComment"),
  handicap_is = document.getElementById("handicap_is"),
  handicap_if = document.getElementById("handicap_if"),
  situation_prof = document.getElementById("situation_prof"),
  type_contrat = document.getElementById("type_contrat"),
  type_contrat_aide = document.getElementById("type_contrat_aide"),
  situation_logement = document.getElementById("situation_logement"),
  situation_logement_precision = document.getElementById(
    "situation_logement_precision",
  );
new ShowHide({
  froms: [ressources],
  container: [ressources_comment],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input").values()) {
        if (input.value === "autre") {
          return input.checked;
        }
      }
    }
  },
});

new ShowHide({
  froms: [handicap_is],
  container: [handicap_if],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input").values()) {
        if (input.value === "1") {
          return input.checked;
        }
      }
    }

    return false;
  },
});

var show_hide_contrat_aide = new ShowHide({
  froms: [type_contrat],
  container: [type_contrat_aide],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input").values()) {
        if (input.value === "contrat_aide") {
          return input.checked;
        }
      }
    }

    return false;
  },
});

new ShowHide({
  id: "situation_prof_type_contrat",
  froms: [situation_prof],
  container: [type_contrat],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input").values()) {
        if (input.value === "en_activite") {
          return input.checked;
        }
      }
    }

    return false;
  },
});

window.addEventListener("show-hide-hide", function (e) {
  if ((e.detail.id = "situation_prof_type_contrat")) {
    show_hide_contrat_aide.forceHide();
  }
});

window.addEventListener("show-hide-show", function (e) {
  if ((e.detail.id = "situation_prof_type_contrat")) {
    show_hide_contrat_aide.forceCompute();
  }
});

new ShowHide({
  froms: [situation_logement],
  container: [situation_logement_precision],
  test: function (froms) {
    for (let f of froms.values()) {
      for (let input of f.querySelectorAll("input")) {
        if (input.value === "heberge_chez_tiers") {
          return input.checked;
        }
      }
    }

    return false;
  },
});
