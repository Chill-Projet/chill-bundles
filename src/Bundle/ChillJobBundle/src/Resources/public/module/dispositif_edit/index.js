import { ShowHide } from "ShowHide/show_hide.js";

var div_accompagnement = document.getElementById("form_accompagnement"),
  div_accompagnement_rqth = document.getElementById("form_accompagnement_rqth"),
  div_accompagnement_comment = document.getElementById(
    "form_accompagnement_comment",
  ),
  div_caf_id = document.getElementById("cafId"),
  div_caf_inscription_date = document.getElementById("cafInscriptionDate"),
  div_neet_eligibilite = document.getElementById("neetEligibilite"),
  div_neet_commission_date = document.getElementById("neetCommissionDate");
// faire apparaitre / disparaitre RQTH si RQTH coché
new ShowHide({
  froms: [div_accompagnement],
  test: function (froms, event) {
    for (let el of froms.values()) {
      for (let input of el.querySelectorAll("input").values()) {
        if (input.value === "rqth") {
          return input.checked;
        }
      }
    }
  },
  container: [div_accompagnement_rqth],
});

// faire apparaitre / disparaitre commetnaire si coché
new ShowHide({
  froms: [div_accompagnement],
  test: function (froms, event) {
    for (let el of froms.values()) {
      for (let input of el.querySelectorAll("input").values()) {
        if (input.value === "autre") {
          return input.checked;
        }
      }
    }

    return false;
  },
  container: [div_accompagnement_comment],
});

// faire apparaitre cafInscriptionDate seulement si cafID est rempli
new ShowHide({
  froms: [div_caf_id],
  test: function (froms, event) {
    for (let el of froms.values()) {
      return el.querySelector("input").value !== "";
    }
  },
  container: [div_caf_inscription_date],
  event_name: "input",
});

// faire apparaitre date de commission neet seulement si eligible
new ShowHide({
  froms: [div_neet_eligibilite],
  test: function (froms, event) {
    for (let el of froms.values()) {
      for (let input of el.querySelectorAll("input").values()) {
        if (input.value === "oui") {
          return input.checked;
        }
      }
    }

    return false;
  },
  container: [div_neet_commission_date],
});
