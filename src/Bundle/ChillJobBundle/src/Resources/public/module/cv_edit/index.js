import { ShowHide } from "ShowHide/show_hide.js";
// listen to adding of formation and register a show hide

var make_show_hide = function (entry) {
  let obtained = entry.querySelector("[data-diploma-obtained]"),
    reconnue = entry.querySelector("[data-diploma-reconnue]");
  var a = new ShowHide({
    load_event: null,
    froms: [obtained],
    container: [reconnue],
    test: function (froms, event) {
      for (let f of froms.values()) {
        for (let input of f.querySelectorAll("input").values()) {
          if (input.value === "non-fr") {
            return input.checked;
          }
        }
      }

      return false;
    },
  });
};

window.addEventListener("collection-add-entry", function (e) {
  if (e.detail.collection.dataset.collectionName === "formations") {
    make_show_hide(e.detail.entry);
  }
});

// starting the formation on load
window.addEventListener("load", function (_e) {
  let formations = document.querySelectorAll("[data-formation-entry]");
  for (let f of formations.values()) {
    make_show_hide(f);
  }
});
