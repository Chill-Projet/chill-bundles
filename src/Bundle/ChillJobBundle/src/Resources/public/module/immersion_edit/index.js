import { ShowHide } from "ShowHide/show_hide.js";

var div_objectifs = document.getElementById("objectifs"),
  div_objectifs_autre = document.getElementById("objectifsAutre");
new ShowHide({
  froms: [div_objectifs],
  container: [div_objectifs_autre],
  test: function (froms, event) {
    for (let el of froms.values()) {
      for (let input of el.querySelectorAll("input").values()) {
        if (input.value === "autre") {
          return input.checked;
        }
      }
    }
  },
});
