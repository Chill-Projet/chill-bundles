<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity;

use Chill\JobBundle\Repository\ProjetProfessionnelRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Chill\PersonBundle\Entity\Person;
use Chill\JobBundle\Entity\Rome\Appellation;

/**
 * ProjetProfessionnel.
 */
#[ORM\Table(name: 'chill_job.projet_professionnel')]
#[ORM\Entity(repositoryClass: ProjetProfessionnelRepository::class)]
class ProjetProfessionnel implements \Stringable
{
    #[ORM\Column(name: 'id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    /**
     * @Assert\NotNull()
     */
    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person = null;

    /**
     * @Assert\NotNull()
     */
    #[ORM\Column(name: 'reportDate', type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $reportDate = null;

    /**
     * @var Collection<int, Appellation>
     */
    #[ORM\JoinTable(name: 'chill_job.projetprofessionnel_souhait')]
    #[ORM\ManyToMany(targetEntity: Appellation::class, cascade: ['persist'])]
    private Collection $souhait;

    #[ORM\Column(name: 'domaineActiviteSouhait', type: Types::TEXT, nullable: true)]
    private ?string $domaineActiviteSouhait = null;

    public const TYPE_CONTRAT = [
        'cdd', 'cdi', 'contrat_insertion',
        'interim', 'indifferent', 'apprentissage',
        'personnalisation', 'creation_entreprise',
    ];

    /**
     * @var array|null
     *
     * @Assert\Count(min=1, minMessage="Indiquez au moins un type de contrat")
     */
    #[ORM\Column(name: 'typeContrat', type: Types::JSON, nullable: true)]
    private $typeContrat;

    #[ORM\Column(name: 'typeContratNotes', type: Types::TEXT, nullable: true)]
    private ?string $typeContratNotes = null;

    public const VOLUME_HORAIRES = [
        'temps_plein',
        'temps_partiel',
    ];

    /**
     * @var array|null
     *
     * @Assert\Count(min=1, minMessage="Indiquez un volume horaire souhaité")
     */
    #[ORM\Column(name: 'volumeHoraire', type: Types::JSON, nullable: true)]
    private $volumeHoraire;

    #[ORM\Column(name: 'volumeHoraireNotes', type: Types::TEXT, nullable: true)]
    private ?string $volumeHoraireNotes = null;

    #[ORM\Column(name: 'idee', type: Types::TEXT, nullable: true)]
    private ?string $idee = null;

    #[ORM\Column(name: 'enCoursConstruction', type: Types::TEXT, nullable: true)]
    private ?string $enCoursConstruction = null;

    /**
     * @var Collection<int, Appellation>
     */
    #[ORM\JoinTable(name: 'chill_job.projetprofessionnel_valide')]
    #[ORM\ManyToMany(targetEntity: Appellation::class)]
    private Collection $valide;

    #[ORM\Column(name: 'domaineActiviteValide', type: Types::TEXT, nullable: true)]
    private ?string $domaineActiviteValide = null;

    #[ORM\Column(name: 'valideNotes', type: Types::TEXT, nullable: true)]
    private ?string $valideNotes = null;

    #[ORM\Column(name: 'projetProfessionnelNote', type: Types::TEXT, nullable: true)]
    private ?string $projetProfessionnelNote = null;

    public function __construct()
    {
        $this->valide = new ArrayCollection();
        $this->souhait = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportDate.
     *
     * @param \DateTime $reportDate
     *
     * @return ProjetProfessionnel
     */
    public function setReportDate(?\DateTimeInterface $reportDate)
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    /**
     * Get reportDate.
     *
     * @return \DateTimeInterface
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * Set typeContrat.
     *
     * @param mixed|null $typeContrat
     *
     * @return ProjetProfessionnel
     */
    public function setTypeContrat($typeContrat = null)
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * Get typeContrat.
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * Set typeContratNotes.
     *
     * @return ProjetProfessionnel
     */
    public function setTypeContratNotes(?string $typeContratNotes = null)
    {
        $this->typeContratNotes = $typeContratNotes;

        return $this;
    }

    /**
     * Get typeContratNotes.
     *
     * @return string|null
     */
    public function getTypeContratNotes()
    {
        return $this->typeContratNotes;
    }

    /**
     * Set volumeHoraire.
     *
     * @param mixed|null $volumeHoraire
     *
     * @return ProjetProfessionnel
     */
    public function setVolumeHoraire($volumeHoraire = null)
    {
        $this->volumeHoraire = $volumeHoraire;

        return $this;
    }

    /**
     * Get volumeHoraire.
     */
    public function getVolumeHoraire()
    {
        return $this->volumeHoraire;
    }

    /**
     * Set volumeHoraireNotes.
     *
     * @return ProjetProfessionnel
     */
    public function setVolumeHoraireNotes(?string $volumeHoraireNotes = null)
    {
        $this->volumeHoraireNotes = $volumeHoraireNotes;

        return $this;
    }

    /**
     * Get volumeHoraireNotes.
     *
     * @return string|null
     */
    public function getVolumeHoraireNotes()
    {
        return $this->volumeHoraireNotes;
    }

    /**
     * Set idee.
     *
     * @return ProjetProfessionnel
     */
    public function setIdee(?string $idee = null)
    {
        $this->idee = $idee;

        return $this;
    }

    /**
     * Get idee.
     *
     * @return string|null
     */
    public function getIdee()
    {
        return $this->idee;
    }

    /**
     * Set enCoursConstruction.
     *
     * @return ProjetProfessionnel
     */
    public function setEnCoursConstruction(?string $enCoursConstruction = null)
    {
        $this->enCoursConstruction = $enCoursConstruction;

        return $this;
    }

    /**
     * Get enCoursConstruction.
     *
     * @return string|null
     */
    public function getEnCoursConstruction()
    {
        return $this->enCoursConstruction;
    }

    /**
     * Set valideNotes.
     *
     * @return ProjetProfessionnel
     */
    public function setValideNotes(?string $valideNotes = null)
    {
        $this->valideNotes = $valideNotes;

        return $this;
    }

    /**
     * Get valideNotes.
     *
     * @return string|null
     */
    public function getValideNotes()
    {
        return $this->valideNotes;
    }

    /**
     * Set projetProfessionnelNote.
     *
     * @return ProjetProfessionnel
     */
    public function setProjetProfessionnelNote(?string $projetProfessionnelNote = null)
    {
        $this->projetProfessionnelNote = $projetProfessionnelNote;

        return $this;
    }

    /**
     * Get projetProfessionnelNote.
     *
     * @return string|null
     */
    public function getProjetProfessionnelNote()
    {
        return $this->projetProfessionnelNote;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getSouhait(): Collection
    {
        return $this->souhait;
    }

    public function getValide(): Collection
    {
        return $this->valide;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    public function setSouhait(Collection $souhait)
    {
        $this->souhait = $souhait;

        return $this;
    }

    public function addSouhait(Appellation $souhait)
    {
        $this->souhait->add($souhait);

        return $this;
    }

    public function setValide(Collection $valide)
    {
        $this->valide = $valide;

        return $this;
    }

    public function addValide(Appellation $valide)
    {
        $this->valide->add($valide);

        return $this;
    }

    public function getDomaineActiviteSouhait(): ?string
    {
        return $this->domaineActiviteSouhait;
    }

    public function getDomaineActiviteValide(): ?string
    {
        return $this->domaineActiviteValide;
    }

    public function setDomaineActiviteSouhait(?string $domaineActiviteSouhait = null)
    {
        $this->domaineActiviteSouhait = $domaineActiviteSouhait;

        return $this;
    }

    public function setDomaineActiviteValide(?string $domaineActiviteValide = null)
    {
        $this->domaineActiviteValide = $domaineActiviteValide;

        return $this;
    }

    public function __toString(): string
    {
        return 'Rapport "projet professionnel" de '.$this->getPerson().' daté du '.
            $this->getReportDate()->format('d-m-Y');
    }
}
