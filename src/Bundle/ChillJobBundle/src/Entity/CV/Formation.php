<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity\CV;

use Doctrine\ORM\Mapping as ORM;
use Chill\JobBundle\Entity\CV;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Formation.
 */
#[ORM\Table(name: 'chill_job.cv_formation')]
#[ORM\Entity(repositoryClass: \Chill\JobBundle\Repository\CV\FormationRepository::class)]
class Formation
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    /**
     * @Assert\Length(min=3)
     *
     * @Assert\NotNull()
     */
    #[ORM\Column(name: 'title', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private ?string $title = null;

    #[ORM\Column(name: 'startDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startDate = null;

    /**
     * @Assert\GreaterThan(propertyPath="startDate", message="La date de fin doit être postérieure à la date de début")
     */
    #[ORM\Column(name: 'endDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endDate = null;

    public const DIPLOMA_OBTAINED = [
        'fr', 'non-fr', 'aucun',
    ];

    #[ORM\Column(name: 'diplomaObtained', type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    private ?string $diplomaObtained = null;

    public const DIPLOMA_RECONNU = [
        'oui', 'non', 'nsp',
    ];

    #[ORM\Column(name: 'diplomaReconnue', type: \Doctrine\DBAL\Types\Types::STRING, length: 50, nullable: true)]
    private ?string $diplomaReconnue = null;

    #[ORM\Column(name: 'organisme', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $organisme = null;

    #[ORM\ManyToOne(targetEntity: CV::class, inversedBy: 'formations')]
    private ?CV $CV = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @return Formation
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return Formation
     */
    public function setStartDate(?\DateTimeInterface $startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTimeInterface
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return Formation
     */
    public function setEndDate(?\DateTimeInterface $endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTimeInterface
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set diplomaObtained.
     *
     * @return Formation
     */
    public function setDiplomaObtained(?string $diplomaObtained = null)
    {
        $this->diplomaObtained = $diplomaObtained;

        return $this;
    }

    /**
     * Get diplomaObtained.
     */
    public function getDiplomaObtained()
    {
        return $this->diplomaObtained;
    }

    /**
     * Set diplomaReconnue.
     */
    public function setDiplomaReconnue(?string $diplomaReconnue = null): self
    {
        $this->diplomaReconnue = $diplomaReconnue;

        return $this;
    }

    /**
     * Get diplomaReconnue.
     */
    public function getDiplomaReconnue(): ?string
    {
        return $this->diplomaReconnue;
    }

    /**
     * Set organisme.
     */
    public function setOrganisme(?string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     */
    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function getCV(): ?CV
    {
        return $this->CV;
    }

    public function setCV(?CV $CV = null): self
    {
        $this->CV = $CV;

        return $this;
    }
}
