<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity\CV;

use Doctrine\ORM\Mapping as ORM;
use Chill\JobBundle\Entity\CV;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Experience.
 */
#[ORM\Table(name: 'chill_job.cv_experience')]
#[ORM\Entity(repositoryClass: \Chill\JobBundle\Repository\CV\ExperienceRepository::class)]
class Experience
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(name: 'poste', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $poste = null;

    #[ORM\Column(name: 'structure', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $structure = null;

    #[ORM\Column(name: 'startDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startDate = null;

    /**
     * @Assert\GreaterThan(propertyPath="startDate", message="La date de fin doit être postérieure à la date de début")
     */
    #[ORM\Column(name: 'endDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endDate = null;

    public const CONTRAT_TYPE = [
        'cddi',
        'cdd-6mois',
        'cdd+6mois',
        'interim',
        'apprentissage',
        'contrat_prof',
        'cui',
        'cae',
        'cdi',
        'stage',
        'volontariat',
        'benevolat',
        'autres',
    ];

    #[ORM\Column(name: 'contratType', type: \Doctrine\DBAL\Types\Types::STRING, length: 100)]
    private ?string $contratType = null;

    #[ORM\Column(name: 'notes', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\ManyToOne(targetEntity: CV::class, inversedBy: 'experiences')]
    private ?CV $CV = null;

    /**
     * Get id.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set poste.
     */
    public function setPoste(?string $poste = null): self
    {
        $this->poste = $poste;

        return $this;
    }

    /**
     * Get poste.
     */
    public function getPoste(): ?string
    {
        return $this->poste;
    }

    /**
     * Set structure.
     */
    public function setStructure(?string $structure = null): self
    {
        $this->structure = $structure;

        return $this;
    }

    /**
     * Get structure.
     */
    public function getStructure(): ?string
    {
        return $this->structure;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     */
    public function setStartDate(?\DateTimeInterface $startDate = null): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTimeInterface
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     */
    public function setEndDate(?\DateTimeInterface $endDate = null): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTimeInterface
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set contratType.
     */
    public function setContratType(?string $contratType): self
    {
        $this->contratType = $contratType;

        return $this;
    }

    /**
     * Get contratType.
     */
    public function getContratType(): ?string
    {
        return $this->contratType;
    }

    /**
     * Set notes.
     */
    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function getCV(): CV
    {
        return $this->CV;
    }

    public function setCV(?CV $CV = null): self
    {
        $this->CV = $CV;

        return $this;
    }
}
