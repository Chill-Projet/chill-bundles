<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity\Rome;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Metier.
 */
#[ORM\Table(name: 'chill_job.rome_metier')]
#[ORM\Entity(repositoryClass: \Chill\JobBundle\Repository\Rome\MetierRepository::class)]
class Metier
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(name: 'libelle', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private ?string $libelle = '';

    #[ORM\Column(name: 'code', type: \Doctrine\DBAL\Types\Types::STRING, length: 20, unique: true)]
    private ?string $code = '';

    /**
     * @var \Doctrine\Common\Collections\Collection<int, Appellation>
     */
    #[ORM\OneToMany(targetEntity: Appellation::class, mappedBy: 'metier')]
    private \Doctrine\Common\Collections\Collection $appellations;

    public function __construct()
    {
        $this->appellations = new ArrayCollection();
        //        $this->appellation = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @return Metier
     */
    public function setLibelle(?string $libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set code.
     *
     * @return Metier
     */
    public function setCode(?string $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
