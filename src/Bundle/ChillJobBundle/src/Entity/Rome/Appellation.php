<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity\Rome;

use Doctrine\ORM\Mapping as ORM;

/**
 * Appellation.
 */
#[ORM\Table(name: 'chill_job.rome_appellation')]
#[ORM\Entity(repositoryClass: \Chill\JobBundle\Repository\Rome\AppellationRepository::class)]
class Appellation implements \Stringable
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(name: 'code', type: \Doctrine\DBAL\Types\Types::STRING, length: 40, unique: true)]
    private ?string $code = '';

    #[ORM\Column(name: 'libelle', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private ?string $libelle = '';

    #[ORM\ManyToOne(targetEntity: Metier::class, inversedBy: 'appellations', cascade: ['persist'])]
    private ?Metier $metier = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @return Appellation
     */
    public function setCode(?string $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle.
     *
     * @return Appellation
     */
    public function setLibelle(?string $libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    public function getMetier(): Metier
    {
        return $this->metier;
    }

    public function setMetier(Metier $metier)
    {
        $this->metier = $metier;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->libelle;
    }
}
