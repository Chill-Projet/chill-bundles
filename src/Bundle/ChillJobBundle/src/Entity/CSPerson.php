<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\PersonBundle\Entity\Person;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Chill\PersonBundle\Entity\MaritalStatus;

/**
 * CSPerson.
 */
#[ORM\Table(name: 'chill_job.cs_person')]
#[ORM\Entity(repositoryClass: \Chill\JobBundle\Repository\CSPersonRepository::class)]
class CSPerson
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private ?int $id = null;

    #[ORM\OneToOne(targetEntity: Person::class)]
    private ?Person $person = null;

    public const SITUATIONS_LOGEMENTS = [
        'proprietaire',
        'locataire',
        'heberge_chez_tiers',
        'heberge_chez_parents',
        'hebergement_en_foyer',
        'sans_domicile',
    ];

    #[ORM\Column(name: 'situationLogement', type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $situationLogement = null;

    #[ORM\Column(name: 'situationLogementPrecision', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $situationLogementPrecision = null;

    /**
     * @Assert\GreaterThanOrEqual(0)
     */
    #[ORM\Column(name: 'enfantACharge', type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: true)]
    private ?int $enfantACharge = null;

    public const NIVEAU_MAITRISE_LANGUE = [
        'lu', 'ecrit', 'parle', 'aucun',
    ];

    /**
     * @var string[]|null
     */
    #[ORM\Column(name: 'niveauMaitriseLangue', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $niveauMaitriseLangue;

    #[ORM\Column(name: 'vehiculePersonnel', type: \Doctrine\DBAL\Types\Types::BOOLEAN, nullable: true)]
    private ?bool $vehiculePersonnel = null;

    public const PERMIS_CONDUIRE = [
        'a', 'b', 'c', 'd', 'e', 'caces', 'en_cours', 'pas_de_permis',
    ];

    /**
     * @var string[]|null
     */
    #[ORM\Column(name: 'permisConduire', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $permisConduire;

    public const SITUATION_PROFESSIONNELLE = [
        'sans_emploi', 'en_activite', 'etudiant', 'etudiant_descolarise',
    ];

    #[ORM\Column(name: 'situationProfessionnelle', type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $situationProfessionnelle = null;

    #[ORM\Column(name: 'dateFinDernierEmploi', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFinDernierEmploi = null;

    public const TYPE_CONTRAT = [
        'cdd',
        'cdi',
        'contrat_interim',
        'contrat_aide',
        'cdd_insertion',
        'contrat_extra',
        'service_civique',
    ];

    /**
     * @var string[]|null
     */
    #[ORM\Column(name: 'typeContrat', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $typeContrat;

    #[ORM\Column(name: 'typeContratAide', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $typeContratAide = null;

    public const RESSOURCES = [
        'salaires',
        'ARE',
        'ASS',
        'RSA',
        'AAH',
        //        'autre',
    ];

    /**
     * @var string[]|null
     */
    #[ORM\Column(name: 'ressources', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $ressources;

    #[ORM\Column(name: 'ressourcesComment', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $ressourcesComment = null;

    #[ORM\Column(name: 'ressourceDate1Versement', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $ressourceDate1Versement = null;

    #[ORM\Column(name: 'CPFMontant', type: \Doctrine\DBAL\Types\Types::DECIMAL, nullable: true, precision: 10, scale: 2)] // Assert\GreaterOrEqualThan(0)
    private ?float $cPFMontant = null;

    #[ORM\Column(name: 'acomptedif', type: \Doctrine\DBAL\Types\Types::DECIMAL, nullable: true, precision: 10, scale: 2)] // Assert\GreaterOrEqualThan(0)
    private ?float $acompteDIF = null;

    public const ACCOMPAGNEMENTS = [
        'plie',
        'pole_emploi',
        'referent_RSA',
        'mission_locale',
        'rqth',
        //        'autre',
    ];

    /**
     * @var string[]|null
     */
    #[ORM\Column(name: 'accompagnement', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $accompagnement;

    #[ORM\Column(name: 'accompagnementRQTHDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $accompagnementRQTHDate = null;

    #[ORM\Column(name: 'accompagnementComment', type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $accompagnementComment = null;

    #[ORM\Column(name: 'poleEmploiId', type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $poleEmploiId = null;

    #[ORM\Column(name: 'poleEmploiInscriptionDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $poleEmploiInscriptionDate = null;

    #[ORM\Column(name: 'cafId', type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $cafId = null;

    #[ORM\Column(name: 'cafInscriptionDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $cafInscriptionDate = null;

    #[ORM\Column(name: 'CERInscriptionDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $cERInscriptionDate = null;

    #[ORM\Column(name: 'PPAEInscriptionDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $pPAEInscriptionDate = null;

    #[ORM\Column(name: 'CERSignataire', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $cERSignataire = null;

    #[ORM\Column(name: 'PPAESignataire', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $pPAESignataire = null;

    public const NEET_ELIGIBILITY = [
        'oui',
        'non',
        'en_attente',
    ];

    #[ORM\Column(name: 'NEETEligibilite', type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    private ?bool $nEETEligibilite = null;

    #[ORM\Column(name: 'NEETCommissionDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $nEETCommissionDate = null;

    #[ORM\Column(name: 'FSEMaDemarcheCode', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $fSEMaDemarcheCode = null;

    #[ORM\Column(name: 'datecontratIEJ', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateContratIEJ = null;

    #[ORM\Column(name: 'dateavenantIEJ', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateAvenantIEJ = null;

    #[ORM\Column(name: 'dispositifs_notes', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $dispositifsNotes = null;

    #[ORM\Column(name: 'handicap', type: \Doctrine\DBAL\Types\Types::BOOLEAN, nullable: true)]
    private ?bool $handicapIs = null;

    #[ORM\Column(name: 'handicapnotes', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $handicapNotes = null;

    public const HANDICAP_RECOMMANDATIONS = [
        'travail_esat',
        'milieu_ordinaire',
        'entreprise_adaptee',
    ];

    #[ORM\Column(name: 'handicapRecommandation', type: \Doctrine\DBAL\Types\Types::STRING, length: 50, nullable: true)]
    private ?string $handicapRecommandation = null;

    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    private ?ThirdParty $handicapAccompagnement = null;

    public const MOBILITE_MOYEN_TRANSPORT = [
        'transport_commun',
        'scooter',
        'velo',
        'voiture',
        //        'autre',
    ];

    /**
     * @var string[]
     */
    #[ORM\Column(name: 'mobilitemoyentransport', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $mobiliteMoyenDeTransport;

    #[ORM\Column(name: 'mobilitenotes', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $mobiliteNotes = null;

    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentCV = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAgrementIAE = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentRQTH = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAttestationNEET = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentCI = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentTitreSejour = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAttestationFiscale = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentPermis = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAttestationCAAF = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentContraTravail = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAttestationFormation = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentQuittanceLoyer = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentFactureElectricite = null;

    /**
     * @Assert\Valid()
     */
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    private ?StoredObject $documentAttestationSecuriteSociale = null;

    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    private ?ThirdParty $prescripteur = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set situationLogement.
     *
     * @return CSPerson
     */
    public function setSituationLogement(?string $situationLogement = null)
    {
        $this->situationLogement = $situationLogement;

        return $this;
    }

    /**
     * Get situationLogement.
     *
     * @return string|null
     */
    public function getSituationLogement()
    {
        return $this->situationLogement;
    }

    /**
     * Set enfantACharge.
     *
     * @return CSPerson
     */
    public function setEnfantACharge(?int $enfantACharge = null)
    {
        $this->enfantACharge = $enfantACharge;

        return $this;
    }

    /**
     * Get enfantACharge.
     *
     * @return int|null
     */
    public function getEnfantACharge()
    {
        return $this->enfantACharge;
    }

    /**
     * Set niveauMaitriseLangue.
     *
     * @return CSPerson
     */
    public function setNiveauMaitriseLangue($niveauMaitriseLangue)
    {
        if (null === $niveauMaitriseLangue) {
            $this->niveauMaitriseLangue = null;

            return $this;
        }

        $this->niveauMaitriseLangue = count($niveauMaitriseLangue) > 0 ?
            $niveauMaitriseLangue : null;

        return $this;
    }

    /**
     * Get niveauMaitriseLangue.
     */
    public function getNiveauMaitriseLangue()
    {
        return $this->niveauMaitriseLangue;
    }

    /**
     * Valide niveauMaitriseLangue.
     *
     * @Assert\Callback()
     */
    public function validateNiveauMatriseLangue(ExecutionContextInterface $context)
    {
        if (null === $this->getNiveauMaitriseLangue()) {
            return;
        }

        if (\in_array('aucun', $this->getNiveauMaitriseLangue(), true)) {
            if (count($this->getNiveauMaitriseLangue()) > 1) {
                $context->buildViolation("Si \"Aucun\" est choisi dans la liste, il n'est "
                    ."pas possible de cocher d'autres indications")
                    ->atPath('niveauMaitriseLangue')
                    ->addViolation();
            }
        }
    }

    /**
     * Set vehiculePersonnel.
     *
     * @return CSPerson
     */
    public function setVehiculePersonnel(?bool $vehiculePersonnel)
    {
        $this->vehiculePersonnel = $vehiculePersonnel;

        return $this;
    }

    /**
     * Get vehiculePersonnel.
     *
     * @return bool
     */
    public function getVehiculePersonnel()
    {
        return $this->vehiculePersonnel;
    }

    /**
     * Set permisConduire.
     *
     * @return CSPerson
     */
    public function setPermisConduire($permisConduire)
    {
        $this->permisConduire = $permisConduire;

        return $this;
    }

    /**
     * Get permisConduire.
     */
    public function getPermisConduire()
    {
        return $this->permisConduire;
    }

    /**
     * Set situationProfessionnelle.
     *
     * @return CSPerson
     */
    public function setSituationProfessionnelle(?string $situationProfessionnelle)
    {
        $this->situationProfessionnelle = $situationProfessionnelle;

        return $this;
    }

    /**
     * Get situationProfessionnelle.
     *
     * @return string
     */
    public function getSituationProfessionnelle()
    {
        return $this->situationProfessionnelle;
    }

    /**
     * Set dateFinDernierEmploi.
     *
     * @param \DateTime $dateFinDernierEmploi
     *
     * @return CSPerson
     */
    public function setDateFinDernierEmploi(?\DateTimeInterface $dateFinDernierEmploi)
    {
        $this->dateFinDernierEmploi = $dateFinDernierEmploi;

        return $this;
    }

    /**
     * Get dateFinDernierEmploi.
     *
     * @return \DateTimeInterface
     */
    public function getDateFinDernierEmploi()
    {
        return $this->dateFinDernierEmploi;
    }

    /**
     * Set typeContrat.
     *
     * @return CSPerson
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * Get typeContrat.
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * Set ressources.
     *
     * @return CSPerson
     */
    public function setRessources($ressources)
    {
        $this->ressources = $ressources;

        return $this;
    }

    /**
     * Get ressources.
     */
    public function getRessources()
    {
        return $this->ressources;
    }

    /**
     * Set ressourcesComment.
     *
     * @return CSPerson
     */
    public function setRessourcesComment(?string $ressourcesComment)
    {
        $this->ressourcesComment = $ressourcesComment;

        return $this;
    }

    /**
     * Get ressourcesComment.
     *
     * @return string
     */
    public function getRessourcesComment()
    {
        return $this->ressourcesComment;
    }

    /**
     * Set ressourceDate1Versement.
     *
     * @param \DateTime $ressourceDate1Versement
     *
     * @return CSPerson
     */
    public function setRessourceDate1Versement(?\DateTimeInterface $ressourceDate1Versement)
    {
        $this->ressourceDate1Versement = $ressourceDate1Versement;

        return $this;
    }

    /**
     * Get ressourceDate1Versement.
     *
     * @return \DateTimeInterface
     */
    public function getRessourceDate1Versement()
    {
        return $this->ressourceDate1Versement;
    }

    public function getCPFMontant()
    {
        return $this->cPFMontant;
    }

    public function setCPFMontant(?float $cPFMontant)
    {
        $this->cPFMontant = $cPFMontant;

        return $this;
    }

    /**
     * Set accompagnement.
     *
     * @return CSPerson
     */
    public function setAccompagnement($accompagnement)
    {
        $this->accompagnement = $accompagnement;

        return $this;
    }

    /**
     * Get accompagnement.
     */
    public function getAccompagnement()
    {
        return $this->accompagnement;
    }

    /**
     * Set accompagnementRQTHDate.
     *
     * @param \DateTime $accompagnementRQTHDate
     *
     * @return CSPerson
     */
    public function setAccompagnementRQTHDate(?\DateTimeInterface $accompagnementRQTHDate)
    {
        $this->accompagnementRQTHDate = $accompagnementRQTHDate;

        return $this;
    }

    /**
     * Get accompagnementRQTHDate.
     *
     * @return \DateTimeInterface
     */
    public function getAccompagnementRQTHDate()
    {
        return $this->accompagnementRQTHDate;
    }

    /**
     * Set accompagnementComment.
     *
     * @return CSPerson
     */
    public function setAccompagnementComment(?string $accompagnementComment)
    {
        $this->accompagnementComment = $accompagnementComment;

        return $this;
    }

    /**
     * Get accompagnementComment.
     *
     * @return string
     */
    public function getAccompagnementComment()
    {
        return $this->accompagnementComment;
    }

    /**
     * Set poleEmploiId.
     *
     * @return CSPerson
     */
    public function setPoleEmploiId(?string $poleEmploiId)
    {
        $this->poleEmploiId = $poleEmploiId;

        return $this;
    }

    /**
     * Get poleEmploiId.
     *
     * @return string
     */
    public function getPoleEmploiId()
    {
        return $this->poleEmploiId;
    }

    /**
     * Set poleEmploiInscriptionDate.
     *
     * @param \DateTime $poleEmploiInscriptionDate
     *
     * @return CSPerson
     */
    public function setPoleEmploiInscriptionDate(?\DateTimeInterface $poleEmploiInscriptionDate)
    {
        $this->poleEmploiInscriptionDate = $poleEmploiInscriptionDate;

        return $this;
    }

    /**
     * Get poleEmploiInscriptionDate.
     *
     * @return \DateTimeInterface
     */
    public function getPoleEmploiInscriptionDate()
    {
        return $this->poleEmploiInscriptionDate;
    }

    /**
     * Set cafId.
     *
     * @return CSPerson
     */
    public function setCafId(?string $cafId)
    {
        $this->cafId = $cafId;

        return $this;
    }

    /**
     * Get cafId.
     *
     * @return string
     */
    public function getCafId()
    {
        return $this->cafId;
    }

    /**
     * Set cafInscriptionDate.
     *
     * @param \DateTime $cafInscriptionDate
     *
     * @return CSPerson
     */
    public function setCafInscriptionDate(?\DateTimeInterface $cafInscriptionDate)
    {
        $this->cafInscriptionDate = $cafInscriptionDate;

        return $this;
    }

    /**
     * Get cafInscriptionDate.
     *
     * @return \DateTimeInterface
     */
    public function getCafInscriptionDate()
    {
        return $this->cafInscriptionDate;
    }

    /**
     * Set cERInscriptionDate.
     *
     * @param \DateTime $cERInscriptionDate
     *
     * @return CSPerson
     */
    public function setCERInscriptionDate(?\DateTimeInterface $cERInscriptionDate)
    {
        $this->cERInscriptionDate = $cERInscriptionDate;

        return $this;
    }

    /**
     * Get cERInscriptionDate.
     *
     * @return \DateTimeInterface
     */
    public function getCERInscriptionDate()
    {
        return $this->cERInscriptionDate;
    }

    /**
     * Set pPAEInscriptionDate.
     *
     * @param \DateTime $pPAEInscriptionDate
     *
     * @return CSPerson
     */
    public function setPPAEInscriptionDate(?\DateTimeInterface $pPAEInscriptionDate)
    {
        $this->pPAEInscriptionDate = $pPAEInscriptionDate;

        return $this;
    }

    /**
     * Get pPAEInscriptionDate.
     *
     * @return \DateTimeInterface
     */
    public function getPPAEInscriptionDate()
    {
        return $this->pPAEInscriptionDate;
    }

    public function getCERSignataire(): ?string
    {
        return $this->cERSignataire;
    }

    public function getPPAESignataire(): ?string
    {
        return $this->pPAESignataire;
    }

    public function setCERSignataire(?string $cERSignataire)
    {
        $this->cERSignataire = $cERSignataire;
    }

    public function setPPAESignataire(?string $pPAESignataire)
    {
        $this->pPAESignataire = $pPAESignataire;
    }

    /**
     * Set nEETEligibilite.
     *
     * @return CSPerson
     */
    public function setNEETEligibilite(?bool $nEETEligibilite)
    {
        $this->nEETEligibilite = $nEETEligibilite;

        return $this;
    }

    /**
     * Get nEETEligibilite.
     *
     * @return bool
     */
    public function getNEETEligibilite()
    {
        return $this->nEETEligibilite;
    }

    /**
     * Set nEETCommissionDate.
     *
     * @param \DateTime $nEETCommissionDate
     *
     * @return CSPerson
     */
    public function setNEETCommissionDate(?\DateTimeInterface $nEETCommissionDate)
    {
        $this->nEETCommissionDate = $nEETCommissionDate;

        return $this;
    }

    /**
     * Get nEETCommissionDate.
     *
     * @return \DateTimeInterface
     */
    public function getNEETCommissionDate()
    {
        return $this->nEETCommissionDate;
    }

    /**
     * Set fSEMaDemarcheCode.
     *
     * @return CSPerson
     */
    public function setFSEMaDemarcheCode(?string $fSEMaDemarcheCode)
    {
        $this->fSEMaDemarcheCode = $fSEMaDemarcheCode;

        return $this;
    }

    /**
     * Get fSEMaDemarcheCode.
     *
     * @return string
     */
    public function getFSEMaDemarcheCode()
    {
        return $this->fSEMaDemarcheCode;
    }

    public function getDispositifsNotes()
    {
        return $this->dispositifsNotes;
    }

    public function setDispositifsNotes(?string $dispositifsNotes)
    {
        $this->dispositifsNotes = $dispositifsNotes;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;
        $this->id = $person->getId();
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    // quick and dirty way to handle marital status from a form in this bundle
    // (CSPersonalSituationType)

    public function getPersonMaritalStatus()
    {
        return $this->getPerson()->getMaritalStatus();
    }

    public function setPersonMaritalStatus(MaritalStatus $maritalStatus)
    {
        return $this->getPerson()->setMaritalStatus($maritalStatus);
    }

    public function getDocumentCV(): ?StoredObject
    {
        return $this->documentCV;
    }

    public function getDocumentAgrementIAE(): ?StoredObject
    {
        return $this->documentAgrementIAE;
    }

    public function getDocumentRQTH(): ?StoredObject
    {
        return $this->documentRQTH;
    }

    public function getDocumentAttestationNEET(): ?StoredObject
    {
        return $this->documentAttestationNEET;
    }

    public function getDocumentCI(): ?StoredObject
    {
        return $this->documentCI;
    }

    public function getDocumentTitreSejour(): ?StoredObject
    {
        return $this->documentTitreSejour;
    }

    public function getDocumentAttestationFiscale(): ?StoredObject
    {
        return $this->documentAttestationFiscale;
    }

    public function getDocumentPermis(): ?StoredObject
    {
        return $this->documentPermis;
    }

    public function getDocumentAttestationCAAF(): ?StoredObject
    {
        return $this->documentAttestationCAAF;
    }

    public function getDocumentContraTravail(): ?StoredObject
    {
        return $this->documentContraTravail;
    }

    public function getDocumentAttestationFormation(): ?StoredObject
    {
        return $this->documentAttestationFormation;
    }

    public function getDocumentQuittanceLoyer(): ?StoredObject
    {
        return $this->documentQuittanceLoyer;
    }

    public function getDocumentFactureElectricite(): ?StoredObject
    {
        return $this->documentFactureElectricite;
    }

    public function getPrescripteur(): ?ThirdParty
    {
        return $this->prescripteur;
    }

    public function setDocumentCV(?StoredObject $documentCV = null)
    {
        $this->documentCV = $documentCV;
    }

    public function setDocumentAgrementIAE(?StoredObject $documentAgrementIAE = null)
    {
        $this->documentAgrementIAE = $documentAgrementIAE;
    }

    public function setDocumentRQTH(?StoredObject $documentRQTH = null)
    {
        $this->documentRQTH = $documentRQTH;
    }

    public function setDocumentAttestationNEET(?StoredObject $documentAttestationNEET = null)
    {
        $this->documentAttestationNEET = $documentAttestationNEET;
    }

    public function setDocumentCI(?StoredObject $documentCI = null)
    {
        $this->documentCI = $documentCI;
    }

    public function setDocumentTitreSejour(?StoredObject $documentTitreSejour = null)
    {
        $this->documentTitreSejour = $documentTitreSejour;
    }

    public function setDocumentAttestationFiscale(?StoredObject $documentAttestationFiscale = null)
    {
        $this->documentAttestationFiscale = $documentAttestationFiscale;
    }

    public function setDocumentPermis(?StoredObject $documentPermis = null)
    {
        $this->documentPermis = $documentPermis;
    }

    public function setDocumentAttestationCAAF(?StoredObject $documentAttestationCAAF = null)
    {
        $this->documentAttestationCAAF = $documentAttestationCAAF;
    }

    public function setDocumentContraTravail(?StoredObject $documentContraTravail = null)
    {
        $this->documentContraTravail = $documentContraTravail;
    }

    public function setDocumentAttestationFormation(?StoredObject $documentAttestationFormation = null)
    {
        $this->documentAttestationFormation = $documentAttestationFormation;
    }

    public function setDocumentQuittanceLoyer(?StoredObject $documentQuittanceLoyer = null)
    {
        $this->documentQuittanceLoyer = $documentQuittanceLoyer;
    }

    public function setDocumentFactureElectricite(?StoredObject $documentFactureElectricite = null)
    {
        $this->documentFactureElectricite = $documentFactureElectricite;
    }

    public function setPrescripteur(?ThirdParty $prescripteur = null)
    {
        $this->prescripteur = $prescripteur;
    }

    public function getSituationLogementPrecision()
    {
        return $this->situationLogementPrecision;
    }

    public function getAcompteDIF()
    {
        return $this->acompteDIF;
    }

    public function getHandicapIs()
    {
        return $this->handicapIs;
    }

    public function getHandicapNotes()
    {
        return $this->handicapNotes;
    }

    public function getHandicapRecommandation()
    {
        return $this->handicapRecommandation;
    }

    public function getMobiliteMoyenDeTransport()
    {
        return $this->mobiliteMoyenDeTransport;
    }

    public function getMobiliteNotes()
    {
        return $this->mobiliteNotes;
    }

    public function setMobiliteMoyenDeTransport($mobiliteMoyenDeTransport)
    {
        $this->mobiliteMoyenDeTransport = $mobiliteMoyenDeTransport;

        return $this;
    }

    public function setMobiliteNotes(?string $mobiliteNotes)
    {
        $this->mobiliteNotes = $mobiliteNotes;

        return $this;
    }

    public function getHandicapAccompagnement(): ?ThirdParty
    {
        return $this->handicapAccompagnement;
    }

    public function getDocumentAttestationSecuriteSociale(): ?StoredObject
    {
        return $this->documentAttestationSecuriteSociale;
    }

    public function setSituationLogementPrecision(?string $situationLogementPrecision)
    {
        $this->situationLogementPrecision = $situationLogementPrecision;

        return $this;
    }

    public function setAcompteDIF(?float $acompteDIF)
    {
        $this->acompteDIF = $acompteDIF;

        return $this;
    }

    public function setHandicapIs(?bool $handicapIs)
    {
        $this->handicapIs = $handicapIs;

        return $this;
    }

    public function setHandicapNotes(?string $handicapNotes)
    {
        $this->handicapNotes = $handicapNotes;

        return $this;
    }

    public function setHandicapRecommandation(?string $handicapRecommandation)
    {
        $this->handicapRecommandation = $handicapRecommandation;

        return $this;
    }

    public function setHandicapAccompagnement(?ThirdParty $handicapAccompagnement = null)
    {
        $this->handicapAccompagnement = $handicapAccompagnement;

        return $this;
    }

    public function setDocumentAttestationSecuriteSociale(?StoredObject $documentAttestationSecuriteSociale = null)
    {
        $this->documentAttestationSecuriteSociale = $documentAttestationSecuriteSociale;

        return $this;
    }

    public function getDateContratIEJ(): ?\DateTimeInterface
    {
        return $this->dateContratIEJ;
    }

    public function setDateContratIEJ(?\DateTime $dateContratIEJ = null)
    {
        $this->dateContratIEJ = $dateContratIEJ;

        return $this;
    }

    public function getTypeContratAide()
    {
        return $this->typeContratAide;
    }

    public function setTypeContratAide(?string $typeContratAide)
    {
        $this->typeContratAide = $typeContratAide;

        return $this;
    }

    public function getDateAvenantIEJ(): ?\DateTimeInterface
    {
        return $this->dateAvenantIEJ;
    }

    public function setDateAvenantIEJ(?\DateTime $dateAvenantIEJ = null)
    {
        $this->dateAvenantIEJ = $dateAvenantIEJ;

        return $this;
    }
}
