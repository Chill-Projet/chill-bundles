<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Entity;

use Chill\JobBundle\Repository\ImmersionRepository;
use Doctrine\ORM\Mapping as ORM;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Address;
use libphonenumber\PhoneNumber;
use Symfony\Component\Validator\Constraints as Assert;
use Chill\MainBundle\Validation\Constraint\PhonenumberConstraint;

/**
 * Immersion.
 */
#[ORM\Table(name: 'chill_job.immersion')]
#[ORM\Entity(repositoryClass: ImmersionRepository::class)]
class Immersion implements \Stringable
{
    public const YES_NO_NSP = [
        'oui',
        'non',
        'nsp',
    ];

    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    /**
     * @Assert\NotNull()
     */
    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\ManyToOne(targetEntity: ThirdParty::class)]
    private ?ThirdParty $entreprise = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $referent = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'domaineActivite', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $domaineActivite = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'tuteurName', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $tuteurName = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'tuteurFonction', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $tuteurFonction = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\NotBlank()
     */
    #[ORM\Column(name: 'tuteurPhoneNumber', type: 'phone_number', nullable: true)]
    #[PhonenumberConstraint(type: 'any')]
    private ?PhoneNumber $tuteurPhoneNumber = null;

    #[ORM\Column(name: 'structureAccName', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $structureAccName = null;

    #[ORM\Column(name: 'structureAccPhonenumber', type: 'phone_number', nullable: true)]
    #[PhonenumberConstraint(type: 'any')]
    private ?PhoneNumber $structureAccPhonenumber = null;

    #[ORM\Column(name: 'structureAccEmail', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $structureAccEmail = null;

    #[ORM\ManyToOne(targetEntity: Address::class, cascade: ['persist', 'remove'])]
    private ?Address $structureAccAddress = null;

    #[ORM\Column(name: 'posteDescriptif', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $posteDescriptif = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'posteTitle', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $posteTitle = null;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'posteLieu', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $posteLieu = null;

    /**
     * @Assert\NotNull()
     */
    #[ORM\Column(name: 'debutDate', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $debutDate = null;

    /**
     * @var string|null
     *
     * @Assert\NotNull()
     */
    #[ORM\Column(name: 'duration', type: \Doctrine\DBAL\Types\Types::DATEINTERVAL, nullable: true)]
    private $duration;

    /**
     * @Assert\NotNull()
     *
     * @Assert\Length(min=2)
     */
    #[ORM\Column(name: 'horaire', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $horaire = null;

    public const OBJECTIFS = [
        'decouvrir_metier',
        'confirmer_projet_prof',
        'acquerir_experience',
        'acquerir_competences',
        'initier_demarche_recrutement',
        'autre',
    ];

    /**
     * @var array|null
     */
    #[ORM\Column(name: 'objectifs', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $objectifs;

    #[ORM\Column(name: 'objectifsAutre', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $objectifsAutre = null;

    #[ORM\Column(name: 'is_bilan_fullfilled', type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => false])]
    private ?bool $isBilanFullfilled = false;

    public const SAVOIR_ETRE = [
        'assiduite',
        'ponctualite',
        'respect_consigne_securite',
        'relation_hierarchie',
        'capacite_adaptation',
        'motivation_travail',
    ];

    /**
     * @var array|null
     */
    #[ORM\Column(name: 'savoirEtre', type: \Doctrine\DBAL\Types\Types::JSON, nullable: true)]
    private $savoirEtre;

    #[ORM\Column(name: 'savoirEtreNote', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $savoirEtreNote = null;

    #[ORM\Column(name: 'noteimmersion', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $noteImmersion = null;

    #[ORM\Column(name: 'principalesActivites', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $principalesActivites = null;

    #[ORM\Column(name: 'competencesAcquises', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $competencesAcquises = null;

    #[ORM\Column(name: 'competencesADevelopper', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $competencesADevelopper = null;

    #[ORM\Column(name: 'noteBilan', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $noteBilan = null;

    public const PONCTUALITE_SALARIE = [
        'aucun',
        'un',
        'plusieurs',
    ];

    #[ORM\Column(name: 'ponctualite_salarie', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $ponctualiteSalarie = null;

    #[ORM\Column(name: 'ponctualite_salarie_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $ponctualiteSalarieNote = null;

    public const ASSIDUITE = [
        'aucun',
        'un',
        'plusieurs',
    ];

    #[ORM\Column(name: 'assiduite', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $assiduite = null;

    #[ORM\Column(name: 'assiduite_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $assiduiteNote = null;

    #[ORM\Column(name: 'interet_activite', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $interetActivite = null;

    #[ORM\Column(name: 'interet_activite_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $interetActiviteNote = null;

    public const INTEGRE_REGLE = [
        '1_temps',
        'rapidement',
        'pas_encore',
    ];

    #[ORM\Column(name: 'integre_regle', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $integreRegle = null;

    #[ORM\Column(name: 'integre_regle_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $integreRegleNote = null;

    #[ORM\Column(name: 'esprit_initiative', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $espritInitiative = null;

    #[ORM\Column(name: 'esprit_initiative_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $espritInitiativeNote = null;

    #[ORM\Column(name: 'organisation', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $organisation = null;

    #[ORM\Column(name: 'organisation_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $organisationNote = null;

    #[ORM\Column(name: 'capacite_travail_equipe', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $capaciteTravailEquipe = null;

    #[ORM\Column(name: 'capacite_travail_equipe_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $capaciteTravailEquipeNote = null;

    #[ORM\Column(name: 'style_vestimentaire', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $styleVestimentaire = null;

    #[ORM\Column(name: 'style_vestimentaire_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $styleVestimentaireNote = null;

    #[ORM\Column(name: 'langage_prof', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $langageProf = null;

    #[ORM\Column(name: 'langage_prof_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $langageProfNote = null;

    #[ORM\Column(name: 'applique_consigne', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $appliqueConsigne = null;

    #[ORM\Column(name: 'applique_consigne_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $appliqueConsigneNote = null;

    #[ORM\Column(name: 'respect_hierarchie', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $respectHierarchie = null;

    #[ORM\Column(name: 'respect_hierarchie_note', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $respectHierarchieNote = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domaineActivite.
     *
     * @return Immersion
     */
    public function setDomaineActivite(?string $domaineActivite = null)
    {
        $this->domaineActivite = $domaineActivite;

        return $this;
    }

    /**
     * Get domaineActivite.
     *
     * @return string|null
     */
    public function getDomaineActivite()
    {
        return $this->domaineActivite;
    }

    /**
     * Set tuteurName.
     *
     * @return Immersion
     */
    public function setTuteurName(?string $tuteurName = null)
    {
        $this->tuteurName = $tuteurName;

        return $this;
    }

    /**
     * Get tuteurName.
     *
     * @return string|null
     */
    public function getTuteurName()
    {
        return $this->tuteurName;
    }

    /**
     * Set tuteurFonction.
     *
     * @return Immersion
     */
    public function setTuteurFonction(?string $tuteurFonction = null)
    {
        $this->tuteurFonction = $tuteurFonction;

        return $this;
    }

    /**
     * Get tuteurFonction.
     *
     * @return string|null
     */
    public function getTuteurFonction()
    {
        return $this->tuteurFonction;
    }

    /**
     * Set tuteurPhoneNumber.
     *
     * @return Immersion
     */
    public function setTuteurPhoneNumber(?PhoneNumber $tuteurPhoneNumber = null)
    {
        $this->tuteurPhoneNumber = $tuteurPhoneNumber;

        return $this;
    }

    /**
     * Get tuteurPhoneNumber.
     */
    public function getTuteurPhoneNumber(): ?PhoneNumber
    {
        return $this->tuteurPhoneNumber;
    }

    /**
     * Set structureAccName.
     *
     * @return Immersion
     */
    public function setStructureAccName(?string $structureAccName = null)
    {
        $this->structureAccName = $structureAccName;

        return $this;
    }

    /**
     * Get structureAccName.
     *
     * @return string|null
     */
    public function getStructureAccName()
    {
        return $this->structureAccName;
    }

    /**
     * Set structureAccPhonenumber.
     *
     * @return Immersion
     */
    public function setStructureAccPhonenumber(?PhoneNumber $structureAccPhonenumber)
    {
        $this->structureAccPhonenumber = $structureAccPhonenumber;

        return $this;
    }

    /**
     * Get structureAccPhonenumber.
     */
    public function getStructureAccPhonenumber(): ?PhoneNumber
    {
        return $this->structureAccPhonenumber;
    }

    /**
     * Set posteDescriptif.
     *
     * @return Immersion
     */
    public function setPosteDescriptif(?string $posteDescriptif = null)
    {
        $this->posteDescriptif = $posteDescriptif;

        return $this;
    }

    /**
     * Get posteDescriptif.
     *
     * @return string|null
     */
    public function getPosteDescriptif()
    {
        return $this->posteDescriptif;
    }

    /**
     * Set posteTitle.
     *
     * @return Immersion
     */
    public function setPosteTitle(?string $posteTitle = null)
    {
        $this->posteTitle = $posteTitle;

        return $this;
    }

    /**
     * Get posteTitle.
     *
     * @return string|null
     */
    public function getPosteTitle()
    {
        return $this->posteTitle;
    }

    /**
     * Set posteLieu.
     *
     * @return Immersion
     */
    public function setPosteLieu(?string $posteLieu = null)
    {
        $this->posteLieu = $posteLieu;

        return $this;
    }

    /**
     * Get posteLieu.
     *
     * @return string|null
     */
    public function getPosteLieu()
    {
        return $this->posteLieu;
    }

    /**
     * Set debutDate.
     *
     * @return Immersion
     */
    public function setDebutDate(?\DateTime $debutDate = null)
    {
        $this->debutDate = $debutDate;

        return $this;
    }

    /**
     * Get debutDate.
     *
     * @return \DateTime
     */
    public function getDebutDate()
    {
        return $this->debutDate;
    }

    /**
     * Set duration.
     *
     * @param string|null $duration
     *
     * @return Immersion
     */
    public function setDuration($duration = null)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     */
    public function getDuration()
    {
        return $this->duration;
    }

    public function getDateEndComputed()
    {
        $startDate = \DateTimeImmutable::createFromMutable($this->getDebutDate());

        return $startDate->add($this->getDuration());
    }

    /**
     * Set horaire.
     *
     * @return Immersion
     */
    public function setHoraire(?string $horaire = null)
    {
        $this->horaire = $horaire;

        return $this;
    }

    /**
     * Get horaire.
     *
     * @return string|null
     */
    public function getHoraire()
    {
        return $this->horaire;
    }

    /**
     * Set objectifs.
     *
     * @param array|null $objectifs
     *
     * @return Immersion
     */
    public function setObjectifs($objectifs = null)
    {
        $this->objectifs = $objectifs;

        return $this;
    }

    /**
     * Get objectifs.
     *
     * @return array|null
     */
    public function getObjectifs()
    {
        return $this->objectifs;
    }

    public function getObjectifsAutre()
    {
        return $this->objectifsAutre;
    }

    public function setObjectifsAutre(?string $objectifsAutre)
    {
        $this->objectifsAutre = $objectifsAutre;

        return $this;
    }

    /**
     * Set savoirEtre.
     *
     * @param array|null $savoirEtre
     *
     * @return Immersion
     */
    public function setSavoirEtre($savoirEtre = null)
    {
        $this->savoirEtre = $savoirEtre;

        return $this;
    }

    /**
     * Get savoirEtre.
     *
     * @return array|null
     */
    public function getSavoirEtre()
    {
        return $this->savoirEtre;
    }

    /**
     * Set note.
     *
     * @return Immersion
     */
    public function setNoteImmersion(?string $note)
    {
        $this->noteImmersion = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return string
     */
    public function getNoteImmersion()
    {
        return $this->noteImmersion;
    }

    /**
     * Set principalesActivites.
     *
     * @return Immersion
     */
    public function setPrincipalesActivites(?string $principalesActivites = null)
    {
        $this->principalesActivites = $principalesActivites;

        return $this;
    }

    /**
     * Get principalesActivites.
     *
     * @return string|null
     */
    public function getPrincipalesActivites()
    {
        return $this->principalesActivites;
    }

    /**
     * Set competencesAcquises.
     *
     * @return Immersion
     */
    public function setCompetencesAcquises(?string $competencesAcquises = null)
    {
        $this->competencesAcquises = $competencesAcquises;

        return $this;
    }

    /**
     * Get competencesAcquises.
     *
     * @return string|null
     */
    public function getCompetencesAcquises()
    {
        return $this->competencesAcquises;
    }

    /**
     * Set competencesADevelopper.
     *
     * @return Immersion
     */
    public function setCompetencesADevelopper(?string $competencesADevelopper = null)
    {
        $this->competencesADevelopper = $competencesADevelopper;

        return $this;
    }

    /**
     * Get competencesADevelopper.
     *
     * @return string|null
     */
    public function getCompetencesADevelopper()
    {
        return $this->competencesADevelopper;
    }

    /**
     * Set noteBilan.
     *
     * @return Immersion
     */
    public function setNoteBilan(?string $noteBilan = null)
    {
        $this->noteBilan = $noteBilan;

        return $this;
    }

    /**
     * Get noteBilan.
     *
     * @return string|null
     */
    public function getNoteBilan()
    {
        return $this->noteBilan;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getEntreprise(): ?ThirdParty
    {
        return $this->entreprise;
    }

    public function getReferent(): ?User
    {
        return $this->referent;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    public function setEntreprise(ThirdParty $entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function setReferent(User $referent)
    {
        $this->referent = $referent;

        return $this;
    }

    public function getStructureAccEmail()
    {
        return $this->structureAccEmail;
    }

    public function getStructureAccAddress(): ?Address
    {
        return $this->structureAccAddress;
    }

    public function setStructureAccEmail(?string $structureAccEmail)
    {
        $this->structureAccEmail = $structureAccEmail;

        return $this;
    }

    public function setStructureAccAddress(Address $structureAccAddress)
    {
        $this->structureAccAddress = $structureAccAddress;

        return $this;
    }

    public function getIsBilanFullfilled()
    {
        return $this->isBilanFullfilled;
    }

    public function getSavoirEtreNote()
    {
        return $this->savoirEtreNote;
    }

    public function setIsBilanFullfilled(?bool $isBilanFullfilled)
    {
        $this->isBilanFullfilled = $isBilanFullfilled;

        return $this;
    }

    public function setSavoirEtreNote(?string $savoirEtreNote)
    {
        $this->savoirEtreNote = $savoirEtreNote;

        return $this;
    }

    public function getPonctualiteSalarie()
    {
        return $this->ponctualiteSalarie;
    }

    public function getPonctualiteSalarieNote()
    {
        return $this->ponctualiteSalarieNote;
    }

    public function getAssiduite()
    {
        return $this->assiduite;
    }

    public function getAssiduiteNote()
    {
        return $this->assiduiteNote;
    }

    public function getInteretActivite()
    {
        return $this->interetActivite;
    }

    public function getInteretActiviteNote()
    {
        return $this->interetActiviteNote;
    }

    public function getIntegreRegle()
    {
        return $this->integreRegle;
    }

    public function getIntegreRegleNote()
    {
        return $this->integreRegleNote;
    }

    public function getEspritInitiative()
    {
        return $this->espritInitiative;
    }

    public function getEspritInitiativeNote()
    {
        return $this->espritInitiativeNote;
    }

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function getOrganisationNote()
    {
        return $this->organisationNote;
    }

    public function getCapaciteTravailEquipe()
    {
        return $this->capaciteTravailEquipe;
    }

    public function getCapaciteTravailEquipeNote()
    {
        return $this->capaciteTravailEquipeNote;
    }

    public function getStyleVestimentaire()
    {
        return $this->styleVestimentaire;
    }

    public function getStyleVestimentaireNote()
    {
        return $this->styleVestimentaireNote;
    }

    public function getLangageProf()
    {
        return $this->langageProf;
    }

    public function getLangageProfNote()
    {
        return $this->langageProfNote;
    }

    public function getAppliqueConsigne()
    {
        return $this->appliqueConsigne;
    }

    public function getAppliqueConsigneNote()
    {
        return $this->appliqueConsigneNote;
    }

    public function getRespectHierarchie()
    {
        return $this->respectHierarchie;
    }

    public function getRespectHierarchieNote()
    {
        return $this->respectHierarchieNote;
    }

    public function setPonctualiteSalarie(?string $ponctualiteSalarie)
    {
        $this->ponctualiteSalarie = $ponctualiteSalarie;

        return $this;
    }

    public function setPonctualiteSalarieNote(?string $ponctualiteSalarieNote)
    {
        $this->ponctualiteSalarieNote = $ponctualiteSalarieNote;

        return $this;
    }

    public function setAssiduite(?string $assiduite)
    {
        $this->assiduite = $assiduite;

        return $this;
    }

    public function setAssiduiteNote(?string $assiduiteNote)
    {
        $this->assiduiteNote = $assiduiteNote;

        return $this;
    }

    public function setInteretActivite(?string $interetActivite)
    {
        $this->interetActivite = $interetActivite;

        return $this;
    }

    public function setInteretActiviteNote(?string $interetActiviteNote)
    {
        $this->interetActiviteNote = $interetActiviteNote;

        return $this;
    }

    public function setIntegreRegle(?string $integreRegle)
    {
        $this->integreRegle = $integreRegle;

        return $this;
    }

    public function setIntegreRegleNote(?string $integreRegleNote)
    {
        $this->integreRegleNote = $integreRegleNote;

        return $this;
    }

    public function setEspritInitiative(?string $espritInitiative)
    {
        $this->espritInitiative = $espritInitiative;

        return $this;
    }

    public function setEspritInitiativeNote(?string $espritInitiativeNote)
    {
        $this->espritInitiativeNote = $espritInitiativeNote;

        return $this;
    }

    public function setOrganisation(?string $organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function setOrganisationNote(?string $organisationNote)
    {
        $this->organisationNote = $organisationNote;

        return $this;
    }

    public function setCapaciteTravailEquipe(?string $capaciteTravailEquipe)
    {
        $this->capaciteTravailEquipe = $capaciteTravailEquipe;

        return $this;
    }

    public function setCapaciteTravailEquipeNote(?string $capaciteTravailEquipeNote)
    {
        $this->capaciteTravailEquipeNote = $capaciteTravailEquipeNote;

        return $this;
    }

    public function setStyleVestimentaire(?string $styleVestimentaire)
    {
        $this->styleVestimentaire = $styleVestimentaire;

        return $this;
    }

    public function setStyleVestimentaireNote(?string $styleVestimentaireNote)
    {
        $this->styleVestimentaireNote = $styleVestimentaireNote;

        return $this;
    }

    public function setLangageProf(?string $langageProf)
    {
        $this->langageProf = $langageProf;

        return $this;
    }

    public function setLangageProfNote(?string $langageProfNote)
    {
        $this->langageProfNote = $langageProfNote;

        return $this;
    }

    public function setAppliqueConsigne(?string $appliqueConsigne)
    {
        $this->appliqueConsigne = $appliqueConsigne;

        return $this;
    }

    public function setAppliqueConsigneNote(?string $appliqueConsigneNote)
    {
        $this->appliqueConsigneNote = $appliqueConsigneNote;

        return $this;
    }

    public function setRespectHierarchie(?string $respectHierarchie)
    {
        $this->respectHierarchie = $respectHierarchie;

        return $this;
    }

    public function setRespectHierarchieNote(?string $respectHierarchieNote)
    {
        $this->respectHierarchieNote = $respectHierarchieNote;

        return $this;
    }

    public function __toString(): string
    {
        return 'Rapport "immersion" de '.$this->getPerson();
    }
}
