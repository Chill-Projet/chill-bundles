<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form\CV;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\JobBundle\Entity\CV\Formation as F;

class FormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => 'Nom de la formation',
                'label_attr' => [
                    'class' => 'required ',
                ],
            ])
            ->add('organisme', TextType::class, [
                'label' => 'Organisme',
                'required' => false,
            ])
            ->add('startDate', ChillDateType::class, [
                'required' => false,
                'label' => 'Date de début',
            ])
            ->add('endDate', ChillDateType::class, [
                'required' => false,
                'label' => 'Date de fin',
            ])
            ->add('diplomaObtained', ChoiceType::class, [
                'label' => 'Diplôme obtenu ?',
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'choices' => \array_combine(F::DIPLOMA_OBTAINED, F::DIPLOMA_OBTAINED),
                'choice_label' => fn ($k) => 'diploma_obtained.'.$k,
            ])
            ->add('diplomaReconnue', ChoiceType::class, [
                'label' => 'Diplôme reconnu en France ?',
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'choices' => \array_combine(F::DIPLOMA_RECONNU, F::DIPLOMA_RECONNU),
                'choice_label' => fn ($k) => 'diploma_reconnu.'.$k,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => F::class]);
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_cv_formation';
    }
}
