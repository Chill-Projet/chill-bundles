<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form\CV;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Chill\JobBundle\Entity\CV\Experience;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('poste', TextType::class, [
                'required' => true,
                'label' => 'Poste',
                'label_attr' => [
                    'class' => 'required ',
                ],
            ])
            ->add('structure', TextType::class, [
                'required' => false,
                'label' => 'Nom de la structure',
            ])
            ->add('startDate', ChillDateType::class, [
                'required' => false,
                'label' => 'Date de début',
            ])
            ->add('endDate', ChillDateType::class, [
                'required' => false,
                'label' => 'Date de fin',
            ])
            ->add('contratType', ChoiceType::class, [
                'required' => false,
                'expanded' => true,
                'multiple' => false,
                'choices' => \array_combine(Experience::CONTRAT_TYPE, Experience::CONTRAT_TYPE),
                'choice_label' => fn ($k) => 'xp_contrat_type.'.$k,
            ])
            ->add('notes', TextareaType::class, [
                'label' => 'Notes',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Experience::class]);
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_cv_experience';
    }
}
