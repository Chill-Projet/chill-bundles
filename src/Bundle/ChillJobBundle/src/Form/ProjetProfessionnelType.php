<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\JobBundle\Entity\ProjetProfessionnel;
use Chill\JobBundle\Form\Type\PickRomeAppellationType;

class ProjetProfessionnelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('souhait', PickRomeAppellationType::class, [
                'label' => 'Souhait',
                'multiple' => true,
                'required' => false,
            ])
            ->add('domaineActiviteSouhait', TextareaType::class, [
                'label' => "Domaine d'activité souhaité",
                'required' => false,
            ])
            ->add('reportDate', ChillDateType::class, [
                'label' => 'Date',
                'required' => true,
            ])
            ->add('typeContrat', ChoiceType::class, [
                'label' => 'Type de contrat recherché',
                'multiple' => true,
                'expanded' => true,
                'choices' => \array_combine(
                    ProjetProfessionnel::TYPE_CONTRAT,
                    ProjetProfessionnel::TYPE_CONTRAT
                ),
                'choice_label' => fn ($k) => 'projet_prof.type_contrat.'.$k,
            ])
            ->add('typeContratNotes', TextareaType::class, [
                'label' => 'Notes concernant le contrat recherché',
                'required' => false,
            ])
            ->add('volumeHoraire', ChoiceType::class, [
                'label' => 'Volume horaire',
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'choices' => \array_combine(
                    ProjetProfessionnel::VOLUME_HORAIRES,
                    ProjetProfessionnel::VOLUME_HORAIRES
                ),
                'choice_label' => fn ($k) => 'projet_prof.volume_horaire.'.$k,
            ])
            ->add('volumeHoraireNotes', TextareaType::class, [
                'label' => 'Notes concernant le volume horaire',
                'required' => false,
            ])
            ->add('idee', TextareaType::class, [
                'label' => 'Idée',
                'required' => false,
            ])
            ->add('enCoursConstruction', TextareaType::class, [
                'label' => 'En cours de construction',
                'required' => false,
            ])
            ->add('valide', PickRomeAppellationType::class, [
                'label' => 'Validé',
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ])
            ->add('domaineActiviteValide', TextareaType::class, [
                'label' => "Domaine d'activité validé",
                'required' => false,
            ])
            ->add('valideNotes', TextareaType::class, [
                'label' => 'Validé (notes)',
                'required' => false,
            ])
            ->add('projetProfessionnelNote', TextareaType::class, [
                'label' => 'Notes concernant le projet professionnel',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ProjetProfessionnel::class]);
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_projetprofessionnel';
    }
}
