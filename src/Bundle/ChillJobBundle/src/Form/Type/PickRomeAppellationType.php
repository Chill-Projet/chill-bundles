<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form\Type;

use Chill\FranceTravailApiBundle\ApiHelper\PartenaireRomeAppellation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\JobBundle\Entity\Rome\Appellation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\Options;
use Chill\JobBundle\Form\ChoiceLoader\RomeAppellationChoiceLoader;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Allow to grab an appellation.
 */
class PickRomeAppellationType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    // /**
    //  *
    //  * @var \Chill\JobBundle\Form\DataTransformer\RomeAppellationTransformer
    //  */
    // protected $romeAppellationTransformer;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var PartenaireRomeAppellation
     */
    protected $apiPartenaire;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * PickRomeAppellationType constructor.
     */
    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator,
        EntityManagerInterface $em,
        PartenaireRomeAppellation $apiPartenaire,
        ValidatorInterface $validator,
    ) {
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
        $this->em = $em;
        $this->apiPartenaire = $apiPartenaire;
        $this->validator = $validator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // ->addModelTransformer($this->romeAppellationTransformer)
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', Appellation::class)
            ->setDefault('choice_label', fn (Appellation $a) => $a->getLibelle())
            ->setDefault('placeholder', 'Choisir une appellation')
            ->setDefault('attr', ['class' => 'select2 '])
            ->setDefault('choice_loader', fn (Options $o) => new RomeAppellationChoiceLoader(
                $this->em,
                $this->apiPartenaire,
                $this->validator
            ))
        ;
    }

    public function getParent()
    {
        return EntityType::class;
    }

    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
        $view->vars['attr']['data-rome-appellation-picker'] = true;
        $view->vars['attr']['data-select-interactive-loading'] = true;
        $view->vars['attr']['data-search-url'] = $this->urlGenerator
            ->generate('chill_france_travail_api_appellation_search', ['_format' => 'json']);
        $view->vars['attr']['data-placeholder'] = 'Choisir une appellation';
        $view->vars['attr']['data-no-results-label'] = $this->translator->trans('select2.no_results');
        $view->vars['attr']['data-error-load-label'] = $this->translator->trans('select2.error_loading');
        $view->vars['attr']['data-searching-label'] = $this->translator->trans('select2.searching');
    }
}
