<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Chill\ThirdPartyBundle\Form\Type\PickThirdpartyDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\JobBundle\Entity\CSPerson;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Chill\DocStoreBundle\Form\StoredObjectType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Chill\PersonBundle\Form\Type\Select2MaritalStatusType;

class CSPersonPersonalSituationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // quick and dirty way to handle marital status...
            ->add('personMaritalStatus', Select2MaritalStatusType::class, ['required' => false, 'label' => 'État civil'])
            ->add('situationLogement', ChoiceType::class, [
                'choices' => \array_combine(
                    CSPerson::SITUATIONS_LOGEMENTS,
                    CSPerson::SITUATIONS_LOGEMENTS
                ),
                'choice_label' => fn ($k) => 'situation_logement.'.$k,
                'required' => false,
                'label' => 'Situation de logement',
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('situationLogementPrecision', TextareaType::class, [
                'label' => 'Précisions',
                'required' => false,
            ])
            ->add('enfantACharge', IntegerType::class, [
                'label' => 'Enfants à charge',
                'required' => false,
            ])
            ->add('niveauMaitriseLangue', ChoiceType::class, [
                'choices' => \array_combine(
                    CSPerson::NIVEAU_MAITRISE_LANGUE,
                    CSPerson::NIVEAU_MAITRISE_LANGUE
                ),
                'choice_label' => fn ($k) => 'niveau_maitrise_langue.'.$k,
                'multiple' => true,
                'required' => false,
                'expanded' => true,
                'label' => 'Maitrise de la langue française',
            ])
            ->add('vehiculePersonnel', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'required' => false,
                'multiple' => false,
            ])
            ->add('permisConduire', ChoiceType::class, [
                'choices' => [
                    \array_combine(CSPerson::PERMIS_CONDUIRE, CSPerson::PERMIS_CONDUIRE),
                ],
                'label' => 'Permis de conduire',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => fn ($k) => 'permis_conduire.'.$k,
            ])
            ->add('situationProfessionnelle', ChoiceType::class, [
                'choices' => \array_combine(CSPerson::SITUATION_PROFESSIONNELLE, CSPerson::SITUATION_PROFESSIONNELLE),
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'choice_label' => fn ($k) => 'situation_professionnelle.'.$k,
            ])
            ->add('dateFinDernierEmploi', ChillDateType::class, [
                'label' => 'Date de la fin du dernier emploi',
                'required' => false,
            ])
            ->add('typeContrat', ChoiceType::class, [
                'choices' => \array_combine(CSPerson::TYPE_CONTRAT, CSPerson::TYPE_CONTRAT),
                'label' => 'Type de contrat',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => fn ($k) => 'type_contrat.'.$k,
            ])
            ->add('typeContratAide', TextType::class, [
                'label' => 'Type de contrat aidé',
                'required' => false,
            ])
            ->add('ressources', ChoiceType::class, [
                'choices' => \array_combine(CSPerson::RESSOURCES, CSPerson::RESSOURCES),
                'choice_label' => fn ($k) => 'ressource.'.$k,
                'required' => false,
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('ressourcesComment', TextareaType::class, [
                'label' => 'Information autre ressource',
                'required' => false,
            ])
            ->add('ressourceDate1Versement', ChillDateType::class, [
                'label' => "Date du premier versement (si bénéficiaire d'une aide)",
                'required' => false,
            ])
            ->add('cPFMontant', MoneyType::class, [
                'label' => 'Montant CPF',
                'required' => false,
            ])
            ->add('acompteDIF', MoneyType::class, [
                'label' => 'Compte DIF',
                'required' => false,
            ])
            ->add('handicapIs', ChoiceType::class, [
                'label' => 'Handicap',
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('handicapNotes', TextareaType::class, [
                'label' => 'Type de handicap',
                'required' => false,
            ])
            ->add('handicapRecommandation', ChoiceType::class, [
                'label' => 'Recommandation',
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'choices' => \array_combine(CSPerson::HANDICAP_RECOMMANDATIONS, CSPerson::HANDICAP_RECOMMANDATIONS),
                'choice_label' => fn ($k) => 'handicap_recommandation.'.$k,
            ])
            ->add('handicapAccompagnement', PickThirdpartyDynamicType::class, [
                'required' => false,
                'multiple' => false,
            ])
            ->add('mobiliteMoyenDeTransport', ChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Moyens de transports accessibles',
                'choices' => \array_combine(
                    CSPerson::MOBILITE_MOYEN_TRANSPORT,
                    CSPerson::MOBILITE_MOYEN_TRANSPORT
                ),
                'choice_label' => fn ($k) => 'moyen_transport.'.$k,
            ])
            ->add('mobiliteNotes', TextareaType::class, [
                'required' => false,
                'label' => 'Notes concernant la mobilité',
            ])
            ->add('documentCV', StoredObjectType::class, [
                'label' => 'CV',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAgrementIAE', StoredObjectType::class, [
                'label' => 'Document Agrément IAE',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentRQTH', StoredObjectType::class, [
                'label' => 'Document RQTH',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAttestationNEET', StoredObjectType::class, [
                'label' => 'Attestation NEET',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentCI', StoredObjectType::class, [
                'label' => 'Carte d\'identité',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentTitreSejour', StoredObjectType::class, [
                'label' => 'Titre de séjour',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAttestationFiscale', StoredObjectType::class, [
                'label' => 'Attestation fiscale',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentPermis', StoredObjectType::class, [
                'label' => 'Permis',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAttestationCAAF', StoredObjectType::class, [
                'label' => 'Attestation CAF',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentContraTravail', StoredObjectType::class, [
                'label' => 'Contrat de travail',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAttestationFormation', StoredObjectType::class, [
                'label' => 'Attestation formation',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentQuittanceLoyer', StoredObjectType::class, [
                'label' => 'Quittance de loyer',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentFactureElectricite', StoredObjectType::class, [
                'label' => 'Facture d\'électricité',
                'required' => false,
                'error_bubbling' => false,
            ])
            ->add('documentAttestationSecuriteSociale', StoredObjectType::class, [
                'label' => 'Attestation de sécurité sociale',
                'required' => false,
                'error_bubbling' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => CSPerson::class]);

        $resolver->setRequired('center')
            ->setAllowedTypes('center', [\Chill\MainBundle\Entity\Center::class])
        ;
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_csperson';
    }
}
