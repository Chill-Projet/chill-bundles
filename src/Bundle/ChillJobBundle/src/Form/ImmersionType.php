<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Chill\MainBundle\Form\Type\ChillPhoneNumberType;
use Chill\MainBundle\Form\Type\PickAddressType;
use Chill\ThirdPartyBundle\Form\Type\PickThirdpartyDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Chill\MainBundle\Form\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\JobBundle\Entity\Immersion;

class ImmersionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ('immersion' === $options['step']) {
            $builder
                ->add('entreprise', PickThirdpartyDynamicType::class, [
                    'label' => "Identité de l'entreprise",
                    'required' => true,
                    'multiple' => false,
                ])
                ->add('domaineActivite', TextType::class, [
                    'label' => "Domaine d'activité",
                    'required' => true,
                ])
                ->add('tuteurName', TextType::class, [
                    'label' => 'Nom du tuteur',
                    'required' => true,
                ])
                ->add('tuteurFonction', TextType::class, [
                    'label' => 'Fonction du tuteur',
                    'required' => true,
                ])
                ->add('tuteurPhoneNumber', ChillPhoneNumberType::class, [
                    'label' => 'Téléphone du tuteur',
                    'required' => true,
                ])
                ->add('structureAccName', TextType::class, [
                    'label' => 'Nom de la structure',
                    'required' => false,
                ])
                ->add('structureAccPhonenumber', ChillPhoneNumberType::class, [
                    'label' => 'Téléphone de la structure',
                    'required' => false,
                ])
                ->add('structureAccEmail', EmailType::class, [
                    'label' => 'Email de la structure',
                    'required' => false,
                ])
                ->add('structureAccAddress', PickAddressType::class, [
                    'label' => 'Addresse de la structure d\'accompagnement',
                    'required' => false,
                ])
                ->add('posteTitle', TextType::class, [
                    'label' => 'Intitulé du poste',
                    'required' => true,
                ])
                ->add('posteLieu', TextType::class, [
                    'label' => "Lieu d'exercice",
                    'required' => true,
                ])
                ->add('debutDate', ChillDateType::class, [
                    'label' => "Date de début de l'immersion",
                    'required' => true,
                ])
                ->add('duration', DateIntervalType::class, [
                    'unit_choices' => [
                        'Weeks' => 'W',
                        'Months' => 'M',
                        'Days' => 'D',
                    ],
                    'label' => "Durée de l'immersion",
                    'required' => true,
                ])
                ->add('horaire', TextareaType::class, [
                    'label' => 'Horaire du stagiaire',
                    'required' => true,
                ])
                ->add('objectifs', ChoiceType::class, [
                    'label' => 'Objectifs',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => \array_combine(Immersion::OBJECTIFS, Immersion::OBJECTIFS),
                    'choice_label' => fn ($k) => 'immersion_objectif.'.$k,
                ])
                ->add('objectifsAutre', TextareaType::class, [
                    'label' => 'Précision sur les objectifs',
                    'required' => false,
                ])
                ->add('noteImmersion', TextareaType::class, [
                    'label' => 'Note',
                    'required' => false,
                ])
            ;
        } elseif ('bilan' === $options['step']) {
            $builder
                ->add('savoirEtre', ChoiceType::class, [
                    'label' => 'Savoir-être du jeune',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => \array_combine(Immersion::SAVOIR_ETRE, Immersion::SAVOIR_ETRE),
                    'choice_label' => fn ($k) => 'immersion_savoir_etre.'.$k,
                ])
                ->add('savoirEtreNote', TextareaType::class, [
                    'label' => 'Note',
                    'required' => false,
                ])
                ->add('principalesActivites', TextareaType::class, [
                    'label' => 'Principales activités',
                    'required' => false,
                ])
                ->add('competencesAcquises', TextareaType::class, [
                    'label' => 'Compétences acquises',
                    'required' => false,
                ])
                ->add('competencesADevelopper', TextareaType::class, [
                    'label' => 'Compétences à développer',
                    'required' => false,
                ])
                ->add('noteBilan', TextareaType::class, [
                    'label' => 'Notes sur le bilan',
                    'required' => false,
                ])

            ;

            foreach ([
                ['ponctualiteSalarie', Immersion::PONCTUALITE_SALARIE, 'Ponctualité du salarié'],
                ['assiduite', Immersion::ASSIDUITE, 'Assiduité'],
                ['interetActivite', Immersion::YES_NO_NSP, 'La personne s’intéresse à l’ensemble des activités et membres de l’entreprise'],
                ['integreRegle', Immersion::INTEGRE_REGLE, 'La personne a intégré les règles (les principes) de l’entreprise'],
                ['espritInitiative', Immersion::YES_NO_NSP, 'La personne fait preuve d’esprit d’initiative'],
                ['organisation', Immersion::YES_NO_NSP, 'La personne a fait preuve d’organisation'],
                ['capaciteTravailEquipe', Immersion::YES_NO_NSP, 'Sa capacité à travailler en équipe'],
                ['styleVestimentaire', Immersion::YES_NO_NSP, 'Style vestimentaire adapté'],
                ['langageProf', Immersion::YES_NO_NSP, 'Langage professionnel'],
                ['appliqueConsigne', Immersion::YES_NO_NSP, 'Applique les consignes'],
                ['respectHierarchie', Immersion::YES_NO_NSP, 'Respecte les niveaux hiérarchiques'],
            ] as [$name, $choices, $label]) {
                $builder
                    ->add($name, ChoiceType::class, [
                        'label' => $label,
                        'multiple' => false,
                        'required' => false,
                        'expanded' => true,
                        'choices' => $choices,
                        'choice_label' => function ($el) use ($choices, $name) {
                            if (Immersion::YES_NO_NSP === $choices) {
                                return 'immersion_nsp.'.$el;
                            }

                            return 'immersion_'.$name.'.'.$el;
                        },
                    ])
                    ->add($name.'Note', TextareaType::class, [
                        'label' => 'Notes',
                        'required' => false,
                    ]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Immersion::class, 'step' => 'immersion']);

        $resolver
            ->setAllowedValues('step', ['immersion', 'bilan'])
            ->setRequired('center')
            ->setAllowedTypes('center', \Chill\MainBundle\Entity\Center::class)
        ;
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_immersion';
    }
}
