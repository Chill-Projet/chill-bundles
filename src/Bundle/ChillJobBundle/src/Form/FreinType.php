<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\JobBundle\Entity\Frein;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FreinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reportDate', ChillDateType::class, [
                'label' => 'Date du rapport',
            ])
            ->add('freinsPerso', ChoiceType::class, [
                'label' => 'Freins identifiés liés à la situation personnelle',
                'choices' => \array_combine(Frein::FREINS_PERSO, Frein::FREINS_PERSO),
                'choice_label' => fn ($k) => 'freins_perso.'.$k,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('freinsEmploi', ChoiceType::class, [
                'label' => 'Freins identifiés liés à la situation professionnelle',
                'choices' => \array_combine(Frein::FREINS_EMPLOI, Frein::FREINS_EMPLOI),
                'choice_label' => fn ($k) => 'freins_emploi.'.$k,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('notesPerso', TextareaType::class, [
                'label' => 'Notes concernant la situation personnelle',
                'required' => false,
            ])
            ->add('notesEmploi', TextareaType::class, [
                'label' => 'Notes concernant l\'accès à l\'emploi',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Frein::class]);
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_frein';
    }
}
