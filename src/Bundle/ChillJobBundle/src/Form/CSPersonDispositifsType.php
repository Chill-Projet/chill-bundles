<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Chill\ThirdPartyBundle\Form\Type\PickThirdpartyDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\JobBundle\Entity\CSPerson;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\MainBundle\Form\Type\ChillDateType;

class CSPersonDispositifsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accompagnement', ChoiceType::class, [
                'choices' => \array_combine(CSPerson::ACCOMPAGNEMENTS, CSPerson::ACCOMPAGNEMENTS),
                'required' => false,
                'label' => 'Accompagnement',
                'multiple' => true,
                'expanded' => true,
                'choice_label' => fn ($k) => 'accompagnement.'.$k,
            ])
            ->add('accompagnementRQTHDate', ChillDateType::class, [
                'label' => "Date d'accompagnement RQTH",
                'required' => false,
            ])
            ->add('accompagnementComment', TextareaType::class, [
                'label' => 'Accompagnement autre: précisions',
                'required' => false,
            ])
            ->add('poleEmploiId', TextType::class, [
                'label' => 'Identifiant pôle emploi',
                'required' => false,
            ])
            ->add('poleEmploiInscriptionDate', ChillDateType::class, [
                'label' => "Date d'inscription Pôle emploi",
                'required' => false,
            ])
            ->add('cafId', TextType::class, [
                'label' => 'Numéro allocataire CAF',
                'required' => false,
            ])
            ->add('cafInscriptionDate', ChillDateType::class, [
                'label' => "Date d'inscription à la CAF",
                'required' => false,
            ])
            ->add('cERInscriptionDate', ChillDateType::class, [
                'label' => 'Date CER',
                'required' => false,
            ])
            ->add('pPAEInscriptionDate', ChillDateType::class, [
                'label' => 'Date PPAE',
                'required' => false,
            ])
            ->add('nEETEligibilite', ChoiceType::class, [
                'label' => 'Éligibilité NEET',
                'choices' => \array_combine(CSPerson::NEET_ELIGIBILITY, CSPerson::NEET_ELIGIBILITY),
                'choice_label' => fn ($k) => 'neet_eligibility.'.$k,
                'multiple' => false,
                'expanded' => true,
                'required' => false,
            ])
            ->add('cERSignataire', TextType::class, [
                'label' => 'Signataire CER',
                'required' => false,
            ])
            ->add('pPAESignataire', TextType::class, [
                'label' => 'Signataire PPAE',
                'required' => false,
            ])
            ->add('nEETCommissionDate', ChillDateType::class, [
                'label' => 'Date commission NEET',
                'required' => false,
            ])
            ->add('fSEMaDemarcheCode', TextType::class, [
                'label' => 'Code "Ma démarche FSE"',
                'required' => false,
            ])
            ->add('prescripteur', PickThirdpartyDynamicType::class, [
                'required' => false,
                'label' => 'Prescripteur',
            ])
            ->add('dispositifsNotes', TextareaType::class, [
                'required' => false,
                'label' => 'Notes',
            ])
            ->add('dateContratIEJ', ChillDateType::class, [
                'required' => false,
                'label' => ' Date du contrat d’engagement IEJ',
            ])
            ->add('dateAvenantIEJ', ChillDateType::class, [
                'required' => false,
                'label' => " Date de l'avenant IEJ",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => CSPerson::class]);

        $resolver
            ->setDefined('center')
            ->setAllowedTypes('center', [\Chill\MainBundle\Entity\Center::class])
        ;
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_csperson';
    }
}
