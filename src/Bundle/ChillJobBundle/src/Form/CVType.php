<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Chill\JobBundle\Entity\CV;
use Chill\MainBundle\Form\Type\ChillCollectionType;
use Chill\JobBundle\Form\CV\FormationType;
use Chill\JobBundle\Form\CV\ExperienceType;
use Chill\MainBundle\Form\Type\Select2LanguageType;

class CVType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reportDate', ChillDateType::class, [
                'label' => 'Date de rédaction du CV',
            ])
            ->add('formationLevel', ChoiceType::class, [
                'label' => 'Niveau de formation',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'choices' => \array_combine(CV::FORMATION_LEVEL, CV::FORMATION_LEVEL),
                'choice_label' => fn ($k) => 'formation_level.'.$k,
            ])
            ->add('formationType', ChoiceType::class, [
                'label' => 'Type de formation',
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'choices' => \array_combine(CV::FORMATION_TYPE, CV::FORMATION_TYPE),
                'choice_label' => fn ($k) => 'formation_type.'.$k,
            ])
            ->add('spokenLanguages', Select2LanguageType::class, [
                'required' => false,
                'multiple' => true,
            ])
            ->add('notes', TextareaType::class, [
                'label' => 'Note',
                'required' => false,
            ])
            ->add('formations', ChillCollectionType::class, [
                'label' => 'Formations',
                'entry_type' => FormationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'button_add_label' => 'Ajouter une formation',
                'button_remove_label' => 'Retirer cette formation',
                'required' => false,
                'by_reference' => false,
                'block_name' => 'formation_list',
            ])
            ->add('experiences', ChillCollectionType::class, [
                'label' => 'Expériences',
                'entry_type' => ExperienceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'button_add_label' => 'Ajouter une expérience',
                'button_remove_label' => 'Retirer cette expérience',
                'required' => false,
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => CV::class]);
    }

    public function getBlockPrefix()
    {
        return 'job_bundle_cv';
    }
}
