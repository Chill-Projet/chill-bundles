<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240424140641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'JobBundle: add missing columns to frein, immersion and projet_professionnel tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_job.frein ADD person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_job.frein ADD CONSTRAINT FK_172EAC0A217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_172EAC0A217BBB47 ON chill_job.frein (person_id)');
        $this->addSql('ALTER TABLE chill_job.immersion ADD person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_job.immersion ADD CONSTRAINT FK_FBB3CBB4217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4217BBB47 ON chill_job.immersion (person_id)');
        $this->addSql('ALTER TABLE chill_job.projet_professionnel ADD person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_job.projet_professionnel ADD CONSTRAINT FK_12E4FFBF217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_12E4FFBF217BBB47 ON chill_job.projet_professionnel (person_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_job.projet_professionnel DROP CONSTRAINT FK_12E4FFBF217BBB47');
        $this->addSql('ALTER TABLE chill_job.projet_professionnel DROP person_id');
        $this->addSql('ALTER TABLE chill_job.immersion DROP CONSTRAINT FK_FBB3CBB4217BBB47');
        $this->addSql('ALTER TABLE chill_job.immersion DROP person_id');
        $this->addSql('ALTER TABLE chill_job.frein DROP CONSTRAINT FK_172EAC0A217BBB47');
        $this->addSql('ALTER TABLE chill_job.frein DROP person_id');
    }
}
