<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240429111628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Emploi - Change field type to phonenumber for tuteur and structure acc';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_job.immersion ALTER tuteurphonenumber TYPE VARCHAR(35)');
        $this->addSql('COMMENT ON COLUMN chill_job.immersion.tuteurPhoneNumber IS \'(DC2Type:phone_number)\'');

        $this->addSql('ALTER TABLE chill_job.immersion ALTER structureAccPhonenumber TYPE VARCHAR(35)');
        $this->addSql('COMMENT ON COLUMN chill_job.immersion.structureAccPhonenumber IS \'(DC2Type:phone_number)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_job.immersion ALTER tuteurPhoneNumber TYPE TEXT');
        $this->addSql('COMMENT ON COLUMN chill_job.immersion.tuteurphonenumber IS NULL');

        $this->addSql('ALTER TABLE chill_job.immersion ALTER structureAccPhonenumber TYPE TEXT');
        $this->addSql('COMMENT ON COLUMN chill_job.immersion.structureAccPhonenumber IS NULL');
    }
}
