<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add schema chill_job & table cs_person inside.
 */
final class Version20191119172511 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA chill_csconnectes');
        $this->addSql('CREATE TABLE chill_csconnectes.cs_person (
            id INT NOT NULL,
            person_id INT NOT NULL,
            prescripteur_id INT DEFAULT NULL,
            situationLogement VARCHAR(255) DEFAULT NULL,
            enfantACharge INT DEFAULT NULL,
            niveauMaitriseLangue JSONB DEFAULT NULL,
            vehiculePersonnel BOOLEAN DEFAULT NULL,
            permisConduire JSONB DEFAULT NULL,
            situationProfessionnelle VARCHAR(255) DEFAULT NULL,
            dateFinDernierEmploi DATE DEFAULT NULL,
            typeContrat JSONB DEFAULT NULL,
            ressources JSONB DEFAULT NULL,
            ressourcesComment TEXT DEFAULT NULL,
            ressourceDate1Versement DATE DEFAULT NULL,
            CPFNombreHeures INT DEFAULT NULL,
            accompagnement JSONB DEFAULT NULL,
            accompagnementRQTHDate DATE DEFAULT NULL,
            accompagnementComment VARCHAR(255) DEFAULT NULL,
            poleEmploiId VARCHAR(255) DEFAULT NULL,
            poleEmploiInscriptionDate DATE DEFAULT NULL,
            cafId VARCHAR(255) DEFAULT NULL,
            cafInscriptionDate DATE DEFAULT NULL,
            CERInscriptionDate DATE DEFAULT NULL,
            PPAEInscriptionDate DATE DEFAULT NULL,
            NEETEligibilite BOOLEAN DEFAULT NULL,
            NEETCommissionDate DATE DEFAULT NULL,
            FSEMaDemarcheCode TEXT DEFAULT NULL,
            documentCV_id INT DEFAULT NULL,
            documentAgrementIAE_id INT DEFAULT NULL,
            documentRQTH_id INT DEFAULT NULL,
            documentAttestationNEET_id INT DEFAULT NULL,
            documentCI_id INT DEFAULT NULL,
            documentTitreSejour_id INT DEFAULT NULL,
            documentAttestationFiscale_id INT DEFAULT NULL,
            documentPermis_id INT DEFAULT NULL,
            documentAttestationCAAF_id INT DEFAULT NULL,
            documentContraTravail_id INT DEFAULT NULL,
            documentAttestationFormation_id INT DEFAULT NULL,
            documentQuittanceLoyer_id INT DEFAULT NULL,
            documentFactureElectricite_id INT DEFAULT NULL,
            PRIMARY KEY(id, person_id))');
        $this->addSql('CREATE INDEX IDX_10864F31217BBB47 ON chill_csconnectes.cs_person (person_id)');
        $this->addSql('CREATE INDEX IDX_10864F3154866550 ON chill_csconnectes.cs_person (documentCV_id)');
        $this->addSql('CREATE INDEX IDX_10864F318825E118 ON chill_csconnectes.cs_person (documentAgrementIAE_id)');
        $this->addSql('CREATE INDEX IDX_10864F31A396AFAC ON chill_csconnectes.cs_person (documentRQTH_id)');
        $this->addSql('CREATE INDEX IDX_10864F3187541764 ON chill_csconnectes.cs_person (documentAttestationNEET_id)');
        $this->addSql('CREATE INDEX IDX_10864F315CFC2299 ON chill_csconnectes.cs_person (documentCI_id)');
        $this->addSql('CREATE INDEX IDX_10864F3134FDF11D ON chill_csconnectes.cs_person (documentTitreSejour_id)');
        $this->addSql('CREATE INDEX IDX_10864F315742C99D ON chill_csconnectes.cs_person (documentAttestationFiscale_id)');
        $this->addSql('CREATE INDEX IDX_10864F31166494D4 ON chill_csconnectes.cs_person (documentPermis_id)');
        $this->addSql('CREATE INDEX IDX_10864F3172820D66 ON chill_csconnectes.cs_person (documentAttestationCAAF_id)');
        $this->addSql('CREATE INDEX IDX_10864F31AFA5E636 ON chill_csconnectes.cs_person (documentContraTravail_id)');
        $this->addSql('CREATE INDEX IDX_10864F3161E05C22 ON chill_csconnectes.cs_person (documentAttestationFormation_id)');
        $this->addSql('CREATE INDEX IDX_10864F316F744BB0 ON chill_csconnectes.cs_person (documentQuittanceLoyer_id)');
        $this->addSql('CREATE INDEX IDX_10864F31AC39B1B ON chill_csconnectes.cs_person (documentFactureElectricite_id)');
        $this->addSql('CREATE INDEX IDX_10864F31D486E642 ON chill_csconnectes.cs_person (prescripteur_id)');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F3154866550 FOREIGN KEY (documentCV_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F318825E118 FOREIGN KEY (documentAgrementIAE_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31A396AFAC FOREIGN KEY (documentRQTH_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F3187541764 FOREIGN KEY (documentAttestationNEET_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F315CFC2299 FOREIGN KEY (documentCI_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F3134FDF11D FOREIGN KEY (documentTitreSejour_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F315742C99D FOREIGN KEY (documentAttestationFiscale_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31166494D4 FOREIGN KEY (documentPermis_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F3172820D66 FOREIGN KEY (documentAttestationCAAF_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31AFA5E636 FOREIGN KEY (documentContraTravail_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F3161E05C22 FOREIGN KEY (documentAttestationFormation_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F316F744BB0 FOREIGN KEY (documentQuittanceLoyer_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31AC39B1B FOREIGN KEY (documentFactureElectricite_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CONSTRAINT FK_10864F31D486E642 FOREIGN KEY (prescripteur_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.niveauMaitriseLangue IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.permisConduire IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.typeContrat IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.ressources IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.accompagnement IS NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE chill_csconnectes.cs_person');
        $this->addSql('DROP SCHEMA chill_csconnectes CASCADE');
    }
}
