<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add index for rome code appellation / metier.
 */
final class Version20200207224152 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE INDEX code_appellation_idx ON chill_csconnectes.rome_appellation (code)');
        $this->addSql('CREATE INDEX code_metier_idx ON chill_csconnectes.rome_metier (code)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX code_appellation_idx ON chill_csconnectes.rome_appellation');
        $this->addSql('DROP INDEX code_metier_idx ON chill_csconnectes.rome_metier');
    }
}
