<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajout:
 *   - structure addresse
 *   - structure email
 *
 * à l'entité Immersion
 */
final class Version20200113142525 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD structureAccEmail TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD structureAccAddress_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4B5E04267 ON chill_csconnectes.immersion (structureAccAddress_id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP structureAccEmail');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP structureAccAddress_id');
    }
}
