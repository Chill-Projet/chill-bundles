<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200313124323 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('ALTER TABLE chill_csconnectes.cv_formation ALTER diplomaobtained TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD ponctualite_salarie TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD ponctualite_salarie_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD assiduite TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD assiduite_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD interet_activite TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD interet_activite_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD integre_regle TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD integre_regle_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD esprit_initiative TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD esprit_initiative_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD organisation TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD organisation_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD capacite_travail_equipe TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD capacite_travail_equipe_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD style_vestimentaire TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD style_vestimentaire_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD langage_prof TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD langage_prof_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD applique_consigne TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD applique_consigne_note TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD respect_hierarchie TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD respect_hierarchie_note TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.objectifs IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.savoirEtre IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.frein.freinsPerso IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.frein.freinsEmploi IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.projet_professionnel.typeContrat IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.projet_professionnel.volumeHoraire IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_3party.third_party.types IS NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP ponctualite_salarie');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP ponctualite_salarie_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP assiduite');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP assiduite_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP interet_activite');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP interet_activite_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP integre_regle');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP integre_regle_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP esprit_initiative');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP esprit_initiative_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP organisation');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP organisation_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP capacite_travail_equipe');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP capacite_travail_equipe_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP style_vestimentaire');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP style_vestimentaire_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP langage_prof');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP langage_prof_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP applique_consigne');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP applique_consigne_note');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP respect_hierarchie');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP respect_hierarchie_note');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.objectifs IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.savoiretre IS \'(DC2Type:json_array)\'');
    }
}
