<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210105342 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D9E9CABC77153098 ON chill_csconnectes.rome_appellation (code);');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3274952577153098 ON chill_csconnectes.rome_metier (code);');
        $this->addSql('DROP INDEX IF EXISTS chill_csconnectes.code_metier_idx ');
        $this->addSql('DROP INDEX IF EXISTS chill_csconnectes.code_appellation_idx ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_csconnectes.UNIQ_D9E9CABC77153098');
        $this->addSql('DROP INDEX chill_csconnectes.UNIQ_3274952577153098');
    }
}
