<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajoute colonnes "domaineActiviteSouhait" et "DomaineAciviteValide" au projet
 * professionnel.
 */
final class Version20200403114520 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->addSql('ALTER TABLE chill_csconnectes.projet_professionnel ADD domaineActiviteSouhait TEXT DEFAULT NULL;');
        $this->addSql('ALTER TABLE chill_csconnectes.projet_professionnel ADD domaineActiviteValide TEXT DEFAULT NULL;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_csconnectes.projet_professionnel DROP domaineActiviteSouhait');
        $this->addSql('ALTER TABLE chill_csconnectes.projet_professionnel DROP domaineActiviteValide');
    }
}
