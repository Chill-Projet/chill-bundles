<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200205132532 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        //        $this->addSql('DROP SEQUENCE report_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE chill_csconnectes.rome_appellation_id_seq '
            .'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_csconnectes.rome_metier_id_seq '
            .'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_csconnectes.projet_professionnel_id_seq '
            .'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_csconnectes.cv_formation_id_seq '
            .'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_csconnectes.rome_appellation ('
            .'id INT NOT NULL, metier_id INT DEFAULT NULL, code VARCHAR(40) NOT NULL, '
            .'libelle TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D9E9CABCED16FA20 ON chill_csconnectes.rome_appellation (metier_id)');
        $this->addSql('CREATE TABLE chill_csconnectes.rome_metier (id INT NOT NULL, '
            .'libelle TEXT NOT NULL, code VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_csconnectes.projet_professionnel (id INT NOT NULL, '
            .'person_id INT DEFAULT NULL, reportDate DATE NOT NULL, '
            .'typeContrat JSONB DEFAULT NULL, typeContratNotes TEXT DEFAULT NULL, '
            .'volumeHoraire JSONB DEFAULT NULL, volumeHoraireNotes TEXT DEFAULT NULL, '
            .'idee TEXT DEFAULT NULL, enCoursConstruction TEXT DEFAULT NULL, '
            .'valideNotes TEXT DEFAULT NULL, projetProfessionnelNote TEXT DEFAULT NULL, '
            .'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_12E4FFBF217BBB47 ON chill_csconnectes.projet_professionnel (person_id)');
        $this->addSql('CREATE TABLE chill_csconnectes.projetprofessionnel_souhait (projetprofessionnel_id INT NOT NULL, appellation_id INT NOT NULL, PRIMARY KEY(projetprofessionnel_id, appellation_id))');
        $this->addSql('CREATE INDEX IDX_3280B96DB87BF7B5 ON chill_csconnectes.projetprofessionnel_souhait (projetprofessionnel_id)');
        $this->addSql('CREATE INDEX IDX_3280B96D7CDE30DD ON chill_csconnectes.projetprofessionnel_souhait (appellation_id)');
        $this->addSql('CREATE TABLE chill_csconnectes.projetprofessionnel_valide (projetprofessionnel_id INT NOT NULL, appellation_id INT NOT NULL, PRIMARY KEY(projetprofessionnel_id, appellation_id))');
        $this->addSql('CREATE INDEX IDX_E0501BE0B87BF7B5 ON chill_csconnectes.projetprofessionnel_valide (projetprofessionnel_id)');
        $this->addSql('CREATE INDEX IDX_E0501BE07CDE30DD ON chill_csconnectes.projetprofessionnel_valide (appellation_id)');
        $this->addSql('ALTER TABLE chill_csconnectes.rome_appellation ADD CONSTRAINT FK_D9E9CABCED16FA20 FOREIGN KEY (metier_id) REFERENCES chill_csconnectes.rome_metier (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.projet_professionnel ADD CONSTRAINT FK_12E4FFBF217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_souhait ADD CONSTRAINT FK_3280B96DB87BF7B5 FOREIGN KEY (projetprofessionnel_id) REFERENCES chill_csconnectes.projet_professionnel (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_souhait ADD CONSTRAINT FK_3280B96D7CDE30DD FOREIGN KEY (appellation_id) REFERENCES chill_csconnectes.rome_appellation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_valide ADD CONSTRAINT FK_E0501BE0B87BF7B5 FOREIGN KEY (projetprofessionnel_id) REFERENCES chill_csconnectes.projet_professionnel (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_valide ADD CONSTRAINT FK_E0501BE07CDE30DD FOREIGN KEY (appellation_id) REFERENCES chill_csconnectes.rome_appellation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_souhait DROP CONSTRAINT FK_3280B96D7CDE30DD');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_valide DROP CONSTRAINT FK_E0501BE07CDE30DD');
        $this->addSql('ALTER TABLE chill_csconnectes.rome_appellation DROP CONSTRAINT FK_D9E9CABCED16FA20');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_souhait DROP CONSTRAINT FK_3280B96DB87BF7B5');
        $this->addSql('ALTER TABLE chill_csconnectes.projetprofessionnel_valide DROP CONSTRAINT FK_E0501BE0B87BF7B5');
        $this->addSql('DROP SEQUENCE chill_csconnectes.rome_appellation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_csconnectes.rome_metier_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_csconnectes.projet_professionnel_id_seq CASCADE');

        $this->addSql('DROP TABLE chill_csconnectes.rome_appellation');
        $this->addSql('DROP TABLE chill_csconnectes.rome_metier');
        $this->addSql('DROP TABLE chill_csconnectes.projet_professionnel');
        $this->addSql('DROP TABLE chill_csconnectes.projetprofessionnel_souhait');
        $this->addSql('DROP TABLE chill_csconnectes.projetprofessionnel_valide');
    }
}
