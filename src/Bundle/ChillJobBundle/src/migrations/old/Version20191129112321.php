<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Fix missing fields in previous migration.
 */
final class Version20191129112321 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX chill_csconnectes.IDX_10864f31217bbb47');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD CERSignataire TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD PPAESignataire TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.niveauMaitriseLangue IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.permisConduire IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.typeContrat IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.ressources IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.accompagnement IS NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_10864F31217BBB47 ON chill_csconnectes.cs_person (person_id)');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person DROP CONSTRAINT cs_person_pkey');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ALTER person_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('this migration is not reversible ('
            .'actions on primary keys)');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX UNIQ_10864F31217BBB47');
        $this->addSql('DROP INDEX cs_person_pkey');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person DROP CERSignataire');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person DROP PPAESignataire');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ALTER person_id SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.niveaumaitriselangue IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.permisconduire IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.typecontrat IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.ressources IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.cs_person.accompagnement IS \'(DC2Type:json_array)\'');
        $this->addSql('CREATE INDEX idx_10864f31217bbb47 ON chill_csconnectes.cs_person (person_id)');
        $this->addSql('ALTER TABLE chill_csconnectes.cs_person ADD PRIMARY KEY (id, person_id)');
    }
}
