<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajout Immersion.
 */
final class Version20200113104411 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_csconnectes.immersion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_csconnectes.immersion (id INT NOT NULL, person_id INT DEFAULT NULL, entreprise_id INT DEFAULT NULL, referent_id INT DEFAULT NULL, domaineActivite TEXT DEFAULT NULL, tuteurName TEXT DEFAULT NULL, tuteurFonction TEXT DEFAULT NULL, tuteurPhoneNumber TEXT DEFAULT NULL, structureAccName TEXT DEFAULT NULL, structureAccPhonenumber TEXT DEFAULT NULL, posteDescriptif TEXT DEFAULT NULL, posteTitle TEXT DEFAULT NULL, posteLieu TEXT DEFAULT NULL, debutDate DATE DEFAULT NULL, duration INTERVAL DEFAULT NULL, horaire TEXT DEFAULT NULL, objectifs JSONB DEFAULT NULL, savoirEtre JSONB DEFAULT NULL, noteimmersion TEXT NOT NULL, principalesActivites TEXT DEFAULT NULL, competencesAcquises TEXT DEFAULT NULL, competencesADevelopper TEXT DEFAULT NULL, noteBilan TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4217BBB47 ON chill_csconnectes.immersion (person_id)');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4A4AEAFEA ON chill_csconnectes.immersion (entreprise_id)');
        $this->addSql('CREATE INDEX IDX_FBB3CBB435E47E35 ON chill_csconnectes.immersion (referent_id)');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.duration IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD CONSTRAINT FK_FBB3CBB4217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD CONSTRAINT FK_FBB3CBB4A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD CONSTRAINT FK_FBB3CBB435E47E35 FOREIGN KEY (referent_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE chill_csconnectes.cv_formation (id INT NOT NULL, title TEXT NOT NULL, startDate DATE DEFAULT NULL, endDate DATE DEFAULT NULL, diplomaObtained VARCHAR(255) DEFAULT NULL, diplomaReconnue VARCHAR(50) DEFAULT NULL, organisme TEXT DEFAULT NULL, CV_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_20BE09E2AE1799D8 ON chill_csconnectes.cv_formation (CV_id)');
        $this->addSql('ALTER TABLE chill_csconnectes.cv_formation ADD CONSTRAINT FK_20BE09E2AE1799D8 FOREIGN KEY (CV_id) REFERENCES chill_csconnectes.cv (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE chill_csconnectes.immersion_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_csconnectes.immersion');

        $this->addSql('DROP SEQUENCE chill_csconnectes.cv_formation_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_csconnectes.cv_formation DROP CONSTRAINT FK_20BE09E2AE1799D8');
        $this->addSql('DROP TABLE chill_csconnectes.cv_formation');
    }
}
