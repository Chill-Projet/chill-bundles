<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajout colonnes manquantes à Immersion:
 *
 * - is bilan fullfilled ;
 * - savoir être notes
 */
final class Version20200114081435 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->skipIf(true, 'Skipping this old migration. Replaced by migration Version20240424095147');
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD is_bilan_fullfilled BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD savoirEtreNote TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_csconnectes.immersion.savoirEtre IS NULL');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion ADD CONSTRAINT FK_FBB3CBB4B5E04267 FOREIGN KEY (structureAccAddress_id) REFERENCES chill_main_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP CONSTRAINT FK_FBB3CBB4B5E04267');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP is_bilan_fullfilled');
        $this->addSql('ALTER TABLE chill_csconnectes.immersion DROP savoirEtreNote');
    }
}
