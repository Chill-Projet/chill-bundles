<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918132916 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rename indexes to reflect changes in column or table names within job bundle';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER INDEX chill_job.uniq_10864f31217bbb47 RENAME TO UNIQ_38D83C54217BBB47');
        $this->addSql('ALTER INDEX chill_job.idx_10864f312654b57d RENAME TO IDX_38D83C542654B57D');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3154866550 RENAME TO IDX_38D83C5454866550');
        $this->addSql('ALTER INDEX chill_job.idx_10864f318825e118 RENAME TO IDX_38D83C548825E118');
        $this->addSql('ALTER INDEX chill_job.idx_10864f31a396afac RENAME TO IDX_38D83C54A396AFAC');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3187541764 RENAME TO IDX_38D83C5487541764');
        $this->addSql('ALTER INDEX chill_job.idx_10864f315cfc2299 RENAME TO IDX_38D83C545CFC2299');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3134fdf11d RENAME TO IDX_38D83C5434FDF11D');
        $this->addSql('ALTER INDEX chill_job.idx_10864f315742c99d RENAME TO IDX_38D83C545742C99D');
        $this->addSql('ALTER INDEX chill_job.idx_10864f31166494d4 RENAME TO IDX_38D83C54166494D4');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3172820d66 RENAME TO IDX_38D83C5472820D66');
        $this->addSql('ALTER INDEX chill_job.idx_10864f31afa5e636 RENAME TO IDX_38D83C54AFA5E636');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3161e05c22 RENAME TO IDX_38D83C5461E05C22');
        $this->addSql('ALTER INDEX chill_job.idx_10864f316f744bb0 RENAME TO IDX_38D83C546F744BB0');
        $this->addSql('ALTER INDEX chill_job.idx_10864f31ac39b1b RENAME TO IDX_38D83C54AC39B1B');
        $this->addSql('ALTER INDEX chill_job.idx_10864f3172a75b6d RENAME TO IDX_38D83C5472A75B6D');
        $this->addSql('ALTER INDEX chill_job.idx_10864f31d486e642 RENAME TO IDX_38D83C54D486E642');
        $this->addSql('ALTER INDEX chill_job.idx_3f24f812217bbb47 RENAME TO IDX_ED350AE5217BBB47');
        $this->addSql('ALTER INDEX chill_job.idx_102a1262ae1799d8 RENAME TO IDX_5BE81B7AAE1799D8');
        $this->addSql('ALTER INDEX chill_job.idx_20be09e2ae1799d8 RENAME TO IDX_E66356C7AE1799D8');
        $this->addSql('ALTER INDEX chill_job.idx_172eac0a217bbb47 RENAME TO IDX_EB9F6A40217BBB47');
        $this->addSql('ALTER INDEX chill_job.idx_fbb3cbb4217bbb47 RENAME TO IDX_D3EDB8D1217BBB47');
        $this->addSql('ALTER INDEX chill_job.idx_fbb3cbb4a4aeafea RENAME TO IDX_D3EDB8D1A4AEAFEA');
        $this->addSql('ALTER INDEX chill_job.idx_fbb3cbb435e47e35 RENAME TO IDX_D3EDB8D135E47E35');
        $this->addSql('ALTER INDEX chill_job.idx_fbb3cbb4b5e04267 RENAME TO IDX_D3EDB8D1B5E04267');
        $this->addSql('ALTER INDEX chill_job.idx_12e4ffbf217bbb47 RENAME TO IDX_BE1A1859217BBB47');
        $this->addSql('ALTER INDEX chill_job.idx_3280b96db87bf7b5 RENAME TO IDX_A90849FBB87BF7B5');
        $this->addSql('ALTER INDEX chill_job.idx_3280b96d7cde30dd RENAME TO IDX_A90849FB7CDE30DD');
        $this->addSql('ALTER INDEX chill_job.idx_e0501be0b87bf7b5 RENAME TO IDX_C623D20B87BF7B5');
        $this->addSql('ALTER INDEX chill_job.idx_e0501be07cde30dd RENAME TO IDX_C623D207CDE30DD');
        $this->addSql('ALTER INDEX chill_job.uniq_d9e9cabc77153098 RENAME TO UNIQ_6F7420EA77153098');
        $this->addSql('ALTER INDEX chill_job.idx_d9e9cabced16fa20 RENAME TO IDX_6F7420EAED16FA20');
        $this->addSql('ALTER INDEX chill_job.uniq_3274952577153098 RENAME TO UNIQ_E3F2021F77153098');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER INDEX chill_job.idx_a90849fb7cde30dd RENAME TO idx_3280b96d7cde30dd');
        $this->addSql('ALTER INDEX chill_job.idx_a90849fbb87bf7b5 RENAME TO idx_3280b96db87bf7b5');
        $this->addSql('ALTER INDEX chill_job.idx_d3edb8d1a4aeafea RENAME TO idx_fbb3cbb4a4aeafea');
        $this->addSql('ALTER INDEX chill_job.idx_d3edb8d1b5e04267 RENAME TO idx_fbb3cbb4b5e04267');
        $this->addSql('ALTER INDEX chill_job.idx_d3edb8d1217bbb47 RENAME TO idx_fbb3cbb4217bbb47');
        $this->addSql('ALTER INDEX chill_job.idx_d3edb8d135e47e35 RENAME TO idx_fbb3cbb435e47e35');
        $this->addSql('ALTER INDEX chill_job.idx_6f7420eaed16fa20 RENAME TO idx_d9e9cabced16fa20');
        $this->addSql('ALTER INDEX chill_job.uniq_6f7420ea77153098 RENAME TO uniq_d9e9cabc77153098');
        $this->addSql('ALTER INDEX chill_job.idx_be1a1859217bbb47 RENAME TO idx_12e4ffbf217bbb47');
        $this->addSql('ALTER INDEX chill_job.idx_c623d20b87bf7b5 RENAME TO idx_e0501be0b87bf7b5');
        $this->addSql('ALTER INDEX chill_job.idx_c623d207cde30dd RENAME TO idx_e0501be07cde30dd');
        $this->addSql('ALTER INDEX chill_job.uniq_e3f2021f77153098 RENAME TO uniq_3274952577153098');
        $this->addSql('ALTER INDEX chill_job.idx_eb9f6a40217bbb47 RENAME TO idx_172eac0a217bbb47');
        $this->addSql('ALTER INDEX chill_job.idx_e66356c7ae1799d8 RENAME TO idx_20be09e2ae1799d8');
        $this->addSql('ALTER INDEX chill_job.idx_5be81b7aae1799d8 RENAME TO idx_102a1262ae1799d8');
        $this->addSql('ALTER INDEX chill_job.idx_ed350ae5217bbb47 RENAME TO idx_3f24f812217bbb47');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5472820d66 RENAME TO idx_10864f3172820d66');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5434fdf11d RENAME TO idx_10864f3134fdf11d');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5454866550 RENAME TO idx_10864f3154866550');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5472a75b6d RENAME TO idx_10864f3172a75b6d');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c542654b57d RENAME TO idx_10864f312654b57d');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c54ac39b1b RENAME TO idx_10864f31ac39b1b');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c54a396afac RENAME TO idx_10864f31a396afac');
        $this->addSql('ALTER INDEX chill_job.uniq_38d83c54217bbb47 RENAME TO uniq_10864f31217bbb47');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c545cfc2299 RENAME TO idx_10864f315cfc2299');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5487541764 RENAME TO idx_10864f3187541764');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c5461e05c22 RENAME TO idx_10864f3161e05c22');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c545742c99d RENAME TO idx_10864f315742c99d');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c54166494d4 RENAME TO idx_10864f31166494d4');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c546f744bb0 RENAME TO idx_10864f316f744bb0');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c548825e118 RENAME TO idx_10864f318825e118');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c54d486e642 RENAME TO idx_10864f31d486e642');
        $this->addSql('ALTER INDEX chill_job.idx_38d83c54afa5e636 RENAME TO idx_10864f31afa5e636');
    }
}
