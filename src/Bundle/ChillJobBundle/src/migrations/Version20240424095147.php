<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Job;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240424095147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration for ChillJobBundle';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA chill_job');
        $this->addSql('CREATE SEQUENCE chill_job.cv_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.cv_experience_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.cv_formation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.frein_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.immersion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.projet_professionnel_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.rome_appellation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_job.rome_metier_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_job.cs_person (id INT NOT NULL, person_id INT DEFAULT NULL, prescripteur_id INT DEFAULT NULL, situationLogement VARCHAR(255) DEFAULT NULL, situationLogementPrecision TEXT DEFAULT NULL, enfantACharge INT DEFAULT NULL, niveauMaitriseLangue JSONB DEFAULT NULL, vehiculePersonnel BOOLEAN DEFAULT NULL, permisConduire JSONB DEFAULT NULL, situationProfessionnelle VARCHAR(255) DEFAULT NULL, dateFinDernierEmploi DATE DEFAULT NULL, typeContrat JSONB DEFAULT NULL, typeContratAide TEXT DEFAULT NULL, ressources JSONB DEFAULT NULL, ressourcesComment TEXT DEFAULT NULL, ressourceDate1Versement DATE DEFAULT NULL, CPFMontant NUMERIC(10, 2) DEFAULT NULL, acomptedif NUMERIC(10, 2) DEFAULT NULL, accompagnement JSONB DEFAULT NULL, accompagnementRQTHDate DATE DEFAULT NULL, accompagnementComment VARCHAR(255) DEFAULT NULL, poleEmploiId VARCHAR(255) DEFAULT NULL, poleEmploiInscriptionDate DATE DEFAULT NULL, cafId VARCHAR(255) DEFAULT NULL, cafInscriptionDate DATE DEFAULT NULL, CERInscriptionDate DATE DEFAULT NULL, PPAEInscriptionDate DATE DEFAULT NULL, CERSignataire TEXT DEFAULT NULL, PPAESignataire TEXT DEFAULT NULL, NEETEligibilite VARCHAR(255) DEFAULT NULL, NEETCommissionDate DATE DEFAULT NULL, FSEMaDemarcheCode TEXT DEFAULT NULL, datecontratIEJ DATE DEFAULT NULL, dateavenantIEJ DATE DEFAULT NULL, dispositifs_notes TEXT DEFAULT NULL, handicap BOOLEAN DEFAULT NULL, handicapnotes TEXT DEFAULT NULL, handicapRecommandation VARCHAR(50) DEFAULT NULL, mobilitemoyentransport JSONB DEFAULT NULL, mobilitenotes TEXT DEFAULT NULL, handicapAccompagnement_id INT DEFAULT NULL, documentCV_id INT DEFAULT NULL, documentAgrementIAE_id INT DEFAULT NULL, documentRQTH_id INT DEFAULT NULL, documentAttestationNEET_id INT DEFAULT NULL, documentCI_id INT DEFAULT NULL, documentTitreSejour_id INT DEFAULT NULL, documentAttestationFiscale_id INT DEFAULT NULL, documentPermis_id INT DEFAULT NULL, documentAttestationCAAF_id INT DEFAULT NULL, documentContraTravail_id INT DEFAULT NULL, documentAttestationFormation_id INT DEFAULT NULL, documentQuittanceLoyer_id INT DEFAULT NULL, documentFactureElectricite_id INT DEFAULT NULL, documentAttestationSecuriteSociale_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_10864F31217BBB47 ON chill_job.cs_person (person_id)');
        $this->addSql('CREATE INDEX IDX_10864F312654B57D ON chill_job.cs_person (handicapAccompagnement_id)');
        $this->addSql('CREATE INDEX IDX_10864F3154866550 ON chill_job.cs_person (documentCV_id)');
        $this->addSql('CREATE INDEX IDX_10864F318825E118 ON chill_job.cs_person (documentAgrementIAE_id)');
        $this->addSql('CREATE INDEX IDX_10864F31A396AFAC ON chill_job.cs_person (documentRQTH_id)');
        $this->addSql('CREATE INDEX IDX_10864F3187541764 ON chill_job.cs_person (documentAttestationNEET_id)');
        $this->addSql('CREATE INDEX IDX_10864F315CFC2299 ON chill_job.cs_person (documentCI_id)');
        $this->addSql('CREATE INDEX IDX_10864F3134FDF11D ON chill_job.cs_person (documentTitreSejour_id)');
        $this->addSql('CREATE INDEX IDX_10864F315742C99D ON chill_job.cs_person (documentAttestationFiscale_id)');
        $this->addSql('CREATE INDEX IDX_10864F31166494D4 ON chill_job.cs_person (documentPermis_id)');
        $this->addSql('CREATE INDEX IDX_10864F3172820D66 ON chill_job.cs_person (documentAttestationCAAF_id)');
        $this->addSql('CREATE INDEX IDX_10864F31AFA5E636 ON chill_job.cs_person (documentContraTravail_id)');
        $this->addSql('CREATE INDEX IDX_10864F3161E05C22 ON chill_job.cs_person (documentAttestationFormation_id)');
        $this->addSql('CREATE INDEX IDX_10864F316F744BB0 ON chill_job.cs_person (documentQuittanceLoyer_id)');
        $this->addSql('CREATE INDEX IDX_10864F31AC39B1B ON chill_job.cs_person (documentFactureElectricite_id)');
        $this->addSql('CREATE INDEX IDX_10864F3172A75B6D ON chill_job.cs_person (documentAttestationSecuriteSociale_id)');
        $this->addSql('CREATE INDEX IDX_10864F31D486E642 ON chill_job.cs_person (prescripteur_id)');
        $this->addSql('CREATE TABLE chill_job.cv (id INT NOT NULL, person_id INT DEFAULT NULL, reportDate DATE NOT NULL, formationLevel VARCHAR(255) DEFAULT NULL, formationType VARCHAR(255) DEFAULT NULL, spokenLanguages JSONB DEFAULT NULL, notes TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3F24F812217BBB47 ON chill_job.cv (person_id)');
        $this->addSql('CREATE TABLE chill_job.cv_experience (id INT NOT NULL, poste TEXT DEFAULT NULL, structure TEXT DEFAULT NULL, startDate DATE DEFAULT NULL, endDate DATE DEFAULT NULL, contratType VARCHAR(100) NOT NULL, notes TEXT DEFAULT NULL, CV_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_102A1262AE1799D8 ON chill_job.cv_experience (CV_id)');
        $this->addSql('CREATE TABLE chill_job.cv_formation (id INT NOT NULL, title TEXT NOT NULL, startDate DATE DEFAULT NULL, endDate DATE DEFAULT NULL, diplomaObtained VARCHAR(255) DEFAULT NULL, diplomaReconnue VARCHAR(50) DEFAULT NULL, organisme TEXT DEFAULT NULL, CV_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_20BE09E2AE1799D8 ON chill_job.cv_formation (CV_id)');
        $this->addSql('CREATE TABLE chill_job.frein (id INT NOT NULL, reportDate DATE NOT NULL, freinsPerso JSONB NOT NULL, notesPerso TEXT NOT NULL, freinsEmploi JSONB NOT NULL, notesEmploi TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_job.immersion (id INT NOT NULL, entreprise_id INT DEFAULT NULL, referent_id INT DEFAULT NULL, domaineActivite TEXT DEFAULT NULL, tuteurName TEXT DEFAULT NULL, tuteurFonction TEXT DEFAULT NULL, tuteurPhoneNumber TEXT DEFAULT NULL, structureAccName TEXT DEFAULT NULL, structureAccPhonenumber TEXT DEFAULT NULL, structureAccEmail TEXT DEFAULT NULL, posteDescriptif TEXT DEFAULT NULL, posteTitle TEXT DEFAULT NULL, posteLieu TEXT DEFAULT NULL, debutDate DATE DEFAULT NULL, duration INTERVAL DEFAULT NULL, horaire TEXT DEFAULT NULL, objectifs JSONB DEFAULT NULL, objectifsAutre TEXT DEFAULT NULL, is_bilan_fullfilled BOOLEAN DEFAULT false NOT NULL, savoirEtre JSONB DEFAULT NULL, savoirEtreNote TEXT DEFAULT NULL, noteimmersion TEXT, principalesActivites TEXT DEFAULT NULL, competencesAcquises TEXT DEFAULT NULL, competencesADevelopper TEXT DEFAULT NULL, noteBilan TEXT DEFAULT NULL, ponctualite_salarie TEXT DEFAULT NULL, ponctualite_salarie_note TEXT DEFAULT NULL, assiduite TEXT DEFAULT NULL, assiduite_note TEXT DEFAULT NULL, interet_activite TEXT DEFAULT NULL, interet_activite_note TEXT DEFAULT NULL, integre_regle TEXT DEFAULT NULL, integre_regle_note TEXT DEFAULT NULL, esprit_initiative TEXT DEFAULT NULL, esprit_initiative_note TEXT DEFAULT NULL, organisation TEXT DEFAULT NULL, organisation_note TEXT DEFAULT NULL, capacite_travail_equipe TEXT DEFAULT NULL, capacite_travail_equipe_note TEXT DEFAULT NULL, style_vestimentaire TEXT DEFAULT NULL, style_vestimentaire_note TEXT DEFAULT NULL, langage_prof TEXT DEFAULT NULL, langage_prof_note TEXT DEFAULT NULL, applique_consigne TEXT DEFAULT NULL, applique_consigne_note TEXT DEFAULT NULL, respect_hierarchie TEXT DEFAULT NULL, respect_hierarchie_note TEXT DEFAULT NULL, structureAccAddress_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4A4AEAFEA ON chill_job.immersion (entreprise_id)');
        $this->addSql('CREATE INDEX IDX_FBB3CBB435E47E35 ON chill_job.immersion (referent_id)');
        $this->addSql('CREATE INDEX IDX_FBB3CBB4B5E04267 ON chill_job.immersion (structureAccAddress_id)');
        $this->addSql('COMMENT ON COLUMN chill_job.immersion.duration IS \'(DC2Type:dateinterval)\'');
        $this->addSql('CREATE TABLE chill_job.projet_professionnel (id INT NOT NULL, reportDate DATE NOT NULL, domaineActiviteSouhait TEXT DEFAULT NULL, typeContrat JSONB DEFAULT NULL, typeContratNotes TEXT DEFAULT NULL, volumeHoraire JSONB DEFAULT NULL, volumeHoraireNotes TEXT DEFAULT NULL, idee TEXT DEFAULT NULL, enCoursConstruction TEXT DEFAULT NULL, domaineActiviteValide TEXT DEFAULT NULL, valideNotes TEXT DEFAULT NULL, projetProfessionnelNote TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_job.projetprofessionnel_souhait (projetprofessionnel_id INT NOT NULL, appellation_id INT NOT NULL, PRIMARY KEY(projetprofessionnel_id, appellation_id))');
        $this->addSql('CREATE INDEX IDX_3280B96DB87BF7B5 ON chill_job.projetprofessionnel_souhait (projetprofessionnel_id)');
        $this->addSql('CREATE INDEX IDX_3280B96D7CDE30DD ON chill_job.projetprofessionnel_souhait (appellation_id)');
        $this->addSql('CREATE TABLE chill_job.projetprofessionnel_valide (projetprofessionnel_id INT NOT NULL, appellation_id INT NOT NULL, PRIMARY KEY(projetprofessionnel_id, appellation_id))');
        $this->addSql('CREATE INDEX IDX_E0501BE0B87BF7B5 ON chill_job.projetprofessionnel_valide (projetprofessionnel_id)');
        $this->addSql('CREATE INDEX IDX_E0501BE07CDE30DD ON chill_job.projetprofessionnel_valide (appellation_id)');
        $this->addSql('CREATE TABLE chill_job.rome_appellation (id INT NOT NULL, metier_id INT DEFAULT NULL, code VARCHAR(40) NOT NULL, libelle TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D9E9CABC77153098 ON chill_job.rome_appellation (code)');
        $this->addSql('CREATE INDEX IDX_D9E9CABCED16FA20 ON chill_job.rome_appellation (metier_id)');
        $this->addSql('CREATE TABLE chill_job.rome_metier (id INT NOT NULL, libelle TEXT NOT NULL, code VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3274952577153098 ON chill_job.rome_metier (code)');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F312654B57D FOREIGN KEY (handicapAccompagnement_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3154866550 FOREIGN KEY (documentCV_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F318825E118 FOREIGN KEY (documentAgrementIAE_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31A396AFAC FOREIGN KEY (documentRQTH_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3187541764 FOREIGN KEY (documentAttestationNEET_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F315CFC2299 FOREIGN KEY (documentCI_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3134FDF11D FOREIGN KEY (documentTitreSejour_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F315742C99D FOREIGN KEY (documentAttestationFiscale_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31166494D4 FOREIGN KEY (documentPermis_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3172820D66 FOREIGN KEY (documentAttestationCAAF_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31AFA5E636 FOREIGN KEY (documentContraTravail_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3161E05C22 FOREIGN KEY (documentAttestationFormation_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F316F744BB0 FOREIGN KEY (documentQuittanceLoyer_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31AC39B1B FOREIGN KEY (documentFactureElectricite_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F3172A75B6D FOREIGN KEY (documentAttestationSecuriteSociale_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cs_person ADD CONSTRAINT FK_10864F31D486E642 FOREIGN KEY (prescripteur_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cv ADD CONSTRAINT FK_3F24F812217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cv_experience ADD CONSTRAINT FK_102A1262AE1799D8 FOREIGN KEY (CV_id) REFERENCES chill_job.cv (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.cv_formation ADD CONSTRAINT FK_20BE09E2AE1799D8 FOREIGN KEY (CV_id) REFERENCES chill_job.cv (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.immersion ADD CONSTRAINT FK_FBB3CBB4A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.immersion ADD CONSTRAINT FK_FBB3CBB435E47E35 FOREIGN KEY (referent_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.immersion ADD CONSTRAINT FK_FBB3CBB4B5E04267 FOREIGN KEY (structureAccAddress_id) REFERENCES chill_main_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_souhait ADD CONSTRAINT FK_3280B96DB87BF7B5 FOREIGN KEY (projetprofessionnel_id) REFERENCES chill_job.projet_professionnel (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_souhait ADD CONSTRAINT FK_3280B96D7CDE30DD FOREIGN KEY (appellation_id) REFERENCES chill_job.rome_appellation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_valide ADD CONSTRAINT FK_E0501BE0B87BF7B5 FOREIGN KEY (projetprofessionnel_id) REFERENCES chill_job.projet_professionnel (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_valide ADD CONSTRAINT FK_E0501BE07CDE30DD FOREIGN KEY (appellation_id) REFERENCES chill_job.rome_appellation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_job.rome_appellation ADD CONSTRAINT FK_D9E9CABCED16FA20 FOREIGN KEY (metier_id) REFERENCES chill_job.rome_metier (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_job.cv_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.cv_experience_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.cv_formation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.frein_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.immersion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.projet_professionnel_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.rome_appellation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_job.rome_metier_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31217BBB47');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F312654B57D');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3154866550');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F318825E118');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31A396AFAC');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3187541764');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F315CFC2299');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3134FDF11D');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F315742C99D');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31166494D4');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3172820D66');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31AFA5E636');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3161E05C22');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F316F744BB0');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31AC39B1B');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F3172A75B6D');
        $this->addSql('ALTER TABLE chill_job.cs_person DROP CONSTRAINT FK_10864F31D486E642');
        $this->addSql('ALTER TABLE chill_job.cv DROP CONSTRAINT FK_3F24F812217BBB47');
        $this->addSql('ALTER TABLE chill_job.cv_experience DROP CONSTRAINT FK_102A1262AE1799D8');
        $this->addSql('ALTER TABLE chill_job.cv_formation DROP CONSTRAINT FK_20BE09E2AE1799D8');
        $this->addSql('ALTER TABLE chill_job.immersion DROP CONSTRAINT FK_FBB3CBB4A4AEAFEA');
        $this->addSql('ALTER TABLE chill_job.immersion DROP CONSTRAINT FK_FBB3CBB435E47E35');
        $this->addSql('ALTER TABLE chill_job.immersion DROP CONSTRAINT FK_FBB3CBB4B5E04267');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_souhait DROP CONSTRAINT FK_3280B96DB87BF7B5');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_souhait DROP CONSTRAINT FK_3280B96D7CDE30DD');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_valide DROP CONSTRAINT FK_E0501BE0B87BF7B5');
        $this->addSql('ALTER TABLE chill_job.projetprofessionnel_valide DROP CONSTRAINT FK_E0501BE07CDE30DD');
        $this->addSql('ALTER TABLE chill_job.rome_appellation DROP CONSTRAINT FK_D9E9CABCED16FA20');
        $this->addSql('DROP TABLE chill_job.cs_person');
        $this->addSql('DROP TABLE chill_job.cv');
        $this->addSql('DROP TABLE chill_job.cv_experience');
        $this->addSql('DROP TABLE chill_job.cv_formation');
        $this->addSql('DROP TABLE chill_job.frein');
        $this->addSql('DROP TABLE chill_job.immersion');
        $this->addSql('DROP TABLE chill_job.projet_professionnel');
        $this->addSql('DROP TABLE chill_job.projetprofessionnel_souhait');
        $this->addSql('DROP TABLE chill_job.projetprofessionnel_valide');
        $this->addSql('DROP TABLE chill_job.rome_appellation');
        $this->addSql('DROP TABLE chill_job.rome_metier');
        $this->addSql('DROP SCHEMA chill_job');
    }
}
