module.exports = function (encore, chillEntries) {
  let dispositif_edit_file =
      __dirname + "/Resources/public/module/dispositif_edit/index.js",
    personal_situation_edit_file =
      __dirname + "/Resources/public/module/personal_situation/index.js",
    cv_edit_file = __dirname + "/Resources/public/module/cv_edit/index.js",
    immersion_edit_file =
      __dirname + "/Resources/public/module/immersion_edit/index.js",
    images = __dirname + "/Resources/public/images/index.js";
  encore.addEntry("dispositifs_edit", dispositif_edit_file);
  encore.addEntry("personal_situation_edit", personal_situation_edit_file);
  encore.addEntry("immersion_edit", immersion_edit_file);
  encore.addEntry("images", images);
  encore.addEntry("cs_cv", cv_edit_file);
};
