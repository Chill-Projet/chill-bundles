<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\ThirdParty;

use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeProviderInterface;

/**
 * Type "Prescripteur" pour les third parties.
 */
class PrescripteurType implements ThirdPartyTypeProviderInterface
{
    public static function getKey(): string
    {
        return 'prescripteur';
    }
}
