<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\DependencyInjection;

use Chill\JobBundle\Controller\CSPersonController;
use Chill\JobBundle\Controller\CVCrudController;
use Chill\JobBundle\Controller\FreinCrudController;
use Chill\JobBundle\Controller\ImmersionCrudController;
use Chill\JobBundle\Controller\ProjetProfessionnelCrudController;
use Chill\JobBundle\Entity\CSPerson;
use Chill\JobBundle\Entity\CV;
use Chill\JobBundle\Entity\Frein;
use Chill\JobBundle\Entity\Immersion;
use Chill\JobBundle\Entity\ProjetProfessionnel;
use Chill\JobBundle\Form\CVType;
use Chill\JobBundle\Form\FreinType;
use Chill\JobBundle\Form\ImmersionType;
use Chill\JobBundle\Form\ProjetProfessionnelType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillJobExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/3party_type.yml');
        $loader->load('services/controller.yml');
        $loader->load('services/form.yml');
        $loader->load('services/export.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/security.yml');
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->prependRoute($container);
        $this->prependCruds($container);
    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => CSPerson::class,
                    'controller' => CSPersonController::class,
                    'name' => 'job',
                    'base_role' => 'ROLE_USER',
                    'base_path' => '/person/job/',
                ],
                [
                    'class' => CV::class,
                    'controller' => CVCrudController::class,
                    'name' => 'cscv',
                    'base_role' => 'ROLE_USER',
                    'base_path' => '/person/report/cv',
                    'form_class' => CVType::class,
                    'actions' => [
                        'view' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/CV/view.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/CV/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/CV/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Report/delete.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => ProjetProfessionnel::class,
                    'controller' => ProjetProfessionnelCrudController::class,
                    'name' => 'projet_prof',
                    'base_role' => 'ROLE_USER',
                    'base_path' => '/person/report/projet-professionnel',
                    'form_class' => ProjetProfessionnelType::class,
                    'actions' => [
                        'view' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/ProjetProfessionnel/view.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/ProjetProfessionnel/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/ProjetProfessionnel/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Report/delete.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Frein::class,
                    'controller' => FreinCrudController::class,
                    'name' => 'csfrein',
                    'base_role' => 'ROLE_USER',
                    'base_path' => '/person/report/frein',
                    'form_class' => FreinType::class,
                    'actions' => [
                        'view' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Frein/view.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Frein/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Frein/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Report/delete.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Immersion::class,
                    'controller' => ImmersionCrudController::class,
                    'name' => 'immersion',
                    'base_role' => 'ROLE_USER',
                    'base_path' => '/person/report/immersion',
                    'form_class' => ImmersionType::class,
                    'actions' => [
                        'view' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Immersion/view.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Immersion/new.html.twig',
                        ],
                        'bilan' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Immersion/edit-bilan.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Immersion/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillJob/Report/delete.html.twig',
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function prependRoute(ContainerBuilder $container): void
    {
        // declare routes for job bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillJobBundle/Resources/config/routing.yml',
                ],
            ],
        ]);
    }
}
