<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Chill\JobBundle\Entity\Frein;
use Chill\JobBundle\Entity\CV;
use Chill\JobBundle\Entity\Immersion;
use Chill\JobBundle\Entity\ProjetProfessionnel;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;

/**
 * check ACL for JobBundle.
 */
class JobVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    public const REPORT_NEW = 'CHILL_JOB_REPORT_NEW';
    public const REPORT_CV = 'CHILL_JOB_REPORT_CV';
    public const REPORT_DELETE = 'CHILL_JOB_REPORT_DELETE';

    public const ALL = [
        self::REPORT_NEW,
        self::REPORT_CV,
        self::REPORT_DELETE,
    ];

    protected $date15DaysAgo;

    /**
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    public function __construct(AuthorizationHelper $authorizationHelper)
    {
        $this->date15DaysAgo = new \DateTime('-15 days');
        $this->authorizationHelper = $authorizationHelper;
    }

    protected function supports($attribute, $subject)
    {
        if (!\in_array($attribute, self::ALL, true)) {
            return false;
        }

        if ($subject instanceof Frein
            || $subject instanceof CV
            || $subject instanceof Immersion
            || $subject instanceof ProjetProfessionnel
            || $subject instanceof Person) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($subject instanceof Person) {
            $center = $subject->getCenter();
        } else {
            $center = $subject->getPerson()->getCenter();
        }

        switch ($attribute) {
            case self::REPORT_NEW:
                return $this->authorizationHelper->userHasAccess($token->getUser(), $center, $attribute);

            case self::REPORT_CV:
                if (! ($subject instanceof CV || $subject instanceof Person)) {
                    return false;
                }

                return $this->authorizationHelper->userHasAccess($token->getUser(), $center, $attribute);

            case self::REPORT_DELETE:
                if ($subject instanceof Immersion) {
                    $date = \DateTimeImmutable::createFromMutable($subject->getDebutDate())->add($subject->getDuration());
                } else {
                    $date = $subject->getReportDate();
                }

                if (!$this->authorizationHelper->userHasAccess($token->getUser(), $center, $attribute)) {
                    return false;
                }

                return $this->date15DaysAgo < $date;
        }

        return true;
    }

    public function getRoles(): array
    {
        return self::ALL;
    }

    public function getRolesWithHierarchy(): array
    {
        return ['JobBundle' => $this->getRoles()];
    }

    public function getRolesWithoutScope(): array
    {
        return $this->getRoles();
    }
}
