<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Controller;

use Chill\PersonBundle\CRUD\Controller\EntityPersonCRUDController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD Controller for reports (Frein, ...).
 */
class CVCrudController extends EntityPersonCRUDController
{
    protected function onBeforeRedirectAfterSubmission(string $action, $entity, FormInterface $form, Request $request): ?Response
    {
        $next = $request->request->get('submit', 'save-and-close');

        return match ($next) {
            'save-and-close', 'delete-and-close' => $this->redirectToRoute('chill_job_report_index', [
                'person' => $entity->getPerson()->getId(),
            ]),
            default => parent::onBeforeRedirectAfterSubmission($action, $entity, $form, $request),
        };
    }

    protected function duplicateEntity(string $action, Request $request)
    {
        if ('cv' === $this->getCrudName()) {
            $id = $request->query->get('duplicate_id', 0);
            /** @var \Chill\JobBundle\Entity\CV $cv */
            $cv = $this->getEntity($action, $id, $request);
            $em = $this->managerRegistry->getManager();

            $em->detach($cv);

            foreach ($cv->getExperiences() as $experience) {
                $cv->removeExperience($experience);
                $em->detach($experience);
                $cv->addExperience($experience);
            }
            foreach ($cv->getFormations() as $formation) {
                $cv->removeFormation($formation);
                $em->detach($formation);
                $cv->addFormation($formation);
            }

            return $cv;
        }

        return parent::duplicateEntity($action, $request);
    }
}
