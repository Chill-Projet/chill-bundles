<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Controller;

use Chill\PersonBundle\CRUD\Controller\OneToOneEntityPersonCRUDController;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Chill\JobBundle\Form\CSPersonPersonalSituationType;
use Chill\JobBundle\Form\CSPersonDispositifsType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CSPersonController extends OneToOneEntityPersonCRUDController
{
    #[Route(path: '{_locale}/person/job/personal_situation/{id}/edit', name: 'chill_crud_job_personal_situation_edit')]
    public function personalSituationEdit(Request $request, $id): Response
    {
        return $this->formEditAction(
            'ps_situation_edit',
            $request,
            $id,
            CSPersonPersonalSituationType::class
        );
    }

    #[Route(path: '{_locale}/person/job/dispositifs/{id}/edit', name: 'chill_crud_job_dispositifs_edit')]
    public function dispositifsEdit(Request $request, $id)
    {
        return $this->formEditAction(
            'dispositifs_edit',
            $request,
            $id,
            CSPersonDispositifsType::class
        );
    }

    #[Route(path: '{_locale}/person/job/{person}/personal_situation', name: 'chill_crud_job_personal_situation_view')]
    public function personalSituationView(Request $request, $person): Response
    {
        return $this->viewAction('ps_situation_view', $request, $person);
    }

    #[Route(path: '{_locale}/person/job/{person}/dispositifs', name: 'chill_crud_job_dispositifs_view')]
    public function dispositifsView(Request $request, $person): Response
    {
        return $this->viewAction('dispositifs_view', $request, $person);
    }

    protected function generateRedirectOnCreateRoute($action, Request $request, $entity): string
    {
        $route = '';

        switch ($action) {
            case 'ps_situation_view':
                $route = 'chill_crud_job_personal_situation_edit';
                break;
            case 'dispositifs_view':
                $route = 'chill_crud_job_dispositifs_edit';
                break;
            default:
                parent::generateRedirectOnCreateRoute($action, $request, $entity);
        }

        return $this->generateUrl($route, ['id' => $entity->getPerson()->getId()]);
    }

    protected function checkACL($action, $entity): void
    {
        match ($action) {
            'ps_situation_edit', 'dispositifs_edit' => $this->denyAccessUnlessGranted(
                PersonVoter::UPDATE,
                $entity->getPerson()
            ),
            'ps_situation_view', 'dispositifs_view' => $this->denyAccessUnlessGranted(
                PersonVoter::SEE,
                $entity->getPerson()
            ),
            default => parent::checkACL($action, $entity),
        };
    }

    protected function onBeforeRedirectAfterSubmission(string $action, $entity, FormInterface $form, Request $request): ?Response
    {
        return match ($action) {
            'ps_situation_edit' => $this->redirectToRoute(
                'chill_crud_'.$this->getCrudName().'_personal_situation_view',
                ['person' => $entity->getId()]
            ),
            'dispositifs_edit' => $this->redirectToRoute(
                'chill_crud_'.$this->getCrudName().'_dispositifs_view',
                ['person' => $entity->getId()]
            ),
            default => null,
        };
    }

    protected function getTemplateFor($action, $entity, Request $request): string
    {
        return match ($action) {
            'ps_situation_edit' => '@ChillJob/CSPerson/personal_situation_edit.html.twig',
            'dispositifs_edit' => '@ChillJob/CSPerson/dispositifs_edit.html.twig',
            'ps_situation_view' => '@ChillJob/CSPerson/personal_situation_view.html.twig',
            'dispositifs_view' => '@ChillJob/CSPerson/dispositifs_view.html.twig',
            default => parent::getTemplateFor($action, $entity, $request),
        };
    }

    protected function createFormFor(string $action, $entity, ?string $formClass = null, array $formOptions = []): FormInterface
    {
        switch ($action) {
            case 'ps_situation_edit':
            case 'dispositifs_edit':
                $form = $this->createForm($formClass, $entity, \array_merge(
                    $formOptions,
                    ['center' => $entity->getPerson()->getCenter()]
                ));
                $this->customizeForm($action, $form);

                return $form;

            default:
                return parent::createFormFor($action, $entity, $formClass, $formOptions);
        }
    }

    protected function generateLabelForButton($action, $formName, $form): string
    {
        switch ($action) {
            case 'ps_situation_edit':
            case 'dispositifs_edit':
                if ('submit' === $formName) {
                    return 'Enregistrer';
                }
                throw new \LogicException("this formName is not supported: {$formName}");
                break;
            default:
                return 'Enregistrer';
        }
    }
}
