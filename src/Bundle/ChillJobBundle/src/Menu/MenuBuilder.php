<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\JobBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\JobBundle\Security\Authorization\JobVoter;

class MenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var \Chill\PersonBundle\Entity\Person $person */
        $person = $parameters['person'];

        if ($this->authorizationChecker->isGranted(JobVoter::REPORT_NEW, $person)) {
            $menu->addChild('Situation personnelle', [
                'route' => 'chill_crud_job_personal_situation_view',
                'routeParameters' => [
                    'person' => $person->getId(),
                ],
            ])
                ->setExtras([
                    'order' => 50,
                ]);
            $menu->addChild('Dispositifs', [
                'route' => 'chill_crud_job_dispositifs_view',
                'routeParameters' => [
                    'person' => $person->getId(),
                ],
            ])
                ->setExtras([
                    'order' => 51,
                ]);
        }

        $menu->addChild('Emploi', [
            'route' => 'chill_job_report_index',
            'routeParameters' => [
                'person' => $person->getId(),
            ],
        ])
            ->setExtras([
                'order' => 52,
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
