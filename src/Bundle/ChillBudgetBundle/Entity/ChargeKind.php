<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Type of charge.
 */
#[UniqueEntity(fields: ['kind'])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_budget.charge_type')]
#[ORM\UniqueConstraint(name: 'charge_kind_unique_type_idx', fields: ['kind'])]
class ChargeKind
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $isActive = true;

    #[Assert\Regex(pattern: '/^[a-z0-9\-_]{1,}$/', message: 'budget.admin.form.kind.only_alphanumeric')]
    #[Assert\Length(min: 3)]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, options: ['default' => ''], nullable: false)]
    private string $kind = '';

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, length: 255, options: ['default' => '{}', 'jsonb' => true])]
    private array $name = [];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::FLOAT, options: ['default' => '0.00'])]
    private float $ordering = 0.00;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, length: 255, options: ['default' => '{}', 'jsonb' => true])]
    private array $tags = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getKind(): string
    {
        return $this->kind;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setKind(string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setOrdering(float $ordering): ChargeKind
    {
        $this->ordering = $ordering;

        return $this;
    }
}
