<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Entity;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractElement.
 */
#[ORM\MappedSuperclass]
abstract class AbstractElement
{
    #[Assert\GreaterThan(value: 0)]
    #[Assert\NotNull(message: 'The amount cannot be empty')]
    #[ORM\Column(name: 'amount', type: \Doctrine\DBAL\Types\Types::DECIMAL, precision: 10, scale: 2)]
    private string $amount;

    #[ORM\Column(name: 'comment', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $comment = null;

    #[Assert\GreaterThan(propertyPath: 'startDate', message: "The budget element's end date must be after the start date")]
    #[ORM\Column(name: 'endDate', type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $endDate = null;

    #[ORM\ManyToOne(targetEntity: Household::class)]
    private ?Household $household = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person = null;

    #[ORM\Column(name: 'startDate', type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $startDate;

    #[ORM\Column(name: 'type', type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private string $type = '';

    /* Getters and Setters */

    public function getAmount(): float
    {
        return (float) $this->amount;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getHousehold(): ?Household
    {
        return $this->household;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getType(): string
    {
        return $this->type;
    }

    abstract public function isCharge(): bool;

    public function isEmpty()
    {
        return 0 === $this->amount;
    }

    abstract public function isResource(): bool;

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function setComment(?string $comment = null): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function setEndDate(?\DateTimeInterface $endDate = null): self
    {
        if ($endDate instanceof \DateTime) {
            $this->endDate = \DateTimeImmutable::createFromMutable($endDate);
        } elseif (null === $endDate) {
            $this->endDate = null;
        } else {
            $this->endDate = $endDate;
        }

        return $this;
    }

    public function setHousehold(?Household $household): self
    {
        $this->household = $household;

        return $this;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        if ($startDate instanceof \DateTime) {
            $this->startDate = \DateTimeImmutable::createFromMutable($startDate);
        } elseif (null === $startDate) {
            $this->startDate = null;
        } else {
            $this->startDate = $startDate;
        }

        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
