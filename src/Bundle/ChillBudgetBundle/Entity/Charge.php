<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Entity;

use Chill\MainBundle\Entity\HasCentersInterface;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * Charge.
 */
#[ORM\Entity(repositoryClass: \Chill\BudgetBundle\Repository\ChargeRepository::class)]
#[ORM\Table(name: 'chill_budget.charge')]
class Charge extends AbstractElement implements HasCentersInterface
{
    final public const HELP_ASKED = 'running';

    final public const HELP_NO = 'no';

    final public const HELP_NOT_RELEVANT = 'not-relevant';

    final public const HELP_YES = 'yes';

    final public const HELPS = [
        self::HELP_ASKED,
        self::HELP_NO,
        self::HELP_YES,
        self::HELP_NOT_RELEVANT,
    ];

    #[ORM\ManyToOne(targetEntity: ChargeKind::class, inversedBy: 'AbstractElement')]
    #[ORM\JoinColumn]
    private ?ChargeKind $charge = null;

    #[ORM\Column(name: 'help', type: \Doctrine\DBAL\Types\Types::STRING, nullable: true)]
    private ?string $help = self::HELP_NOT_RELEVANT;

    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    public function __construct()
    {
        $this->setStartDate(new \DateTimeImmutable('today'));
    }

    public function getCenters(): array
    {
        if (null !== $this->getPerson()) {
            return [$this->getPerson()->getCenter()];
        }

        return $this->getHousehold()->getCurrentPersons()->map(static fn (Person $p) => $p->getCenter())->toArray();
    }

    public function getCharge(): ?ChargeKind
    {
        return $this->charge;
    }

    public function getHelp()
    {
        return $this->help;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function isCharge(): bool
    {
        return true;
    }

    public function isResource(): bool
    {
        return false;
    }

    public function setCharge(?ChargeKind $charge): self
    {
        $this->charge = $charge;

        return $this;
    }

    public function setHelp(?string $help)
    {
        $this->help = $help;

        return $this;
    }
}
