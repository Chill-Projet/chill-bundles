<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Repository;

use Chill\BudgetBundle\Entity\ChargeKind;
use Doctrine\Persistence\ObjectRepository;

interface ChargeKindRepositoryInterface extends ObjectRepository
{
    public function find($id): ?ChargeKind;

    /**
     * @return array<ChargeKind>
     */
    public function findAll(): array;

    /**
     * @return array<ChargeKind>
     */
    public function findAllActive(): array;

    /**
     * @return array<ChargeKind>
     */
    public function findAllByType(string $type): array;

    /**
     * @return array<ChargeKind>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?ChargeKind;

    public function findOneByKind(string $kind): ?ChargeKind;

    public function getClassName(): string;
}
