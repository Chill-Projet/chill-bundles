<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Repository;

use Chill\BudgetBundle\Entity\ChargeKind;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class ChargeKindRepository implements ChargeKindRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(ChargeKind::class);
    }

    public function find($id): ?ChargeKind
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<ChargeKind>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array<ChargeKind>
     */
    public function findAllActive(): array
    {
        $qb = $this->repository->createQueryBuilder('c');

        return $qb
            ->select('c')
            ->where($qb->expr()->eq('c.isActive', 'true'))
            ->orderBy('c.ordering', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<ChargeKind>
     */
    public function findAllByType(string $type): array
    {
        return $this->findBy(['elementType' => $type]);
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<ChargeKind>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?ChargeKind
    {
        return $this->repository->findOneBy($criteria);
    }

    public function findOneByKind(string $kind): ?ChargeKind
    {
        return $this->repository->findOneBy(['kind' => $kind]);
    }

    public function getClassName(): string
    {
        return ChargeKind::class;
    }
}
