<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Repository;

use Chill\BudgetBundle\Entity\ResourceKind;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class ResourceKindRepository implements ResourceKindRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(ResourceKind::class);
    }

    public function find($id): ?ResourceKind
    {
        return $this->repository->find($id);
    }

    /**
     * @return list<ResourceKind>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return list<ResourceKind>
     */
    public function findAllActive(): array
    {
        $qb = $this->repository->createQueryBuilder('r');

        return $qb
            ->select('r')
            ->where($qb->expr()->eq('r.isActive', 'true'))
            ->orderBy('r.ordering', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByKind(string $kind): ?ResourceKind
    {
        return $this->repository->findOneBy(['kind' => $kind]);
    }

    /**
     * @return list<ResourceKind>
     */
    public function findAllByType(string $type): array
    {
        return $this->findBy(['elementType' => $type]);
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return list<ResourceKind>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?ResourceKind
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return ResourceKind::class;
    }
}
