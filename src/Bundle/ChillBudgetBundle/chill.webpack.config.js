// this file loads all assets from the Chill budget bundle
module.exports = function (encore, entries) {
  encore.addAliases({
    ChillBudgetAssets: __dirname + "/Resources/public",
  });

  encore.addEntry("page_budget", __dirname + "/Resources/public/page/index.js");
};
