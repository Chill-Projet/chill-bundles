<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ElementControllerTest extends WebTestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testIndex()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/index');
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testList()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/list');
    }
}
