<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Service\Summary;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Person;

/**
 * Helps to find a summary of the budget: the sum of resources and charges.
 */
interface SummaryBudgetInterface
{
    public function getSummaryForHousehold(?Household $household): array;

    public function getSummaryForPerson(?Person $person): array;
}
