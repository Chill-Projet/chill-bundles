<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221130101659 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.resource_type DROP kind');
        $this->addSql('ALTER TABLE chill_budget.resource_type DROP tags');
        $this->addSql('ALTER TABLE chill_budget.charge_type DROP kind');
        $this->addSql('ALTER TABLE chill_budget.charge_type DROP tags');
    }

    public function getDescription(): string
    {
        return 'Add kind and tags property to charge and resource types';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge_type ADD kind VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_budget.charge_type ADD tags JSONB DEFAULT \'{}\'::jsonb NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_budget.charge_type.tags IS \'(DC2Type:jsonb)\'');
        $this->addSql('ALTER TABLE chill_budget.resource_type ADD kind VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_budget.resource_type ADD tags JSONB DEFAULT \'{}\'::jsonb NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_budget.resource_type.tags IS \'(DC2Type:jsonb)\'');
    }
}
