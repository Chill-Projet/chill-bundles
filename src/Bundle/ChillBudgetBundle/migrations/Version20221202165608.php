<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221202165608 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge DROP CONSTRAINT FK_5C99D2C355284914');
        $this->addSql('ALTER TABLE chill_budget.charge DROP charge_id');
        $this->addSql('ALTER TABLE chill_budget.resource DROP CONSTRAINT FK_5E0A5E9789329D25');
        $this->addSql('ALTER TABLE chill_budget.resource DROP resource_id');
    }

    public function getDescription(): string
    {
        return 'Integrate budget admin entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge ADD charge_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_budget.charge ADD CONSTRAINT FK_5C99D2C355284914 FOREIGN KEY (charge_id) REFERENCES chill_budget.charge_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5C99D2C355284914 ON chill_budget.charge (charge_id)');
        $this->addSql('ALTER TABLE chill_budget.resource ADD resource_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_budget.resource ADD CONSTRAINT FK_5E0A5E9789329D25 FOREIGN KEY (resource_id) REFERENCES chill_budget.resource_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5E0A5E9789329D25 ON chill_budget.resource (resource_id)');
    }
}
