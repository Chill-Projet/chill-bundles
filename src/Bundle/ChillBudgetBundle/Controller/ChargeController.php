<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Controller;

use Chill\BudgetBundle\Entity\Charge;
use Chill\BudgetBundle\Form\ChargeType;
use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;

class ChargeController extends AbstractElementController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/charge/{id}/delete', name: 'chill_budget_charge_delete')]
    public function deleteAction(Request $request, Charge $charge)
    {
        return $this->_delete(
            $charge,
            $request,
            '@ChillBudget/Charge/confirm_delete.html.twig',
            'Charge deleted'
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/charge/{id}/edit', name: 'chill_budget_charge_edit')]
    public function editAction(Request $request, Charge $charge)
    {
        return $this->_edit(
            $charge,
            $request,
            '@ChillBudget/Charge/edit.html.twig',
            'Charge updated'
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/charge/by-person/{id}/new', name: 'chill_budget_charge_new')]
    public function newAction(Request $request, Person $person)
    {
        return $this->_new(
            $person,
            $request,
            '@ChillBudget/Charge/new.html.twig',
            'Charge created'
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/charge/by-household/{id}/new', name: 'chill_budget_charge_household_new')]
    public function newHouseholdAction(Request $request, Household $household)
    {
        return $this->_new(
            $household,
            $request,
            '@ChillBudget/Charge/new.html.twig',
            'Charge created'
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/charge/{id}/view', name: 'chill_budget_charge_view')]
    public function viewAction(Charge $charge)
    {
        return $this->_view($charge, '@ChillBudget/Charge/view.html.twig');
    }

    protected function createNewElement()
    {
        return new Charge();
    }

    protected function getType()
    {
        return ChargeType::class;
    }
}
