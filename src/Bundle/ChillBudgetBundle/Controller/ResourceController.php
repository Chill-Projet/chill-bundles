<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Controller;

use Chill\BudgetBundle\Entity\Resource;
use Chill\BudgetBundle\Form\ResourceType;
use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ResourceController extends AbstractElementController
{
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/resource/{id}/delete', name: 'chill_budget_resource_delete')]
    public function deleteAction(Request $request, Resource $resource)
    {
        return $this->_delete(
            $resource,
            $request,
            '@ChillBudget/Resource/confirm_delete.html.twig',
            'Resource deleted'
        );
    }

    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/resource/{id}/edit', name: 'chill_budget_resource_edit')]
    public function editAction(Request $request, Resource $resource): Response
    {
        return $this->_edit(
            $resource,
            $request,
            '@ChillBudget/Resource/edit.html.twig',
            'Resource updated'
        );
    }

    /**
     * Create a new budget element for a person.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/resource/by-person/{id}/new', name: 'chill_budget_resource_new')]
    public function newAction(Request $request, Person $person): Response
    {
        return $this->_new(
            $person,
            $request,
            '@ChillBudget/Resource/new.html.twig',
            'Resource created'
        );
    }

    /**
     * Create new budget element for a household.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/resource/by-household/{id}/new', name: 'chill_budget_resource_household_new')]
    public function newHouseholdAction(Request $request, Household $household): Response
    {
        return $this->_new(
            $household,
            $request,
            '@ChillBudget/Resource/new.html.twig',
            'Resource created'
        );
    }

    #[\Symfony\Component\Routing\Annotation\Route(path: '{_locale}/budget/resource/{id}/view', name: 'chill_budget_resource_view')]
    public function viewAction(Resource $resource): Response
    {
        return $this->_view($resource, '@ChillBudget/Resource/view.html.twig');
    }

    protected function createNewElement()
    {
        return new Resource();
    }

    protected function getType()
    {
        return ResourceType::class;
    }
}
