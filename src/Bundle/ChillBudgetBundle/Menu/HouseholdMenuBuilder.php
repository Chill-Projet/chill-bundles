<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Menu;

use Chill\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Entity\Household\Household;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class HouseholdMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private Security $security, private TranslatorInterface $translator) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var Household $household */
        $household = $parameters['household'];

        if ($this->security->isGranted(BudgetElementVoter::SEE, $household)) {
            $menu->addChild($this->translator->trans('household.Budget'), [
                'route' => 'chill_budget_elements_household_index',
                'routeParameters' => [
                    'id' => $household->getId(),
                ], ])
                ->setExtras(['order' => 19]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['household'];
    }
}
