<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\FranceTravailApiBundle\Tests\ApiHelper;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\FranceTravailApiBundle\ApiHelper\PartenaireRomeAppellation;

/**
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * @internal
 *
 * @coversNothing
 */
class PartenaireRomeAppellationTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
    }

    public function testGetListeMetiersSimple()
    {
        /** @var PartenaireRomeAppellation $appellations */
        $appellations = self::$kernel
            ->getContainer()
            ->get(PartenaireRomeAppellation::class)
        ;

        $data = $appellations->getListeAppellation('arb');

        $this->assertTrue(\is_array($data));
        $this->assertNotNull($data[0]->libelle);
        $this->assertNotNull($data[0]->code);
    }

    public function testGetListeMetiersTooMuchRequests()
    {
        /** @var PartenaireRomeMetier $appellations */
        $appellations = self::$kernel
            ->getContainer()
            ->get(PartenaireRomeAppellation::class)
        ;

        $appellations->getListeAppellation('arb');
        $appellations->getListeAppellation('ing');
        $appellations->getListeAppellation('rob');
        $appellations->getListeAppellation('chori');
        $data = $appellations->getListeAppellation('camion');

        $this->assertTrue(
            $data[0] instanceof \stdClass,
            'assert that first index of data is an instance of stdClass'
        );
    }

    public function testGetAppellation()
    {
        /** @var PartenaireRomeMetier $appellations */
        $appellations = self::$kernel
            ->getContainer()
            ->get(PartenaireRomeAppellation::class)
        ;

        $a = $appellations->getListeAppellation('arb');

        $full = $appellations->getAppellation($a[0]->code);

        $this->assertNotNull(
            $full->libelle,
            'assert that libelle is not null'
        );
        $this->assertTrue(
            $full->metier instanceof \stdClass,
            'assert that metier is returned'
        );
        $this->assertNotNull(
            $full->metier->libelle,
            'assert that metier->libelle is not null'
        );
    }
}
