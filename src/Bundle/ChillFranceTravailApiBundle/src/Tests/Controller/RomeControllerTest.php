<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\FranceTravailApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class RomeControllerTest extends WebTestCase
{
    public function testAppellationsearch()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/{_locale}/pole-emploi/appellation/search');
    }
}
