<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Menu;

use Chill\EventBundle\Security\EventVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(
        private Security $security,
        private TranslatorInterface $translator,
    ) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->security->isGranted(EventVoter::SEE)) {
            $menu->addChild(
                $this->translator->trans('Events'),
                [
                    'route' => 'chill_event_event_list',
                ]
            )->setExtras([
                'order' => 250,
            ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
