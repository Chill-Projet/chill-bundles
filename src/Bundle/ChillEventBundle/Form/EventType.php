<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Form;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Form\StoredObjectType;
use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Form\Type\PickEventTypeType;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Form\Type\ChillCollectionType;
use Chill\MainBundle\Form\Type\ChillDateTimeType;
use Chill\MainBundle\Form\Type\CommentType;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Form\Type\PickUserLocationType;
use Chill\MainBundle\Form\Type\ScopePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('date', ChillDateTimeType::class, [
                'required' => true,
            ])
            ->add('circle', ScopePickerType::class, [
                'center' => $options['center'],
                'role' => $options['role'],
            ])
            ->add('type', PickEventTypeType::class, [
                'placeholder' => 'Pick a type of event',
                'attr' => [
                    'class' => '',
                ],
            ])
            ->add('moderator', PickUserDynamicType::class, [
                'label' => 'Pick a moderator',
            ])
            ->add('location', PickUserLocationType::class, [
                'label' => 'event.fields.location',
            ])
            ->add('comment', CommentType::class, [
                'label' => 'Comment',
                'required' => false,
            ])
            ->add('documents', ChillCollectionType::class, [
                'entry_type' => StoredObjectType::class,
                'entry_options' => [
                    'has_title' => true,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => fn (StoredObject $storedObject): bool => '' === $storedObject->getFilename(),
                'button_remove_label' => 'event.form.remove_document',
                'button_add_label' => 'event.form.add_document',
            ])
            ->add('organizationCost', MoneyType::class, [
                'label' => 'event.fields.organizationCost',
                'help' => 'event.form.organisationCost_help',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
        $resolver
            ->setRequired(['center', 'role'])
            ->setAllowedTypes('center', Center::class)
            ->setAllowedTypes('role', 'string');
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_eventbundle_event';
    }
}
