<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\DependencyInjection;

use Chill\EventBundle\Security\EventVoter;
use Chill\EventBundle\Security\ParticipationVoter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillEventExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/forms.yaml');
        $loader->load('services/repositories.yaml');
        $loader->load('services/search.yaml');
        $loader->load('services/timeline.yaml');
        $loader->load('services/export.yaml');
    }

    /** (non-PHPdoc).
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoute($container);
    }

    /**
     * add authorization hierarchy.
     */
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                EventVoter::SEE_DETAILS => [EventVoter::SEE],
                EventVoter::UPDATE => [EventVoter::SEE_DETAILS],
                EventVoter::CREATE => [EventVoter::SEE_DETAILS],
                ParticipationVoter::SEE_DETAILS => [ParticipationVoter::SEE],
                ParticipationVoter::UPDATE => [ParticipationVoter::SEE_DETAILS],
            ],
        ]);
    }

    /**
     * add route to route loader for chill.
     */
    protected function prependRoute(ContainerBuilder $container)
    {
        // add routes for custom bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillEventBundle/config/routes.yaml',
                ],
            ],
        ]);
    }
}
