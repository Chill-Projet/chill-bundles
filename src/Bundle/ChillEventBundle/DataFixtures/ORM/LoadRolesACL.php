<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\DataFixtures\ORM;

use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Add roles to existing groups.
 */
class LoadRolesACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 30011;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef, PermissionsGroup::class);

            foreach (LoadScopes::$references as $scopeRef) {
                $scope = $this->getReference($scopeRef, Scope::class);
                // create permission group
                switch ($permissionsGroup->getName()) {
                    case 'social':
                        if ('administrative' === $scope->getName()['en']) {
                            break 2; // we do not want any power on administrative
                        }

                        break;

                    case 'administrative':
                    case 'direction':
                        if (\in_array($scope->getName()['en'], ['administrative', 'social'], true)) {
                            break 2; // we do not want any power on social or administrative
                        }

                        break;
                }

                printf(
                    'Adding CHILL_EVENT_UPDATE & CHILL_EVENT_CREATE '
                        .'& CHILL_EVENT_PARTICIPATION_UPDATE & CHILL_EVENT_PARTICIPATION_CREATE '
                        .'& CHILL_EVENT_SEE & CHILL_EVENT_SEE_DETAILS '
                        .'to %s '
                        ."permission group, scope '%s' \n",
                    $permissionsGroup->getName(),
                    $scope->getName()['en']
                );

                $roleScopeUpdate = (new RoleScope())
                    ->setRole('CHILL_EVENT_UPDATE')
                    ->setScope($scope);
                $roleScopeUpdate2 = (new RoleScope())
                    ->setRole('CHILL_EVENT_PARTICIPATION_UPDATE')
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeUpdate);
                $permissionsGroup->addRoleScope($roleScopeUpdate2);

                $roleScopeCreate = (new RoleScope())
                    ->setRole('CHILL_EVENT_CREATE')
                    ->setScope($scope);
                $roleScopeCreate2 = (new RoleScope())
                    ->setRole('CHILL_EVENT_PARTICIPATION_CREATE')
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreate);
                $permissionsGroup->addRoleScope($roleScopeCreate2);

                $roleScopeSee = (new RoleScope())
                    ->setRole('CHILL_EVENT_SEE')
                    ->setScope($scope);
                $roleScopeSee2 = (new RoleScope())
                    ->setRole('CHILL_EVENT_SEE_DETAILS')
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeSee);
                $permissionsGroup->addRoleScope($roleScopeSee2);

                $manager->persist($roleScopeUpdate);
                $manager->persist($roleScopeUpdate2);
                $manager->persist($roleScopeCreate);
                $manager->persist($roleScopeCreate2);
                $manager->persist($roleScopeSee);
                $manager->persist($roleScopeSee2);
            }
        }

        $manager->flush();
    }
}
