<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Controller;

use Chill\EventBundle\Entity\Role;
use Chill\EventBundle\Form\RoleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoleController.
 */
class RoleController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry) {}

    /**
     * Creates a new Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/create', name: 'chill_event_admin_role_create', methods: ['POST'])]
    public function createAction(Request $request)
    {
        $entity = new Role();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('chill_event_admin_role', ['id' => $entity->getId()]);
        }

        return $this->render('@ChillEvent/Role/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/{id}/delete', name: 'chill_event_admin_role_delete', methods: ['POST', 'DELETE'])]
    public function deleteAction(Request $request, mixed $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $entity = $em->getRepository(Role::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Role entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('chill_event_admin_role');
    }

    /**
     * Displays a form to edit an existing Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/{id}/edit', name: 'chill_event_admin_role_edit')]
    public function editAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Role::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/Role/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all Role entities.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/', name: 'chill_event_admin_role', options: [null])]
    public function indexAction()
    {
        $em = $this->managerRegistry->getManager();

        $entities = $em->getRepository(Role::class)->findAll();

        return $this->render('@ChillEvent/Role/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/new', name: 'chill_event_admin_role_new')]
    public function newAction()
    {
        $entity = new Role();
        $form = $this->createCreateForm($entity);

        return $this->render('@ChillEvent/Role/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/{id}/show', name: 'chill_event_admin_role_show')]
    public function showAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Role::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/Role/show.html.twig', [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing Role entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/role/{id}/update', name: 'chill_event_admin_role_update', methods: ['POST', 'PUT'])]
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Role::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('chill_event_admin_role', ['id' => $id]);
        }

        return $this->render('@ChillEvent/Role/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Role $entity)
    {
        $form = $this->createForm(RoleType::class, $entity, [
            'action' => $this->generateUrl('chill_event_admin_role_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to delete a Role entity by id.
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(mixed $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chill_event_admin_role_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, ['label' => 'Delete'])
            ->getForm();
    }

    /**
     * Creates a form to edit a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Role $entity)
    {
        $form = $this->createForm(RoleType::class, $entity, [
            'action' => $this->generateUrl(
                'chill_event_admin_role_update',
                ['id' => $entity->getId()]
            ),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
