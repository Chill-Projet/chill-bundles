<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Controller;

use Chill\EventBundle\Entity\Status;
use Chill\EventBundle\Form\StatusType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StatusController.
 */
class StatusController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry) {}

    /**
     * Creates a new Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/create', name: 'chill_event_admin_status_create', methods: ['POST'])]
    public function createAction(Request $request)
    {
        $entity = new Status();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('chill_event_admin_status', ['id' => $entity->getId()]);
        }

        return $this->render('@ChillEvent/Status/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/{id}/delete', name: 'chill_event_admin_status_delete', methods: ['POST', 'DELETE'])]
    public function deleteAction(Request $request, mixed $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $entity = $em->getRepository(Status::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Status entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('chill_event_admin_status');
    }

    /**
     * Displays a form to edit an existing Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/{id}/edit', name: 'chill_event_admin_status_edit')]
    public function editAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Status::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Status entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/Status/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all Status entities.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/', name: 'chill_event_admin_status', options: [null])]
    public function indexAction()
    {
        $em = $this->managerRegistry->getManager();

        $entities = $em->getRepository(Status::class)->findAll();

        return $this->render('@ChillEvent/Status/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/new', name: 'chill_event_admin_status_new')]
    public function newAction()
    {
        $entity = new Status();
        $form = $this->createCreateForm($entity);

        return $this->render('@ChillEvent/Status/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/{id}/show', name: 'chill_event_admin_status_show')]
    public function showAction(mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Status::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Status entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@ChillEvent/Status/show.html.twig', [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing Status entity.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/{_locale}/admin/event/status/{id}/update', name: 'chill_event_admin_status_update', methods: ['POST', 'PUT'])]
    public function updateAction(Request $request, mixed $id)
    {
        $em = $this->managerRegistry->getManager();

        $entity = $em->getRepository(Status::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Status entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('chill_event_admin_status', ['id' => $id]);
        }

        return $this->render('@ChillEvent/Status/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to create a Status entity.
     *
     * @param Status $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Status $entity)
    {
        $form = $this->createForm(StatusType::class, $entity, [
            'action' => $this->generateUrl('chill_event_admin_status_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to delete a Status entity by id.
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(mixed $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chill_event_admin_status_delete', ['id' => $id]))
            ->add('submit', SubmitType::class, ['label' => 'Delete'])
            ->getForm();
    }

    /**
     * Creates a form to edit a Status entity.
     *
     * @param Status $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Status $entity)
    {
        $form = $this->createForm(StatusType::class, $entity, [
            'action' => $this->generateUrl('chill_event_admin_status_update', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Update']);

        return $form;
    }
}
