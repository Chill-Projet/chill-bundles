<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\filters;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Export\Filter\EventDateFilter;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class EventDateFilterTest extends AbstractFilterTest
{
    private RollingDateConverterInterface $rollingDateConverter;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->rollingDateConverter = self::getContainer()->get(RollingDateConverterInterface::class);
    }

    public function getFilter()
    {
        return new EventDateFilter($this->rollingDateConverter);
    }

    public function getFormData()
    {
        return [
            [
                'date_from' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
                'date_to' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('event.id')
                ->from(Event::class, 'event'),
        ];
    }
}
