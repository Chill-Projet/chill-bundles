<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\filters;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Export\Filter\EventTypeFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class EventTypeFilterTest extends AbstractFilterTest
{
    private EventTypeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->filter = self::getContainer()->get(EventTypeFilter::class);
    }

    public function getFilter(): EventTypeFilter|\Chill\MainBundle\Export\FilterInterface
    {
        return $this->filter;
    }

    public function getFormData()
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(EventType::class, 'et')
            ->select('et')
            ->getQuery()
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'types' => new ArrayCollection([$a]),
            ];
        }

        return $data;
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('event.id')
                ->from(Event::class, 'event'),
        ];
    }
}
