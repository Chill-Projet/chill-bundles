<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\filters;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Entity\Participation;
use Chill\EventBundle\Entity\Role;
use Chill\EventBundle\Export\Filter\RoleFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class RoleFilterTest extends AbstractFilterTest
{
    private RoleFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::getContainer()->get(RoleFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(Role::class, 'r')
            ->select('r')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'roles' => new ArrayCollection([$a]),
            ];
        }

        return $data;
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('event.id')
                ->from(Event::class, 'event'),
            $em->createQueryBuilder()
                ->select('event_part')
                ->from(Participation::class, 'event_part'),
        ];
    }
}
