<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Tests\Export;

use Chill\EventBundle\Export\Export\CountEventParticipations;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CountEventParticipationsTest extends KernelTestCase
{
    private CountEventParticipations $countEventParticipations;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->countEventParticipations = self::getContainer()->get(CountEventParticipations::class);
    }

    public function testExecuteQuery(): void
    {
        $qb = $this->countEventParticipations->initiateQuery([], [], [])
            ->setMaxResults(1);

        $results = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        self::assertIsArray($results, 'smoke test: test that the result is an array');
    }
}
