<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Tests\Export;

use Chill\EventBundle\Export\Export\CountEvents;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CountEventTest extends KernelTestCase
{
    private CountEvents $countEvents;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->countEvents = self::getContainer()->get(CountEvents::class);
    }

    public function testExecuteQuery(): void
    {
        $qb = $this->countEvents->initiateQuery([], [], [])
            ->setMaxResults(1);

        $results = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        self::assertIsArray($results, 'smoke test: test that the result is an array');
    }
}
