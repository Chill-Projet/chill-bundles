<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\aggregators;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Entity\Participation;
use Chill\EventBundle\Export\Aggregator\RoleAggregator;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class RoleAggregatorTest extends AbstractAggregatorTest
{
    private $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::getContainer()->get(RoleAggregator::class);
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('event.id')
                ->from(Event::class, 'event'),
            $em->createQueryBuilder()
                ->select('event_part')
                ->from(Participation::class, 'event_part'),
        ];
    }
}
