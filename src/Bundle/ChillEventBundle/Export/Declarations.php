<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Export;

/**
 * This class declare constants used for the export framework.
 */
abstract class Declarations
{
    final public const EVENT = 'event';

    final public const EVENT_PARTICIPANTS = 'event_participants';
}
