<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Export\Aggregator;

use Chill\EventBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class EventDateAggregator implements AggregatorInterface
{
    private const CHOICES = [
        'by month' => 'month',
        'by week' => 'week',
        'by year' => 'year',
    ];

    private const DEFAULT_CHOICE = 'year';

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $order = null;

        switch ($data['frequency']) {
            case 'month':
                $fmt = 'YYYY-MM';

                break;

            case 'week':
                $fmt = 'YYYY-IW';

                break;

            case 'year':
                $fmt = 'YYYY';
                $order = 'DESC';

                break;

            default:
                throw new \RuntimeException(sprintf("The frequency data '%s' is invalid.", $data['frequency']));
        }

        $qb->addSelect(sprintf("TO_CHAR(event.date, '%s') AS date_aggregator", $fmt));
        $qb->addGroupBy('date_aggregator');
        $qb->addOrderBy('date_aggregator', $order);
    }

    public function applyOn(): string
    {
        return Declarations::EVENT;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('frequency', ChoiceType::class, [
            'choices' => self::CHOICES,
            'multiple' => false,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['frequency' => self::DEFAULT_CHOICE];
    }

    public function getLabels($key, array $values, $data)
    {
        return static function ($value) use ($data): string {
            if ('_header' === $value) {
                return 'by '.$data['frequency'];
            }

            if (null === $value) {
                return '';
            }

            return match ($data['frequency']) {
                default => $value,
            };
        };
    }

    public function getQueryKeys($data): array
    {
        return ['date_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group event by date';
    }
}
