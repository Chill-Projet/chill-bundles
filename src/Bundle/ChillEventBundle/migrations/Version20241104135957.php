<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Event;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241104135957 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Define default values for entity properties';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event ALTER organizationcost DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_user_history ALTER startdate SET DEFAULT \'now()\'');
        $this->addSql('ALTER TABLE chill_event_event ALTER organizationcost SET DEFAULT \'0.0\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event ALTER organizationCost SET DEFAULT \'0.0\'');
    }
}
