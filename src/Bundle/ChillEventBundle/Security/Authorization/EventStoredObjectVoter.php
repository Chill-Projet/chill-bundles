<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Security\Authorization;

use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectVoter\AbstractStoredObjectVoter;
use Chill\MainBundle\Workflow\Helper\WorkflowRelatedEntityPermissionHelper;
use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Repository\EventRepository;
use Chill\EventBundle\Security\EventVoter;
use Symfony\Component\Security\Core\Security;

class EventStoredObjectVoter extends AbstractStoredObjectVoter
{
    public function __construct(
        private readonly EventRepository $repository,
        Security $security,
        WorkflowRelatedEntityPermissionHelper $workflowDocumentService,
    ) {
        parent::__construct($security, $workflowDocumentService);
    }

    protected function getRepository(): AssociatedEntityToStoredObjectInterface
    {
        return $this->repository;
    }

    protected function getClass(): string
    {
        return Event::class;
    }

    protected function attributeToRole(StoredObjectRoleEnum $attribute): string
    {
        return match ($attribute) {
            StoredObjectRoleEnum::EDIT => EventVoter::UPDATE,
            StoredObjectRoleEnum::SEE => EventVoter::SEE_DETAILS,
        };
    }

    protected function canBeAssociatedWithWorkflow(): bool
    {
        return false;
    }
}
