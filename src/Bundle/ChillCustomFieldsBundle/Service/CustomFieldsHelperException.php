<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Service;

class CustomFieldsHelperException extends \Exception
{
    public static function customFieldsGroupNotFound($entity)
    {
        return new CustomFieldsHelperException("The customFieldsGroups associated with {$entity} are not found");
    }

    public static function slugIsMissing()
    {
        return new CustomFieldsHelperException('The slug is missing');
    }
}
