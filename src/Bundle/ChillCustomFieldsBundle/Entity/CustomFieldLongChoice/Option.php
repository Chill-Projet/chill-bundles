<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: \Chill\CustomFieldsBundle\EntityRepository\CustomFieldLongChoice\OptionRepository::class)]
#[ORM\Table(name: 'custom_field_long_choice_options')]
class Option
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $active = true;

    /**
     * @var Collection<int, Option>
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: Option::class)]
    private Collection $children;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 50, name: 'internal_key', options: ['default' => ''])]
    private string $internalKey = '';

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 15)]
    private ?string $key = null;

    #[ORM\ManyToOne(targetEntity: Option::class, inversedBy: 'children')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Option $parent = null;

    /**
     * A json representation of text (multilingual).
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    private ?array $text = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->isActive();
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInternalKey()
    {
        return $this->internalKey;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return Option
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return array
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return null === $this->parent ? false : true;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return $this
     */
    public function setInternalKey(string $internal_key)
    {
        $this->internalKey = $internal_key;

        return $this;
    }

    /**
     * @return $this
     */
    public function setKey(?string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return $this
     */
    public function setParent(?Option $parent = null)
    {
        $this->parent = $parent;
        $this->key = $parent->getKey();

        return $this;
    }

    /**
     * @return $this
     */
    public function setText(array $text)
    {
        $this->text = $text;

        return $this;
    }
}
