<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CustomFieldGroup.
 */
#[ORM\Entity]
#[ORM\Table(name: 'customfieldsgroup')]
class CustomFieldsGroup
{
    /**
     * The custom fields of the group that are active.
     * This variable if null, if this informations has not been computed.
     */
    private ?array $activeCustomFields = null;

    /**
     * The custom fields of the group.
     * The custom fields are asc-ordered regarding to their property "ordering".
     *
     * @var Collection<int, CustomField>
     */
    #[ORM\OneToMany(mappedBy: 'customFieldGroup', targetEntity: CustomField::class)]
    #[ORM\OrderBy(['ordering' => \Doctrine\Common\Collections\Criteria::ASC])]
    private Collection $customFields;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private ?string $entity = null;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    private array|string $name;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, nullable: true, options: ['default' => '{}', 'jsonb' => true])]
    private array $options = [];

    /**
     * CustomFieldsGroup constructor.
     */
    public function __construct()
    {
        $this->customFields = new ArrayCollection();
    }

    /**
     * Add customField.
     *
     * @return CustomFieldsGroup
     */
    public function addCustomField(CustomField $customField)
    {
        $this->customFields[] = $customField;

        return $this;
    }

    /**
     * Get all the custom.
     */
    public function getActiveCustomFields(): array
    {
        if (null === $this->activeCustomFields) {
            $this->activeCustomFields = [];

            foreach ($this->customFields as $cf) {
                if ($cf->isActive()) {
                    $this->activeCustomFields[] = $cf;
                }
            }
        }

        return $this->activeCustomFields;
    }

    /**
     * @return Collection
     */
    public function getCustomFields()
    {
        return $this->customFields;
    }

    /**
     * Get entity.
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     */
    public function getName(?string $language = null): array|string
    {
        // TODO set this in a service, PLUS twig function
        if (null !== $language) {
            if (isset($this->name[$language])) {
                return $this->name[$language];
            }

            foreach ($this->name as $name) {
                if (!empty($name)) {
                    return $name;
                }
            }

            return '';
        }

        return $this->name;
    }

    /**
     * get options array.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Remove customField.
     */
    public function removeCustomField(CustomField $customField)
    {
        $this->customFields->removeElement($customField);
    }

    /**
     * Set entity.
     *
     * @return CustomFieldsGroup
     */
    public function setEntity(?string $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Set name.
     *
     * @param array $name
     *
     * @return CustomFieldsGroup
     */
    public function setName(array|string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * set options array.
     *
     * @return CustomFieldsGroup
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }
}
