<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * Controller for the custom fields configuration section (in admin section).
 */
class AdminController extends AbstractController
{
    #[Route(path: '/{_locale}/admin/customfield/', name: 'customfield_section')]
    public function indexAction(): Response
    {
        return $this->render('@ChillCustomFields/Admin/layout.html.twig');
    }
}
