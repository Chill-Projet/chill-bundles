<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomFieldsTitleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {}

    public function getBlockPrefix()
    {
        return 'custom_field_title';
    }
}
