<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;

/**
 * This class add the PostTextExtension to integer fields.
 */
class PostTextIntegerExtension extends PostTextExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [IntegerType::class];
    }
}
