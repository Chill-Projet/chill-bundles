<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form\DataTransformer;

use Chill\CustomFieldsBundle\CustomFields\CustomFieldInterface;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Symfony\Component\Form\DataTransformerInterface;

class CustomFieldDataTransformer implements DataTransformerInterface
{
    public function __construct(private readonly CustomFieldInterface $customFieldDefinition, private readonly CustomField $customField) {}

    public function reverseTransform($value)
    {
        return $this->customFieldDefinition->serialize(
            $value,
            $this->customField
        );
    }

    public function transform($value)
    {
        return $this->customFieldDefinition->deserialize(
            $value,
            $this->customField
        );
    }
}
