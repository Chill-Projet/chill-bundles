<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\CustomFields;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Twig\Environment;

class CustomFieldText extends AbstractCustomField
{
    final public const MAX_LENGTH = 'maxLength';

    final public const MULTIPLE_CF_INLINE = 'multipleCFInline';

    public function __construct(
        private readonly Environment $templating,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {}

    /**
     * Create a form according to the maxLength option.
     *
     * if maxLength < 256 THEN the form type is 'text'
     * if not, THEN the form type is textarea
     */
    public function buildForm(FormBuilderInterface $builder, CustomField $customField)
    {
        $options = $customField->getOptions();

        $type = (256 > $options[self::MAX_LENGTH]) ? TextType::class
              : TextareaType::class;

        $attrArray = [];

        if (
            \array_key_exists(self::MULTIPLE_CF_INLINE, $options)
            && $options[self::MULTIPLE_CF_INLINE]
        ) {
            $attrArray['class'] = 'multiple-cf-inline';
        }

        $builder->add($customField->getSlug(), $type, [
            'label' => $this->translatableStringHelper->localize($customField->getName()),
            'required' => false,
            'attr' => $attrArray,
        ]);
    }

    public function buildOptionsForm(FormBuilderInterface $builder)
    {
        return $builder
            ->add(self::MAX_LENGTH, IntegerType::class, ['empty_data' => 256])
            ->add(self::MULTIPLE_CF_INLINE, ChoiceType::class, [
                'choices' => [
                    'Multiple boxes on the line' => '1',
                    'One box on the line' => '0',
                ],
                'label' => 'Box appearance',
                'expanded' => true,
            ]);
    }

    public function deserialize($serialized, CustomField $customField)
    {
        return $serialized;
    }

    public function getName()
    {
        return 'Text field';
    }

    public function render($value, CustomField $customField, $documentType = 'html')
    {
        $template = '@ChillCustomFields/CustomFieldsRendering/text.html.twig';

        if ('csv' === $documentType) {
            $template = '@ChillCustomFields/CustomFieldsRendering/text.csv.twig';
        }

        return $this->templating
            ->render($template, ['text' => $value]);
    }

    public function serialize($value, CustomField $customField)
    {
        return $value;
    }
}
