<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\CustomFields;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add an option column to customfieldsgroup.
 */
class Version20150224164531 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE CustomFieldsGroup DROP options');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE customfieldsgroup ADD options JSON DEFAULT \'{}\'::json');
    }
}
