module.exports = function (encore, entries) {
  // Aliases are used when webpack is trying to resolve modules path
  encore.addAliases({
    ChillThirdPartyAssets: __dirname + "/Resources/public",
  });

  entries.push(__dirname + "/Resources/public/chill/index.js");
};
