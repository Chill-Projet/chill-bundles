<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Add a constraint to check that the thirdparty has an email address.
 */
class ThirdPartyHasEmail extends Constraint
{
    public string $message = 'thirdParty.thirdParty_has_no_email';
    public string $code = 'f2da71f8-818c-11ef-9f6a-3ba3597ab60b';

    /**
     * @return array<string>|string
     */
    public function getTargets(): array|string
    {
        return [self::PROPERTY_CONSTRAINT];
    }
}
