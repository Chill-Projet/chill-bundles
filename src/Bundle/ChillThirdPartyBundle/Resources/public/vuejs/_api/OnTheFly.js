/*
 * GET a thirdparty by id
 */
const getThirdparty = (id) => {
  const url = `/api/1.0/thirdparty/thirdparty/${id}.json`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * POST a new thirdparty
 */
const postThirdparty = (body) => {
  const url = `/api/1.0/thirdparty/thirdparty.json`;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * PATCH an existing thirdparty
 */
const patchThirdparty = (id, body) => {
  const url = `/api/1.0/thirdparty/thirdparty/${id}.json`;
  return fetch(url, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

export { getThirdparty, postThirdparty, patchThirdparty };
