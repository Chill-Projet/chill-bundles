const thirdpartyMessages = {
  fr: {
    thirdparty: {
      firstname: "Prénom",
      lastname: "Nom",
      name: "Dénomination",
      email: "Courriel",
      phonenumber: "Téléphone",
      comment: "Commentaire",
      profession: "Qualité",
      civility: "Civilité",
    },
    child_of: "Contact de: ",
    children: "Personnes de contact: ",
  },
};

export { thirdpartyMessages };
