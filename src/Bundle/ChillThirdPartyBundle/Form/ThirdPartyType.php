<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Form;

use Chill\MainBundle\Form\Type\ChillCollectionType;
use Chill\MainBundle\Form\Type\ChillPhoneNumberType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Chill\MainBundle\Form\Type\PickAddressType;
use Chill\MainBundle\Form\Type\PickCenterType;
use Chill\MainBundle\Form\Type\PickCivilityType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Form\Type\PickThirdPartyTypeCategoryType;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ThirdPartyType extends AbstractType
{
    private readonly bool $askCenter;

    public function __construct(
        protected AuthorizationHelper $authorizationHelper,
        protected TokenStorageInterface $tokenStorage,
        protected ThirdPartyTypeManager $typesManager,
        protected TranslatableStringHelper $translatableStringHelper,
        protected EntityManagerInterface $om,
        ParameterBagInterface $parameterBag,
    ) {
        $this->askCenter = $parameterBag->get('chill_main')['acl']['form_show_centers'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('telephone', ChillPhoneNumberType::class, [
                'label' => 'Phonenumber',
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('comment', ChillTextareaType::class, [
                'required' => false,
            ]);

        if ($this->askCenter) {
            $builder
                ->add('centers', PickCenterType::class, [
                    'role' => (\array_key_exists('data', $options) && $this->om->contains($options['data'])) ?
                        ThirdPartyVoter::UPDATE : ThirdPartyVoter::CREATE,
                    'choice_options' => [
                        'multiple' => true,
                        'attr' => ['class' => 'select2'],
                    ],
                ]);
        }

        // Contact Person ThirdParty (child)
        if (ThirdParty::KIND_CONTACT === $options['kind'] || ThirdParty::KIND_CHILD === $options['kind']) {
            $builder
                ->add('firstname', TextType::class, [
                    'label' => 'firstname',
                    'required' => false,
                ])
                ->add('civility', PickCivilityType::class, [
                    'label' => 'thirdparty.Civility',
                    'placeholder' => 'thirdparty.choose civility',
                    'required' => false,
                ])
                ->add('profession', TextType::class, [
                    'label' => 'thirdparty.Profession',
                    'required' => false,
                    'empty_data' => '',
                ])
                ->add('contactDataAnonymous', CheckboxType::class, [
                    'required' => false,
                    'label' => 'thirdparty.Contact data are confidential',
                ]);

            // Institutional ThirdParty (parent)
        } else {
            $builder
                ->add('nameCompany', TextType::class, [
                    'label' => 'thirdparty.NameCompany',
                    'required' => false,
                ])
                ->add('acronym', TextType::class, [
                    'label' => 'thirdparty.Acronym',
                    'required' => false,
                ])
                ->add('activeChildren', ChillCollectionType::class, [
                    'entry_type' => ThirdPartyType::class,
                    'entry_options' => [
                        'is_child' => true,
                        'block_name' => 'children',
                        'kind' => ThirdParty::KIND_CHILD,
                    ],
                    'block_name' => 'active_children',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'button_add_label' => 'Add a contact',
                    'button_remove_label' => 'Remove a contact',
                    'empty_collection_explain' => 'No contacts associated',
                ]);
        }

        if (ThirdParty::KIND_CHILD !== $options['kind']) {
            $builder
                ->add('address', PickAddressType::class, [
                    'label' => 'Address',
                ])
                ->add('typesAndCategories', PickThirdPartyTypeCategoryType::class, [
                    'label' => 'thirdparty.Categories',
                ])
                ->add('active', ChoiceType::class, [
                    'label' => 'thirdparty.Status',
                    'choices' => [
                        'Active, shown to users' => true,
                        'Inactive, not shown to users' => false,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ThirdParty::class,
            'is_child' => false,
            'kind' => null,
        ]);
    }
}
