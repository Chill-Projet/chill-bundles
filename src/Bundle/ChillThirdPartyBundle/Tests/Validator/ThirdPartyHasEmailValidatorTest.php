<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace ChillThirdParty\Tests\Validator;

use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Chill\ThirdPartyBundle\Validator\ThirdPartyHasEmail;
use Chill\ThirdPartyBundle\Validator\ThirdPartyHasEmailValidator;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class ThirdPartyHasEmailValidatorTest extends \Symfony\Component\Validator\Test\ConstraintValidatorTestCase
{
    use ProphecyTrait;

    public function testThirdPartyWithEmail(): void
    {
        $thirdParty = new ThirdParty();
        $thirdParty->setEmail('third@example.com');

        $this->validator->validate($thirdParty, new ThirdPartyHasEmail());

        $this->assertNoViolation();
    }

    public function testThirdPartyHasNoEmail(): void
    {
        $thirdParty = new ThirdParty();

        $constraint = new ThirdPartyHasEmail();
        $constraint->message = 'message';

        $this->validator->validate($thirdParty, $constraint);

        $this->buildViolation($constraint->message)
            ->setParameter('{{ thirdParty }}', '3party-string')
            ->setCode($constraint->code)
            ->assertRaised();
    }

    protected function createValidator(): ThirdPartyHasEmailValidator
    {
        $render = $this->prophesize(ThirdPartyRender::class);
        $render->renderString(Argument::type(ThirdParty::class), [])
            ->willReturn('3party-string');

        return new ThirdPartyHasEmailValidator($render->reveal());
    }
}
