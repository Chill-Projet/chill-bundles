<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Tests\Serializer\Normalizer;

use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ThirdPartyJsonDenormalizerTest extends KernelTestCase
{
    private DenormalizerInterface $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->normalizer = self::getContainer()->get(DenormalizerInterface::class);
    }

    public function testDenormalizeContact()
    {
        $str = <<<'JSON'
            {
              "type": "thirdparty",
              "name": "badaboum",
              "email": "badaboum@email.com",
              "telephone": "+32486540660",
              "kind": "contact"
            }
            JSON;

        $actual = $this->normalizer->denormalize(json_decode($str, true), ThirdParty::class, 'json', [
            'groups' => ['write'],
        ]);

        $this->assertInstanceOf(ThirdParty::class, $actual);
        $this->assertEquals('badaboum', $actual->getName());
        $this->assertEquals('badaboum@email.com', $actual->getEmail());
        $this->assertEquals(ThirdParty::KIND_CONTACT, $actual->getKind());
    }
}
