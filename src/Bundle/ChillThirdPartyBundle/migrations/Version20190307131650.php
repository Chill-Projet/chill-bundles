<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Move table third-party to another schema.
 */
final class Version20190307131650 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->throwIrreversibleMigrationException('The down version of this migration is '
            .'not written');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA chill_3party');
        $this->addSql('ALTER TABLE chill_third_party SET SCHEMA chill_3party');
        $this->addSql('ALTER TABLE chill_3party.chill_third_party RENAME TO third_party');
        $this->addSql('ALTER SEQUENCE chill_third_party_id_seq SET SCHEMA chill_3party');
        $this->addSql('ALTER SEQUENCE chill_3party.chill_third_party_id_seq RENAME TO third_party_id_seq');
    }
}
