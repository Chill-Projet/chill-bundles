<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918152229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Drop foreign key profession_id and set not null on firstname';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party DROP profession_id');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER firstname SET NOT NULL');
    }

    public function down(Schema $schema): void {}
}
