<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\DataFixtures\ORM;

use Chill\ThirdPartyBundle\Entity\ThirdPartyProfession;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class LoadThirdPartyProfession.
 */
class LoadThirdPartyProfession extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['thirdparty_professions'];
    }

    public function load(ObjectManager $manager): void
    {
        $professions = [
            ['name' => ['fr' => 'Directeur']],
            ['name' => ['fr' => 'Docteur']],
            ['name' => ['fr' => 'Médecin']],
            ['name' => ['fr' => 'Opérateur']],
            ['name' => ['fr' => 'Personnel administratif']],
            ['name' => ['fr' => 'Président']],
            ['name' => ['fr' => 'Responsable infirmier.ère']],
        ];

        foreach ($professions as $val) {
            echo 'Creating thirdparty professions : '.$val['name']['fr']."\n";
            $profession = (new ThirdPartyProfession())
                ->setName($val['name'])
                ->setActive(true);
            $manager->persist($profession);
        }

        $manager->flush();
    }
}
