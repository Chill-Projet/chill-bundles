<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Export\Helper;

use Chill\ThirdPartyBundle\Repository\ThirdPartyRepository;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;

class LabelThirdPartyHelper
{
    public function __construct(private readonly ThirdPartyRender $thirdPartyRender, private readonly ThirdPartyRepository $thirdPartyRepository) {}

    public function getLabel(string $key, array $values, string $header): callable
    {
        return function ($value) use ($header) {
            if ('_header' === $value) {
                return $header;
            }

            if ('' === $value || null === $value || null === $thirdParty = $this->thirdPartyRepository->find($value)) {
                return '';
            }

            return $this->thirdPartyRender->renderString($thirdParty, []);
        };
    }

    public function getLabelMulti(string $key, array $values, string $header): callable
    {
        return function ($value) use ($header) {
            if ('_header' === $value) {
                return $header;
            }

            if (null === $value) {
                return '';
            }

            $decoded = json_decode((string) $value, null, 512, JSON_THROW_ON_ERROR);

            if (0 === \count($decoded)) {
                return '';
            }

            return
                implode(
                    '|',
                    array_map(
                        function (int $tpId) {
                            $tp = $this->thirdPartyRepository->find($tpId);

                            if (null === $tp) {
                                return '';
                            }

                            return $this->thirdPartyRender->renderString($tp, []);
                        },
                        array_unique(
                            array_filter($decoded, static fn (?int $id) => null !== $id),
                            \SORT_NUMERIC
                        )
                    )
                );
        };
    }
}
