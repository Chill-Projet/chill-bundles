<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\BoxUtilsChillEntityRenderTrait;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\ThirdPartyBundle\Entity\ThirdParty;

/**
 * @implements ChillEntityRenderInterface<ThirdParty>
 */
class ThirdPartyRender implements ChillEntityRenderInterface
{
    use BoxUtilsChillEntityRenderTrait;

    public function __construct(protected \Twig\Environment $engine, protected TranslatableStringHelper $translatableStringHelper) {}

    public function renderBox($entity, array $options): string
    {
        $params = [
            'with_valid_from' => $options['with_valid_from'] ?? true,
            'addLink' => $options['addLink'] ?? false,
            'addEntity' => $options['addEntity'] ?? false,
            'addId' => $options['addId'] ?? false,
            'addInfo' => $options['addInfo'] ?? false,
            'hLevel' => $options['hLevel'] ?? 3,
            'customButtons' => $options['customButtons'] ?? [],
            'customArea' => $options['customArea'] ?? [],
            'showContacts' => $options['showContacts'] ?? false,
            'showParent' => $options['showParent'] ?? true,
            'isConfidential' => $options['isConfidential'] ?? false,
        ];

        return
            $this->getDefaultOpeningBox('thirdparty').
            $this->engine->render('@ChillThirdParty/Entity/thirdparty.html.twig', [
                'thirdparty' => $entity,
                'render' => $options['render'] ?? 'raw',
                'options' => $params,
            ]).
            $this->getDefaultClosingBox();
    }

    public function renderString($entity, array $options): string
    {
        if (null !== $entity->getCivility()) {
            $civility = $this->translatableStringHelper
                ->localize($entity->getCivility()->getAbbreviation()).' ';
        } else {
            $civility = '';
        }

        if ('' !== (string) $entity->getAcronym()) {
            $acronym = ' ('.$entity->getAcronym().')';
        } else {
            $acronym = '';
        }

        $firstname = ('' === $entity->getFirstname()) ? '' : $entity->getFirstname();

        return $civility.$firstname.' '.$entity->getName().$acronym;
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof ThirdParty;
    }
}
