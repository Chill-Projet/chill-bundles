<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class WopiEditTwigExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('chill_document_edit_button', [WopiEditTwigExtensionRuntime::class, 'renderEditButton'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
            new TwigFilter('chill_document_button_group', [WopiEditTwigExtensionRuntime::class, 'renderButtonGroup'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
            new TwigFilter('chill_document_download_only_button', [WopiEditTwigExtensionRuntime::class, 'renderDownloadButton'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('chill_document_is_editable', [WopiEditTwigExtensionRuntime::class, 'isEditable']),
        ];
    }
}
