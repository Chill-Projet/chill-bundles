<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final class AsyncFileExists extends Constraint
{
    public string $message = "The file '{{ filename }}' is not stored properly.";

    public function __construct(?array $options = null, ?string $message = null, ?array $groups = null, $payload = null)
    {
        parent::__construct($options ?? [], $groups, $payload);

        if (null !== $message) {
            $this->message = $message;
        }
    }

    public function validatedBy()
    {
        return AsyncFileExistsValidator::class;
    }

    public function getTargets()
    {
        return [Constraint::CLASS_CONSTRAINT, Constraint::PROPERTY_CONSTRAINT];
    }
}
