<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210903123835 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F6369A0BE36EF62EFC');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F6232D562B');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F6682B5931');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F6A76ED395');
        $this->addSql('DROP INDEX IDX_A45098F6369A0BE36EF62EFC');
        $this->addSql('DROP INDEX IDX_A45098F6232D562B');
        $this->addSql('DROP INDEX IDX_A45098F6682B5931');
        $this->addSql('DROP INDEX IDX_A45098F6A76ED395');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP category_bundle_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP category_id_inside_bundle');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP object_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP scope_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP user_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP title');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP description');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP date');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD category_bundle_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD category_id_inside_bundle INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD object_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD scope_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD title TEXT NOT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD description TEXT NOT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F6369A0BE36EF62EFC FOREIGN KEY (category_bundle_id, category_id_inside_bundle) REFERENCES chill_doc.document_category (bundle_id, id_inside_bundle) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F6232D562B FOREIGN KEY (object_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F6682B5931 FOREIGN KEY (scope_id) REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F6A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A45098F6369A0BE36EF62EFC ON chill_doc.accompanyingcourse_document (category_bundle_id, category_id_inside_bundle)');
        $this->addSql('CREATE INDEX IDX_A45098F6232D562B ON chill_doc.accompanyingcourse_document (object_id)');
        $this->addSql('CREATE INDEX IDX_A45098F6682B5931 ON chill_doc.accompanyingcourse_document (scope_id)');
        $this->addSql('CREATE INDEX IDX_A45098F6A76ED395 ON chill_doc.accompanyingcourse_document (user_id)');
    }
}
