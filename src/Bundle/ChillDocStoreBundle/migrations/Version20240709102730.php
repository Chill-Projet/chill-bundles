<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240709102730 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add versioning to stored objects';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_doc.stored_object_version_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            <<<'SQL'
                CREATE TABLE chill_doc.stored_object_version (
                    id INT NOT NULL,
                    stored_object_id INT NOT NULL,
                    version INT DEFAULT 0 NOT NULL,
                    filename TEXT NOT NULL,
                    iv JSON NOT NULL,
                    key JSON NOT NULL,
                    type TEXT DEFAULT '' NOT NULL,
                    createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
                    createdBy_id INT DEFAULT NULL,
                    PRIMARY KEY(id))
                SQL
        );
        $this->addSql('CREATE INDEX IDX_C1D55302232D562B ON chill_doc.stored_object_version (stored_object_id)');
        $this->addSql('CREATE INDEX IDX_C1D553023174800F ON chill_doc.stored_object_version (createdBy_id)');
        $this->addSql('CREATE UNIQUE INDEX chill_doc_stored_object_version_unique_by_object ON chill_doc.stored_object_version (stored_object_id, version)');
        $this->addSql('COMMENT ON COLUMN chill_doc.stored_object_version.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object_version ADD CONSTRAINT FK_C1D55302232D562B FOREIGN KEY (stored_object_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.stored_object_version ADD CONSTRAINT FK_C1D553023174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql(
            <<<'SQL'
            INSERT INTO chill_doc.stored_object_version (id, stored_object_id, version, filename, iv, key, type)
                SELECT nextval('chill_doc.stored_object_version_id_seq'), id, 1, filename, iv, key, type FROM chill_doc.stored_object
            SQL
        );
        $this->addSql('ALTER TABLE chill_doc.stored_object RENAME COLUMN filename TO prefix');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP key');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP iv');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP type');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER title SET DEFAULT \'\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_doc.stored_object_version_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_doc.stored_object RENAME COLUMN prefix TO filename');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD type TEXT NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD key JSON NOT NULL DEFAULT \'{}\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD iv JSON NOT NULL DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER title DROP DEFAULT');
        $this->addSql(
            <<<'SQL'
            UPDATE chill_doc.stored_object SET filename=sov.filename, type=sov.type, iv=sov.iv, key=sov.key
            FROM chill_doc.stored_object_version sov WHERE sov.stored_object_id = stored_object.id
                AND sov.version = (SELECT MAX(version) FROM chill_doc.stored_object_version AS sub_sov WHERE sub_sov.stored_object_id = stored_object.id)
        SQL
        );
        $this->addSql('ALTER TABLE chill_doc.stored_object_version DROP CONSTRAINT FK_C1D55302232D562B');
        $this->addSql('ALTER TABLE chill_doc.stored_object_version DROP CONSTRAINT FK_C1D553023174800F');
        $this->addSql('DROP TABLE chill_doc.stored_object_version');
    }
}
