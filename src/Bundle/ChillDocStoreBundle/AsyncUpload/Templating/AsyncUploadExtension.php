<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload\Templating;

use Chill\DocStoreBundle\AsyncUpload\TempUrlGeneratorInterface;
use Chill\DocStoreBundle\Entity\StoredObject;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * This class extends the AbstractExtension class and provides Twig filter functions for generating URLs for asynchronous
 * file uploads.
 */
class AsyncUploadExtension extends AbstractExtension
{
    public function __construct(
        private readonly TempUrlGeneratorInterface $tempUrlGenerator,
        private readonly UrlGeneratorInterface $routingUrlGenerator,
    ) {}

    public function getFilters()
    {
        return [
            new TwigFilter('file_url', $this->computeSignedUrl(...)),
            new TwigFilter('generate_url', $this->computeGenerateUrl(...)),
        ];
    }

    public function computeSignedUrl(StoredObject|string $file, string $method = 'GET', ?int $expiresDelay = null): string
    {
        if ($file instanceof StoredObject) {
            $object_name = $file->getFilename();
        } else {
            $object_name = $file;
        }

        return $this->tempUrlGenerator->generate($method, $object_name, $expiresDelay)->url;
    }

    public function computeGenerateUrl(StoredObject|string $file, string $method = 'GET', ?int $expiresDelay = null): string
    {
        if ($file instanceof StoredObject) {
            $object_name = $file->getFilename();
        } else {
            $object_name = $file;
        }

        $args = [
            'method' => $method,
            'object_name' => $object_name,
        ];

        if (null !== $expiresDelay) {
            $args['expires_delay'] = $expiresDelay;
        }

        return $this->routingUrlGenerator->generate('async_upload.generate_url', $args);
    }
}
