<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload\Exception;

class TempUrlRemoteServerException extends \RuntimeException
{
    public function __construct(int $statusCode, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct('Could not reach remote server: '.(string) $statusCode, $code, $previous);
    }
}
