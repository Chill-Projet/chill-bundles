<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload\Driver\OpenstackObjectStore;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ConfigureOpenstackObjectStorageCommand extends Command
{
    protected static $defaultDescription = 'Configure openstack container to store documents';
    private readonly string $basePath;

    private readonly string $tempUrlKey;

    public function __construct(private readonly HttpClientInterface $client, ParameterBagInterface $parameterBag)
    {
        $config = $parameterBag->get('chill_doc_store')['openstack']['temp_url'];

        $this->tempUrlKey = $config['temp_url_key'];
        $this->basePath = $config['temp_url_base_path'];

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chill:doc-store:configure-openstack')
            ->addOption('os_token', 'o', InputOption::VALUE_REQUIRED, 'Openstack token')
            ->addOption('domain', 'd', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'Domain name')
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->hasOption('os_token')) {
            $output->writeln('The option os_token is required');

            throw new \RuntimeException('The option os_token is required');
        }

        if (0 === count($input->getOption('domain'))) {
            $output->writeln('At least one occurence of option domain is required');

            throw new \RuntimeException('At least one occurence of option domain is required');
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domains = trim(implode(' ', $input->getOption('domain')));

        if ($output->isVerbose()) {
            $output->writeln(['Domains configured will be', $domains]);
        }

        try {
            $response = $this->client->request('POST', $this->basePath, [
                'headers' => [
                    'X-Auth-Token' => $input->getOption('os_token'),
                    'X-Container-Meta-Access-Control-Allow-Origin' => $domains,
                    'X-Container-Meta-Temp-URL-Key' => $this->tempUrlKey,
                ],
            ]);
            $response->getContent();
        } catch (HttpExceptionInterface $e) {
            $output->writeln('Error');
            $output->writeln($e->getMessage());

            if ($output->isVerbose()) {
                $output->writeln($e->getTraceAsString());
            }
        }

        return Command::SUCCESS;
    }
}
