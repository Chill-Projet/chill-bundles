<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload;

use Symfony\Component\Serializer\Annotation as Serializer;

readonly class SignedUrlPost extends SignedUrl
{
    public function __construct(
        string $url,
        \DateTimeImmutable $expires,
        string $object_name,
        #[Serializer\Groups(['read'])]
        public int $max_file_size,
        #[Serializer\Groups(['read'])]
        public int $max_file_count,
        #[Serializer\Groups(['read'])]
        public int $submit_delay,
        #[Serializer\Groups(['read'])]
        public string $redirect,
        #[Serializer\Groups(['read'])]
        public string $prefix,
        #[Serializer\Groups(['read'])]
        public string $signature,
    ) {
        parent::__construct('POST', $url, $expires, $object_name);
    }
}
