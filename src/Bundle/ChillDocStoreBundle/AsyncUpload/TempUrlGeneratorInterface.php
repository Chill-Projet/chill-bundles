<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload;

interface TempUrlGeneratorInterface
{
    public function generatePost(
        ?int $expire_delay = null,
        ?int $submit_delay = null,
        int $max_file_count = 1,
        ?string $object_name = null,
    ): SignedUrlPost;

    public function generate(string $method, string $object_name, ?int $expire_delay = null): SignedUrl;
}
