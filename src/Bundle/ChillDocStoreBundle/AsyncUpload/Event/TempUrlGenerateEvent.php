<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload\Event;

use Chill\DocStoreBundle\AsyncUpload\SignedUrl;

final class TempUrlGenerateEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const NAME_GENERATE = 'async_uploader.generate_url';

    public function __construct(private readonly SignedUrl $data) {}

    public function getMethod(): string
    {
        return $this->data->method;
    }

    public function getData(): SignedUrl
    {
        return $this->data;
    }
}
