<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\AsyncUpload;

use Symfony\Component\Serializer\Annotation as Serializer;

readonly class SignedUrl
{
    public function __construct(
        #[Serializer\Groups(['read'])]
        public string $method,
        #[Serializer\Groups(['read'])]
        public string $url,
        public \DateTimeImmutable $expires,
        #[Serializer\Groups(['read'])]
        public string $object_name,
    ) {}

    #[Serializer\Groups(['read'])]
    public function getExpires(): int
    {
        return $this->expires->getTimestamp();
    }
}
