module.exports = function (encore) {
  encore.addAliases({
    ChillDocStoreAssets: __dirname + "/Resources/public",
  });
  encore.addEntry(
    "mod_async_upload",
    __dirname + "/Resources/public/module/async_upload/index.ts",
  );
  encore.addEntry(
    "mod_document_action_buttons_group",
    __dirname + "/Resources/public/module/document_action_buttons_group/index",
  );
  encore.addEntry(
    "mod_document_download_button",
    __dirname + "/Resources/public/module/button_download/index",
  );
  encore.addEntry(
    "vue_document_signature",
    __dirname + "/Resources/public/vuejs/DocumentSignature/index",
  );
};
