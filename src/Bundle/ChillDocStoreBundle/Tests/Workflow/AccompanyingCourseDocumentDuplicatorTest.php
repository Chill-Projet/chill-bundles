<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Workflow;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Service\StoredObjectDuplicate;
use Chill\DocStoreBundle\Workflow\AccompanyingCourseDocumentDuplicator;
use Chill\MainBundle\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Clock\MockClock;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingCourseDocumentDuplicatorTest extends TestCase
{
    public function testDuplicate(): void
    {
        $object = new StoredObject();

        $document = new AccompanyingCourseDocument();
        $document
            ->setDate($date = new \DateTimeImmutable())
            ->setObject($object)
            ->setTitle('Title')
            ->setUser($user = new User())
            ->setCategory($category = new DocumentCategory('bundle', 10))
            ->setDescription($description = 'Description');

        $actual = $this->buildDuplicator()->duplicate($document);

        self::assertSame($date, $actual->getDate());
        // FYI, the duplication of object is checked by the mock
        self::assertNotNull($actual->getObject());
        self::assertStringStartsWith('Title', $actual->getTitle());
        self::assertSame($user, $actual->getUser());
        self::assertSame($category, $actual->getCategory());
        self::assertEquals($description, $actual->getDescription());
    }

    private function buildDuplicator(): AccompanyingCourseDocumentDuplicator
    {
        $storedObjectDuplicate = $this->createMock(StoredObjectDuplicate::class);
        $storedObjectDuplicate->expects($this->once())->method('duplicate')
            ->with($this->isInstanceOf(StoredObject::class))->willReturn(new StoredObject());
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->method('trans')->withAnyParameters()->willReturn('duplicated');
        $clock = new MockClock();

        return new AccompanyingCourseDocumentDuplicator(
            $storedObjectDuplicate,
            $translator,
            $clock
        );
    }
}
