<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Workflow;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Repository\AccompanyingCourseDocumentRepository;
use Chill\DocStoreBundle\Workflow\AccompanyingCourseDocumentWorkflowHandler;
use Chill\DocStoreBundle\Workflow\WorkflowWithPublicViewDocumentHelper;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvidePersonsAssociated;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvideThirdPartiesAssociated;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * @internal
 *
 * @coversNothing
 */
class AccompanyingCourseDocumentWorkflowHandlerTest extends TestCase
{
    use ProphecyTrait;

    public function testGetSuggestedUsers()
    {
        $accompanyingPeriod = new AccompanyingPeriod();
        $document = new AccompanyingCourseDocument();
        $document->setCourse($accompanyingPeriod)->setUser($user1 = new User());
        $accompanyingPeriod->setUser($user = new User());
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setRelatedEntityId(1);

        $handler = new AccompanyingCourseDocumentWorkflowHandler(
            $this->prophesize(TranslatorInterface::class)->reveal(),
            $this->prophesize(EntityWorkflowRepository::class)->reveal(),
            $this->buildRepository($document, 1),
            new WorkflowWithPublicViewDocumentHelper($this->prophesize(Environment::class)->reveal()),
            $this->prophesize(ProvideThirdPartiesAssociated::class)->reveal(),
            $this->prophesize(ProvidePersonsAssociated::class)->reveal(),
        );

        $users = $handler->getSuggestedUsers($entityWorkflow);

        self::assertCount(2, $users);
        self::assertContains($user, $users);
        self::assertContains($user1, $users);
    }

    public function testGetSuggestedUsersWithDuplicates()
    {
        $accompanyingPeriod = new AccompanyingPeriod();
        $document = new AccompanyingCourseDocument();
        $document->setCourse($accompanyingPeriod)->setUser($user1 = new User());
        $accompanyingPeriod->setUser($user1);
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setRelatedEntityId(1);

        $handler = new AccompanyingCourseDocumentWorkflowHandler(
            $this->prophesize(TranslatorInterface::class)->reveal(),
            $this->prophesize(EntityWorkflowRepository::class)->reveal(),
            $this->buildRepository($document, 1),
            new WorkflowWithPublicViewDocumentHelper($this->prophesize(Environment::class)->reveal()),
            $this->prophesize(ProvideThirdPartiesAssociated::class)->reveal(),
            $this->prophesize(ProvidePersonsAssociated::class)->reveal(),
        );

        $users = $handler->getSuggestedUsers($entityWorkflow);

        self::assertCount(1, $users);
        self::assertContains($user1, $users);
    }

    private function buildRepository(AccompanyingCourseDocument $document, int $id): AccompanyingCourseDocumentRepository
    {
        $repository = $this->prophesize(AccompanyingCourseDocumentRepository::class);
        $repository->find($id)->willReturn($document);

        return $repository->reveal();
    }
}
