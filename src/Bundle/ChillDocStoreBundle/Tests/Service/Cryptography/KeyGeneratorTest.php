<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Service\Cryptography;

use Chill\DocStoreBundle\Service\Cryptography\KeyGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class KeyGeneratorTest extends TestCase
{
    public function testGenerateKey(): void
    {
        $keyGenerator = new KeyGenerator();

        $key = $keyGenerator->generateKey();

        self::assertNotEmpty($key['k']);
        self::assertEquals('A256CBC', $key['alg']);
    }

    public function testGenerateIv(): void
    {
        $keyGenerator = new KeyGenerator();

        $actual = $keyGenerator->generateIv();

        self::assertCount(16, $actual);
        foreach ($actual as $value) {
            self::assertIsInt($value);
            self::assertGreaterThanOrEqual(0, $value);
            self::assertLessThan(256, $value);
        }
    }
}
