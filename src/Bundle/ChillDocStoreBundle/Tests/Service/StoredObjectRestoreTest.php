<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Service;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Service\StoredObjectManagerInterface;
use Chill\DocStoreBundle\Service\StoredObjectRestore;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectRestoreTest extends TestCase
{
    use ProphecyTrait;

    public function testRestore(): void
    {
        $storedObject = new StoredObject();
        $version = $storedObject->registerVersion(type: 'application/test');

        $storedObjectManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storedObjectManager->read($version)->willReturn('1234')->shouldBeCalledOnce();
        $storedObjectManager->write($storedObject, '1234', 'application/test')->shouldBeCalledOnce()
            ->will(function ($args) {
                /** @var StoredObject $object */
                $object = $args[0];

                return $object->registerVersion();
            })
        ;

        $restore = new StoredObjectRestore($storedObjectManager->reveal(), new NullLogger());

        $newVersion = $restore->restore($version);

        self::assertNotSame($version, $newVersion);
        self::assertSame($version, $newVersion->getCreatedFrom());
    }
}
