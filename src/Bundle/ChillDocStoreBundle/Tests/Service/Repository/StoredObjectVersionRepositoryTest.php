<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Repository;

use Chill\DocStoreBundle\Repository\StoredObjectVersionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectVersionRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testFindIdsByVersionsOlderThanDateAndNotLastVersion(): void
    {
        $repository = new StoredObjectVersionRepository($this->entityManager);

        // get old version, to get a chance to get one
        $actual = $repository->findIdsByVersionsOlderThanDateAndNotLastVersionAndNotPointInTime(new \DateTimeImmutable('1970-01-01'));

        self::assertIsIterable($actual);
        self::assertContainsOnly('int', $actual);
    }
}
