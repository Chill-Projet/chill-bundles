<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Service;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Entity\StoredObjectPointInTime;
use Chill\DocStoreBundle\Entity\StoredObjectVersion;
use Chill\DocStoreBundle\Service\StoredObjectManagerInterface;
use Chill\DocStoreBundle\Service\StoredObjectToPdfConverter;
use Chill\WopiBundle\Service\WopiConverter;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Mime\MimeTypes;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectToPdfConverterTest extends TestCase
{
    use ProphecyTrait;

    public function testAddConvertedVersion(): void
    {
        $storedObject = new StoredObject();
        $currentVersion = $storedObject->registerVersion(type: 'text/html');

        $storedObjectManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storedObjectManager->read($currentVersion)->willReturn('1234');
        $storedObjectManager->write($storedObject, '5678', 'application/pdf')->shouldBeCalled()
            ->will(function ($args) {
                /** @var StoredObject $storedObject */
                $storedObject = $args[0];

                return $storedObject->registerVersion(type: $args[2]);
            });

        $converter = $this->prophesize(WopiConverter::class);
        $converter->convert('fr', '1234', 'application/pdf', 'pdf')->shouldBeCalled()
            ->willReturn('5678');

        $converter = new StoredObjectToPdfConverter($storedObjectManager->reveal(), $converter->reveal(), MimeTypes::getDefault());

        $actual = $converter->addConvertedVersion($storedObject, 'fr');

        self::assertIsArray($actual);
        self::assertInstanceOf(StoredObjectPointInTime::class, $actual[0]);
        self::assertSame($currentVersion, $actual[0]->getObjectVersion());
        self::assertInstanceOf(StoredObjectVersion::class, $actual[1]);
    }
}
