<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Service\Signature\Driver\BaseSigner;

use Chill\DocStoreBundle\Service\Signature\Driver\BaseSigner\PdfSignedMessage;
use Chill\DocStoreBundle\Service\Signature\Driver\BaseSigner\PdfSignedMessageSerializer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;

/**
 * @internal
 *
 * @coversNothing
 */
class PdfSignedMessageSerializerTest extends TestCase
{
    public function testDecode(): void
    {
        $asString = <<<'JSON'
            {"signatureId": 0, "signatureZoneIndex": 10, "content": "dGVzdAo="}
            JSON;

        $actual = $this->buildSerializer()->decode(['body' => $asString]);

        self::assertInstanceOf(Envelope::class, $actual);
        $message = $actual->getMessage();
        self::assertInstanceOf(PdfSignedMessage::class, $message);
        self::assertEquals("test\n", $message->content);
        self::assertEquals(0, $message->signatureId);
        self::assertEquals(10, $message->signatureZoneIndex);
    }

    public function testEncode(): void
    {
        $envelope = new Envelope(
            new PdfSignedMessage(0, 10, "test\n")
        );

        $actual = $this->buildSerializer()->encode($envelope);

        self::assertIsArray($actual);
        self::assertArrayHasKey('body', $actual);
        self::assertArrayHasKey('headers', $actual);
        self::assertEquals([], $actual['headers']);

        self::assertEquals(<<<'JSON'
                {"signatureId":0,"signatureZoneIndex":10,"content":"dGVzdAo="}
                JSON, $actual['body']);
    }

    private function buildSerializer(): PdfSignedMessageSerializer
    {
        return new PdfSignedMessageSerializer();
    }
}
