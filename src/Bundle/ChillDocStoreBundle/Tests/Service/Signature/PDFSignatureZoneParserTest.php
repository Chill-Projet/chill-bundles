<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Tests\Service\Signature;

use Chill\DocStoreBundle\Service\Signature\PDFPage;
use PHPUnit\Framework\TestCase;
use Chill\DocStoreBundle\Service\Signature\PDFSignatureZone;
use Chill\DocStoreBundle\Service\Signature\PDFSignatureZoneParser;

/**
 * @internal
 *
 * @coversNothing
 */
class PDFSignatureZoneParserTest extends TestCase
{
    private static PDFSignatureZoneParser $parser;

    public static function setUpBeforeClass(): void
    {
        self::$parser = new PDFSignatureZoneParser();
    }

    /**
     * @dataProvider provideFiles
     *
     * @param list<PDFSignatureZone> $expected
     */
    public function testFindSignatureZones(string $filePath, array $expected): void
    {
        $content = file_get_contents($filePath);

        if (false === $content) {
            throw new \LogicException("Unable to read file {$filePath}");
        }

        $actual = self::$parser->findSignatureZones($content);

        self::assertEquals(count($expected), count($actual));

        foreach ($actual as $index => $signatureZone) {
            self::assertObjectEquals($expected[$index], $signatureZone);
        }
    }

    public static function provideFiles(): iterable
    {
        yield [
            __DIR__.'/data/signature_2_signature_page_1.pdf',
            [
                new PDFSignatureZone(
                    0,
                    127.7,
                    95.289,
                    90.0,
                    180.0,
                    $page = new PDFPage(0, 595.30393, 841.8897)
                ),
                new PDFSignatureZone(
                    1,
                    269.5,
                    95.289,
                    90.0,
                    180.0,
                    $page,
                ),
            ],
        ];
    }
}
