<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Serializer\Normalizer;

use Chill\DocStoreBundle\AsyncUpload\SignedUrl;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class SignedUrlNormalizerTest extends KernelTestCase
{
    public static NormalizerInterface $normalizer;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::bootKernel();
        self::$normalizer = self::getContainer()->get(NormalizerInterface::class);
    }

    public function testNormalizerSignedUrl(): void
    {
        $signedUrl = new SignedUrl(
            'GET',
            'https://object.store.example/container/object',
            \DateTimeImmutable::createFromFormat('U', '1700000'),
            'object'
        );

        $actual = self::$normalizer->normalize($signedUrl, 'json', [AbstractNormalizer::GROUPS => ['read']]);

        self::assertEqualsCanonicalizing(
            [
                'method' => 'GET',
                'expires' => 1_700_000,
                'url' => 'https://object.store.example/container/object',
                'object_name' => 'object',
            ],
            $actual
        );
    }
}
