<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Tests\Serializer\Normalizer;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\ManagerInterface;
use Chill\DocStoreBundle\Serializer\Normalizer\GenericDocNormalizer;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class GenericDocNormalizerTest extends TestCase
{
    private $normalizer;

    private ManagerInterface $manager;

    protected function setUp(): void
    {
        $this->manager = $this->createMock(ManagerInterface::class);

        $this->normalizer = new GenericDocNormalizer($this->manager);

        $innerNormalizer = $this->createMock(NormalizerInterface::class);
        $innerNormalizer->method('normalize')
            ->willReturnCallback(fn ($date) => $date instanceof \DateTimeImmutable ? $date->format(DATE_ATOM) : null);

        $this->normalizer->setNormalizer($innerNormalizer);
    }

    public function testNormalize()
    {
        $docDate = new \DateTimeImmutable('2023-10-01T15:03:01.012345Z');

        $object = new GenericDocDTO(
            'some_key',
            ['id' => 'id1', 'other_id' => 'id2'],
            $docDate,
            new AccompanyingPeriod(),
        );

        $expected = [
            'type' => 'doc_store_generic_doc',
            'key' => 'some_key',
            'identifiers' => ['id' => 'id1', 'other_id' => 'id2'],
            'context' => 'accompanying-period',
            'doc_date' => $docDate->format(DATE_ATOM),
            'uniqueKey' => 'some_keyidother_idid1id2',
            'metadata' => [],
            'storedObject' => null,
        ];

        $this->manager->expects($this->once())->method('isGenericDocNormalizable')
            ->with($object, 'json', [])
            ->willReturn(true);

        $actual = $this->normalizer->normalize($object, 'json', []);

        $this->assertEquals($expected, $actual);
    }
}
