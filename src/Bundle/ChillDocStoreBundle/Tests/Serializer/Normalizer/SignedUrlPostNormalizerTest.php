<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Serializer\Normalizer;

use Chill\DocStoreBundle\AsyncUpload\SignedUrlPost;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class SignedUrlPostNormalizerTest extends KernelTestCase
{
    public static NormalizerInterface $normalizer;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::bootKernel();
        self::$normalizer = self::getContainer()->get(NormalizerInterface::class);
    }

    public function testNormalizerSignedUrl(): void
    {
        $signedUrl = new SignedUrlPost(
            'https://object.store.example/container/object',
            \DateTimeImmutable::createFromFormat('U', '1700000'),
            'abc',
            15000,
            1,
            180,
            '',
            'abc',
            'SiGnaTure'
        );

        $actual = self::$normalizer->normalize($signedUrl, 'json', [AbstractNormalizer::GROUPS => ['read']]);

        self::assertEqualsCanonicalizing(
            [
                'max_file_size' => 15000,
                'max_file_count' => 1,
                'submit_delay' => 180,
                'redirect' => '',
                'prefix' => 'abc',
                'signature' => 'SiGnaTure',
                'method' => 'POST',
                'expires' => 1_700_000,
                'url' => 'https://object.store.example/container/object',
                'object_name' => 'abc',
            ],
            $actual
        );
    }
}
