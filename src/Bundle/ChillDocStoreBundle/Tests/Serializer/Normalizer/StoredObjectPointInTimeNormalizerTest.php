<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Serializer\Normalizer;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Entity\StoredObjectPointInTime;
use Chill\DocStoreBundle\Entity\StoredObjectPointInTimeReasonEnum;
use Chill\DocStoreBundle\Serializer\Normalizer\StoredObjectPointInTimeNormalizer;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Serializer\Normalizer\UserNormalizer;
use Chill\MainBundle\Templating\Entity\UserRender;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Clock\MockClock;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectPointInTimeNormalizerTest extends TestCase
{
    use ProphecyTrait;

    public function testNormalize(): void
    {
        $storedObject = new StoredObject();
        $version = $storedObject->registerVersion();
        $storedObjectPointInTime = new StoredObjectPointInTime($version, StoredObjectPointInTimeReasonEnum::KEEP_BEFORE_CONVERSION, new User());

        $normalizer = new StoredObjectPointInTimeNormalizer();
        $normalizer->setNormalizer($this->buildNormalizer());

        $actual = $normalizer->normalize($storedObjectPointInTime, 'json', ['read']);

        self::assertIsArray($actual);
        self::assertArrayHasKey('id', $actual);
        self::assertArrayHasKey('byUser', $actual);
        self::assertArrayHasKey('reason', $actual);
        self::assertEquals(StoredObjectPointInTimeReasonEnum::KEEP_BEFORE_CONVERSION->value, $actual['reason']);
    }

    public function buildNormalizer(): NormalizerInterface
    {
        $userRender = $this->prophesize(UserRender::class);
        $userRender->renderString(Argument::type(User::class), Argument::type('array'))->willReturn('username');

        return new Serializer(
            [new UserNormalizer($userRender->reveal(), new MockClock())]
        );
    }
}
