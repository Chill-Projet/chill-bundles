<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\AsyncUpload\Driver\OpenstackObjectStore;

use Chill\DocStoreBundle\AsyncUpload\Driver\OpenstackObjectStore\ConfigureOpenstackObjectStorageCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

/**
 * @internal
 *
 * @coversNothing
 */
class ConfigureOpenstackObjectStorageCommandTest extends TestCase
{
    public function testRun(): void
    {
        $client = new MockHttpClient(function ($method, $url, $options): MockResponse {
            self::assertSame('POST', $method);
            self::assertSame($url, 'https://object.store.example/v1/AUTH/container');

            $headers = $options['headers'];

            self::assertContains('X-Auth-Token: abc', $headers);
            self::assertContains('X-Container-Meta-Temp-URL-Key: 12345679801234567890', $headers);
            self::assertContains('X-Container-Meta-Access-Control-Allow-Origin: https://chill.domain.social https://chill2.domain.social', $headers);

            return new MockResponse('', ['http_code' => 204]);
        });

        $parameters = new ParameterBag([
            'chill_doc_store' => [
                'openstack' => [
                    'temp_url' => [
                        'temp_url_key' => '12345679801234567890',
                        'temp_url_base_path' => 'https://object.store.example/v1/AUTH/container',
                    ],
                ],
            ],
        ]);

        $command = new ConfigureOpenstackObjectStorageCommand($client, $parameters);

        $tester = new CommandTester($command);

        $status = $tester->execute([
            '--os_token' => 'abc',
            '--domain' => ['https://chill.domain.social', 'https://chill2.domain.social'],
        ]);

        self::assertSame(0, $status);
    }
}
