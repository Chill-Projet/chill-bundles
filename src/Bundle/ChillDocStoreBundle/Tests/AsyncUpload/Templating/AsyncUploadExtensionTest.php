<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace AsyncUpload\Templating;

use Chill\DocStoreBundle\AsyncUpload\SignedUrl;
use Chill\DocStoreBundle\AsyncUpload\Templating\AsyncUploadExtension;
use Chill\DocStoreBundle\AsyncUpload\TempUrlGeneratorInterface;
use Chill\DocStoreBundle\Entity\StoredObject;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class AsyncUploadExtensionTest extends KernelTestCase
{
    use ProphecyTrait;

    private AsyncUploadExtension $asyncUploadExtension;

    public function setUp(): void
    {
        $generator = $this->prophesize(TempUrlGeneratorInterface::class);
        $generator->generate(Argument::in(['GET', 'POST']), Argument::type('string'), Argument::any())
            ->will(fn (array $args): SignedUrl => new SignedUrl($args[0], 'https://object.store.example/container/'.$args[1], new \DateTimeImmutable('1 hours'), $args[1]));

        $urlGenerator = $this->prophesize(UrlGeneratorInterface::class);
        $urlGenerator->generate('async_upload.generate_url', Argument::type('array'))
            ->willReturn('url');

        $this->asyncUploadExtension = new AsyncUploadExtension(
            $generator->reveal(),
            $urlGenerator->reveal()
        );
    }

    /**
     * @dataProvider dataProviderStoredObject
     */
    public function testComputeSignedUrl(StoredObject|string $storedObject): void
    {
        $actual = $this->asyncUploadExtension->computeSignedUrl($storedObject);

        self::assertStringContainsString('https://object.store.example/container', $actual);
        self::assertStringContainsString(is_string($storedObject) ? $storedObject : $storedObject->getFilename(), $actual);
    }

    /**
     * @dataProvider dataProviderStoredObject
     */
    public function testComputeGenerateUrl(StoredObject|string $storedObject): void
    {
        $actual = $this->asyncUploadExtension->computeGenerateUrl($storedObject);

        self::assertEquals('url', $actual);
    }

    public static function dataProviderStoredObject(): iterable
    {
        yield [(new StoredObject())->registerVersion(filename: 'blabla')->getStoredObject()];

        yield ['blabla'];
    }
}
