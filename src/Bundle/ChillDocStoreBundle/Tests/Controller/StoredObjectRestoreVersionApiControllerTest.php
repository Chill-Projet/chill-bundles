<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Tests\Chill\DocStoreBundle\Tests\Controller;

use Chill\DocStoreBundle\Entity\StoredObjectVersion;
use Chill\DocStoreBundle\Service\StoredObjectRestoreInterface;
use Chill\DocStoreBundle\Controller\StoredObjectRestoreVersionApiController;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectRestoreVersionApiControllerTest extends TestCase
{
    public function testRestoreStoredObjectVersion(): void
    {
        $security = $this->createMock(Security::class);
        $storedObjectRestore = $this->createMock(StoredObjectRestoreInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $serializer = $this->createMock(SerializerInterface::class);
        $storedObjectVersion = $this->createMock(StoredObjectVersion::class);
        $controller = new StoredObjectRestoreVersionApiController($security, $storedObjectRestore, $entityManager, $serializer);

        $security->expects($this->once())
            ->method('isGranted')
            ->willReturn(true);
        $storedObjectRestore->expects($this->once())
            ->method('restore')
            ->willReturn($storedObjectVersion);
        $entityManager->expects($this->once())
            ->method('persist');
        $entityManager->expects($this->once())
            ->method('flush');

        $serializer->expects($this->once())
            ->method('serialize')
            ->willReturn('test');

        $response = $controller->restoreStoredObjectVersion($storedObjectVersion);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('test', $response->getContent());
    }

    public function testRestoreStoredObjectVersionAccessDenied(): void
    {
        $security = $this->createMock(Security::class);
        $storedObjectRestore = $this->createMock(StoredObjectRestoreInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $serializer = $this->createMock(SerializerInterface::class);
        $storedObjectVersion = $this->createMock(StoredObjectVersion::class);
        $controller = new StoredObjectRestoreVersionApiController($security, $storedObjectRestore, $entityManager, $serializer);

        self::expectException(\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException::class);
        $security->expects($this->once())
            ->method('isGranted')
            ->willReturn(false);
        $controller->restoreStoredObjectVersion($storedObjectVersion);
    }
}
