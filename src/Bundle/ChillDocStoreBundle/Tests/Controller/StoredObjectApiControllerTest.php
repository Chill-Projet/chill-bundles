<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Controller;

use Chill\DocStoreBundle\Controller\StoredObjectApiController;
use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectApiControllerTest extends TestCase
{
    public function testCreate(): void
    {
        $security = $this->createMock(Security::class);
        $security->expects($this->atLeastOnce())->method('isGranted')
            ->with($this->logicalOr($this->identicalTo('ROLE_ADMIN'), $this->identicalTo('ROLE_USER')))
            ->willReturn(true)
        ;

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('persist')->with($this->isInstanceOf(StoredObject::class));
        $entityManager->expects($this->once())->method('flush');

        $serializer = $this->createMock(SerializerInterface::class);
        $serializer->expects($this->once())->method('serialize')
            ->with($this->isInstanceOf(StoredObject::class), 'json', $this->anything())
            ->willReturn($r = <<<'JSON'
            {"type":  "stored-object", "id":  1}
            JSON);

        $controller = new StoredObjectApiController($security, $serializer, $entityManager);

        $actual = $controller->createStoredObject();

        self::assertInstanceOf(JsonResponse::class, $actual);
        self::assertEquals($r, $actual->getContent());
    }
}
