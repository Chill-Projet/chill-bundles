<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Entity;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Entity\StoredObjectPointInTime;
use Chill\DocStoreBundle\Entity\StoredObjectPointInTimeReasonEnum;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class StoredObjectTest extends KernelTestCase
{
    public function testRegisterVersion(): void
    {
        $object = new StoredObject();
        $firstVersion = $object->registerVersion(
            [5, 6, 7, 8],
            ['key' => ['some key']],
            'text/html',
        );

        self::assertSame($firstVersion, $object->getCurrentVersion());

        $version = $object->registerVersion(
            [1, 2, 3, 4],
            $k = ['key' => ['data0' => 'data0']],
            'text/text',
            'abcde',
        );

        self::assertSame($version, $object->getCurrentVersion());

        self::assertCount(2, $object->getVersions());
        self::assertEquals('abcde', $object->getFilename());
        self::assertEquals([1, 2, 3, 4], $object->getIv());
        self::assertEqualsCanonicalizing($k, $object->getKeyInfos());
        self::assertEquals('text/text', $object->getType());

        self::assertEquals('abcde', $version->getFilename());
        self::assertEquals([1, 2, 3, 4], $version->getIv());
        self::assertEqualsCanonicalizing($k, $version->getKeyInfos());
        self::assertEquals('text/text', $version->getType());

        self::assertNotSame($firstVersion, $version);
    }

    public function testHasKeptBeforeConversionVersion(): void
    {
        $storedObject = new StoredObject();
        $version1 = $storedObject->registerVersion();

        self::assertFalse($storedObject->hasKeptBeforeConversionVersion());

        // add a point in time without the correct version
        new StoredObjectPointInTime($version1, StoredObjectPointInTimeReasonEnum::KEEP_BY_USER);

        self::assertFalse($storedObject->hasKeptBeforeConversionVersion());
        self::assertNull($storedObject->getLastKeptBeforeConversionVersion());

        new StoredObjectPointInTime($version1, StoredObjectPointInTimeReasonEnum::KEEP_BEFORE_CONVERSION);

        self::assertTrue($storedObject->hasKeptBeforeConversionVersion());
        // add a second version
        $version2 = $storedObject->registerVersion();
        new StoredObjectPointInTime($version2, StoredObjectPointInTimeReasonEnum::KEEP_BEFORE_CONVERSION);

        self::assertSame($version2, $storedObject->getLastKeptBeforeConversionVersion());
    }
}
