<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Security\Guard;

use Chill\DocStoreBundle\Security\Guard\DavOnUrlTokenExtractor;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 *
 * @coversNothing
 */
class DavOnUrlTokenExtractorTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @dataProvider provideDataUri
     */
    public function testExtract(string $uri, false|string $expected): void
    {
        $request = $this->prophesize(Request::class);
        $request->getRequestUri()->willReturn($uri);

        $extractor = new DavOnUrlTokenExtractor(new NullLogger());

        $actual = $extractor->extract($request->reveal());

        self::assertEquals($expected, $actual);
    }

    /**
     * @phpstan-pure
     */
    public static function provideDataUri(): iterable
    {
        yield ['/dav/123456789/get/d07d2230-5326-11ee-8fd4-93696acf5ea1/d', '123456789'];
        yield ['/dav/123456789', '123456789'];
        yield ['/not-dav/123456978', false];
        yield ['/dav', false];
        yield ['/', false];
    }
}
