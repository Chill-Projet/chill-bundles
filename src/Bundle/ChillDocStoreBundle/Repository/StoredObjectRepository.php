<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Repository;

use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

final readonly class StoredObjectRepository implements StoredObjectRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(StoredObject::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?StoredObject
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    /**
     * @return array<int, StoredObject>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, StoredObject>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?StoredObject
    {
        return $this->repository->findOneBy($criteria);
    }

    public function findByExpired(\DateTimeImmutable $expiredAtDate): iterable
    {
        $qb = $this->repository->createQueryBuilder('stored_object');
        $qb
            ->where('stored_object.deleteAt <= :expiredAt')
            ->setParameter('expiredAt', $expiredAtDate);

        return $qb->getQuery()->toIterable(hydrationMode: Query::HYDRATE_OBJECT);
    }

    public function findOneByUUID(string $uuid): ?StoredObject
    {
        return $this->repository->findOneBy(['uuid' => $uuid]);
    }

    public function getClassName(): string
    {
        return StoredObject::class;
    }
}
