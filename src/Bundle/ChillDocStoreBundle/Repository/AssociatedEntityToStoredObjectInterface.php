<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Repository;

use Chill\DocStoreBundle\Entity\StoredObject;

interface AssociatedEntityToStoredObjectInterface
{
    public function findAssociatedEntityToStoredObject(StoredObject $storedObject): ?object;
}
