<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle;

use Chill\DocStoreBundle\DependencyInjection\Compiler\StorageConfigurationCompilerPass;
use Chill\DocStoreBundle\GenericDoc\GenericDocForAccompanyingPeriodProviderInterface;
use Chill\DocStoreBundle\GenericDoc\GenericDocForPersonProviderInterface;
use Chill\DocStoreBundle\GenericDoc\GenericDocNormalizerInterface;
use Chill\DocStoreBundle\GenericDoc\Twig\GenericDocRendererInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ChillDocStoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(GenericDocForAccompanyingPeriodProviderInterface::class)
            ->addTag('chill_doc_store.generic_doc_accompanying_period_provider');
        $container->registerForAutoconfiguration(GenericDocForPersonProviderInterface::class)
            ->addTag('chill_doc_store.generic_doc_person_provider');
        $container->registerForAutoconfiguration(GenericDocRendererInterface::class)
            ->addTag('chill_doc_store.generic_doc_renderer');
        $container->registerForAutoconfiguration(GenericDocNormalizerInterface::class)
            ->addTag('chill_doc_store.generic_doc_metadata_normalizer');

        $container->addCompilerPass(new StorageConfigurationCompilerPass());
    }
}
