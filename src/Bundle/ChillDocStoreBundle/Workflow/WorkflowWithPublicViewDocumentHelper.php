<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowSend;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\MainBundle\Workflow\EntityWorkflowWithStoredObjectHandlerInterface;
use Chill\MainBundle\Workflow\Templating\EntityWorkflowViewMetadataDTO;
use Twig\Environment;

class WorkflowWithPublicViewDocumentHelper
{
    public function __construct(private readonly Environment $twig) {}

    public function render(EntityWorkflowSend $send, EntityWorkflowViewMetadataDTO $metadata, EntityWorkflowHandlerInterface&EntityWorkflowWithStoredObjectHandlerInterface $handler): string
    {
        $entityWorkflow = $send->getEntityWorkflowStep()->getEntityWorkflow();
        $storedObject = $handler->getAssociatedStoredObject($entityWorkflow);

        if (null === $storedObject) {
            return 'document removed';
        }

        $title = $handler->getEntityTitle($entityWorkflow);

        return $this->twig->render(
            '@ChillDocStore/Workflow/public_view_with_document_render.html.twig',
            [
                'title' => $title,
                'storedObject' => $storedObject,
                'send' => $send,
                'metadata' => $metadata,
                'attachments' => $entityWorkflow->getAttachments(),
            ]
        );
    }
}
