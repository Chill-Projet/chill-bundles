<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Workflow;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Repository\AccompanyingCourseDocumentRepository;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Entity\Workflow\EntityWorkflowSend;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Chill\MainBundle\Workflow\EntityWorkflowWithPublicViewInterface;
use Chill\MainBundle\Workflow\EntityWorkflowWithStoredObjectHandlerInterface;
use Chill\MainBundle\Workflow\Templating\EntityWorkflowViewMetadataDTO;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvideThirdPartiesAssociated;
use Chill\PersonBundle\Service\AccompanyingPeriod\ProvidePersonsAssociated;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements EntityWorkflowWithStoredObjectHandlerInterface<AccompanyingCourseDocument>
 */
final readonly class AccompanyingCourseDocumentWorkflowHandler implements EntityWorkflowWithStoredObjectHandlerInterface, EntityWorkflowWithPublicViewInterface
{
    public function __construct(
        private TranslatorInterface $translator,
        private EntityWorkflowRepository $workflowRepository,
        private AccompanyingCourseDocumentRepository $repository,
        private WorkflowWithPublicViewDocumentHelper $publicViewDocumentHelper,
        private ProvideThirdPartiesAssociated $thirdPartiesAssociated,
        private ProvidePersonsAssociated $providePersonsAssociated,
    ) {}

    public function getDeletionRoles(): array
    {
        return [
            AccompanyingCourseDocumentVoter::DELETE,
        ];
    }

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $course = $this->getRelatedEntity($entityWorkflow)?->getCourse();
        $persons = [];

        if (null !== $course) {
            $persons = $course->getCurrentParticipations()->map(static fn (AccompanyingPeriodParticipation $participation) => $participation->getPerson())->toArray();
        }

        return [
            'persons' => array_values($persons),
        ];
    }

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        $doc = $this->getRelatedEntity($entityWorkflow);

        if (null === $doc) {
            return $this->translator->trans('workflow.Document deleted');
        }

        return $this->translator->trans('entity_display_title.Document (n°%doc%)', ['%doc%' => $entityWorkflow->getRelatedEntityId()])
            .' - '.$doc->getTitle();
    }

    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?AccompanyingCourseDocument
    {
        return $this->repository->find($entityWorkflow->getRelatedEntityId());
    }

    public function getRelatedAccompanyingPeriod(EntityWorkflow $entityWorkflow): ?AccompanyingPeriod
    {
        if (null === $document = $this->getRelatedEntity($entityWorkflow)) {
            return null;
        }

        return $document->getCourse();
    }

    /**
     * @return array[]
     */
    public function getRelatedObjects(object $object): array
    {
        return [
            ['entityClass' => AccompanyingCourseDocument::class, 'entityId' => $object->getId()],
        ];
    }

    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string
    {
        return null;
    }

    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        $users = [];
        if (null !== $user = $related->getUser()) {
            $users[] = $user;
        }
        if (null !== $user = $related->getCourse()->getUser()) {
            $users[] = $user;
        }

        return array_values(
            // filter objects to remove duplicates
            array_filter(
                $users,
                fn ($o, $k) => array_search($o, $users, true) === $k,
                ARRAY_FILTER_USE_BOTH
            )
        );
    }

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        return '@ChillDocStore/AccompanyingCourseDocument/_workflow.html.twig';
    }

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        return [
            'entity_workflow' => $entityWorkflow,
            'document' => $this->getRelatedEntity($entityWorkflow),
        ];
    }

    public function isObjectSupported(object $object): bool
    {
        return $object instanceof AccompanyingCourseDocument;
    }

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return AccompanyingCourseDocument::class === $entityWorkflow->getRelatedEntityClass();
    }

    public function getAssociatedStoredObject(EntityWorkflow $entityWorkflow): ?StoredObject
    {
        return $this->getRelatedEntity($entityWorkflow)?->getObject();
    }

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return false;
    }

    public function findByRelatedEntity(object $object): array
    {
        if (!$object instanceof AccompanyingCourseDocument) {
            return [];
        }

        return $this->workflowRepository->findByRelatedEntity(AccompanyingCourseDocument::class, $object->getId());
    }

    public function renderPublicView(EntityWorkflowSend $entityWorkflowSend, EntityWorkflowViewMetadataDTO $metadata): string
    {
        return $this->publicViewDocumentHelper->render($entityWorkflowSend, $metadata, $this);
    }

    public function getSuggestedPersons(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        return $this->providePersonsAssociated->getPersonsAssociated($related->getCourse());
    }

    public function getSuggestedThirdParties(EntityWorkflow $entityWorkflow): array
    {
        $related = $this->getRelatedEntity($entityWorkflow);

        if (null === $related) {
            return [];
        }

        return $this->thirdPartiesAssociated->getThirdPartiesAssociated($related->getCourse());
    }
}
