<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Workflow;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Service\StoredObjectDuplicate;
use Symfony\Component\Clock\ClockInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Stores the logic to duplicate an AccompanyingCourseDocument associated to a workflow.
 */
class AccompanyingCourseDocumentDuplicator
{
    public function __construct(
        private readonly StoredObjectDuplicate $storedObjectDuplicate,
        private readonly TranslatorInterface $translator,
        private readonly ClockInterface $clock,
    ) {}

    public function duplicate(AccompanyingCourseDocument $document): AccompanyingCourseDocument
    {
        $newDoc = new AccompanyingCourseDocument();
        $newDoc
            ->setCourse($document->getCourse())
            ->setTitle($document->getTitle().' ('.$this->translator->trans('acc_course_document.duplicated_at', ['at' => $this->clock->now()]).')')
            ->setDate($document->getDate())
            ->setDescription($document->getDescription())
            ->setCategory($document->getCategory())
            ->setUser($document->getUser())
            ->setObject($this->storedObjectDuplicate->duplicate($document->getObject()))
        ;

        return $newDoc;
    }
}
