<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\GenericDoc\Exception\AssociatedStoredObjectNotFound;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\DBAL\Exception;

interface ManagerInterface
{
    /**
     * @param list<string> $places
     *
     * @throws Exception
     */
    public function countDocForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, array $places = []): int;

    public function countDocForPerson(Person $person, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, array $places = []): int;

    /**
     * @param list<string> $places places to search. When empty, search in all places
     *
     * @return iterable<GenericDocDTO>
     *
     * @throws Exception
     */
    public function findDocForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod, int $offset = 0, int $limit = 20, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, array $places = []): iterable;

    /**
     * @param list<string> $places places to search. When empty, search in all places
     *
     * @return iterable<GenericDocDTO>
     */
    public function findDocForPerson(Person $person, int $offset = 0, int $limit = 20, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null, array $places = []): iterable;

    public function placesForPerson(Person $person): array;

    public function placesForAccompanyingPeriod(AccompanyingPeriod $accompanyingPeriod): array;

    public function isGenericDocNormalizable(GenericDocDTO $genericDocDTO, string $format, array $context = []): bool;

    /**
     * @return array{title: string, html?: string}
     */
    public function normalizeGenericDoc(GenericDocDTO $genericDocDTO, string $format, array $context = []): array;

    public function buildOneGenericDoc(string $key, array $identifiers): ?GenericDocDTO;

    /**
     * @throws AssociatedStoredObjectNotFound if no stored object can be found
     */
    public function fetchStoredObject(GenericDocDTO $genericDocDTO): StoredObject;
}
