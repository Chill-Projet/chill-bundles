<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

use Chill\DocStoreBundle\Entity\StoredObject;

interface GenericDocProviderInterface
{
    public function fetchAssociatedStoredObject(GenericDocDTO $genericDocDTO): ?StoredObject;

    /**
     * Return true if this provider supports the given Generic doc for various informations.
     *
     * Concerned:
     *
     * - @see{self::fetchAssociatedStoredObject}
     */
    public function supportsGenericDoc(GenericDocDTO $genericDocDTO): bool;

    /**
     * return true if the implementation supports key and identifiers.
     */
    public function supportsKeyAndIdentifiers(string $key, array $identifiers): bool;

    /**
     * Build a GenericDocDTO, given the key and identifiers.
     */
    public function buildOneGenericDoc(string $key, array $identifiers): ?GenericDocDTO;
}
