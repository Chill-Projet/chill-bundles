<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

use Chill\PersonBundle\Entity\Person;

interface GenericDocForPersonProviderInterface
{
    public function buildFetchQueryForPerson(
        Person $person,
        ?\DateTimeImmutable $startDate = null,
        ?\DateTimeImmutable $endDate = null,
        ?string $content = null,
        ?string $origin = null,
    ): FetchQueryInterface;

    /**
     * Return true if the user is allowed to see some documents for this provider.
     */
    public function isAllowedForPerson(Person $person): bool;
}
