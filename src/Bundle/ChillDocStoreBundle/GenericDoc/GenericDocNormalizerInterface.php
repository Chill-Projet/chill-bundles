<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

/**
 * Normalize a Generic Doc.
 */
interface GenericDocNormalizerInterface
{
    /**
     * Return true if a generic doc can be normalized by this implementation.
     */
    public function supportsNormalization(GenericDocDTO $genericDocDTO, string $format, array $context = []): bool;

    /**
     * Normalize a generic doc into an array.
     *
     * @return array{title: string, html?: string, isPresent: bool}
     */
    public function normalize(GenericDocDTO $genericDocDTO, string $format, array $context = []): array;
}
