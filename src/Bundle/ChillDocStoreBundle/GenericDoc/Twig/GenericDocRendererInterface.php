<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc\Twig;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;

/**
 * Render a generic doc, to display it into a page.
 *
 * @template T of array
 */
interface GenericDocRendererInterface
{
    /**
     * @param T $options the options defined by the renderer
     */
    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool;

    /**
     * @param T $options the options defined by the renderer
     */
    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string;

    /**
     * @param T $options the options defined by the renderer
     */
    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array;
}
