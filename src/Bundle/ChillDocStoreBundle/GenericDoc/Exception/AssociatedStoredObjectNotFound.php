<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc\Exception;

class AssociatedStoredObjectNotFound extends \RuntimeException
{
    public function __construct(string $key, array $identifiers, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('No stored object found for generic doc with key "%s" and identifiers "%s"', $key, json_encode($identifiers)), $code, $previous);
    }
}
