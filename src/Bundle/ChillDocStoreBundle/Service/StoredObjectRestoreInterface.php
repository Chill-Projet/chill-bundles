<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service;

use Chill\DocStoreBundle\Entity\StoredObjectVersion;

/**
 * Restore an old version of the stored object as the current one.
 */
interface StoredObjectRestoreInterface
{
    public function restore(StoredObjectVersion $storedObjectVersion): StoredObjectVersion;
}
