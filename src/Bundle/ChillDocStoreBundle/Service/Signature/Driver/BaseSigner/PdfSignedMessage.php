<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service\Signature\Driver\BaseSigner;

/**
 * Message which is received when a pdf is signed.
 */
final readonly class PdfSignedMessage
{
    public function __construct(
        public readonly int $signatureId,
        public readonly ?int $signatureZoneIndex,
        public readonly string $content,
    ) {}
}
