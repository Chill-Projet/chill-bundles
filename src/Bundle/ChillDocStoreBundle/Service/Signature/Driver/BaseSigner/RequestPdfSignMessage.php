<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service\Signature\Driver\BaseSigner;

use Chill\DocStoreBundle\Service\Signature\PDFSignatureZone;

/**
 * Message which is sent when we request a signature on a pdf.
 */
final readonly class RequestPdfSignMessage
{
    public function __construct(
        public int $signatureId,
        public PDFSignatureZone $PDFSignatureZone,
        public ?int $signatureZoneIndex,
        public string $reason,
        public string $signerText,
        public string $content,
    ) {}
}
