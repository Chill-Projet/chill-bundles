<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service\Signature;

use Symfony\Component\Serializer\Annotation\Groups;

final readonly class PDFSignatureZone
{
    public function __construct(
        #[Groups(['read'])]
        public ?int $index,
        #[Groups(['read'])]
        public float $x,
        #[Groups(['read'])]
        public float $y,
        #[Groups(['read'])]
        public float $height,
        #[Groups(['read'])]
        public float $width,
        #[Groups(['read'])]
        public PDFPage $PDFPage,
    ) {}

    public function equals(self $other): bool
    {
        return
            $this->index == $other->index
            && $this->x == $other->x
            && $this->y == $other->y
            && $this->height == $other->height
            && $this->width == $other->width
            && $this->PDFPage->equals($other->PDFPage);
    }
}
