<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service\Signature;

use Symfony\Component\Serializer\Annotation\Groups;

final readonly class PDFPage
{
    public function __construct(
        #[Groups(['read'])]
        public int $index,
        #[Groups(['read'])]
        public float $width,
        #[Groups(['read'])]
        public float $height,
    ) {}

    public function equals(self $page): bool
    {
        return $page->index === $this->index
            && round($page->width, 2) === round($this->width, 2)
            && round($page->height, 2) === round($this->height, 2);
    }
}
