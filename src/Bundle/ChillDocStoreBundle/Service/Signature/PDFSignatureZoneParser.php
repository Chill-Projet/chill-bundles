<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service\Signature;

use Smalot\PdfParser\Parser;

class PDFSignatureZoneParser
{
    public const ZONE_SIGNATURE_START = 'signature_zone';

    private readonly Parser $parser;

    public function __construct(
        public float $defaultHeight = 90.0,
        public float $defaultWidth = 180.0,
    ) {
        $this->parser = new Parser();
    }

    /**
     * @return list<PDFSignatureZone>
     */
    public function findSignatureZones(string $fileContent): array
    {
        $pdf = $this->parser->parseContent($fileContent);
        $zones = [];

        $defaults = $pdf->getObjectsByType('Pages');
        $defaultPage = reset($defaults);
        $defaultPageDetails = $defaultPage->getDetails();
        $zoneIndex = 0;

        foreach ($pdf->getPages() as $index => $page) {
            $details = $page->getDetails();
            $pdfPage = new PDFPage(
                $index,
                (float) ($details['MediaBox'][2] ?? $defaultPageDetails['MediaBox'][2]),
                (float) ($details['MediaBox'][3] ?? $defaultPageDetails['MediaBox'][3]),
            );

            foreach ($page->getDataTm() as $dataTm) {
                if (str_starts_with((string) $dataTm[1], self::ZONE_SIGNATURE_START)) {
                    $zones[] = new PDFSignatureZone($zoneIndex, (float) $dataTm[0][4], (float) $dataTm[0][5], $this->defaultHeight, $this->defaultWidth, $pdfPage);
                    ++$zoneIndex;
                }
            }
        }

        return $zones;
    }
}
