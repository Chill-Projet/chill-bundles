<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\DataFixtures\ORM;

use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;
use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Adding acl for person document.
 */
class LoadDocumentACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 35000;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef, PermissionsGroup::class);
            printf("processing permission group %s \n", $permissionsGroup->getName());

            foreach (LoadScopes::$references as $scopeRef) {
                $scope = $this->getReference($scopeRef, Scope::class);
                printf("processing scope %s \n", $scope->getName()['en']);
                // create permission group
                switch ($permissionsGroup->getName()) {
                    case 'social':
                        if ('administrative' === $scope->getName()['en']) {
                            printf("denying power on administrative \n");

                            break 2; // we do not want any power on administrative
                        }

                        break;

                    case 'administrative':
                    case 'direction':
                        if (\in_array($scope->getName()['en'], ['administrative', 'social'], true)) {
                            printf("denying power on %s\n", $scope->getName()['en']);

                            break 2; // we do not want any power on social or administrative
                        }

                        break;
                }

                printf(
                    'Adding Person report acl to %s '
                        ."permission group, scope '%s' \n",
                    $permissionsGroup->getName(),
                    $scope->getName()['en']
                );
                $roleScopeUpdate = (new RoleScope())
                    ->setRole(PersonDocumentVoter::CREATE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeUpdate);
                $roleScopeCreate = (new RoleScope())
                    ->setRole(PersonDocumentVoter::UPDATE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreate);
                $roleScopeDelete = (new RoleScope())
                    ->setRole(PersonDocumentVoter::DELETE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeDelete);
                $manager->persist($roleScopeUpdate);
                $manager->persist($roleScopeCreate);
                $manager->persist($roleScopeDelete);
            }
        }

        $manager->flush();
    }
}
