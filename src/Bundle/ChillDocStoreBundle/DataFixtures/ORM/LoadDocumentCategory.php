<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\DataFixtures\ORM;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadDocumentCategory extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 35010;
    }

    public function load(ObjectManager $manager): void
    {
        $category = (new DocumentCategory('chill-doc-store', 10))
            ->setDocumentClass(\Chill\DocStoreBundle\Entity\PersonDocument::class)
            ->setName([
                'fr' => "Document d'identité",
                'en' => 'Identity',
            ]);

        $manager->persist($category);

        $category = (new DocumentCategory('chill-doc-store', 20))
            ->setDocumentClass(\Chill\DocStoreBundle\Entity\PersonDocument::class)
            ->setName([
                'fr' => 'Courrier reçu',
                'en' => 'Received email',
            ]);

        $manager->persist($category);

        $manager->flush();
    }
}
