<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\MainBundle\Entity\HasCentersInterface;
use Chill\MainBundle\Entity\HasScopesInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table('chill_doc.accompanyingcourse_document')]
#[ORM\UniqueConstraint(name: 'acc_course_document_unique_stored_object', columns: ['object_id'])]
class AccompanyingCourseDocument extends Document implements HasScopesInterface, HasCentersInterface
{
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriod::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?AccompanyingPeriod $course = null;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    public function getCenters(): ?iterable
    {
        return $this->course->getCenters();
    }

    public function getCourse(): ?AccompanyingPeriod
    {
        return $this->course;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getScopes(): iterable
    {
        if (null === $this->course) {
            return [];
        }

        return $this->course->getScopes();
    }

    public function setCourse(?AccompanyingPeriod $course): self
    {
        $this->course = $course;

        return $this;
    }
}
