<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\MappedSuperclass]
class Document implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    #[ORM\ManyToOne(targetEntity: DocumentCategory::class)]
    #[ORM\JoinColumn(name: 'category_bundle_id', referencedColumnName: 'bundle_id')]
    #[ORM\JoinColumn(name: 'category_id_inside_bundle', referencedColumnName: 'id_inside_bundle')]
    private ?DocumentCategory $category = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $description = '';

    #[Assert\Valid]
    #[Assert\NotNull(message: 'Upload a document')]
    #[ORM\ManyToOne(targetEntity: StoredObject::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name: 'object_id', referencedColumnName: 'id')]
    private ?StoredObject $object = null;

    #[ORM\ManyToOne(targetEntity: DocGeneratorTemplate::class)]
    private ?DocGeneratorTemplate $template = null;

    /**
     * Store the title of the document, if the title is set before the document.
     */
    private string $proxyTitle = '';

    #[ORM\ManyToOne(targetEntity: \Chill\MainBundle\Entity\User::class)]
    private ?\Chill\MainBundle\Entity\User $user = null;

    public function getCategory(): ?DocumentCategory
    {
        return $this->category;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getObject(): ?StoredObject
    {
        return $this->object;
    }

    public function getTemplate(): ?DocGeneratorTemplate
    {
        return $this->template;
    }

    #[Assert\Length(min: 2, max: 250)]
    public function getTitle(): string
    {
        return (string) $this->getObject()?->getTitle();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setCategory(DocumentCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function setDescription($description): self
    {
        $this->description = (string) $description;

        return $this;
    }

    public function setObject(?StoredObject $object = null)
    {
        $this->object = $object;

        if ('' !== $this->proxyTitle) {
            $this->object->setTitle($this->proxyTitle);
        }

        return $this;
    }

    public function setTemplate(?DocGeneratorTemplate $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function setTitle(string $title): self
    {
        if (null !== $this->getObject()) {
            $this->getObject()->setTitle($title);
        } else {
            $this->proxyTitle = $title;
        }

        return $this;
    }

    public function setUser(?\Chill\MainBundle\Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
