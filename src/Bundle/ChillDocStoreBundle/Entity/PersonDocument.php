<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\Scope;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table('chill_doc.person_document')]
#[ORM\UniqueConstraint(name: 'person_document_unique_stored_object', columns: ['object_id'])]
class PersonDocument extends Document implements HasCenterInterface, HasScopeInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    private Person $person;

    /**
     * @var Scope The document's center
     */
    #[ORM\ManyToOne(targetEntity: Scope::class)]
    private ?Scope $scope = null;

    public function getCenter()
    {
        return $this->getPerson()->getCenter();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getScope(): ?Scope
    {
        return $this->scope;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setScope(?Scope $scope): self
    {
        $this->scope = $scope;

        return $this;
    }
}
