<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table('chill_doc.document_category')]
class DocumentCategory
{
    /**
     * @var string The class of the document (ie Chill\DocStoreBundle\PersonDocument)
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, name: 'document_class')]
    private ?string $documentClass = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    private $name;

    /**
     * @param string $bundleId
     * @param int    $idInsideBundle
     */
    public function __construct(
        /**
         * @var string The id of the bundle that has create the category (i.e. 'person', 'activity', ....)
         */
        #[ORM\Id]
        #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, name: 'bundle_id')]
        private $bundleId,
        /**
         * @var int The id which is unique inside the bundle
         */
        #[ORM\Id]
        #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, name: 'id_inside_bundle')]
        private $idInsideBundle,
    ) {}

    public function getBundleId() // ::class BundleClass (FQDN)
    {
        return $this->bundleId;
    }

    public function getDocumentClass()
    {
        return $this->documentClass;
    }

    public function getIdInsideBundle()
    {
        return $this->idInsideBundle;
    }

    public function getName($locale = null)
    {
        if ($locale) {
            if (isset($this->name[$locale])) {
                return $this->name[$locale];
            }

            foreach ($this->name as $name) {
                if (!empty($name)) {
                    return $name;
                }
            }

            return '';
        }

        return $this->name;
    }

    public function setBundleId($id): self
    {
        $this->bundleId = $id;

        return $this;
    }

    public function setDocumentClass(?string $documentClass): self
    {
        $this->documentClass = $documentClass;

        return $this;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }
}
