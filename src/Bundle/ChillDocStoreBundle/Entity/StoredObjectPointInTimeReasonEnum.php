<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

enum StoredObjectPointInTimeReasonEnum: string
{
    case KEEP_BEFORE_CONVERSION = 'keep-before-conversion';
    case KEEP_BY_USER = 'keep-by-user';
}
