<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Represents a snapshot of a stored object at a specific point in time.
 *
 * This entity tracks versions of stored objects, reasons for the snapshot,
 * and the user who initiated the action.
 */
#[ORM\Entity]
#[ORM\Table(name: 'stored_object_point_in_time', schema: 'chill_doc')]
class StoredObjectPointInTime implements TrackCreationInterface
{
    use TrackCreationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: StoredObjectVersion::class, inversedBy: 'pointInTimes')]
        #[ORM\JoinColumn(name: 'stored_object_version_id', nullable: false)]
        private StoredObjectVersion $objectVersion,
        #[ORM\Column(name: 'reason', type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, enumType: StoredObjectPointInTimeReasonEnum::class)]
        private StoredObjectPointInTimeReasonEnum $reason,
        #[ORM\ManyToOne(targetEntity: User::class)]
        private ?User $byUser = null,
    ) {
        $this->objectVersion->addPointInTime($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getByUser(): ?User
    {
        return $this->byUser;
    }

    public function getObjectVersion(): StoredObjectVersion
    {
        return $this->objectVersion;
    }

    public function getReason(): StoredObjectPointInTimeReasonEnum
    {
        return $this->reason;
    }
}
