import { _createI18n } from "../../../../../ChillMainBundle/Resources/public/vuejs/_js/i18n";
import { createApp } from "vue";
import DocumentActionButtonsGroup from "../../vuejs/DocumentActionButtonsGroup.vue";
import { StoredObject, StoredObjectStatusChange } from "../../types";
import { defineComponent } from "vue";
import DownloadButton from "../../vuejs/StoredObjectButton/DownloadButton.vue";
import ToastPlugin from "vue-toast-notification";

const i18n = _createI18n({});

window.addEventListener("DOMContentLoaded", function (e) {
    document
        .querySelectorAll<HTMLDivElement>("div[data-download-button-single]")
        .forEach((el) => {
            const storedObject = JSON.parse(
                el.dataset.storedObject as string,
            ) as StoredObject;
            const title = el.dataset.title as string;
            const app = createApp({
                components: { DownloadButton },
                data() {
                    return {
                        storedObject,
                        title,
                        classes: { btn: true, "btn-outline-primary": true },
                    };
                },
                template:
                    '<download-button :stored-object="storedObject" :at-version="storedObject.currentVersion" :classes="classes" :filename="title" :direct-download="true"></download-button>',
            });

            app.use(i18n).use(ToastPlugin).mount(el);
        });
});
