import { createApp } from "vue";
// @ts-ignore
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import App from "./App.vue";

const appMessages = {
    fr: {
        yes: "Oui",
        are_you_sure: "Êtes-vous sûr·e?",
        you_are_going_to_sign: "Vous allez signer le document",
        signature_confirmation: "Confirmation de la signature",
        sign: "Signer",
        choose_another_signature: "Choisir une autre zone",
        cancel: "Annuler",
        last_sign_zone: "Zone de signature précédente",
        next_sign_zone: "Zone de signature suivante",
        add_sign_zone: "Ajouter une zone de signature",
        click_on_document: "Cliquer sur le document",
        last_zone: "Zone précédente",
        next_zone: "Zone suivante",
        add_zone: "Ajouter une zone",
        another_zone: "Autre zone",
        electronic_signature_in_progress: "Signature électronique en cours...",
        loading: "Chargement...",
        remove_sign_zone: "Enlever la zone",
        return: "Retour",
        see_all_pages: "Voir toutes les pages",
        all_pages: "Toutes les pages",
    },
};

const i18n = _createI18n(appMessages);

const app = createApp({
    template: `<app></app>`,
})
    .use(i18n)
    .component("app", App)
    .mount("#document-signature");
