import {
    StoredObject,
    StoredObjectVersionPersisted,
    StoredObjectVersionWithPointInTime,
} from "../../../types";
import {
    fetchResults,
    makeFetch,
} from "../../../../../../ChillMainBundle/Resources/public/lib/api/apiMethods";

export const get_versions = async (
    storedObject: StoredObject,
): Promise<StoredObjectVersionWithPointInTime[]> => {
    const versions = await fetchResults<StoredObjectVersionWithPointInTime>(
        `/api/1.0/doc-store/stored-object/${storedObject.uuid}/versions`,
    );

    return versions.sort(
        (
            a: StoredObjectVersionWithPointInTime,
            b: StoredObjectVersionWithPointInTime,
        ) => b.version - a.version,
    );
};

export const restore_version = async (
    version: StoredObjectVersionPersisted,
): Promise<StoredObjectVersionWithPointInTime> => {
    return await makeFetch<null, StoredObjectVersionWithPointInTime>(
        "POST",
        `/api/1.0/doc-store/stored-object/restore-from-version/${version.id}`,
    );
};
