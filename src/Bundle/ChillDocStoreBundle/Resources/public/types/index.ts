import { DateTime, User } from "ChillMainAssets/types";
import { SignedUrlGet } from "ChillDocStoreAssets/vuejs/StoredObjectButton/helpers";

export type StoredObjectStatus = "empty" | "ready" | "failure" | "pending";

export interface StoredObject {
    id: number;
    title: string | null;
    uuid: string;
    prefix: string;
    status: StoredObjectStatus;
    currentVersion:
        | null
        | StoredObjectVersionCreated
        | StoredObjectVersionPersisted;
    totalVersions: number;
    datas: object;
    /** @deprecated */
    creationDate: DateTime;
    createdAt: DateTime | null;
    createdBy: User | null;
    _permissions: {
        canEdit: boolean;
        canSee: boolean;
    };
    _links?: {
        dav_link?: {
            href: string;
            expiration: number;
        };
        downloadLink?: SignedUrlGet;
    };
}

export interface StoredObjectVersion {
    /**
     * filename of the object in the object storage
     */
    filename: string;
    iv: number[];
    keyInfos: JsonWebKey;
    type: string;
}

export interface StoredObjectVersionCreated extends StoredObjectVersion {
    persisted: false;
}

export interface StoredObjectVersionPersisted
    extends StoredObjectVersionCreated {
    version: number;
    id: number;
    createdAt: DateTime | null;
    createdBy: User | null;
}

export interface StoredObjectStatusChange {
    id: number;
    filename: string;
    status: StoredObjectStatus;
    type: string;
}

export interface StoredObjectVersionWithPointInTime
    extends StoredObjectVersionPersisted {
    "point-in-times": StoredObjectPointInTime[];
    "from-restored": StoredObjectVersionPersisted | null;
}

export interface StoredObjectPointInTime {
    id: number;
    byUser: User | null;
    reason: "keep-before-conversion" | "keep-by-user";
}

/**
 * Function executed by the WopiEditButton component.
 */
export type WopiEditButtonExecutableBeforeLeaveFunction = () => Promise<void>;

/**
 * Object containing information for performering a POST request to a swift object store
 */
export interface PostStoreObjectSignature {
    method: "POST";
    max_file_size: number;
    max_file_count: 1;
    expires: number;
    submit_delay: 180;
    redirect: string;
    prefix: string;
    url: string;
    signature: string;
}

export interface PDFPage {
    index: number;
    width: number;
    height: number;
}
export interface SignatureZone {
    index: number | null;
    x: number;
    y: number;
    width: number;
    height: number;
    PDFPage: PDFPage;
}

export interface Signature {
    id: number;
    storedObject: StoredObject;
    zones: SignatureZone[];
}

export type SignedState =
    | "pending"
    | "signed"
    | "rejected"
    | "canceled"
    | "error";

export interface CheckSignature {
    state: SignedState;
    storedObject: StoredObject;
}

export type CanvasEvent = "select" | "add";

export interface ZoomLevel {
    id: number;
    zoom: number;
    label: {
        fr?: string;
        nl?: string;
    };
}

export interface GenericDoc {
    type: "doc_store_generic_doc";
    key: string;
    context: "person" | "accompanying-period";
    doc_date: DateTime;
}
