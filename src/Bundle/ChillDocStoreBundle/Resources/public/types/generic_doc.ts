import { DateTime } from "ChillMainAssets/types";
import { StoredObject } from "ChillDocStoreAssets/types/index";

export interface GenericDocMetadata {
    isPresent: boolean;
}

/**
 * Empty metadata for a GenericDoc
 */
// eslint-disable-next-line @typescript-eslint/no-empty-object-type
export interface EmptyMetadata extends GenericDocMetadata {}

/**
 * Minimal Metadata for a GenericDoc with a normalizer
 */
export interface BaseMetadata extends GenericDocMetadata {
    title: string;
}

/**
 * A generic doc is a document attached to a Person or an AccompanyingPeriod.
 */
export interface GenericDoc {
    type: "doc_store_generic_doc";
    uniqueKey: string;
    key: string;
    identifiers: object;
    context: "person" | "accompanying-period";
    doc_date: DateTime;
    metadata: GenericDocMetadata;
    storedObject: StoredObject | null;
}

export interface GenericDocForAccompanyingPeriod extends GenericDoc {
    context: "accompanying-period";
}

interface BaseMetadataWithHtml extends BaseMetadata {
    html: string;
}

export interface GenericDocForAccompanyingCourseDocument
    extends GenericDocForAccompanyingPeriod {
    key: "accompanying_course_document";
    metadata: BaseMetadataWithHtml;
}

export interface GenericDocForAccompanyingCourseActivityDocument
    extends GenericDocForAccompanyingPeriod {
    key: "accompanying_course_activity_document";
    metadata: BaseMetadataWithHtml;
}

export interface GenericDocForAccompanyingCourseCalendarDocument
    extends GenericDocForAccompanyingPeriod {
    key: "accompanying_course_calendar_document";
    metadata: BaseMetadataWithHtml;
}

export interface GenericDocForAccompanyingCoursePersonDocument
    extends GenericDocForAccompanyingPeriod {
    key: "person_document";
    metadata: BaseMetadataWithHtml;
}

export interface GenericDocForAccompanyingCourseWorkEvaluationDocument
    extends GenericDocForAccompanyingPeriod {
    key: "accompanying_period_work_evaluation_document";
    metadata: BaseMetadataWithHtml;
}
