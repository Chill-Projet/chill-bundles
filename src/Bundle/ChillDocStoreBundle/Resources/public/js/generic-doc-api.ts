import { fetchResults } from "ChillMainAssets/lib/api/apiMethods";
import { GenericDocForAccompanyingPeriod } from "ChillDocStoreAssets/types/generic_doc";

export function fetch_generic_docs_by_accompanying_period(
    periodId: number,
): Promise<GenericDocForAccompanyingPeriod[]> {
    return fetchResults(
        `/api/1.0/doc-store/generic-doc/by-period/${periodId}/index`,
    );
}
