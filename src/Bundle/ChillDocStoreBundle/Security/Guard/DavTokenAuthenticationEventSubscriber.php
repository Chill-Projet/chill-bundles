<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Guard;

use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Store some data from the JWT's payload inside the token's attributes.
 */
class DavTokenAuthenticationEventSubscriber implements EventSubscriberInterface
{
    final public const STORED_OBJECT = 'stored_object';
    final public const ACTIONS = 'stored_objects_actions';

    public static function getSubscribedEvents(): array
    {
        return [
            Events::JWT_AUTHENTICATED => ['onJWTAuthenticated', 0],
        ];
    }

    public function onJWTAuthenticated(JWTAuthenticatedEvent $event): void
    {
        $payload = $event->getPayload();

        if (!(array_key_exists('dav', $payload) && 1 === $payload['dav'])) {
            return;
        }

        $token = $event->getToken();
        $token->setAttribute(self::ACTIONS, match ($payload['e']) {
            0 => StoredObjectRoleEnum::SEE,
            1 => StoredObjectRoleEnum::EDIT,
            default => throw new \UnexpectedValueException('unsupported value for e parameter'),
        });

        $token->setAttribute(self::STORED_OBJECT, $payload['so']);
    }
}
