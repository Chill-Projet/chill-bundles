<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Guard;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;

/**
 * Provide a JWT Token which will be valid for viewing or editing a document.
 */
interface JWTDavTokenProviderInterface
{
    public function createToken(StoredObject $storedObject, StoredObjectRoleEnum $roleEnum): string;

    public function getTokenExpiration(string $tokenString): \DateTimeImmutable;
}
