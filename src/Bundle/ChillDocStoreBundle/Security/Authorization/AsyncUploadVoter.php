<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Authorization;

use Chill\DocStoreBundle\AsyncUpload\SignedUrl;
use Chill\DocStoreBundle\Repository\StoredObjectRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

final class AsyncUploadVoter extends Voter
{
    public const GENERATE_SIGNATURE = 'CHILL_DOC_GENERATE_ASYNC_SIGNATURE';

    public function __construct(
        private readonly Security $security,
        private readonly StoredObjectRepository $storedObjectRepository,
    ) {}

    protected function supports($attribute, $subject): bool
    {
        return self::GENERATE_SIGNATURE === $attribute && $subject instanceof SignedUrl;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var SignedUrl $subject */
        if (!in_array($subject->method, ['POST', 'GET', 'HEAD', 'PUT'], true)) {
            return false;
        }

        $storedObject = $this->storedObjectRepository->findOneBy(['filename' => $subject->object_name]);

        return match ($subject->method) {
            'GET', 'HEAD' => $this->security->isGranted(StoredObjectRoleEnum::SEE->value, $storedObject),
            'PUT' => $this->security->isGranted(StoredObjectRoleEnum::EDIT->value, $storedObject),
            'POST' => $this->security->isGranted('ROLE_USER') || $this->security->isGranted('ROLE_ADMIN'),
        };
    }
}
