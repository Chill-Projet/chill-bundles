<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Authorization\StoredObjectVoter;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Repository\AccompanyingCourseDocumentRepository;
use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\MainBundle\Workflow\Helper\WorkflowRelatedEntityPermissionHelper;
use Symfony\Component\Security\Core\Security;

final class AccompanyingCourseDocumentStoredObjectVoter extends AbstractStoredObjectVoter
{
    public function __construct(
        private readonly AccompanyingCourseDocumentRepository $repository,
        Security $security,
        WorkflowRelatedEntityPermissionHelper $workflowDocumentService,
    ) {
        parent::__construct($security, $workflowDocumentService);
    }

    protected function getRepository(): AssociatedEntityToStoredObjectInterface
    {
        return $this->repository;
    }

    protected function attributeToRole(StoredObjectRoleEnum $attribute): string
    {
        return match ($attribute) {
            StoredObjectRoleEnum::EDIT => AccompanyingCourseDocumentVoter::UPDATE,
            StoredObjectRoleEnum::SEE => AccompanyingCourseDocumentVoter::SEE_DETAILS,
        };
    }

    protected function getClass(): string
    {
        return AccompanyingCourseDocument::class;
    }

    protected function canBeAssociatedWithWorkflow(): bool
    {
        return true;
    }
}
