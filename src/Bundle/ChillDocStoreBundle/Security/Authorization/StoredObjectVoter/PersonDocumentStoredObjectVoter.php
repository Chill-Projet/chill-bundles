<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Authorization\StoredObjectVoter;

use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\DocStoreBundle\Repository\AssociatedEntityToStoredObjectInterface;
use Chill\DocStoreBundle\Repository\PersonDocumentRepository;
use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\MainBundle\Workflow\Helper\WorkflowRelatedEntityPermissionHelper;
use Symfony\Component\Security\Core\Security;

class PersonDocumentStoredObjectVoter extends AbstractStoredObjectVoter
{
    public function __construct(
        private readonly PersonDocumentRepository $repository,
        Security $security,
        WorkflowRelatedEntityPermissionHelper $workflowDocumentService,
    ) {
        parent::__construct($security, $workflowDocumentService);
    }

    protected function getRepository(): AssociatedEntityToStoredObjectInterface
    {
        return $this->repository;
    }

    protected function getClass(): string
    {
        return PersonDocument::class;
    }

    protected function attributeToRole(StoredObjectRoleEnum $attribute): string
    {
        return match ($attribute) {
            StoredObjectRoleEnum::EDIT => PersonDocumentVoter::UPDATE,
            StoredObjectRoleEnum::SEE => PersonDocumentVoter::SEE_DETAILS,
        };
    }

    protected function canBeAssociatedWithWorkflow(): bool
    {
        return true;
    }
}
