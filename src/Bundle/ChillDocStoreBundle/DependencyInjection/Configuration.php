<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_doc_store');
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        /* @phpstan-ignore-next-line As there are inconsistencies in return types, but the code works... */
        $rootNode->children()
            ->enumNode('use_driver')
            ->values(['local_storage', 'openstack'])
            ->info('Driver to use. Default to the single one if multiple driver are defined. Configuration will raise an error if there are multiple drivers defined, and if this key is not set')
            ->end()
            ->arrayNode('local_storage')
            ->info('where the stored object should be stored')
            ->children()
            ->scalarNode('storage_path')
            ->info('the folder where the stored object should be stored')
            ->isRequired()->cannotBeEmpty()
            ->end() // end of storage_path
            ->end() // end of children
            ->end() // end of local_storage
            // openstack node
            ->arrayNode('openstack')
            ->info('parameters to authenticate and generate temp url against the openstack object storage service')
            ->children()
            // openstack.temp_url
            ->arrayNode('temp_url')
            ->addDefaultsIfNotSet()
            ->children()
            // openstack.temp_url.temp_url_key
            ->scalarNode('temp_url_key')
            ->isRequired()->cannotBeEmpty()
            ->info('the temp url key')
            ->end()

            ->scalarNode('temp_url_base_path')
            ->isRequired()->cannotBeEmpty()
            ->info('the base path to add **before** the path to media. Must contains the container')
            ->end()

            ->scalarNode('container')
            ->info('the container name')
            ->isRequired()->cannotBeEmpty()
            ->end()

            ->integerNode('max_post_file_size')
            ->defaultValue(15_000_000)
            ->info('Maximum size of the posted file, in bytes')
            ->end()

            ->integerNode('max_post_file_count')
            ->defaultValue(1)
            ->info('Maximum number of files which may be posted at once using a POST operation using async upload')
            ->end()

            ->integerNode('max_expires_delay')
            ->defaultValue(180)
            ->info('the maximum of seconds a cryptographic signature '
                .'will be valid for submitting a file. This should be '
                .'short, to avoid uploading multiple files')
            ->end()

            ->integerNode('max_submit_delay')
            ->defaultValue(3600)
            ->info('the maximum of seconds between the upload of a file and '
                .'a the submission of the form. This delay will also prevent '
                .'the check of persistence of uploaded file. Should be long '
                .'enough for keeping user-friendly forms')
            ->end()

            ->end() // end of children 's temp_url
            ->end() // end array temp_url

            ->end() // end of children's openstack
            ->end() // end of openstack
            ->end() // end of root's children
            ->end();

        return $treeBuilder;
    }
}
