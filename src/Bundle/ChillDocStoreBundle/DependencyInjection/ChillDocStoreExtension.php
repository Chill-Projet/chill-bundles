<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\DependencyInjection;

use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectVoterInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillDocStoreExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('chill_doc_store', $config);

        $container->registerForAutoconfiguration(StoredObjectVoterInterface::class)->addTag('stored_object_voter');

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/form.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/security.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoute($container);
        $this->prependAuthorization($container);
        $this->prependTwig($container);
    }

    private function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                PersonDocumentVoter::UPDATE => [PersonDocumentVoter::SEE_DETAILS],
                PersonDocumentVoter::CREATE => [PersonDocumentVoter::SEE_DETAILS],
                PersonDocumentVoter::DELETE => [PersonDocumentVoter::SEE_DETAILS],
                PersonDocumentVoter::SEE_DETAILS => [PersonDocumentVoter::SEE],
                AccompanyingCourseDocumentVoter::UPDATE => [AccompanyingCourseDocumentVoter::SEE_DETAILS],
                AccompanyingCourseDocumentVoter::CREATE => [AccompanyingCourseDocumentVoter::SEE_DETAILS],
                AccompanyingCourseDocumentVoter::DELETE => [AccompanyingCourseDocumentVoter::SEE_DETAILS],
                AccompanyingCourseDocumentVoter::SEE_DETAILS => [AccompanyingCourseDocumentVoter::SEE],
            ],
        ]);
    }

    private function prependRoute(ContainerBuilder $container)
    {
        // declare routes for task bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillDocStoreBundle/config/routes.yaml',
                ],
            ],
        ]);
    }

    private function prependTwig(ContainerBuilder $container)
    {
        $twigConfig = [
            'form_themes' => ['@ChillDocStore/Form/fields.html.twig'],
        ];
        $container->prependExtensionConfig('twig', $twigConfig);
    }
}
