<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Form\DataMapper\StoredObjectDataMapper;
use Chill\DocStoreBundle\Form\DataTransformer\StoredObjectDataTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form type which allow to join a document.
 */
final class StoredObjectType extends AbstractType
{
    public function __construct(
        private readonly StoredObjectDataTransformer $storedObjectDataTransformer,
        private readonly StoredObjectDataMapper $storedObjectDataMapper,
    ) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (true === $options['has_title']) {
            $builder
                ->add('title', TextType::class, [
                    'label' => 'Title',
                ]);
        }

        $builder->add('stored_object', HiddenType::class);
        $builder->get('stored_object')->addModelTransformer($this->storedObjectDataTransformer);
        $builder->setDataMapper($this->storedObjectDataMapper);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', StoredObject::class);

        $resolver
            ->setDefault('has_title', false)
            ->setAllowedTypes('has_title', ['bool']);
    }
}
