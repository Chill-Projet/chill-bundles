<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\MainBundle\Form\Type\ChillCollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionStoredObjectType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('entry_type', StoredObjectType::class)
            ->setDefault('allow_add', true)
            ->setDefault('allow_delete', true)
            ->setDefault('button_add_label', 'stored_object.Insert a document')
            ->setDefault('button_remove_label', 'stored_object.Remove a document')
            ->setDefault('empty_collection_explain', 'No documents')
            ->setDefault('entry_options', ['has_title' => true])
            ->setDefault('js_caller', 'data-collection-stored-object');
    }

    public function getParent()
    {
        return ChillCollectionType::class;
    }
}
