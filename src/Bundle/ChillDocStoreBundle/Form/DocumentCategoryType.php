<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentCategoryType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $bundles = [
            'chill-doc-store' => 'chill-doc-store',
        ];

        $documentClasses = [
            $this->translator->trans('Accompanying period document') => \Chill\DocStoreBundle\Entity\AccompanyingCourseDocument::class,
            $this->translator->trans('Person document') => \Chill\DocStoreBundle\Entity\PersonDocument::class,
        ];

        $builder
            ->add('bundleId', ChoiceType::class, [
                'choices' => $bundles,
                'disabled' => false,
            ])
            ->add('idInsideBundle', null, [
                'disabled' => true,
            ])
            ->add('documentClass', ChoiceType::class, [
                'choices' => $documentClasses,
                'expanded' => false,
                'required' => true,
                'disabled' => false,
            ])
            ->add('name', TranslatableStringFormType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentCategory::class,
        ]);
    }
}
