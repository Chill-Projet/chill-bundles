<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\Entity\StoredObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class StoredObjectStatusApiController
{
    public function __construct(private readonly Security $security) {}

    #[Route(path: '/api/1.0/doc-store/stored-object/{uuid}/is-ready')]
    public function isDocumentReady(StoredObject $storedObject): Response
    {
        if (!$this->security->isGranted('ROLE_USER')) {
            throw new AccessDeniedHttpException();
        }

        return new JsonResponse(
            [
                'id' => $storedObject->getId(),
                'filename' => $storedObject->getFilename(),
                'status' => $storedObject->getStatus(),
                'type' => $storedObject->getType(),
            ]
        );
    }
}
