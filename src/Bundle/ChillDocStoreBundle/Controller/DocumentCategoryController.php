<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\ChillDocStoreBundle;
use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\DocStoreBundle\Form\DocumentCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/{_locale}/admin/document/category')]
class DocumentCategoryController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry) {}

    #[Route(path: '/{bundleId}/{idInsideBundle}', name: 'document_category_delete', methods: 'DELETE')]
    public function delete(Request $request, mixed $bundleId, mixed $idInsideBundle): Response
    {
        $em = $this->managerRegistry->getManager();
        $documentCategory = $em
            ->getRepository(DocumentCategory::class)
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]
            );

        if ($this->isCsrfTokenValid('delete'.$bundleId.$idInsideBundle, $request->request->get('_token'))) {
            $em->remove($documentCategory);
            $em->flush();
        }

        return $this->redirectToRoute('document_category_index');
    }

    #[Route(path: '/{bundleId}/{idInsideBundle}/edit', name: 'document_category_edit', methods: 'GET|POST')]
    public function edit(Request $request, mixed $bundleId, mixed $idInsideBundle): Response
    {
        $em = $this->managerRegistry->getManager();
        $documentCategory = $em
            ->getRepository(DocumentCategory::class)
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]
            );

        $form = $this->createForm(DocumentCategoryType::class, $documentCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('document_category_index', [
                'bundleId' => $documentCategory->getBundleId(),
                'idInsideBundle' => $documentCategory->getIdInsideBundle(), ]);
        }

        return $this->render('@ChillDocStore/DocumentCategory/edit.html.twig', [
            'document_category' => $documentCategory,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/', name: 'document_category_index', methods: 'GET')]
    #[Route(path: '/', name: 'chill_docstore_category_admin', methods: 'GET')]
    public function index(): Response
    {
        $em = $this->managerRegistry->getManager();
        $categories = $em->getRepository(DocumentCategory::class)->findAll();

        return $this->render(
            '@ChillDocStore/DocumentCategory/index.html.twig',
            [
                'document_categories' => $categories,
            ]
        );
    }

    #[Route(path: '/new', name: 'document_category_new', methods: 'GET|POST')]
    public function new(Request $request): Response
    {
        $em = $this->managerRegistry->getManager();

        $nextId = $em
            ->createQuery(
                'SELECT (CASE WHEN MAX(c.idInsideBundle) IS NULL THEN 1 ELSE MAX(c.idInsideBundle) + 1 END)
         FROM ChillDocStoreBundle:DocumentCategory c'
            )
            ->getSingleScalarResult();

        $documentCategory = new DocumentCategory(
            ChillDocStoreBundle::class,
            $nextId
        );

        $documentCategory
            ->setDocumentClass(PersonDocument::class);

        $form = $this->createForm(DocumentCategoryType::class, $documentCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $em->persist($documentCategory);
            $em->flush();

            return $this->redirectToRoute('document_category_index');
        }

        return $this->render('@ChillDocStore/DocumentCategory/new.html.twig', [
            'document_category' => $documentCategory,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{bundleId}/{idInsideBundle}', name: 'document_category_show', methods: 'GET')]
    public function show(mixed $bundleId, mixed $idInsideBundle): Response
    {
        $em = $this->managerRegistry->getManager();
        $documentCategory = $em
            ->getRepository(DocumentCategory::class)
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]
            );

        return $this->render(
            '@ChillDocStore/DocumentCategory/show.html.twig',
            ['document_category' => $documentCategory]
        );
    }
}
