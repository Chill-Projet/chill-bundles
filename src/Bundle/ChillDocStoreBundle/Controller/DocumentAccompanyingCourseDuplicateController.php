<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\DocStoreBundle\Workflow\AccompanyingCourseDocumentDuplicator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

final readonly class DocumentAccompanyingCourseDuplicateController
{
    public function __construct(
        private Security $security,
        private AccompanyingCourseDocumentDuplicator $documentWorkflowDuplicator,
        private EntityManagerInterface $entityManager,
        private UrlGeneratorInterface $urlGenerator,
    ) {}

    #[Route('/{_locale}/doc-store/accompanying-course-document/{id}/duplicate', name: 'chill_doc_store_accompanying_course_document_duplicate')]
    public function __invoke(AccompanyingCourseDocument $document, Request $request, Session $session): Response
    {
        if (!$this->security->isGranted(AccompanyingCourseDocumentVoter::SEE, $document)) {
            throw new AccessDeniedHttpException('not allowed to see this document');
        }

        if (!$this->security->isGranted(AccompanyingCourseDocumentVoter::CREATE, $document->getCourse())) {
            throw new AccessDeniedHttpException('not allowed to create this document');
        }

        $duplicated = $this->documentWorkflowDuplicator->duplicate($document);
        $this->entityManager->persist($duplicated);
        $this->entityManager->flush();

        return new RedirectResponse(
            $this->urlGenerator->generate('accompanying_course_document_edit', ['id' => $duplicated->getId(), 'course' => $duplicated->getCourse()->getId()])
        );
    }
}
