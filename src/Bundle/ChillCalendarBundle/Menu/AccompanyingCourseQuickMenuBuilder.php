<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Menu;

use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;

final readonly class AccompanyingCourseQuickMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private Security $security) {}

    public static function getMenuIds(): array
    {
        return ['accompanying_course_quick_menu'];
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var \Chill\PersonBundle\Entity\AccompanyingPeriod $accompanyingCourse */
        $accompanyingCourse = $parameters['accompanying-course'];

        if ($this->security->isGranted(CalendarVoter::CREATE, $accompanyingCourse)) {
            $menu
                ->addChild('Create a new calendar in accompanying course', [
                    'route' => 'chill_calendar_calendar_new',
                    'routeParameters' => [
                        'accompanying_period_id' => $accompanyingCourse->getId(),
                    ],
                ])
                ->setExtras([
                    'order' => 20,
                    'icon' => 'plus',
                ])
            ;
        }
    }
}
