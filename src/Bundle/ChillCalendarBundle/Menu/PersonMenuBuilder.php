<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Menu;

use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private readonly Security $security, protected TranslatorInterface $translator) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        $person = $parameters['person'];

        if ($this->security->isGranted(CalendarVoter::SEE, $person)) {
            $menu->addChild($this->translator->trans('Calendar'), [
                'route' => 'chill_calendar_calendar_list_by_person',
                'routeParameters' => [
                    'id' => $person->getId(),
                ], ])
                ->setExtras(['order' => 198]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
