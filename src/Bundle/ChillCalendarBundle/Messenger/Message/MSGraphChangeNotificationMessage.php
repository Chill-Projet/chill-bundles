<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

class MSGraphChangeNotificationMessage
{
    public function __construct(private readonly array $content, private readonly int $userId) {}

    public function getContent(): array
    {
        return $this->content;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
