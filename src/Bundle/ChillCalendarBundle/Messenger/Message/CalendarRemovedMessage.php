<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\MainBundle\Entity\User;

class CalendarRemovedMessage
{
    private ?int $associatedCalendarRangeId = null;

    private ?int $byUserId = null;

    private readonly int $calendarUserId;

    private readonly array $remoteAttributes;

    private readonly string $remoteId;

    public function __construct(Calendar $calendar, ?User $byUser)
    {
        $this->remoteId = $calendar->getRemoteId();
        $this->remoteAttributes = $calendar->getRemoteAttributes();
        $this->calendarUserId = $calendar->getMainUser()->getId();

        if ($calendar->hasCalendarRange()) {
            $this->associatedCalendarRangeId = $calendar->getCalendarRange()->getId();
        }

        if (null !== $byUser) {
            $this->byUserId = $byUser->getId();
        }
    }

    public function getAssociatedCalendarRangeId(): ?int
    {
        return $this->associatedCalendarRangeId;
    }

    public function getByUserId(): ?int
    {
        return $this->byUserId;
    }

    public function getCalendarUserId(): ?int
    {
        return $this->calendarUserId;
    }

    public function getRemoteAttributes(): array
    {
        return $this->remoteAttributes;
    }

    public function getRemoteId(): string
    {
        return $this->remoteId;
    }
}
