<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

use Chill\CalendarBundle\Entity\Invite;
use Chill\MainBundle\Entity\User;

class InviteUpdateMessage
{
    private readonly int $byUserId;

    private readonly int $inviteId;

    public function __construct(Invite $invite, User $byUser)
    {
        $this->inviteId = $invite->getId();
        $this->byUserId = $byUser->getId();
    }

    public function getByUserId(): int
    {
        return $this->byUserId;
    }

    public function getInviteId(): int
    {
        return $this->inviteId;
    }
}
