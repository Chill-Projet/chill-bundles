<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Entity\Invite;
use Chill\MainBundle\Entity\User;

class CalendarMessage
{
    final public const CALENDAR_PERSIST = 'CHILL_CALENDAR_CALENDAR_PERSIST';

    final public const CALENDAR_UPDATE = 'CHILL_CALENDAR_CALENDAR_UPDATE';

    private readonly int $byUserId;

    private readonly int $calendarId;

    private array $newInvitesIds = [];

    /**
     * @var array<array{inviteId: int, userId: int, userEmail: int, userLabel: string}>
     */
    private array $oldInvites = [];

    private ?int $previousCalendarRangeId = null;

    private ?int $previousMainUserId = null;

    public function __construct(
        Calendar $calendar,
        private readonly string $action,
        User $byUser,
    ) {
        $this->calendarId = $calendar->getId();
        $this->byUserId = $byUser->getId();
        $this->previousCalendarRangeId = null !== $calendar->previousCalendarRange ?
            $calendar->previousCalendarRange->getId() : null;
        $this->previousMainUserId = null !== $calendar->previousMainUser ?
            $calendar->previousMainUser->getId() : null;
        $this->newInvitesIds = array_map(static fn (Invite $i) => $i->getId(), $calendar->newInvites);
        $this->oldInvites = array_map(static fn (Invite $i) => [
            'inviteId' => $i->getId(),
            'userId' => $i->getUser()->getId(),
            'userEmail' => $i->getUser()->getEmail(),
            'userLabel' => $i->getUser()->getLabel(),
        ], $calendar->oldInvites);
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getByUserId(): ?int
    {
        return $this->byUserId;
    }

    public function getCalendarId(): ?int
    {
        return $this->calendarId;
    }

    /**
     * @return array|int[]|null[]
     */
    public function getNewInvitesIds(): array
    {
        return $this->newInvitesIds;
    }

    /**
     * @return array<array{inviteId: int, userId: int, userEmail: int, userLabel: string}>
     */
    public function getOldInvites(): array
    {
        return $this->oldInvites;
    }

    public function getPreviousCalendarRangeId(): ?int
    {
        return $this->previousCalendarRangeId;
    }

    public function getPreviousMainUserId(): ?int
    {
        return $this->previousMainUserId;
    }
}
