<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Handler;

use Chill\CalendarBundle\Messenger\Message\CalendarRangeRemovedMessage;
use Chill\CalendarBundle\RemoteCalendar\Connector\RemoteCalendarConnectorInterface;
use Chill\MainBundle\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Remove a calendar range when it is removed from local calendar.
 *
 * @AsMessageHandler
 */
class CalendarRangeRemoveToRemoteHandler implements MessageHandlerInterface
{
    public function __construct(private readonly RemoteCalendarConnectorInterface $remoteCalendarConnector, private readonly UserRepository $userRepository) {}

    public function __invoke(CalendarRangeRemovedMessage $calendarRangeRemovedMessage)
    {
        $this->remoteCalendarConnector->removeCalendarRange(
            $calendarRangeRemovedMessage->getRemoteId(),
            $calendarRangeRemovedMessage->getRemoteAttributes(),
            $this->userRepository->find($calendarRangeRemovedMessage->getCalendarRangeUserId())
        );
    }
}
