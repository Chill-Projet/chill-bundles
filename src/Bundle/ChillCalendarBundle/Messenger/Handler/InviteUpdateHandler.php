<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Handler;

use Chill\CalendarBundle\Messenger\Message\InviteUpdateMessage;
use Chill\CalendarBundle\RemoteCalendar\Connector\RemoteCalendarConnectorInterface;
use Chill\CalendarBundle\Repository\InviteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Sync the local invitation to the remote calendar.
 *
 * @AsMessageHandler
 */
class InviteUpdateHandler implements MessageHandlerInterface
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly InviteRepository $inviteRepository, private readonly RemoteCalendarConnectorInterface $remoteCalendarConnector) {}

    public function __invoke(InviteUpdateMessage $inviteUpdateMessage): void
    {
        if (null === $invite = $this->inviteRepository->find($inviteUpdateMessage->getInviteId())) {
            return;
        }

        $this->remoteCalendarConnector->syncInvite($invite);

        $this->em->flush();
    }
}
