<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Form;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Entity\CancelReason;
use Chill\CalendarBundle\Form\DataTransformer\IdToCalendarRangeDataTransformer;
use Chill\MainBundle\Form\DataTransformer\IdToLocationDataTransformer;
use Chill\MainBundle\Form\DataTransformer\IdToUserDataTransformer;
use Chill\MainBundle\Form\DataTransformer\IdToUsersDataTransformer;
use Chill\MainBundle\Form\Type\CommentType;
use Chill\MainBundle\Form\Type\PrivateCommentType;
use Chill\PersonBundle\Form\DataTransformer\PersonsToIdDataTransformer;
use Chill\ThirdPartyBundle\Form\DataTransformer\ThirdPartiesToIdDataTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarType extends AbstractType
{
    public function __construct(
        private readonly PersonsToIdDataTransformer $personsToIdDataTransformer,
        private readonly IdToUserDataTransformer $idToUserDataTransformer,
        private readonly IdToUsersDataTransformer $idToUsersDataTransformer,
        private readonly IdToLocationDataTransformer $idToLocationDataTransformer,
        private readonly ThirdPartiesToIdDataTransformer $partiesToIdDataTransformer,
        private readonly IdToCalendarRangeDataTransformer $calendarRangeDataTransformer,
    ) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', CommentType::class, [
                'required' => false,
            ])
            ->add('privateComment', PrivateCommentType::class, [
                'required' => false,
                'label' => 'private comment',
            ])
            // ->add('cancelReason', EntityType::class, [
            //     'required' => false,
            //     'class' => CancelReason::class,
            //     'choice_label' => function (CancelReason $entity) {
            //         return $entity->getCanceledBy();
            //     },
            // ])
            ->add('sendSMS', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ]);

        $builder->add('mainUser', HiddenType::class);
        $builder->get('mainUser')->addModelTransformer($this->idToUserDataTransformer);

        $builder->add('startDate', HiddenType::class);
        $builder->get('startDate')
            ->addModelTransformer(new CallbackTransformer(
                static function (?\DateTimeImmutable $dateTimeImmutable): string {
                    if (null !== $dateTimeImmutable) {
                        $res = date_format($dateTimeImmutable, \DateTimeImmutable::ATOM);
                    } else {
                        $res = '';
                    }

                    return $res;
                },
                static function (?string $dateAsString): ?\DateTimeImmutable {
                    if ('' === $dateAsString || null === $dateAsString) {
                        return null;
                    }

                    return \DateTimeImmutable::createFromFormat(\DateTimeImmutable::ATOM, $dateAsString);
                }
            ));

        $builder->add('endDate', HiddenType::class);
        $builder->get('endDate')
            ->addModelTransformer(new CallbackTransformer(
                static function (?\DateTimeImmutable $dateTimeImmutable): string {
                    if (null !== $dateTimeImmutable) {
                        $res = date_format($dateTimeImmutable, \DateTimeImmutable::ATOM);
                    } else {
                        $res = '';
                    }

                    return $res;
                },
                static function (?string $dateAsString): ?\DateTimeImmutable {
                    if ('' === $dateAsString || null === $dateAsString) {
                        return null;
                    }

                    return \DateTimeImmutable::createFromFormat(\DateTimeImmutable::ATOM, $dateAsString);
                }
            ));

        $builder->add('persons', HiddenType::class);
        $builder->get('persons')
            ->addModelTransformer($this->personsToIdDataTransformer);

        $builder->add('professionals', HiddenType::class);
        $builder->get('professionals')
            ->addModelTransformer($this->partiesToIdDataTransformer);

        $builder->add('users', HiddenType::class);
        $builder->get('users')
            ->addModelTransformer($this->idToUsersDataTransformer);

        $builder->add('calendarRange', HiddenType::class);
        $builder->get('calendarRange')
            ->addModelTransformer($this->calendarRangeDataTransformer);

        $builder->add('location', HiddenType::class)
            ->get('location')
            ->addModelTransformer($this->idToLocationDataTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Calendar::class,
        ]);
    }

    public function getBlockPrefix()
    {
        // as the js share some hardcoded items from activity, we have to rewrite block prefix
        return 'chill_activitybundle_activity';
    }
}
