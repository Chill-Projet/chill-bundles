<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Entity\CalendarRange;
use Chill\CalendarBundle\Entity\Invite;
use Chill\CalendarBundle\RemoteCalendar\Model\RemoteEvent;
use Chill\MainBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

interface RemoteCalendarConnectorInterface
{
    public function countEventsForUser(User $user, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate): int;

    /**
     * Return a response, more probably a RedirectResponse, where the user
     * will be able to fullfill requirements to prepare this connector and
     * make it ready.
     */
    public function getMakeReadyResponse(string $returnPath): Response;

    /**
     * Return true if the connector is ready to act as a proxy for reading
     * remote calendars.
     */
    public function isReady(): bool;

    /**
     * @return array|RemoteEvent[]
     */
    public function listEventsForUser(User $user, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate, ?int $offset = 0, ?int $limit = 50): array;

    public function removeCalendar(string $remoteId, array $remoteAttributes, User $user, ?CalendarRange $associatedCalendarRange = null): void;

    public function removeCalendarRange(string $remoteId, array $remoteAttributes, User $user): void;

    /**
     * @param array<array{inviteId: int, userId: int, userEmail: int, userLabel: string}> $oldInvites
     */
    public function syncCalendar(Calendar $calendar, string $action, ?CalendarRange $previousCalendarRange, ?User $previousMainUser, ?array $oldInvites, ?array $newInvites): void;

    public function syncCalendarRange(CalendarRange $calendarRange): void;

    public function syncInvite(Invite $invite): void;
}
