<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph;

use Chill\MainBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Clock\ClockInterface;

readonly class MSUserAbsenceSync
{
    public function __construct(
        private MSUserAbsenceReaderInterface $absenceReader,
        private ClockInterface $clock,
        private LoggerInterface $logger,
    ) {}

    public function syncUserAbsence(User $user): void
    {
        $absence = $this->absenceReader->isUserAbsent($user);

        if (null === $absence) {
            return;
        }

        if ($absence === $user->isAbsent()) {
            // nothing to do
            return;
        }

        $this->logger->info('will change user absence', ['userId' => $user->getId()]);

        if ($absence) {
            $this->logger->debug('make user absent', ['userId' => $user->getId()]);
            $user->setAbsenceStart($this->clock->now());
        } else {
            $this->logger->debug('make user present', ['userId' => $user->getId()]);
            $user->setAbsenceStart(null);
        }
    }
}
