<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Model;

use Symfony\Component\Serializer\Annotation as Serializer;

class RemoteEvent
{
    public function __construct(
        #[Serializer\Groups(['read'])]
        public string $id,
        #[Serializer\Groups(['read'])]
        public string $title,
        public string $description,
        #[Serializer\Groups(['read'])]
        public \DateTimeImmutable $startDate,
        #[Serializer\Groups(['read'])]
        public \DateTimeImmutable $endDate,
        #[Serializer\Groups(['read'])]
        public bool $isAllDay = false,
    ) {}
}
