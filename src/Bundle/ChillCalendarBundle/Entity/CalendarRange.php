<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\Table(name: 'chill_calendar.calendar_range')]
#[ORM\UniqueConstraint(name: 'idx_calendar_range_remote', columns: ['remoteId'], options: ['where' => "remoteId <> ''"])]
class CalendarRange implements TrackCreationInterface, TrackUpdateInterface
{
    use RemoteCalendarTrait;

    use TrackCreationTrait;

    use TrackUpdateTrait;

    #[ORM\OneToOne(targetEntity: Calendar::class, mappedBy: 'calendarRange')]
    private ?Calendar $calendar = null;

    #[Groups(['read', 'write', 'calendar:read'])]
    #[Assert\NotNull]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: false)]
    private ?\DateTimeImmutable $endDate = null;

    #[Groups(['read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Groups(['read', 'write', 'calendar:read'])]
    #[Assert\NotNull]
    #[ORM\ManyToOne(targetEntity: Location::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Location $location = null;

    #[Groups(['read', 'write', 'calendar:read'])]
    #[Assert\NotNull]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: false)]
    private ?\DateTimeImmutable $startDate = null;

    #[Groups(['read', 'write', 'calendar:read'])]
    #[Assert\NotNull]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $user = null;

    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @internal use {@link (Calendar::setCalendarRange)} instead
     */
    public function setCalendar(?Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }

    public function setEndDate(\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
