<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * An invitation for another user to a Calendar.
 *
 * The event/calendar in the user may have a different id than the mainUser. We add then fields to store the
 * remote id of this event in the remote calendar.
 */
#[ORM\Entity]
#[ORM\Table(name: 'chill_calendar.invite')]
#[ORM\UniqueConstraint(name: 'idx_calendar_invite_remote', columns: ['remoteId'], options: ['where' => "remoteId <> ''"])]
class Invite implements TrackUpdateInterface, TrackCreationInterface
{
    use RemoteCalendarTrait;

    use TrackCreationTrait;

    use TrackUpdateTrait;

    final public const ACCEPTED = 'accepted';

    final public const DECLINED = 'declined';

    final public const PENDING = 'pending';

    /**
     * all statuses in one const.
     */
    final public const STATUSES = [
        self::ACCEPTED,
        self::DECLINED,
        self::PENDING,
        self::TENTATIVELY_ACCEPTED,
    ];

    final public const TENTATIVELY_ACCEPTED = 'tentative';

    #[ORM\ManyToOne(targetEntity: Calendar::class, inversedBy: 'invites')]
    private ?Calendar $calendar = null;

    #[Serializer\Groups(groups: ['calendar:read', 'read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(groups: ['calendar:read', 'read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => 'pending'])]
    private string $status = self::PENDING;

    #[Serializer\Groups(groups: ['calendar:read', 'read', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @internal use Calendar::addInvite instead
     */
    public function setCalendar(?Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setUser(?User $user): self
    {
        if ($user instanceof User && $this->user instanceof User && $user !== $this->user) {
            throw new \LogicException('Not allowed to associate an invite to a different user');
        }

        $this->user = $user;

        return $this;
    }
}
