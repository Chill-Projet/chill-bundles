<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait RemoteCalendarTrait
{
    /**
     * If true, the changes won't be enqueued to remote.
     *
     * This is required to prevent update loop: a persist trigger an event creation on remote,
     * which in turn change remoteId and, in turn, trigger an update change, ...
     */
    public bool $preventEnqueueChanges = false;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, options: ['default' => '[]'], nullable: false)]
    private array $remoteAttributes = [];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, options: ['default' => ''], nullable: false)]
    private string $remoteId = '';

    public function addRemoteAttributes(array $remoteAttributes): self
    {
        $this->remoteAttributes = array_merge($this->remoteAttributes, $remoteAttributes);

        return $this;
    }

    public function getRemoteAttributes(): array
    {
        return $this->remoteAttributes;
    }

    public function getRemoteId(): string
    {
        return $this->remoteId;
    }

    public function hasRemoteId(): bool
    {
        return '' !== $this->remoteId;
    }

    public function setRemoteId(string $remoteId): self
    {
        $this->remoteId = $remoteId;

        return $this;
    }
}
