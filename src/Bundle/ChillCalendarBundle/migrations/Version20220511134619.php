<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220511134619 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_invites (calendar_id INT NOT NULL, invite_id INT NOT NULL, PRIMARY KEY(calendar_id, invite_id))');
        $this->addSql('CREATE INDEX idx_fcbeaaaea417747 ON chill_calendar.calendar_to_invites (invite_id)');
        $this->addSql('CREATE INDEX idx_fcbeaaaa40a2c8 ON chill_calendar.calendar_to_invites (calendar_id)');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites ADD CONSTRAINT fk_fcbeaaaa40a2c8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites ADD CONSTRAINT fk_fcbeaaaea417747 FOREIGN KEY (invite_id) REFERENCES chill_calendar.invite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP CONSTRAINT FK_F517FFA7A40A2C8');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP CONSTRAINT FK_F517FFA73174800F');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP CONSTRAINT FK_F517FFA765FF1AEC');
        $this->addSql('DROP INDEX chill_calendar.IDX_F517FFA7A40A2C8');
        $this->addSql('DROP INDEX chill_calendar.IDX_F517FFA73174800F');
        $this->addSql('DROP INDEX chill_calendar.IDX_F517FFA765FF1AEC');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP calendar_id');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP createdAt');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP updatedAt');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP updatedBy_id');
        $this->addSql('ALTER TABLE chill_calendar.invite ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP COLUMN status');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD COLUMN status JSON');
        $this->addSql('ALTER TABLE chill_calendar.invite ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT FK_712315AC3174800F');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT FK_712315AC65FF1AEC');
        $this->addSql('DROP INDEX chill_calendar.IDX_712315AC3174800F');
        $this->addSql('DROP INDEX chill_calendar.IDX_712315AC65FF1AEC');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP createdAt');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP updatedAt');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP updatedBy_id');
    }

    public function getDescription(): string
    {
        return 'Prepare schema for handling calendar invites';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_calendar.calendar_to_invites');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315AC3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315AC65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_712315AC3174800F ON chill_calendar.calendar (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_712315AC65FF1AEC ON chill_calendar.calendar (updatedBy_id)');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD calendar_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('DELETE FROM chill_calendar.invite WHERE user_id IS NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite DROP COLUMN status');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD COLUMN status TEXT DEFAULT \'pending\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.invite.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.invite.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD CONSTRAINT FK_F517FFA7A40A2C8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD CONSTRAINT FK_F517FFA73174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD CONSTRAINT FK_F517FFA765FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F517FFA7A40A2C8 ON chill_calendar.invite (calendar_id)');
        $this->addSql('CREATE INDEX IDX_F517FFA73174800F ON chill_calendar.invite (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_F517FFA765FF1AEC ON chill_calendar.invite (updatedBy_id)');
    }
}
