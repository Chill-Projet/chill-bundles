<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Aggregator;

use Chill\CalendarBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UrgencyAggregator implements AggregatorInterface
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('cal.urgent AS urgency_aggregator');
        $qb->addGroupBy('urgency_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Urgency';
            }

            return match ($value) {
                true => $this->translator->trans('is urgent'),
                false => $this->translator->trans('is not urgent'),
                default => throw new \LogicException(sprintf('The value %s is not valid', $value)),
            };
        };
    }

    public function getQueryKeys($data): array
    {
        return ['urgency_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group calendars by urgency';
    }
}
