<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Aggregator;

use Chill\CalendarBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserRepository;
use Chill\MainBundle\Templating\Entity\UserRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class AgentAggregator implements AggregatorInterface
{
    public function __construct(private UserRepository $userRepository, private UserRender $userRender) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('caluser', $qb->getAllAliases(), true)) {
            $qb->join('cal.mainUser', 'caluser');
        }

        $qb->addSelect('caluser.id AS agent_aggregator');
        $qb->addGroupBy('agent_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Agent';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $r = $this->userRepository->find($value);

            return $this->userRender->renderString($r, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['agent_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group calendars by agent';
    }
}
