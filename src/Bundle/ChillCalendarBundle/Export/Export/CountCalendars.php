<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Export;

use Chill\CalendarBundle\Export\Declarations;
use Chill\CalendarBundle\Repository\CalendarRepository;
use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Exception\LogicException;

class CountCalendars implements ExportInterface, GroupedExportInterface
{
    public function __construct(
        private readonly CalendarRepository $calendarRepository,
    ) {}

    public function buildForm(FormBuilderInterface $builder)
    {
        // No form necessary
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'Count calendars by various parameters.';
    }

    public function getGroup(): string
    {
        return 'Exports of calendar';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new LogicException("the key {$key} is not used by this export");
        }

        $labels = array_combine($values, $values);
        $labels['_header'] = $this->getTitle();

        return static fn ($value) => $labels[$value];
    }

    public function getQueryKeys($data): array
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(AbstractQuery::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'Count calendars';
    }

    public function getType(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    /**
     * Initiate the query.
     */
    public function initiateQuery(array $requiredModifiers, array $acl, array $data = []): QueryBuilder
    {
        $centers = array_map(static fn ($el) => $el['center'], $acl);

        $qb = $this->calendarRepository->createQueryBuilder('cal');

        $qb->select('COUNT(cal.id) AS export_result');
        $qb->leftJoin('cal.accompanyingPeriod', 'acp');

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        return $qb;
    }

    public function requiredRole(): string
    {
        // which role should we give here?
        return PersonVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::CALENDAR_TYPE,
        ];
    }
}
