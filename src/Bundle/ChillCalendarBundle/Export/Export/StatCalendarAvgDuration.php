<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Export;

use Chill\CalendarBundle\Export\Declarations;
use Chill\CalendarBundle\Repository\CalendarRepository;
use Chill\MainBundle\Export\AccompanyingCourseExportHelper;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class StatCalendarAvgDuration implements ExportInterface, GroupedExportInterface
{
    public function __construct(private readonly CalendarRepository $calendarRepository) {}

    public function buildForm(FormBuilderInterface $builder): void
    {
        // no form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'Get the average of calendar duration according to various filters';
    }

    public function getGroup(): string
    {
        return 'Exports of calendar';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_result' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        $labels = array_combine($values, $values);
        $labels['_header'] = $this->getTitle();

        return static fn ($value) => $labels[$value];
    }

    public function getQueryKeys($data): array
    {
        return ['export_result'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'Average calendar duration';
    }

    public function getType(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = []): QueryBuilder
    {
        $qb = $this->calendarRepository->createQueryBuilder('cal');

        $qb->select('AVG(cal.endDate - cal.startDate) AS export_result');
        $qb->join('cal.accompanyingPeriod', 'acp');

        AccompanyingCourseExportHelper::addClosingMotiveExclusionClause($qb);

        return $qb;
    }

    public function requiredRole(): string
    {
        return AccompanyingPeriodVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [
            Declarations::CALENDAR_TYPE,
        ];
    }
}
