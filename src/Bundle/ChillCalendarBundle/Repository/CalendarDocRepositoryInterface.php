<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Repository;

use Chill\CalendarBundle\Entity\CalendarDoc;
use Chill\DocStoreBundle\Entity\StoredObject;

interface CalendarDocRepositoryInterface
{
    public function find($id): ?CalendarDoc;

    /**
     * @return array|CalendarDoc[]
     */
    public function findAll(): array;

    /**
     * @return array|CalendarDoc[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    public function findOneBy(array $criteria): ?CalendarDoc;

    public function findOneByStoredObject(StoredObject|int $storedObject): ?CalendarDoc;

    public function getClassName();
}
