<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Repository;

use Chill\CalendarBundle\Entity\CalendarDoc;
use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class CalendarDocRepository implements ObjectRepository, CalendarDocRepositoryInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository($this->getClassName());
    }

    public function find($id): ?CalendarDoc
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?CalendarDoc
    {
        return $this->findOneBy($criteria);
    }

    public function getClassName()
    {
        return CalendarDoc::class;
    }

    /**
     * @param StoredObject|int $storedObject the StoredObject instance, or the id of the stored object
     */
    public function findOneByStoredObject(StoredObject|int $storedObject): ?CalendarDoc
    {
        $storedObjectId = $storedObject instanceof StoredObject ? $storedObject->getId() : $storedObject;

        $qb = $this->repository->createQueryBuilder('c');
        $qb->where(
            $qb->expr()->eq(':storedObject', 'c.storedObject')
        );

        $qb->setParameter('storedObject', $storedObjectId);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
