<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Messenger\Transport\InMemoryTransport;

/**
 * @internal
 *
 * @coversNothing
 */
final class RemoteCalendarMSGraphSyncControllerTest extends WebTestCase
{
    private const SAMPLE_BODY = <<<'JSON'
        {
            "value": [
                {
                    "subscriptionId": "739703eb-80c4-4c03-b15a-ca370f19624b",
                    "subscriptionExpirationDateTime": "2022-06-09T02:40:28-07:00",
                    "changeType": "updated",
                    "resource": "Users/4feb0ae3-7ffb-48dd-891e-c86b2cdeefd4/Events/AAMkADM1MTdlMGIzLTZhZWUtNDQ0ZC05Y2M4LWViMjhmOWJlMDhhMQBGAAAAAAA5e3965gkBSLcU1p00sMSyBwAHduaxajFfTpv0kchk_m1FAAAAAAENAAAHduaxajFfTpv0kchk_m1FAAAl1BupAAA=",
                    "resourceData": {
                        "@odata.type": "#Microsoft.Graph.Event",
                        "@odata.id": "Users/4feb0ae3-7ffb-48dd-891e-c86b2cdeefd4/Events/AAMkADM1MTdlMGIzLTZhZWUtNDQ0ZC05Y2M4LWViMjhmOWJlMDhhMQBGAAAAAAA5e3965gkBSLcU1p00sMSyBwAHduaxajFfTpv0kchk_m1FAAAAAAENAAAHduaxajFfTpv0kchk_m1FAAAl1BupAAA=",
                        "@odata.etag": "W/\"DwAAABYAAAAHduaxajFfTpv0kchk+m1FAAAlyzAU\"",
                        "id": "AAMkADM1MTdlMGIzLTZhZWUtNDQ0ZC05Y2M4LWViMjhmOWJlMDhhMQBGAAAAAAA5e3965gkBSLcU1p00sMSyBwAHduaxajFfTpv0kchk_m1FAAAAAAENAAAHduaxajFfTpv0kchk_m1FAAAl1BupAAA="
                    },
                    "clientState": "2k05qlr3ds2KzvUP3Ps4A+642fYaI8ThxHGIGbNr2p0MnNkmzxLTNEMxpMc/UEuDkBHfID7OYWj4DQc94vlEkPBdsh9sGTTkHxIE68hqkKkDKhwvfvdj6lS6Dus=",
                    "tenantId": "421bf216-3f48-47bd-a7cf-8b1995cb24bd"
                }
            ]
        }
        JSON;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public function testSendNotification(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/public/incoming-hook/calendar/msgraph/events/23',
            [],
            [],
            [],
            self::SAMPLE_BODY
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(202);

        /** @var InMemoryTransport $transport */
        $transport = self::getContainer()->get('messenger.transport.async');
        $this->assertCount(1, $transport->getSent());
    }

    public function testValidateSubscription(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/public/incoming-hook/calendar/msgraph/events/23?validationToken=something%20to%20decode'
        );

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();

        $this->assertResponseHasHeader('Content-Type');
        $this->assertStringContainsString('text/plain', $response->headers->get('Content-Type'));
        $this->assertEquals('something to decode', $response->getContent());
    }
}
