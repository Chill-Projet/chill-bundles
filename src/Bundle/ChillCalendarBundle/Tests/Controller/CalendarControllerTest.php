<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 *
 * @coversNothing
 */
final class CalendarControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    private KernelBrowser $client;

    /**
     * Setup before each test method (see phpunit doc).
     */
    protected function setUp(): void
    {
        $this->client = $this->getClientAuthenticated();
    }

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public static function provideAccompanyingPeriod(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $nb = $em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'ac')
            ->join('ac.participations', 'acp')
            ->join('acp.person', 'person')
            ->join('person.centerCurrent', 'pc')
            ->join('pc.center', 'center')
            ->where('center.name LIKE :n')
            ->setParameter('n', 'Center A')
            ->join('ac.scopes', 's')
            ->andWhere('JSON_EXTRACT(s.name,\'fr\') LIKE :s')
            ->setParameter('s', 'social')
            ->select('COUNT(DISTINCT ac) AS nb')
            ->getQuery()
            ->getSingleScalarResult();

        yield [$em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'ac')
            ->select('ac.id')
            ->join('ac.participations', 'acp')
            ->join('acp.person', 'person')
            ->join('person.centerCurrent', 'pc')
            ->join('pc.center', 'center')
            ->where('center.name LIKE :n')
            ->setParameter('n', 'Center A')
            ->join('ac.scopes', 's')
            ->andWhere('JSON_EXTRACT(s.name,\'fr\') LIKE :s')
            ->setParameter('s', 'social')
            ->setFirstResult(\random_int(0, $nb - 1))
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult(),
        ];

        self::ensureKernelShutdown();
    }

    /**
     * @dataProvider provideAccompanyingPeriod
     */
    public function testList(int $accompanyingPeriodId)
    {
        $this->client->request(
            Request::METHOD_GET,
            sprintf('/fr/calendar/calendar/by-period/%d', $accompanyingPeriodId)
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideAccompanyingPeriod
     */
    public function testNew(int $accompanyingPeriodId)
    {
        $this->client->request(
            Request::METHOD_GET,
            sprintf('/fr/calendar/calendar/new?accompanying_period_id=%d', $accompanyingPeriodId)
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
