<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\RemoteCalendar\Connector\MSGraph;

use Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph\MSUserAbsenceReaderInterface;
use Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph\MSUserAbsenceSync;
use Chill\MainBundle\Entity\User;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Clock\MockClock;

/**
 * @internal
 *
 * @coversNothing
 */
class MSUserAbsenceSyncTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @dataProvider provideDataTestSyncUserAbsence
     */
    public function testSyncUserAbsence(User $user, ?bool $absenceFromMicrosoft, bool $expectedAbsence, ?\DateTimeImmutable $expectedAbsenceStart, string $message): void
    {
        $userAbsenceReader = $this->prophesize(MSUserAbsenceReaderInterface::class);
        $userAbsenceReader->isUserAbsent($user)->willReturn($absenceFromMicrosoft);

        $clock = new MockClock(new \DateTimeImmutable('2023-07-01T12:00:00'));

        $syncer = new MSUserAbsenceSync($userAbsenceReader->reveal(), $clock, new NullLogger());

        $syncer->syncUserAbsence($user);

        self::assertEquals($expectedAbsence, $user->isAbsent(), $message);
        self::assertEquals($expectedAbsenceStart, $user->getAbsenceStart(), $message);
    }

    public static function provideDataTestSyncUserAbsence(): iterable
    {
        yield [new User(), false, false, null, 'user present remains present'];
        yield [new User(), true, true, new \DateTimeImmutable('2023-07-01T12:00:00'), 'user present becomes absent'];

        $user = new User();
        $user->setAbsenceStart($abs = new \DateTimeImmutable('2023-07-01T12:00:00'));
        yield [$user, true, true, $abs, 'user absent remains absent'];

        $user = new User();
        $user->setAbsenceStart($abs = new \DateTimeImmutable('2023-07-01T12:00:00'));
        yield [$user, false, false, null, 'user absent becomes present'];

        yield [new User(), null, false, null, 'user not syncable: presence do not change'];

        $user = new User();
        $user->setAbsenceStart($abs = new \DateTimeImmutable('2023-07-01T12:00:00'));
        yield [$user, null, true, $abs, 'user not syncable: absence do not change'];
    }
}
