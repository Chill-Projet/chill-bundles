<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Service\ShortMessageNotification;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Service\ShortMessageNotification\DefaultShortMessageForCalendarBuilder;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class DefaultShortMessageForCalendarBuilderTest extends TestCase
{
    use ProphecyTrait;

    private PhoneNumberUtil $phoneNumberUtil;

    protected function setUp(): void
    {
        $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    }

    public function testBuildMessageForCalendar()
    {
        $calendar = new Calendar();
        $calendar
            ->setStartDate(new \DateTimeImmutable('now'))
            ->setEndDate($calendar->getStartDate()->add(new \DateInterval('PT30M')))
            ->setMainUser($user = new User())
            ->addPerson($person = new Person())
            ->setSendSMS(false);
        $user
            ->setLabel('Alex')
            ->setMainLocation($location = new Location());
        $location->setName('LOCAMAT');
        $person
            ->setMobilenumber($this->phoneNumberUtil->parse('+32470123456', 'BE'))
            ->setAcceptSMS(false);

        $engine = $this->prophesize(\Twig\Environment::class);
        $engine->render(Argument::exact('@ChillCalendar/CalendarShortMessage/short_message.txt.twig'), Argument::withKey('calendar'))
            ->willReturn('message content')
            ->shouldBeCalledTimes(1);
        $engine->render(Argument::exact('@ChillCalendar/CalendarShortMessage/short_message_canceled.txt.twig'), Argument::withKey('calendar'))
            ->willReturn('message canceled')
            ->shouldBeCalledTimes(1);

        $builder = new DefaultShortMessageForCalendarBuilder(
            $engine->reveal()
        );

        // if the calendar should not send sms
        $sms = $builder->buildMessageForCalendar($calendar);
        $this->assertCount(0, $sms);

        // if the person do not accept sms
        $calendar->setSendSMS(true);
        $sms = $builder->buildMessageForCalendar($calendar);
        $this->assertCount(0, $sms);

        // person accepts sms
        $person->setAcceptSMS(true);
        $sms = $builder->buildMessageForCalendar($calendar);

        $this->assertCount(1, $sms);
        $this->assertEquals(
            '+32470123456',
            $sms[0]->getPhone()
        );
        $this->assertEquals('message content', $sms[0]->getSubject());

        // if the calendar is canceled
        $calendar
            ->setSmsStatus(Calendar::SMS_SENT)
            ->setStatus(Calendar::STATUS_CANCELED);

        $sms = $builder->buildMessageForCalendar($calendar);

        $this->assertCount(1, $sms);
        $this->assertEquals(
            '+32470123456',
            $sms[0]->getRecipientId(),
        );
        $this->assertEquals('message canceled', $sms[0]->getSubject());
    }
}
