<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Export\Filter;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Export\Filter\BetweenDatesFilter;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class BetweenDatesFilterTest extends AbstractFilterTest
{
    private BetweenDatesFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        // add a fake request with a default locale (used in translatable string)
        $request = $this->prophesize();

        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');

        $this->filter = self::getContainer()->get('chill.calendar.export.between_dates_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        return [
            [
                'date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'date_to' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('cal.id')
                ->from(Calendar::class, 'cal'),
        ];
    }
}
