<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Export\Filter;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Export\Filter\AgentFilter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class AgentFilterTest extends AbstractFilterTest
{
    private AgentFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        // add a fake request with a default locale (used in translatable string)
        $request = $this->prophesize();

        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');

        $this->filter = self::getContainer()->get('chill.calendar.export.agent_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public static function getFormData(): array
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $array = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->getResult();
        $data = [];
        foreach ($array as $a) {
            $data[] = [
                'accepted_agents' => $a,
            ];
        }
        self::ensureKernelShutdown();

        return $data;
    }

    public static function getQueryBuilders(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        yield $em->createQueryBuilder()
            ->select('cal.id')
            ->from(Calendar::class, 'cal')
        ;
        self::ensureKernelShutdown();
    }
}
