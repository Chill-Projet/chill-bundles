<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Repository;

use Chill\CalendarBundle\Repository\CalendarACLAwareRepository;
use Chill\PersonBundle\DataFixtures\Helper\PersonRandomHelper;
use Chill\PersonBundle\Repository\AccompanyingPeriodACLAwareRepositoryInterface;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CalendarACLAwareRepositoryTest extends KernelTestCase
{
    use PersonRandomHelper;

    use ProphecyTrait;

    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testCountByPerosn()
    {
        $person = $this->getRandomPerson($this->entityManager);

        $periodRepository = $this->prophesize(AccompanyingPeriodACLAwareRepositoryInterface::class);
        $periodRepository->findByPerson($person, AccompanyingPeriodVoter::SEE)->willReturn([]);

        $calendarRepository = new CalendarACLAwareRepository(
            $periodRepository->reveal(),
            $this->entityManager
        );

        $count = $calendarRepository->countByPerson($person, new \DateTimeImmutable('yesterday'), new \DateTimeImmutable('tomorrow'));

        $this->assertIsInt($count);
    }

    /**
     * Test that the query does not throw any error.
     */
    public function testFindByPerson()
    {
        $person = $this->getRandomPerson($this->entityManager);

        $periodRepository = $this->prophesize(AccompanyingPeriodACLAwareRepositoryInterface::class);
        $periodRepository->findByPerson($person, AccompanyingPeriodVoter::SEE)->willReturn([]);

        $calendarRepository = new CalendarACLAwareRepository(
            $periodRepository->reveal(),
            $this->entityManager
        );

        $calendars = $calendarRepository->findByPerson($person, null, null, ['startDate' => 'ASC'], 10, 1);

        $this->assertIsArray($calendars);
    }
}
