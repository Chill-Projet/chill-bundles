<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DataFixtures\ORM;

use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadCalendarACL extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $roleScopes = [];

        foreach ([
            CalendarVoter::CREATE,
            CalendarVoter::DELETE,
            CalendarVoter::DELETE,
        ] as $role) {
            $roleScopes[] = $r = (new RoleScope())
                ->setRole($role);
            $manager->persist($r);
        }

        foreach (LoadPermissionsGroup::$refs as $permissionGroupRef) {
            /** @var PermissionsGroup $group */
            $group = $this->getReference($permissionGroupRef, PermissionsGroup::class);

            foreach ($roleScopes as $scope) {
                $group->addRoleScope($scope);
            }
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 16000;
    }
}
