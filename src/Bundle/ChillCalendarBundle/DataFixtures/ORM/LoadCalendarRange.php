<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DataFixtures\ORM;

use Chill\CalendarBundle\Entity\CalendarRange;
use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\LocationType;
use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use libphonenumber\PhoneNumberUtil;

class LoadCalendarRange extends Fixture implements FixtureGroupInterface, OrderedFixtureInterface
{
    public static array $references = [];

    public function __construct(private readonly UserRepository $userRepository) {}

    public static function getGroups(): array
    {
        return ['calendar'];
    }

    public function getOrder(): int
    {
        return 40003;
    }

    public function load(ObjectManager $manager): void
    {
        $users = $this->userRepository->findAll();

        $location = (new Location())
            ->setAddress($address = new Address())
            ->setName('Centre A')
            ->setEmail('centreA@test.chill.social')
            ->setLocationType($type = new LocationType())
            ->setPhonenumber1(PhoneNumberUtil::getInstance()->parse('+3287653812'));
        $type->setTitle(['fr' => 'Service']);
        $address->setStreet('Rue des Épaules')->setStreetNumber('14')
            ->setPostcode($postCode = new PostalCode());
        $postCode->setCode('4145')->setName('Houte-Si-Plout')->setCountry(
            ($country = new Country())->setName(['fr' => 'Pays'])->setCountryCode('ZZ')
        );
        $manager->persist($country);
        $manager->persist($postCode);
        $manager->persist($address);
        $manager->persist($type);
        $manager->persist($location);

        $now =  new \DateTimeImmutable();

        $days = [
            '2021-08-23',
            '2021-08-24',
            '2021-08-25',
            '2021-08-26',
            '2021-08-30',
            '2021-08-31',
            '2021-09-01',
            '2021-09-02',
            (new \DateTimeImmutable('tomorrow'))->format('Y-m-d'),
            (new \DateTimeImmutable('today'))->format('Y-m-d'),
            $now->add(new \DateInterval('P7D'))->format('Y-m-d'),
            $now->add(new \DateInterval('P8D'))->format('Y-m-d'),
            $now->add(new \DateInterval('P9D'))->format('Y-m-d'),
            $now->add(new \DateInterval('P10D'))->format('Y-m-d'),
        ];

        $hours = [
            '10:00:00',
            '11:30:00',
            '13:30:00',
            '15:00:00',
        ];

        foreach ($users as $u) {
            foreach ($days as $d) {
                foreach ($hours as $h) {
                    $event = $d.' '.$h;
                    $startEvent = new \DateTimeImmutable($event);
                    $endEvent = new \DateTimeImmutable($event.' + 1 hours');
                    $calendarRange = (new CalendarRange())
                        ->setUser($u)
                        ->setStartDate($startEvent)
                        ->setEndDate($endEvent)
                        ->setLocation($location);

                    $manager->persist($calendarRange);
                }
            }
        }
        $manager->flush();
    }
}
