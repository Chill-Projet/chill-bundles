<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\GenericDoc\Renderers;

use Chill\CalendarBundle\Repository\CalendarDocRepository;
use Chill\CalendarBundle\Service\GenericDoc\Providers\AccompanyingPeriodCalendarGenericDocProvider;
use Chill\CalendarBundle\Service\GenericDoc\Providers\PersonCalendarGenericDocProvider;
use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;
use Chill\DocStoreBundle\GenericDoc\Twig\GenericDocRendererInterface;

/**
 * @implements GenericDocRendererInterface<array{row-only?: bool, show-actions?: bool}>
 */
final readonly class AccompanyingPeriodCalendarGenericDocRenderer implements GenericDocRendererInterface
{
    public function __construct(private CalendarDocRepository $repository) {}

    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool
    {
        return AccompanyingPeriodCalendarGenericDocProvider::KEY === $genericDocDTO->key || PersonCalendarGenericDocProvider::KEY === $genericDocDTO->key;
    }

    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string
    {
        return $options['row-only'] ?? false ? '@ChillCalendar/GenericDoc/calendar_document_row.html.twig'
            : '@ChillCalendar/GenericDoc/calendar_document.html.twig';
    }

    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array
    {
        return [
            'document' => $this->repository->find($genericDocDTO->identifiers['id']),
            'context' => $genericDocDTO->getContext(),
            'show_actions' => $options['show-actions'] ?? true,
        ];
    }
}
