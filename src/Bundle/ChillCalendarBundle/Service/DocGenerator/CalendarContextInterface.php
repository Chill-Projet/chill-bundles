<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\DocGenerator;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\DocGeneratorBundle\Context\DocGeneratorContextWithAdminFormInterface;
use Chill\DocGeneratorBundle\Context\DocGeneratorContextWithPublicFormInterface;

/**
 * @extends DocGeneratorContextWithPublicFormInterface<Calendar>
 * @extends DocGeneratorContextWithAdminFormInterface<Calendar>
 */
interface CalendarContextInterface extends DocGeneratorContextWithPublicFormInterface, DocGeneratorContextWithAdminFormInterface {}
