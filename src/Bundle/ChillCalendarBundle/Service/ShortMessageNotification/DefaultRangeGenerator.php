<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\ShortMessageNotification;

/**
 * * Lundi => Envoi des rdv du mardi et mercredi.
 * * Mardi => Envoi des rdv du jeudi.
 * * Mercredi => Envoi des rdv du vendredi
 * * Jeudi => envoi des rdv du samedi et dimanche
 * * Vendredi => Envoi des rdv du lundi.
 */
class DefaultRangeGenerator implements RangeGeneratorInterface
{
    public function generateRange(\DateTimeImmutable $date): ?array
    {
        $onMidnight = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $date->format('Y-m-d').' 00:00:00');

        switch ($dow = (int) $onMidnight->format('w')) {
            case 6: // Saturday
            case 0: // Sunday
                return null;

            case 1: // Monday
                // send for Tuesday and Wednesday
                $startDate = $onMidnight->add(new \DateInterval('P1D'));
                $endDate = $startDate->add(new \DateInterval('P2D'));

                break;

            case 2: // tuesday
            case 3: // wednesday
                $startDate = $onMidnight->add(new \DateInterval('P2D'));
                $endDate = $startDate->add(new \DateInterval('P1D'));

                break;

            case 4: // thursday
                $startDate = $onMidnight->add(new \DateInterval('P2D'));
                $endDate = $startDate->add(new \DateInterval('P2D'));

                break;

            case 5: // friday
                $startDate = $onMidnight->add(new \DateInterval('P3D'));
                $endDate = $startDate->add(new \DateInterval('P1D'));

                break;

            default:
                throw new \UnexpectedValueException('a day of a week should not have the value: '.$dow);
        }

        return ['startDate' => $startDate, 'endDate' => $endDate];
    }
}
