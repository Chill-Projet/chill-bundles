<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\ShortMessageNotification;

use Chill\CalendarBundle\Entity\Calendar;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Notifier\Message\SmsMessage;

class DefaultShortMessageForCalendarBuilder implements ShortMessageForCalendarBuilderInterface
{
    private readonly PhoneNumberUtil $phoneUtil;

    public function __construct(private readonly \Twig\Environment $engine)
    {
        $this->phoneUtil = PhoneNumberUtil::getInstance();
    }

    /**
     * @return list<SmsMessage>
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function buildMessageForCalendar(Calendar $calendar): array
    {
        if (true !== $calendar->getSendSMS()) {
            return [];
        }

        $toUsers = [];

        foreach ($calendar->getPersons() as $person) {
            if (false === $person->getAcceptSMS() || null === $person->getAcceptSMS() || null === $person->getMobilenumber()) {
                continue;
            }

            if (Calendar::SMS_PENDING === $calendar->getSmsStatus()) {
                $toUsers[] = new SmsMessage(
                    $this->phoneUtil->format($person->getMobilenumber(), PhoneNumberFormat::E164),
                    $this->engine->render('@ChillCalendar/CalendarShortMessage/short_message.txt.twig', ['calendar' => $calendar]),
                );
            } elseif (Calendar::SMS_CANCEL_PENDING === $calendar->getSmsStatus()) {
                $toUsers[] = new SmsMessage(
                    $this->phoneUtil->format($person->getMobilenumber(), PhoneNumberFormat::E164),
                    $this->engine->render('@ChillCalendar/CalendarShortMessage/short_message_canceled.txt.twig', ['calendar' => $calendar]),
                );
            }
        }

        return $toUsers;
    }
}
