<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\ShortMessageNotification;

use Chill\CalendarBundle\Entity\Calendar;
use Symfony\Component\Notifier\Message\SmsMessage;

interface ShortMessageForCalendarBuilderInterface
{
    /**
     * @return list<SmsMessage>
     */
    public function buildMessageForCalendar(Calendar $calendar): array;
}
