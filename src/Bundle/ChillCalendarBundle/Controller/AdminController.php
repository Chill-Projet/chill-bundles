<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * Calendar admin.
     */
    #[Route(path: '/{_locale}/admin/calendar', name: 'chill_calendar_admin_index')]
    public function indexAdminAction()
    {
        return $this->render('@ChillCalendar/Admin/index.html.twig');
    }
}
