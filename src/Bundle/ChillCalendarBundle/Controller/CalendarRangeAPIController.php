<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Controller;

use Chill\CalendarBundle\Repository\CalendarRangeRepository;
use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Serializer\Model\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CalendarRangeAPIController extends ApiController
{
    public function __construct(private readonly CalendarRangeRepository $calendarRangeRepository) {}

    #[Route(path: '/api/1.0/calendar/calendar-range-available/{id}.{_format}', name: 'chill_api_single_calendar_range_available', requirements: ['_format' => 'json'])]
    public function availableRanges(User $user, Request $request, string $_format): JsonResponse
    {
        // return new JsonResponse(['ok' => true], 200, [], false);
        $this->denyAccessUnlessGranted('ROLE_USER');

        if (!$request->query->has('dateFrom')) {
            throw new BadRequestHttpException('You must provide a dateFrom parameter');
        }

        if (false === $dateFrom = \DateTimeImmutable::createFromFormat(
            \DateTimeImmutable::ATOM,
            $request->query->get('dateFrom')
        )) {
            throw new BadRequestHttpException('dateFrom not parsable');
        }

        if (!$request->query->has('dateTo')) {
            throw new BadRequestHttpException('You must provide a dateTo parameter');
        }

        if (false === $dateTo = \DateTimeImmutable::createFromFormat(
            \DateTimeImmutable::ATOM,
            $request->query->get('dateTo')
        )) {
            throw new BadRequestHttpException('dateTo not parsable');
        }

        $total = $this->calendarRangeRepository->countByAvailableRangesForUser($user, $dateFrom, $dateTo);
        $paginator = $this->getPaginatorFactory()->create($total);
        $ranges = $this->calendarRangeRepository->findByAvailableRangesForUser(
            $user,
            $dateFrom,
            $dateTo,
            $paginator->getItemsPerPage(),
            $paginator->getCurrentPageFirstItemNumber()
        );

        $collection = new Collection($ranges, $paginator);

        return $this->json($collection, Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
