<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Controller;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Form\CalendarType;
use Chill\CalendarBundle\RemoteCalendar\Connector\RemoteCalendarConnectorInterface;
use Chill\CalendarBundle\Repository\CalendarACLAwareRepositoryInterface;
use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Chill\DocGeneratorBundle\Repository\DocGeneratorTemplateRepository;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Repository\UserRepositoryInterface;
use Chill\MainBundle\Templating\Listing\FilterOrderHelper;
use Chill\MainBundle\Templating\Listing\FilterOrderHelperFactoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\AccompanyingPeriodRepository;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use http\Exception\UnexpectedValueException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CalendarController extends AbstractController
{
    public function __construct(
        private readonly CalendarACLAwareRepositoryInterface $calendarACLAwareRepository,
        private readonly DocGeneratorTemplateRepository $docGeneratorTemplateRepository,
        private readonly FilterOrderHelperFactoryInterface $filterOrderHelperFactory,
        private readonly LoggerInterface $logger,
        private readonly PaginatorFactory $paginator,
        private readonly RemoteCalendarConnectorInterface $remoteCalendarConnector,
        private readonly SerializerInterface $serializer,
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
        private readonly PersonRepository $personRepository,
        private readonly AccompanyingPeriodRepository $accompanyingPeriodRepository,
        private readonly UserRepositoryInterface $userRepository,
        private readonly TranslatorInterface $translator,
        private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry,
    ) {}

    /**
     * Delete a calendar item.
     */
    #[Route(path: '/{_locale}/calendar/{id}/delete', name: 'chill_calendar_calendar_delete')]
    public function deleteAction(Request $request, Calendar $entity)
    {
        $em = $this->managerRegistry->getManager();

        [$person, $accompanyingPeriod] = [$entity->getPerson(), $entity->getAccompanyingPeriod()];

        if ($accompanyingPeriod instanceof AccompanyingPeriod) {
            $view = '@ChillCalendar/Calendar/confirm_deleteByAccompanyingCourse.html.twig';
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_period', ['id' => $accompanyingPeriod->getId()]);
        } elseif ($person instanceof Person) {
            $view = '@ChillCalendar/Calendar/confirm_deleteByPerson.html.twig';
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_person', ['id' => $person->getId()]);
        } else {
            throw new \RuntimeException('nor person or accompanying period');
        }

        $form = $this->createDeleteForm($entity);

        if (Request::METHOD_POST === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->logger->notice('A calendar event has been removed', [
                    'by_user' => $this->getUser()->getUsername(),
                    'calendar_id' => $entity->getId(),
                ]);

                $em->remove($entity);
                $em->flush();

                $this->addFlash('success', $this->translator
                    ->trans('The calendar item has been successfully removed.'));

                return new RedirectResponse($redirectRoute);
            }
        }

        return $this->render($view, [
            'calendar' => $entity,
            'delete_form' => $form->createView(),
            'accompanyingCourse' => $accompanyingPeriod,
            'person' => $person,
        ]);
    }

    /**
     * Edit a calendar item.
     */
    #[Route(path: '/{_locale}/calendar/calendar/{id}/edit', name: 'chill_calendar_calendar_edit')]
    public function editAction(Calendar $entity, Request $request): Response
    {
        $this->denyAccessUnlessGranted(CalendarVoter::EDIT, $entity);

        if (!$this->remoteCalendarConnector->isReady()) {
            return $this->remoteCalendarConnector->getMakeReadyResponse($request->getUri());
        }

        $em = $this->managerRegistry->getManager();

        [$person, $accompanyingPeriod] = [$entity->getPerson(), $entity->getAccompanyingPeriod()];

        if ($accompanyingPeriod instanceof AccompanyingPeriod) {
            $view = '@ChillCalendar/Calendar/editByAccompanyingCourse.html.twig';
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_period', ['id' => $accompanyingPeriod->getId()]);
        } elseif ($person instanceof Person) {
            $view = '@ChillCalendar/Calendar/editByPerson.html.twig';
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_person', ['id' => $person->getId()]);
        } else {
            throw new \RuntimeException('no person nor accompanying period');
        }

        $form = $this->createForm(CalendarType::class, $entity)
            ->add('save', SubmitType::class);

        $form->add('save_and_upload_doc', SubmitType::class);
        $templates = $this->docGeneratorTemplateRepository->findByEntity(Calendar::class);

        foreach ($templates as $template) {
            $form->add('save_and_generate_doc_'.$template->getId(), SubmitType::class, [
                'label' => $this->translatableStringHelper->localize($template->getName()),
            ]);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->translator->trans('Success : calendar item updated!'));

            if ($form->get('save_and_upload_doc')->isClicked()) {
                return $this->redirectToRoute('chill_calendar_calendardoc_new', ['id' => $entity->getId()]);
            }

            foreach ($templates as $template) {
                if ($form->get('save_and_generate_doc_'.$template->getId())->isClicked()) {
                    return $this->redirectToRoute('chill_docgenerator_generate_from_template', [
                        'entityClassName' => Calendar::class,
                        'entityId' => $entity->getId(),
                        'template' => $template->getId(),
                        'returnPath' => $request->getRequestUri(),
                    ]);
                }
            }

            return new RedirectResponse($redirectRoute);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans('This form contains errors'));
        }

        $entity_array = $this->serializer->normalize($entity, 'json', ['groups' => 'read']);

        return $this->render($view, [
            'entity' => $entity,
            'form' => $form->createView(),
            'accompanyingCourse' => $entity->getAccompanyingPeriod(),
            'person' => $entity->getPerson(),
            'entity_json' => $entity_array,
            'templates' => $templates,
        ]);
    }

    /**
     * Lists all Calendar entities.
     */
    #[Route(path: '/{_locale}/calendar/calendar/by-period/{id}', name: 'chill_calendar_calendar_list_by_period')]
    public function listActionByCourse(AccompanyingPeriod $accompanyingPeriod): Response
    {
        $this->denyAccessUnlessGranted(CalendarVoter::SEE, $accompanyingPeriod);

        $filterOrder = $this->buildListFilterOrder();
        ['from' => $from, 'to' => $to] = $filterOrder->getDateRangeData('startDate');

        $total = $this->calendarACLAwareRepository
            ->countByAccompanyingPeriod($accompanyingPeriod, $from, $to);
        $paginator = $this->paginator->create($total);
        $calendarItems = $this->calendarACLAwareRepository->findByAccompanyingPeriod(
            $accompanyingPeriod,
            $from,
            $to,
            ['startDate' => 'DESC'],
            $paginator->getCurrentPageFirstItemNumber(),
            $paginator->getItemsPerPage()
        );

        return $this->render('@ChillCalendar/Calendar/listByAccompanyingCourse.html.twig', [
            'calendarItems' => $calendarItems,
            'accompanyingCourse' => $accompanyingPeriod,
            'paginator' => $paginator,
            'filterOrder' => $filterOrder,
            'nbIgnored' => $this->calendarACLAwareRepository->countIgnoredByAccompanyingPeriod($accompanyingPeriod, $from, $to),
            'templates' => $this->docGeneratorTemplateRepository->findByEntity(Calendar::class),
        ]);
    }

    /**
     * Lists all Calendar entities on a person.
     */
    #[Route(path: '/{_locale}/calendar/calendar/by-person/{id}', name: 'chill_calendar_calendar_list_by_person')]
    public function listActionByPerson(Person $person): Response
    {
        $this->denyAccessUnlessGranted(CalendarVoter::SEE, $person);

        $filterOrder = $this->buildListFilterOrder();
        ['from' => $from, 'to' => $to] = $filterOrder->getDateRangeData('startDate');

        $total = $this->calendarACLAwareRepository
            ->countByPerson($person, $from, $to);
        $paginator = $this->paginator->create($total);
        $calendarItems = $this->calendarACLAwareRepository->findByPerson(
            $person,
            $from,
            $to,
            ['startDate' => 'DESC'],
            $paginator->getCurrentPageFirstItemNumber(),
            $paginator->getItemsPerPage()
        );

        return $this->render('@ChillCalendar/Calendar/listByPerson.html.twig', [
            'calendarItems' => $calendarItems,
            'person' => $person,
            'paginator' => $paginator,
            'filterOrder' => $filterOrder,
            'nbIgnored' => $this->calendarACLAwareRepository->countIgnoredByPerson($person, $from, $to),
            'templates' => $this->docGeneratorTemplateRepository->findByEntity(Calendar::class),
        ]);
    }

    #[Route(path: '/{_locale}/calendar/calendar/my', name: 'chill_calendar_calendar_list_my')]
    public function myCalendar(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        if (!$this->remoteCalendarConnector->isReady()) {
            return $this->remoteCalendarConnector->getMakeReadyResponse($request->getUri());
        }

        if (!$this->getUser() instanceof User) {
            throw new UnauthorizedHttpException('you are not an user');
        }

        $view = '@ChillCalendar/Calendar/listByUser.html.twig';

        return $this->render($view, [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * Create a new calendar item.
     */
    #[Route(path: '/{_locale}/calendar/calendar/new', name: 'chill_calendar_calendar_new')]
    public function newAction(Request $request): Response
    {
        if (!$this->remoteCalendarConnector->isReady()) {
            return $this->remoteCalendarConnector->getMakeReadyResponse($request->getUri());
        }

        $view = null;
        $em = $this->managerRegistry->getManager();

        [$person, $accompanyingPeriod] = $this->getEntity($request);

        $entity = new Calendar();

        $redirectRoute = '';

        if ($accompanyingPeriod instanceof AccompanyingPeriod) {
            $view = '@ChillCalendar/Calendar/newByAccompanyingCourse.html.twig';
            $entity->setAccompanyingPeriod($accompanyingPeriod);
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_period', ['id' => $accompanyingPeriod->getId()]);
        } elseif (null !== $person) {
            $view = '@ChillCalendar/Calendar/newByPerson.html.twig';
            $entity->setPerson($person)->addPerson($person);
            $redirectRoute = $this->generateUrl('chill_calendar_calendar_list_by_person', ['id' => $person->getId()]);
        }

        if ($request->query->has('mainUser')) {
            $entity->setMainUser($this->userRepository->find($request->query->getInt('mainUser')));
        }

        $this->denyAccessUnlessGranted(CalendarVoter::CREATE, $entity);

        $form = $this->createForm(CalendarType::class, $entity)
            ->add('save', SubmitType::class);

        $templates = $this->docGeneratorTemplateRepository->findByEntity(Calendar::class);
        $form->add('save_and_upload_doc', SubmitType::class);

        foreach ($templates as $template) {
            $form->add('save_and_generate_doc_'.$template->getId(), SubmitType::class, [
                'label' => $this->translatableStringHelper->localize($template->getName()),
            ]);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', $this->translator->trans('Success : calendar item created!'));

            if ($form->get('save_and_upload_doc')->isClicked()) {
                return $this->redirectToRoute('chill_calendar_calendardoc_new', [
                    'id' => $entity->getId(),
                ]);
            }

            foreach ($templates as $template) {
                if ($form->get('save_and_generate_doc_'.$template->getId())->isClicked()) {
                    return $this->redirectToRoute('chill_docgenerator_generate_from_template', [
                        'entityClassName' => Calendar::class,
                        'entityId' => $entity->getId(),
                        'template' => $template->getId(),
                        'returnPath' => $this->generateUrl('chill_calendar_calendar_edit', ['id' => $entity->getId()]),
                    ]);
                }
            }

            if ('' !== $redirectRoute) {
                return new RedirectResponse($redirectRoute);
            }

            throw new UnexpectedValueException('No person id or accompanying period id was given');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans('This form contains errors'));
        }

        if (null === $view) {
            throw $this->createNotFoundException('Template not found');
        }

        $entity_array = $this->serializer->normalize($entity, 'json', ['groups' => 'read']);

        return $this->render($view, [
            'context' => $entity->getContext(),
            'person' => $person,
            'accompanyingCourse' => $accompanyingPeriod,
            'entity' => $entity,
            'form' => $form->createView(),
            'entity_json' => $entity_array,
            'templates' => $templates,
        ]);
    }

    /**
     * Show a calendar item.
     */
    #[Route(path: '/{_locale}/calendar/calendar/{id}/show', name: 'chill_calendar_calendar_show')]
    public function showAction(Request $request, int $id): Response
    {
        throw new \Exception('not implemented');
        $view = null;
        $em = $this->managerRegistry->getManager();

        [$user, $accompanyingPeriod] = $this->getEntity($request);

        if ($accompanyingPeriod instanceof AccompanyingPeriod) {
            $view = '@ChillCalendar/Calendar/showByAccompanyingCourse.html.twig';
        } elseif ($user instanceof User) {
            $view = '@ChillCalendar/Calendar/showByUser.html.twig';
        }

        if (null === $view) {
            throw $this->createNotFoundException('Template not found');
        }

        /** @var Calendar $entity */
        $entity = $em->getRepository(Calendar::class)->find($id);

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Calendar entity.');
        }

        if (null !== $accompanyingPeriod) {
            // @TODO: These properties are declared dynamically.
            // It must be removed.
            // @See https://wiki.php.net/rfc/deprecate_dynamic_properties
            $entity->personsAssociated = $entity->getPersonsAssociated();
            $entity->personsNotAssociated = $entity->getPersonsNotAssociated();
        }

        // $deleteForm = $this->createDeleteForm($id, $accompanyingPeriod);

        $personsId = array_map(
            static fn (Person $p): int => $p->getId(),
            $entity->getPersons()->toArray()
        );

        $professionalsId = array_map(
            static fn (ThirdParty $thirdParty): ?int => $thirdParty->getId(),
            $entity->getProfessionals()->toArray()
        );

        $durationTime = $entity->getEndDate()->diff($entity->getStartDate());
        $durationTimeInMinutes = $durationTime->days * 1440 + $durationTime->h * 60 + $durationTime->i;

        $activityData = [
            'calendarId' => $id,
            'personsId' => $personsId,
            'professionalsId' => $professionalsId,
            'date' => $entity->getStartDate()->format('Y-m-d'),
            'durationTime' => $durationTimeInMinutes,
            'location' => $entity->getLocation() ? $entity->getLocation()->getId() : null,
            'comment' => $entity->getComment()->getComment(),
        ];

        return $this->render($view, [
            'accompanyingCourse' => $accompanyingPeriod,
            'entity' => $entity,
            'user' => $user,
            'activityData' => $activityData,
            // 'delete_form' => $deleteForm->createView(),
        ]);
    }

    #[Route(path: '/{_locale}/calendar/calendar/{id}/to-activity', name: 'chill_calendar_calendar_to_activity')]
    public function toActivity(Request $request, Calendar $calendar): RedirectResponse
    {
        $this->denyAccessUnlessGranted(CalendarVoter::SEE, $calendar);

        $personsId = array_map(
            static fn (Person $p): int => $p->getId(),
            $calendar->getPersons()->toArray()
        );

        $professionalsId = array_map(
            static fn (ThirdParty $thirdParty): ?int => $thirdParty->getId(),
            $calendar->getProfessionals()->toArray()
        );

        $usersId = array_map(
            static fn (User $user): ?int => $user->getId(),
            array_merge($calendar->getUsers()->toArray(), [$calendar->getMainUser()])
        );

        $durationTime = $calendar->getEndDate()->diff($calendar->getStartDate());
        $durationTimeInMinutes = $durationTime->days * 1440 + $durationTime->h * 60 + $durationTime->i;

        $activityData = [
            'calendarId' => $calendar->getId(),
            'personsId' => $personsId,
            'professionalsId' => $professionalsId,
            'usersId' => $usersId,
            'date' => $calendar->getStartDate()->format('Y-m-d'),
            'durationTime' => $durationTimeInMinutes,
            'location' => $calendar->getLocation() ? $calendar->getLocation()->getId() : null,
            'comment' => $calendar->getComment()->getComment(),
        ];

        $routeParams = [
            'activityData' => $activityData,
            'returnPath' => $request->query->get('returnPath', null),
        ];

        if ('accompanying_period' === $calendar->getContext()) {
            $routeParams['accompanying_period_id'] = $calendar->getAccompanyingPeriod()->getId();
        } elseif ('person' === $calendar->getContext()) {
            $routeParams['person_id'] = $calendar->getPerson()->getId();
        } else {
            throw new \RuntimeException('context not found for this calendar');
        }

        return $this->redirectToRoute('chill_activity_activity_new', $routeParams);
    }

    private function buildListFilterOrder(): FilterOrderHelper
    {
        $filterOrder = $this->filterOrderHelperFactory->create(self::class);
        $filterOrder->addDateRange('startDate', null, new \DateTimeImmutable('3 days ago'), null);

        return $filterOrder->build();
    }

    /**
     * Creates a form to delete a Calendar entity by id.
     */
    private function createDeleteForm(Calendar $calendar): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chill_calendar_calendar_delete', ['id' => $calendar->getId()]))
            ->add('submit', SubmitType::class, ['label' => 'Delete'])
            ->getForm();
    }

    /**
     * @return array{0: ?Person, 1: ?AccompanyingPeriod}
     */
    private function getEntity(Request $request): array
    {
        $em = $this->managerRegistry->getManager();
        $person = $accompanyingPeriod = null;

        if ($request->query->has('person_id')) {
            $person = $this->personRepository->find($request->query->getInt('person_id'));

            if (null === $person) {
                throw $this->createNotFoundException('Person not found');
            }

            $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        } elseif ($request->query->has('accompanying_period_id')) {
            $accompanyingPeriod = $this->accompanyingPeriodRepository->find($request->query->getInt('accompanying_period_id'));

            if (null === $accompanyingPeriod) {
                throw $this->createNotFoundException('Accompanying Period not found');
            }

            $this->denyAccessUnlessGranted(AccompanyingPeriodVoter::SEE, $accompanyingPeriod);
        } else {
            throw $this->createNotFoundException('Person or Accompanying Period not found');
        }

        return [
            $person, $accompanyingPeriod,
        ];
    }
}
