/*
 * Endpoint chill_api_single_calendar_range
 * method GET, get Calendar ranges
 * @returns {Promise} a promise containing all Calendar ranges objects
 */
const fetchCalendarRanges = () => {
  return Promise.resolve([]);
  const url = `/api/1.0/calendar/calendar-range-available.json`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

const fetchCalendarRangesByUser = (userId) => {
  return Promise.resolve([]);
  const url = `/api/1.0/calendar/calendar-range-available.json?user=${userId}`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_calendar
 * method GET, get Calendar events, can be filtered by mainUser
 * @returns {Promise} a promise containing all Calendar objects
 */
const fetchCalendar = (mainUserId) => {
  return Promise.resolve([]);
  const url = `/api/1.0/calendar/calendar.json?main_user=${mainUserId}&item_per_page=1000`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_calendar_range__entity_create
 * method POST, post CalendarRange entity
 */
const postCalendarRange = (body) => {
  const url = `/api/1.0/calendar/calendar-range.json?`;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_calendar_range__entity
 * method PATCH, patch CalendarRange entity
 */
const patchCalendarRange = (id, body) => {
  console.log(body);
  const url = `/api/1.0/calendar/calendar-range/${id}.json`;
  return fetch(url, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_calendar_range__entity
 * method DELETE, delete CalendarRange entity
 */
const deleteCalendarRange = (id) => {
  const url = `/api/1.0/calendar/calendar-range/${id}.json`;
  return fetch(url, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

export {
  fetchCalendarRanges,
  fetchCalendar,
  fetchCalendarRangesByUser,
  postCalendarRange,
  patchCalendarRange,
  deleteCalendarRange,
};
