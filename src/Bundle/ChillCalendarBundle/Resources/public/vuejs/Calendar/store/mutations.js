import {
  createUserData,
  calendarRangeToFullCalendarEvent,
  remoteToFullCalendarEvent,
  localsToFullCalendarEvent,
} from "./utils";

export default {
  setWhoAmiI(state, me) {
    state.me = me;
  },
  setCurrentDatesView(state, { start, end }) {
    state.currentView.start = start;
    state.currentView.end = end;
  },
  showUserOnCalendar(state, { user, ranges, remotes }) {
    if (!state.usersData.has(user.id)) {
      state.usersData.set(user.id, createUserData(user, state.usersData.size));
    }

    const cur = state.currentView.users.get(user.id);

    state.currentView.users.set(user.id, {
      ranges: typeof ranges !== "undefined" ? ranges : cur.ranges,
      remotes: typeof remotes !== "undefined" ? remotes : cur.remotes,
    });
  },
  /**
   * Set the event start and end to the given start and end,
   * and remove eventually the calendar range.
   *
   * @param state
   * @param Date start
   * @param Date end
   */
  setEventTimes(state, { start, end }) {
    state.activity.startDate = start;
    state.activity.endDate = end;
    state.activity.calendarRange = null;
  },
  /**
   * Set the event's start and end from the calendar range data,
   * and associate event to calendar range.
   *
   * @param state
   * @param range
   */
  associateCalendarToRange(state, { range }) {
    console.log("associateCalendarToRange", range);

    if (null === range) {
      state.activity.calendarRange = null;
      state.activity.startDate = null;
      state.activity.endDate = null;

      return;
    }

    console.log("userId", range.extendedProps.userId);

    const r = state.usersData
      .get(range.extendedProps.userId)
      .calendarRanges.find(
        (r) => r.calendarRangeId === range.extendedProps.calendarRangeId,
      );

    if (typeof r === "undefined") {
      throw Error("Could not find managed calendar range");
    }

    console.log("range found", r);

    state.activity.startDate = range.start;
    state.activity.endDate = range.end;
    state.activity.calendarRange = r;

    console.log("activity", state.activity);
  },

  setMainUser(state, user) {
    state.activity.mainUser = user;
  },

  // ConcernedGroups
  addPersonsInvolved(state, payload) {
    //console.log('### mutation addPersonsInvolved', payload.result.type);
    switch (payload.result.type) {
      case "person":
        state.activity.persons.push(payload.result);
        break;
      case "thirdparty":
        state.activity.thirdParties.push(payload.result);
        break;
      case "user":
        state.activity.users.push(payload.result);
        break;
    }
  },
  removePersonInvolved(state, payload) {
    //console.log('### mutation removePersonInvolved', payload.type);
    switch (payload.type) {
      case "person":
        state.activity.persons = state.activity.persons.filter(
          (person) => person !== payload,
        );
        break;
      case "thirdparty":
        state.activity.thirdParties = state.activity.thirdParties.filter(
          (thirdparty) => thirdparty !== payload,
        );
        break;
      case "user":
        state.activity.users = state.activity.users.filter(
          (user) => user !== payload,
        );
        break;
    }
  },
  /**
   * Add CalendarRange object for an user
   *
   * @param state
   * @param user
   * @param ranges
   * @param start
   * @param end
   */
  addCalendarRangesForUser(state, { user, ranges, start, end }) {
    let userData;
    if (state.usersData.has(user.id)) {
      userData = state.usersData.get(user.id);
    } else {
      userData = createUserData(user, state.usersData.size);
      state.usersData.set(user.id, userData);
    }

    const eventRanges = ranges
      .filter((r) => !state.existingEvents.has(`range_${r.id}`))
      .map((r) => {
        // add to existing ids
        state.existingEvents.add(`range_${r.id}`);
        return r;
      })
      .map((r) => calendarRangeToFullCalendarEvent(r));

    userData.calendarRanges = userData.calendarRanges.concat(eventRanges);
    userData.calendarRangesLoaded.push({ start, end });
  },
  addCalendarRemotesForUser(state, { user, remotes, start, end }) {
    let userData;
    if (state.usersData.has(user.id)) {
      userData = state.usersData.get(user.id);
    } else {
      userData = createUserData(user, state.usersData.size);
      state.usersData.set(user.id, userData);
    }

    const eventRemotes = remotes
      .filter((r) => !state.existingEvents.has(`remote_${r.id}`))
      .map((r) => {
        // add to existing ids
        state.existingEvents.add(`remote_${r.id}`);
        return r;
      })
      .map((r) => remoteToFullCalendarEvent(r));

    userData.remotes = userData.remotes.concat(eventRemotes);
    userData.remotesLoaded.push({ start, end });
  },
  addCalendarLocalsForUser(state, { user, locals, start, end }) {
    let userData;
    if (state.usersData.has(user.id)) {
      userData = state.usersData.get(user.id);
    } else {
      userData = createUserData(user, state.usersData.size);
      state.usersData.set(user.id, userData);
    }

    const eventRemotes = locals
      .filter((r) => !state.existingEvents.has(`locals_${r.id}`))
      .map((r) => {
        // add to existing ids
        state.existingEvents.add(`locals_${r.id}`);
        return r;
      })
      .map((r) => localsToFullCalendarEvent(r));

    userData.locals = userData.locals.concat(eventRemotes);
    userData.localsLoaded.push({ start, end });
  },
  // Location
  updateLocation(state, value) {
    console.log("### mutation: updateLocation", value);
    state.activity.location = value;
  },
  addAvailableLocationGroup(state, group) {
    state.availableLocations.push(group);
  },
};
