import { createApp } from "vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import { appMessages } from "./i18n";
import store from "./store";

import App from "./App.vue";

const i18n = _createI18n(appMessages);

const app = createApp({
  template: `<app></app>`,
})
  .use(store)
  .use(i18n)
  .component("app", App)
  .mount("#calendar");
