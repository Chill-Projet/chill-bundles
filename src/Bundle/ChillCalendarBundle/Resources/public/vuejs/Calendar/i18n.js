import { personMessages } from "ChillPersonAssets/vuejs/_js/i18n";
import { calendarUserSelectorMessages } from "../_components/CalendarUserSelector/js/i18n";
import { activityMessages } from "ChillActivityAssets/vuejs/Activity/i18n";

const appMessages = {
  fr: {
    choose_your_date: "Sélectionnez votre plage",
    activity: {
      add_persons: "Ajouter des personnes concernées",
      bloc_persons: "Usagers",
      bloc_persons_associated: "Usagers du parcours",
      bloc_persons_not_associated: "Tiers non-pro.",
      bloc_thirdparty: "Tiers professionnels",
      bloc_users: "T(M)S",
    },
    this_calendar_range_will_change_main_user:
      "Cette plage de disponibilité n'est pas celle de l'utilisateur principal. Si vous continuez, l'utilisateur principal sera adapté. Êtes-vous sûr·e ?",
    will_change_main_user_for_me:
      "Vous ne pouvez pas écrire dans le calendrier d'un autre utilisateur. Voulez-vous être l'utilisateur principal de ce rendez-vous ?",
    main_user_is_mandatory:
      "L'utilisateur principal est requis. Vous pouvez le modifier, mais pas le supprimer",
    change_main_user_will_reset_event_data:
      "Modifier l'utilisateur principal nécessite de choisir une autre plage de disponibilité ou un autre horaire. Ces informations seront perdues. Êtes-vous sûr·e de vouloir continuer ?",
    list_three_days: "Liste 3 jours",
    current_selected: "Rendez-vous fixé",
    main_user: "Utilisateur principal",
  },
};

Object.assign(appMessages.fr, personMessages.fr);
Object.assign(appMessages.fr, calendarUserSelectorMessages.fr);
Object.assign(appMessages.fr, activityMessages.fr);

export { appMessages };
