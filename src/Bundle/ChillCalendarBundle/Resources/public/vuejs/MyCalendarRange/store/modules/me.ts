import { State } from "./../index";
import { User } from "../../../../../../../ChillMainBundle/Resources/public/types";
import { ActionContext } from "vuex";

export interface MeState {
    me: User | null;
}

type Context = ActionContext<MeState, State>;

export default {
    namespaced: true,
    state: (): MeState => ({
        me: null,
    }),
    getters: {
        getMe: function (state: MeState): User | null {
            return state.me;
        },
    },
    mutations: {
        setWhoAmi(state: MeState, me: User) {
            state.me = me;
        },
    },
};
