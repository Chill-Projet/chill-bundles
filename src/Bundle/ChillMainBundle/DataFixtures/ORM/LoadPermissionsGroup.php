<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadPermissionsGroup extends AbstractFixture implements OrderedFixtureInterface
{
    public static $permissionGroup = [
        [
            'name' => 'social',
            'role_scopes' => [
                'role_scope_CHILL_FOO_EDIT_social',
                'role_scope_CHILL_FOO_SEE_administrative',
                'role_scope_CHILL_FOO_EDIT_all',
            ],
        ],
        [
            'name' => 'administrative',
            'role_scopes' => [
                'role_scope_CHILL_FOO_SEE_social',
                'role_scope_CHILL_FOO_EDIT_administrative',
                'role_scope_CHILL_FOO_EDIT_all',
            ],
        ],
        [
            'name' => 'direction',
            'role_scopes' => [
                'role_scope_CHILL_FOO_EDIT_all',
                'role_scope_CHILL_FOO_SEE_DETAILS_social',
                'role_scope_CHILL_FOO_SEE_DETAILS_administrative',
            ],
        ],
    ];

    public static $refs = [];

    public function getOrder(): int
    {
        return 400;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (static::$permissionGroup as $new) {
            $permissionGroup = new PermissionsGroup();
            $permissionGroup->setName($new['name']);

            foreach ($new['role_scopes'] as $roleScopeRef) {
                $permissionGroup->addRoleScope($this->getReference($roleScopeRef, RoleScope::class));
            }

            $manager->persist($permissionGroup);
            $reference = 'permission_group_'.$new['name'];
            echo "Creating {$reference} \n";
            $this->setReference($reference, $permissionGroup);
            static::$refs[] = $reference;
        }

        $manager->flush();
    }
}
