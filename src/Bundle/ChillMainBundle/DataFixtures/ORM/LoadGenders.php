<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Entity\GenderEnum;
use Chill\MainBundle\Entity\GenderIconEnum;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadGenders extends AbstractFixture implements OrderedFixtureInterface
{
    private array $genders = [
        [
            'label' => ['en' => 'man', 'fr' => 'homme'],
            'genderTranslation' => GenderEnum::MALE,
            'icon' => GenderIconEnum::MALE,
        ],
        [
            'label' => ['en' => 'woman', 'fr' => 'femme'],
            'genderTranslation' => GenderEnum::FEMALE,
            'icon' => GenderIconEnum::FEMALE,
        ],
        [
            'label' => ['en' => 'neutral', 'fr' => 'neutre'],
            'genderTranslation' => GenderEnum::NEUTRAL,
            'icon' => GenderIconEnum::NEUTRAL,
        ],
    ];

    public function getOrder(): int
    {
        return 100;
    }

    public function load(ObjectManager $manager): void
    {
        echo "loading genders... \n";

        foreach ($this->genders as $g) {
            echo $g['label']['fr'].' ';
            $new_g = new Gender();
            $new_g->setGenderTranslation($g['genderTranslation']);
            $new_g->setLabel($g['label']);
            $new_g->setIcon($g['icon']);

            $this->addReference('g_'.$g['genderTranslation']->value, $new_g);
            $manager->persist($new_g);
        }

        $manager->flush();
    }
}
