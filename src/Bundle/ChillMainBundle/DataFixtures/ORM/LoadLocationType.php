<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\LocationType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load location types into database.
 */
class LoadLocationType extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private ?ContainerInterface $container = null;

    public function getOrder(): int
    {
        return 52;
    }

    public function load(ObjectManager $manager): void
    {
        echo "loading some location type... \n";

        $arr = [
            [
                'name' => ['fr' => 'Mairie'],
                'address_required' => LocationType::STATUS_OPTIONAL,
            ],
            [
                'name' => ['fr' => 'Guichet d\'accueil'],
                'address_required' => LocationType::STATUS_OPTIONAL,
            ],
            [
                'name' => ['fr' => 'Domicile de l\'usager'],
                'address_required' => LocationType::STATUS_REQUIRED,
            ],
            [
                'name' => ['fr' => 'Centre d\'aide sociale'],
                'address_required' => LocationType::STATUS_OPTIONAL,
            ],
        ];

        foreach ($arr as $a) {
            $locationType = (new LocationType())
                ->setTitle($a['name'])
                ->setAvailableForUsers(true)
                ->setActive(true)
                ->setAddressRequired($a['address_required']);
            $manager->persist($locationType);
        }

        $manager->flush();
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
