<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadUserGroup extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['user-group'];
    }

    public function load(ObjectManager $manager): void
    {
        $centerASocial = $manager->getRepository(User::class)->findOneBy(['username' => 'center a_social']);
        $centerBSocial = $manager->getRepository(User::class)->findOneBy(['username' => 'center b_social']);
        $multiCenter = $manager->getRepository(User::class)->findOneBy(['username' => 'multi_center']);
        $administrativeA = $manager->getRepository(User::class)->findOneBy(['username' => 'center a_administrative']);
        $administrativeB = $manager->getRepository(User::class)->findOneBy(['username' => 'center b_administrative']);

        $level1 = $this->generateLevelGroup('Niveau 1', '#eec84aff', '#000000ff', 'level');
        $level1->addUser($centerASocial)->addUser($centerBSocial);
        $manager->persist($level1);

        $level2 = $this->generateLevelGroup('Niveau 2', ' #e2793dff', '#000000ff', 'level');
        $level2->addUser($multiCenter);
        $manager->persist($level2);

        $level3 = $this->generateLevelGroup('Niveau 3', ' #df4949ff', '#000000ff', 'level');
        $level3->addUser($multiCenter);
        $manager->persist($level3);

        $tss = $this->generateLevelGroup('Travailleur sociaux', '#43b29dff', '#000000ff', '');
        $tss->addUser($multiCenter)->addUser($centerASocial)->addUser($centerBSocial);
        $manager->persist($tss);
        $admins = $this->generateLevelGroup('Administratif', '#334d5cff', '#000000ff', '');
        $admins->addUser($administrativeA)->addUser($administrativeB);
        $manager->persist($admins);

        $manager->flush();
    }

    private function generateLevelGroup(string $title, string $backgroundColor, string $foregroundColor, string $excludeKey): UserGroup
    {
        $userGroup = new UserGroup();

        return $userGroup
            ->setLabel(['fr' => $title])
            ->setBackgroundColor($backgroundColor)
            ->setForegroundColor($foregroundColor)
            ->setExcludeKey($excludeKey)
        ;
    }
}
