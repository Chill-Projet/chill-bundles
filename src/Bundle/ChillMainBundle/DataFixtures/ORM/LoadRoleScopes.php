<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadRoleScopes extends AbstractFixture implements OrderedFixtureInterface
{
    public static $permissions = [
        'CHILL_FOO_SEE' => [
            'names' => [
                'fr' => 'voir foo',
                'en' => 'see foo',
                'nl' => 'zie foo',
            ],
        ],
        'CHILL_FOO_SEE_DETAILS' => [
            'names' => [
                'fr' => 'voir foo avec détails',
                'en' => 'see foo with details',
                'nl' => 'zie foo in details',
            ],
        ],
        'CHILL_FOO_EDIT' => [
            'names' => [
                'fr' => 'modifier foo',
                'en' => 'edit foo',
                'nl' => 'editie foo',
            ],
        ],
    ];

    public static $references = [];

    public function getOrder(): int
    {
        return 300;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (static::$permissions as $key => $permission) {
            foreach (LoadScopes::$references as $scopeReference) {
                $roleScope = new RoleScope();
                $roleScope->setRole($key)
                    ->setScope($this->getReference($scopeReference, Scope::class));
                $reference = 'role_scope_'.$key.'_'.$this->getReference($scopeReference, Scope::class)->getName()['en'];
                echo "Creating {$reference} \n";
                $this->addReference($reference, $roleScope);
                $manager->persist($roleScope);
                static::$references[] = $reference;
            }
        }

        $manager->flush();
    }
}
