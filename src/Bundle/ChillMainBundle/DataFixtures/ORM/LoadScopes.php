<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Create scopes.
 */
class LoadScopes extends AbstractFixture implements OrderedFixtureInterface
{
    public static $references = [];

    public $scopes = [
        [
            'names' => [
                'fr' => 'tous',
                'en' => 'all',
                'nl' => 'algemeen',
            ],
        ],
        [
            'names' => [
                'fr' => 'social',
                'en' => 'social',
                'nl' => 'sociaal',
            ],
        ],
        [
            'names' => [
                'fr' => 'administratif',
                'en' => 'administrative',
                'nl' => 'administratief',
            ],
        ],
    ];

    public function getOrder(): int
    {
        return 200;
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->scopes as $new) {
            $scope = new \Chill\MainBundle\Entity\Scope();
            $scope->setName($new['names']);

            $manager->persist($scope);
            $reference = 'scope_'.$new['names']['en'];
            $this->addReference($reference, $scope);
            static::$references[] = $reference;
        }

        $manager->flush();
    }
}
