<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Command\LoadCountriesCommand;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load countries into database.
 */
class LoadCountries extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private ?ContainerInterface $container = null;

    public function getOrder(): int
    {
        return 20;
    }

    public function load(ObjectManager $manager): void
    {
        echo "loading countries... \n";

        $languages = $this->container->getParameter('chill_main.available_languages');

        foreach (LoadCountriesCommand::prepareCountryList($languages) as $country) {
            $manager->persist($country);
        }

        $manager->flush();
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
