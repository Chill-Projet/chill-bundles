<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\GroupCenter;
use Chill\MainBundle\Entity\PermissionsGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadGroupCenters extends AbstractFixture implements OrderedFixtureInterface
{
    public static $refs = [];

    public function getOrder(): int
    {
        return 500;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (LoadCenters::$refs as $centerRef) {
            foreach (LoadPermissionsGroup::$refs as $permissionGroupRef) {
                $GroupCenter = new GroupCenter();
                $GroupCenter->setCenter($this->getReference($centerRef, Center::class));
                $GroupCenter->setPermissionsGroup($this->getReference($permissionGroupRef, PermissionsGroup::class));

                $manager->persist($GroupCenter);

                $reference = $centerRef.'_'.$permissionGroupRef;
                $this->addReference($reference, $GroupCenter);
                static::$refs[] = $reference;
                echo "Creating {$reference}...  \n";
            }
        }

        $manager->flush();
    }
}
