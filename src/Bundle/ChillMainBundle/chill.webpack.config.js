
// Compile and loads all assets from the Chill Main Bundle
module.exports = function (encore, entries) {
  // Push into "chill" entrypoint
  entries.push(__dirname + "/Resources/public/chill/index.js");

  // Aliases are used when webpack is trying to resolve modules path
  encore.addAliases({
    ChillMainAssets: __dirname + "/Resources/public",
    ShowHide: __dirname + "/Resources/public/lib/show_hide",
    Tabs: __dirname + "/Resources/public/lib/tabs",
  });

  // Page entrypoints
  encore.addEntry(
    "page_login",
    __dirname + "/Resources/public/page/login/index.js",
  );
  encore.addEntry(
    "page_location",
    __dirname + "/Resources/public/page/location/index.js",
  );
  encore.addEntry(
    "page_workflow_show",
    __dirname + "/Resources/public/page/workflow-show/index.js",
  );
  encore.addEntry(
    "page_homepage_widget",
    __dirname + "/Resources/public/page/homepage_widget/index.js",
  );
  encore.addEntry(
    "page_export",
    __dirname + "/Resources/public/page/export/index.js",
  );
  encore.addEntry(
    "page_download_exports",
    __dirname + "/Resources/public/page/export/download-export.js",
  );

  // Modules entrypoints
  encore.addEntry(
    "mod_collection",
    __dirname + "/Resources/public/module/collection/index.ts",
  );
  encore.addEntry(
    "mod_forkawesome",
    __dirname + "/Resources/public/module/forkawesome/index.js",
  );
  encore.addEntry(
    "mod_bootstrap",
    __dirname + "/Resources/public/module/bootstrap/index.js",
  );
  encore.addEntry(
    "mod_ckeditor5",
    __dirname + "/Resources/public/module/ckeditor5/index",
  );
  encore.addEntry(
    "mod_disablebuttons",
    __dirname + "/Resources/public/module/disable-buttons/index.js",
  );
  encore.addEntry(
    "mod_blur",
    __dirname + "/Resources/public/module/blur/index.js",
  );
  encore.addEntry(
    "mod_input_address",
    __dirname + "/Resources/public/vuejs/Address/mod_input_address_index.js",
  );
  encore.addEntry(
    "mod_notification_toggle_read_status",
    __dirname + "/Resources/public/module/notification/toggle_read.js",
  );
  encore.addEntry(
    "mod_notification_toggle_read_all_status",
    __dirname + "/Resources/public/module/notification/toggle_read_all.ts",
  );
  encore.addEntry(
    "mod_pickentity_type",
    __dirname + "/Resources/public/module/pick-entity/index.js",
  );
  encore.addEntry(
    "mod_entity_workflow_subscribe",
    __dirname + "/Resources/public/module/entity-workflow-subscribe/index.js",
  );
  encore.addEntry(
    "mod_entity_workflow_pick",
    __dirname + "/Resources/public/module/entity-workflow-pick/index.js",
  );
  encore.addEntry(
    "mod_wopi_link",
    __dirname + "/Resources/public/module/wopi-link/index.js",
  );
  encore.addEntry(
    "mod_pick_postal_code",
    __dirname + "/Resources/public/module/pick-postal-code/index.js",
  );
  encore.addEntry(
    "mod_pick_rolling_date",
    __dirname + "/Resources/public/module/pick-rolling-date/index.js",
  );
  encore.addEntry(
    "mod_address_details",
    __dirname + "/Resources/public/module/address-details/index",
  );
  encore.addEntry(
    "mod_news",
    __dirname + "/Resources/public/module/news/index.js",
  );
  encore.addEntry(
    "mod_workflow_attachment",
    __dirname + "/Resources/public/vuejs/WorkflowAttachment/index",
  );

  // Vue entrypoints
  encore.addEntry(
    "vue_address",
    __dirname + "/Resources/public/vuejs/Address/index.js",
  );
  encore.addEntry(
    "vue_onthefly",
    __dirname + "/Resources/public/vuejs/OnTheFly/index.js",
  );
};
