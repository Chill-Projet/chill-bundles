<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Cron;

use Chill\MainBundle\Entity\CronJobExecution;

interface CronJobInterface
{
    public function canRun(?CronJobExecution $cronJobExecution): bool;

    public function getKey(): string;

    /**
     * Execute the cronjob.
     *
     * If data is returned, this data is passed as argument on the next execution
     *
     * @param array<string|int, int|float|string|bool|array<int|string, int|float|string|bool>> $lastExecutionData the data which was returned from the previous execution
     *
     * @return array<string|int, int|float|string|bool|array<int|string, int|float|string|bool>>|null optionally return an array with the same data than the previous execution
     */
    public function run(array $lastExecutionData): ?array;
}
