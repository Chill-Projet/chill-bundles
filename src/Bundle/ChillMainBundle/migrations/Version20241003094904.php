<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241003094904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create tables for EntityWorkflowSend and EntityWorkflowSendView';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_workflow_entity_send_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_main_workflow_entity_send_views_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_workflow_entity_send (id INT NOT NULL,'
            .' destineeEmail TEXT DEFAULT \'\' NOT NULL, uuid UUID NOT NULL, privateToken VARCHAR(255) NOT NULL,'
            .' numberOfErrorTrials INT DEFAULT 0 NOT NULL, expireAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
            .'createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, destineeThirdParty_id INT DEFAULT NULL, '
            .'entityWorkflowStep_id INT NOT NULL, createdBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A0C0620FD17F50A6 ON chill_main_workflow_entity_send (uuid)');
        $this->addSql('CREATE INDEX IDX_A0C0620FDDFA98DE ON chill_main_workflow_entity_send (destineeThirdParty_id)');
        $this->addSql('CREATE INDEX IDX_A0C0620F3912FED6 ON chill_main_workflow_entity_send (entityWorkflowStep_id)');
        $this->addSql('CREATE INDEX IDX_A0C0620F3174800F ON chill_main_workflow_entity_send (createdBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_send.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_send.expireAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_send.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chill_main_workflow_entity_send_views (id INT NOT NULL, send_id INT NOT NULL, '
            .'viewAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, remoteIp TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2659558513933E7B ON chill_main_workflow_entity_send_views (send_id)');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_send_views.viewAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send ADD CONSTRAINT FK_A0C0620FDDFA98DE FOREIGN KEY (destineeThirdParty_id) REFERENCES chill_3party.third_party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send ADD CONSTRAINT FK_A0C0620F3912FED6 FOREIGN KEY (entityWorkflowStep_id) REFERENCES chill_main_workflow_entity_step (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send ADD CONSTRAINT FK_A0C0620F3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send_views ADD CONSTRAINT FK_2659558513933E7B FOREIGN KEY (send_id) REFERENCES chill_main_workflow_entity_send (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_main_workflow_entity_send_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_main_workflow_entity_send_views_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send DROP CONSTRAINT FK_A0C0620FDDFA98DE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send DROP CONSTRAINT FK_A0C0620F3912FED6');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send DROP CONSTRAINT FK_A0C0620F3174800F');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_send_views DROP CONSTRAINT FK_2659558513933E7B');
        $this->addSql('DROP TABLE chill_main_workflow_entity_send');
        $this->addSql('DROP TABLE chill_main_workflow_entity_send_views');
    }
}
