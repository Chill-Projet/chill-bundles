<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241129112740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add table for EntityWorkflowAttachment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_workflow_entity_attachment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_workflow_entity_attachment (id INT NOT NULL, relatedGenericDocKey VARCHAR(255) NOT NULL, relatedGenericDocIdentifiers JSONB NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, entityWorkflow_id INT NOT NULL, createdBy_id INT DEFAULT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_279415FFFB054143 ON chill_main_workflow_entity_attachment (entityWorkflow_id)');
        $this->addSql('CREATE INDEX IDX_279415FF3174800F ON chill_main_workflow_entity_attachment (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_279415FF65FF1AEC ON chill_main_workflow_entity_attachment (updatedBy_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_generic_doc_by_workflow ON chill_main_workflow_entity_attachment (relatedGenericDocKey, relatedGenericDocIdentifiers, entityworkflow_id)');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_attachment.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_workflow_entity_attachment.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment ADD CONSTRAINT FK_279415FFFB054143 FOREIGN KEY (entityWorkflow_id) REFERENCES chill_main_workflow_entity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment ADD CONSTRAINT FK_279415FF3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment ADD CONSTRAINT FK_279415FF65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment ADD storedobject_id INT NOT NULL');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment ADD CONSTRAINT FK_279415FFEE684399 FOREIGN KEY (storedobject_id) REFERENCES chill_doc.stored_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_279415FFEE684399 ON chill_main_workflow_entity_attachment (storedobject_id)');
        $this->addSql('ALTER INDEX idx_279415fffb054143 RENAME TO IDX_279415FF7D99CE94');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_main_workflow_entity_attachment_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment DROP CONSTRAINT FK_279415FFFB054143');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment DROP CONSTRAINT FK_279415FF3174800F');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_attachment DROP CONSTRAINT FK_279415FF65FF1AEC');
        $this->addSql('DROP TABLE chill_main_workflow_entity_attachment');
    }
}
