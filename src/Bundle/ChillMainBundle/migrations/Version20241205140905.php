<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241205140905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Restore the nullable person phone, as this is used by comments';
    }

    public function up(Schema $schema): void
    {
        // this should be already not null, but helps some instances, where a migration which drop not null
        // was executed, and was wrong.
        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber DROP NOT NULL');
    }

    public function down(Schema $schema): void {}
}
