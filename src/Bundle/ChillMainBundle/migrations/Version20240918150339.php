<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240918150339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set not null and defaults on columns. Drop customs column on address';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address DROP customs');
        $this->addSql('ALTER TABLE chill_main_dashboard_config_item ALTER metadata SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_location ALTER active SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_location_type ALTER active SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_location_type ALTER editablebyusers SET NOT NULL');
        $this->addSql('ALTER TABLE country ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE permission_groups ALTER name SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE users ALTER attributes SET DEFAULT \'[]\'');
    }

    public function down(Schema $schema): void {}
}
