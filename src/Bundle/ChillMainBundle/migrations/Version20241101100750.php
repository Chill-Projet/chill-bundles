<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241101100750 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rename indexes all capital';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER INDEX idx_154a075fef1a9d84 RENAME TO IDX_589D51B4EF1A9D84');
        $this->addSql('ALTER INDEX idx_154a075fa76ed395 RENAME TO IDX_589D51B4A76ED395');
        $this->addSql('ALTER INDEX idx_a9f001fa7e6af9d4 RENAME TO IDX_D63001607E6AF9D4');
        $this->addSql('ALTER INDEX idx_a9f001faa76ed395 RENAME TO IDX_D6300160A76ED395');
        $this->addSql('ALTER INDEX idx_64a4a621504cb38d RENAME TO IDX_E260A868504CB38D');
        $this->addSql('ALTER INDEX idx_92351ece727aca70 RENAME TO IDX_72D110E8727ACA70');
        $this->addSql('ALTER INDEX idx_person_center RENAME TO IDX_BF210A145932F377');
        $this->addSql('ALTER INDEX idx_3370d4403818da5 RENAME TO IDX_BF210A143818DA5');
        $this->addSql('ALTER INDEX idx_3370d4401c9da55 RENAME TO IDX_BF210A141C9DA55');
        $this->addSql('ALTER INDEX idx_9bc1fd50f5b7af75 RENAME TO IDX_B9CEBEACF5B7AF75');
        $this->addSql('ALTER INDEX idx_9bc1fd50dca38092 RENAME TO IDX_B9CEBEACDCA38092');
        $this->addSql('ALTER INDEX idx_9bc1fd508dfc48dc RENAME TO IDX_B9CEBEAC8DFC48DC');
        $this->addSql('ALTER INDEX idx_9bc1fd50217bbb47 RENAME TO IDX_B9CEBEAC217BBB47');
        $this->addSql('ALTER INDEX idx_40fb5d6dfec418b RENAME TO IDX_7A6FDBEFEC418B');
        $this->addSql('ALTER INDEX idx_286dc95df53b66 RENAME TO IDX_7A48DF7F5DF53B66');
        $this->addSql('ALTER INDEX idx_a14d8f3d447bbb3b RENAME TO IDX_A14D8F3D96DF1F10');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER INDEX idx_72d110e8727aca70 RENAME TO idx_92351ece727aca70');
        $this->addSql('ALTER INDEX idx_589d51b4a76ed395 RENAME TO idx_154a075fa76ed395');
        $this->addSql('ALTER INDEX idx_589d51b4ef1a9d84 RENAME TO idx_154a075fef1a9d84');
        $this->addSql('ALTER INDEX idx_bf210a143818da5 RENAME TO idx_3370d4403818da5');
        $this->addSql('ALTER INDEX idx_bf210a141c9da55 RENAME TO idx_3370d4401c9da55');
        $this->addSql('ALTER INDEX idx_bf210a145932f377 RENAME TO idx_person_center');
        $this->addSql('ALTER INDEX idx_e260a868504cb38d RENAME TO idx_64a4a621504cb38d');
        $this->addSql('ALTER INDEX idx_d63001607e6af9d4 RENAME TO idx_a9f001fa7e6af9d4');
        $this->addSql('ALTER INDEX idx_d6300160a76ed395 RENAME TO idx_a9f001faa76ed395');
        $this->addSql('ALTER INDEX idx_7a48df7f5df53b66 RENAME TO idx_286dc95df53b66');
        $this->addSql('ALTER INDEX idx_a14d8f3d96df1f10 RENAME TO idx_a14d8f3d447bbb3b');
        $this->addSql('ALTER INDEX idx_b9cebeacdca38092 RENAME TO idx_9bc1fd50dca38092');
        $this->addSql('ALTER INDEX idx_b9cebeacf5b7af75 RENAME TO idx_9bc1fd50f5b7af75');
        $this->addSql('ALTER INDEX idx_b9cebeac8dfc48dc RENAME TO idx_9bc1fd508dfc48dc');
        $this->addSql('ALTER INDEX idx_b9cebeac217bbb47 RENAME TO idx_9bc1fd50217bbb47');
        $this->addSql('ALTER INDEX idx_7a6fdbefec418b RENAME TO idx_40fb5d6dfec418b');
    }
}
