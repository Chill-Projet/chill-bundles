<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241015142142 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add an email address to user groups';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_user_group ADD email TEXT DEFAULT \'\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_user_group DROP COLUMN email');
    }
}
