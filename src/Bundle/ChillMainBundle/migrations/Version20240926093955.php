<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240926093955 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create gender table and default entities';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_gender_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_gender (id INT NOT NULL, label JSON NOT NULL, active BOOLEAN NOT NULL, genderTranslation VARCHAR(255) NOT NULL, icon VARCHAR(255) NOT NULL, ordering DOUBLE PRECISION DEFAULT \'0.0\', PRIMARY KEY(id))');

        // Insert the four gender records into the chill_main_gender table
        $this->addSql("
            INSERT INTO chill_main_gender (id, label, active, genderTranslation, icon, ordering)
            VALUES
            (nextval('chill_main_gender_id_seq'),
             '{\"fr\": \"homme\", \"nl\": \"man\"}',
             true,
             'man',
             'bi bi-gender-male',
             1.0
            ),
            (nextval('chill_main_gender_id_seq'),
             '{\"fr\": \"femme\", \"nl\": \"vrouw\"}',
             true,
             'woman',
             'bi bi-gender-female',
             1.1
            ),
            (nextval('chill_main_gender_id_seq'),
             '{\"fr\": \"neutre\", \"nl\": \"neutraal\"}',
             true,
             'neutral',
             'bi bi-gender-neuter',
             1.1
            ),
            (nextval('chill_main_gender_id_seq'),
             '{\"fr\": \"inconnu\", \"nl\": \"ongekend\"}',
             true,
             'unknown',
             'bi bi-question',
             1.2
            )
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_main_gender_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_main_gender');
    }
}
