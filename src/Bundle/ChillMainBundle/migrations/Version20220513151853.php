<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220513151853 extends AbstractMigration
{
    public function down(Schema $schema): void {}

    public function getDescription(): string
    {
        return 'set default on attributes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('update users set attributes = \'[]\'::json where attributes IS NULL');
    }
}
