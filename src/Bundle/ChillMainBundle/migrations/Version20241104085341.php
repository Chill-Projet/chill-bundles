<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241104085341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Define NULL on entity properties';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_dashboard_config_item ALTER metadata SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_location_type ALTER editablebyusers SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER privatecomment_comments SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_relationships ALTER createdby_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_relationships ALTER createdat DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdby_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdat DROP NOT NULL');
        $this->addSql('ALTER TABLE country ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE customfield ALTER required SET NOT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER firstname SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER privateComment_comments DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_location_type ALTER editableByUsers DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_dashboard_config_item ALTER metadata DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ALTER firstname DROP NOT NULL');
        $this->addSql('ALTER TABLE country ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household_composition_type ALTER label DROP NOT NULL');
        $this->addSql('ALTER TABLE customfieldsgroup ALTER options DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdAt SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_resource ALTER createdBy_id SET NOT NULL');
        $this->addSql('ALTER TABLE customfield ALTER required DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_relations ALTER isActive SET NOT NULL');
    }
}
