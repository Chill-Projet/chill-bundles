<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230301155213 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Alter type to TEXT for regroupment.name';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE regroupment ALTER name TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE regroupment ALTER name TYPE VARCHAR(15)');
    }
}
