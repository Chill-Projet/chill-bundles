<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Phonenumber;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Templating extends AbstractExtension
{
    public function __construct(protected PhonenumberHelper $phonenumberHelper) {}

    public function formatPhonenumber($phonenumber)
    {
        return $this->phonenumberHelper->format($phonenumber) ?? $phonenumber;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('chill_format_phonenumber', $this->formatPhonenumber(...)),
        ];
    }
}
