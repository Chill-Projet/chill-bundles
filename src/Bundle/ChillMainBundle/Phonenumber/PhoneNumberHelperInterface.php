<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Phonenumber;

use libphonenumber\PhoneNumber;

/**
 * Helper to some task linked to phonenumber.
 *
 * Currently, only Twilio is supported (https://www.twilio.com/lookup). A method
 * allow to check if the helper is configured for validation. This should be used
 * before doing some validation.
 */
interface PhoneNumberHelperInterface
{
    public function format(?PhoneNumber $phoneNumber = null): string;

    /**
     * Get type (mobile, landline, ...) for phone number.
     */
    public function getType(PhoneNumber $phonenumber): string;

    /**
     * Return true if the validation is configured and available.
     */
    public function isPhonenumberValidationConfigured(): bool;

    /**
     * Return true if the phonenumber is a landline or voip phone.  Return always true
     * if the validation is not configured.
     */
    public function isValidPhonenumberAny(string $phonenumber): bool;

    /**
     * Return true if the phonenumber is a landline or voip phone.  Return always true
     * if the validation is not configured.
     */
    public function isValidPhonenumberLandOrVoip(string $phonenumber): bool;

    /**
     * REturn true if the phoennumber is a mobile phone. Return always true
     * if the validation is not configured.
     */
    public function isValidPhonenumberMobile(string $phonenumber): bool;
}
