<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD)]
class PhonenumberConstraint extends Constraint
{
    public $notLandlineMessage = 'This is not a landline phonenumber';

    public $notMobileMessage = 'This is not a mobile phonenumber';

    public $notValidMessage = 'This is not a valid phonenumber';

    /**
     * The type of phone: landline (not able to receive sms) or mobile (can receive sms).
     *
     * @var string 'landline', 'mobile' or 'any'
     */
    public string $type;

    public function __construct(?array $options = null, ?string $type = null, ?array $groups = null, $payload = null)
    {
        parent::__construct($options ?? [], $groups, $payload);
        $this->type = $type ?? 'any';
    }

    public function validatedBy()
    {
        return \Chill\MainBundle\Validation\Validator\ValidPhonenumber::class;
    }
}
