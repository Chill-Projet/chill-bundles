<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class UserGroupDoNotExclude extends Constraint
{
    public string $message = 'The groups {{ excluded_groups }} do exclude themselves. Please choose one between them';
    public string $code = 'e16c8226-0090-11ef-8560-f7239594db09';

    public function getTargets()
    {
        return [self::PROPERTY_CONSTRAINT];
    }

    public function validatedBy()
    {
        return \Chill\MainBundle\Validation\Validator\UserGroupDoNotExclude::class;
    }
}
