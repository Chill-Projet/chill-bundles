<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search\Model;

class Result
{
    public function __construct(
        private readonly float $relevance,
        /**
         * mixed an arbitrary result.
         */
        private $result,
    ) {}

    public function getRelevance(): float
    {
        return $this->relevance;
    }

    public function getResult()
    {
        return $this->result;
    }
}
