<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search\Entity;

use Chill\MainBundle\Repository\UserGroupRepositoryInterface;
use Chill\MainBundle\Search\SearchApiInterface;
use Chill\MainBundle\Search\SearchApiQuery;
use Symfony\Contracts\Translation\LocaleAwareInterface;

/**
 * Provide search api for user group.
 */
class SearchUserGroupApiProvider implements SearchApiInterface, LocaleAwareInterface
{
    private string $locale;

    public function __construct(private readonly UserGroupRepositoryInterface $userGroupRepository) {}

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getResult(string $key, array $metadata, float $pertinence)
    {
        return $this->userGroupRepository->find($metadata['id']);
    }

    public function prepare(array $metadatas): void {}

    public function provideQuery(string $pattern, array $parameters): SearchApiQuery
    {
        return $this->userGroupRepository->provideSearchApiQuery($pattern, $this->getLocale(), 'user-group');
    }

    public function supportsResult(string $key, array $metadatas): bool
    {
        return 'user-group' === $key;
    }

    public function supportsTypes(string $pattern, array $types, array $parameters): bool
    {
        return in_array('user-group', $types, true);
    }
}
