<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

/**
 * This interface must be implemented on services which provide search results.
 *
 * @todo : write doc and add a link to documentation
 */
interface SearchInterface
{
    /**
     * Supplementary parameters to the query string.
     */
    public const REQUEST_QUERY_KEY_ADD_PARAMETERS = 'add_q';

    /**
     * Request parameters contained inside the `add_q` parameters.
     */
    public const REQUEST_QUERY_PARAMETERS = '_search_parameters';

    public const SEARCH_PREVIEW_OPTION = '_search_preview';

    /**
     * the order in which the results will appears in the global view.
     *
     * (this may be eventually defined in config.yml)
     */
    public function getOrder(): int;

    /**
     * we may desactive the search interface by default. in this case,
     * the search will be launch and rendered only with "advanced search".
     *
     * this may be activated/desactived from bundle definition in config.yml
     *
     * @return bool
     */
    public function isActiveByDefault();

    /**
     * return the result in a html string. The string will be inclued (as raw)
     * into a global view.
     *
     * The global view may be :
     * {% for result as resultsFromDifferentSearchInterface %}
     *    {{ result|raw }}
     * {% endfor %}
     *
     * **available options** :
     * - SEARCH_PREVIEW_OPTION (boolean) : if renderResult should return a "preview" of
     * the results. In this case, a subset of results should be returned, and,
     * if the query return more results, a button "see all results" should be
     * displayed at the end of the list.
     *
     * **Interaction between `start` and `limit` and pagination : you should
     * take only the given parameters into account; the results from pagination
     * should be ignored. (Most of the time, it should be the same).
     *
     * @param array         $terms  the string to search
     * @param int           $start  the first result (for pagination)
     * @param int           $limit  the number of result (for pagination)
     * @param "html"|"json" $format The format for result
     *
     * @return string|array a string if format is html, an array if format is json
     */
    public function renderResult(array $terms, $start = 0, $limit = 50, array $options = [], $format = 'html');

    /**
     * indicate if the implementation supports the given domain.
     *
     * @return bool
     */
    public function supports($domain, $format);
}
