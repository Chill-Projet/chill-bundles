<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\Exception\NotificationHandlerNotFound;

final readonly class NotificationHandlerManager
{
    public function __construct(private iterable $handlers) {}

    /**
     * @throw NotificationHandlerNotFound if handler is not found
     */
    public function getHandler(Notification $notification, array $options = []): NotificationHandlerInterface
    {
        foreach ($this->handlers as $renderer) {
            if ($renderer->supports($notification, $options)) {
                return $renderer;
            }
        }

        throw new NotificationHandlerNotFound();
    }

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return $this->getHandler($notification, $options)->getTemplate($notification, $options);
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return $this->getHandler($notification, $options)->getTemplateData($notification, $options);
    }
}
