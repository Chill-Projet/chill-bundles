<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Class Mailer
 * Classe d'aide pour l'envoi de notification.
 *
 * Héberge toutes les méthodes pour ré-écrire les URL en fonction de la langue de l'utilisateur.
 *
 * @deprecated use the MailerInterface
 */
class Mailer
{
    /**
     * Mailer constructor.
     *
     * @param mixed[] $routeParameters
     */
    public function __construct(private readonly MailerInterface $mailer, private readonly LoggerInterface $logger, private readonly Environment $twig, private readonly RouterInterface $router, private readonly TranslatorInterface $translator, protected $routeParameters) {}

    /**
     * @return string
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function renderContentToUser(User $to, $template, array $parameters = [])
    {
        $context = $this->router->getContext();
        $previousHost = $context->getHost();
        $previousScheme = $context->getScheme();

        $context->setHost($this->routeParameters['host']);
        $context->setScheme($this->routeParameters['scheme']);

        $content = $this->twig->render($template, $parameters);

        // reset the host
        $context->setHost($previousHost);
        $context->setScheme($previousScheme);

        return $content;
    }

    /**
     * Envoie une notification à un utilisateur.
     *
     * @param array    $subject  Subject of the message [ 0 => $message (required), 1 => $parameters (optional), 3 => $domain (optional) ]
     * @param array    $bodies   The bodies. An array where keys are the contentType and values the bodies
     * @param callable $callback a callback to customize the message (add attachment, etc.)
     */
    public function sendNotification(
        mixed $recipient,
        array $subject,
        array $bodies,
        ?callable $callback = null,
        mixed $force = false,
    ) {
        $fromEmail = $this->routeParameters['from_email'];
        $fromName = $this->routeParameters['from_name'];
        $to = $recipient instanceof User ? $recipient->getEmail() : $recipient;

        $subjectI18n = $this->translator->trans(
            $subject[0],
            $subject[1] ?? [],
            $subject[2] ?? null
        );

        $email = new Email();
        $email->addTo($to)->subject($subjectI18n);

        foreach ($bodies as $contentType => $content) {
            match ($contentType) {
                'text/plain' => $email->text($content),
                default => $email->text($content),
            };
        }

        if (null !== $callback) {
            \call_user_func($callback, $email);
        }

        $this->logger->info('[notification] Sending notification', [
            'to' => $email->getTo(),
            'subject' => $email->getSubject(),
        ]);

        $this->mailer->send($email);
    }
}
