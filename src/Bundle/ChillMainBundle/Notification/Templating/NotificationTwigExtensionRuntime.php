<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification\Templating;

use Chill\MainBundle\Entity\NotificationComment;
use Chill\MainBundle\Form\NotificationCommentType;
use Chill\MainBundle\Notification\NotificationPresence;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;

class NotificationTwigExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(private readonly FormFactoryInterface $formFactory, private readonly NotificationPresence $notificationPresence, private readonly UrlGeneratorInterface $urlGenerator) {}

    public function counterNotificationFor(Environment $environment, string $relatedEntityClass, int $relatedEntityId, array $more = [], array $options = []): string
    {
        return $environment->render(
            '@ChillMain/Notification/extension_counter_notifications_for.html.twig',
            [
                'counter' => $this->notificationPresence->countNotificationsForClassAndEntity($relatedEntityClass, $relatedEntityId, $more),
            ]
        );
    }

    /**
     * @param list<array{relatedEntityClass: class-string, relatedEntityId: int}> $more
     */
    public function countNotificationsFor(string $relatedEntityClass, int $relatedEntityId, array $more = [], array $options = []): array
    {
        return $this->notificationPresence->countNotificationsForClassAndEntity($relatedEntityClass, $relatedEntityId, $more);
    }

    /**
     * @param list<array{relatedEntityClass: class-string, relatedEntityId: int}> $more
     */
    public function listNotificationsFor(Environment $environment, string $relatedEntityClass, int $relatedEntityId, array $more = [], array $options = []): string
    {
        $notifications = $this->notificationPresence->getNotificationsForClassAndEntity($relatedEntityClass, $relatedEntityId, $more);

        if ([] === $notifications) {
            return '';
        }

        $appendCommentForms = [];

        foreach ($notifications as $notification) {
            $appendComment = new NotificationComment();
            $appendCommentForms[$notification->getId()] = $this->formFactory->create(
                NotificationCommentType::class,
                $appendComment,
                [
                    'action' => $this->urlGenerator->generate(
                        'chill_main_notification_show',
                        ['id' => $notification->getId()]
                    ),
                ]
            )->createView();
        }

        return $environment->render('@ChillMain/Notification/extension_list_notifications_for.html.twig', [
            'notifications' => $notifications,
            'appendCommentForms' => $appendCommentForms,
        ]);
    }
}
