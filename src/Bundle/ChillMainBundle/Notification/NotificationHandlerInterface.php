<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\Notification;
use Symfony\Contracts\Translation\TranslatableInterface;

interface NotificationHandlerInterface
{
    /**
     * Return the template path (twig file).
     */
    public function getTemplate(Notification $notification, array $options = []): string;

    /**
     * Return an array which will be passed as data for the template.
     */
    public function getTemplateData(Notification $notification, array $options = []): array;

    /**
     * Return true if the handler supports the handling for this notification.
     */
    public function supports(Notification $notification, array $options = []): bool;

    public function getTitle(Notification $notification, array $options = []): TranslatableInterface;

    /*
     * return list<Person>
     */
    public function getAssociatedPersons(Notification $notification, array $options = []): array;

    public function getRelatedEntity(Notification $notification): ?object;
}
