<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\Notification;

/**
 * Store the notification.
 *
 * Those notification will be stored into database by the kernel. This
 * will also ensure that this happens outside of regular operations
 * operated by the entity manager.
 */
interface NotificationPersisterInterface
{
    /**
     * @return array|Notification[]
     */
    public function getWaitingNotifications(): array;

    public function persist(Notification $notification): void;
}
