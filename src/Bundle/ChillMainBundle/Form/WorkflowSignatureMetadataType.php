<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class WorkflowSignatureMetadataType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $parameterBag, private readonly TranslatableStringHelperInterface $translatableStringHelper) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $documentTypeChoices = $this->parameterBag->get('chill_main')['workflow_signature']['base_signer']['document_kinds'];

        $choices = [];

        foreach ($documentTypeChoices as $documentType) {
            $labels = [];

            foreach ($documentType['labels'] as $label) {
                $labels[$label['lang']] = $label['label'];
            }

            $localizedLabel = $this->translatableStringHelper->localize($labels);
            if (null !== $localizedLabel) {
                $choices[$localizedLabel] = $documentType['key'];
            }
        }

        $builder
            ->add('documentType', ChoiceType::class, [
                'label' => 'workflow.signature_zone.metadata.docType',
                'expanded' => false,
                'required' => true,
                'choices' => $choices,
            ])
            ->add('documentNumber', TextType::class, [
                'required' => true,
                'label' => 'workflow.signature_zone.metadata.docNumber',
            ])
            ->add('expirationDate', ChillDateType::class, [
                'required' => true,
                'input' => 'datetime_immutable',
                'label' => 'workflow.signature_zone.metadata.docExpiration',
            ]);
    }
}
