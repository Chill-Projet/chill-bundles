<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\LocationType as EntityLocationType;
use Chill\MainBundle\Form\Type\ChillPhoneNumberType;
use Chill\MainBundle\Form\Type\PickAddressType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class LocationFormType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locationType', EntityType::class, [
                'class' => EntityLocationType::class,
                'choice_attr' => static fn (EntityLocationType $entity) => [
                    'data-address' => $entity->getAddressRequired(),
                    'data-contact' => $entity->getContactData(),
                ],
                'choice_label' => fn (EntityLocationType $entity) => $this->translatableStringHelper->localize($entity->getTitle()),
            ])
            ->add('name', TextType::class)
            ->add('phonenumber1', ChillPhoneNumberType::class, ['required' => false])
            ->add('phonenumber2', ChillPhoneNumberType::class, ['required' => false])
            ->add('email', TextType::class, ['required' => false])
            ->add('address', PickAddressType::class, [
                'required' => false,
                'label' => 'Address',
                'use_valid_from' => false,
                'use_valid_to' => false,
            ])
            ->add(
                'active',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \Chill\MainBundle\Entity\Location::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_mainbundle_location';
    }
}
