<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type\Listing;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Templating\Listing\FilterOrderHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

final class FilterOrderType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FilterOrderHelper $helper */
        $helper = $options['helper'];

        if ($helper->hasSearchBox()) {
            $builder->add('q', SearchType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'filter_order.Search',
                ],
            ]);
        }

        $checkboxesBuilder = $builder->create('checkboxes', null, ['compound' => true]);

        foreach ($helper->getCheckboxes() as $name => $c) {
            $choices = self::buildCheckboxChoices($c['choices'], $c['trans']);

            $checkboxesBuilder->add($name, ChoiceType::class, [
                'choices' => $choices,
                'expanded' => true,
                'multiple' => true,
            ]);
        }

        if (0 < \count($helper->getCheckboxes())) {
            $builder->add($checkboxesBuilder);
        }

        if ([] !== $helper->getEntityChoices()) {
            $entityChoicesBuilder = $builder->create('entity_choices', null, ['compound' => true]);

            foreach ($helper->getEntityChoices() as $key => [
                'label' => $label, 'choices' => $choices, 'options' => $opts, 'class' => $class,
            ]) {
                $entityChoicesBuilder->add($key, EntityType::class, [
                    'label' => $label,
                    'choices' => $choices,
                    'class' => $class,
                    'multiple' => true,
                    'expanded' => true,
                    ...$opts,
                ]);
            }

            $builder->add($entityChoicesBuilder);
        }

        if (0 < \count($helper->getDateRanges())) {
            $dateRangesBuilder = $builder->create('dateRanges', null, ['compound' => true]);

            foreach ($helper->getDateRanges() as $name => $opts) {
                $rangeBuilder = $dateRangesBuilder->create($name, null, [
                    'compound' => true,
                    'label' => null === $opts['label'] ? false : $opts['label'] ?? $name,
                ]);

                $rangeBuilder->add(
                    'from',
                    ChillDateType::class,
                    ['input' => 'datetime_immutable', 'required' => false]
                );
                $rangeBuilder->add(
                    'to',
                    ChillDateType::class,
                    ['input' => 'datetime_immutable', 'required' => false]
                );

                $dateRangesBuilder->add($rangeBuilder);
            }

            $builder->add($dateRangesBuilder);
        }

        if ([] !== $helper->getSingleCheckbox()) {
            $singleCheckBoxBuilder = $builder->create('single_checkboxes', null, ['compound' => true]);

            foreach ($helper->getSingleCheckbox() as $name => ['label' => $label]) {
                $singleCheckBoxBuilder->add($name, CheckboxType::class, ['label' => $label, 'required' => false]);
            }

            $builder->add($singleCheckBoxBuilder);
        }

        if ([] !== $helper->getUserPickers()) {
            $userPickersBuilder = $builder->create('user_pickers', null, ['compound' => true]);

            foreach ($helper->getUserPickers() as $name => [
                'label' => $label, 'options' => $opts,
            ]) {
                $userPickersBuilder->add(
                    $name,
                    PickUserDynamicType::class,
                    [
                        'multiple' => true,
                        'label' => $label,
                        ...$opts,
                    ]
                );
            }

            $builder->add($userPickersBuilder);
        }
    }

    public static function buildCheckboxChoices(array $choices, array $trans = []): array
    {
        return \array_combine(
            \array_map(static function ($c, $t) {
                if (null !== $t) {
                    return $t;
                }

                return $c;
            }, $choices, $trans),
            $choices
        );
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        /** @var FilterOrderHelper $helper */
        $helper = $options['helper'];
        $view->vars['has_search_box'] = $helper->hasSearchBox();
        $view->vars['checkboxes'] = [];

        foreach ($helper->getCheckboxes() as $name => $c) {
            $view->vars['checkboxes'][$name] = [];
        }
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setRequired('helper')
            ->setAllowedTypes('helper', FilterOrderHelper::class);
    }
}
