<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Civility;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PickCivilityType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('label', 'Civility')
            ->setDefault(
                'choice_label',
                fn (Civility $civility): string => $this->translatableStringHelper->localize($civility->getName())
            )
            ->setDefault(
                'query_builder',
                static fn (EntityRepository $er): QueryBuilder => $er->createQueryBuilder('c')
                    ->where('c.active = true')
                    ->orderBy('c.order'),
            )
            ->setDefault('placeholder', 'choose civility')
            ->setDefault('class', Civility::class);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
