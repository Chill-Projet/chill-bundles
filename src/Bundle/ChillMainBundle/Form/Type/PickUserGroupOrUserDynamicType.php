<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Form\Type\DataTransformer\EntityToJsonTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Entity which picks a user **or** a user group.
 */
final class PickUserGroupOrUserDynamicType extends AbstractType
{
    public function __construct(
        private readonly DenormalizerInterface $denormalizer,
        private readonly SerializerInterface $serializer,
        private readonly NormalizerInterface $normalizer,
        private readonly Security $security,
    ) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new EntityToJsonTransformer($this->denormalizer, $this->serializer, $options['multiple'], 'user_group_or_user'));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'];
        $view->vars['types'] = ['user-group', 'user'];
        $view->vars['uniqid'] = uniqid('pick_usergroup_dyn');
        $view->vars['suggested'] = [];
        $view->vars['as_id'] = true === $options['as_id'] ? '1' : '0';
        $view->vars['submit_on_adding_new_entity'] = true === $options['submit_on_adding_new_entity'] ? '1' : '0';

        foreach ($options['suggested'] as $userGroup) {
            $view->vars['suggested'][] = $this->normalizer->normalize($userGroup, 'json', ['groups' => 'read']);
        }
        $user = $this->security->getUser();
        if ($user instanceof User) {
            if (true === $options['suggest_myself'] && !in_array($user, $options['suggested'], true)) {
                $view->vars['suggested'][] = $this->normalizer->normalize($user, 'json', ['groups' => 'read']);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('multiple', false)
            ->setAllowedTypes('multiple', ['bool'])
            ->setDefault('compound', false)
            ->setDefault('suggested', [])
            ->setDefault('suggest_myself', false)
            ->setAllowedTypes('suggest_myself', ['bool'])
            // if set to true, only the id will be set inside the content. The denormalization will not work.
            ->setDefault('as_id', false)
            ->setAllowedTypes('as_id', ['bool'])
            ->setDefault('submit_on_adding_new_entity', false)
            ->setAllowedTypes('submit_on_adding_new_entity', ['bool']);
    }

    public function getBlockPrefix()
    {
        return 'pick_entity_dynamic';
    }
}
