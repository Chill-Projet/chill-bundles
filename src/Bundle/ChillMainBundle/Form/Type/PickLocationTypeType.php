<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\LocationType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PickLocationTypeType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper) {}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'class' => LocationType::class,
                'choice_label' => fn (LocationType $type) => $this->translatableStringHelper->localize($type->getTitle()),
                'placeholder' => 'Pick a location type',
                'required' => false,
                'attr' => ['class' => 'select2'],
                'label' => 'Location type',
                'multiple' => false,
            ])
            ->setAllowedTypes('multiple', ['bool']);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
