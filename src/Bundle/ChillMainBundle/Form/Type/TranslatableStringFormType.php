<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

/*
 * TODO
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\Translator;

class TranslatableStringFormType extends AbstractType
{
    // The langauges availaible

    private readonly array $frameworkTranslatorFallback; // The langagues used for the translation

    public function __construct(private readonly array $availableLanguages, Translator $translator)
    {
        $this->frameworkTranslatorFallback = $translator->getFallbackLocales();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->availableLanguages as $lang) {
            $builder->add(
                $lang,
                TextType::class,
                ['required' => \in_array(
                    $lang,
                    $this->frameworkTranslatorFallback,
                    true
                )]
            );
        }
    }

    public function getBlockPrefix()
    {
        return 'translatable_string';
    }
}
