<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Repository\LocationRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PickUserLocationType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper, private readonly LocationRepository $locationRepository) {}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'class' => Location::class,
                'choices' => $this->locationRepository->findByPublicLocations(),
                'choice_label' => fn (Location $entity) => $entity->getName() ?
                    $entity->getName().' ('.$this->translatableStringHelper->localize($entity->getLocationType()->getTitle()).')' :
                    $this->translatableStringHelper->localize($entity->getLocationType()->getTitle()),
                'placeholder' => 'Pick a location',
                'required' => false,
                'attr' => ['class' => 'select2'],
                'label' => 'Current location',
                'multiple' => false,
            ])
            ->setAllowedTypes('multiple', ['bool']);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
