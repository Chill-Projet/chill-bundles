<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Repository\CenterRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ComposedGroupCenterType extends AbstractType
{
    public function __construct(private readonly CenterRepository $centerRepository) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $centers = $this->centerRepository->findActive();

        $builder->add('permissionsgroup', EntityType::class, [
            'class' => PermissionsGroup::class,
            'choice_label' => static fn (PermissionsGroup $group) => $group->getName(),
        ])->add('center', ChoiceType::class, [
            'choices' => $centers,
            'choice_label' => fn (Center $center) => $center->getName(),
            'multiple' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'composed_groupcenter';
    }
}
