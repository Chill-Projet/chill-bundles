<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Entity\GenderEnum;
use Chill\MainBundle\Entity\GenderIconEnum;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label', TranslatableStringFormType::class, [
                'required' => true,
            ])
            ->add('icon', EnumType::class, [
                'class' => GenderIconEnum::class,
                'choices' => GenderIconEnum::cases(),
                'expanded' => true,
                'multiple' => false,
                'mapped' => true,
                'choice_label' => fn (GenderIconEnum $enum) => '<i class="'.strtolower($enum->value).'"></i>',
                'choice_value' => fn (?GenderIconEnum $enum) => null !== $enum ? $enum->value : null,
                'label' => 'gender.admin.Select gender icon',
                'label_html' => true,
            ])
            ->add('genderTranslation', EnumType::class, [
                'class' => GenderEnum::class,
                'choice_label' => fn (GenderEnum $enum) => $enum->value,
                'label' => 'gender.admin.Select gender translation',
            ])
            ->add('active', ChoiceType::class, [
                'choices' => [
                    'Active' => true,
                    'Inactive' => false,
                ],
            ])
            ->add('order', NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Gender::class,
        ]);
    }
}
