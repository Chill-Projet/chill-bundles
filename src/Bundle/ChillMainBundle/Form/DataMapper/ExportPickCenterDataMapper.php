<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\DataMapper;

use Chill\MainBundle\Entity\Regroupment;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormInterface;

final readonly class ExportPickCenterDataMapper implements DataMapperInterface
{
    public function mapDataToForms($viewData, \Traversable $forms): void
    {
        if (null === $viewData) {
            return;
        }

        /** @var array<string, FormInterface> $form */
        $form = iterator_to_array($forms);

        $form['center']->setData($viewData);

        // NOTE: we do not map back the regroupments
    }

    public function mapFormsToData(\Traversable $forms, &$viewData): void
    {
        /** @var array<string, FormInterface> $forms */
        $forms = iterator_to_array($forms);

        $centers = [];

        foreach ($forms['center']->getData() as $center) {
            $centers[spl_object_hash($center)] = $center;
        }

        if (\array_key_exists('regroupment', $forms)) {
            /** @var Regroupment $regroupment */
            foreach ($forms['regroupment']->getData() as $regroupment) {
                foreach ($regroupment->getCenters() as $center) {
                    $centers[spl_object_hash($center)] = $center;
                }
            }
        }

        $viewData = array_values($centers);
    }
}
