<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\DataMapper;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;

class RollingDateDataMapper implements DataMapperInterface
{
    public function mapDataToForms($viewData, \Traversable $forms)
    {
        if (null === $viewData) {
            return;
        }

        if (!$viewData instanceof RollingDate) {
            throw new Exception\UnexpectedTypeException($viewData, RollingDate::class);
        }

        $forms = iterator_to_array($forms);

        $forms['roll']->setData($viewData->getRoll());
        $forms['fixedDate']->setData($viewData->getFixedDate());
    }

    public function mapFormsToData(\Traversable $forms, &$viewData): void
    {
        $forms = iterator_to_array($forms);

        if (null === $forms['roll']->getData()) {
            $viewData = null;
        } else {
            $viewData = new RollingDate(
                $forms['roll']->getData(),
                $forms['fixedDate']->getData()
            );
        }
    }
}
