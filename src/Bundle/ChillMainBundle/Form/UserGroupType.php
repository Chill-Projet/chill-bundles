<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TranslatableStringFormType::class, [
                'label' => 'user_group.Label',
                'required' => true,
            ])
            ->add('active')
            ->add('backgroundColor', ColorType::class, [
                'label' => 'user_group.BackgroundColor',
            ])
            ->add('foregroundColor', ColorType::class, [
                'label' => 'user_group.ForegroundColor',
            ])
            ->add('email', EmailType::class, [
                'label' => 'user_group.Email',
                'help' => 'user_group.EmailHelp',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('excludeKey', TextType::class, [
                'label' => 'user_group.ExcludeKey',
                'help' => 'user_group.ExcludeKeyHelp',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('users', PickUserDynamicType::class, [
                'label' => 'user_group.Users',
                'multiple' => true,
                'required' => false,
                'empty_data' => [],
            ])
            ->add('adminUsers', PickUserDynamicType::class, [
                'label' => 'user_group.adminUsers',
                'multiple' => true,
                'required' => false,
                'empty_data' => [],
                'help' => 'user_group.adminUsersHelp',
            ])
        ;
    }
}
