<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Listing;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class FilterOrderHelperFactory implements FilterOrderHelperFactoryInterface
{
    public function __construct(private readonly FormFactoryInterface $formFactory, private readonly RequestStack $requestStack) {}

    public function create(string $context, ?array $options = []): FilterOrderHelperBuilder
    {
        return new FilterOrderHelperBuilder($this->formFactory, $this->requestStack);
    }
}
