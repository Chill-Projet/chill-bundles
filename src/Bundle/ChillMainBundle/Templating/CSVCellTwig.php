<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig filter to transform a string in a safer way to be the content of a csv
 * cell.
 *
 * This filter replace the char " by ""
 */
class CSVCellTwig extends AbstractExtension
{
    /**
     * Replace into a string the char " by "".
     *
     * @param string $content the input string
     *
     * @return string the safe string
     */
    public function csvCellFilter($content)
    {
        return str_replace('"', '""', $content);
    }

    /**
     * Returns a list of filters to add to the existing list.
     *
     * (non-PHPdoc)
     *
     * @see Twig_Extension::getFilters()
     */
    public function getFilters()
    {
        return [
            new TwigFilter(
                'csv_cell',
                $this->csvCellFilter(...),
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * Returns the name of the extension.
     *
     * @return The name of the extension
     */
    public function getName()
    {
        return 'chill_main_csv_cell';
    }
}
