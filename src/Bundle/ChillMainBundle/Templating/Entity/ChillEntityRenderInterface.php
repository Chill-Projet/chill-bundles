<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

/**
 * Interface to implement which will render an entity in template on a custom
 * manner.
 *
 * @template T of object
 */
interface ChillEntityRenderInterface
{
    /**
     * Return the entity in a box.
     *
     * Example: return a person inside a box:
     *
     * ```html
     * <span class="chill-entity">
     *  <span class="chill-entity__first-name">Roger</span>
     *  <span class="chill-entity__last-name">Dupont</span>
     * </span>
     * ```
     *
     * @param T|null $entity
     *
     * @phpstan-pure
     */
    public function renderBox(mixed $entity, array $options): string;

    /**
     * Return the entity as a string.
     *
     * Example: returning the name of a person.
     *
     * @param T|null $entity
     *
     * @phpstan-pure
     */
    public function renderString(mixed $entity, array $options): string;

    /**
     * Return true if the class support this object for the given options.
     *
     * @phpstan-pure
     */
    public function supports(object $entity, array $options): bool;
}
