<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

interface ChillEntityRenderManagerInterface
{
    public function renderBox(?object $entity, array $options = []): string;

    public function renderString(?object $entity, array $options = []): string;
}
