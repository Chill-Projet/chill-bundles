<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Chill\MainBundle\Entity\NewsItem;

/**
 * @implements ChillEntityRenderInterface<NewsItem>
 */
final readonly class NewsItemRender implements ChillEntityRenderInterface
{
    public function renderBox($entity, array $options): string
    {
        return '';
    }

    public function renderString($entity, array $options): string
    {
        return $entity->getTitle();
    }

    public function supports($newsItem, array $options): bool
    {
        return $newsItem instanceof NewsItem;
    }
}
