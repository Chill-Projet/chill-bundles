<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

final readonly class ChillEntityRenderManager implements ChillEntityRenderManagerInterface
{
    private ChillEntityRender $defaultRender;

    public function __construct(/**
     * @var iterable<ChillEntityRenderInterface>
     */
        private iterable $renders,
    ) {
        $this->defaultRender = new ChillEntityRender();
    }

    public function renderBox($entity, array $options = []): string
    {
        if (null === $entity) {
            return '';
        }

        return $this->getRender($entity, $options)
            ->renderBox($entity, $options);
    }

    public function renderString($entity, array $options = []): string
    {
        if (null === $entity) {
            return '';
        }

        return $this->getRender($entity, $options)
            ->renderString($entity, $options);
    }

    private function getRender($entity, $options): ChillEntityRenderInterface
    {
        foreach ($this->renders as $render) {
            if ($render->supports($entity, $options)) {
                return $render;
            }
        }

        return $this->defaultRender;
    }
}
