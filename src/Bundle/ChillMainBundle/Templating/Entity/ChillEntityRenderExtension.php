<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class ChillEntityRenderExtension.
 */
class ChillEntityRenderExtension extends AbstractExtension
{
    public function __construct(private readonly ChillEntityRenderManagerInterface $renderManager) {}

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('chill_entity_render_string', $this->renderString(...), [
                'is_safe' => ['html'],
            ]),
            new TwigFilter('chill_entity_render_box', $this->renderBox(...), [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderBox(?object $entity, array $options = []): string
    {
        return $this->renderManager->renderBox($entity, $options);
    }

    public function renderString(?object $entity, array $options = []): string
    {
        return $this->renderManager->renderString($entity, $options);
    }
}
