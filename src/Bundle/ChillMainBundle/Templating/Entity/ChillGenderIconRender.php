<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Chill\MainBundle\Entity\GenderIconEnum;

/**
 * @implements ChillEntityRenderInterface<GenderIconEnum>
 */
final readonly class ChillGenderIconRender implements ChillEntityRenderInterface
{
    public function renderBox($icon, array $options): string
    {
        return '<i class="'.htmlspecialchars($icon->value, ENT_QUOTES, 'UTF-8').'"></i>';
    }

    public function renderString($icon, array $options): string
    {
        return $icon->value;
    }

    public function supports($icon, array $options): bool
    {
        return $icon instanceof GenderIconEnum;
    }
}
