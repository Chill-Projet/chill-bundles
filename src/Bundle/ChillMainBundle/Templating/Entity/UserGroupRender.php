<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Entity;

use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Twig\Environment;

final readonly class UserGroupRender implements UserGroupRenderInterface
{
    public function __construct(private TranslatableStringHelperInterface $translatableStringHelper, private Environment $environment) {}

    public function renderBox($entity, array $options): string
    {
        /* @var $entity UserGroup */
        return $this->environment->render('@ChillMain/Entity/user_group.html.twig', ['user_group' => $entity]);
    }

    public function renderString($entity, array $options): string
    {
        /* @var $entity UserGroup */
        return (string) $this->translatableStringHelper->localize($entity->getLabel());
    }

    public function supports(object $entity, array $options): bool
    {
        return $entity instanceof UserGroup;
    }
}
