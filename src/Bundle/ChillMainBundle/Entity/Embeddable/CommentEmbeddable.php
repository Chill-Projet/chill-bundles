<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class CommentEmbeddable
{
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: true)]
    private ?string $comment = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $date = null;

    /**
     * Embeddable does not support associations.
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: true)]
    private ?int $userId = null;

    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function isEmpty()
    {
        return null === $this->getComment() || '' === $this->getComment();
    }

    public function setComment(?string $comment)
    {
        $this->comment = $comment;
    }

    public function setDate(?\DateTime $date)
    {
        $this->date = $date;
    }

    public function setUserId(?int $userId)
    {
        $this->userId = $userId;
    }
}
