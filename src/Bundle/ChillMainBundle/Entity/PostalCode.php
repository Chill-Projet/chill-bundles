<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\Point;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * PostalCode.
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'chill_main_postal_code')]
#[ORM\Index(name: 'search_name_code', columns: ['code', 'label'])]
#[ORM\Index(name: 'search_by_reference_code', columns: ['code', 'refpostalcodeid'])]
#[ORM\UniqueConstraint(name: 'postal_code_import_unicity', columns: ['code', 'refpostalcodeid', 'postalcodesource'], options: ['where' => 'refpostalcodeid is not null'])]
#[ORM\UniqueConstraint(name: 'postal_code_import_unicity', columns: ['code', 'refpostalcodeid', 'postalcodesource'], options: ['where' => 'refpostalcodeid is not null'])] // ,
#[ORM\Index(name: 'search_name_code', columns: ['code', 'label'])] // ,
#[ORM\Index(name: 'search_by_reference_code', columns: ['code', 'refpostalcodeid'])]
class PostalCode implements TrackUpdateInterface, TrackCreationInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * This is an internal column which is populated by database.
     *
     * This column will ease the search operations
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, options: ['default' => ''])]
    private string $canonical = '';

    #[Groups(['read'])]
    #[ORM\Column(type: 'point', nullable: true)]
    private ?Point $center = null;

    #[Groups(['write', 'read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 100)]
    private ?string $code = null;

    #[Groups(['write', 'read'])]
    #[ORM\ManyToOne(targetEntity: Country::class)]
    private ?Country $country = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $deletedAt = null;

    #[Groups(['write', 'read'])]
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[Groups(['write', 'read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, name: 'label')]
    private ?string $name = null;

    #[Groups(['write', 'read'])]
    #[ORM\Column(name: 'origin', type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: true)]
    private int $origin = 0;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $postalCodeSource = null;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $refPostalCodeId = null;

    public function getCenter(): ?Point
    {
        return $this->center;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get country.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get origin.
     *
     * @return int
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    public function getPostalCodeSource(): ?string
    {
        return $this->postalCodeSource;
    }

    public function getRefPostalCodeId(): ?string
    {
        return $this->refPostalCodeId;
    }

    public function setCenter(?Point $center): self
    {
        $this->center = $center;

        return $this;
    }

    /**
     * Set code.
     *
     * @return PostalCode
     */
    public function setCode(?string $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set country.
     *
     * @return PostalCode
     */
    public function setCountry(?Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Set name.
     *
     * @return PostalCode
     */
    public function setName(?string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set origin.
     *
     * @return PostalCode
     */
    public function setOrigin(int $origin)
    {
        $this->origin = $origin;

        return $this;
    }

    public function setPostalCodeSource(?string $postalCodeSource): self
    {
        $this->postalCodeSource = $postalCodeSource;

        return $this;
    }

    public function setRefPostalCodeId(?string $refPostalCodeId): self
    {
        $this->refPostalCodeId = $refPostalCodeId;

        return $this;
    }
}
