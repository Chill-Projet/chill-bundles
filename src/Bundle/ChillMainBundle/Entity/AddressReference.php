<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\Point;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'chill_main_address_reference')]
#[ORM\Index(name: 'address_refid', columns: ['refId'])]
#[ORM\UniqueConstraint(name: 'chill_main_address_reference_unicity', columns: ['refId', 'source'])]
class AddressReference
{
    /**
     * This is an internal column which is populated by database.
     *
     * This column will ease the search operations
     */
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, options: ['default' => ''])]
    private string $addressCanonical = '';

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[Groups(['read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $municipalityCode = '';

    /**
     * A geospatial field storing the coordinates of the Address.
     */
    #[Groups(['read'])]
    #[ORM\Column(type: 'point')]
    private ?Point $point = null;

    #[Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: PostalCode::class)]
    private ?PostalCode $postcode = null;

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $refId = '';

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $source = '';

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $street = '';

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, nullable: false, options: ['default' => ''])]
    private string $streetNumber = '';

    #[Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMunicipalityCode(): string
    {
        return $this->municipalityCode;
    }

    public function getPoint(): ?Point
    {
        return $this->point;
    }

    /**
     * Get postcode.
     */
    public function getPostcode(): ?PostalCode
    {
        return $this->postcode;
    }

    public function getRefId(): string
    {
        return $this->refId;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function hasPoint(): bool
    {
        return null !== $this->getPoint();
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function setMunicipalityCode(?string $municipalityCode): self
    {
        $this->municipalityCode = (string) $municipalityCode;

        return $this;
    }

    public function setPoint(?Point $point): self
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Set postcode.
     *
     * @return Address
     */
    public function setPostcode(?PostalCode $postcode = null)
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function setRefId(string $refId): self
    {
        $this->refId = $refId;

        return $this;
    }

    public function setSource(?string $source): self
    {
        $this->source = (string) $source;

        return $this;
    }

    public function setStreet(?string $street): self
    {
        $this->street = (string) $street;

        return $this;
    }

    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = (string) $streetNumber;

        return $this;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
