<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\GeographicalUnit;

use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * Simple GeographialUnit Data Transfer Object.
 *
 * This allow to get access to id, unitName, unitRefId, and layer's id
 */
class SimpleGeographicalUnitDTO
{
    public function __construct(
        /**
         * @readonly
         *
         * @psalm-readonly
         */
        #[Serializer\Groups(['read'])]
        public int $id,
        /**
         * @readonly
         *
         * @psalm-readonly
         */
        #[Serializer\Groups(['read'])]
        public string $unitName,
        /**
         * @readonly
         *
         * @psalm-readonly
         */
        #[Serializer\Groups(['read'])]
        public string $unitRefId,
        /**
         * @readonly
         *
         * @psalm-readonly
         */
        #[Serializer\Groups(['read'])]
        public int $layerId,
    ) {}
}
