<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Repository\LocationTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

#[DiscriminatorMap(typeProperty: 'type', mapping: ['location-type' => LocationType::class])]
#[UniqueEntity(['defaultFor'])]
#[ORM\Entity(repositoryClass: LocationTypeRepository::class)]
#[ORM\Table(name: 'chill_main_location_type')]
class LocationType
{
    final public const DEFAULT_FOR_3PARTY = 'thirdparty';

    final public const DEFAULT_FOR_PERSON = 'person';

    final public const STATUS_NEVER = 'never';

    final public const STATUS_OPTIONAL = 'optional';

    final public const STATUS_REQUIRED = 'required';

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $active = true;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 32, options: ['default' => 'optional'])]
    private string $addressRequired = self::STATUS_OPTIONAL;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $availableForUsers = true;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 32, options: ['default' => 'optional'])]
    private string $contactData = self::STATUS_OPTIONAL;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, nullable: true, length: 32, unique: true)]
    private ?string $defaultFor = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $editableByUsers = true;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $title = [];

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getAddressRequired(): ?string
    {
        return $this->addressRequired;
    }

    public function getAvailableForUsers(): ?bool
    {
        return $this->availableForUsers;
    }

    public function getContactData(): ?string
    {
        return $this->contactData;
    }

    public function getDefaultFor(): ?string
    {
        return $this->defaultFor;
    }

    public function getEditableByUsers(): ?bool
    {
        return $this->editableByUsers;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setAddressRequired(string $addressRequired): self
    {
        $this->addressRequired = $addressRequired;

        return $this;
    }

    public function setAvailableForUsers(bool $availableForUsers): self
    {
        $this->availableForUsers = $availableForUsers;

        return $this;
    }

    public function setContactData(string $contactData): self
    {
        $this->contactData = $contactData;

        return $this;
    }

    public function setDefaultFor(?string $defaultFor): self
    {
        $this->defaultFor = $defaultFor;

        return $this;
    }

    public function setEditableByUsers(bool $editableByUsers): self
    {
        $this->editableByUsers = $editableByUsers;

        return $this;
    }

    public function setTitle(array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
