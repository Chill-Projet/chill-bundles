<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['chill_main_civility' => Civility::class])]
#[ORM\Entity]
#[ORM\Table(name: 'chill_main_civility')]
class Civility
{
    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $abbreviation = [];

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $active = true;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $name = [];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::FLOAT, name: 'ordering', nullable: true, options: ['default' => '0.0'])]
    private float $order = 0;

    public function getAbbreviation(): array
    {
        return $this->abbreviation;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function getOrder(): ?float
    {
        return $this->order;
    }

    public function setAbbreviation(array $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setOrder(float $order): self
    {
        $this->order = $order;

        return $this;
    }
}
