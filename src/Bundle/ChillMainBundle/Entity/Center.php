<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[ORM\Entity]
#[ORM\Table(name: 'centers')]
class Center implements HasCenterInterface, \Stringable
{
    /**
     * @var Collection<int, GroupCenter>
     */
    #[ORM\OneToMany(mappedBy: 'center', targetEntity: GroupCenter::class)]
    private Collection $groupCenters;

    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private string $name = '';

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $isActive = true;

    /**
     * @var Collection<int, Regroupment>
     */
    #[ORM\ManyToMany(targetEntity: Regroupment::class, mappedBy: 'centers')]
    private Collection $regroupments;

    /**
     * Center constructor.
     */
    public function __construct()
    {
        $this->groupCenters = new ArrayCollection();
        $this->regroupments = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return $this
     */
    public function addGroupCenter(GroupCenter $groupCenter)
    {
        $this->groupCenters->add($groupCenter);

        return $this;
    }

    /**
     * @return $this|Center
     */
    public function getCenter()
    {
        return $this;
    }

    public function getGroupCenters(): ArrayCollection|Collection
    {
        return $this->groupCenters;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Collection<Regroupment>
     */
    public function getRegroupments(): Collection
    {
        return $this->regroupments;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
