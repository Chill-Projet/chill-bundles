<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'regroupment')]
class Regroupment
{
    /**
     * @var Collection<int, Center>
     */
    #[ORM\ManyToMany(targetEntity: Center::class, inversedBy: 'regroupments')]
    #[ORM\Id]
    private Collection $centers;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $isActive = true;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::TEXT, options: ['default' => ''], nullable: false)]
    private string $name = '';

    public function __construct()
    {
        $this->centers = new ArrayCollection();
    }

    public function addCenter(Center $center): self
    {
        if (!$this->centers->contains($center)) {
            $this->centers->add($center);
            $center->getRegroupments()->add($this);
        }

        return $this;
    }

    public function removeCenter(Center $center): self
    {
        if ($this->centers->contains($center)) {
            $this->centers->removeElement($center);
            $center->getRegroupments()->removeElement($this);
        }

        return $this;
    }

    public function getCenters(): Collection
    {
        return $this->centers;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setCenters(?Collection $centers): self
    {
        $this->centers = $centers;

        return $this;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
