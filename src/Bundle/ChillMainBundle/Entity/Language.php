<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * Language.
 */
#[ORM\Entity]
#[ORM\Cache(usage: 'READ_ONLY', region: 'language_cache_region')]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'language')]
class Language
{
    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Id]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING)]
    private ?string $id = null;

    /**
     * @var string array
     */
    #[Serializer\Groups(['docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    private array $name = [];

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set id.
     *
     * @return Language
     */
    public function setId(?string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set name.
     *
     * @param string array $name
     *
     * @return Language
     */
    public function setName(array $name)
    {
        $this->name = $name;

        return $this;
    }
}
