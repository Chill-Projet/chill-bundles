<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

enum GenderIconEnum: string
{
    case MALE = 'bi bi-gender-male';
    case FEMALE = 'bi bi-gender-female';
    case NEUTRAL = 'bi bi-gender-neuter';
    case AMBIGUOUS = 'bi bi-gender-ambiguous';
    case TRANS = 'bi bi-gender-trans';
    case UNKNOWN = 'bi bi-question';
}
