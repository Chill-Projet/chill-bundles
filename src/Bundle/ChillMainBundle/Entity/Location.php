<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Repository\LocationRepository;
use Chill\MainBundle\Validation\Constraint\PhonenumberConstraint;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

#[DiscriminatorMap(typeProperty: 'type', mapping: ['location' => Location::class])]
#[ORM\Entity(repositoryClass: LocationRepository::class)]
#[ORM\Table(name: 'chill_main_location')]
class Location implements TrackCreationInterface, TrackUpdateInterface
{
    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => true])]
    private bool $active = true;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: Address::class, cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Address $address = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $availableForUsers = false;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[Serializer\Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $createdBy = null;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $email = null;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\ManyToOne(targetEntity: LocationType::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?LocationType $locationType = null;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255, nullable: true)]
    private ?string $name = null;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[PhonenumberConstraint(type: 'any')]
    private ?PhoneNumber $phonenumber1 = null;

    #[Serializer\Groups(['read', 'write', 'docgen:read'])]
    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[PhonenumberConstraint(type: 'any')]
    private ?PhoneNumber $phonenumber2 = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[Serializer\Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $updatedBy = null;

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getAvailableForUsers(): ?bool
    {
        return $this->availableForUsers;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocationType(): ?LocationType
    {
        return $this->locationType;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhonenumber1(): ?PhoneNumber
    {
        return $this->phonenumber1;
    }

    public function getPhonenumber2(): ?PhoneNumber
    {
        return $this->phonenumber2;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function hasAddress(): bool
    {
        return null !== $this->getAddress();
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setAvailableForUsers(bool $availableForUsers): self
    {
        $this->availableForUsers = $availableForUsers;

        return $this;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setLocationType(?LocationType $locationType): self
    {
        $this->locationType = $locationType;

        return $this;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setPhonenumber1(?PhoneNumber $phonenumber1): self
    {
        $this->phonenumber1 = $phonenumber1;

        return $this;
    }

    public function setPhonenumber2(?PhoneNumber $phonenumber2): self
    {
        $this->phonenumber2 = $phonenumber2;

        return $this;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
