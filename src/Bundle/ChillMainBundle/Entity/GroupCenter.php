<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'acl_cache_region')]
#[ORM\Table(name: 'group_centers')]
class GroupCenter
{
    #[ORM\ManyToOne(targetEntity: Center::class, inversedBy: 'groupCenters')]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    private ?Center $center = null;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: PermissionsGroup::class, inversedBy: 'groupCenters')]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    private ?PermissionsGroup $permissionsGroup = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'groupCenters')]
    private Collection $users;

    /**
     * GroupCenter constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getCenter(): ?Center
    {
        return $this->center;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPermissionsGroup(): ?PermissionsGroup
    {
        return $this->permissionsGroup;
    }

    /**
     * @return Collection<User::class>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setCenter(Center $center): self
    {
        $this->center = $center;

        return $this;
    }

    public function setPermissionsGroup(PermissionsGroup $permissionsGroup): self
    {
        $this->permissionsGroup = $permissionsGroup;

        return $this;
    }
}
