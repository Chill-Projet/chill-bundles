<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'acl_cache_region')]
#[ORM\Table(name: 'role_scopes')]
class RoleScope
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    /**
     * @var Collection<int, PermissionsGroup>
     */
    #[ORM\ManyToMany(targetEntity: PermissionsGroup::class, mappedBy: 'roleScopes')]
    private Collection $permissionsGroups;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private ?string $role = null;

    #[ORM\ManyToOne(targetEntity: Scope::class, inversedBy: 'roleScopes')]
    #[ORM\JoinColumn(nullable: true, name: 'scope_id')]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    private ?Scope $scope = null;

    public function __construct()
    {
        $this->permissionsGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function getScope(): ?Scope
    {
        return $this->scope;
    }

    public function setRole(?string $role = null): self
    {
        $this->role = $role;

        return $this;
    }

    public function setScope(?Scope $scope = null): self
    {
        $this->scope = $scope;

        return $this;
    }
}
