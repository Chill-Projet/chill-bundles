<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: 'chill_main_workflow_entity_attachment')]
#[ORM\UniqueConstraint(name: 'unique_generic_doc_by_workflow', columns: ['relatedGenericDocKey', 'relatedGenericDocIdentifiers', 'entityworkflow_id'])]
class EntityWorkflowAttachment implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;
    use TrackUpdateTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id = null;

    public function __construct(
        #[ORM\Column(name: 'relatedGenericDocKey', type: Types::STRING, length: 255, nullable: false)]
        private string $relatedGenericDocKey,
        #[ORM\Column(name: 'relatedGenericDocIdentifiers', type: Types::JSON, nullable: false, options: ['jsonb' => true])]
        private array $relatedGenericDocIdentifiers,
        #[ORM\ManyToOne(targetEntity: EntityWorkflow::class, inversedBy: 'attachments')]
        #[ORM\JoinColumn(nullable: false, name: 'entityworkflow_id')]
        private EntityWorkflow $entityWorkflow,

        /**
         * Stored object related to the generic doc.
         *
         * This is a story to keep track more easily to stored object
         */
        #[ORM\ManyToOne(targetEntity: StoredObject::class)]
        #[ORM\JoinColumn(nullable: false, name: 'storedobject_id')]
        private StoredObject $proxyStoredObject,
    ) {
        $this->entityWorkflow->addAttachment($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntityWorkflow(): EntityWorkflow
    {
        return $this->entityWorkflow;
    }

    public function getRelatedGenericDocIdentifiers(): array
    {
        return $this->relatedGenericDocIdentifiers;
    }

    public function getRelatedGenericDocKey(): string
    {
        return $this->relatedGenericDocKey;
    }

    public function getProxyStoredObject(): StoredObject
    {
        return $this->proxyStoredObject;
    }
}
