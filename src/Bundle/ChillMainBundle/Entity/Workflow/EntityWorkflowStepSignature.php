<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'chill_main_workflow_entity_step_signature')]
class EntityWorkflowStepSignature implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;
    use TrackUpdateTrait;

    #[ORM\Id]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, unique: true)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $userSigner = null;

    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Person $personSigner = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::STRING, length: 50, nullable: false, enumType: EntityWorkflowSignatureStateEnum::class)]
    private EntityWorkflowSignatureStateEnum $state = EntityWorkflowSignatureStateEnum::PENDING;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::DATETIMETZ_IMMUTABLE, nullable: true, options: ['default' => null])]
    private ?\DateTimeImmutable $stateDate = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, nullable: false, options: ['default' => '[]'])]
    private array $signatureMetadata = [];

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER, nullable: true, options: ['default' => null])]
    private ?int $zoneSignatureIndex = null;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: EntityWorkflowStep::class, inversedBy: 'signatures')]
        #[ORM\JoinColumn(nullable: false)]
        private EntityWorkflowStep $step,
        User|Person $signer,
    ) {
        $this->step->addSignature($this);
        $this->setSigner($signer);
    }

    private function setSigner(User|Person $signer): void
    {
        if ($signer instanceof User) {
            $this->userSigner = $signer;
        } else {
            $this->personSigner = $signer;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStep(): EntityWorkflowStep
    {
        return $this->step;
    }

    public function getSigner(): User|Person
    {
        if (null !== $this->userSigner) {
            return $this->userSigner;
        }

        return $this->personSigner;
    }

    public function getSignatureMetadata(): array
    {
        return $this->signatureMetadata;
    }

    public function setSignatureMetadata(array $signatureMetadata): EntityWorkflowStepSignature
    {
        $this->signatureMetadata = $signatureMetadata;

        return $this;
    }

    public function getState(): EntityWorkflowSignatureStateEnum
    {
        return $this->state;
    }

    /**
     * @return $this
     *
     * @internal You should not use this method directly, use @see{Chill\MainBundle\Workflow\SignatureStepStateChanger} instead
     */
    public function setState(EntityWorkflowSignatureStateEnum $state): EntityWorkflowStepSignature
    {
        $this->state = $state;

        return $this;
    }

    public function getStateDate(): ?\DateTimeImmutable
    {
        return $this->stateDate;
    }

    /**
     * @return $this
     *
     * @internal You should not use this method directly, use @see{Chill\MainBundle\Workflow\SignatureStepStateChanger} instead
     */
    public function setStateDate(?\DateTimeImmutable $stateDate): EntityWorkflowStepSignature
    {
        $this->stateDate = $stateDate;

        return $this;
    }

    public function getZoneSignatureIndex(): ?int
    {
        return $this->zoneSignatureIndex;
    }

    /**
     * @return $this
     *
     * @internal You should not use this method directly, use @see{Chill\MainBundle\Workflow\SignatureStepStateChanger} instead
     */
    public function setZoneSignatureIndex(?int $zoneSignatureIndex): EntityWorkflowStepSignature
    {
        $this->zoneSignatureIndex = $zoneSignatureIndex;

        return $this;
    }

    public function isSigned(): bool
    {
        return EntityWorkflowSignatureStateEnum::SIGNED == $this->getState();
    }

    public function isPending(): bool
    {
        return EntityWorkflowSignatureStateEnum::PENDING == $this->getState();
    }

    public function isCanceled(): bool
    {
        return EntityWorkflowSignatureStateEnum::CANCELED === $this->getState();
    }

    public function isRejected(): bool
    {
        return EntityWorkflowSignatureStateEnum::REJECTED === $this->getState();
    }

    /**
     * Checks whether all signatures associated with a given workflow step are not pending.
     *
     * Iterates over each signature in the provided workflow step, and returns false if any signature
     * is found to be pending. If all signatures are not pending, returns true.
     *
     * @param EntityWorkflowStep $step the workflow step whose signatures are to be checked
     *
     * @return bool true if all signatures are not pending, false otherwise
     */
    public static function isAllSignatureNotPendingForStep(EntityWorkflowStep $step): bool
    {
        foreach ($step->getSignatures() as $signature) {
            if ($signature->isPending()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return 'person'|'user'
     */
    public function getSignerKind(): string
    {
        if ($this->personSigner instanceof Person) {
            return 'person';
        }

        return 'user';
    }
}
