<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table('chill_main_workflow_entity_step_hold')]
#[ORM\UniqueConstraint(name: 'chill_main_workflow_hold_unique_idx', columns: ['step_id', 'byUser_id'])]
class EntityWorkflowStepHold implements TrackCreationInterface
{
    use TrackCreationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private ?int $id = null;

    public function __construct(#[ORM\ManyToOne(targetEntity: EntityWorkflowStep::class)]
        #[ORM\JoinColumn(nullable: false)]
        private EntityWorkflowStep $step, #[ORM\ManyToOne(targetEntity: User::class)]
        #[ORM\JoinColumn(nullable: false)]
        private User $byUser)
    {
        $step->addOnHold($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStep(): EntityWorkflowStep
    {
        return $this->step;
    }

    public function getByUser(): User
    {
        return $this->byUser;
    }
}
