<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Register the viewing action from an external destinee.
 */
#[ORM\Entity(readOnly: true)]
#[ORM\Table(name: 'chill_main_workflow_entity_send_views')]
class EntityWorkflowSendView
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id = null;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: EntityWorkflowSend::class, inversedBy: 'views')]
        #[ORM\JoinColumn(nullable: false)]
        private EntityWorkflowSend $send,
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
        private \DateTimeInterface $viewAt,
        #[ORM\Column(type: Types::TEXT)]
        private string $remoteIp = '',
    ) {
        $this->send->addView($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRemoteIp(): string
    {
        return $this->remoteIp;
    }

    public function getSend(): EntityWorkflowSend
    {
        return $this->send;
    }

    public function getViewAt(): \DateTimeInterface
    {
        return $this->viewAt;
    }
}
