<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity\Workflow;

enum EntityWorkflowSignatureStateEnum: string
{
    case PENDING = 'pending';
    case SIGNED = 'signed';
    case REJECTED = 'rejected';
    case CANCELED = 'canceled';
}
