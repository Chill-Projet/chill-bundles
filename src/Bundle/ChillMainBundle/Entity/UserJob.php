<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['user_job' => UserJob::class])]
#[ORM\Entity]
#[ORM\Table('chill_main_user_job')]
class UserJob
{
    #[ORM\Column(name: 'active', type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    protected bool $active = true;

    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    protected ?int $id = null;

    /**
     * @var array<string, string>
     */
    #[Serializer\Groups(['read', 'docgen:read'])]
    #[ORM\Column(name: 'label', type: \Doctrine\DBAL\Types\Types::JSON)]
    #[Serializer\Context(['is-translatable' => true], groups: ['docgen:read'])]
    protected array $label = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return array|string[]
     */
    public function getLabel(): array
    {
        return $this->label;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): UserJob
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @param array|string[] $label
     */
    public function setLabel(array $label): UserJob
    {
        $this->label = $label;

        return $this;
    }
}
