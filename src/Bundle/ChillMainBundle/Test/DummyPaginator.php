<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Test;

use Chill\MainBundle\Pagination\Paginator;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Pagination\PaginatorFactoryInterface;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DummyPaginator implements PaginatorFactoryInterface
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly string $route,
        private readonly array $routeParameters = [],
    ) {}

    public function create(int $totalItems, ?string $route = null, ?array $routeParameters = null): PaginatorInterface
    {
        return new Paginator(
            $totalItems,
            $totalItems,
            1,
            $this->route,
            $this->routeParameters,
            $this->urlGenerator,
            PaginatorFactory::DEFAULT_CURRENT_PAGE_KEY,
            PaginatorFactory::DEFAULT_ITEM_PER_NUMBER_KEY
        );
    }

    public function getCurrentItemsPerPage(): int
    {
        return 20;
    }

    public function getCurrentPageFirstItemNumber(): int
    {
        return 1;
    }

    public function getCurrentPageNumber(): int
    {
        return 1;
    }
}
