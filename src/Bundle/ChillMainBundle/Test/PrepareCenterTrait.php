<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Test;

use Chill\MainBundle\Entity\Center;

/**
 * A trait to prepare center.
 *
 * **Usage :** You must set up trait with `setUpTrait` before use
 * and use tearDownTrait after usage.
 *
 * @codeCoverageIgnore
 */
trait PrepareCenterTrait
{
    private $centerProphet;

    /**
     * prepare a mocked center, with and id and name given.
     */
    protected static function prepareCenter(int $id, string $name): Center
    {
        $center = new Center();
        $center->setName($name);

        $reflectionClass = new \ReflectionClass($center);
        $reflectionId = $reflectionClass->getProperty('id');
        $reflectionId->setAccessible(true);
        $reflectionId->setValue($center, $id);

        return $center;
    }
}
