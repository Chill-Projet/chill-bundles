<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\Workflow;

class CancelStaleWorkflowMessage
{
    public function __construct(public int $workflowId) {}

    public function getWorkflowId(): int
    {
        return $this->workflowId;
    }
}
