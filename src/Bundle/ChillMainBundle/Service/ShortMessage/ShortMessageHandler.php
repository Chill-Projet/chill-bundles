<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\ShortMessage;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;

/**
 * @AsMessageHandler
 */
class ShortMessageHandler implements MessageHandlerInterface
{
    private readonly PhoneNumberUtil $phoneNumberUtil;

    public function __construct(private readonly TexterInterface $texter)
    {
        $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    }

    public function __invoke(ShortMessage $message): void
    {
        trigger_deprecation('Chill-project/chill-bundles', '3.7.0', 'Send message using Notifier component');

        $this->texter->send(
            new SmsMessage(
                $this->phoneNumberUtil->format($message->getPhoneNumber(), PhoneNumberFormat::E164),
                $message->getContent(),
            ),
        );
    }
}
