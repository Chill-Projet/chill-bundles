<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\ShortMessage;

use libphonenumber\PhoneNumber;

trigger_deprecation('chill-project/chill-bundles', '3.7', 'Short Messages are deprecated, use SmsMessage and Notifier component instead');

class ShortMessage
{
    final public const PRIORITY_LOW = 'low';

    final public const PRIORITY_MEDIUM = 'medium';

    public function __construct(private string $content, private PhoneNumber $phoneNumber, private string $priority = 'low') {}

    public function getContent(): string
    {
        return $this->content;
    }

    public function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function getPriority(): string
    {
        return $this->priority;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function setPhoneNumber(PhoneNumber $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
