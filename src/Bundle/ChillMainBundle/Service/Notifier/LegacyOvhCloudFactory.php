<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\Notifier;

use Symfony\Component\Notifier\Exception\UnsupportedSchemeException;
use Symfony\Component\Notifier\Transport\AbstractTransportFactory;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Component\Notifier\Transport\TransportInterface;

/**
 * This is a legacy ovh cloud provider, to provide the regular OvhCloudTransporter from the previous configuration.
 *
 * This is only for transition purpose from the previous ovh dsn, which was existing in chill.
 */
class LegacyOvhCloudFactory extends AbstractTransportFactory
{
    protected function getSupportedSchemes(): array
    {
        return ['ovh'];
    }

    public function create(Dsn $dsn): TransportInterface
    {
        $scheme = $dsn->getScheme();

        if ('ovh' !== $scheme) {
            throw new UnsupportedSchemeException($dsn, 'ovh', $this->getSupportedSchemes());
        }

        if (!class_exists($class = '\Symfony\Component\Notifier\Bridge\OvhCloud\OvhCloudTransport')) {
            throw new \RuntimeException(sprintf('The class %s is missing, please add the dependency with "composer require symfony/ovh-cloud-notifier".', $class));
        }

        $applicationKey = $this->getUser($dsn);
        $applicationSecret = $this->getPassword($dsn);
        $consumerKey = $dsn->getRequiredOption('consumer_key');
        $serviceName = $dsn->getRequiredOption('service_name');
        $sender = $dsn->getOption('sender');
        $host = null;
        $port = $dsn->getPort();

        return (new $class($applicationKey, $applicationSecret, $consumerKey, $serviceName, $this->client, $this->dispatcher))
            ->setHost($host)->setPort($port)->setSender($sender);
    }
}
