<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\EntityInfo;

use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;

class ViewEntityInfoManager
{
    public function __construct(
        /**
         * @var ViewEntityInfoProviderInterface[]
         */
        private readonly iterable $vienEntityInfoProviders,
        private readonly Connection $connection,
        private readonly LoggerInterface $logger,
    ) {}

    public function synchronizeOnDB(): void
    {
        $this->connection->transactional(function (Connection $conn): void {
            foreach ($this->vienEntityInfoProviders as $viewProvider) {
                foreach ($this->createOrReplaceViewSQL($viewProvider, $viewProvider->getViewName()) as $sql) {
                    $this->logger->debug('Will execute create view sql', ['sql' => $sql]);
                    $this->logger->debug($sql);
                    $conn->executeQuery($sql);
                }
            }
        });
    }

    /**
     * @return array<string>
     */
    private function createOrReplaceViewSQL(ViewEntityInfoProviderInterface $viewProvider, string $viewName): array
    {
        return [
            "DROP VIEW IF EXISTS {$viewName}",
            sprintf("CREATE OR REPLACE VIEW {$viewName} AS %s", $viewProvider->getViewQuery()),
        ];
    }
}
