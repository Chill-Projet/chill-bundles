<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Normalizer;

use Chill\MainBundle\Entity\Embeddable\PrivateCommentEmbeddable;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PrivateCommentEmbeddableNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function __construct(private readonly Security $security) {}

    public function denormalize($data, string $type, ?string $format = null, array $context = []): PrivateCommentEmbeddable
    {
        $comment = new PrivateCommentEmbeddable();

        if (null === $data) {
            return $comment;
        }

        $comment->setCommentForUser($this->security->getUser(), $data);

        return $comment;
    }

    public function normalize($object, $format = null, array $context = []): string
    {
        return $object->getCommentForUser($this->security->getUser());
    }

    public function supportsDenormalization($data, string $type, ?string $format = null): bool
    {
        return PrivateCommentEmbeddable::class === $type;
    }

    public function supportsNormalization($data, ?string $format = null): bool
    {
        return $data instanceof PrivateCommentEmbeddable;
    }
}
