<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Normalizer;

use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Templating\Entity\UserGroupRenderInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UserGroupNormalizer implements NormalizerInterface
{
    public function __construct(private readonly UserGroupRenderInterface $userGroupRender) {}

    public function normalize($object, ?string $format = null, array $context = [])
    {
        /* @var UserGroup $object */

        return [
            'type' => 'user_group',
            'id' => $object->getId(),
            'label' => $object->getLabel(),
            'backgroundColor' => $object->getBackgroundColor(),
            'foregroundColor' => $object->getForegroundColor(),
            'excludeKey' => $object->getExcludeKey(),
            'text' => $this->userGroupRender->renderString($object, []),
        ];
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof UserGroup;
    }
}
