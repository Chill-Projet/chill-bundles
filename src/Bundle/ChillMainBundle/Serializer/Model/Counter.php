<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Model;

class Counter implements \JsonSerializable
{
    public function __construct(private ?int $counter) {}

    public function getCounter(): ?int
    {
        return $this->counter;
    }

    public function jsonSerialize(): array
    {
        return ['count' => $this->counter];
    }

    public function setCounter(?int $counter): Counter
    {
        $this->counter = $counter;

        return $this;
    }
}
