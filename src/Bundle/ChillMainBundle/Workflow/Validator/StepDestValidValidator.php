<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowStep;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class StepDestValidValidator extends ConstraintValidator
{
    /**
     * @param EntityWorkflowStep $value
     *
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof StepDestValid) {
            throw new UnexpectedTypeException($constraint, StepDestValid::class);
        }

        if (!$value instanceof EntityWorkflowStep) {
            throw new UnexpectedValueException($value, EntityWorkflowStep::class);
        }

        if ($value->isFinal() && 0 < \count($value->getDestUser())) {
            $this->context->buildViolation($constraint->messageDestNotAllowed)
                ->addViolation();
        }

        if (!$value->isFinal() && 0 === \count($value->getDestUser())) {
            $this->context->buildViolation($constraint->messageRequireDest)
                ->addViolation();
        }
    }
}
