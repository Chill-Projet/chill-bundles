<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Check that the next stop has a dest user if this is required by the transition.
 */
#[\Attribute]
class TransitionHasDestUserIfRequired extends Constraint
{
    public $messageDestUserRequired = 'workflow.You must add at least one dest user or email';
    public $codeDestUserRequired = '637c20a6-822c-11ef-a4dd-07b4c0c0efa8';
    public $messageDestUserNotAuthorized = 'workflow.dest_user_not_authorized';
    public $codeDestUserNotAuthorized = '8377be2c-822e-11ef-b53a-57ad65828a8e';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
