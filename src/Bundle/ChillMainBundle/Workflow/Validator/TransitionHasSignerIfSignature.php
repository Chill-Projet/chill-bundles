<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class TransitionHasSignerIfSignature extends Constraint
{
    public $message = 'workflow.You must add a destinee for signing';
    public $messageShouldBeEmpty = 'workflow.You must not add a destinee for signing';
    public $code = '81d9e284-9d31-11ef-a078-6767f100370b';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
