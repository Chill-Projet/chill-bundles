<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Check that a transition does have at least one external if the 'to' is 'isSentExternal'.
 */
#[\Attribute]
class TransitionHasDestineeIfIsSentExternal extends Constraint
{
    public $messageDestineeRequired = 'workflow.transition_has_destinee_if_sent_external';
    public $messageDestineeNotNecessary = 'workflow.transition_destinee_not_necessary';
    public $codeNoNecessaryDestinee = 'd78ea142-819d-11ef-a459-b7009a3e4caf';
    public $codeDestineeUnauthorized = 'eb8051fc-8227-11ef-8c3b-7f2de85bdc5b';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
