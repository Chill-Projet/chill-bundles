<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Messenger;

/**
 * Message sent after a EntityWorkflowSendView was created, which means that
 * an external user has seen a link for a public view.
 */
class PostPublicViewMessage
{
    public function __construct(
        public int $entityWorkflowSendViewId,
    ) {}
}
