<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowSend;
use Chill\MainBundle\Workflow\Templating\EntityWorkflowViewMetadataDTO;

interface EntityWorkflowWithPublicViewInterface
{
    /**
     * Render the public view for EntityWorkflowSend.
     *
     * The public view must be a safe html string
     */
    public function renderPublicView(EntityWorkflowSend $entityWorkflowSend, EntityWorkflowViewMetadataDTO $metadata): string;
}
