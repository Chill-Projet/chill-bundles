<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Templating;

/**
 * This is a DTO to give some metadata about the vizualisation of the
 * EntityWorkflowView.
 *
 * The aim is to give some information from the controller which show the public view and the
 * handler which will render the view.
 */
final readonly class EntityWorkflowViewMetadataDTO
{
    public function __construct(
        public int $viewsCount,
        public int $viewsRemaining,
    ) {}
}
