<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Templating;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Twig\Environment;

final readonly class WorkflowEntityRender implements ChillEntityRenderInterface
{
    public function __construct(private EntityWorkflowManager $entityWorkflowManager, private Environment $twig) {}

    public function renderBox($entity, array $options): string
    {
        /** @var EntityWorkflow $entity */
        $handler = $this->entityWorkflowManager->getHandler($entity);

        return $this->twig->render(
            $handler->getTemplate($entity),
            $handler->getTemplateData($entity)
        );
    }

    public function renderString($entity, array $options): string
    {
        /** @var EntityWorkflow $entity */
        $handler = $this->entityWorkflowManager->getHandler($entity);

        return $handler->getEntityTitle($entity, $options);
    }

    public function supports(object $entity, array $options): bool
    {
        return $entity instanceof EntityWorkflow;
    }
}
