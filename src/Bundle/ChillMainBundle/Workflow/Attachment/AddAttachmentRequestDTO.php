<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Attachment;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class AddAttachmentRequestDTO
{
    #[Serializer\Groups('write')]
    #[Assert\NotNull()]
    public ?string $relatedGenericDocKey = null;
    #[Serializer\Groups('write')]
    #[Assert\NotNull()]
    public ?array $relatedGenericDocIdentifiers = null;

    public function __construct(
        public readonly EntityWorkflow $entityWorkflow,
    ) {}
}
