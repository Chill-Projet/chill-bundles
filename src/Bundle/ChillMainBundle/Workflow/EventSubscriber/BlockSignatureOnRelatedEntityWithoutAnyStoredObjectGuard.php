<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\TransitionBlocker;

final readonly class BlockSignatureOnRelatedEntityWithoutAnyStoredObjectGuard implements EventSubscriberInterface
{
    public function __construct(private EntityWorkflowManager $entityWorkflowManager) {}

    public static function getSubscribedEvents()
    {
        return [
            'workflow.guard' => [
                ['blockSignatureIfNoStoredObject', 0],
            ],
        ];
    }

    public function blockSignatureIfNoStoredObject(GuardEvent $event): void
    {
        $entityWorkflow = $event->getSubject();

        if (!$entityWorkflow instanceof EntityWorkflow) {
            return;
        }

        $metadataStore = $event->getWorkflow()->getMetadataStore();

        foreach ($event->getTransition()->getTos() as $to) {
            $placeMetadata = $metadataStore->getPlaceMetadata($to);
            if ([] !== ($placeMetadata['isSignature'] ?? [])) {
                if (!$this->entityWorkflowManager->canAssociateStoredObject($entityWorkflow)) {
                    $event->addTransitionBlocker(
                        new TransitionBlocker(
                            'workflow.May not associate a document',
                            'e8e28caa-a106-11ef-97e8-f3919e8b5c8a'
                        )
                    );
                }
            }
        }
    }
}
