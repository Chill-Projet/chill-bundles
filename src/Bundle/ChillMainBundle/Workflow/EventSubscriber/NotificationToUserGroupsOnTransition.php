<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Chill\MainBundle\Workflow\Helper\MetadataExtractor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Registry;

final readonly class NotificationToUserGroupsOnTransition implements EventSubscriberInterface
{
    public function __construct(
        private \Twig\Environment $engine,
        private MetadataExtractor $metadataExtractor,
        private Registry $registry,
        private MailerInterface $mailer,
        private EntityManagerInterface $entityManager,
        private EntityWorkflowManager $entityWorkflowManager,
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.completed' => ['onCompletedSendNotification', 2048],
        ];
    }

    /**
     * Send a notification to:.
     *
     * * the dests of the new step;
     * * the users which subscribed to workflow, on each step, or on final
     *
     * **Warning** take care that this method must be executed **after** the dest users are added to
     * the step (@see{EntityWorkflowStep::addDestUser}). Currently, this is done during
     *
     * @see{EntityWorkflowTransitionEventSubscriber::addDests}.
     */
    public function onCompletedSendNotification(Event $event): void
    {
        if (!$event->getSubject() instanceof EntityWorkflow) {
            return;
        }

        /** @var EntityWorkflow $entityWorkflow */
        $entityWorkflow = $event->getSubject();

        $place = $this->metadataExtractor->buildArrayPresentationForPlace($entityWorkflow);
        $workflow = $this->metadataExtractor->buildArrayPresentationForWorkflow(
            $this->registry->get($entityWorkflow, $entityWorkflow->getWorkflowName())
        );
        $title = $this->entityWorkflowManager->getHandler($entityWorkflow)->getEntityTitle($entityWorkflow);

        $currentStep = $entityWorkflow->getCurrentStep();
        if (!$this->entityManager->contains($currentStep)) {
            // this is necessary to generate an id for the step
            $this->entityManager->persist($currentStep);
        }

        // send to groups
        foreach ($entityWorkflow->getCurrentStep()->getDestUserGroups() as $userGroup) {
            if (!$userGroup->hasEmail()) {
                continue;
            }

            $context = [
                'entity_workflow' => $entityWorkflow,
                'user_group' => $userGroup,
                'place' => $place,
                'workflow' => $workflow,
                'is_dest' => true,
                'title' => $title,
            ];

            $email = new TemplatedEmail();
            $email
                ->htmlTemplate('@ChillMain/Workflow/workflow_notification_on_transition_completed_content_to_user_group.fr.txt.twig')
                ->context($context)
                ->subject(
                    $this->engine->render('@ChillMain/Workflow/workflow_notification_on_transition_completed_title.fr.txt.twig', $context)
                )
                ->to($userGroup->getEmail());

            $this->mailer->send($email);
        }
    }
}
