<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;

/**
 * Add methods to handle workflows associated with @see{StoredObject}.
 *
 * @template T of object
 *
 * @template-extends EntityWorkflowHandlerInterface<T>
 */
interface EntityWorkflowWithStoredObjectHandlerInterface extends EntityWorkflowHandlerInterface
{
    public function getAssociatedStoredObject(EntityWorkflow $entityWorkflow): ?StoredObject;
}
