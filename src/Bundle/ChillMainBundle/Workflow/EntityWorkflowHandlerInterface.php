<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\ThirdPartyBundle\Entity\ThirdParty;

/**
 * @template T of object
 */
interface EntityWorkflowHandlerInterface
{
    /**
     * @return array|string[]
     */
    public function getDeletionRoles(): array;

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array;

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string;

    /**
     * @return T|null
     */
    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?object;

    /**
     * Return a related accompanying period, if the workflow can be related in an accompanying period.
     */
    public function getRelatedAccompanyingPeriod(EntityWorkflow $entityWorkflow): ?AccompanyingPeriod;

    public function getRelatedObjects(object $object): array;

    /**
     * Return a string representing the role required for seeing the workflow.
     *
     * Return Null if any check is required, or a role name. The voter will check for
     * authorization with the role as attribute, and the
     */
    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string;

    /**
     * @return User[]
     */
    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array;

    /**
     * @return list<Person>
     */
    public function getSuggestedPersons(EntityWorkflow $entityWorkflow): array;

    /**
     * @return list<ThirdParty>
     */
    public function getSuggestedThirdParties(EntityWorkflow $entityWorkflow): array;

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string;

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array;

    public function isObjectSupported(object $object): bool;

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool;

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool;

    /**
     * @return list<EntityWorkflow>
     */
    public function findByRelatedEntity(object $object): array;
}
