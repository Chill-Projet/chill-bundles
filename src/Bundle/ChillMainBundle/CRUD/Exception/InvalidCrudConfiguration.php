<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\CRUD\Exception;

class InvalidCrudConfiguration extends \RuntimeException
{
    public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function configurationRequiresControllerDefinition(string $class, string $apiOrCrud): self
    {
        return new self(sprintf('The definition requires that a controller definition exists in container configuration. Add a `controller` key to your %s definition, for class %s', $apiOrCrud, $class), 48);
    }
}
