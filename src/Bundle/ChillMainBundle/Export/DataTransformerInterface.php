<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

/**
 * Transform data from filter.
 *
 * This interface defines a method for transforming filter's form data before it is processed.
 *
 * You can implement this interface on @see{FilterInterface} or @see{AggregatorInterface}, to allow to transform existing data in saved exports
 * and replace it with some default values, or new default values.
 */
interface DataTransformerInterface
{
    public function transformData(?array $before): array;
}
