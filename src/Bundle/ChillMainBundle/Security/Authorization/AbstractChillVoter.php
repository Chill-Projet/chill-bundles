<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter for Chill software.
 *
 * This abstract Voter provide generic methods to handle object specific to Chill
 */
abstract class AbstractChillVoter extends Voter implements ChillVoterInterface {}
