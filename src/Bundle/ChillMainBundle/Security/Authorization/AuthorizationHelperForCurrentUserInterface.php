<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Scope;

interface AuthorizationHelperForCurrentUserInterface
{
    /**
     * Get reachable Centers for the given user, role,
     * and optionnaly Scope.
     *
     * @return Center[]
     */
    public function getReachableCenters(string $role, ?Scope $scope = null): array;

    /**
     * @param list<Center>|Center $center
     *
     * @return list<Scope>
     */
    public function getReachableScopes(string $role, array|Center $center): array;
}
