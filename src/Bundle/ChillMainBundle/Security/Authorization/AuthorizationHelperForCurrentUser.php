<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Security;

class AuthorizationHelperForCurrentUser implements AuthorizationHelperForCurrentUserInterface
{
    public function __construct(private readonly AuthorizationHelperInterface $authorizationHelper, private readonly Security $security) {}

    public function getReachableCenters(string $role, ?Scope $scope = null): array
    {
        if (!$this->security->getUser() instanceof User) {
            return [];
        }

        return $this->authorizationHelper->getReachableCenters($this->security->getUser(), $role, $scope);
    }

    public function getReachableScopes(string $role, $center): array
    {
        if (!$this->security->getUser() instanceof User) {
            return [];
        }

        return $this->authorizationHelper->getReachableScopes($this->security->getUser(), $role, $center);
    }
}
