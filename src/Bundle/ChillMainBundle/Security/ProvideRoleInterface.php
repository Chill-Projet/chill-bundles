<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security;

/**
 * Declare role.
 *
 * The role are added to the configuration at compile time.
 *
 * The implemented object must be declared as a service and tagged as
 *
 * <pre>
 * my_role_declaration:
 *    # ...
 *    tags:
 *       - { name: chill_main.provide_role }
 * </pre>
 *
 * This tag is also automatically added using dependency injection autoconfiguration.
 */
interface ProvideRoleInterface
{
    /**
     * Return an array of role provided by the object.
     *
     * @return string[] array of roles (as string)
     */
    public function getRoles(): array;

    /**
     * Return roles which doesn't need.
     *
     * @return string[] array of roles without scopes
     */
    public function getRolesWithoutScope(): array;
}
