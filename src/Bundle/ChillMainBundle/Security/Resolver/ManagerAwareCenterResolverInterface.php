<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

/**
 * Center resolver which need the CenterResolverManager.
 *
 * Appended to @see{CenterResolverInterface} which needs the @{CenterResolverManager} to resolve the center
 */
interface ManagerAwareCenterResolverInterface
{
    public function setResolverManager(CenterResolverManagerInterface $manager): void;
}
