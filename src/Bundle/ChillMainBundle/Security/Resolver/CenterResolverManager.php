<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Chill\MainBundle\Entity\Center;

final readonly class CenterResolverManager implements CenterResolverManagerInterface
{
    /**
     * @param CenterResolverInterface[] $resolvers
     */
    public function __construct(private iterable $resolvers = [])
    {
        foreach ($resolvers as $resolver) {
            if ($resolver instanceof ManagerAwareCenterResolverInterface) {
                $resolver->setResolverManager($this);
            }
        }
    }

    public function resolveCenters($entity, ?array $options = []): array
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($entity, $options)) {
                $resolved = $resolver->resolveCenter($entity, $options);

                if (null === $resolved) {
                    return [];
                }

                if ($resolved instanceof Center) {
                    return [$resolved];
                }

                if (\is_array($resolved)) {
                    return $resolved;
                }

                throw new \UnexpectedValueException(sprintf('the return type of a %s should be an instance of %s, an array or null. Resolver is %s', CenterResolverInterface::class, Center::class, $resolver::class));
            }
        }

        return [];
    }
}
