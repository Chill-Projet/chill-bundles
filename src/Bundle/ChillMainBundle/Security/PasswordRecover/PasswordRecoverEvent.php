<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\PasswordRecover;

use Chill\MainBundle\Entity\User;

class PasswordRecoverEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const ASK_TOKEN_INVALID_FORM = 'chill_main.ask_token_invalid_form';

    final public const ASK_TOKEN_SUCCESS = 'chill_main.ask_token_success';

    final public const INVALID_TOKEN = 'chill_main.password_recover_invalid_token';

    /**
     * @param bool       $safelyGenerated true if generated safely (from console command, etc.)
     * @param mixed|null $ip
     */
    public function __construct(
        private readonly ?string $token = null,
        private readonly ?User $user = null,
        private $ip = null,
        private readonly bool $safelyGenerated = false,
    ) {}

    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function hasIp(): bool
    {
        return !empty($this->ip);
    }

    public function hasUser(): bool
    {
        return $this->user instanceof User;
    }

    public function isSafelyGenerated(): bool
    {
        return $this->safelyGenerated;
    }
}
