<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

/**
 * Create paginator instances.
 */
interface PaginatorFactoryInterface
{
    /**
     * create a paginator instance.
     *
     * The default route and route parameters are the current ones. If set,
     * thos route are overriden.
     *
     * @param string|null $route           the specific route to use in pages
     * @param array|null  $routeParameters the specific route parameters to use in pages
     */
    public function create(int $totalItems, ?string $route = null, ?array $routeParameters = null): PaginatorInterface;

    public function getCurrentItemsPerPage(): int;

    public function getCurrentPageFirstItemNumber(): int;

    public function getCurrentPageNumber(): int;
}
