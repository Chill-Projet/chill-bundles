<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

/**
 * PageGenerator associated with a Paginator.
 */
class PageGenerator implements \Iterator
{
    protected int $current = 1;

    public function __construct(protected Paginator $paginator) {}

    public function current(): Page
    {
        return $this->paginator->getPage($this->current);
    }

    public function key(): int
    {
        return $this->current;
    }

    public function next(): void
    {
        ++$this->current;
    }

    public function rewind(): void
    {
        $this->current = 1;
    }

    public function valid(): bool
    {
        return 0 < $this->current
              && $this->paginator->countPages() >= $this->current;
    }
}
