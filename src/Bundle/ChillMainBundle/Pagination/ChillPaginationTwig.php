<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * add twig function to render pagination.
 */
class ChillPaginationTwig extends AbstractExtension
{
    final public const LONG_TEMPLATE = '@ChillMain/Pagination/long.html.twig';

    final public const SHORT_TEMPLATE = '@ChillMain/Pagination/short.html.twig';

    public function getFunctions()
    {
        return [
            new TwigFunction(
                'chill_pagination',
                $this->paginationRender(...),
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ]
            ),
        ];
    }

    public function getName()
    {
        return 'chill_pagination';
    }

    public function paginationRender(
        Environment $env,
        PaginatorInterface $paginator,
        $template = '@ChillMain/Pagination/long.html.twig',
    ) {
        $t = match ($template) {
            'long' => self::LONG_TEMPLATE,
            'short' => self::SHORT_TEMPLATE,
            default => $template,
        };

        return $env->render($t, [
            'paginator' => $paginator,
            'current' => $paginator->getCurrentPage()->getNumber(),
        ]);
    }
}
