<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * add twig function to render pagination.
 */
class ChillItemsPerPageTwig extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction(
                'chill_items_per_page',
                $this->paginationRender(...),
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ]
            ),
        ];
    }

    public function getName()
    {
        return 'chill_items_per_page';
    }

    public function paginationRender(
        Environment $env,
        PaginatorInterface $paginator,
        $template = '@ChillMain/Pagination/items_per_page.html.twig',
    ) {
        return $env->render($template, [
            'paginator' => $paginator,
            'current' => $paginator->getItemsPerPage(),
        ]);
    }
}
