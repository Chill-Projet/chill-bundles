<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController.
 */
class LoginController extends AbstractController
{
    /**
     * @var AuthenticationUtils
     */
    protected $helper;

    public function __construct(AuthenticationUtils $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Show a login form.
     */
    #[\Symfony\Component\Routing\Annotation\Route(path: '/login', name: 'login')]
    public function loginAction(Request $request): Response
    {
        return $this->render('@ChillMain/Login/login.html.twig', [
            'last_username' => $this->helper->getLastUsername(),
            'error' => $this->helper->getLastAuthenticationError(),
        ]);
    }

    public function LoginCheckAction(Request $request) {}
}
