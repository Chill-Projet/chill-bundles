<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Templating\UI\CountNotificationUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class UIController.
 */
class UIController extends AbstractController
{
    public function showNotificationUserCounterAction(
        CountNotificationUser $counter,
    ) {
        $nb = $counter->getSumNotification($this->getUser());

        return $this->render('@ChillMain/UI/notification_user_counter.html.twig', [
            'nb' => $nb,
        ]);
    }
}
