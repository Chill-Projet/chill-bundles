<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PermissionApiController extends AbstractController
{
    public function __construct(private readonly DenormalizerInterface $denormalizer, private readonly Security $security) {}

    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    #[Route(path: '/api/1.0/main/permissions/info.json', methods: ['POST'])]
    public function getPermissions(Request $request): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $data = \json_decode($request->getContent(), true);

        if (null === $data) {
            throw new BadRequestHttpException(sprintf('Could not decode json received, or data invalid: %s, %s', json_last_error(), json_last_error_msg()));
        }

        if (!\array_key_exists('object', $data)) {
            throw new BadRequestHttpException('the object key is not present');
        }

        if (!\array_key_exists('class', $data)) {
            throw new BadRequestHttpException('the class key is not present');
        }

        if (null !== $data['object']) {
            $object = $this->denormalizer->denormalize($data['object'], $data['class'], 'json');
        } else {
            $object = null;
        }
        $roles = [];

        foreach (($data['roles'] ?? []) as $role) {
            $roles[$role] = $this->security->isGranted($role, $object);
        }

        return $this->json(
            ['roles' => $roles],
            200,
            [],
        );
    }
}
