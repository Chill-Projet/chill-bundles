<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route(path: '/{_locale}/admin', name: 'chill_main_admin_central')]
    public function indexAction()
    {
        return $this->render('@ChillMain/Admin/index.html.twig');
    }

    #[Route(path: '/{_locale}/admin/language', name: 'chill_main_language_admin')]
    public function indexLanguageAction()
    {
        return $this->render('@ChillMain/Admin/indexLanguage.html.twig');
    }

    #[Route(path: '/{_locale}/admin/location', name: 'chill_main_location_admin')]
    public function indexLocationAction()
    {
        return $this->render('@ChillMain/Admin/indexLocation.html.twig');
    }

    #[Route(path: '/{_locale}/admin/user', name: 'chill_main_user_admin')]
    public function indexUserAction()
    {
        return $this->render('@ChillMain/Admin/indexUser.html.twig');
    }

    #[Route(path: '/{_locale}/admin/dashboard', name: 'chill_main_dashboard_admin')]
    public function indexDashboardAction()
    {
        return $this->render('@ChillMain/Admin/indexDashboard.html.twig');
    }
}
