<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class UserJobController extends CRUDController
{
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        $query->addOrderBy('e.id', 'ASC');

        return parent::orderQuery($action, $query, $request, $paginator);
    }
}
