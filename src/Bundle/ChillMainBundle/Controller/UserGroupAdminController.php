<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class UserGroupAdminController extends CRUDController
{
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        $query->addSelect('JSON_EXTRACT(e.label, :lang) AS HIDDEN labeli18n')
            ->setParameter('lang', $request->getLocale());
        $query->addOrderBy('labeli18n', 'ASC');

        return $query;
    }
}
