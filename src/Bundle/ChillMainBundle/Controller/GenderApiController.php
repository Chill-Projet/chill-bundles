<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class GenderApiController extends ApiController
{
    protected function customizeQuery(string $action, Request $request, $query): void
    {
        $query
            ->andWhere(
                $query->expr()->eq('e.active', "'TRUE'")
            );
    }

    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator, $_format)
    {
        return $query->addOrderBy('e.order', 'ASC');
    }
}
