<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class MenuController.
 */
class MenuController extends AbstractController
{
    public function writeMenuAction($menu, $layout, $activeRouteKey = null, array $args = [])
    {
        return $this->render($layout, [
            'menu_composer' => $this->get('chill.main.menu_composer'),
            'menu' => $menu,
            'args' => $args,
            'activeRouteKey' => $activeRouteKey,
        ]);
    }
}
