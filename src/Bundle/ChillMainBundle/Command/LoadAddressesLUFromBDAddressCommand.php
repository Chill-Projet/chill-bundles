<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Service\Import\AddressReferenceLU;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadAddressesLUFromBDAddressCommand extends Command
{
    protected static $defaultDescription = 'Import LUX addresses from BD addresses (see https://data.public.lu/fr/datasets/adresses-georeferencees-bd-adresses/)';

    public function __construct(
        private readonly AddressReferenceLU $addressImporter,
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chill:main:address-ref-lux')
            ->addOption('send-report-email', 's', InputOption::VALUE_REQUIRED, 'Email address where a list of unimported addresses can be send');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->addressImporter->import(
            $input->hasOption('send-report-email') ? $input->getOption('send-report-email') : null,
        );

        return Command::SUCCESS;
    }
}
