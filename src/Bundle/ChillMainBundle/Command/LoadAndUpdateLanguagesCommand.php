<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Intl\Languages;

/*
 * Load or update the languages entities command
 */
class LoadAndUpdateLanguagesCommand extends Command
{
    final public const INCLUDE_ANCIENT = 'include_ancient';

    final public const INCLUDE_REGIONAL_VERSION = 'include_regional';

    // Array of ancien languages (to exclude)
    private array $ancientToExclude = ['ang', 'egy', 'fro', 'goh', 'grc', 'la', 'non', 'peo', 'pro', 'sga',
        'dum', 'enm', 'frm', 'gmh', 'mga', 'akk', 'phn', 'zxx', 'got', 'und', ];

    // The regional version of language are language with _ in the code
    // This array contains regional code to not exclude
    private array $regionalVersionToInclude = ['ro_MD'];

    /**
     * LoadCountriesCommand constructor.
     */
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly ParameterBagInterface $parameterBag)
    {
        parent::__construct();
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this
            ->setName('chill:main:languages:populate')
            ->addOption(
                self::INCLUDE_REGIONAL_VERSION,
                null,
                InputOption::VALUE_NONE,
                'Include the regional languages. The regional languages are languages with code containing _ excepted '
                        .implode(',', $this->regionalVersionToInclude).'.'
            )
            ->addOption(
                self::INCLUDE_ANCIENT,
                null,
                InputOption::VALUE_NONE,
                'Include the ancient languages that are languages with code '
                        .implode(', ', $this->ancientToExclude).'.'
            );
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->entityManager;
        $chillAvailableLanguages = $this->parameterBag->get('chill_main.available_languages');
        $languages = [];

        foreach ($chillAvailableLanguages as $avLang) {
            $languages[$avLang] = Languages::getNames();
        }

        foreach (Languages::getNames() as $code => $lang) {
            $excludeCode = (
                (
                    !$input->getOption(self::INCLUDE_REGIONAL_VERSION)
                    && strpos($code, '_')
                    && !\in_array($code, $this->regionalVersionToInclude, true)
                ) || (
                    !$input->getOption(self::INCLUDE_ANCIENT)
                    && \in_array($code, $this->ancientToExclude, true)
                )
            );

            if (true === $excludeCode) {
                continue;
            }

            $languageDB = $em->getRepository(Language::class)->find($code);

            if (null === $languageDB) {
                $languageDB = new Language();
                $languageDB->setId($code);
                $em->persist($languageDB);
            }

            $avLangNames = [];

            foreach ($chillAvailableLanguages as $avLang) {
                $avLangNames[$avLang] = ucfirst(Languages::getName($code, $avLang));
            }

            $languageDB->setName($avLangNames);
        }

        $em->flush();

        return Command::SUCCESS;
    }
}
