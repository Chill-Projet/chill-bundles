<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Service\Import\AddressReferenceFromBano;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadAddressesFRFromBANOCommand extends Command
{
    protected static $defaultDescription = 'Import FR addresses from bano (see https://bano.openstreetmap.fr';

    public function __construct(private readonly AddressReferenceFromBano $addressReferenceFromBano)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('chill:main:address-ref-from-bano')
            ->addArgument('departementNo', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'a list of departement numbers')
            ->addOption('send-report-email', 's', InputOption::VALUE_REQUIRED, 'Email address where a list of unimported addresses can be send');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($input->getArgument('departementNo') as $departementNo) {
            $output->writeln('Import addresses for '.$departementNo);

            $this->addressReferenceFromBano->import($departementNo, $input->hasOption('send-report-email') ? $input->getOption('send-report-email') : null);
        }

        return Command::SUCCESS;
    }
}
