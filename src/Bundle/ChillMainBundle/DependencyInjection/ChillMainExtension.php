<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

use Chill\MainBundle\Controller\AddressApiController;
use Chill\MainBundle\Controller\CenterController;
use Chill\MainBundle\Controller\CivilityApiController;
use Chill\MainBundle\Controller\CivilityController;
use Chill\MainBundle\Controller\CountryApiController;
use Chill\MainBundle\Controller\CountryController;
use Chill\MainBundle\Controller\GenderApiController;
use Chill\MainBundle\Controller\GenderController;
use Chill\MainBundle\Controller\GeographicalUnitApiController;
use Chill\MainBundle\Controller\LanguageController;
use Chill\MainBundle\Controller\LocationController;
use Chill\MainBundle\Controller\LocationTypeController;
use Chill\MainBundle\Controller\NewsItemController;
use Chill\MainBundle\Controller\RegroupmentController;
use Chill\MainBundle\Controller\UserController;
use Chill\MainBundle\Controller\UserGroupAdminController;
use Chill\MainBundle\Controller\UserGroupApiController;
use Chill\MainBundle\Controller\UserJobApiController;
use Chill\MainBundle\Controller\UserJobController;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Chill\MainBundle\Doctrine\DQL\Age;
use Chill\MainBundle\Doctrine\DQL\Extract;
use Chill\MainBundle\Doctrine\DQL\GetJsonFieldByKey;
use Chill\MainBundle\Doctrine\DQL\Greatest;
use Chill\MainBundle\Doctrine\DQL\JsonAggregate;
use Chill\MainBundle\Doctrine\DQL\JsonbArrayLength;
use Chill\MainBundle\Doctrine\DQL\JsonbExistsInArray;
use Chill\MainBundle\Doctrine\DQL\JsonBuildObject;
use Chill\MainBundle\Doctrine\DQL\JsonExtract;
use Chill\MainBundle\Doctrine\DQL\Least;
use Chill\MainBundle\Doctrine\DQL\OverlapsI;
use Chill\MainBundle\Doctrine\DQL\Replace;
use Chill\MainBundle\Doctrine\DQL\Similarity;
use Chill\MainBundle\Doctrine\DQL\STContains;
use Chill\MainBundle\Doctrine\DQL\StrictWordSimilarityOPS;
use Chill\MainBundle\Doctrine\DQL\STX;
use Chill\MainBundle\Doctrine\DQL\STY;
use Chill\MainBundle\Doctrine\DQL\ToChar;
use Chill\MainBundle\Doctrine\DQL\Unaccent;
use Chill\MainBundle\Doctrine\ORM\Hydration\FlatHierarchyEntityHydrator;
use Chill\MainBundle\Doctrine\Type\NativeDateIntervalType;
use Chill\MainBundle\Doctrine\Type\PointType;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Civility;
use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Entity\GeographicalUnitLayer;
use Chill\MainBundle\Entity\Language;
use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\LocationType;
use Chill\MainBundle\Entity\NewsItem;
use Chill\MainBundle\Entity\Regroupment;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Form\CenterType;
use Chill\MainBundle\Form\CivilityType;
use Chill\MainBundle\Form\CountryType;
use Chill\MainBundle\Form\GenderType;
use Chill\MainBundle\Form\LanguageType;
use Chill\MainBundle\Form\LocationFormType;
use Chill\MainBundle\Form\LocationTypeType;
use Chill\MainBundle\Form\NewsItemType;
use Chill\MainBundle\Form\RegroupmentType;
use Chill\MainBundle\Form\UserGroupType;
use Chill\MainBundle\Form\UserJobType;
use Chill\MainBundle\Form\UserType;
use Misd\PhoneNumberBundle\Doctrine\DBAL\Types\PhoneNumberType;
use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class ChillMainExtension
 * This class load config for chillMainExtension.
 */
class ChillMainExtension extends Extension implements
    PrependExtensionInterface,
    Widget\HasWidgetFactoriesExtensionInterface
{
    /**
     * widget factory.
     *
     * @var WidgetFactoryInterface[]
     */
    protected $widgetFactories = [];

    public function addWidgetFactory(WidgetFactoryInterface $factory)
    {
        $this->widgetFactories[] = $factory;
    }

    public function getConfiguration(array $config, ContainerBuilder $container): Configuration
    {
        return new Configuration($this->widgetFactories, $container);
    }

    /**
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories()
    {
        return $this->widgetFactories;
    }

    /**
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // configuration for main bundle
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        // replace all config with a main key:
        $container->setParameter('chill_main', $config);

        // legacy config
        $container->setParameter(
            'chill_main.installation_name',
            $config['installation_name']
        );

        $container->setParameter(
            'chill_main.available_languages',
            $config['available_languages']
        );

        $container->setParameter(
            'chill_main.available_countries',
            $config['available_countries']
        );

        $container->setParameter(
            'chill_main.access_global_history',
            $config['access_global_history']
        );

        $container->setParameter(
            'chill_main.access_user_change_password',
            $config['access_user_change_password']
        );

        $container->setParameter(
            'chill_main.access_permissions_group_list',
            $config['access_permissions_group_list']
        );

        $container->setParameter(
            'chill_main.add_address',
            $config['add_address']
        );

        $container->setParameter(
            'chill_main.routing.resources',
            $config['routing']['resources']
        );

        $container->setParameter(
            'chill_main.pagination.item_per_page',
            $config['pagination']['item_per_page']
        );

        $container->setParameter(
            'chill_main.notifications',
            $config['notifications']
        );

        $container->setParameter(
            'chill_main.redis',
            $config['redis']
        );

        $container->setParameter(
            'chill_main.phone_helper',
            $config['phone_helper'] ?? []
        );

        // add the key 'widget' without the key 'enable'
        $container->setParameter(
            'chill_main.widgets',
            isset($config['widgets']['homepage']) ?
                ['homepage' => $config['widgets']['homepage']] :
                []
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/doctrine.yaml');
        $loader->load('services/logger.yaml');
        $loader->load('services/pagination.yaml');
        $loader->load('services/export.yaml');
        $loader->load('services/form.yaml');
        $loader->load('services/validator.yaml');
        $loader->load('services/widget.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/routing.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/notification.yaml');
        $loader->load('services/redis.yaml');
        $loader->load('services/command.yaml');
        $loader->load('services/phonenumber.yaml');
        $loader->load('services/cache.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/timeline.yaml');
        $loader->load('services/search.yaml');
        $loader->load('services/serializer.yaml');
        $loader->load('services/mailer.yaml');
        $loader->load('services/short_message.yaml');

        $this->configureCruds($container, $config['cruds'], $config['apis'], $loader);
        $container->setParameter('chill_main.short_messages', $config['short_messages']);
        // $this->configureSms($config['short_messages'], $container, $loader);
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependNotifierTexterWithLegacyData($container);

        // add installation_name and date_format to globals
        $chillMainConfig = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration($this
            ->getConfiguration($chillMainConfig, $container), $chillMainConfig);
        $twigConfig = [
            'globals' => [
                'installation' => [
                    'name' => $config['installation_name'], ],
                'available_languages' => $config['available_languages'],
                'add_address' => $config['add_address'],
            ],
            'form_themes' => ['@ChillMain/Form/fields.html.twig'],
        ];
        $container->prependExtensionConfig('twig', $twigConfig);

        // add DQL function to ORM (default entity_manager)
        $container
            ->prependExtensionConfig(
                'doctrine',
                [
                    'orm' => [
                        'dql' => [
                            'string_functions' => [
                                'unaccent' => Unaccent::class,
                                'GET_JSON_FIELD_BY_KEY' => GetJsonFieldByKey::class,
                                'AGGREGATE' => JsonAggregate::class,
                                'REPLACE' => Replace::class,
                                'JSON_EXTRACT' => JsonExtract::class,
                                'JSON_BUILD_OBJECT' => JsonBuildObject::class,
                            ],
                            'numeric_functions' => [
                                'JSONB_EXISTS_IN_ARRAY' => JsonbExistsInArray::class,
                                'SIMILARITY' => Similarity::class,
                                'OVERLAPSI' => OverlapsI::class,
                                'STRICT_WORD_SIMILARITY_OPS' => StrictWordSimilarityOPS::class,
                                'ST_CONTAINS' => STContains::class,
                                'JSONB_ARRAY_LENGTH' => JsonbArrayLength::class,
                                'ST_X' => STX::class,
                                'ST_Y' => STY::class,
                                'GREATEST' => Greatest::class,
                                'LEAST' => Least::class,
                            ],
                            'datetime_functions' => [
                                'EXTRACT' => Extract::class,
                                'TO_CHAR' => ToChar::class,
                                'AGE' => Age::class,
                            ],
                        ],
                        'hydrators' => [
                            'chill_flat_hierarchy_list' => FlatHierarchyEntityHydrator::class,
                        ],
                    ],
                ],
            );

        // add dbal types (default entity_manager)
        $container
            ->prependExtensionConfig(
                'doctrine',
                [
                    'dbal' => [
                        // ignore views:
                        'schema_filter' => '~^(?!view_)~',
                        // This is mandatory since we are using postgis as database.
                        'mapping_types' => [
                            'geometry' => 'string',
                        ],
                        'types' => [
                            'dateinterval' => NativeDateIntervalType::class,
                            'point' => PointType::class,
                            'uuid' => UuidType::class,
                            'phone_number' => PhoneNumberType::class,
                        ],
                    ],
                ]
            );

        // add current route to chill main
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillMainBundle/config/routes.yaml',
                ],
            ],
        ]);

        // add a channel to log app events
        $container->prependExtensionConfig('monolog', [
            'channels' => ['chill'],
        ]);

        $container->prependExtensionConfig('security', [
            'access_decision_manager' => [
                'strategy' => 'unanimous',
                'allow_if_all_abstain' => false,
            ],
        ]);

        // add crud api
        $this->prependCruds($container);
    }

    /**
     * Load parameter for configuration and set parameters for api.
     */
    protected function configureCruds(
        ContainerBuilder $container,
        array $crudConfig,
        array $apiConfig,
        Loader\YamlFileLoader $loader,
    ): void {
        if (0 === \count($crudConfig)) {
            return;
        }

        $loader->load('services/crud.yaml');

        $container->setParameter('chill_main_crud_route_loader_config', $crudConfig);
        $container->setParameter('chill_main_api_route_loader_config', $apiConfig);

        // Note: the controller are loaded inside compiler pass
    }

    /**
     * This method prepend framework configuration with legacy configuration from "ovhCloudTransporter".
     *
     * It can be safely removed when the option chill_main.short_message.dsn will be removed.
     */
    private function prependNotifierTexterWithLegacyData(ContainerBuilder $container): void
    {
        foreach (array_reverse($container->getExtensionConfig('framework')) as $c) {
            // we look into each configuration for framework. If there is a configuration for
            // texter_transports in one of them, we don't configure anything else
            if (null !== $notifConfig = $c['notifier'] ?? null) {
                if (null !== ($notifConfig['texter_transports'] ?? null)) {
                    return;
                }
            }
        }

        // there is no texter config, we try to configure one
        $configs = $container->getExtensionConfig('chill_main');
        $notifierSet = false;
        foreach (array_reverse($configs) as $config) {
            if (!array_key_exists('short_messages', $config)) {
                continue;
            }

            if (array_key_exists('dsn', $config['short_messages'])) {
                $container->prependExtensionConfig('framework', [
                    'notifier' => [
                        'texter_transports' => [
                            'ovh_legacy' => $config['short_messages']['dsn'],
                        ],
                    ],
                ]);
                $notifierSet = true;
            }
        }
        if (!$notifierSet) {
            $container->prependExtensionConfig('framework', [
                'notifier' => [
                    'texter_transports' => [
                        'dummy' => 'null://null',
                    ],
                ],
            ]);
        }


    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => UserGroup::class,
                    'controller' => UserGroupAdminController::class,
                    'name' => 'admin_user_group',
                    'base_path' => '/admin/main/user-group',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => UserGroupType::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserGroup/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserGroup/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserGroup/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => UserJob::class,
                    'controller' => UserJobController::class,
                    'name' => 'admin_user_job',
                    'base_path' => '/admin/main/user-job',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => UserJobType::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserJob/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserJob/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/UserJob/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => User::class,
                    'controller' => UserController::class,
                    'name' => 'admin_user',
                    'base_path' => '/admin/main/user',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => UserType::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/User/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/User/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/User/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Location::class,
                    'name' => 'main_location',
                    'base_path' => '/admin/main/location',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => LocationFormType::class,
                    'controller' => LocationController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Location/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Location/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Location/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => LocationType::class,
                    'name' => 'main_location_type',
                    'base_path' => '/admin/main/location-type',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => LocationTypeType::class,
                    'controller' => LocationTypeController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/LocationType/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/LocationType/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/LocationType/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Country::class,
                    'name' => 'main_country',
                    'base_path' => '/admin/main/country',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => CountryType::class,
                    'controller' => CountryController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Country/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Country/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Country/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Civility::class,
                    'name' => 'main_civility',
                    'base_path' => '/admin/main/civility',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => CivilityType::class,
                    'controller' => CivilityController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Civility/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Civility/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Civility/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Gender::class,
                    'name' => 'main_gender',
                    'base_path' => '/admin/main/gender',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => GenderType::class,
                    'controller' => GenderController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Gender/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Gender/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Gender/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Language::class,
                    'name' => 'main_language',
                    'base_path' => '/admin/main/language',
                    'base_role' => 'ROLE_ADMIN',
                    'form_class' => LanguageType::class,
                    'controller' => LanguageController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Language/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Language/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Language/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Regroupment::class,
                    'name' => 'regroupment',
                    'base_path' => '/admin/regroupment',
                    'form_class' => RegroupmentType::class,
                    'controller' => RegroupmentController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Regroupment/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Regroupment/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Regroupment/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => Center::class,
                    'name' => 'center',
                    'base_path' => '/admin/center',
                    'form_class' => CenterType::class,
                    'controller' => CenterController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Center/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Center/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/Admin/Center/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => NewsItem::class,
                    'name' => 'news_item',
                    'base_path' => '/admin/news_item',
                    'form_class' => NewsItemType::class,
                    'controller' => NewsItemController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/NewsItem/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/NewsItem/new.html.twig',
                        ],
                        'view' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/NewsItem/view_admin.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/NewsItem/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillMain/NewsItem/delete.html.twig',
                        ],
                    ],
                ],
            ],
            'apis' => [
                [
                    'class' => \Chill\MainBundle\Entity\Address::class,
                    'controller' => AddressApiController::class,
                    'name' => 'address',
                    'base_path' => '/api/1.0/main/address',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_POST => true,
                                Request::METHOD_HEAD => true,
                                Request::METHOD_PATCH => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => UserJob::class,
                    'name' => 'user_job',
                    'base_path' => '/api/1.0/main/user-job',
                    'base_role' => 'ROLE_USER',
                    'controller' => UserJobApiController::class,
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'controller' => \Chill\MainBundle\Controller\AddressReferenceAPIController::class,
                    'class' => \Chill\MainBundle\Entity\AddressReference::class,
                    'name' => 'address_reference',
                    'base_path' => '/api/1.0/main/address-reference',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'controller' => \Chill\MainBundle\Controller\PostalCodeAPIController::class,
                    'class' => \Chill\MainBundle\Entity\PostalCode::class,
                    'name' => 'postal_code',
                    'base_path' => '/api/1.0/main/postal-code',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                                Request::METHOD_POST => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => Country::class,
                    'controller' => CountryApiController::class,
                    'name' => 'country',
                    'base_path' => '/api/1.0/main/country',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => User::class,
                    'controller' => \Chill\MainBundle\Controller\UserApiController::class,
                    'name' => 'user',
                    'base_path' => '/api/1.0/main/user',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => \Chill\MainBundle\Entity\Scope::class,
                    'controller' => \Chill\MainBundle\Controller\ScopeApiController::class,
                    'name' => 'scope',
                    'base_path' => '/api/1.0/main/scope',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => Location::class,
                    'controller' => \Chill\MainBundle\Controller\LocationApiController::class,
                    'name' => 'location',
                    'base_path' => '/api/1.0/main/location',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                                Request::METHOD_POST => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => LocationType::class,
                    'controller' => \Chill\MainBundle\Controller\LocationTypeApiController::class,
                    'name' => 'location_type',
                    'base_path' => '/api/1.0/main/location-type',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => Civility::class,
                    'name' => 'civility',
                    'base_path' => '/api/1.0/main/civility',
                    'base_role' => 'ROLE_USER',
                    'controller' => CivilityApiController::class,
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => Gender::class,
                    'name' => 'gender',
                    'base_path' => '/api/1.0/main/gender',
                    'base_role' => 'ROLE_USER',
                    'controller' => GenderApiController::class,
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => GeographicalUnitLayer::class,
                    'controller' => GeographicalUnitApiController::class,
                    'name' => 'geographical-unit-layer',
                    'base_path' => '/api/1.0/main/geographical-unit-layer',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
                [
                    'class' => UserGroup::class,
                    'controller' => UserGroupApiController::class,
                    'name' => 'user-group',
                    'base_path' => '/api/1.0/main/user-group',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
