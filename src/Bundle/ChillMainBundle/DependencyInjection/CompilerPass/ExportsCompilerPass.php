<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Chill\MainBundle\Export\ExportManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Compiles the services tagged with :.
 *
 * - chill.export
 * - chill.export_formatter
 * - chill.export_aggregator
 * - chill.export_filter
 * - chill.export_elements_provider
 */
class ExportsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(ExportManager::class)) {
            throw new \LogicException('service '.ExportManager::class.' is not defined. It is required by ExportsCompilerPass');
        }

        $chillManagerDefinition = $container->findDefinition(
            ExportManager::class
        );

        $this->compileFormatters($chillManagerDefinition, $container);
        $this->compileExportElementsProvider($chillManagerDefinition, $container);
    }

    private function compileExportElementsProvider(
        Definition $chillManagerDefinition,
        ContainerBuilder $container,
    ) {
        $taggedServices = $container->findTaggedServiceIds(
            'chill.export_elements_provider'
        );

        $knownAliases = [];

        foreach ($taggedServices as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!isset($attributes['prefix'])) {
                    throw new \LogicException("the 'prefix' attribute is missing in your service '{$id}' definition");
                }

                if (array_search($attributes['prefix'], $knownAliases, true)) {
                    throw new \LogicException('There is already a chill.export_elements_provider service with prefix '.$attributes['prefix'].'. Choose another prefix.');
                }
                $knownAliases[] = $attributes['prefix'];

                $chillManagerDefinition->addMethodCall(
                    'addExportElementsProvider',
                    [new Reference($id), $attributes['prefix']]
                );
            }
        }
    }

    private function compileFormatters(
        Definition $chillManagerDefinition,
        ContainerBuilder $container,
    ) {
        $taggedServices = $container->findTaggedServiceIds(
            'chill.export_formatter'
        );

        $knownAliases = [];

        foreach ($taggedServices as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!isset($attributes['alias'])) {
                    throw new \LogicException("the 'alias' attribute is missing in your service '{$id}' definition");
                }

                if (array_search($attributes['alias'], $knownAliases, true)) {
                    throw new \LogicException('There is already a chill.export_formatter service with alias '.$attributes['alias'].'. Choose another alias.');
                }
                $knownAliases[] = $attributes['alias'];

                $chillManagerDefinition->addMethodCall(
                    'addFormatter',
                    [new Reference($id), $attributes['alias']]
                );
            }
        }
    }
}
