import { createApp } from "vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import { appMessages } from "ChillMainAssets/vuejs/HomepageWidget/js/i18n";
import { store } from "ChillMainAssets/vuejs/HomepageWidget/js/store";
import App from "ChillMainAssets/vuejs/HomepageWidget/App";

const i18n = _createI18n(appMessages);

const app = createApp({
  template: `<app></app>`,
})
  .use(store)
  .use(i18n)
  .component("app", App)
  .mount("#homepage_widget");
