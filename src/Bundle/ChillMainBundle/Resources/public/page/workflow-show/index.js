import { ShowHide } from "ChillMainAssets/lib/show_hide/show_hide.js";

window.addEventListener("DOMContentLoaded", function () {
  const divTransitions = document.querySelector("#transitions"),
    futureDestUsersContainer = document.querySelector("#futureDests"),
    personSignatureField = document.querySelector("#person-signature-field"),
    userSignatureField = document.querySelector("#user-signature-field"),
    signatureTypeChoices = document.querySelector("#signature-type-choice"),
    personChoice = document.querySelector(
      "#workflow_step_isPersonOrUserSignature_0",
    ),
    userChoice = document.querySelector(
      "#workflow_step_isPersonOrUserSignature_1",
    ),
    signatureZone = document.querySelector("#signature-zone"),
    transitionFilterContainer = document.querySelector("#transitionFilter"),
    transitionsContainer = document.querySelector("#transitions"),
    sendExternalContainer = document.querySelector("#sendExternalContainer");
  // ShowHide instance for future dest users
  new ShowHide({
    debug: false,
    load_event: null,
    froms: [divTransitions],
    container: [futureDestUsersContainer],
    test: function (froms, event) {
      for (let transition of froms) {
        for (let input of transition.querySelectorAll("input")) {
          if (input.checked) {
            if ("1" === input.dataset.toFinal) {
              return false;
            }

            if ("1" === input.dataset.isSentExternal) {
              return false;
            }

            const inputData = JSON.parse(
              input.getAttribute("data-is-signature"),
            );
            if (inputData.includes("person") || inputData.includes("user")) {
              return false;
            } else {
              personChoice.checked = false;
              userChoice.checked = false;

              return true;
            }
          }
        }
      }
      return false;
    },
  });

  // ShowHide instance for send external
  new ShowHide({
    debug: false,
    load_event: null,
    froms: [divTransitions],
    container: [sendExternalContainer],
    test: function (froms, event) {
      for (let transition of froms) {
        for (let input of transition.querySelectorAll("input")) {
          if (input.checked) {
            if ("1" === input.dataset.isSentExternal) {
              return true;
            }
          }
        }
      }
      return false;
    },
  });

  // ShowHide signature zone
  new ShowHide({
    debug: false,
    load_event: null,
    froms: [divTransitions],
    container: [signatureZone],
    test: function (froms, event) {
      for (let transition of froms) {
        for (let input of transition.querySelectorAll("input")) {
          if (input.checked) {
            const inputData = JSON.parse(
              input.getAttribute("data-is-signature"),
            );
            if (inputData.includes("person") || inputData.includes("user")) {
              return true;
            }
          }
        }
      }
      return false;
    },
  });

  // ShowHides for personSignatureField or userSignatureField within signature zone
  new ShowHide({
    debug: false,
    froms: [signatureTypeChoices],
    container: [personSignatureField],
    test: function (froms, event) {
      for (let container of froms) {
        return personChoice.checked;
      }
      return false;
    },
  });

  new ShowHide({
    debug: false,
    froms: [signatureTypeChoices],
    container: [userSignatureField],
    test: function (froms, event) {
      for (let container of froms) {
        return userChoice.checked;
      }
      return false;
    },
  });

  if (null !== divTransitions) {
    new ShowHide({
      load_event: null,
      froms: [divTransitions],
      container: [futureDestUsersContainer],
      test: function (divs, arg2, arg3) {
        for (let div of divs) {
          for (let input of div.querySelectorAll("input")) {
            if (input.checked) {
              if (input.dataset.toFinal === "1") {
                return false;
              } else {
                return true;
              }
            }
          }
        }

        return true;
      },
    });
  }

  if (null !== transitionFilterContainer) {
    transitionsContainer
      .querySelectorAll(".form-check")
      .forEach(function (row) {
        const isForward = row.querySelector("input").dataset.isForward;

        new ShowHide({
          load_event: null,
          froms: [transitionFilterContainer],
          container: row,
          test: function (containers) {
            for (let container of containers) {
              for (let input of container.querySelectorAll("input")) {
                if (input.checked) {
                  return isForward === input.value;
                }
              }
            }
            return true;
          },
          toggle_callback: function (c, dir) {
            for (let div of c) {
              let input = div.querySelector("input");
              if ("hide" === dir) {
                input.checked = false;
                input.disabled = true;
              } else {
                input.disabled = false;
              }
            }
          },
        });
      });
  }
});
