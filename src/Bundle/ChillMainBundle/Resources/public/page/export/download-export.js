import { download_report } from "../../lib/download-report/download-report";

window.addEventListener("DOMContentLoaded", function (e) {
  const export_generate_url = window.export_generate_url;

  if (typeof export_generate_url === "undefined") {
    console.error("Alias not found!");
    throw new Error("Alias not found!");
  }

  const query = window.location.search,
    container = document.querySelector("#download_container");
  download_report(export_generate_url + query.toString(), container);
});
