import { fetchResults } from "./apiMethods";
import { Location, LocationType } from "../../types";

export const getLocations = (): Promise<Location[]> =>
    fetchResults("/api/1.0/main/location.json");

export const getLocationTypes = (): Promise<LocationType[]> =>
    fetchResults("/api/1.0/main/location-type.json");
