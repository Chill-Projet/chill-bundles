import { User } from "../../types";
import { makeFetch } from "./apiMethods";

export const whoami = (): Promise<User> => {
    const url = `/api/1.0/main/whoami.json`;
    return fetch(url).then((response) => {
        if (response.ok) {
            return response.json();
        }
        throw {
            msg: "Error while getting whoami.",
            sta: response.status,
            txt: response.statusText,
            err: new Error(),
            body: response.body,
        };
    });
};

export const whereami = (): Promise<Location | null> => {
    const url = `/api/1.0/main/user-current-location.json`;

    return makeFetch<null, Location | null>("GET", url);
};
