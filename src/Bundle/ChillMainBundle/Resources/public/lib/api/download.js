// const _fetchAction = (page, uri, params) => {
//     const item_per_page = 50;
//     if (params === undefined) {
//         params = {};
//     }
//     let url = uri + '?' + new URLSearchParams({ item_per_page, page, ...params });

//     return fetch(url, {
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json;charset=utf-8'
//         },
//     }).then(response => {
//         if (response.ok) { return response.json(); }
//         throw Error({ m: response.statusText });
//     });
// };

// const fetchResults = async (uri, params) => {
//     let promises = [],
//         page = 1;
//     let firstData = await _fetchAction(page, uri, params);

//     promises.push(Promise.resolve(firstData.results));

//     if (firstData.pagination.more) {
//         do {
//             page = ++page;
//             promises.push(_fetchAction(page, uri, params).then(r => Promise.resolve(r.results)));
//         } while (page * firstData.pagination.items_per_page < firstData.count)
//     }

//     return Promise.all(promises).then(values => values.flat());
// };

// export {
//     fetchResults
// };
