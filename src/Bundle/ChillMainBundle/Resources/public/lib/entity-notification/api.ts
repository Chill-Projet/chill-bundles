const buildLinkCreate = function (
    relatedEntityClass: string,
    relatedEntityId: number,
    to: number | null,
    returnPath: string | null,
): string {
    const params = new URLSearchParams();
    params.append("entityClass", relatedEntityClass);
    params.append("entityId", relatedEntityId.toString());

    if (null !== to) {
        params.append("tos[0]", to.toString());
    }

    if (null !== returnPath) {
        params.append("returnPath", returnPath);
    }

    return `/fr/notification/create?${params.toString()}`;
};

export { buildLinkCreate };
