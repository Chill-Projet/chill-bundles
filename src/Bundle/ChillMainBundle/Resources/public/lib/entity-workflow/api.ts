/**
 * Constructs a URL for creating a workflow link associated with a specific entity.
 *
 * @param {string} workflowName - The name of the workflow to associate.
 * @param {string} relatedEntityClass - The class/type of the related entity.
 * @param {number|undefined} relatedEntityId - The unique identifier of the related entity. Must be defined.
 *
 * @returns {string} The constructed URL containing query parameters based on the provided arguments.
 *
 * @throws {Error} If the related entity ID is undefined.
 */
export const buildLinkCreate = (
    workflowName: string,
    relatedEntityClass: string,
    relatedEntityId: number | undefined,
): string => {
    if (typeof relatedEntityId === "undefined") {
        throw new Error("the related entity id is not set");
    }
    const params = new URLSearchParams();
    params.set("entityClass", relatedEntityClass);
    params.set("entityId", relatedEntityId.toString(10));
    params.set("workflow", workflowName);

    return `/fr/main/workflow/create?` + params.toString();
};
