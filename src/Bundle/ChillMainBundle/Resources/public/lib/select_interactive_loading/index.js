window.addEventListener("load", function (e) {
  var postalCodes = document.querySelectorAll(
    "[data-select-interactive-loading]",
  );
  for (let i = 0; i < postalCodes.length; i++) {
    let searchUrl = postalCodes[i].dataset.searchUrl,
      noResultsLabel = postalCodes[i].dataset.noResultsLabel,
      errorLoadLabel = postalCodes[i].dataset.errorLoadLabel,
      searchingLabel = postalCodes[i].dataset.searchingLabel;
    $(postalCodes[i]).select2({
      allowClear: true,
      language: {
        errorLoading: function () {
          return errorLoadLabel;
        },
        noResults: function () {
          return noResultsLabel;
        },
        searching: function () {
          return searchingLabel;
        },
      },
      ajax: {
        url: searchUrl,
        dataType: "json",
        delay: 250,
      },
    });
  }
});
