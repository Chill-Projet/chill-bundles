import { WorkflowAttachment } from "ChillMainAssets/types";
import { GenericDocForAccompanyingPeriod } from "ChillDocStoreAssets/types/generic_doc";
import { makeFetch } from "ChillMainAssets/lib/api/apiMethods";

export const find_attachments_by_workflow = async (
    workflowId: number,
): Promise<WorkflowAttachment[]> =>
    makeFetch("GET", `/api/1.0/main/workflow/${workflowId}/attachment`);

export const create_attachment = async (
    workflowId: number,
    genericDoc: GenericDocForAccompanyingPeriod,
): Promise<WorkflowAttachment> =>
    makeFetch("POST", `/api/1.0/main/workflow/${workflowId}/attachment`, {
        relatedGenericDocKey: genericDoc.key,
        relatedGenericDocIdentifiers: genericDoc.identifiers,
    });

export const delete_attachment = async (
    attachment: WorkflowAttachment,
): Promise<void> =>
    makeFetch("DELETE", `/api/1.0/main/workflow/attachment/${attachment.id}`);
