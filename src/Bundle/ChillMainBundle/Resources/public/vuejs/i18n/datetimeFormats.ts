export default {
    fr: {
        short: {
            year: "numeric",
            month: "numeric",
            day: "numeric",
        },
        text: {
            year: "numeric",
            month: "long",
            day: "numeric",
        },
        long: {
            year: "numeric",
            month: "numeric",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        },
        hoursOnly: {
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        },
    },
};
