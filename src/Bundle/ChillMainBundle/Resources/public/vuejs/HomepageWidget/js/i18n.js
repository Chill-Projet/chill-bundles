const appMessages = {
  fr: {
    main_title: "Vue d'ensemble",
    my_works: {
      tab: "Mes actions",
      description:
        "Liste des actions d'accompagnement dont je suis référent et qui arrivent à échéance.",
    },
    my_evaluations: {
      tab: "Mes évaluations",
      description:
        "Liste des évaluations dont je suis référent et qui arrivent à échéance.",
    },
    my_tasks: {
      tab: "Mes tâches",
      description_alert:
        "Liste des tâches auxquelles je suis assigné et dont la date de rappel est dépassée.",
      description_warning:
        "Liste des tâches auxquelles je suis assigné et dont la date d'échéance est dépassée.",
    },
    my_accompanying_courses: {
      tab: "Mes nouveaux parcours",
      description:
        "Liste des parcours d'accompagnement que l'on vient de m'attribuer depuis moins de 15 jours.",
    },
    my_notifications: {
      tab: "Mes nouvelles notifications",
      description: "Liste des notifications reçues et non lues.",
    },
    my_workflows: {
      tab: "Mes workflows",
      description: "Liste des workflows en attente d'une action.",
      description_cc: "Liste des workflows dont je suis en copie.",
    },
    opening_date: "Date d'ouverture",
    social_issues: "Problématiques sociales",
    concerned_persons: "Usagers concernés",
    max_date: "Date d'échéance",
    warning_date: "Date de rappel",
    evaluation: "Évaluation",
    task: "Tâche",
    Date: "Date",
    From: "Expéditeur",
    Subject: "Objet",
    Entity: "Associé à",
    Step: "Étape",
    concerned_users: "Usagers concernés",
    Object_workflow: "Objet du workflow",
    on_hold: "En attente",
    show_entity: "Voir {entity}",
    the_activity: "l'échange",
    the_course: "le parcours",
    the_action: "l'action",
    the_evaluation: "l'évaluation",
    the_evaluation_document: "le document",
    the_task: "la tâche",
    the_workflow: "le workflow",
    StartDate: "Date d'ouverture",
    SocialAction: "Action d'accompagnement",
    no_data: "Aucun résultats",
    no_dashboard: "Pas de tableaux de bord",
    counter: {
      unread_notifications:
        "{n} notification non lue | {n} notifications non lues",
      assignated_courses:
        "{n} parcours récent assigné | {n} parcours récents assignés",
      assignated_actions: "{n} action assignée | {n} actions assignées",
      assignated_evaluations:
        "{n} évaluation assignée | {n} évaluations assignées",
      alert_tasks: "{n} tâche en rappel | {n} tâches en rappel",
      warning_tasks: "{n} tâche à échéance | {n} tâches à échéance",
    },
    emergency: "Urgent",
    confidential: "Confidentiel",
    automatic_notification: "Notification automatique",
    widget: {
      news: {
        title: "Actualités",
        readMore: "Lire la suite",
        date: "Date",
        none: "Aucune actualité",
      },
    },
  },
};

Object.assign(appMessages.fr);

export { appMessages };
