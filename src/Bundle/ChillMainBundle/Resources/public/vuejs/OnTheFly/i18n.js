const ontheflyMessages = {
  fr: {
    onthefly: {
      show: {
        person: "Détails de l'usager",
        thirdparty: "Détails du tiers",
        file_person: "Ouvrir la fiche de l'usager",
        file_thirdparty: "Voir le Tiers",
      },
      edit: {
        person: "Modifier un usager",
        thirdparty: "Modifier un tiers",
      },
      create: {
        button: 'Créer "{q}"',
        title: {
          default: "Création d'un nouvel usager ou d'un tiers professionnel",
          person: "Création d'un nouvel usager",
          thirdparty: "Création d'un nouveau tiers professionnel",
        },
        person: "un nouvel usager",
        thirdparty: "un nouveau tiers professionnel",
      },
      addContact: {
        title: "Créer un contact pour {q}",
      },
      resource_comment_title: "Un commentaire est associé à cet interlocuteur",
      addContact: {
        title: "Créer un contact pour {q}",
      },
    },
  },
};

export { ontheflyMessages };
