import { multiSelectMessages } from "ChillMainAssets/vuejs/_js/i18n";

const addressMessages = {
  fr: {
    add_an_address_title: "Créer une adresse",
    edit_an_address_title: "Modifier une adresse",
    create_a_new_address: "Créer une nouvelle adresse",
    edit_address: "Modifier l'adresse",
    select_an_address_title: "Sélectionner une adresse",
    fill_an_address: "Compléter l'adresse",
    select_country: "Choisir le pays",
    country: "Pays",
    select_city: "Choisir une localité",
    city: "Localité",
    other_city: "Autre localité",
    select_address: "Choisir une adresse",
    address: "Adresse",
    other_address: "Autre adresse",
    create_address:
      "Adresse inconnue. Cliquez ici pour créer une nouvelle adresse",
    isNoAddress: "Pas d'adresse complète",
    isConfidential: "Adresse confidentielle",
    street: "Nom de rue",
    streetNumber: "Numéro",
    floor: "Étage",
    corridor: "Couloir",
    steps: "Escalier",
    flat: "Appartement",
    buildingName: "Résidence",
    extra: "Complément d'adresse",
    distribution: "Cedex",
    create_postal_code:
      "Localité inconnue. Cliquez ici pour créer une nouvelle localité",
    postalCode_name: "Nom",
    postalCode_code: "Code postal",
    date: "Date de la nouvelle adresse",
    validFrom: "L'adresse est valable à partir du",
    validTo: "L'adresse est valable jusqu'au",
    back_to_the_list: "Retour à la liste",
    loading: "chargement en cours...",
    address_suggestions: "Suggestion d'adresses",
    address_new_success: "La nouvelle adresse est enregistrée.",
    address_edit_success: "L'adresse a été mise à jour.",
    wait_redirection: " La page est redirigée.",
    not_yet_address:
      "Il n'y a pas encore d'adresse. Cliquez sur '+ Créer une adresse'",
    use_this_address: "Utiliser cette adresse",

    // household specific
    move_date: "Date du déménagement",
  },
};

Object.assign(addressMessages.fr, multiSelectMessages.fr);

export { addressMessages };
