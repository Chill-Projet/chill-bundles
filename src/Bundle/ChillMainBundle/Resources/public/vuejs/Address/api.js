import { getAddressById } from "ChillMainAssets/lib/api/address";

/**
 * Endpoint chill_api_single_country__index
 * method GET, get Country Object
 * @returns {Promise} a promise containing all Country object
 */
const fetchCountries = () => {
  //console.log('<<< fetching countries');

  const url = `/api/1.0/main/country.json?item_per_page=1000`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_api_single_postal_code__index
 * method GET, get Cities Object
 * @params {object} a country object
 * @returns {Promise} a promise containing all Postal Code objects filtered with country
 */
const fetchCities = (country) => {
  //console.log('<<< fetching cities for', country);
  // warning: do not use fetchResults (in apiMethods): we need only a **part** of the results in the db
  const url = `/api/1.0/main/postal-code.json?item_per_page=1000&country=${country.id}`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_main_postalcodeapi_search
 * method GET, get Cities Object
 * @params {string} search a search string
 * @params {object} country a country object
 * @returns {Promise} a promise containing all Postal Code objects filtered with country and a search string
 */
const searchCities = (search, country) => {
  const url = `/api/1.0/main/postal-code/search.json?q=${search}&country=${country.id}`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_main_addressreferenceapi_search
 * method GET, get AddressReference Object
 * @params {string} search a search string
 * @params {object} postalCode a postalCode object
 * @returns {Promise} a promise containing all Postal Code objects filtered with country and a search string
 */
const searchReferenceAddresses = (search, postalCode) => {
  const url = `/api/1.0/main/address-reference/by-postal-code/${postalCode.id}/search.json?q=${search}`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_api_single_address_reference__index
 * method GET, get AddressReference Object
 * @returns {Promise} a promise containing all AddressReference objects filtered with postal code
 */
const fetchReferenceAddresses = (postalCode) => {
  //console.log('<<< fetching references addresses for', postalCode);
  const url = `/api/1.0/main/address-reference.json?item_per_page=1000&postal_code=${postalCode.id}`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_api_single_address_reference__index
 * method GET, get AddressReference Object
 * @returns {Promise} a promise containing all AddressReference objects filtered with postal code
 */
const fetchAddresses = () => {
  //console.log('<<< fetching addresses');
  //TODO deal with huge number of addresses... we should do suggestion...
  const url = `/api/1.0/main/address.json?item_per_page=1000`;
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_api_single_address__entity__create
 * method POST, post Address Object
 * @returns {Promise}
 */
const postAddress = (address) => {
  const url = `/api/1.0/main/address.json?`;
  const body = address;

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 *
 * @param address
 * @returns {Promise<Response>}
 */
const duplicateAddress = (address) => {
  const url = `/api/1.0/main/address/${address.address_id}/duplicate.json`;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/**
 * Endpoint chill_api_single_address__entity__create
 * method PATCH, patch Address Instance
 *
 *  @id     integer - id of address
 *  @body   Object - dictionary with changes to post
 */
const patchAddress = (id, body) => {
  const url = `/api/1.0/main/address/${id}.json`;
  return fetch(url, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_postal_code__entity_create
 * method POST, post Postal Code Object
 * @returns {Promise}
 */
const postPostalCode = (postalCode) => {
  //<--
  const url = `/api/1.0/main/postal-code.json?`;
  const body = postalCode;

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw Error("Error with request resource response");
  });
};

/*
 * Endpoint chill_api_single_address__index
 * method GET, get Address Object
 * @params {id} the address id
 * @returns {Promise} a promise containing a Address object
 */
const getAddress = (id) => {
  return getAddressById(id);
};

export {
  duplicateAddress,
  fetchCountries,
  fetchCities,
  fetchReferenceAddresses,
  fetchAddresses,
  postAddress,
  patchAddress,
  postPostalCode,
  getAddress,
  searchCities,
  searchReferenceAddresses,
};
