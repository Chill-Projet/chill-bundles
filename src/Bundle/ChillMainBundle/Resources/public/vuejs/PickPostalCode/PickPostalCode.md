# Pickpostalcode

Allow to pick a postal code.

In use with module `mod_pick_postal_code`, associated  with `PickPostalCodeType` in php.

## Usage
    
    `<pick-postal-code @select-city="onCitySelected" @removeCity="onCityRemoved" :picked="city"></pick-postal-code>`

## Props

* `picked`: the city picked. A javascript object (a city). Null if empty.
* `country`: country to restraint search on picked. May be null.

## Emits

### `selectCity`

When a city is onCitySelected.

Argument: a js object, representing a city

### `removeCity`

When a city is removed.


Argument: a js object, representing a city
