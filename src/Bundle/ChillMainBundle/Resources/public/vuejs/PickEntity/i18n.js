import { personMessages } from "ChillPersonAssets/vuejs/_js/i18n";
import { thirdpartyMessages } from "ChillThirdPartyAssets/vuejs/_js/i18n";
import { addressMessages } from "ChillMainAssets/vuejs/Address/i18n";
import { ontheflyMessages } from "ChillMainAssets/vuejs/OnTheFly/i18n";

const appMessages = {
  fr: {
    pick_entity: {
      add: "Ajouter",
      modal_title: "Ajouter des ",
      user: "Utilisateurs",
      person: "Usagers",
      thirdparty: "Tiers",
      modal_title_one: "Indiquer un ",
      user_one: "Utilisateur",
      thirdparty_one: "Tiers",
      person_one: "Usager",
    },
  },
};

Object.assign(
  appMessages.fr,
  personMessages.fr,
  thirdpartyMessages.fr,
  addressMessages.fr,
  ontheflyMessages.fr,
);

export { appMessages };
