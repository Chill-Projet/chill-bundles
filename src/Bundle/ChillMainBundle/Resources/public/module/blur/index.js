require("./blur.scss");

document.querySelectorAll(".confidential").forEach(function (el) {
  let i = document.createElement("i");
  const classes = ["fa", "fa-eye-slash", "toggle-twig"];
  i.classList.add(...classes);
  el.appendChild(i);

  const toggleBlur = function (e) {
    for (let child of el.children) {
      if (!child.classList.contains("toggle-twig")) {
        child.classList.toggle("blur");
      }
    }
    i.classList.toggle("fa-eye-slash");
    i.classList.toggle("fa-eye");
  };
  i.addEventListener("click", toggleBlur);
  toggleBlur();
});
