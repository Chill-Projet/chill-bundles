import { createApp } from "vue";
import EntityWorkflowVueSubscriber from "ChillMainAssets/vuejs/_components/EntityWorkflow/EntityWorkflowVueSubscriber.vue";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";
import { appMessages } from "ChillMainAssets/vuejs/PickEntity/i18n";

const i18n = _createI18n(appMessages);

let containers = document.querySelectorAll("[data-entity-workflow-subscribe]");

containers.forEach((container) => {
  let app = {
    components: {
      EntityWorkflowVueSubscriber,
    },
    template:
      '<entity-workflow-vue-subscriber :entityWorkflowId="this.entityWorkflowId" :subscriberStep="this.subscriberStep" :subscriberFinal="this.subscriberFinal" @subscriptionUpdated="onUpdate"></entity-workflow-vue-subscriber>',
    data() {
      return {
        entityWorkflowId: Number.parseInt(container.dataset.entityWorkflowId),
        subscriberStep: container.dataset.subscribeStep === "1",
        subscriberFinal: container.dataset.subscribeFinal === "1",
      };
    },
    methods: {
      onUpdate(status) {
        this.subscriberStep = status.step;
        this.subscriberFinal = status.final;
      },
    },
  };

  createApp(app).use(i18n).mount(container);
});
