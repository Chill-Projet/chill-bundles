/**
 * Submit button(s) are disabled as soon as submit button is clicked and form is submitted.
 * ID 'create-form' must be added to submit forms.
 */

var form = document.getElementById("create-form");
var submitButtons = document.querySelectorAll("[type=submit]");

form.addEventListener("submit", function (e) {
  for (var i = 0; i < submitButtons.length; i++) {
    submitButtons[i].disabled = true;
  }
});
