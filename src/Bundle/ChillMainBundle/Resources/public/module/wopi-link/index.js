import { createApp } from "vue";
import OpenWopiLink from "ChillMainAssets/vuejs/_components/OpenWopiLink";
import { _createI18n } from "ChillMainAssets/vuejs/_js/i18n";

const i18n = _createI18n({});

//TODO move to chillDocStore or ChillWopi

/*

tags to load module:

<span data-module="wopi-link"
    data-wopi-url="{{ path('chill_wopi_file_edit', {'fileId': document.uuid}) }}"
    data-doc-type="{{ document.type|e('html_attr') }}"
    data-options="{{ options|json_encode }}"
        ></span>

*/

window.addEventListener("DOMContentLoaded", function (e) {
  document
    .querySelectorAll('span[data-module="wopi-link"]')
    .forEach(function (el) {
      createApp({
        template:
          '<open-wopi-link :wopiUrl="wopiUrl" :type="type" :options="options"></open-wopi-link>',
        components: {
          OpenWopiLink,
        },
        data() {
          return {
            wopiUrl: el.dataset.wopiUrl,
            type: el.dataset.docType,
            options:
              el.dataset.options !== "null"
                ? JSON.parse(el.dataset.options)
                : {},
          };
        },
      })
        .use(i18n)
        .mount(el);
    });
});
