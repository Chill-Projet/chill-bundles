import { createApp } from "vue";
import { _createI18n } from "../../vuejs/_js/i18n";
import NotificationReadAllToggle from "../../vuejs/_components/Notification/NotificationReadAllToggle.vue";

const i18n = _createI18n({});

document.addEventListener("DOMContentLoaded", function () {
    const elements = document.querySelectorAll(".notification_all_read");

    elements.forEach((element) => {
        console.log("launch");
        createApp({
            template: `<notification-read-all-toggle @markAsRead="markAsRead" @markAsUnRead="markAsUnread"></notification-read-all-toggle>`,
            components: {
                NotificationReadAllToggle,
            },
            methods: {
                markAsRead(id: number) {
                    const el = document.querySelector<HTMLDivElement>(
                        `div.notification-status[data-notification-id="${id}"]`,
                    );
                    if (el === null) {
                        return;
                    }
                    el.classList.add("read");
                    el.classList.remove("unread");
                },
                markAsUnread(id: number) {
                    const el = document.querySelector<HTMLDivElement>(
                        `div.notification-status[data-notification-id="${id}"]`,
                    );
                    if (el === null) {
                        return;
                    }
                    el.classList.remove("read");
                    el.classList.add("unread");
                },
            },
        })
            .use(i18n)
            .mount(element);
    });
});
