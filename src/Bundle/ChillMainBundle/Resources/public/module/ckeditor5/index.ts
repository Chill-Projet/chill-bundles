import config from "./editor_config";
import { ClassicEditor } from "ckeditor5";

const ckeditorFields: NodeListOf<HTMLTextAreaElement> =
    document.querySelectorAll("textarea[ckeditor]");
ckeditorFields.forEach((field: HTMLTextAreaElement): void => {
    ClassicEditor.create(field, config).catch((error) => {
        console.error(error.stack);
        throw error;
    });
});
//Fields.push.apply(Fields, document.querySelectorAll('.cf-fields textarea'));
