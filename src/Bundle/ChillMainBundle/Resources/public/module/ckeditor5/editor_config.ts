import {
    Essentials,
    Bold,
    Italic,
    Paragraph,
    Markdown,
    BlockQuote,
    Heading,
    Link,
    List,
} from "ckeditor5";
import coreTranslations from "ckeditor5/translations/fr.js";

import "ckeditor5/ckeditor5.css";

import "./index.scss";

export default {
    plugins: [
        Essentials,
        Markdown,
        Bold,
        Italic,
        BlockQuote,
        Heading,
        Link,
        List,
        Paragraph,
    ],
    toolbar: {
        items: [
            "heading",
            "|",
            "bold",
            "italic",
            "link",
            "bulletedList",
            "numberedList",
            "blockQuote",
            "undo",
            "redo",
        ],
    },
    translations: [coreTranslations],
    licenseKey: "GPL",
};
