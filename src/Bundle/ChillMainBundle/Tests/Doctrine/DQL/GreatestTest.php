<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Doctrine\DQL;

use Chill\MainBundle\Entity\Address;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class GreatestTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testGreatestInDQL()
    {
        $dql = 'SELECT GREATEST(a.validFrom, a.validTo, :now) AS g FROM '.Address::class.' a WHERE a.validTo < :now and a.validFrom < :now';

        $actual = $this->entityManager
            ->createQuery($dql)
            ->setParameter('now', $now = new \DateTimeImmutable('now'), Types::DATE_IMMUTABLE)
            ->setMaxResults(3)
            ->getResult();

        $this->assertIsArray($actual);
        $this->assertEquals($now->format('Y-m-d'), $actual[0]['g']);
    }
}
