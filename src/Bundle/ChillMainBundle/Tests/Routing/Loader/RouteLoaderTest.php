<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Routing\Loader;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the route loader.
 *
 * @internal
 *
 * @coversNothing
 */
final class RouteLoaderTest extends KernelTestCase
{
    private ?object $router = null;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->router = self::$kernel->getContainer()->get('router');
    }

    /**
     * Test that the route loader loads at least homepage.
     */
    public function testRouteFromMainBundleAreLoaded()
    {
        $homepage = $this->router->getRouteCollection()->get('chill_main_homepage');

        $this->assertNotNull($homepage);
    }
}
