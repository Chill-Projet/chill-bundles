<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Pagination;

use Chill\MainBundle\Pagination\Page;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Test the Page class.
 *
 * @internal
 *
 * @coversNothing
 */
final class PageTest extends KernelTestCase
{
    protected $paginator;

    protected $prophet;

    protected function setUp(): void
    {
        $this->prophet = new \Prophecy\Prophet();
    }

    /**
     * return a set of element to testGetFirstItemNumber.
     *
     * the set contains :
     * - the page number ;
     * - the number of item per page ;
     * - the expected first item number
     *
     * @return array
     */
    public static function generateGetFirstItemNumber()
    {
        return [
            [1, 10, 0],
            [2, 10, 10],
        ];
    }

    /**
     * return a set of element to testGetLastItemNumber.
     *
     * the set contains :
     * - the page number ;
     * - the number of item per page ;
     * - the expected last item number
     *
     * @return array
     */
    public static function generateGetLastItemNumber()
    {
        return [
            [1, 10, 9],
            [2, 10, 19],
        ];
    }

    /**
     * @param int $number
     * @param int $itemPerPage
     * @param int $expectedItemPerPage
     *
     * @dataProvider generateGetFirstItemNumber
     */
    public function testGetFirstItemNumber(
        $number,
        $itemPerPage,
        $expectedItemPerPage,
    ) {
        $page = $this->generatePage($number, $itemPerPage);

        $this->assertEquals($expectedItemPerPage, $page->getFirstItemNumber());
    }

    /**
     * @param int $number
     * @param int $itemPerPage
     * @param int $expectedItemPerPage
     *
     * @dataProvider generateGetLastItemNumber
     */
    public function testGetLastItemNumber(
        $number,
        $itemPerPage,
        $expectedItemPerPage,
    ) {
        $page = $this->generatePage($number, $itemPerPage);

        $this->assertEquals($expectedItemPerPage, $page->getLastItemNumber());
    }

    public function testPageNumber()
    {
        $page = $this->generatePage(1);

        $this->assertEquals(1, $page->getNumber());
    }

    /**
     * @param int    $itemPerPage
     * @param string $route
     *
     * @return Page
     */
    protected function generatePage(
        mixed $number = 1,
        $itemPerPage = 10,
        $route = 'route',
        array $routeParameters = [],
        mixed $totalItems = 100,
    ) {
        $urlGenerator = $this->prophet->prophesize();
        $urlGenerator->willImplement(UrlGeneratorInterface::class);

        return new Page(
            $number,
            $itemPerPage,
            $urlGenerator->reveal(),
            $route,
            $routeParameters,
            $totalItems
        );
    }
}
