<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Serializer\Normalizer;

use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Serializer\Normalizer\UserGroupNormalizer;
use Chill\MainBundle\Templating\Entity\UserGroupRenderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @internal
 *
 * @coversNothing
 */
class UserGroupNormalizerTest extends TestCase
{
    public function testNormalize()
    {
        $userGroup = new UserGroup();
        $userGroup
            ->setLabel(['fr' => 'test'])
            ->setExcludeKey('top')
            ->setForegroundColor('#123456')
            ->setBackgroundColor('#456789');

        $entityRender = $this->createMock(UserGroupRenderInterface::class);
        $entityRender->expects($this->once())
            ->method('renderString')
            ->with($userGroup, [])
            ->willReturn('text');

        $normalizer = new UserGroupNormalizer($entityRender);

        $actual = $normalizer->normalize($userGroup, 'json', [AbstractNormalizer::GROUPS => ['read']]);

        self::assertEqualsCanonicalizing([
            'type' => 'user_group',
            'text' => 'text',
            'label' => ['fr' => 'test'],
            'excludeKey' => 'top',
            'foregroundColor' => '#123456',
            'backgroundColor' => '#456789',
            'id' => null,
        ], $actual);
    }
}
