<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Repository\UserGroupRepositoryInterface;
use Chill\MainBundle\Serializer\Normalizer\UserGroupDenormalizer;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UserGroupDenormalizerTest extends TestCase
{
    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     *
     * @dataProvider provideSupportsDenormalization
     */
    public function testSupportsDenormalization($data, string $type, bool $expected): void
    {
        $repository = $this->createMock(UserGroupRepositoryInterface::class);
        $denormalizer = new UserGroupDenormalizer($repository);

        $actual = $denormalizer->supportsDenormalization($data, $type, 'json');

        self::assertSame($expected, $actual);
    }

    public static function provideSupportsDenormalization(): iterable
    {
        yield [['type' => 'user_group', 'id' => 10], UserGroup::class, true];
        yield [['type' => 'person', 'id' => 10], UserGroup::class, false];
        yield [['type' => 'user_group', 'id' => 10], \stdClass::class, false];
    }

    public function testDenormalize(): void
    {
        $repository = $this->createMock(UserGroupRepositoryInterface::class);
        $repository->expects($this->once())
            ->method('find')
            ->with(10)
            ->willReturn($userGroup = new UserGroup());

        $denormalizer = new UserGroupDenormalizer($repository);

        $actual = $denormalizer->denormalize(['type' => 'user_group', 'id' => 10], UserGroup::class, 'json');

        self::assertSame($userGroup, $actual);
    }
}
