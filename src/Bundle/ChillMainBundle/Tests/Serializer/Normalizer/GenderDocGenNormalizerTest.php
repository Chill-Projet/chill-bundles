<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\MainBundle\Entity\Gender;
use Chill\MainBundle\Entity\GenderEnum;
use Chill\MainBundle\Serializer\Normalizer\GenderDocGenNormalizer;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class GenderDocGenNormalizerTest extends TestCase
{
    private GenderDocGenNormalizer $normalizer;
    private TranslatableStringHelperInterface $translatableStringHelper;

    protected function setUp(): void
    {
        $this->translatableStringHelper = $this->createMock(TranslatableStringHelperInterface::class);
        $this->normalizer = new GenderDocGenNormalizer($this->translatableStringHelper);
    }

    public function testSupportsNormalizationReturnsTrueForGenderInstance(): void
    {
        $gender = $this->createMock(Gender::class);
        $this->assertTrue($this->normalizer->supportsNormalization($gender));
    }

    public function testSupportsNormalizationReturnsFalseForNonGenderInstance(): void
    {
        $this->assertFalse($this->normalizer->supportsNormalization(new \stdClass()));
    }

    public function testNormalizeReturnsCorrectData(): void
    {
        $gender = $this->createMock(Gender::class);

        $gender->method('getId')->willReturn(1);
        $gender->method('getLabel')->willReturn(['fr' => 'homme', 'en' => 'man']);
        $gender->method('getGenderTranslation')->willReturn(GenderEnum::MALE);

        $this->translatableStringHelper
            ->method('localize')
            ->with(['fr' => 'homme', 'en' => 'man'])
            ->willReturn('homme');

        $expected = [
            'id' => 1,
            'label' => 'homme',
            'genderTranslation' => GenderEnum::MALE,
            'type' => 'chill_main_gender',
        ];

        $this->assertEquals($expected, $this->normalizer->normalize($gender));
    }
}
