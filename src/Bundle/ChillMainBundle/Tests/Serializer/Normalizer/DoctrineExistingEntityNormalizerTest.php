<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Serializer\Normalizer;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Serializer\Normalizer\DoctrineExistingEntityNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class DoctrineExistingEntityNormalizerTest extends KernelTestCase
{
    protected DoctrineExistingEntityNormalizer $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $serializerFactory = self::getContainer()->get(ClassMetadataFactoryInterface::class);

        $this->normalizer = new DoctrineExistingEntityNormalizer($em, $serializerFactory);
    }

    public static function dataProviderUserId()
    {
        self::bootKernel();

        $userIds = self::getContainer()->get(EntityManagerInterface::class)
            ->getRepository(User::class)
            ->createQueryBuilder('u')
            ->select('u.id')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        yield [$userIds[0]['id']];
    }

    /**
     * @dataProvider dataProviderUserId
     */
    public function testGetMappedClass(mixed $userId)
    {
        $data = ['type' => 'user', 'id' => $userId];
        $supports = $this->normalizer->supportsDenormalization($data, User::class);

        $this->assertTrue($supports);
    }
}
