<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace ChillMainBundle\Tests\Repository;

use Chill\MainBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UserRepositoryTest extends KernelTestCase
{
    private UserRepository $userRepository;

    protected function setUp(): void
    {
        self::bootKernel();
        $entityManager = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $connection = $entityManager->getConnection();
        $this->userRepository = new UserRepository($entityManager, $connection);
    }

    public function testCountFilteredUsers(): void
    {
        self::assertIsInt($this->userRepository->countFilteredUsers(null, ['Active']));
        self::assertIsInt($this->userRepository->countFilteredUsers(null, ['Active', 'Inactive']));
        self::assertIsInt($this->userRepository->countFilteredUsers(null, ['Inactive']));
        self::assertIsInt($this->userRepository->countFilteredUsers('center', ['Active']));
        self::assertIsInt($this->userRepository->countFilteredUsers('center', ['Active', 'Inactive']));
        self::assertIsInt($this->userRepository->countFilteredUsers('center', ['Inactive']));
        self::assertIsInt($this->userRepository->countFilteredUsers('center'));
    }

    public function testFindByFilteredUsers(): void
    {
        self::assertIsArray($this->userRepository->findFilteredUsers(null, ['Active']));
        self::assertIsArray($this->userRepository->findFilteredUsers(null, ['Active', 'Inactive']));
        self::assertIsArray($this->userRepository->findFilteredUsers(null, ['Inactive']));
        self::assertIsArray($this->userRepository->findFilteredUsers('center', ['Active']));
        self::assertIsArray($this->userRepository->findFilteredUsers('center', ['Active', 'Inactive']));
        self::assertIsArray($this->userRepository->findFilteredUsers('center', ['Inactive']));
        self::assertIsArray($this->userRepository->findFilteredUsers('center'));
    }
}
