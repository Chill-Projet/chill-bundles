<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace ChillMainBundle\Tests\Repository;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $em;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->em = static::getContainer()->get(EntityManagerInterface::class);
    }

    /**
     * This test only check that the query is ok.
     *
     * The semantic of the query is not checked.
     */
    public function testFindWorkflowsWithoutFinalStepAndOlderThanCheckQueryIsOk(): void
    {
        $repository = new EntityWorkflowRepository($this->em);

        $actual = $repository->findWorkflowsWithoutFinalStepAndOlderThan((new \DateTimeImmutable('now'))->add(new \DateInterval('P10Y')));

        self::assertIsIterable($actual, 'check that the query is successful');

        foreach ($actual as $entityWorkflow) {
            self::assertInstanceOf(EntityWorkflow::class, $entityWorkflow);
        }
    }

    public function testCountQueryByDest(): void
    {
        $repository = new EntityWorkflowRepository($this->em);
        $user = $this->em->createQuery(sprintf('SELECT u FROM %s u', User::class))
            ->setMaxResults(1)->getSingleResult();
        $actual = $repository->countByDest($user);

        self::assertIsInt($actual);
    }
}
