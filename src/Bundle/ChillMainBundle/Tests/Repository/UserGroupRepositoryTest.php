<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Repository;

use Chill\MainBundle\Repository\UserGroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UserGroupRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);

    }

    public function testProvideSearchApiQuery(): void
    {
        $repository = new UserGroupRepository($this->entityManager);

        $apiQuery = $repository->provideSearchApiQuery('trav', 'fr');

        // test that the query does works
        $sql = $apiQuery->buildQuery();
        $params = $apiQuery->buildParameters();

        $result = $this->entityManager->getConnection()->executeQuery($sql, $params);
        $results = $result->fetchAllAssociative();

        self::assertIsArray($results);

    }
}
