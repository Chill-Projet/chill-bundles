<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace ChillMainBundle\Tests\Repository;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\Workflow\EntityWorkflowStepRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowStepRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testCountUnreadByUser(): void
    {
        $repository = new EntityWorkflowStepRepository($this->entityManager);
        $user = $this->entityManager->createQuery(sprintf('SELECT u FROM %s u', User::class))
            ->setMaxResults(1)->getSingleResult();

        $actual = $repository->countUnreadByUser($user);

        self::assertIsInt($actual);
    }
}
