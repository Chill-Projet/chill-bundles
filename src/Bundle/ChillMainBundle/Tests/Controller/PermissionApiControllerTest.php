<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PermissionApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public function testDenormalizingObject()
    {
        // for a unknown reason, the kernel may be booted before running this test...
        self::ensureKernelShutdown();
        $client = $this->getClientAuthenticated();

        $client->request(
            'POST',
            '/api/1.0/main/permissions/info.json',
            [], // parameters
            [], // files
            [], // server
            json_encode([
                'object' => [
                    'datetime' => '1969-07-09T00:00:00+0100',
                ],
                'class' => \DateTime::class,
                'roles' => ['FOO_ROLE'],
            ])
        );

        $this->assertResponseIsSuccessful();

        $data = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertFalse($data['roles']['FOO_ROLE']);
    }

    public function testNullObject()
    {
        $client = $this->getClientAuthenticated();

        $client->request(
            'POST',
            '/api/1.0/main/permissions/info.json',
            [], // parameters
            [], // files
            [], // server
            json_encode([
                'object' => null,
                'class' => null,
                'roles' => ['ROLE_USER', 'ROLE_ADMIN'],
            ])
        );

        $this->assertResponseIsSuccessful();

        $data = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertTrue($data['roles']['ROLE_USER']);
        $this->assertFalse($data['roles']['ROLE_ADMIN']);
    }
}
