<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CenterControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    /**
     * This is a smoke test that ensure that the list page does show.
     */
    public function testCenterAdminSmokeTest(): void
    {
        $client = $this->getClientAuthenticated('admin', 'password');

        $crawler = $client->request('GET', '/fr/admin/center');
        self::assertResponseIsSuccessful('Test that /fr/admin/center does show');

        $btnEdit = $crawler->filter('.btn-edit')?->first();

        self::assertNotNull($btnEdit, 'check that there is at least one btn-edit on center page');

        $client->click($btnEdit->link());

        self::assertResponseIsSuccessful();
    }
}
