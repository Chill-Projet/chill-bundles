<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the export.
 *
 * @internal
 *
 * @coversNothing
 */
final class ExportControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public function testIndex()
    {
        $client = $this->getClientAuthenticatedAsAdmin();

        $client->request('GET', '/fr/exports/');

        self::assertResponseIsSuccessful();
    }
}
