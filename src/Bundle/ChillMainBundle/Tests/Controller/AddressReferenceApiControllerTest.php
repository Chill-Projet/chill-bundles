<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Test\PrepareClientTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddressReferenceApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public static function provideData()
    {
        self::bootKernel();
        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $postalCode = $em->createQueryBuilder()
            ->select('pc')
            ->from(PostalCode::class, 'pc')
            ->where('pc.origin = :origin')
            ->setParameter('origin', 0)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();

        yield [$postalCode->getId(), 'rue'];
    }

    /**
     * @dataProvider provideData
     */
    public function testSearch(int $postCodeId, string $pattern)
    {
        $client = $this->getClientAuthenticated();

        $client->request(
            'GET',
            "/api/1.0/main/address-reference/by-postal-code/{$postCodeId}/search.json",
            ['q' => $pattern]
        );

        $this->assertResponseIsSuccessful();
    }
}
