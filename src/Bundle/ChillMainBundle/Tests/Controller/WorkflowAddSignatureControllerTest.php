<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Service\Signature\PDFPage;
use Chill\DocStoreBundle\Service\Signature\PDFSignatureZone;
use Chill\DocStoreBundle\Service\Signature\PDFSignatureZoneAvailable;
use Chill\MainBundle\Controller\WorkflowAddSignatureController;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Security\Authorization\EntityWorkflowStepSignatureVoter;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Chill\MainBundle\Workflow\WorkflowTransitionContextDTO;
use Chill\PersonBundle\Entity\Person;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Twig\Environment;

/**
 * @internal
 *
 * @coversNothing
 */
class WorkflowAddSignatureControllerTest extends TestCase
{
    public function testAddSignature(): void
    {
        $storedObject = new StoredObject();
        $entityWorkflow = new EntityWorkflow();
        $stepTransition = new WorkflowTransitionContextDTO($entityWorkflow);
        $stepTransition->futurePersonSignatures[] = new Person();
        $entityWorkflow->setStep('step_signature', $stepTransition, 'to_signature', new \DateTimeImmutable('now'), new User());

        $signature = $entityWorkflow->getCurrentStep()->getSignatures()->first();

        $entityWorkflowManager = $this->createMock(EntityWorkflowManager::class);
        $entityWorkflowManager->method('getAssociatedStoredObject')
            ->with($entityWorkflow)
            ->willReturn($storedObject);

        $pdfSignatureZoneAvailable = $this->createMock(PDFSignatureZoneAvailable::class);
        $pdfSignatureZoneAvailable->method('getAvailableSignatureZones')->withAnyParameters()
            ->willReturn([
                new PDFSignatureZone(1, 0.0, 0.0, 100, 100, new PDFPage(1, 500.0, 500.0)),
            ]);

        $normalizer = $this->createMock(NormalizerInterface::class);
        $normalizer->method('normalize')->withAnyParameters()
            ->willReturn([]);

        $twig = $this->createMock(Environment::class);
        $twig->method('render')->with('@ChillMain/Workflow/signature_sign.html.twig', $this->isType('array'))
            ->willReturn('ok');

        $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())->method('isGranted')->with(EntityWorkflowStepSignatureVoter::SIGN, $signature)
            ->willReturn(true);

        $controller = new WorkflowAddSignatureController($entityWorkflowManager, $pdfSignatureZoneAvailable, $normalizer, $twig, $urlGenerator, $security);

        $actual = $controller($signature, new Request());

        self::assertEquals(200, $actual->getStatusCode());
        self::assertEquals('ok', $actual->getContent());
    }
}
