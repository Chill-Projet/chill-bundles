<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Test\PrepareClientTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddressControllerTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    use PrepareClientTrait;

    private KernelBrowser $client;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public static function generateAddressIds(): iterable
    {
        self::bootKernel();
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $qb = $em->createQueryBuilder();
        $addresses = $qb->select('a')->from(Address::class, 'a')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($addresses as $a) {
            yield [$a->getId()];
        }

        self::ensureKernelShutdown();
    }

    /**
     * @dataProvider generateAddressIds
     */
    public function testDuplicate(int $addressId)
    {
        $this->client = $this->getClientAuthenticated();
        $this->client->request('POST', "/api/1.0/main/address/{$addressId}/duplicate.json");

        $this->assertResponseIsSuccessful('test that duplicate is successful');
    }
}
