<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Bundle\ChillMainBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 *
 * @coversNothing
 */
final class SearchApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public static function generateSearchData()
    {
        yield ['per', ['person', 'thirdparty']];

        yield ['per', ['thirdparty']];

        yield ['per', ['person']];

        yield ['fjklmeqjfkdqjklrmefdqjklm', ['person', 'thirdparty']];
    }

    /**
     * @dataProvider generateSearchData
     */
    public function testSearch(string $pattern, array $types)
    {
        $client = $this->getClientAuthenticated();

        $client->request(
            Request::METHOD_GET,
            '/api/1.0/search.json',
            ['q' => $pattern, 'type' => $types]
        );

        $this->assertResponseIsSuccessful();
    }
}
