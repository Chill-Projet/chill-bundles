<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Entity\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Entity\Workflow\EntityWorkflowStepSignature;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowStepSignatureTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testConstruct()
    {
        $workflow = new EntityWorkflow();
        $workflow->setWorkflowName('vendee_internal')
            ->setRelatedEntityId(0)
            ->setRelatedEntityClass(AccompanyingPeriodWorkEvaluationDocument::class);

        $step = $workflow->getCurrentStep();

        $person = $this->entityManager->createQuery('SELECT p FROM '.Person::class.' p')
            ->setMaxResults(1)
            ->getSingleResult();

        $signature = new EntityWorkflowStepSignature($step, $person);

        self::assertCount(1, $step->getSignatures());
        self::assertSame($signature, $step->getSignatures()->first());

        $this->entityManager->getConnection()->beginTransaction();
        $this->entityManager->persist($workflow);
        $this->entityManager->persist($step);
        $this->entityManager->persist($signature);

        $this->entityManager->flush();
        $this->entityManager->getConnection()->commit();

        $this->entityManager->clear();

        $signatureBis = $this->entityManager->find(EntityWorkflowStepSignature::class, $signature->getId());

        self::assertEquals($signature->getId(), $signatureBis->getId());
        self::assertEquals($step->getId(), $signatureBis->getStep()->getId());
    }
}
