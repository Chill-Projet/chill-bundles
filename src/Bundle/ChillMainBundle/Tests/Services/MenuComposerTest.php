<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\RouteCollection;

/**
 * This class provide functional test for MenuComposer.
 *
 * @internal
 *
 * @coversNothing
 */
final class MenuComposerTest extends KernelTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader;
     */
    private $loader;

    /**
     * @var \Chill\MainBundle\DependencyInjection\Services\MenuComposer;
     */
    private $menuComposer;

    protected function setUp(): void
    {
        self::bootKernel(['environment' => 'test']);
        $this->menuComposer = self::getContainer()
            ->get('chill.main.menu_composer');
    }

    /**
     * @covers \Chill\MainBundle\Routing\MenuComposer
     */
    public function testMenuComposer()
    {
        $collection = new RouteCollection();

        $routes = $this->menuComposer->getRoutesFor('dummy0');

        $this->assertIsArray($routes);
    }
}
