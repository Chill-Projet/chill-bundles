<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Services\Import;

use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Repository\AddressReferenceRepository;
use Chill\MainBundle\Repository\PostalCodeRepository;
use Chill\MainBundle\Service\Import\AddressReferenceBaseImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddressReferenceBaseImporterTest extends KernelTestCase
{
    private AddressReferenceRepository $addressReferenceRepository;

    private EntityManagerInterface $entityManager;

    private AddressReferenceBaseImporter $importer;

    private PostalCodeRepository $postalCodeRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->importer = self::getContainer()->get(AddressReferenceBaseImporter::class);
        $this->addressReferenceRepository = self::getContainer()->get(AddressReferenceRepository::class);
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $this->postalCodeRepository = self::getContainer()->get(PostalCodeRepository::class);
    }

    public function testImportAddress(): void
    {
        $postalCode = (new PostalCode())
            ->setRefPostalCodeId($postalCodeId = '1234'.uniqid())
            ->setPostalCodeSource('testing')
            ->setCode('TEST456')
            ->setName('testing');

        $this->entityManager->persist($postalCode);
        $this->entityManager->flush();

        $this->importer->importAddress(
            '0000',
            $postalCodeId,
            'TEST456',
            'Rue test abccc-guessed',
            '-1',
            'unit-test',
            50.0,
            5.0,
            4326
        );

        $this->importer->finalize();

        $addresses = $this->addressReferenceRepository->findByPostalCodePattern(
            $postalCode,
            'Rue test abcc guessed'
        );

        $this->assertCount(1, $addresses);
        $this->assertEquals('Rue test abccc-guessed', $addresses[0]->getStreet());

        $previousAddressId = $addresses[0]->getId();

        $this->entityManager->clear();

        $this->importer->importAddress(
            '0000',
            $postalCodeId,
            'TEST456',
            'Rue test abccc guessed fixed',
            '-1',
            'unit-test',
            50.0,
            5.0,
            4326
        );

        $this->importer->finalize();

        $addresses = $this->addressReferenceRepository->findByPostalCodePattern(
            $postalCode,
            'abcc guessed fixed'
        );

        $this->assertCount(1, $addresses);
        $this->assertEquals('Rue test abccc guessed fixed', $addresses[0]->getStreet());
        $this->assertEquals($previousAddressId, $addresses[0]->getId());
    }
}
