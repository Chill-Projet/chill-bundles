<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Services\Import;

use Chill\MainBundle\Service\Import\GeographicalUnitBaseImporter;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class GeographicalUnitBaseImporterTest extends KernelTestCase
{
    private Connection $connection;

    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();

        $this->connection = self::getContainer()->get(Connection::class);
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testImportUnit(): void
    {
        $importer = new GeographicalUnitBaseImporter(
            $this->connection,
            new NullLogger()
        );

        $importer->importUnit(
            'test',
            ['fr' => 'Test Layer'],
            'Layer one',
            'layer_one',
            'MULTIPOLYGON (((30 20, 45 40, 10 40, 30 20)),((15 5, 40 10, 10 20, 5 10, 15 5)))',
            3812
        );

        $importer->finalize();

        $unit = $this->connection->executeQuery('
            SELECT unitname, unitrefid, cmgul.refid AS layerrefid, cmgul.name AS layername, ST_AsText(ST_snapToGrid(ST_Transform(u.geom, 3812), 1)) AS geom
            FROM chill_main_geographical_unit u JOIN chill_main_geographical_unit_layer cmgul on u.layer_id = cmgul.id
            WHERE u.unitrefid = ?', ['layer_one']);

        $results = $unit->fetchAssociative();

        $this->assertEquals($results['unitrefid'], 'layer_one');
        $this->assertEquals($results['unitname'], 'Layer one');
        $this->assertEquals(json_decode((string) $results['layername'], true, 512, JSON_THROW_ON_ERROR), ['fr' => 'Test Layer']);
        $this->assertEquals($results['layerrefid'], 'test');
        $this->assertEquals($results['geom'], 'MULTIPOLYGON(((30 20,45 40,10 40,30 20)),((15 5,40 10,10 20,5 10,15 5)))');

        $importer = new GeographicalUnitBaseImporter(
            $this->connection,
            new NullLogger()
        );

        $importer->importUnit(
            'test',
            ['fr' => 'Test Layer fixed'],
            'Layer one fixed',
            'layer_one',
            'MULTIPOLYGON (((130 120, 45 40, 10 40, 130 120)),((0 0, 15 5, 40 10, 10 20, 0 0)))',
            3812
        );

        $importer->finalize();

        $unit = $this->connection->executeQuery('
            SELECT unitname, unitrefid, cmgul.refid AS layerrefid, cmgul.name AS layername, ST_AsText(ST_snapToGrid(ST_Transform(u.geom, 3812), 1)) AS geom
            FROM chill_main_geographical_unit u JOIN chill_main_geographical_unit_layer cmgul on u.layer_id = cmgul.id
            WHERE u.unitrefid = ?', ['layer_one']);

        $results = $unit->fetchAssociative();

        $this->assertEquals($results['unitrefid'], 'layer_one');
        $this->assertEquals($results['unitname'], 'Layer one fixed');
        $this->assertEquals(json_decode((string) $results['layername'], true, 512, JSON_THROW_ON_ERROR), ['fr' => 'Test Layer fixed']);
        $this->assertEquals($results['layerrefid'], 'test');
        $this->assertEquals($results['geom'], 'MULTIPOLYGON(((130 120,45 40,10 40,130 120)),((0 0,15 5,40 10,10 20,0 0)))');
    }
}
