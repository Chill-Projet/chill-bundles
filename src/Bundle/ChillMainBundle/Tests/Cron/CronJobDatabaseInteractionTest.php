<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Cron;

use Chill\MainBundle\Cron\CronJobInterface;
use Chill\MainBundle\Cron\CronManager;
use Chill\MainBundle\Entity\CronJobExecution;
use Chill\MainBundle\Repository\CronJobExecutionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CronJobDatabaseInteractionTest extends KernelTestCase
{
    use ProphecyTrait;

    private EntityManagerInterface $entityManager;

    private CronJobExecutionRepository $cronJobExecutionRepository;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $this->cronJobExecutionRepository = self::getContainer()->get(CronJobExecutionRepository::class);
    }

    public static function tearDownAfterClass(): void
    {
        self::bootKernel();

        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $entityManager->createQuery('DELETE '.CronJobExecution::class.' cje WHERE cje.key LIKE :key')
            ->setParameter('key', 'test-with-data')
            ->execute();
    }

    public function testCompleteLifeCycle(): void
    {
        $cronjob = $this->prophesize(CronJobInterface::class);
        $cronjob->canRun(null)->willReturn(true);
        $cronjob->canRun(Argument::type(CronJobExecution::class))->willReturn(true);
        $cronjob->getKey()->willReturn('test-with-data');
        $cronjob->run([])->willReturn(['test' => 'execution-0']);
        $cronjob->run(['test' => 'execution-0'])->willReturn(['test' => 'execution-1']);

        $cronjob->run([])->shouldBeCalledOnce();
        $cronjob->run(['test' => 'execution-0'])->shouldBeCalledOnce();

        $manager = new CronManager(
            $this->cronJobExecutionRepository,
            $this->entityManager,
            [$cronjob->reveal()],
            new NullLogger()
        );

        // run a first time
        $manager->run();

        // run a second time
        $manager->run();
    }
}

class JobWithReturn implements CronJobInterface
{
    public function canRun(?CronJobExecution $cronJobExecution): bool
    {
        return true;
    }

    public function getKey(): string
    {
        return 'with-data';
    }

    public function run(array $lastExecutionData): ?array
    {
        return ['data' => 'test'];
    }
}
