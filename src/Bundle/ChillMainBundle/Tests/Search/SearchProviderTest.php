<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Search;

use Chill\MainBundle\Search\ParsingException;
use Chill\MainBundle\Search\SearchInterface;
use Chill\MainBundle\Search\SearchProvider;
use Chill\MainBundle\Search\UnknowSearchDomainException;
use Chill\MainBundle\Search\UnknowSearchNameException;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class SearchProviderTest extends TestCase
{
    private SearchProvider $search;

    protected function setUp(): void
    {
        $this->search = new SearchProvider();

        // add a default service
        $this->addSearchService(
            $this->createDefaultSearchService('I am default', 10),
            'default'
        );
        // add a domain service
        $this->addSearchService(
            $this->createNonDefaultDomainSearchService('I am domain bar', 20, false),
            'bar'
        );
    }

    public function testAccentued()
    {
        // $this->markTestSkipped('accentued characters must be implemented');

        $terms = $this->p('manço bélier aztèque à saloù ê');

        $this->assertEquals([
            '_domain' => null,
            '_default' => 'manco belier azteque a salou e',
        ], $terms);
    }

    public function testAccentuedCapitals()
    {
        // $this->markTestSkipped('accentued characters must be implemented');

        $terms = $this->p('MANÉÀ oÛ lÎ À');

        $this->assertEquals([
            '_domain' => null,
            '_default' => 'manea ou li a',
        ], $terms);
    }

    public function testArgumentNameWithTrait()
    {
        $terms = $this->p('date-from:2016-05-04');

        $this->assertEquals([
            '_domain' => null,
            'date-from' => '2016-05-04',
            '_default' => '',
        ], $terms);
    }

    public function testCapitalLetters()
    {
        $terms = $this->p('Foo:Bar LOL marCi @PERSON');

        $this->assertEquals([
            '_domain' => 'person',
            '_default' => 'lol marci',
            'foo' => 'bar',
        ], $terms);
    }

    public function testDoubleParenthesis()
    {
        $terms = $this->p('@papamobile name:"my beautiful name" residual surname:"i love techno"');

        $this->assertEquals([
            '_domain' => 'papamobile',
            'name' => 'my beautiful name',
            '_default' => 'residual',
            'surname' => 'i love techno',
        ], $terms);
    }

    public function testInvalidSearchName()
    {
        $this->expectException(UnknowSearchNameException::class);

        $this->search->getByName('invalid name');
    }

    public function testMultipleDomainError()
    {
        $this->expectException(ParsingException::class);

        $this->p('@person @report');
    }

    /**
     * Test the behaviour when no domain is provided in the search pattern :
     * the default search should be enabled.
     */
    public function testSearchResultDefault()
    {
        $response = $this->search->getSearchResults('default search');

        // $this->markTestSkipped();

        $this->assertEquals([
            'I am default',
        ], $response);
    }

    public function testSearchResultDomainSearch()
    {
        // add a search service which will be supported
        $this->addSearchService(
            $this->createNonDefaultDomainSearchService('I am domain foo', 100, true),
            'foo'
        );

        $response = $this->search->getSearchResults('@foo default search');

        $this->assertEquals([
            'I am domain foo',
        ], $response);
    }

    public function testSearchResultDomainUnknow()
    {
        $this->expectException(UnknowSearchDomainException::class);

        $this->search->getSearchResults('@unknow domain');
    }

    public function testSearchWithinSpecificSearchName()
    {
        // add a search service which will be supported
        $this->addSearchService(
            $this->createNonDefaultDomainSearchService('I am domain foo', 100, true),
            'foo'
        );

        $response = $this->search->getResultByName('@foo search', 'foo');

        $this->assertEquals('I am domain foo', $response);
    }

    public function testSearchWithinSpecificSearchNameInConflictWithSupport()
    {
        $this->expectException(ParsingException::class);

        $this->search->getResultByName('@foo default search', 'bar');
    }

    public function testSimplePattern()
    {
        $terms = $this->p('@person birthdate:2014-01-02 name:"my name" is not my name');

        $this->assertEquals([
            '_domain' => 'person',
            'birthdate' => '2014-01-02',
            '_default' => 'is not my name',
            'name' => 'my name',
        ], $terms);
    }

    public function testTrimInDefault()
    {
        $terms = $this->p('  foo bar     ');

        $this->assertEquals([
            '_domain' => null,
            '_default' => 'foo bar',
        ], $terms);
    }

    public function testTrimInParenthesis()
    {
        $terms = $this->p('foo:"bar     "');

        $this->assertEquals([
            '_domain' => null,
            'foo' => 'bar',
            '_default' => '',
        ], $terms);
    }

    public function testWithoutDefault()
    {
        $terms = $this->p('@person foo:bar');

        $this->assertEquals([
            '_domain' => 'person',
            'foo' => 'bar',
            '_default' => '',
        ], $terms);
    }

    public function testWithoutDomain()
    {
        $terms = $this->p('foo:bar residual');

        $this->assertEquals([
            '_domain' => null,
            'foo' => 'bar',
            '_default' => 'residual',
        ], $terms);
    }

    /**
     * Add a search service to the chill.main.search_provider.
     *
     * Useful for mocking the SearchInterface
     *
     * @param string $name
     */
    private function addSearchService(SearchInterface $search, $name)
    {
        $this->search
            ->addSearchService($search, $name);
    }

    private function createDefaultSearchService($result, $order)
    {
        $mock = $this
            ->getMockForAbstractClass(\Chill\MainBundle\Search\AbstractSearch::class);

        // set the mock as default
        $mock->expects($this->any())
            ->method('isActiveByDefault')
            ->will($this->returnValue(true));

        $mock->expects($this->any())
            ->method('getOrder')
            ->will($this->returnValue($order));

        // set the return value
        $mock->expects($this->any())
            ->method('renderResult')
            ->will($this->returnValue($result));

        return $mock;
    }

    private function createNonDefaultDomainSearchService($result, $order, $domain)
    {
        $mock = $this
            ->getMockForAbstractClass(\Chill\MainBundle\Search\AbstractSearch::class);

        // set the mock as default
        $mock->expects($this->any())
            ->method('isActiveByDefault')
            ->will($this->returnValue(false));

        $mock->expects($this->any())
            ->method('getOrder')
            ->will($this->returnValue($order));

        $mock->expects($this->any())
            ->method('supports')
            ->will($this->returnValue($domain));

        // set the return value
        $mock->expects($this->any())
            ->method('renderResult')
            ->will($this->returnValue($result));

        return $mock;
    }

    /**
     * shortcut for executing parse method.
     *
     * @param unknown $pattern
     *
     * @return string[]
     */
    private function p($pattern)
    {
        return $this->search->parse($pattern);
    }
}
