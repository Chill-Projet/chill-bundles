<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Search\Utils;

use Chill\MainBundle\Search\Utils\ExtractPhonenumberFromPattern;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

/**
 * @internal
 *
 * @coversNothing
 */
final class ExtractPhonenumberFromPatternTest extends KernelTestCase
{
    public static function provideData()
    {
        yield ['BE', 'Diallo', 0, [], 'Diallo', 'no phonenumber'];

        yield ['BE', 'Diallo 15/06/2021', 0, [], 'Diallo 15/06/2021', 'no phonenumber and a date'];

        yield ['BE', 'Diallo 0486 123 456', 1, ['+32486123456'], 'Diallo', 'a phonenumber and a name'];

        yield ['BE', 'Diallo 123 456', 1, ['123456'], 'Diallo', 'a number and a name, without leadiing 0'];

        yield ['BE', '123 456', 1, ['123456'], '', 'only phonenumber'];

        yield ['BE', '0123 456', 1, ['+32123456'], '', 'only phonenumber with a leading 0'];

        yield ['FR', '123 456', 1, ['123456'], '', 'only phonenumber'];

        yield ['FR', '0123 456', 1, ['+33123456'], '', 'only phonenumber with a leading 0'];

        yield ['FR', 'Diallo 0486 123 456', 1, ['+33486123456'], 'Diallo', 'a phonenumber and a name'];

        yield ['FR', 'Diallo +32486 123 456', 1, ['+32486123456'], 'Diallo', 'a phonenumber and a name'];
    }

    /**
     * @dataProvider provideData
     */
    public function testExtract(string $defaultCarrierCode, mixed $subject, mixed $expectedCount, mixed $expected, mixed $filteredSubject, mixed $msg)
    {
        $extractor = new ExtractPhonenumberFromPattern(new ParameterBag(['chill_main' => [
            'phone_helper' => ['default_carrier_code' => $defaultCarrierCode],
        ]]));
        $result = $extractor->extractPhonenumber($subject);

        $this->assertCount($expectedCount, $result->getFound());
        $this->assertEquals($filteredSubject, $result->getFilteredSubject());
        $this->assertEquals($expected, $result->getFound());
    }
}
