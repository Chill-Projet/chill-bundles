<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Security\Resolver;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Security\Resolver\CenterResolverDispatcher;
use Chill\MainBundle\Security\Resolver\CenterResolverDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CenterResolverDispatcherTest extends KernelTestCase
{
    private CenterResolverDispatcherInterface $dispatcher;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->dispatcher = self::getContainer()->get(CenterResolverDispatcher::class);
    }

    /**
     * @group legacy
     */
    public function testResolveCenter()
    {
        $center = new Center();

        $resolved = $this->dispatcher->resolveCenter($center);

        $this->assertSame($center, $resolved);
    }
}
