<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Form\DataTransformer;

use Chill\MainBundle\Form\DataTransformer\IdToEntityDataTransformer;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class IdToEntityDataTransformerTest extends TestCase
{
    use ProphecyTrait;

    public function testReverseTransformMulti()
    {
        $o1 = new \stdClass();
        $o2 = new \stdClass();

        $repository = $this->prophesize(ObjectRepository::class);
        $repository->findOneBy(Argument::exact(['id' => 1]))->willReturn($o1);
        $repository->findOneBy(Argument::exact(['id' => 2]))->willReturn($o2);

        $transformer = new IdToEntityDataTransformer(
            $repository->reveal(),
            true
        );

        $this->assertEquals([], $transformer->reverseTransform(null));
        $this->assertEquals([], $transformer->reverseTransform(''));
        $r = $transformer->reverseTransform('1,2');

        $this->assertIsArray($r);
        $this->assertSame($o1, $r[0]);
        $this->assertSame($o2, $r[1]);
    }

    public function testReverseTransformSingle()
    {
        $o = new \stdClass();

        $repository = $this->prophesize(ObjectRepository::class);
        $repository->findOneBy(Argument::exact(['id' => 1]))->willReturn($o);

        $transformer = new IdToEntityDataTransformer(
            $repository->reveal(),
            false
        );

        $this->assertEquals(null, $transformer->reverseTransform(null));
        $this->assertEquals(null, $transformer->reverseTransform(''));
        $r = $transformer->reverseTransform('1');

        $this->assertSame($o, $r);
    }

    public function testTransformMulti()
    {
        $o1 = new class () {
            public function getId()
            {
                return 1;
            }
        };
        $o2 = new class () {
            public function getId()
            {
                return 2;
            }
        };
        $repository = $this->prophesize(ObjectRepository::class);

        $transformer = new IdToEntityDataTransformer(
            $repository->reveal(),
            true
        );

        $this->assertEquals(
            '1,2',
            $transformer->transform([$o1, $o2])
        );
    }

    public function testTransformSingle()
    {
        $o = new class () {
            public function getId()
            {
                return 1;
            }
        };
        $repository = $this->prophesize(ObjectRepository::class);

        $transformer = new IdToEntityDataTransformer(
            $repository->reveal(),
            false
        );

        $this->assertEquals(
            '1',
            $transformer->transform($o)
        );
    }
}
