<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Chill\MainBundle\Workflow\EntityWorkflowMarkingStore;
use Chill\MainBundle\Workflow\EntityWorkflowWithPublicViewInterface;
use Chill\MainBundle\Workflow\EventSubscriber\EntityWorkflowGuardSendExternalIfNoPublicView;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Metadata\InMemoryMetadataStore;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowGuardSendExternalIfNoPublicViewTest extends TestCase
{
    public function testGuardSendExternalIfNoStoredObjectWithNoStoredObject()
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $handler = $this->createMock(EntityWorkflowHandlerInterface::class);
        $handler->method('supports')->willReturn(true);

        $registry = $this->buildRegistry($handler);

        $workflow = $registry->get($entityWorkflow, $entityWorkflow->getWorkflowName());

        self::assertFalse($workflow->can($entityWorkflow, 'to_send_external'));
        self::assertTrue($workflow->can($entityWorkflow, 'to_other'));
    }

    public function testGuardSendExternalIfNoStoredObjectWithStoredObject()
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $handler = $this->createMockForIntersectionOfInterfaces(
            [EntityWorkflowHandlerInterface::class, EntityWorkflowWithPublicViewInterface::class]
        );
        $handler->method('supports')->willReturn(true);

        $registry = $this->buildRegistry($handler);

        $workflow = $registry->get($entityWorkflow, $entityWorkflow->getWorkflowName());

        self::assertTrue($workflow->can($entityWorkflow, 'to_send_external'));
        self::assertTrue($workflow->can($entityWorkflow, 'to_other'));
    }

    private function buildRegistry(EntityWorkflowHandlerInterface $handler): Registry
    {
        $builder = new DefinitionBuilder();

        $builder->addPlace('initial');
        $builder->addPlace('send_external');
        $builder->addPlace('other');
        $builder->addTransition(new Transition('to_send_external', 'initial', 'send_external'));
        $builder->addTransition(new Transition('to_other', 'initial', 'other'));
        $builder->setMetadataStore(
            new InMemoryMetadataStore(
                placesMetadata: [
                    'send_external' => [
                        'isSentExternal' => true,
                    ],
                ]
            )
        );
        $definition = $builder->build();
        $workflow = new Workflow($definition, new EntityWorkflowMarkingStore(), $eventDispatcher = new EventDispatcher(), 'dummy');
        $registry = new Registry();
        $registry->addWorkflow($workflow, new class () implements WorkflowSupportStrategyInterface {
            public function supports(\Symfony\Component\Workflow\WorkflowInterface $workflow, object $subject): bool
            {
                return true;
            }
        });

        $eventSubscriber = new EntityWorkflowGuardSendExternalIfNoPublicView(
            $registry,
            new EntityWorkflowManager([$handler], $registry)
        );
        $eventDispatcher->addSubscriber($eventSubscriber);

        return $registry;
    }
}
