<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow\Helper;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowMarkingStore;
use Chill\MainBundle\Workflow\Helper\OpenedEntityWorkflowHelper;
use Chill\MainBundle\Workflow\WorkflowTransitionContextDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Metadata\InMemoryMetadataStore;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class OpenedEntityWorkflowHelperTest extends TestCase
{
    private function buildRegistry(): Registry
    {
        $builder = new DefinitionBuilder();
        $builder
            ->setInitialPlaces('initial')
            ->addPlaces(['initial', 'final_positive', 'final_negative', 'final_no_data'])
            ->setMetadataStore(
                new InMemoryMetadataStore(
                    placesMetadata: [
                        'final_positive' => ['isFinalPositive' => true, 'isFinal' => true],
                        'final_negative' => ['isFinalPositive' => false, 'isFinal' => true],
                        'final_no_data' => ['isFinal' => true],
                    ]
                )
            );

        $workflow = new Workflow($builder->build(), new EntityWorkflowMarkingStore(), name: 'dummy');
        $registry = new Registry();
        $registry->addWorkflow(
            $workflow,
            new class () implements WorkflowSupportStrategyInterface {
                public function supports(WorkflowInterface $workflow, object $subject): bool
                {
                    return true;
                }
            }
        );

        return $registry;
    }

    public function testIsFinalPositive(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');
        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $entityWorkflow->setStep('final_positive', $dto, 'fake', new \DateTimeImmutable());
        $entityWorkflow->getCurrentStep()->setIsFinal(true);

        $helper = new OpenedEntityWorkflowHelper($this->buildRegistry());

        self::assertTrue($helper->isFinalPositive($entityWorkflow));
        self::assertFalse($helper->isFinalNegative($entityWorkflow));
    }

    public function testIsFinalNegative(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');
        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $entityWorkflow->setStep('final_negative', $dto, 'fake', new \DateTimeImmutable());
        $entityWorkflow->getCurrentStep()->setIsFinal(true);

        $helper = new OpenedEntityWorkflowHelper($this->buildRegistry());

        self::assertFalse($helper->isFinalPositive($entityWorkflow));
        self::assertTrue($helper->isFinalNegative($entityWorkflow));
    }

    public function testIsFinalNoInformation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');
        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $entityWorkflow->setStep('final_no_data', $dto, 'fake', new \DateTimeImmutable());
        $entityWorkflow->getCurrentStep()->setIsFinal(true);

        $helper = new OpenedEntityWorkflowHelper($this->buildRegistry());

        self::assertFalse($helper->isFinalPositive($entityWorkflow));
        self::assertFalse($helper->isFinalNegative($entityWorkflow));
    }
}
