<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow\Validator;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\Validator\TransitionHasDestineeIfIsSentExternal;
use Chill\MainBundle\Workflow\Validator\TransitionHasDestineeIfIsSentExternalValidator;
use Chill\MainBundle\Workflow\WorkflowTransitionContextDTO;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Metadata\InMemoryMetadataStore;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class TransitionHasDestineeIfIsSentExternalValidatorTest extends ConstraintValidatorTestCase
{
    private Transition $transitionToSent;
    private Transition $transitionToNotSent;

    private function buildRegistry(): Registry
    {
        $builder = new DefinitionBuilder(
            ['initial', 'sent', 'notSent'],
            [
                $this->transitionToSent = new Transition('send', 'initial', 'sent'),
                $this->transitionToNotSent = new Transition('notSend', 'initial', 'notSent'),
            ]
        );
        $builder
            ->setInitialPlaces('initial')
            ->setMetadataStore(new InMemoryMetadataStore(
                placesMetadata: [
                    'sent' => ['isSentExternal' => true],
                ]
            ))
        ;

        $workflow = new Workflow($builder->build(), name: 'dummy');
        $registry = new Registry();
        $registry->addWorkflow($workflow, new class () implements WorkflowSupportStrategyInterface {
            public function supports(WorkflowInterface $workflow, object $subject): bool
            {
                return true;
            }
        });

        return $registry;
    }

    public function testToSentPlaceWithoutDestineeAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToSent;

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::buildViolation('validation_message')
            ->setCode('d78ea142-819d-11ef-a459-b7009a3e4caf')
            ->atPath('property.path.futureDestineeThirdParties')
            ->assertRaised();
    }

    public function testToSentPlaceWithDestineeThirdPartyDoesNotAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToSent;
        $dto->futureDestineeThirdParties = [new ThirdParty()];

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::assertNoViolation();
    }

    public function testToSentPlaceWithDestineeEmailDoesNotAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToSent;
        $dto->futureDestineeEmails = ['test@example.com'];

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::assertNoViolation();
    }

    public function testToNoSentPlaceWithNoDestineesDoesNotAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToNotSent;

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::assertNoViolation();
    }

    public function testToNoSentPlaceWithDestineeThirdPartyAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToNotSent;
        $dto->futureDestineeThirdParties = [new ThirdParty()];

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::buildViolation('validation_message')
            ->atPath('property.path.futureDestineeThirdParties')
            ->setCode('eb8051fc-8227-11ef-8c3b-7f2de85bdc5b')
            ->assertRaised();
    }

    public function testToNoSentPlaceWithDestineeEmailAddViolation(): void
    {
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow->setWorkflowName('dummy');

        $dto = new WorkflowTransitionContextDTO($entityWorkflow);
        $dto->transition = $this->transitionToNotSent;
        $dto->futureDestineeEmails = ['test@example.com'];

        $constraint = new TransitionHasDestineeIfIsSentExternal();
        $constraint->messageDestineeRequired = 'validation_message';

        $this->validator->validate($dto, $constraint);

        self::buildViolation('validation_message')
            ->atPath('property.path.futureDestineeEmails')
            ->setCode('eb8051fc-8227-11ef-8c3b-7f2de85bdc5b')
            ->assertRaised();
    }

    protected function createValidator(): TransitionHasDestineeIfIsSentExternalValidator
    {
        return new TransitionHasDestineeIfIsSentExternalValidator($this->buildRegistry());
    }
}
