<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Workflow\Registry;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowManagerTest extends TestCase
{
    public function testGetSuggestedUsers()
    {
        $user1 = new User();
        $user2 = new User();
        $entityWorkflow = $this->createMock(EntityWorkflow::class);
        $entityWorkflow->method('getUsersInvolved')->willReturn([$user1, $user2]);

        $user3 = new User();
        $handler = $this->createMock(EntityWorkflowHandlerInterface::class);
        $handler->method('getSuggestedUsers')->willReturn([$user1, $user3]);
        $handler->method('supports')->willReturn(true);

        $manager = new EntityWorkflowManager([$handler], new Registry());

        $users = $manager->getSuggestedUsers($entityWorkflow);

        self::assertcount(3, $users);
        self::assertContains($user1, $users);
        self::assertContains($user2, $users);
        self::assertContains($user3, $users);
    }
}
