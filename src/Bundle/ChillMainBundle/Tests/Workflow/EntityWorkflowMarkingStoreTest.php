<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowMarkingStore;
use Chill\MainBundle\Workflow\WorkflowTransitionContextDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Workflow\Marking;

/**
 * @internal
 *
 * @coversNothing
 */
class EntityWorkflowMarkingStoreTest extends TestCase
{
    public function testGetMarking(): void
    {
        $markingStore = $this->buildMarkingStore();
        $workflow = new EntityWorkflow();

        $marking = $markingStore->getMarking($workflow);

        self::assertEquals(['initial' => 1], $marking->getPlaces());
    }

    public function testSetMarking(): void
    {
        $markingStore = $this->buildMarkingStore();
        $workflow = new EntityWorkflow();
        $previousStep = $workflow->getCurrentStep();

        $dto = new WorkflowTransitionContextDTO($workflow);
        $dto->futureCcUsers[] = $user1 = new User();
        $dto->futureDestUsers[] = $user2 = new User();

        $markingStore->setMarking($workflow, new Marking(['foo' => 1]), [
            'context' => $dto,
            'transition' => 'bar_transition',
            'byUser' => $user3 = new User(),
            'transitionAt' => $at = new \DateTimeImmutable(),
        ]);

        $currentStep = $workflow->getCurrentStep();
        self::assertEquals('foo', $currentStep->getCurrentStep());
        self::assertContains($user1, $currentStep->getCcUser());
        self::assertContains($user2, $currentStep->getDestUser());

        self::assertSame($user3, $previousStep->getTransitionBy());
        self::assertSame($at, $previousStep->getTransitionAt());
        self::assertEquals('bar_transition', $previousStep->getTransitionAfter());
    }

    private function buildMarkingStore(): EntityWorkflowMarkingStore
    {
        return new EntityWorkflowMarkingStore();
    }
}
