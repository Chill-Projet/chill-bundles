<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Gender;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Gender>
 */
class GenderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gender::class);
    }

    public function findByActiveOrdered(): array
    {
        return $this->createQueryBuilder('g')
            ->select('g')
            ->where('g.active = True')
            ->orderBy('g.order', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByGenderTranslation($gender): array
    {
        return $this->createQueryBuilder('g')
            ->select('g')
            ->where('g.genderTranslation = :gender')
            ->setParameter('gender', $gender)
            ->getQuery()
            ->getResult();
    }
}
