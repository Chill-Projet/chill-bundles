<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowStepSignature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Chill\PersonBundle\Entity\Person;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends ServiceEntityRepository<EntityWorkflowStepSignature>
 */
class EntityWorkflowStepSignatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntityWorkflowStepSignature::class);
    }

    public function findByPerson(Person $person): array
    {
        $qb = $this->createQueryBuilder('ewss');
        $qb->select('ewss');
        $qb->where($qb->expr()->eq('ewss.personSigner', ':person'));

        $qb->setParameter('person', $person);

        return $qb->getQuery()->getResult();
    }
}
