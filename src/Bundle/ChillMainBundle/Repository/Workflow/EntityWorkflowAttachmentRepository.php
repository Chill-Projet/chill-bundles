<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository\Workflow;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Entity\Workflow\EntityWorkflowAttachment;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

/**
 * @implements ObjectRepository<EntityWorkflowAttachment>
 */
class EntityWorkflowAttachmentRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $registry)
    {
        $this->repository = $registry->getRepository(EntityWorkflowAttachment::class);
    }

    public function find($id): ?EntityWorkflowAttachment
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return array<EntityWorkflowAttachment>
     */
    public function findByStoredObject(StoredObject $storedObject): array
    {
        $qb = $this->repository->createQueryBuilder('a');
        $qb->where('a.proxyStoredObject = :storedObject')->setParameter('storedObject', $storedObject);

        return $qb->getQuery()->getResult();
    }

    public function getClassName()
    {
        return EntityWorkflowAttachment::class;
    }
}
