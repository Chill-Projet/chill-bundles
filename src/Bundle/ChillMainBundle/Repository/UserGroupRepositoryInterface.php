<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserGroup;
use Chill\MainBundle\Search\SearchApiQuery;
use Doctrine\Persistence\ObjectRepository;

/**
 * @template-extends ObjectRepository<UserGroup>
 */
interface UserGroupRepositoryInterface extends ObjectRepository
{
    /**
     * Provide a SearchApiQuery for searching amongst user groups.
     */
    public function provideSearchApiQuery(string $pattern, string $lang, string $selectKey = 'user-group'): SearchApiQuery;

    public function findByUser(User $user, bool $onlyActive = true, ?int $limit = null, ?int $offset = null): array;

    public function countByUser(User $user, bool $onlyActive = true): int;
}
