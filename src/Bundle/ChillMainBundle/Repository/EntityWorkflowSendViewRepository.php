<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowSendView;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;

/**
 * @template-implements ObjectRepository<EntityWorkflowSendView>
 */
class EntityWorkflowSendViewRepository implements ObjectRepository
{
    private readonly ObjectRepository $repository;

    public function __construct(ManagerRegistry $registry)
    {
        $this->repository = $registry->getRepository($this->getClassName());
    }

    public function find($id): ?EntityWorkflowSendView
    {
        return $this->repository->find($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?EntityWorkflowSendView
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return EntityWorkflowSendView::class;
    }
}
