<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

readonly class UserJobRepository implements UserJobRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em, private TranslatableStringHelperInterface $translatableStringHelper)
    {
        $this->repository = $em->getRepository(UserJob::class);
    }

    public function find($id): ?UserJob
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|UserJob[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllActive(): array
    {
        $jobs = $this->repository->findBy(['active' => true]);

        usort($jobs, fn (UserJob $a, UserJob $b) => $this->translatableStringHelper->localize($a->getLabel()) <=> $this->translatableStringHelper->localize($b->getLabel()));

        return $jobs;
    }

    public function findAllOrderedByName(): array
    {
        $jobs = $this->findAll();

        usort($jobs, fn (UserJob $a, UserJob $b) => $this->translatableStringHelper->localize($a->getLabel()) <=> $this->translatableStringHelper->localize($b->getLabel()));

        return $jobs;
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array|object[]|UserJob[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?UserJob
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return UserJob::class;
    }
}
