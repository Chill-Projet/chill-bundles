<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\GeographicalUnitLayer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final readonly class GeographicalUnitLayerLayerRepository implements GeographicalUnitLayerRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository($this->getClassName());
    }

    public function find($id): ?GeographicalUnitLayer
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|GeographicalUnitLayer[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllHavingUnits(): array
    {
        $qb = $this->repository->createQueryBuilder('l');

        return $qb->where($qb->expr()->gt('SIZE(l.units)', 0))
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array|GeographicalUnitLayer[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?GeographicalUnitLayer
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return GeographicalUnitLayer::class;
    }
}
