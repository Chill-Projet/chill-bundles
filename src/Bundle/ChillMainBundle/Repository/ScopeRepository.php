<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

final readonly class ScopeRepository implements ScopeRepositoryInterface
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, private TranslatableStringHelperInterface $translatableStringHelper)
    {
        $this->repository = $entityManager->getRepository(Scope::class);
    }

    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        return $this->repository->createQueryBuilder($alias, $indexBy);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?Scope
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    /**
     * @return Scope[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findAllActive(): array
    {
        $scopes = $this->repository->findBy(['active' => true]);

        usort($scopes, fn (Scope $a, Scope $b) => $this->translatableStringHelper->localize($a->getName()) <=> $this->translatableStringHelper->localize($b->getName()));

        return $scopes;
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return Scope[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?Scope
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    public function getClassName(): string
    {
        return Scope::class;
    }
}
