<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;

interface UserACLAwareRepositoryInterface
{
    /**
     * Find the users reaching the given center and scope, for the given role.
     *
     * @param array|Center|Center[]|null $center
     * @param array|Scope|Scope[]|null   $scope
     * @param bool                       $onlyActive true if get only active users
     *
     * @return User[]
     */
    public function findUsersByReachedACL(string $role, $center, $scope = null, bool $onlyActive = true): array;
}
