<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\User;
use Doctrine\Persistence\ObjectRepository;

/**
 * @template ObjectRepository<User>
 */
interface UserRepositoryInterface extends ObjectRepository
{
    public function countBy(array $criteria): int;

    public function countByActive(): int;

    public function countByNotHavingAttribute(string $key): int;

    public function countByUsernameOrEmail(string $pattern): int;

    /**
     * Find a list of all users.
     *
     * The main purpose for this method is to provide a lightweight list of all users in the database.
     *
     * @param string $lang The lang to display all the translatable string (no fallback if not present)
     *
     * @return iterable<array{id: int, username: string, email: string, enabled: bool, civility_id: int, civility_abbreviation: string, civility_name: string, label: string, mainCenter_id: int, mainCenter_name: string, mainScope_id: int, mainScope_name: string, userJob_id: int, userJob_name: string, currentLocation_id: int, currentLocation_name: string, mainLocation_id: int, mainLocation_name: string, absenceStart: \DateTimeImmutable}>
     */
    public function findAllAsArray(string $lang): iterable;

    /**
     * Find a list of permissions associated to each users.
     *
     * The main purpose for this method is to provide a lightweight list of all permissions group and center
     * associated to each user.
     *
     * @return iterable<array{id: int, username: string, email: string, enabled: bool, center_id: int, center_name: string, permissionsGroup_id: int, permissionsGroup_name: string}>
     */
    public function findAllUserACLAsArray(): iterable;

    /**
     * @return array|User[]
     */
    public function findByActive(?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * Find users which does not have a key on attribute column.
     *
     * @return array|User[]
     */
    public function findByNotHavingAttribute(string $key, ?int $limit = null, ?int $offset = null): array;

    public function findByUsernameOrEmail(string $pattern, ?array $orderBy = [], ?int $limit = null, ?int $offset = null): array;

    public function findOneByUsernameOrEmail(string $pattern): ?User;

    /**
     * Get the users having a specific flags.
     *
     * If provided, only the users amongst "filtered users" are searched. This
     * allows to make a first search amongst users based on role and center
     * and, then filter those users having some flags.
     *
     * @param User[] $amongstUsers
     */
    public function findUsersHavingFlags(mixed $flag, array $amongstUsers = []): array;
}
