<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Interface for generating URLs with returnPath information.
 */
interface ChillUrlGeneratorInterface
{
    /**
     * Generate an URL without any return path. This is the same as using @see{UrlGeneratorInterface::generate}.
     */
    public function generate(string $name, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string;

    /**
     * Generate an url, append a return path in it.
     */
    public function generateWithReturnPath(string $name, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string;

    /**
     * Get the return path or, if any, generate an url.
     */
    public function returnPathOr(string $name, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string;

    /**
     * Return a new URL, with the same return path as the existing one. If any, no return path is forwarded.
     */
    public function forwardReturnPath(string $name, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string;
}
