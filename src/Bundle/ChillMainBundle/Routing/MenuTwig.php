<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Add the filter 'chill_menu'.
 */
class MenuTwig extends AbstractExtension
{
    /**
     * the default parameters for chillMenu.
     *
     * @var mixed[]
     */
    private array $defaultParams = [
        'layout' => '@ChillMain/Menu/defaultMenu.html.twig',
        'args' => [],
        'activeRouteKey' => null,
    ];

    public function __construct(private readonly MenuComposer $menuComposer) {}

    /**
     * Render a Menu corresponding to $menuId.
     *
     * Expected params :
     * - args: the arguments to build the path (i.e: if pattern is /something/{bar}, args must contain {'bar': 'foo'}
     * - layout: the layout. Absolute path needed (i.e.: @ChillXyz/section/foo.html.twig)
     * - activeRouteKey : the key active, will render the menu differently.
     *
     * @deprecated link: see https://redmine.champs-libres.coop/issues/179 for more informations
     *
     * @param string  $menuId
     * @param mixed[] $params
     */
    public function chillMenu(Environment $env, $menuId, array $params = [])
    {
        $resolvedParams = array_merge($this->defaultParams, $params);

        $layout = $resolvedParams['layout'];
        unset($resolvedParams['layout']);

        if (false === $this->menuComposer->hasLocalMenuBuilder($menuId)) {
            $resolvedParams['routes'] = $this->menuComposer->getRoutesFor($menuId, $resolvedParams);

            return $env->render($layout, $resolvedParams);
        }
        $resolvedParams['menus'] = $this->menuComposer->getMenuFor($menuId, $resolvedParams);

        return $env->render($layout, $resolvedParams);
    }

    public function getFunctions()
    {
        return [new TwigFunction(
            'chill_menu',
            $this->chillMenu(...),
            [
                'is_safe' => ['html'],
                'needs_environment' => true,
            ]
        ),
        ];
    }

    public function getName()
    {
        return 'chill_menu';
    }
}
