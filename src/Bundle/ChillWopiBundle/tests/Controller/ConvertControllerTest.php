<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Tests\Controller;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\DocStoreBundle\Service\StoredObjectManagerInterface;
use Chill\WopiBundle\Controller\ConvertController;
use Chill\WopiBundle\Service\WopiConverter;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 *
 * @coversNothing
 */
final class ConvertControllerTest extends TestCase
{
    use ProphecyTrait;

    public function testConversionFailed(): void
    {
        $storedObject = new StoredObject();
        $storedObject->registerVersion(type: 'application/vnd.oasis.opendocument.text');

        $security = $this->prophesize(Security::class);
        $security->isGranted('ROLE_USER')->willReturn(true);
        $security->isGranted(StoredObjectRoleEnum::SEE->value, $storedObject)->willReturn(true);

        $storeManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storeManager->read($storedObject)->willReturn('content');

        $wopiConverter = $this->prophesize(WopiConverter::class);
        $wopiConverter->convert('fr', 'content', 'application/vnd.oasis.opendocument.text')
            ->willThrow(new \RuntimeException());

        $controller = new ConvertController(
            $security->reveal(),
            $storeManager->reveal(),
            $wopiConverter->reveal(),
            new NullLogger(),
        );

        $request = new Request();
        $request->setLocale('fr');

        $response = $controller($storedObject, $request);

        $this->assertNotEquals(200, $response->getStatusCode());
    }

    public function testEverythingWentFine(): void
    {
        $storedObject = new StoredObject();
        $storedObject->registerVersion(type: 'application/vnd.oasis.opendocument.text');

        $security = $this->prophesize(Security::class);
        $security->isGranted('ROLE_USER')->willReturn(true);
        $security->isGranted(StoredObjectRoleEnum::SEE->value, $storedObject)->willReturn(true);

        $storeManager = $this->prophesize(StoredObjectManagerInterface::class);
        $storeManager->read($storedObject)->willReturn('content');

        $wopiConverter = $this->prophesize(WopiConverter::class);
        $wopiConverter->convert('fr', 'content', 'application/vnd.oasis.opendocument.text')
            ->willReturn('1234');

        $controller = new ConvertController(
            $security->reveal(),
            $storeManager->reveal(),
            $wopiConverter->reveal(),
            new NullLogger(),
        );

        $request = new Request();
        $request->setLocale('fr');

        $response = $controller($storedObject, $request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('1234', $response->getContent());
    }
}
