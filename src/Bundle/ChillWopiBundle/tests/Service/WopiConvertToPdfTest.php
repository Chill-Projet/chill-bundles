<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Tests\Service;

use Chill\WopiBundle\Service\WopiConverter;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

/**
 * @internal
 *
 * @coversNothing
 */
class WopiConvertToPdfTest extends TestCase
{
    /**
     * @group collabora-integration
     */
    public function testConvertToPdfWithRealServer(): void
    {
        $content = file_get_contents(__DIR__.'/fixtures/test-document.odt');

        $client = HttpClient::create();
        $parameters = new ParameterBag([
            'wopi' => ['server' => $_ENV['EDITOR_SERVER']],
        ]);

        $converter = new WopiConverter($client, new NullLogger(), $parameters);

        $actual = $converter->convert('fr', $content, 'application/vnd.oasis.opendocument.text');

        self::assertIsString($actual);
    }

    public function testConvertToPdfWithMock(): void
    {
        $httpClient = new MockHttpClient([
            new MockResponse('1234', ['http_code' => 200]),
        ], 'http://collabora:9980');
        $parameters = new ParameterBag([
            'wopi' => ['server' => 'http://collabora:9980'],
        ]);

        $converter = new WopiConverter($httpClient, new NullLogger(), $parameters);

        $actual = $converter->convert('fr', 'content-string', 'application/vnd.oasis.opendocument.text');

        self::assertEquals('1234', $actual);
    }
}
