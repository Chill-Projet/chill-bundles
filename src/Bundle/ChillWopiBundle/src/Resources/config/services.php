<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use ChampsLibres\WopiBundle\Contracts\AuthorizationManagerInterface;
use ChampsLibres\WopiBundle\Contracts\UserManagerInterface;
use ChampsLibres\WopiBundle\Service\Wopi as CLWopi;
use ChampsLibres\WopiLib\Contract\Service\DocumentLockManagerInterface;
use ChampsLibres\WopiLib\Contract\Service\DocumentManagerInterface;
use Chill\WopiBundle\Service\Wopi\AuthorizationManager;
use Chill\WopiBundle\Service\Wopi\ChillDocumentLockManager;
use Chill\WopiBundle\Service\Wopi\ChillDocumentManager;
use Chill\WopiBundle\Service\Wopi\ChillWopi;
use Chill\WopiBundle\Service\Wopi\UserManager;

return static function (ContainerConfigurator $container) {
    $services = $container
        ->services();

    $services
        ->defaults()
        ->autowire()
        ->autoconfigure();

    $services
        ->load('Chill\WopiBundle\Service\\', __DIR__.'/../../Service');

    $services
        ->load('Chill\WopiBundle\Controller\\', __DIR__.'/../../Controller')
        ->tag('controller.service_arguments');

    $services
        ->set(ChillWopi::class)
        ->decorate(CLWopi::class)
        ->arg('$wopi', service('.inner'));

    $services
        ->alias(DocumentManagerInterface::class, ChillDocumentManager::class);

    $services
        ->set(ChillDocumentLockManager::class)
        ->decorate(DocumentLockManagerInterface::class);

    $services
        ->set(AuthorizationManager::class);

    $services->alias(AuthorizationManagerInterface::class, AuthorizationManager::class);

    $services
        ->set(UserManager::class);

    $services->alias(UserManagerInterface::class, UserManager::class);
};
