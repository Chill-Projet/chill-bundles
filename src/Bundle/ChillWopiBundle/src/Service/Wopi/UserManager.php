<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Service\Wopi;

use Chill\MainBundle\Entity\User;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Security\Core\Security;

class UserManager implements \ChampsLibres\WopiBundle\Contracts\UserManagerInterface
{
    public function __construct(private readonly Security $security) {}

    public function getUserFriendlyName(string $accessToken, string $fileId, RequestInterface $request): ?string
    {
        $user = $this->security->getUser();

        if (!$user instanceof User && $this->security->isGranted('ROLE_ADMIN')) {
            return $user->getUsername();
        }

        if (!$user instanceof User) {
            return null;
        }

        return $user->getLabel();
    }

    public function getUserId(string $accessToken, string $fileId, RequestInterface $request): ?string
    {
        $user = $this->security->getUser();

        if (!$user instanceof User && $this->security->isGranted('ROLE_ADMIN')) {
            return $user->getUsername();
        }

        if (!$user instanceof User) {
            return null;
        }

        return (string) $user->getId();
    }

    public function isAnonymousUser(string $accessToken, string $fileId, RequestInterface $request): bool
    {
        return false;
    }
}
