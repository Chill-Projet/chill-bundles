<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Service\Wopi;

use ChampsLibres\WopiLib\Contract\Service\ProofValidatorInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Create a proof validator for WOPI services which always return true.
 *
 * Useful for debugging purposes.
 */
final readonly class NullProofValidator implements ProofValidatorInterface
{
    public function isValid(RequestInterface $request): bool
    {
        return true;
    }
}
