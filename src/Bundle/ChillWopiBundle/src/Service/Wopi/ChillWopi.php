<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\WopiBundle\Service\Wopi;

use ChampsLibres\WopiLib\Contract\Service\WopiInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final readonly class ChillWopi implements WopiInterface
{
    public function __construct(private WopiInterface $wopi) {}

    public function checkFileInfo(
        string $fileId,
        ?string $accessToken,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->checkFileInfo($fileId, $accessToken, $request, [
            'SupportsRename' => false,
        ]);
    }

    public function deleteFile(string $fileId, ?string $accessToken, RequestInterface $request): ResponseInterface
    {
        return $this->wopi->deleteFile($fileId, $accessToken, $request);
    }

    public function enumerateAncestors(
        string $fileId,
        ?string $accessToken,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->enumerateAncestors($fileId, $accessToken, $request);
    }

    public function getFile(string $fileId, ?string $accessToken, RequestInterface $request): ResponseInterface
    {
        return $this->wopi->getFile($fileId, $accessToken, $request);
    }

    public function getLock(string $fileId, ?string $accessToken, RequestInterface $request): ResponseInterface
    {
        return $this->wopi->getLock($fileId, $accessToken, $request);
    }

    public function getShareUrl(
        string $fileId,
        ?string $accessToken,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->getShareUrl($fileId, $accessToken, $request);
    }

    public function lock(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->lock($fileId, $accessToken, $xWopiLock, $request);
    }

    public function putFile(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        string $xWopiEditors,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->putFile($fileId, $accessToken, $xWopiLock, $xWopiEditors, $request);
    }

    public function putRelativeFile(string $fileId, string $accessToken, ?string $suggestedTarget, ?string $relativeTarget, bool $overwriteRelativeTarget, int $size, RequestInterface $request): ResponseInterface
    {
        return $this->wopi->putRelativeFile($fileId, $accessToken, $suggestedTarget, $relativeTarget, $overwriteRelativeTarget, $size, $request);
    }

    public function putUserInfo(string $fileId, ?string $accessToken, RequestInterface $request): ResponseInterface
    {
        return $this->wopi->putUserInfo($fileId, $accessToken, $request);
    }

    public function refreshLock(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->refreshLock($fileId, $accessToken, $xWopiLock, $request);
    }

    public function renameFile(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        string $xWopiRequestedName,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->renameFile($fileId, $accessToken, $xWopiLock, $xWopiRequestedName, $request);
    }

    public function unlock(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->unlock($fileId, $accessToken, $xWopiLock, $request);
    }

    public function unlockAndRelock(
        string $fileId,
        ?string $accessToken,
        string $xWopiLock,
        string $xWopiOldLock,
        RequestInterface $request,
    ): ResponseInterface {
        return $this->wopi->unlockAndRelock($fileId, $accessToken, $xWopiLock, $xWopiOldLock, $request);
    }
}
