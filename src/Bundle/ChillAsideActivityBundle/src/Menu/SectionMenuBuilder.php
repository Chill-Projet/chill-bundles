<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SectionMenuBuilder.
 */
class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(protected TranslatorInterface $translator, public AuthorizationCheckerInterface $authorizationChecker) {}

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $menu->addChild($this->translator->trans('Create an aside activity'), [
                'route' => 'chill_crud_aside_activity_new',
            ])
                ->setExtras([
                    'order' => 11,
                    'icons' => ['plus'],
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
