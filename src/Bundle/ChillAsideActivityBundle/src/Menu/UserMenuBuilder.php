<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\TaskBundle\Templating\UI\CountNotificationTask;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    public $authorizationChecker;

    /**
     * @var CountNotificationTask
     */
    public $counter;

    /**
     * @var TokenStorageInterface
     */
    public $tokenStorage;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    public function __construct(
        CountNotificationTask $counter,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
    ) {
        $this->counter = $counter;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $menu->addChild('My aside activities', [
                'route' => 'chill_crud_aside_activity_index',
            ])
                ->setExtras([
                    'order' => 10,
                    'icon' => 'tasks',
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['user'];
    }
}
