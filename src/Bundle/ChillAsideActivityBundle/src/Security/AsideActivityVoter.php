<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Security;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\VoterHelperFactoryInterface;
use Chill\MainBundle\Security\Authorization\VoterHelperInterface;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AsideActivityVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    final public const STATS = 'CHILL_ASIDE_ACTIVITY_STATS';

    private readonly VoterHelperInterface $voterHelper;

    public function __construct(
        VoterHelperFactoryInterface $voterHelperFactory,
    ) {
        $this->voterHelper = $voterHelperFactory
            ->generate(self::class)
            ->addCheckFor(Center::class, [self::STATS])
            ->build();
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->getAttributes();
    }

    /**
     * @return string[][]
     */
    public function getRolesWithHierarchy(): array
    {
        return ['Aside activity' => $this->getRoles()];
    }

    /**
     * @return string[]
     */
    public function getRolesWithoutScope(): array
    {
        return $this->getAttributes();
    }

    protected function supports($attribute, $subject)
    {
        return $this->voterHelper->supports($attribute, $subject);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }

        return $this->voterHelper->voteOnAttribute($attribute, $subject, $token);
    }

    private function getAttributes(): array
    {
        return [self::STATS];
    }
}
