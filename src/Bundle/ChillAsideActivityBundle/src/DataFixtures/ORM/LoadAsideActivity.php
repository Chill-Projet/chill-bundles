<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\DataFixtures\ORM;

use Chill\AsideActivityBundle\Entity\AsideActivity;
use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Chill\MainBundle\DataFixtures\ORM\LoadUsers;
use Chill\MainBundle\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadAsideActivity extends Fixture implements DependentFixtureInterface
{
    public function __construct(private readonly UserRepository $userRepository) {}

    public function getDependencies(): array
    {
        return [
            LoadUsers::class,
            LoadAsideActivityCategory::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $user = $this->userRepository->findOneBy(['username' => 'center a_social']);

        for ($i = 0; 50 > $i; ++$i) {
            $activity = new AsideActivity();
            $activity
                ->setAgent($user)
                ->setCreatedAt(new \DateTimeImmutable('now'))
                ->setCreatedBy($user)
                ->setUpdatedAt(new \DateTimeImmutable('now'))
                ->setUpdatedBy($user)
                ->setType(
                    $this->getReference('aside_activity_category_0', AsideActivityCategory::class)
                )
                ->setDate((new \DateTimeImmutable('today'))
                    ->sub(new \DateInterval('P'.\random_int(1, 100).'D')));

            $manager->persist($activity);
        }

        $manager->flush();
    }
}
