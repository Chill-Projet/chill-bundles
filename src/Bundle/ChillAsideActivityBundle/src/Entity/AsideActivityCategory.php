<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity]
#[ORM\Table(schema: 'chill_asideactivity')]
class AsideActivityCategory
{
    /**
     * @var Collection<int, AsideActivityCategory>
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: AsideActivityCategory::class)]
    private Collection $children;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::INTEGER)]
    private int $id;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::BOOLEAN)]
    private bool $isActive = true;

    private AsideActivityCategory $oldParent;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::FLOAT, options: ['default' => '0.00'])]
    private float $ordering = 0.00;

    #[ORM\ManyToOne(targetEntity: AsideActivityCategory::class, inversedBy: 'children')]
    #[ORM\JoinColumn(nullable: true)]
    private ?AsideActivityCategory $parent = null;

    #[ORM\Column(type: \Doctrine\DBAL\Types\Types::JSON, length: 255)]
    private array $title;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function hasParent(): bool
    {
        return null !== $this->parent;
    }

    #[Assert\Callback]
    public function preventRecursiveParent(ExecutionContextInterface $context, mixed $payload)
    {
        if (!$this->hasParent()) {
            return;
        }

        if ($this->getParent() === $this) {
            // replace parent with old parent. This prevent recursive loop
            // when displaying form
            $this->parent = $this->oldParent;
            $context->buildViolation('You must not add twice the same category in the parent tree (previous result returned)')
                ->atPath('parent')
                ->addViolation();
        }
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setOrdering(float $ordering): AsideActivityCategory
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function setParent(?self $parent): self
    {
        // cache the old result for changing it during validaiton
        if ($this->parent) {
            $this->oldParent = $this->parent;
        }

        $this->parent = $parent;

        return $this;
    }

    public function setTitle(array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
