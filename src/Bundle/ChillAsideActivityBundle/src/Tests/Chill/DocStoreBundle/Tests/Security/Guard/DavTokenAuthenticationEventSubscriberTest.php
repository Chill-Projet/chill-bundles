<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Tests\Security\Guard;

use Chill\DocStoreBundle\Security\Authorization\StoredObjectRoleEnum;
use Chill\DocStoreBundle\Security\Guard\DavTokenAuthenticationEventSubscriber;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * @internal
 *
 * @coversNothing
 */
class DavTokenAuthenticationEventSubscriberTest extends TestCase
{
    public function testOnJWTAuthenticatedWithDavDataInPayload(): void
    {
        $eventSubscriber = new DavTokenAuthenticationEventSubscriber();
        $token = new class () extends AbstractToken {
            public function getCredentials()
            {
                return null;
            }
        };
        $event = new JWTAuthenticatedEvent([
            'dav' => 1,
            'so' => '1234',
            'e' => 1,
        ], $token);

        $eventSubscriber->onJWTAuthenticated($event);

        self::assertTrue($token->hasAttribute(DavTokenAuthenticationEventSubscriber::STORED_OBJECT));
        self::assertTrue($token->hasAttribute(DavTokenAuthenticationEventSubscriber::ACTIONS));
        self::assertEquals('1234', $token->getAttribute(DavTokenAuthenticationEventSubscriber::STORED_OBJECT));
        self::assertEquals(StoredObjectRoleEnum::EDIT, $token->getAttribute(DavTokenAuthenticationEventSubscriber::ACTIONS));
    }

    public function testOnJWTAuthenticatedWithDavNoDataInPayload(): void
    {
        $eventSubscriber = new DavTokenAuthenticationEventSubscriber();
        $token = new class () extends AbstractToken {
            public function getCredentials()
            {
                return null;
            }
        };
        $event = new JWTAuthenticatedEvent([], $token);

        $eventSubscriber->onJWTAuthenticated($event);

        self::assertFalse($token->hasAttribute(DavTokenAuthenticationEventSubscriber::STORED_OBJECT));
        self::assertFalse($token->hasAttribute(DavTokenAuthenticationEventSubscriber::ACTIONS));
    }
}
