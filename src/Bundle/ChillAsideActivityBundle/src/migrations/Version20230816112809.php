<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\AsideActivity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230816112809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update location attribute in asideactivity';
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_asideactivity.IDX_A866DA0E64D218E');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity DROP CONSTRAINT FK_A866DA0E64D218E');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity DROP location_id');
        $this->addSql('ALTER TABLE chill_asideactivity.AsideActivity ADD location VARCHAR(100) DEFAULT NULL');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity DROP location');
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivity ADD CONSTRAINT FK_A866DA0E64D218E FOREIGN KEY (location_id) REFERENCES chill_main_location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A866DA0E64D218E ON chill_asideactivity.asideactivity (location_id)');
    }
}
