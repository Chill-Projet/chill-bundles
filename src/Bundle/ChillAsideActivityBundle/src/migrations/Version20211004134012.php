<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\AsideActivity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * allow to add an ordering to aside activity categories.
 */
final class Version20211004134012 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivitycategory DROP ordering');
    }

    public function getDescription(): string
    {
        return 'allow to add an ordering to aside activity categories';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_asideactivity.asideactivitycategory ADD ordering DOUBLE PRECISION NOT NULL DEFAULT 0.00');
    }
}
