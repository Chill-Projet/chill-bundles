<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Aggregator;

use Chill\AsideActivityBundle\Export\Declarations;
use Chill\AsideActivityBundle\Repository\AsideActivityCategoryRepository;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByActivityTypeAggregator implements AggregatorInterface
{
    public function __construct(
        private readonly AsideActivityCategoryRepository $asideActivityCategoryRepository,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('IDENTITY(aside.type) AS by_aside_activity_type_aggregator')
            ->addGroupBy('by_aside_activity_type_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // No form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.Aside activity type';
            }

            if (null === $value || null === $t = $this->asideActivityCategoryRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize($t->getTitle());
        };
    }

    public function getQueryKeys($data): array
    {
        return ['by_aside_activity_type_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.Group by aside activity type';
    }
}
