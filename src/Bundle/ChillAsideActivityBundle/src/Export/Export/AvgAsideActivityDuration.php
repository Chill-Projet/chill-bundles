<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Export;

use Chill\AsideActivityBundle\Export\Declarations;
use Chill\AsideActivityBundle\Repository\AsideActivityRepository;
use Chill\AsideActivityBundle\Security\AsideActivityVoter;
use Chill\MainBundle\Export\ExportInterface;
use Chill\MainBundle\Export\FormatterInterface;
use Chill\MainBundle\Export\GroupedExportInterface;
use Doctrine\ORM\Query;
use Symfony\Component\Form\FormBuilderInterface;

class AvgAsideActivityDuration implements ExportInterface, GroupedExportInterface
{
    public function __construct(private readonly AsideActivityRepository $repository) {}

    public function buildForm(FormBuilderInterface $builder) {}

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getAllowedFormattersTypes(): array
    {
        return [FormatterInterface::TYPE_TABULAR];
    }

    public function getDescription(): string
    {
        return 'export.Average aside activities duration';
    }

    public function getGroup(): string
    {
        return 'export.Exports of aside activities';
    }

    public function getLabels($key, array $values, $data)
    {
        if ('export_avg_aside_activity_duration' !== $key) {
            throw new \LogicException("the key {$key} is not used by this export");
        }

        return static fn ($value) => '_header' === $value ? 'Average duration aside activities' : $value;
    }

    public function getQueryKeys($data): array
    {
        return ['export_avg_aside_activity_duration'];
    }

    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    public function getTitle(): string
    {
        return 'export.Average aside activities duration';
    }

    public function getType(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = [])
    {
        $qb = $this->repository->createQueryBuilder('aside');

        $qb
            ->select('AVG(aside.duration) as export_avg_aside_activity_duration')
            ->andWhere($qb->expr()->isNotNull('aside.duration'));

        return $qb;
    }

    public function requiredRole(): string
    {
        return AsideActivityVoter::STATS;
    }

    public function supportsModifiers(): array
    {
        return [Declarations::ASIDE_ACTIVITY_TYPE];
    }
}
