<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Filter;

use Chill\AsideActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ByDateFilter implements FilterInterface
{
    public function __construct(private readonly RollingDateConverterInterface $rollingDateConverter, protected TranslatorInterface $translator) {}

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $clause = $qb->expr()->between(
            'aside.date',
            ':date_from',
            ':date_to'
        );

        $qb->andWhere($clause);

        $qb->setParameter(
            'date_from',
            $this->rollingDateConverter->convert($data['date_from'])
        )->setParameter(
            'date_to',
            $this->rollingDateConverter->convert($data['date_to'])
        );
    }

    public function applyOn(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('date_from', PickRollingDateType::class, [
                'label' => 'export.filter.Aside activities after this date',
            ])
            ->add('date_to', PickRollingDateType::class, [
                'label' => 'export.filter.Aside activities before this date',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
            'date_to' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function describeAction($data, $format = 'string'): array
    {
        return ['export.filter.Filtered by aside activities between %dateFrom% and %dateTo%', [
            '%dateFrom%' => $this->rollingDateConverter->convert($data['date_from'])->format('d-m-Y'),
            '%dateTo%' => $this->rollingDateConverter->convert($data['date_to'])->format('d-m-Y'),
        ]];
    }

    public function getTitle(): string
    {
        return 'export.filter.Filter by aside activity date';
    }
}
