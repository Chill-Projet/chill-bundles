<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;

final class ChillAsideActivityExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('chill_aside_activity.form.time_duration', $config['form']['time_duration']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/form.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/export.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoute($container);
        $this->prependCruds($container);
    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => \Chill\AsideActivityBundle\Entity\AsideActivityCategory::class,
                    'name' => 'aside_activity_category',
                    'base_path' => '/admin/asideactivity/category',
                    'form_class' => \Chill\AsideActivityBundle\Form\AsideActivityCategoryType::class,
                    'controller' => \Chill\AsideActivityBundle\Controller\AsideActivityCategoryController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillAsideActivity/asideActivityCategory/index.html.twig',
                            'role' => 'ROLE_ADMIN',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillAsideActivity/asideActivityCategory/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillAsideActivity/asideActivityCategory/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => \Chill\AsideActivityBundle\Entity\AsideActivity::class,
                    'name' => 'aside_activity',
                    'base_path' => '/asideactivity',
                    'form_class' => \Chill\AsideActivityBundle\Form\AsideActivityFormType::class,
                    'controller' => \Chill\AsideActivityBundle\Controller\AsideActivityController::class,
                    'actions' => [
                        'index' => [
                            'template' => '@ChillAsideActivity/asideActivity/index.html.twig',
                            'role' => 'ROLE_USER',
                        ],
                        'new' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillAsideActivity/asideActivity/new.html.twig',
                        ],
                        'view' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillAsideActivity/asideActivity/view.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillAsideActivity/asideActivity/edit.html.twig',
                        ],
                        'delete' => [
                            'role' => 'ROLE_USER',
                            'template' => '@ChillAsideActivity/asideActivity/delete.html.twig',
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function prependRoute(ContainerBuilder $container)
    {
        // declare routes for task bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillAsideActivityBundle/config/routes.yaml',
                ],
            ],
        ]);
    }
}
