<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Event;

use Chill\TaskBundle\Entity\AbstractTask;

class TaskEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const PERSIST = 'chill_task.task_persist';

    /**
     * @var AbstractTask
     */
    protected $task;

    public function __construct(AbstractTask $task)
    {
        $this->task = $task;
    }

    public function getTask(): AbstractTask
    {
        return $this->task;
    }

    public function setTask(AbstractTask $task)
    {
        $this->task = $task;

        return $this;
    }
}
