module.exports = function (encore, entries) {
  entries.push(__dirname + "/Resources/public/chill/index.js");

  encore.addEntry(
    "page_task_list",
    __dirname + "/Resources/public/page/tile_list/index.js",
  );
};
