<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Security\Authorization;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AuthorizationEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const VOTE = 'chill_task.vote';

    /**
     * @var bool
     */
    protected $vote;

    public function __construct(
        private readonly AbstractTask|AccompanyingPeriod|Person|null $subject,
        private readonly string $attribute,
        private readonly TokenInterface $token,
    ) {}

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getToken(): TokenInterface
    {
        return $this->token;
    }

    public function getVote()
    {
        return $this->vote;
    }

    public function hasVote()
    {
        return null !== $this->vote;
    }

    public function removeVote()
    {
        $this->vote = null;
    }

    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }
}
