<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Repository;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

interface SingleTaskAclAwareRepositoryInterface
{
    public function countByAllViewable(
        ?string $pattern = null,
        ?array $flags = [],
        ?array $types = [],
        ?array $users = [],
    ): int;

    public function countByCourse(
        AccompanyingPeriod $course,
        ?string $pattern = null,
        ?array $flags = [],
    ): int;

    public function countByCurrentUsersTasks(?string $pattern = null, ?array $flags = []): int;

    public function countByPerson(
        Person $person,
        ?string $pattern = null,
        ?array $flags = [],
    ): int;

    public function findByAllViewable(
        ?string $pattern = null,
        ?array $flags = [],
        ?array $types = [],
        ?array $users = [],
        ?int $start = 0,
        ?int $limit = 50,
        ?array $orderBy = [],
    ): array;

    public function findByCourse(
        AccompanyingPeriod $course,
        ?string $pattern = null,
        ?array $flags = [],
        ?int $start = 0,
        ?int $limit = 50,
        ?array $orderBy = [],
    ): array;

    public function findByCurrentUsersTasks(?string $pattern = null, ?array $flags = [], ?int $start = 0, ?int $limit = 50, ?array $orderBy = []): array;

    public function findByPerson(
        Person $person,
        ?string $pattern = null,
        ?array $flags = [],
        ?int $start = 0,
        ?int $limit = 50,
        ?array $orderBy = [],
    ): array;
}
