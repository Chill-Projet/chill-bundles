<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Workflow\Definition;

use Chill\TaskBundle\Entity\AbstractTask;
use Chill\TaskBundle\Entity\SingleTask;
use Symfony\Component\Workflow\Transition;

class DefaultTaskDefinition implements \Chill\TaskBundle\Workflow\TaskWorkflowDefinition
{
    final public const DEFINITION_METADATA = [
        'name' => 'Default task',
    ];

    final public const TRANSITION_METADATA = [
        'close' => [
            'verb' => 'close',
            'class' => 'btn btn-task-label btn-task-close',
            'sentence' => '%user% has closed the task',
            'sentence_confirmation' => 'Are you sure you want to close this task ?',
            'apply_transition_submit_label' => 'Close_verb',
        ],
        'cancel' => [
            'verb' => 'cancel',
            'class' => 'btn btn-task-label btn-task-cancel',
            'sentence' => '%user% has canceled the task',
            'sentence_confirmation' => 'Are you sure you want to cancel this task ?',
            'apply_transition_submit_label' => 'Set this task to cancel state',
        ],
        'start' => [
            'verb' => 'start',
            'class' => 'btn btn-task-label btn-task-start',
            'sentence' => '%user% has started the task',
            'sentence_confirmation' => 'Are you sure you want to start this task ?',
            'apply_transition_submit_label' => 'Start_verb',
        ],
    ];

    public static function getAssociatedWorkflowName()
    {
        return 'task_default';
    }

    public function getWorkflowMetadata(
        AbstractTask $task,
        string $key,
        $metadataSubject = null,
    ) {
        $keys = \explode('.', $key);

        switch ($keys[0]) {
            case 'transition':
                if (!$metadataSubject instanceof Transition) {
                    throw new \LogicException('You must give a transition as metadatasubject');
                }

                return $this->getTransitionMetadata(\implode('.', \array_slice($keys, 1)), $metadataSubject);

            case 'definition':
                return self::DEFINITION_METADATA[$keys[1]] ?? $key;

            default:
                return $key;
        }
    }

    public function isClosed(AbstractTask $task)
    {
        return \array_key_exists('closed', $task->getCurrentStates())
            || \array_key_exists('canceled', $task->getCurrentStates());
    }

    public function supports(AbstractTask $task)
    {
        return $task instanceof SingleTask
            && 'task_default' === $task->getType();
    }

    protected function getTransitionMetadata($key, Transition $transition)
    {
        if (!\array_key_exists($transition->getName(), self::TRANSITION_METADATA)) {
            return $key;
        }

        if (!\array_key_exists($key, self::TRANSITION_METADATA[$transition->getName()])) {
            return $key;
        }

        return self::TRANSITION_METADATA[$transition->getName()][$key];
    }
}
