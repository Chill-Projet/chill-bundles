<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity;

use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractTask.
 */
#[Serializer\DiscriminatorMap(typeProperty: 'type', mapping: ['single_task' => SingleTask::class])]
#[ORM\MappedSuperclass]
abstract class AbstractTask implements HasCenterInterface, HasScopeInterface
{
    #[Serializer\Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $assignee = null;

    #[ORM\ManyToOne(targetEntity: Scope::class)]
    private ?Scope $circle = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(name: 'closed', type: \Doctrine\DBAL\Types\Types::BOOLEAN, options: ['default' => false])]
    private bool $closed = false;

    #[Serializer\Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: AccompanyingPeriod::class)]
    private ?AccompanyingPeriod $course = null;

    #[Serializer\Groups(['read'])]
    #[ORM\Column(name: 'current_states', type: \Doctrine\DBAL\Types\Types::JSON, options: ['jsonb' => true, 'default' => '[]'])]
    private array $currentStates = [];

    #[Serializer\Groups(['read'])]
    #[ORM\Column(name: 'description', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $description = '';

    #[Serializer\Groups(['read'])]
    #[ORM\ManyToOne(targetEntity: Person::class)]
    private ?Person $person = null;

    #[Assert\NotBlank]
    #[Serializer\Groups(['read'])]
    #[ORM\Column(name: 'title', type: \Doctrine\DBAL\Types\Types::TEXT)]
    private string $title = '';

    #[Serializer\Groups(['read'])]
    #[ORM\Column(name: 'type', type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private ?string $type = null;

    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    public function getCenter(): ?\Chill\MainBundle\Entity\Center
    {
        if ($this->getPerson() instanceof Person) {
            return $this->getPerson()->getCenter();
        }

        return $this->getCourse()->getCenter();
    }

    public function getCircle(): ?Scope
    {
        return $this->circle;
    }

    public function getContext()
    {
        return $this->getPerson() ?? $this->getCourse();
    }

    public function getCourse(): ?AccompanyingPeriod
    {
        return $this->course;
    }

    /**
     * Get currentStates.
     *
     * The states are returned as required by marking store format.
     *
     * @return array
     */
    public function getCurrentStates()
    {
        return \array_fill_keys($this->currentStates, 1);
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getScope(): ?Scope
    {
        return $this->getCircle();
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function isClosed(): bool
    {
        return $this->closed;
    }

    public function setAssignee(?User $assignee = null)
    {
        $this->assignee = $assignee;

        return $this;
    }

    public function setCircle(Scope $circle)
    {
        $this->circle = $circle;

        return $this;
    }

    public function setClosed(bool $closed)
    {
        $this->closed = $closed;
    }

    public function setCourse(AccompanyingPeriod $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Set currentStates.
     *
     * The current states are sorted in a single array, non associative.
     *
     * @return AbstractTask
     */
    public function setCurrentStates($currentStates)
    {
        $this->currentStates = \array_keys($currentStates);

        return $this;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return AbstractTask
     */
    public function setDescription($description)
    {
        $this->description = (string) $description;

        return $this;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return AbstractTask
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;

        return $this;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return AbstractTask
     */
    public function setType($type)
    {
        $this->type = (string) $type;

        return $this;
    }
}
