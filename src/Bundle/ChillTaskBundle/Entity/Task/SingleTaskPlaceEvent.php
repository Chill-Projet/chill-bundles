<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity\Task;

use Chill\TaskBundle\Entity\SingleTask;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'chill_task.single_task_place_event')]
#[ORM\Index(name: 'transition_task_date', columns: ['task_id', 'transition', 'datetime'])]
#[ORM\Index(name: 'transition_task', columns: ['task_id', 'transition'])]
class SingleTaskPlaceEvent extends AbstractTaskPlaceEvent
{
    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: SingleTask::class, inversedBy: 'taskPlaceEvents')]
    protected ?SingleTask $task = null;

    public function getTask(): SingleTask
    {
        return $this->task;
    }

    public function setTask(SingleTask $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
