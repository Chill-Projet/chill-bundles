<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity\Task;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractTaskPlaceEvent.
 */
#[ORM\MappedSuperclass]
class AbstractTaskPlaceEvent
{
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $author = null;

    #[ORM\Column(name: 'data', type: \Doctrine\DBAL\Types\Types::JSON)]
    private array $data = [];

    /**
     * @var \DateTimeImmutable
     */
    #[ORM\Column(name: 'datetime', type: \Doctrine\DBAL\Types\Types::DATETIME_IMMUTABLE)]
    private $datetime;

    #[ORM\Column(name: 'transition', type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private string $transition = '';

    public function __construct()
    {
        $this->datetime = new \DateTimeImmutable('now');
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * Get data.
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Get datetime.
     */
    public function getDatetime(): \DateTimeImmutable
    {
        return $this->datetime;
    }

    /**
     * Get transition.
     *
     * @return string
     */
    public function getTransition()
    {
        return $this->transition;
    }

    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Set data.
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set datetime.
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setDatetime(\DateTimeImmutable $datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Set transition.
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setTransition(string $transition)
    {
        $this->transition = $transition;

        return $this;
    }
}
