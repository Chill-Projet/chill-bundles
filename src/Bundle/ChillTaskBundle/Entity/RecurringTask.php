<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RecurringTask.
 */
#[ORM\Entity(repositoryClass: \Chill\TaskBundle\Repository\RecurringTaskRepository::class)]
#[ORM\Table(name: 'chill_task.recurring_task')]
class RecurringTask extends AbstractTask
{
    #[ORM\Column(name: 'first_occurence_end_date', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE)]
    private ?\DateTime $firstOccurenceEndDate = null;

    #[ORM\Column(name: 'id', type: \Doctrine\DBAL\Types\Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(name: 'last_occurence_end_date', type: \Doctrine\DBAL\Types\Types::DATE_MUTABLE)]
    private ?\DateTime $lastOccurenceEndDate = null;

    #[ORM\Column(name: 'occurence_frequency', type: \Doctrine\DBAL\Types\Types::STRING, length: 255)]
    private ?string $occurenceFrequency = null;

    #[ORM\Column(name: 'occurence_start_date', type: \Doctrine\DBAL\Types\Types::DATEINTERVAL)]
    private $occurenceStartDate;

    #[ORM\Column(name: 'occurence_warning_interval', type: \Doctrine\DBAL\Types\Types::DATEINTERVAL, nullable: true)]
    private $occurenceWarningInterval;

    /**
     * @var Collection<int, SingleTask>
     */
    #[ORM\OneToMany(mappedBy: 'recurringTask', targetEntity: SingleTask::class)]
    private Collection $singleTasks;

    public function __construct()
    {
        $this->singleTasks = new ArrayCollection();
    }

    /**
     * Get firstOccurenceEndDate.
     *
     * @return \DateTime
     */
    public function getFirstOccurenceEndDate()
    {
        return $this->firstOccurenceEndDate;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get lastOccurenceEndDate.
     *
     * @return \DateTime
     */
    public function getLastOccurenceEndDate()
    {
        return $this->lastOccurenceEndDate;
    }

    /**
     * Get occurenceFrequency.
     *
     * @return string
     */
    public function getOccurenceFrequency()
    {
        return $this->occurenceFrequency;
    }

    /**
     * Get occurenceStartDate.
     */
    public function getOccurenceStartDate()
    {
        return $this->occurenceStartDate;
    }

    /**
     * Get occurenceWarningInterval.
     */
    public function getOccurenceWarningInterval()
    {
        return $this->occurenceWarningInterval;
    }

    /**
     * Set firstOccurenceEndDate.
     *
     * @return RecurringTask
     */
    public function setFirstOccurenceEndDate(?\DateTime $firstOccurenceEndDate)
    {
        $this->firstOccurenceEndDate = $firstOccurenceEndDate;

        return $this;
    }

    /**
     * Set lastOccurenceEndDate.
     *
     * @return RecurringTask
     */
    public function setLastOccurenceEndDate(?\DateTime $lastOccurenceEndDate)
    {
        $this->lastOccurenceEndDate = $lastOccurenceEndDate;

        return $this;
    }

    /**
     * Set occurenceFrequency.
     *
     * @return RecurringTask
     */
    public function setOccurenceFrequency(?string $occurenceFrequency)
    {
        $this->occurenceFrequency = $occurenceFrequency;

        return $this;
    }

    /**
     * Set occurenceStartDate.
     *
     * @return RecurringTask
     */
    public function setOccurenceStartDate(mixed $occurenceStartDate)
    {
        $this->occurenceStartDate = $occurenceStartDate;

        return $this;
    }

    /**
     * Set occurenceWarningInterval.
     *
     * @return RecurringTask
     */
    public function setOccurenceWarningInterval(mixed $occurenceWarningInterval)
    {
        $this->occurenceWarningInterval = $occurenceWarningInterval;

        return $this;
    }
}
