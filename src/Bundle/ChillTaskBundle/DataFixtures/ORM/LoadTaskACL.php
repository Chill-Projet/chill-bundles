<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\DataFixtures\ORM;

use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Add a role UPDATE & CREATE for all groups except administrative,
 * and a role SEE for administrative.
 */
class LoadTaskACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 16000;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef, PermissionsGroup::class);

            foreach (LoadScopes::$references as $scopeRef) {
                $scope = $this->getReference($scopeRef, Scope::class);
                // create permission group
                switch ($permissionsGroup->getName()) {
                    case 'social':
                        if ('administrative' === $scope->getName()['en']) {
                            break 2; // we do not want any power on administrative
                        }

                        break;

                    case 'administrative':
                    case 'direction':
                        if (\in_array($scope->getName()['en'], ['administrative', 'social'], true)) {
                            break 2; // we do not want any power on social or administrative
                        }

                        break;
                }

                printf(
                    'Adding CHILL_TASK_TASK_UPDATE & CHILL_TASK_TASK_CREATE & Chill_TASK_TASK_DELETE permissions to %s '
                        ."permission group, scope '%s' \n",
                    $permissionsGroup->getName(),
                    $scope->getName()['en']
                );
                $roleScopeUpdate = (new RoleScope())
                    ->setRole(TaskVoter::UPDATE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeUpdate);
                $roleScopeCreateP = (new RoleScope())
                    ->setRole(TaskVoter::CREATE_PERSON)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreateP);
                $roleScopeCreateC = (new RoleScope())
                    ->setRole(TaskVoter::CREATE_COURSE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreateC);
                $roleScopeDelete = (new RoleScope())
                    ->setRole(TaskVoter::DELETE)
                    ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeDelete);

                $manager->persist($roleScopeUpdate);
                $manager->persist($roleScopeCreateP);
                $manager->persist($roleScopeCreateC);
                $manager->persist($roleScopeDelete);
            }
        }

        $manager->flush();
    }
}
