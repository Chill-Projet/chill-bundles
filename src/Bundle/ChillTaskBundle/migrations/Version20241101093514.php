<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Task;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241101093514 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set a default for task current_states';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_task.recurring_task ALTER current_states SET DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE chill_task.single_task ALTER current_states SET DEFAULT \'[]\'');

    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_task.single_task ALTER current_states DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_task.recurring_task ALTER current_states DROP DEFAULT');
    }
}
