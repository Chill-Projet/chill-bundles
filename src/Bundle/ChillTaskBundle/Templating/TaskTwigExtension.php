<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Templating;

use Chill\TaskBundle\Entity\AbstractTask;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TaskTwigExtension extends AbstractExtension
{
    /**
     * @var TaskWorkflowManager
     */
    protected $taskWorkflowManager;

    public function __construct(TaskWorkflowManager $taskWorkflowManager)
    {
        $this->taskWorkflowManager = $taskWorkflowManager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('task_workflow_metadata', $this->getWorkflowMetadata(...)),
        ];
    }

    public function getWorkflowMetadata(
        AbstractTask $task,
        string $key,
        $metadataSubject = null,
        ?string $name = null,
    ) {
        return $this->taskWorkflowManager->getWorkflowMetadata($task, $key, $metadataSubject, $name);
    }
}
