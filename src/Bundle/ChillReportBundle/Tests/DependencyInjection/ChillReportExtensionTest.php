<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Tests\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ChillReportExtensionTest extends KernelTestCase
{
    /*
     * Check if class Chill\ReportBundle\Entity\Report is in chill_custom_fields.customizables_entities
     */
    /**
     * @doesNotPerformAssertions
     */
    public function testDeclareReportAsCustomizable()
    {
        self::bootKernel(['environment' => 'test']);
        $customizablesEntities = self::$kernel->getContainer()
            ->getParameter('chill_custom_fields.customizables_entities');

        $reportFounded = false;

        foreach ($customizablesEntities as $customizablesEntity) {
            if (\Chill\ReportBundle\Entity\Report::class === $customizablesEntity['class']) {
                $reportFounded = true;
            }
        }

        if (!$reportFounded) {
            throw new \Exception('Class Chill\ReportBundle\Entity\Report not found in chill_custom_fields.customizables_entities', 1);
        }
    }
}
