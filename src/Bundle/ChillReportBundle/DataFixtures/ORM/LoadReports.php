<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\DataFixtures\ORM;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\DataFixtures\ORM\LoadUsers;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use Chill\ReportBundle\Entity\Report;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as FakerFactory;

/**
 * Load reports into DB.
 */
final class LoadReports extends AbstractFixture implements OrderedFixtureInterface
{
    private readonly \Faker\Generator $faker;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
        $this->faker = FakerFactory::create('fr_FR');
    }

    public function getOrder(): int
    {
        return 15002;
    }

    public function load(ObjectManager $manager): void
    {
        $this->createExpected($manager);

        // create random 2 times, to allow multiple report on some people
        $this->createRandom($manager, 90);
        $this->createRandom($manager, 30);

        $manager->flush();
    }

    private function createExpected(ObjectManager $manager)
    {
        $charline = $this->entityManager
            ->getRepository(Person::class)
            ->findOneBy(['firstName' => 'Charline', 'lastName' => 'DEPARDIEU']);

        if (null !== $charline) {
            $report = (new Report())
                ->setPerson($charline)
                ->setCFGroup($this->getReference('cf_group_report_logement', CustomFieldsGroup::class))
                ->setDate(new \DateTime('2015-01-05'))
                ->setScope($this->getReference('scope_social', Scope::class));
            $this->fillReport($report);

            $manager->persist($report);
        } else {
            echo 'WARNING: Charline DEPARDIEU not found in database';
        }
    }

    private function createRandom(ObjectManager $manager, $percentage)
    {
        $people = $this->getPeopleRandom($percentage);

        foreach ($people as $person) {
            // create a report, set logement or education report
            $report = (new Report())
                ->setPerson($person)
                ->setCFGroup(
                    random_int(0, 10) > 5 ?
                            $this->getReference('cf_group_report_logement', CustomFieldsGroup::class) :
                            $this->getReference('cf_group_report_education', CustomFieldsGroup::class)
                )
                ->setScope($this->getScopeRandom());
            $this->fillReport($report);
            $manager->persist($report);
        }
    }

    private function fillReport(Report $report)
    {
        // setUser
        $usernameRef = array_rand(LoadUsers::$refs);
        $report->setUser(
            $this->getReference($usernameRef, User::class)
        );

        // set date if null
        if (null === $report->getDate()) {
            // set date. 30% of the dates are 2015-05-01
            $expectedDate = new \DateTime('2015-01-05');

            if (random_int(0, 100) < 30) {
                $report->setDate($expectedDate);
            } else {
                $report->setDate($this->faker->dateTimeBetween('-1 year', 'now')
                    ->setTime(0, 0, 0));
            }
        }

        // fill data
        $datas = [];

        foreach ($report->getCFGroup()->getCustomFields() as $field) {
            switch ($field->getType()) {
                case 'title':
                    $datas[$field->getSlug()] = [];

                    break;

                case 'choice':
                    $datas[$field->getSlug()] = $this->getRandomChoice($field);

                    break;

                case 'text':
                    $datas[$field->getSlug()] = $this->faker->realText($field->getOptions()['maxLength']);

                    break;
            }
        }
        $report->setCFData($datas);

        return $report;
    }

    private function getPeopleRandom($percentage)
    {
        $people = $this->entityManager
            ->getRepository(Person::class)
            ->findAll();

        // keep only a part ($percentage) of the people
        $selectedPeople = [];

        foreach ($people as $person) {
            if (random_int(0, 100) < $percentage) {
                $selectedPeople[] = $person;
            }
        }

        return $selectedPeople;
    }

    /**
     * pick a random choice.
     *
     * @return string|string[]
     */
    private function getRandomChoice(CustomField $field): array|string
    {
        $choices = $field->getOptions()['choices'];
        $multiple = $field->getOptions()['multiple'];
        $other = $field->getOptions()['other'];

        // add other if allowed
        if ($other) {
            $choices[] = ['slug' => '_other'];
        }

        // initialize results
        $picked = [];

        if ($multiple) {
            $numberSelected = random_int(1, \count($choices) - 1);

            for ($i = 0; $i < $numberSelected; ++$i) {
                $picked[] = $this->pickChoice($choices);
            }

            if ($other) {
                $result = ['_other' => null, '_choices' => $picked];

                if (\in_array('_other', $picked, true)) {
                    $result['_other'] = $this->faker->realText(70);
                }

                return $result;
            }
        } else {
            $picked = $this->pickChoice($choices);

            if ($other) {
                $result = ['_other' => null, '_choices' => $picked];

                if ('_other' === $picked) {
                    $result['_other'] = $this->faker->realText(70);
                }

                return $result;
            }
        }

        return $picked;
    }

    /**
     * @return Scope
     */
    private function getScopeRandom()
    {
        $ref = LoadScopes::$references[array_rand(LoadScopes::$references)];

        return $this->getReference($ref, Scope::class);
    }

    /**
     * pick a choice within a 'choices' options (for choice type).
     *
     * @return string the slug of the selected choice
     */
    private function pickChoice(array $choices)
    {
        return $choices[array_rand($choices)]['slug'];
    }
}
