<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\DataFixtures\ORM;

use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load CustomFieldsGroup for Report into database.
 */
class LoadCustomFieldsGroup extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 15000;
    }

    public function load(ObjectManager $manager): void
    {
        echo "loading customFieldsGroup...\n";

        $report = $this->createReport(
            $manager,
            ['fr' => 'Situation de logement'],
            ['summary_fields' => ['has_logement', 'house-desc']]
        );
        $this->addReference(
            'cf_group_report_logement',
            $report
        );

        $report = $this->createReport($manager, ['fr' => 'Alphabétisme']);
        $this->addReference('cf_group_report_education', $report);

        for ($i = 0; 3 >= $i; ++$i) {
            $report = $this->createReport($manager, ['fr' => 'ZZ Rapport aléatoire '.$i]);

            $this->addReference('cf_group_report_'.$i, $report);
        }

        $manager->flush();
    }

    /**
     * create a report and persist in the db.
     *
     * @return CustomFieldsGroup
     */
    private function createReport(
        ObjectManager $manager,
        array $name,
        array $options = [],
    ) {
        echo $name['fr']." \n";

        $cFGroup = (new CustomFieldsGroup())
            ->setName($name)
            ->setEntity(\Chill\ReportBundle\Entity\Report::class)
            ->setOptions($options);

        $manager->persist($cFGroup);

        return $cFGroup;
    }
}
