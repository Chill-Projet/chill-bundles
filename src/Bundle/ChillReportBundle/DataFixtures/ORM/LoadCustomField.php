<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\DataFixtures\ORM;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load CustomField for Report into database.
 */
class LoadCustomField extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder(): int
    {
        return 15001;
    }

    public function load(ObjectManager $manager): void
    {
        $cFTypes = [
            ['type' => 'text', 'options' => ['maxLength' => '255']],
            ['type' => 'text', 'options' => ['maxLength' => '1000']],
            ['type' => 'text', 'options' => ['maxLength' => '2000']],
            ['type' => 'title', 'options' => ['type' => 'title']],
            ['type' => 'title', 'options' => ['type' => 'subtitle']],
            ['type' => 'choice', 'options' => [
                'multiple' => false,
                'expanded' => false,
                'other' => false,
                'choices' => [
                    [
                        'name' => [
                            'fr' => 'Options 1 FR',
                            'nl' => 'Options 1 NL',
                            'en' => 'Options 1 EN', ],
                        'active' => true,
                        'slug' => 'el-1-fr', ],
                    [
                        'name' => [
                            'fr' => 'Options 2 FR',
                            'nl' => 'Options 2 NL',
                            'en' => 'Options 2 EN', ],
                        'active' => true,
                        'slug' => 'el-2-fr', ],
                    [
                        'name' => [
                            'fr' => 'Options 2 FR',
                            'nl' => 'Options 2 NL',
                            'en' => 'Options 2 EN', ],
                        'active' => true,
                        'slug' => 'el-3-fr', ],
                ],
            ],
            ],
        ];

        for ($i = 0; 25 >= $i; ++$i) {
            $cFType = $cFTypes[random_int(0, \count($cFTypes) - 1)];

            $customField = (new CustomField())
                ->setSlug("cf_report_{$i}")
                ->setType($cFType['type'])
                ->setOptions($cFType['options'])
                ->setName(['fr' => "CustomField {$i}"])
                ->setOrdering(random_int(0, 1000) / 1000)
                ->setCustomFieldsGroup($this->getReference('cf_group_report_'.random_int(0, 3), CustomFieldsGroup::class));

            $manager->persist($customField);
        }

        $this->createExpectedFields($manager);

        $manager->flush();
    }

    private function createExpectedFields(ObjectManager $manager)
    {
        // report logement
        $reportLogement = $this->getReference('cf_group_report_logement', CustomFieldsGroup::class);

        $houseTitle = (new CustomField())
            ->setSlug('house_title')
            ->setType('title')
            ->setOptions(['type' => 'title'])
            ->setName(['fr' => 'Situation de logement'])
            ->setOrdering(10)
            ->setCustomFieldsGroup($reportLogement);
        $manager->persist($houseTitle);

        $hasLogement = (new CustomField())
            ->setSlug('has_logement')
            ->setName(['fr' => 'Logement actuel'])
            ->setType('choice')
            ->setOptions([
                'multiple' => false,
                'expanded' => true,
                'other' => true,
                'choices' => [
                    [
                        'name' => ['fr' => 'Locataire d\' un logement'],
                        'slug' => 'rent_house',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Propriétaire d\' un logement'],
                        'slug' => 'own_house',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Par-ci, par là (amis, famille, ...)'],
                        'slug' => 'here-and-there',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'A la rue'],
                        'slug' => 'street',
                        'active' => true,
                    ],
                ],
            ])
            ->setOrdering(20)
            ->setCustomFieldsGroup($reportLogement);
        $manager->persist($hasLogement);

        $descriptionLogement = (new CustomField())
            ->setSlug('house-desc')
            ->setName(['fr' => 'Plaintes éventuelles sur le logement'])
            ->setType('text')
            ->setOptions(['maxLength' => 1500])
            ->setOrdering(30)
            ->setCustomFieldsGroup($reportLogement);
        $manager->persist($descriptionLogement);

        // report problems
        $reportEducation = $this->getReference('cf_group_report_education', CustomFieldsGroup::class);

        $title = (new CustomField())
            ->setSlug('title')
            ->setType('title')
            ->setOptions(['type' => 'title'])
            ->setName(['fr' => 'Éducation'])
            ->setOrdering(10)
            ->setCustomFieldsGroup($reportEducation);
        $manager->persist($title);

        $educationLevel = (new CustomField())
            ->setSlug('level')
            ->setName(['fr' => 'Niveau du plus haut diplôme'])
            ->setType('choice')
            ->setOptions([
                'multiple' => false,
                'expanded' => false,
                'other' => false,
                'choices' => [
                    [
                        'name' => ['fr' => 'Supérieur'],
                        'slug' => 'superieur',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Secondaire supérieur (CESS)'],
                        'slug' => 'cess',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Secondaire deuxième degré ou inférieur (C2D)'],
                        'slug' => 'c2d',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Primaire'],
                        'slug' => 'low',
                        'active' => true,
                    ],
                    [
                        'name' => ['fr' => 'Aucun diplome'],
                        'slug' => 'no',
                        'active' => true,
                    ],
                ],
            ])
            ->setOrdering(20)
            ->setCustomFieldsGroup($reportEducation);
        $manager->persist($educationLevel);
    }
}
